/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "la_5.h"

extern LA MAIN;
extern tnsMain* T;

void la_AnimationEvaluateActions(int ClampOffsets);
void laget_AnimationActionHolderCategory(void* a_unused, laActionHolder* ah, char* copy, char** ptr);
void laui_AnimationActionHolder(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *ExtraColumns, int context);

int laAnimationRegisterHolderPath(char* Path){
    laPropPack PP={0}; if(!la_GetPropFromPath(&PP,0,Path,0)){ la_FreePropStepCache(PP.Go); return 0; }
    la_EnsureSubTarget(PP.LastPs->p,0);
    if((!PP.LastPs->p->SubProp) || (!la_PropLookup(&PP.LastPs->p->SubProp->Props,"__actions__"))){
        la_FreePropStepCache(PP.Go); return 0;
    }
    laActionHolderPath* ahp=memAcquire(sizeof(laActionHolderPath));
    ahp->OriginalPath=Path;
    la_CopyPropPack(&PP,&ahp->PP); lstAppendItem(&MAIN.Animation->ActionHolderPaths,ahp);
    la_FreePropStepCache(PP.Go);
    return 1;
}

void laAnimationUpdateHolderList(){
    laActionHolder* ah;
    while(ah=lstPopItem(&MAIN.Animation->ActionHolders)){
        strSafeDestroy(&ah->Name); strSafeDestroy(&ah->CategoryTitle); memFree(ah);
    }

    for(laActionHolderPath* ahp=MAIN.Animation->ActionHolderPaths.pFirst;ahp;ahp=ahp->Item.pNext){
        la_StepPropPack(&ahp->PP); laSubProp* pa=ahp->PP.LastPs->p; la_EnsureSubTarget(pa,0); laPropIterator pi={0};
        int ListOffset=0,PropOffset=0;
        laSubProp* paa=la_PropLookup(&pa->Base.SubProp->Props,"__actions__");
        laSubProp* paap=la_PropLookup(&pa->Base.SubProp->Props,"__action_props__");
        if((!paa) || (!paap) || (!paa->ListHandleOffset) || (!paap->ListHandleOffset)){ continue; }
        ListOffset=paa->ListHandleOffset; PropOffset=paap->ListHandleOffset;
        void* inst=laGetInstance(ahp->PP.LastPs->p,ahp->PP.LastPs->UseInstance,&pi); int FirstIn=1;
        while(inst){
            ah=memAcquire(sizeof(laActionHolder));
            memAssignRef(ah,&ah->Instance,inst);
            ah->ActionOffset=ListOffset; ah->PropOffset=PropOffset;
            ah->Container=ahp->PP.LastPs->p->SubProp;
            char _id[64]="unamed", *id=_id;
            laTryGetInstanceIdentifier(inst,pa->Base.SubProp,_id,&id);
            strSafeSet(&ah->Name,id);
            if(FirstIn){ strSafeSet(&ah->CategoryTitle,pa->Base.SubProp->Name); FirstIn=0; }
            lstAppendItem(&MAIN.Animation->ActionHolders,ah);
            inst=laGetNextInstance(ahp->PP.LastPs->p, inst, &pi);
        }
    }
    laNotifyUsers("la.animation.action_holders"); 
}
laActionHolder* laFindAnimationHolder(void* inst,char* container){
    for(laActionHolder*ah=MAIN.Animation->ActionHolders.pFirst;ah;ah=ah->Item.pNext){
        if(ah->Instance==inst){ if(ah->Container&&strSame(ah->Container->Identifier,container)) return ah; }
    }
    return 0;
}

laAction* laAnimiationNewAction(laActionHolder* ah, char* Name){
    laAction* aa=memAcquire(sizeof(laAction));
    if(!Name || !Name[0]){ Name="New Action"; }
    strSafeSet(&aa->Name,Name);
    aa->Length=2; aa->FrameCount=24; aa->HolderContainer=ah->Container;
    memAssignRef(aa,&aa->HolderInstance,ah->Instance);
    memAssignRef(MAIN.Animation,&MAIN.Animation->CurrentAction,aa);
    void* lh=((uint8_t*)ah->Instance)+ah->ActionOffset;
    lstAppendItem(lh,aa);
    laNotifyInstanceUsers(ah->Instance);
    laNotifyUsers("la.animation.current_action");
    return aa;
}
laActionProp* laAnimationEnsureProp(laAction* aa, void* hyper1, laProp* p){
    int DataSize=la_GetKeyablePropertyStorageSize(p); if(!DataSize || !aa->HolderInstance || !aa->HolderContainer) return 0;
    laSubProp* paap=la_PropLookup(&aa->HolderContainer->Props,"__action_props__"); if(!paap){ return 0; }
    laListHandle *pl=((uint8_t*)aa->HolderInstance)+paap->ListHandleOffset;
    for(laActionProp* ap=pl->pFirst;ap;ap=ap->Item.pNext){ if(ap->For==hyper1 && ap->Prop==p){ return ap; } }
    laActionProp* ap = memAcquire(sizeof(laActionProp)); lstAppendItem(pl,ap);
    memAssignRef(ap,&ap->For,hyper1); ap->Prop=p;
    ap->DataSize = DataSize;
    char _name[128]={0}, *name=_name; laTryGetInstanceIdentifier(hyper1, p->Container,_name,&name);
    strSafeSet(&ap->CachedName,name); strSafePrint(&ap->CachedName," ⯈ %s",p->Identifier);
    return ap;
}
laActionChannel* laAnimationEnsureChannel(laAction* aa, void* hyper1, laProp* p){
    laActionProp* ap=laAnimationEnsureProp(aa, hyper1,p); if(!ap) return 0; laActionChannel* ac;
    for(ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){ if(ac->AP==ap) return ac; }
    ac=memAcquire(sizeof(laActionChannel)); ac->AP=ap;
    lstAppendItem(&aa->Channels,ac); laNotifyUsers("la.animation.current_action.channels");
    return ac;
}
laActionChannel* laAnimationEnsureFrame(laActionChannel* ac, int frame){
    laActionKey* akf=0,*beforeakf=0;
    for(laActionKey* ak=ac->Keys.pFirst;ak;ak=ak->Item.pNext){
        if(ak->At==frame){ akf=ak; break; } if(ak->At>frame){ beforeakf=ak; break; }
    }
    if(!akf){
        akf=memAcquireSimple(sizeof(laActionKey));
        akf->At=frame; akf->Data=memAcquireSimple(ac->AP->DataSize); akf->DataSize=ac->AP->DataSize;
        if(beforeakf){ lstInsertItemBefore(&ac->Keys, akf, beforeakf); }
        else{ lstAppendItem(&ac->Keys, akf); }
    }
    return akf;
}
void laAnimationStoreKeyValue(laActionChannel* ac, laActionKey* ak){
    laPropPack PP={0}; laPropStep PS={0}; laActionProp* ap=ac->AP; if(!ap) return;
    PS.p=ap->Prop; PS.Type='.'; PS.UseInstance=ap->For; PP.LastPs=&PS; PP.EndInstance=ap->For;
    switch(ap->Prop->PropertyType){
    case LA_PROP_INT: case LA_PROP_INT | LA_PROP_ARRAY:     laGetIntArray(&PP,(int*)ak->Data); break;
    case LA_PROP_FLOAT: case LA_PROP_FLOAT | LA_PROP_ARRAY: laGetFloatArray(&PP,(real*)ak->Data); break;
    case LA_PROP_ENUM: case LA_PROP_ENUM | LA_PROP_ARRAY:   laGetEnumArray(&PP,(laEnumItem**)ak->Data); break;
    case LA_PROP_SUB: case LA_PROP_OPERATOR: case LA_PROP_STRING: case LA_PROP_RAW: default: return;
    }
}
laActionKey* laAnimationInsertKeyFrame(laAction* aa, void* hyper1, laProp* p, int* error){
    if(error) *error=0; if(!aa) return 0;
    if(!aa->HolderInstance||!aa->HolderContainer){ if(error) *error=1; return 0; }
    if(aa->HolderContainer->ActionHolderVerify){
        if(!aa->HolderContainer->ActionHolderVerify(aa->HolderInstance,aa->HolderContainer,hyper1,p->Container)){
            if(error) *error=2; return 0;
        }
    }
    laActionChannel* ac=laAnimationEnsureChannel(aa,hyper1,p); if(!ac || !ac->AP || !ac->AP->For) return 0;
    int frame=LA_ACTION_FRAME(aa,-1);
    laActionKey* ak=laAnimationEnsureFrame(ac,frame);
    laAnimationStoreKeyValue(ac,ak);
    laNotifyUsers("la.animation.current_action");
    return ak;
}

void laAnimationSetPlayStatus(int PlayStatus){
    if(PlayStatus>3||PlayStatus<0) PlayStatus=0; MAIN.Animation->PlayStatus=PlayStatus;
    if(PlayStatus) laRecordTime(&MAIN.Animation->TimeOrigin);
    laNotifyUsers("la.animation.play_status");
}
void laAnimationSetPlayHead(real time){
    MAIN.Animation->PlayHead=time;
}

STRUCTURE(laNewActionData){
    laActionHolder* SelectedHolder;
};
laActionHolder* laget_AnimationFirstActionHolder(void* unused1, void* unused2){
    return MAIN.Animation->ActionHolders.pFirst;
}
void laset_AnimationNewActionSetHolder(laNewActionData *np, laActionHolder *ah, int State){
    np->SelectedHolder = ah;
}
int OPINV_AnimationNewAction(laOperator *a, laEvent *e){
    laAnimationUpdateHolderList();
    laNewActionData* np= memAcquire(sizeof(laNewActionData));
    a->CustomData = np;
    laEnableOperatorPanel(a, 0, e->x-50,e->y-50,500,500,10000,0,0,0,0,0,0,0,e);
    return LA_RUNNING;
}
int OPMOD_AnimationNewAction(laOperator *a, laEvent *e){
    laNewActionData* np=a->CustomData;

    if(!a->ConfirmData) return LA_RUNNING;

    if(a->ConfirmData->Mode == LA_CONFIRM_CANCEL||a->ConfirmData->Mode == LA_CONFIRM_OK){ if(np) memFree(np); return LA_CANCELED; }

    if(a->ConfirmData->Mode == LA_CONFIRM_DATA){
        if (!np || !np->SelectedHolder){ if(np) memFree(np); return LA_CANCELED; }
        laAnimiationNewAction(np->SelectedHolder,0);
        laRecordInstanceDifferences(np->SelectedHolder->Instance,np->SelectedHolder->Container->Identifier);
        laRecordDifferences(0,"la.animation"); laPushDifferences("New action",0); memFree(np);
        return LA_FINISHED;
    }

    return LA_RUNNING;
}
void laui_AnimationNewAction(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED, int context){
    laColumn *c = laFirstColumn(uil);
    laUiItem* ui=laShowItem(uil, c, OperatorProps, "holder");ui->Extent=3; ui->Flags|=LA_UI_FLAGS_NO_DECAL;
}

int OPINV_AnimationSelectAction(laOperator *a, laEvent *e){
    laEnableOperatorPanel(a, 0, e->x-50,e->y-50,500,500,10000,0,0,0,0,0,0,0,e);
    return LA_RUNNING;
}
void laui_AnimationSelectAction(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED, int context){
    laColumn *c = laFirstColumn(uil);
    for(laActionHolderPath* ahp=MAIN.Animation->ActionHolderPaths.pFirst;ahp;ahp=ahp->Item.pNext){
        laShowLabel(uil,c,ahp->PP.LastPs->p->Name,0,0)->Flags|=LA_UI_FLAGS_DISABLED|LA_TEXT_MONO;
        laShowItemFull(uil,c,0,ahp->OriginalPath,LA_WIDGET_COLLECTION,"feedback=NONE",laui_AnimationActionHolder,0)->Flags|=LA_UI_FLAGS_NO_DECAL|LA_UI_FLAGS_NO_GAP;
    }
}

void laAnimationRemoveFrame(laActionChannel* ac, laActionKey* ak){
    ak->DataSize=0; memLeave(ak->Data); ak->Data=0;
    lstRemoveItem(&ac->Keys,ak); memLeave(ak);
}
void laAnimationRemoveChannel(laAction* aa, laActionChannel* ac){
    while(ac->Keys.pFirst){ laAnimationRemoveFrame(ac,ac->Keys.pFirst); }
    lstRemoveItem(&aa->Channels,ac); memLeave(ac);
}
void laAnimationRemoveAction(laAction* aa){
    while(aa->Channels.pFirst){ laAnimationRemoveChannel(aa,aa->Channels.pFirst); }
    if(!aa->HolderContainer || !aa->HolderInstance){ return; }
    strSafeDestroy(&aa->Name);
    if(aa==MAIN.Animation->CurrentAction){
        if(aa->Item.pNext){ memAssignRef(MAIN.Animation,&MAIN.Animation->CurrentAction,aa->Item.pNext); }
        else{ memAssignRef(MAIN.Animation,&MAIN.Animation->CurrentAction,aa->Item.pPrev); }
    }
    laSubProp* paa=la_PropLookup(&aa->HolderContainer->Props,"__actions__"); if(!paa){ goto action_remove_clean; }
    laListHandle* al=((uint8_t*)aa->HolderInstance)+paa->ListHandleOffset;
    lstRemoveItem(al,aa);
action_remove_clean:
    memLeave(aa);
}

void* laAnimationGetRetargetedPropInstance(laProp* p, void* Instance){
    laPropContainer* pc=p->Container;
    laProp* rp = la_PropLookup(&pc->Props,"__action_retarget__");
    if(rp->PropertyType!=LA_PROP_SUB) return Instance;
    laPropIterator pi={0};
    void* retargeted = laGetInstance(rp,Instance,&pi);
    return retargeted;
}
void laAnimationEnsureRetarget(void* HolderInstance, laListHandle* action_list, laActionRetarget** retarget){
    if((*retarget) || (!HolderInstance) || (!action_list)){ return; }
    int count = lstCountElements(action_list); if(!count){ return; }

    laActionRetarget* ar=memAcquire(sizeof(laActionRetarget));
    ar->ActionCount = count; ar->Actions = action_list; ar->HolderInstance = HolderInstance;
    ar->Retargeted = memAcquireSimple(sizeof(laRetargetedAction)*count);
    int i=0;
    for(laAction* aa=action_list->pFirst;aa;aa=aa->Item.pNext){
        int channels = lstCountElements(&aa->Channels);
        ar->Retargeted[i].Instances = memAcquireSimple(sizeof(void*)*channels);
        ar->Retargeted[i].PlayStatus = aa->PlayByDefault?LA_ANIMATION_STATUS_PLAY_FWD:0;
        int j=0;
        for(laActionChannel* ac = aa->Channels.pFirst;ac;ac=ac->Item.pNext){
            ar->Retargeted[i].Instances[j] = laAnimationGetRetargetedPropInstance(ac->AP->Prop,ac->AP->For); j++;
        }
        i++;
    }
    *retarget = ar;
}
laActionRetarget* laAnimationCopyRetargetFrom(laActionRetarget* from_ar){
    if(!from_ar) return 0;
    laActionRetarget* ar=0;
    laAnimationEnsureRetarget(from_ar->HolderInstance, from_ar->Actions, &ar);
    return ar;
}
void laAnimationClearRetarget(laActionRetarget **ar_ptr){
    if(!ar_ptr) return;
    laActionRetarget *ar = *ar_ptr;
    if(!ar || !ar->ActionCount) return;
    for(int i=0;i<ar->ActionCount;i++){ memFree(ar->Retargeted[i].Instances); }
    memFree(ar->Retargeted); memFree(ar); *ar_ptr=0;
}
void laSetRunPlayHead(real RunPlayHead){
    MAIN.Animation->RunPlayHead = RunPlayHead;
}

int OPCHK_AnimationRemoveAction(laPropPack *This, laStringSplitor *Instructions){
    laPropContainer* pc; return LA_VERIFY_THIS_TYPE(This,pc,"la_animation_action");
}
int OPINV_AnimationRemoveAction(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance){ return LA_CANCELED; }
    laAction* aa=a->This->EndInstance;
    char str[256]; sprintf(str,"Will remove action \"%s\".",SSTR(aa->Name));
    laEnableYesNoPanel(a,0,"Remove?",str,e->x,e->y,200,e);
    return LA_RUNNING;
}
int OPMOD_AnimationRemoveAction(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance){ return LA_CANCELED; }
    laAction* aa=a->This->EndInstance;

    if(a->ConfirmData && a->ConfirmData->Mode==LA_CONFIRM_OK){
        laAnimationRemoveAction(aa); laNotifyUsers("la.animation");
        laRecordInstanceDifferences(aa->HolderInstance,aa->HolderContainer->Identifier);
        laRecordDifferences(0,"la.animation"); laPushDifferences("Remove action",0);
    }

    return LA_FINISHED;
}

int OPINV_AnimationPlayAction(laOperator *a, laEvent *e){
    char* str=strGetArgumentString(a->ExtraInstructionsP, "mode");
    int PlayStatus=LA_ANIMATION_STATUS_PAUSED;
    if(strSame(str,"forward")){ PlayStatus=LA_ANIMATION_STATUS_PLAY_FWD; }
    elif(strSame(str,"reverse")){ if(MAIN.Animation->PlayHead>0) PlayStatus=LA_ANIMATION_STATUS_PLAY_REV; }
    laAnimationSetPlayStatus(PlayStatus);
    return LA_FINISHED;
}

void la_AnimationActionSetOwnFrame(laAction* aa, int frame, int clamp){
    if(!aa) return;
    aa->Offset+=(aa->PlayHead-(real)frame/aa->FrameCount)*aa->Length;
    la_AnimationEvaluateActions(clamp);
    laNotifyUsers("la.animation.current_action");
}

int OPINV_AnimationResetTime(laOperator *a, laEvent *e){
    char* arg=strGetArgumentString(a->ExtraInstructionsP,"current");
    if(strSame(arg,"true")){ la_AnimationActionSetOwnFrame(MAIN.Animation->CurrentAction,0,1); return LA_FINISHED; }
    laAnimationSetPlayHead(0); la_AnimationEvaluateActions(1);
    return LA_FINISHED;
}


laActionChannel* laAnimationGetFrame(laActionChannel* ac, int frame){
    if(ac->Keys.pFirst==ac->Keys.pLast){ return ac->Keys.pFirst; }
    for(laActionKey* ak=ac->Keys.pFirst;ak;ak=ak->Item.pNext){
        if(ak->At<=frame && ((!ak->Item.pNext) ||ak->Item.pNext && ((laActionKey*)ak->Item.pNext)->At>frame)){ return ak; }
    }
    return ac->Keys.pFirst;
}

void la_AnimationMarkPropReset(){
    for(laActionHolder* ah=MAIN.Animation->ActionHolders.pFirst;ah;ah=ah->Item.pNext){
        if(!ah->Instance){ continue; } laListHandle* lp=((uint8_t*)ah->Instance)+ah->PropOffset;
        for(laActionProp* ap=lp->pFirst;ap;ap=ap->Item.pNext){ ap->Reset=1; }
    }
}
void la_AnimationInterpolateKeys(laActionChannel* ac, laActionKey* ak1, laActionKey* ak2, real PlayHead_mul_Length, void** data){
    int* id1,*id2,*iret=(*data); real* fd1,*fd2,*fret=(*data);
    if(!ak2){ *data=ak1->Data; return; } laActionProp* ap=ac->AP; if(!ap) return;
    int arrlen=ap->Prop->Len?ap->Prop->Len:1;
    real fac=tnsGetRatiod(ak1->At,ak2->At,PlayHead_mul_Length); TNS_CLAMP(fac,0,1);
    switch(ap->Prop->PropertyType){
    case LA_PROP_INT: case LA_PROP_INT|LA_PROP_ARRAY:
        id1=ak1->Data;id2=ak2->Data; for(int i=0;i<arrlen;i++){ iret[i]=tnsLinearItp(id1[i],id2[i],fac); } break;
    case LA_PROP_FLOAT: case LA_PROP_FLOAT|LA_PROP_ARRAY:
        fd1=ak1->Data;fd2=ak2->Data; for(int i=0;i<arrlen;i++){ fret[i]=tnsLinearItp(fd1[i],fd2[i],fac); } break;
    default:
        *data=ak1->Data; break;
    }
}
void la_AnimationSetPropValue(laActionProp* ap, void* OverrideInstance){
    void* data=ap->Data;
    laPropPack PP={0}; laPropStep PS={0}; if(!ap) return;
    PS.p=ap->Prop; PS.Type='.'; PS.UseInstance=OverrideInstance?OverrideInstance:ap->For; PP.LastPs=&PS; PP.EndInstance=OverrideInstance?OverrideInstance:ap->For;
    switch(ap->Prop->PropertyType){
    case LA_PROP_INT: laSetInt(&PP,*((int*)data)); break;
    case LA_PROP_INT|LA_PROP_ARRAY: laSetIntArrayAllArray(&PP,(int*)data); break;
    case LA_PROP_FLOAT: laSetFloat(&PP,*((real*)data)); break;
    case LA_PROP_FLOAT|LA_PROP_ARRAY: laSetFloatArrayAllArray(&PP,(real*)data); break;
    case LA_PROP_ENUM: laSetEnum(&PP,*((int*)data)); break;
    //case LA_PROP_ENUM|LA_PROP_ARRAY: laSetEnumArrayAllArray(&PP,(real*)data); break; //doesnt work
    default: break;
    }
}
void la_AnimationMixChannelValue(laActionChannel* ac, void* data, int MixMode, real Factor){
    laActionProp* ap=ac->AP; if(!ap) return;  int* ip=ap->Data,*ipd=data; real* fp=ap->Data,*fpd=data;
    int arrlen=ap->Prop->Len?ap->Prop->Len:1;
    switch(ap->Prop->PropertyType){
    case LA_PROP_INT:  case LA_PROP_INT|LA_PROP_ARRAY: for(int i=0;i<arrlen;i++){ 
            ip[i]=ap->Reset?ipd[i]:(MixMode==LA_ANIMATION_MIX_REPLACE?ipd[i]:(ipd[i]+ip[i]));
        } break;
    case LA_PROP_FLOAT: case LA_PROP_FLOAT|LA_PROP_ARRAY: for(int i=0;i<arrlen;i++){
            fp[i]=ap->Reset?fpd[i]:(MixMode==LA_ANIMATION_MIX_REPLACE?fpd[i]:(fpd[i]+fp[i]));
        } break;
    case LA_PROP_ENUM: ip[0]=ipd[0]; break;
    //case LA_PROP_ENUM|LA_PROP_ARRAY: laSetEnumArrayAllArray(&PP,(real*)data); break; //doesnt work
    default: break;
    }
    ap->Reset=0;
}
void la_AnimationEvaluateActionChannels(laAction* aa, real OverridePlayHead){
    int64_t _data[16]; void* data=_data;
    int frame=LA_ACTION_FRAME(aa,OverridePlayHead); real totframe=((OverridePlayHead>=0)?OverridePlayHead:aa->PlayHead)*aa->FrameCount;
    for(laActionChannel* ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){
        data=_data;
        laActionKey* ak1=laAnimationGetFrame(ac,frame); if(!ak1) continue;
        laActionKey* ak2=ak1->Item.pNext;
        la_AnimationInterpolateKeys(ac,ak1,ak2,totframe,&data);
        la_AnimationMixChannelValue(ac, data, aa->MixMode, 0);
    }
}
void la_AnimationEvaluateActions(int ClampOffsets){
    int any=0;
    la_AnimationMarkPropReset();
    for(laActionHolder* ah=MAIN.Animation->ActionHolders.pFirst;ah;ah=ah->Item.pNext){
        if(!ah->Instance) continue;
        laListHandle* lh=((uint8_t*)ah->Instance)+ah->ActionOffset;
        for(laAction* aa=lh->pFirst;aa;aa=aa->Item.pNext){
            real preoffset=0,postoffset=aa->Offset/aa->Length;
            if(ClampOffsets || (MAIN.Animation->PlayStatus!=LA_ANIMATION_STATUS_PAUSED)){
                while(aa->Offset>aa->Length*2){ aa->Offset-=aa->Length*2; }
                while(aa->Offset<0){ aa->Offset+=aa->Length*2; }
                preoffset=aa->Offset; postoffset=0;
            }
            real UseTime=MAIN.Animation->PlayHead-preoffset+aa->Length*2;
            int repeats=(real)(UseTime/aa->Length);
            real remaining=UseTime-repeats*aa->Length;
            while(remaining<0){ remaining+=aa->Length; }
            if(aa->PlayMode==LA_ANIMATION_PLAY_MODE_REPEAT){ aa->PlayHead=remaining/aa->Length-postoffset; }
            elif(aa->PlayMode==LA_ANIMATION_PLAY_MODE_HOLD){ aa->PlayHead=((UseTime>aa->Length)?1.0:(UseTime<0?0:UseTime/aa->Length))-postoffset; }
            elif(aa->PlayMode==LA_ANIMATION_PLAY_MODE_BOUNCE){ real t=remaining/aa->Length; aa->PlayHead=((repeats%2)?(1-t):t)-postoffset; }
            any=1;
            la_AnimationEvaluateActionChannels(aa,-1);
        }
    }
    
    for(laActionHolder* ah=MAIN.Animation->ActionHolders.pFirst;ah;ah=ah->Item.pNext){
        if(!ah->Instance){ continue; } laListHandle* lp=((uint8_t*)ah->Instance)+ah->PropOffset;
        for(laActionProp* ap=lp->pFirst;ap;ap=ap->Item.pNext){ if(ap->Reset){ continue; }
            la_AnimationSetPropValue(ap,0);
        }
    }
    if(any) laNotifyUsers("la.animation");
}
void la_AnimationMarkRetargetedReset(laActionRetarget* ar){
    for(laAction* aa=ar->Actions->pFirst;aa;aa=aa->Item.pNext){
        for(laActionChannel* ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){
            if(ac->AP) ac->AP->Reset = 1;
        }
    }
}
laRetargetedAction* la_AnimationGetRetargetedAction(laActionRetarget* ar, laAction* aa){
    int i=0; for(laAction* a=ar->Actions->pFirst;a&&i<ar->ActionCount;a=a->Item.pNext){ if(a==aa) return &ar->Retargeted[i]; i++; }
    return 0;
}
int laAnimationSyncRetarget(laActionRetarget* ar, real PlayHead){
    int need_sync = (ar->PlaySync != PlayHead);
    real delta = PlayHead-ar->PlaySync; ar->PlaySync = PlayHead; int i=0;
    for(laAction* aa=ar->Actions->pFirst;aa;aa=aa->Item.pNext){
        if(ar->Retargeted[i].PlayStatus && need_sync){
            int fac_direction = ((ar->Retargeted[i].Direction<0)?-1:1);
            ar->Retargeted[i].PlayHead += delta / aa->Length * fac_direction;
        }
        if(aa->PlayMode==LA_ANIMATION_PLAY_MODE_REPEAT){ ar->Retargeted[i].Direction = 1;ar->Retargeted[i].PlayHead=fmod(ar->Retargeted[i].PlayHead,1.0f); }
        elif(aa->PlayMode==LA_ANIMATION_PLAY_MODE_HOLD){ ar->Retargeted[i].Direction = 1; TNS_CLAMP(ar->Retargeted[i].PlayHead,0.0f,1.0f); }
        elif(aa->PlayMode==LA_ANIMATION_PLAY_MODE_BOUNCE){
            if(ar->Retargeted[i].PlayHead<0){ ar->Retargeted[i].PlayHead=-ar->Retargeted[i].PlayHead; ar->Retargeted[i].Direction = 1; }
            elif(ar->Retargeted[i].PlayHead>1.0f){ ar->Retargeted[i].PlayHead=1.0f-(ar->Retargeted[i].PlayHead-1.0f); ar->Retargeted[i].Direction = -1; }
        }
        i++;
    }
    return 1;
}
int laAnimationEvaluateRetargetedActions(laActionRetarget* ar){
    int any=0; if(!ar || !ar->Retargeted){ return 0; }

    la_AnimationMarkRetargetedReset(ar); int i=0;
    for(laAction* aa=ar->Actions->pFirst;aa;aa=aa->Item.pNext){
        la_AnimationEvaluateActionChannels(aa,ar->Retargeted[i].PlayHead);
        i++;
    }
    
    i=0;
    for(laAction* aa=ar->Actions->pFirst;aa;aa=aa->Item.pNext){
        int j=0;
        for(laActionChannel* ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){
            if(ac->AP->Reset){ j++; continue; }
            la_AnimationSetPropValue(ac->AP,ar->Retargeted[i].Instances[j]);
            j++; any=1;
        }
        i++;
    }
    return any;
}

void la_AnimationPreFrame(){
    if(MAIN.Animation->PlayHead<0){
        MAIN.Animation->PlayHead=0; laAnimationSetPlayStatus(0);
        la_AnimationEvaluateActions(1);
        laNotifyUsers("la.animation.play_head");
    }
    if(MAIN.Animation->PlayStatus){
        la_AnimationEvaluateActions(0);
    }
}
void la_AnimationPostFrame(){
    laTimeRecorder cur={0};
    if(MAIN.Animation->PlayStatus){ laRecordTime(&cur);
        real td=laTimeElapsedSecondsf(&cur,&MAIN.Animation->TimeOrigin);
        laRecordTime(&MAIN.Animation->TimeOrigin);
        if(MAIN.Animation->PlayStatus==LA_ANIMATION_STATUS_PLAY_REV){ td*=-1; }
        MAIN.Animation->PlayHead+=td; laNotifyUsers("la.animation.play_head");
    }
}

void la_ActionDeselectFrames(laAction* aa){
    for(laActionChannel* ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){
        for(laActionKey* ak=ac->Keys.pFirst;ak;ak=ak->Item.pNext){
            ak->Selected=0;
        }
    }
}
void la_ActionSelectFrame(laAction* aa, int Channel, real At, real zoomx){
    int tc=0;laActionChannel* ac;
    for(ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){ if(tc==Channel) break; tc++; }
    if(!ac) return;
    real ClosestDist=FLT_MAX; laActionKey* ClosestKey=0;
    for(laActionKey* ak=ac->Keys.pFirst;ak;ak=ak->Item.pNext){
        real d=fabs(ak->At-At+0.5); if(d*zoomx<LA_RH*2 && d<ClosestDist){ ClosestDist=d; ClosestKey=ak; }
    }
    if(ClosestKey){ ClosestKey->Selected=1; }
}
int la_ActionSaveKeyOriginalAt(laAction* aa){
    int any=0;
    for(laActionChannel* ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){
        for(laActionKey* ak=ac->Keys.pFirst;ak;ak=ak->Item.pNext){ if(!ak->Selected) continue; any=1; ak->OriginaAt=ak->At; }
    }
    return any;
}
void la_ActionEnsureFrameOrder(laAction* aa){
    for(laActionChannel* ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){
        laListHandle lst={0};
        laActionKey* ak; while(ak=lstPopItem(&ac->Keys)){ int done=0;
            if(!lst.pFirst){ lstAppendItem(&lst,ak); continue; }
            laActionKey* ak2=0;
            for(ak2=lst.pFirst;ak2;ak2=ak2->Item.pNext){
                if(ak2->At>=ak->At){ lstInsertItemBefore(&lst,ak,ak2); done=1; break; }
            }
            if(!done) lstAppendItem(&lst,ak);
        }
        lstCopyHandle(&ac->Keys,&lst);
    }
}
void la_ActionRestoreKeyOriginalAt(laAction* aa) {
    for (laActionChannel* ac = aa->Channels.pFirst; ac; ac = ac->Item.pNext) {
        for (laActionKey* ak = ac->Keys.pFirst; ak; ak = ak->Item.pNext) { if (!ak->Selected) continue; ak->At = ak->OriginaAt; }
    }
    la_ActionEnsureFrameOrder(aa);
}
void la_ActionRemoveDuplicatedFrames(laAction* aa){
    for(laActionChannel* ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){
        laActionKey* ak2; for(laActionKey* ak=ac->Keys.pFirst;ak;ak=ak->Item.pNext){
            while((ak2=ak->Item.pNext)&&ak->At==ak2->At){ lstRemoveItem(&ac->Keys,ak2); memLeave(ak2); }
        }
    }
}
void la_ActionMoveSelectedFrames(laAction* aa, int Delta){
    for(laActionChannel* ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){
        for(laActionKey* ak=ac->Keys.pFirst;ak;ak=ak->Item.pNext){ if(!ak->Selected) continue;
            ak->At=ak->OriginaAt+Delta;
        }
    }
    la_ActionEnsureFrameOrder(aa);
}
int la_ActionDeleteSelectedFrames(laAction* aa){
    int any=0; laActionChannel* NextAc;
    for(laActionChannel* ac=aa->Channels.pFirst;ac;ac=NextAc){ NextAc=ac->Item.pNext;
        laActionKey* NextAk; for(laActionKey* ak=ac->Keys.pFirst;ak;ak=NextAk){ NextAk=ak->Item.pNext;if(!ak->Selected) continue;
            laAnimationRemoveFrame(ac,ak); any=1;
        }
        if(!ac->Keys.pFirst){ laAnimationRemoveChannel(aa,ac); any=1; }
    }
    return any;
}

int LAMOD_AnimationActionsCanvas(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance; laBoxedTheme *bt = (*ui->Type->Theme);
    laCanvasExtra *ex = a->CustomData; laAction* aa=ui->PP.EndInstance;
    if(!aa) LA_RUNNING_PASS;
    int W, H; W = ui->R - ui->L; H = ui->B - ui->U;
    int ShowSide=(ex->ShowLegend&&ex->LW<W-2*LA_RH);
    int ll=ui->L, lr=ll+(ShowSide?ex->LW:0), tl=lr, tr=ui->R;
    int FW=LA_RH/2;
    int Channels=aa?lstCountElements(&aa->Channels):0;
    int Modal=0; int mid=((W-lr)/2);
    
    if(e->type==LA_M_MOUSE_DOWN){
        ex->UiMode=1; ex->ClickedX=e->x; ex->ClickedY=e->y; Modal=1; ex->Dragging=10;
    }elif(e->type==LA_M_MOUSE_UP || e->type==LA_L_MOUSE_UP){
        ex->UiMode=0; Modal=1; ex->Dragging=0; ex->TargetIndexVali=0;
    }
    if(ex->UiMode==1){
        if(e->type&LA_MOUSE_EVENT){
            ex->PanY-=e->y-ex->ClickedY; ex->PanX-=e->x-ex->ClickedX;
            ex->ClickedX=e->x; ex->ClickedY=e->y; Modal=1;
        }
    }elif(ex->UiMode==2){ Modal=1;
        if(e->type&LA_MOUSE_EVENT){
            ex->LW=e->x-ui->L+LA_SEAM_W; ex->ClickedX=e->x; ex->ClickedY=e->y;
            if(ex->LW<LA_RH*3 && ex->LW>=LA_RH/2){ ex->LW=LA_RH*3; }
            if(ex->LW<LA_RH/2 && ex->Dragging!=12){ ex->ShowLegend=0; ex->LW=0; }
            if(ex->LW<LA_RH*3){ ex->LW=LA_RH*3;}
        }
    }elif(ex->UiMode==3){ Modal=1;
        if(e->type&LA_MOUSE_EVENT){ ex->ClickedX=e->x; ex->ClickedY=e->y;
            int SetFrame=(real)(e->x-tl+ex->PanX)/ex->ZoomX/FW;
            la_AnimationActionSetOwnFrame(aa,SetFrame,0);
        }
    }elif(ex->UiMode==4){ Modal=1;
        if(e->type==LA_R_MOUSE_DOWN || (e->type == LA_KEY_DOWN && e->key==LA_KEY_ESCAPE)){ ex->UiMode=0; ex->Dragging=0;
            la_ActionRestoreKeyOriginalAt(aa); laNotifyUsers("la.animation.current_action");
        }elif(e->type==LA_L_MOUSE_DOWN){ la_ActionRemoveDuplicatedFrames(aa);
            laNotifyUsers("la.animation.current_action"); ex->UiMode=0; ex->Dragging=0;
        }elif(e->type&LA_MOUSE_EVENT){ Modal=1;
            int ToFrame=(real)(e->x-tl+ex->PanX)/ex->ZoomX/FW,FromFrame=(real)(ex->ClickedX-tl+ex->PanX)/ex->ZoomX/FW;
            la_ActionMoveSelectedFrames(aa,ToFrame-FromFrame); laNotifyUsers("la.animation.current_action");
        }
    }

    int MaxDN=LA_RH*(Channels+1)-H+LA_M+LA_M; if(MaxDN<0) MaxDN=0;
    if(!Modal){
        if(e->x<=lr){
            if(e->type==LA_MOUSE_WHEEL_DOWN){ ex->PanY+=LA_RH*MAIN.ScrollingSpeed; Modal=1; }
            if(e->type==LA_MOUSE_WHEEL_UP){ ex->PanY-=LA_RH*MAIN.ScrollingSpeed; Modal=1; }
            if(e->x>=lr-LA_SEAM_W*2){ if(!ex->TargetIndexVali){ ex->TargetIndexVali=1; Modal=1; }
                if(e->type==LA_L_MOUSE_DOWN){
                    ex->UiMode=2; ex->ClickedX=e->x; ex->ClickedY=e->y; Modal=1; ex->Dragging=10; Modal=1;
                }
            }else{ if(ex->TargetIndexVali){ ex->TargetIndexVali=0; Modal=1; } }
        }elif(aa){
            if((!ex->ShowLegend)&&e->x<=ll+LA_SEAM_W*2){
                if(!ex->TargetIndexVali){ ex->TargetIndexVali=1; Modal=1; }
                if(e->type==LA_L_MOUSE_DOWN){ ex->LW=LA_RH*4; ex->Dragging=12; ex->ShowLegend=1; Modal=1; ex->UiMode=2; ex->TargetIndexVali=1; }
            }else{ if(ex->TargetIndexVali){ ex->TargetIndexVali=0; Modal=1; } }
            if(e->type==LA_MOUSE_WHEEL_DOWN){ ex->ZoomX*=0.9; ex->PanX+=mid; ex->PanX*=0.9; ex->PanX-=mid; Modal=1; }
            elif(e->type==LA_MOUSE_WHEEL_UP){ ex->ZoomX*=1.1; ex->PanX+=mid; ex->PanX*=1.1; ex->PanX-=mid; Modal=1; }
            elif(e->type==LA_L_MOUSE_DOWN){
                ex->UiMode=3; int SetFrame=(real)(e->x-tl+ex->PanX)/ex->ZoomX/FW;
                la_AnimationActionSetOwnFrame(aa,SetFrame,0); Modal=1;
            }elif(e->type==LA_R_MOUSE_DOWN){
                int row=(e->y-ui->U-LA_M-LA_RH)/LA_RH; real at=(real)(e->x-tl+ex->PanX)/ex->ZoomX/FW;
                if(!(e->SpecialKeyBit&LA_KEY_SHIFT)){ la_ActionDeselectFrames(aa); }
                la_ActionSelectFrame(aa,row,at,ex->ZoomX);
                laNotifyUsers("la.animation.current_action");
            }elif(e->type==LA_KEY_DOWN&&e->key=='g'){
                int any=la_ActionSaveKeyOriginalAt(aa);
                if(any){ ex->ClickedX=e->x; ex->ClickedY=e->y; ex->UiMode=4; Modal=1; ex->Dragging=13;
                    laNotifyUsers("la.animation.current_action");
                }
            }elif(e->type==LA_KEY_DOWN&&e->key=='x'){
                int any=la_ActionDeleteSelectedFrames(aa);
                if(any){
                    laNotifyUsers("la.animation.current_action");
                }
            }
        }
    }
    if(ex->PanY>MaxDN){ ex->PanY=MaxDN; } if(ex->PanY<0){ ex->PanY=0; }
    //if(ex->PanX<0){ ex->PanX=0; }

    if(ex->TargetIndexVali){ laSetWindowCursor(LA_LEFT_AND_RIGHT); }else{ laSetWindowCursor(LA_ARROW); }

    if(Modal){ laRedrawCurrentPanel(); return LA_RUNNING; }

    return LA_RUNNING_PASS;
}
void la_AnimationActionCanvasInit(laUiItem *ui){
    la_CanvasInit(ui);
    laCanvasExtra* ex=ui->Extra; ex->LW=LA_RH*7; ex->ShowLegend=1;
}
void la_AnimationActionDrawCanvas(laBoxedTheme *bt, laAction *aa, laUiItem* ui){
    laCanvasExtra* ex=ui->Extra; //laAction* aa=ui->PP.EndInstance;
    int W, H; W = ui->R - ui->L; H = ui->B - ui->U; if (W<=0 || H<=0) return;
    int ShowSide=(ex->ShowLegend&&ex->LW<W-2*LA_RH);
    int ll=ui->L, lr=ll+(ShowSide?ex->LW-1:0), tl=lr, tr=ui->R;
    int FW=LA_RH/2;

    //if (!e->OffScr || e->OffScr->pColor[0]->Height != ui->B - ui->U || e->Be.OffScr->pColor[0]->Width != ui->R - ui->L){
    //    if (e->OffScr) tnsDelete2DOffscreen(e->OffScr);
    //    e->OffScr = tnsCreate2DOffscreen(GL_RGBA, W, H, MAIN.PanelMultisample, 1, 0);
    //}

    real *bkg=laThemeColor(bt,LA_BT_NORMAL);
    real *txt=laThemeColor(bt,LA_BT_TEXT);
    real *act=laThemeColor(bt,LA_BT_ACTIVE);
    tnsUseNoTexture();
    tnsColor4d(LA_COLOR3(bkg),bkg[3]*0.3);
    tnsVertex2d(tl, ui->U); tnsVertex2d(tr, ui->U);
    tnsVertex2d(tr, ui->B); tnsVertex2d(tl, ui->B);
    tnsPackAs(GL_TRIANGLE_FAN);

    int row=1,curframe,cx; real fl,fr;

    tnsColor4dv(laThemeColor(bt,LA_BT_NORMAL));
    tnsVertex2d(ui->L, ui->U+LA_M+LA_RH); tnsVertex2d(ui->R, ui->U+LA_M+LA_RH);
    tnsVertex2d(ui->R, ui->U); tnsVertex2d(ui->L, ui->U);
    tnsPackAs(GL_TRIANGLE_FAN);

    tnsColor4dv(laThemeColor(bt,LA_BT_TEXT));
    tnsVertex2d(tl, LA_RH+ui->U); tnsVertex2d(tr, LA_RH+ui->U);
    tnsPackAs(GL_LINES);

    tnsFlush();
    int sx,sy,sw,sh,vl,vr,vu,vb;
    la_DoUiScissor(ui,&sx,&sy,&sw,&sh,&vl,&vr,&vu,&vb);

    if(aa){
        curframe=LA_ACTION_FRAME(aa,-1); fl=tl+curframe*FW*ex->ZoomX-ex->PanX, fr=tl+(curframe+1)*FW*ex->ZoomX-ex->PanX;
        tnsUseNoTexture();
        tnsColor4dv(laAccentColor(LA_BT_NORMAL));
        tnsVertex2d(fl, ui->U); tnsVertex2d(fr, ui->U);
        tnsVertex2d(fr, ui->B); tnsVertex2d(fl, ui->B);
        tnsPackAs(GL_TRIANGLE_FAN);
        tnsFlush(); glLineWidth(2);
        for(laActionChannel* ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){
            real ku=ui->U+LA_M+row*LA_RH-ex->PanY,kb=ku+LA_RH;
            for(laActionKey* ak=ac->Keys.pFirst;ak;ak=ak->Item.pNext){
                real kl=tl+ak->At*FW*ex->ZoomX-ex->PanX, kr=tl+(ak->At+1)*FW*ex->ZoomX-ex->PanX;
                if(ak->Selected) tnsColor4dv(laAccentColor(LA_BT_TEXT_ACTIVE));
                elif(curframe==ak->At) tnsColor4d(LA_COLOR3(txt),txt[3]*0.6);
                else tnsColor4dv(laThemeColor(bt,LA_BT_ACTIVE));
                tnsVertex2d(kl, ku); tnsVertex2d(kr, ku);
                tnsVertex2d(kr, kb); tnsVertex2d(kl, kb);
                tnsPackAs(GL_TRIANGLE_FAN);
                tnsColor4dv(laThemeColor(bt,LA_BT_TEXT_ACTIVE));
                tnsVertex2d(kl, ku); tnsVertex2d(kr, ku);
                tnsVertex2d(kr, kb); tnsVertex2d(kl, kb);
                tnsPackAs(GL_LINE_LOOP);
            }
            row++;
        }
        tnsFlush(); glLineWidth(1);

        real end=tl+aa->FrameCount*FW*ex->ZoomX-ex->PanX;
        tnsColor4d(0,0,0,0.3);
        tnsVertex2d(end, ui->U+LA_RH); tnsVertex2d(end+1e6, ui->U+LA_RH);
        tnsVertex2d(end+1e6, ui->B); tnsVertex2d(end, ui->B);
        tnsPackAs(GL_TRIANGLE_FAN); 
        tnsVertex2d(tl-ex->PanX, ui->U+LA_RH); tnsVertex2d(tl-ex->PanX-1e6, ui->U+LA_RH);
        tnsVertex2d(tl-ex->PanX-1e6, ui->B); tnsVertex2d(tl-ex->PanX, ui->B);
        tnsPackAs(GL_TRIANGLE_FAN); 

        tnsColor4dv(laAccentColor(LA_BT_TEXT_ACTIVE));
        int FrameFix=aa->PlayHead<0?1:0;
        cx=tl+(aa->PlayHead*aa->FrameCount+FrameFix)*FW*ex->ZoomX-ex->PanX; tnsVertex2d(cx, ui->U); tnsVertex2d(cx, ui->B);
        tnsPackAs(GL_LINES); glLineWidth(3); tnsFlush(); glLineWidth(1);

        char buf[32]; sprintf(buf,"%d",curframe); real tlen=tnsStringGetDimension(buf,0,0,0,0,0)+LA_M+LA_M;
        tnsColor4dv(laAccentColor(LA_BT_TEXT_ACTIVE));
        tnsVertex2d(cx, ui->U+LA_RH); tnsVertex2d(cx+tlen, ui->U+LA_RH);
        tnsVertex2d(cx+tlen, ui->U); tnsVertex2d(cx, ui->U);
        tnsPackAs(GL_TRIANGLE_FAN);
        tnsDrawStringAuto(buf,laThemeColor(bt,LA_BT_TEXT_ACTIVE),cx+LA_M,cx+LA_RH*2,ui->U+LA_M,0);
        tnsPackAs(GL_TRIANGLE_FAN); tnsUseNoTexture();
    }

    if(ShowSide){
        tnsColor4dv(laThemeColor(bt,LA_BT_NORMAL));
        tnsVertex2d(ll, ui->U); tnsVertex2d(lr, ui->U);
        tnsVertex2d(lr, ui->B); tnsVertex2d(ll, ui->B);
        tnsPackAs(GL_TRIANGLE_FAN);
        if(!aa){
            tnsDrawStringAuto("No action selected",laThemeColor(bt,LA_BT_TEXT),ll+LA_M,lr-LA_M,ui->U+LA_M,0);
        }else{ int row=1;
            tnsDrawStringAuto(SSTR(aa->Name),laThemeColor(bt,LA_BT_TEXT),ll+LA_M,lr-LA_M,ui->U+LA_M,0);
            for(laActionChannel* ac=aa->Channels.pFirst;ac;ac=ac->Item.pNext){ if(!ac->AP){ continue; }
                tnsDrawStringAuto(SSTR(ac->AP->CachedName),laThemeColor(bt,LA_BT_TEXT),ll+LA_M,lr-LA_M,ui->U+LA_M+row*LA_RH-ex->PanY,0);
                row++;
            }
        }
    }
    if(ex->TargetIndexVali==1){
        int LR=TNS_MAX2(lr,ui->L+LA_SEAM_W*2);
        tnsColor4dv(act);tnsUseNoTexture();
        tnsVertex2d(LR, ui->B); tnsVertex2d(LR-LA_SEAM_W*2, ui->B);
        tnsVertex2d(LR-LA_SEAM_W*2, ui->U); tnsVertex2d(LR, ui->U);
        tnsPackAs(GL_TRIANGLE_FAN);
    }

    tnsFlush();
    tnsViewportWithScissor(sx,sy,sw,sh);
    tnsOrtho(vl,vr,vb,vu,-100,100);

    tnsUseNoTexture();
    tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
    tnsVertex2d(ui->L, ui->U); tnsVertex2d(ui->R, ui->U);
    tnsVertex2d(ui->R, ui->B); tnsVertex2d(ui->L, ui->B);
    tnsPackAs(GL_LINE_LOOP);
}
void la_AnimationActionDrawOverlay(laUiItem *ui, int h){
    laCanvasExtra *e = ui->Extra; laBoxedTheme *bt = (*ui->Type->Theme);

    tnsUseImmShader();

    if(MAIN.CurrentWindow->MaximizedUi!=ui){
        tnsUseNoTexture();
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        tnsVertex2d(ui->L, ui->U); tnsVertex2d(ui->R, ui->U);
        tnsVertex2d(ui->R, ui->B); tnsVertex2d(ui->L, ui->B);
        tnsPackAs(GL_LINE_LOOP);
    }

    if (e->DrawCursor){
        tnsColor4dv(laThemeColor(bt,LA_BT_TEXT));
        int drawx=(e->DrawCursor==LA_CANVAS_CURSOR_CROSS)||(e->DrawCursor==LA_CANVAS_CURSOR_X);
        int drawy=(e->DrawCursor==LA_CANVAS_CURSOR_CROSS)||(e->DrawCursor==LA_CANVAS_CURSOR_Y);
        if(drawx) tnsVertex2d(e->OnX, ui->U); tnsVertex2d(e->OnX, ui->B);
        if(drawy) tnsVertex2d(ui->L, e->OnY); tnsVertex2d(ui->R, e->OnY);
        tnsPackAs(GL_LINES);
        tnsFlush();
    }

    return;
}

void laui_AnimationActionMenu(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){

}

void la_RegisterAnimationResources(){
    laPropContainer *pc; laProp *p; laOperatorType *at; laEnumProp *ep;

    at=laCreateOperatorType("LA_animation_new_action", "New Action", "Add a new action",0,0,0,OPINV_AnimationNewAction,OPMOD_AnimationNewAction,U'🞦',0);
    pc = laDefineOperatorProps(at, 1);
    p = laAddSubGroup(pc, "holder", "Holder", "Action holder to add the new action into", "la_animation_action_holder",
        0, 0, laui_IdentifierOnly, -1, laget_AnimationFirstActionHolder, 0, laget_ListNext, 0, 0, laset_AnimationNewActionSetHolder,0,0);
    laSubGroupExtraFunctions(p,0,0,0,0,laget_AnimationActionHolderCategory);
    at->UiDefine=laui_AnimationNewAction;

    at=laCreateOperatorType("LA_animation_remove_action", "Remove Action", "Remove this action",OPCHK_AnimationRemoveAction,0,0,OPINV_AnimationRemoveAction,OPMOD_AnimationRemoveAction,U'🞫',0);

    at=laCreateOperatorType("LA_animation_select_action", "Select Action", "Select an action",0,0,0,OPINV_AnimationSelectAction,OPMOD_FinishOnData,U'⯆',0);
    at->UiDefine=laui_AnimationSelectAction;

    laCreateOperatorType("LA_animation_set_play_status", "Set Play", "Set global animation player status",0,0,0,OPINV_AnimationPlayAction,0,0,0);
    laCreateOperatorType("LA_animation_reset_time", "Reset Time", "Reset Time",0,0,0,OPINV_AnimationResetTime,0,U'🡄',0);

    laCanvasTemplate* ct=laRegisterCanvasTemplate("la_AnimationActionDrawCanvas", "la_animation_action", LAMOD_AnimationActionsCanvas, la_AnimationActionDrawCanvas, la_AnimationActionDrawOverlay, la_AnimationActionCanvasInit, la_CanvasDestroy);
    pc = laCanvasHasExtraProps(ct,sizeof(laCanvasExtra),2); laAddIntProperty(pc, "height_coeff", "Ui Height", "Ui Height Coefficiency Entry", 0, 0, "Rows", 100, -100, 1, 0, 0, offsetof(laCanvasExtra, HeightCoeff), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    
}

