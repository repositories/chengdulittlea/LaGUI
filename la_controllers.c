/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "la_5.h"

#include <math.h>

#ifdef LA_LINUX
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/inotify.h>
#include <linux/input.h>
#include <linux/joystick.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <X11/extensions/XInput2.h>
#endif

extern LA MAIN;

#define LA_JS_EVENT_BUTTON 0x01 // button pressed/released
#define LA_JS_EVENT_AXIS   0x02 // joystick moved
#define LA_JS_EVENT_INIT   0x80 // initial state of device

#define LA_JS_TYPE_X56_THROTTLE 1
#define LA_JS_TYPE_X56_STICK 2

char LA_JS_BTN_NAMES[LA_JS_MAX_BUTTONS][12];
char LA_JS_BTN_MAP_NAMES[LA_JS_MAX_BUTTONS][12];

char LA_JS_AXIS_NAMES[LA_JS_MAX_AXES][12];
char LA_JS_AXIS_MAP_NAMES[LA_JS_MAX_AXES][12];
char LA_JS_AXIS_CMAX_NAMES[LA_JS_MAX_AXES][12];
char LA_JS_AXIS_CMIN_NAMES[LA_JS_MAX_AXES][12];
char LA_JS_AXIS_LMAX_NAMES[LA_JS_MAX_AXES][12];
char LA_JS_AXIS_LMIN_NAMES[LA_JS_MAX_AXES][12];

int la_IdentifyControllerInternalType(char* name){
    if(strstr(name, "X-56") && strstr(name, "Throttle")){ return LA_JS_TYPE_X56_THROTTLE; }
    if(strstr(name, "X-56") && strstr(name, "Stick")){ return LA_JS_TYPE_X56_STICK; }
    return 0;
}

void la_EnsureControllerNames(laController* c){
    laPropContainer*pc=la_EnsureSubTarget(LA_PROP_CONTROLLER,c);
    if(!pc){ return; }
    for(laProp* p=pc->Props.pFirst;p;p=p->Item.pNext){
        if(p->PropertyType & LA_PROP_INT && p->Offset){ int off=p->Offset;
            if(off >= offsetof(laController,AxisValues[0]) && off <= offsetof(laController,AxisValues[LA_JS_MAX_AXES-1])){
                int index = (off - offsetof(laController,AxisValues[0]))/sizeof(u16bit);
                if(p->Len<2){ strSafeSet(&c->AxisNames[index], p->Name); }
                else for(int i=0;i<p->Len;i++){ strSafePrint(&c->AxisNames[index+i],"%s.%d", p->Name,i+1); }
            }
        }elif(p->PropertyType & LA_PROP_ENUM && p->Offset){ int off=p->Offset;
            if(off >= offsetof(laController,ButtonValues[0]) && off <= offsetof(laController,ButtonValues[LA_JS_MAX_BUTTONS-1])){
                int index = (off - offsetof(laController,ButtonValues[0]));
                if(p->Len<2){ strSafeSet(&c->ButtonNames[index], p->Name); }
                else for(int i=0;i<p->Len;i++){ strSafePrint(&c->ButtonNames[index+i],"%s.%d", p->Name,i+1); }
            }
        }
    }
}

laController* la_NewController(char* name, char* path, int device, int NumAxes, int NumButtons){
    laController* c=memAcquire(sizeof(laController));
    logPrint("Found controller %s\n    at %s\n    with %d axes, %d buttons\n", name, path, NumAxes, NumButtons);
    strSafeSet(&c->Name, name); strSafeSet(&c->Path, path); c->fd=device;
    c->NumAxes = NumAxes; c->NumButtons=NumButtons;
    c->InternalType = la_IdentifyControllerInternalType(name);
    lstPushItem(&MAIN.Controllers,c);
    c->UserAssignedID=MAIN.NextControllerID; MAIN.NextControllerID++;
    
    la_EnsureControllerNames(c);

    return c;
}
void la_DestroyController(laController* c){
    strSafeDestroy(&c->Name); strSafeDestroy(&c->Path); if(c->fd){ close(c->fd); }
    lstRemoveItem(&MAIN.Controllers,c); laNotifyInstanceUsers(c);  memFree(c); laNotifyUsers("la.controllers");
}

laController* la_FindControllerWithID(int id){
    for(laController* c=MAIN.Controllers.pFirst;c;c=c->Item.pNext){ if(c->UserAssignedID==id) return c; } return 0;
}

void la_ReadControllerAxisLimit(laController* c, int i, int Min, int Max){
    c->AxisMins[i] = Min;
    c->AxisMaxes[i] = Max;
    c->AxisLimitMins[i] = -32768*0.95;
    c->AxisLimitMaxes[i] = 32767*0.95;
}

int la_ControllerButtonToMap(int btn){
#ifdef LA_LINUX
    if(btn<BTN_MISC) return -1;
    if(btn<=BTN_GEAR_UP) return btn-BTN_MISC;
    if(btn<BTN_DPAD_UP) return -1;
    if(btn<=BTN_DPAD_RIGHT) return BTN_GEAR_UP-BTN_MISC+1+btn-BTN_DPAD_UP;
    if(btn<BTN_TRIGGER_HAPPY) return -1;
    if(btn<=BTN_TRIGGER_HAPPY40) return BTN_GEAR_UP-BTN_MISC+1+BTN_DPAD_RIGHT-BTN_DPAD_UP+1+btn-BTN_TRIGGER_HAPPY;
    return -1;
#endif
#ifdef _WIN32
    return -1;
#endif
}
int la_ControllerButtonToIndex(laController* c,int btn){
    int map=la_ControllerButtonToMap(btn); if(map<0) return -1; return c->ButtonsMap[map];
}
int la_ControllerAxisToMap(int abs){
#ifdef LA_LINUX
    if(abs<ABS_X) return -1; if(abs>ABS_MAX) return -1;
    return abs;
#endif
#ifdef _WIN32
    return abs;
#endif
}
int la_ControllerAxisToIndex(laController* c,int abs){
    int map=la_ControllerAxisToMap(abs); if(map<0) return -1; return c->AxisMap[map];
}
char* laControllerIDGetAxisName(int id, int axis){
    laController* c=la_FindControllerWithID(id); if(!c){ return "?"; }
    return SSTR(c->AxisNames[axis]);
}
char* laControllerIDGetButtonName(int id, int button){
    laController* c=la_FindControllerWithID(id); if(!c){ return "?"; }
    return SSTR(c->ButtonNames[button]);
}
int laControllerIDGetAxis(int id, char* name){
    laController* c=la_FindControllerWithID(id); if(!c){ return -1; }
    for(int i=0;i<LA_JS_MAX_AXES;i++){ if(strSame(SSTR(c->AxisNames[i]),name)) return i; } return -1;
}
int laControllerIDGetButton(int id, char* name){
    laController* c=la_FindControllerWithID(id); if(!c){ return -1; }
    for(int i=0;i<LA_JS_MAX_AXES;i++){ if(strSame(SSTR(c->ButtonNames[i]),name)) return i; } return -1;
}

void la_InitControllers(){
#ifdef LA_LINUX
    char path[32]="/dev/input/js";
    char name[128]={0};
    int numpos=strlen(path);

    char fileName[32];
	for (int i=0; i<32; ++i) {
		sprintf(fileName, "/dev/input/event%d", i);
		int file = open(fileName, O_RDWR | O_NONBLOCK);
		if (file != -1){
			ioctl(file, EVIOCGNAME(128), name);

            barray_t *abs_barray = barray_init(ABS_CNT);
            ioctl(file, EVIOCGBIT(EV_ABS, ABS_CNT), barray_data(abs_barray));
            size_t abs_count = barray_count_set(abs_barray);

            barray_t *key_barray = barray_init(KEY_CNT);
            ioctl(file, EVIOCGBIT(EV_KEY, KEY_CNT), barray_data(key_barray));
            size_t key_count = barray_count_set(key_barray);

            if(!abs_count && !key_count){ close(file); continue; }

            laController* c=la_NewController(name, fileName, file, abs_count, key_count);

            int nextid=0;
            for (unsigned int j=0; j<KEY_CNT; j++){
                if(barray_is_set(key_barray,j)){
                    int mapid=la_ControllerButtonToMap(j); if(mapid<0){ continue; }
                    c->ButtonsMap[mapid]=nextid; nextid++;
                }
			}
            nextid=0;
			for (unsigned int j=0; j<ABS_CNT; j++){ struct input_absinfo axisInfo;
				if (barray_is_set(abs_barray,j) && (ioctl(file, EVIOCGABS(j), &axisInfo) != -1)){
                    int mapid=la_ControllerAxisToMap(j); if(mapid<0){ continue; }
                    la_ReadControllerAxisLimit(c,nextid,axisInfo.minimum,axisInfo.maximum);
                    c->AxisMap[mapid]=nextid; nextid++;
				}
			}

            barray_free(abs_barray);
            barray_free(key_barray);
		}
	}
#endif
}

void la_UpdateControllerStatus(){
#ifdef LA_LINUX
    struct input_event event; int HasEvent=0;
    for(laController* c=MAIN.Controllers.pFirst;c;c=c->Item.pNext){ if(c->Error) continue;
        if(!c->Error && !c->fd){ c->fd=open(SSTR(c->Path), O_RDWR | O_NONBLOCK); if(c->fd<0){ c->Error=1; } continue; }
        int bytes; while((bytes=read(c->fd, &event, sizeof(struct input_event)))>0){
            if(event.type == EV_KEY){
                int idx = la_ControllerButtonToIndex(c,event.code); if(idx<0) continue;
                c->ButtonValues[idx]=event.value; HasEvent=1;
                MAIN.LastControllerKey=idx; MAIN.LastControllerKeyDevice=c->UserAssignedID; MAIN.ControllerHasNewKey = 1; laRetriggerOperators();
            }
            else if(event.type == EV_ABS){
                int idx = la_ControllerAxisToIndex(c,event.code); if(idx<0) continue; HasEvent=1;
                c->AxisValues[idx]=rint(tnsLinearItp(-32768.0f,32767.0f,(((real)event.value-c->AxisMins[idx])/(c->AxisMaxes[idx]-c->AxisMins[idx]))));
                if(abs(c->AxisValues[idx]-c->SaveAxisValues[idx])>10000){ c->SaveAxisValues[idx]=c->AxisValues[idx];
                    MAIN.LastControllerAxis=idx; MAIN.LastControllerAxisDevice=c->UserAssignedID; MAIN.ControllerHasNewAxis = 1; laRetriggerOperators();
                }
            }
        }
        if(bytes<=0){ struct stat buffer; if(stat(c->Path->Ptr,&buffer)<0){ c->Error=1; HasEvent=1; } }
    }
    if(HasEvent){ laNotifyUsers("la.controllers"); laMappingRequestEval(); }
#endif
}

void la_CopyControllerConfig(laController* to, laController* from){
    strSafeSet(&to->Path,SSTR(from->Path));
    to->UserAssignedID = from->UserAssignedID;
    memcpy(to->AxisCenterMaxes,from->AxisCenterMaxes,sizeof(int16_t)*LA_JS_MAX_AXES);
    memcpy(to->AxisCenterMins,from->AxisCenterMins,sizeof(int16_t)*LA_JS_MAX_AXES);
    memcpy(to->AxisLimitMaxes,from->AxisLimitMaxes,sizeof(int16_t)*LA_JS_MAX_AXES);
    memcpy(to->AxisLimitMins,from->AxisLimitMins,sizeof(int16_t)*LA_JS_MAX_AXES);
    memcpy(to->ButtonsMap,from->ButtonsMap,sizeof(u8bit)*LA_JS_MAX_BUTTONS);
    memcpy(to->AxisMap,from->AxisMap,sizeof(u8bit)*LA_JS_MAX_AXES);
}

void la_RemoveDuplicatedControllers(){
    laController* NextC;
    for(laController* fc=MAIN.Controllers.pFirst;fc;fc=fc->Item.pNext){
        if(fc->fd>=0){ close(fc->fd); fc->fd=0; }
    }
    for(laController* c=MAIN.Controllers.pFirst;c;c=c->Item.pNext){
        for(laController* fc=c->Item.pNext;fc;fc=NextC){
            NextC=fc->Item.pNext;
            if(strSame(SSTR(fc->Name),SSTR(c->Name))){
                la_CopyControllerConfig(c,fc); la_EnsureControllerNames(c);
                la_DestroyController(fc);
            }
        }
    }
}

void la_RefreshControllers(){
    la_InitControllers();
    la_RemoveDuplicatedControllers();
}
int OPINV_RefreshControllers(laOperator* a, laEvent* e){
    la_RefreshControllers();
#ifdef LA_LINUX
    la_ScanWacomDevices(MAIN.dpy,XIAllDevices);
#endif
#ifdef _WIN32
    MAIN.WinTabOpened = 0;
    if(MAIN.CurrentWindow){ la_OpenWacomWinTab(MAIN.CurrentWindow->win); }
#endif
    laNotifyUsers("la.controllers");
    return LA_FINISHED;
}
int OPINV_RemoveController(laOperator* a, laEvent* e){
    if(!a->This || !a->This->EndInstance) return LA_FINISHED;
    laEnableYesNoPanel(a, 0, "Confirm?", "Will remove this controller config.", e->x-200, e->y, 200, e);
    return LA_RUNNING;
}
int OPMOD_RemoveController(laOperator* a, laEvent* e){
    if(!a->This || !a->This->EndInstance) return LA_FINISHED; laController* c=a->This->EndInstance;
    if(a->ConfirmData){
        if(a->ConfirmData->Mode == LA_CONFIRM_OK){
            la_DestroyController(c); laNotifyUsers("la.controllers");
        }
        return LA_FINISHED;
    }
    return LA_FINISHED;
}

void lapost_Controller(laController* c){
    c->InternalType = la_IdentifyControllerInternalType(SSTR(c->Name));
#ifdef LA_LINUX
    c->fd=open(SSTR(c->Path),O_RDWR | O_NONBLOCK); if(c->fd<0){ c->Error=1; }
#endif
}

void* lagetraw_ControllerButtonMap(laController* c, int* r_size, int* ret_is_copy){
    int use_size=/*sizeof(int16_t)*LA_JS_MAX_AXES*4 +*/ sizeof(u8bit)*LA_JS_MAX_BUTTONS + sizeof(u8bit)*LA_JS_MAX_AXES;
    char* data = malloc(use_size); char* p = data; if (!data) return 0;
    //memcpy(p,c->AxisCenterMaxes,sizeof(int16_t)*LA_JS_MAX_AXES); p+= sizeof(int16_t)*LA_JS_MAX_AXES;
    //memcpy(p,c->AxisCenterMins,sizeof(int16_t)*LA_JS_MAX_AXES); p+= sizeof(int16_t)*LA_JS_MAX_AXES;
    //memcpy(p,c->AxisLimitMaxes,sizeof(int16_t)*LA_JS_MAX_AXES); p+= sizeof(int16_t)*LA_JS_MAX_AXES;
    //memcpy(p,c->AxisLimitMins,sizeof(int16_t)*LA_JS_MAX_AXES); p+= sizeof(int16_t)*LA_JS_MAX_AXES;
    memcpy(p,c->ButtonsMap,sizeof(u8bit)*LA_JS_MAX_BUTTONS); p+= sizeof(u8bit)*LA_JS_MAX_BUTTONS;
    memcpy(p,c->AxisMap,sizeof(u8bit)*LA_JS_MAX_AXES); p+= sizeof(u8bit)*LA_JS_MAX_AXES;
    return data;
}
void lasetraw_ControllerButtonMap(laController* c, void* data, int DataSize){
    int use_size=/*sizeof(int16_t)*LA_JS_MAX_AXES*4 +*/ sizeof(u8bit)*LA_JS_MAX_BUTTONS + sizeof(u8bit)*LA_JS_MAX_AXES;
    if(DataSize!=use_size){ c->Error=1; return; /* need reinit */ }
    char* p=data;
    //memcpy(c->AxisCenterMaxes,p,sizeof(int16_t)*LA_JS_MAX_AXES); p+= sizeof(int16_t)*LA_JS_MAX_AXES;
    //memcpy(c->AxisCenterMins,p,sizeof(int16_t)*LA_JS_MAX_AXES); p+= sizeof(int16_t)*LA_JS_MAX_AXES;
    //memcpy(c->AxisLimitMaxes,p,sizeof(int16_t)*LA_JS_MAX_AXES); p+= sizeof(int16_t)*LA_JS_MAX_AXES;
    //memcpy(c->AxisLimitMins,p,sizeof(int16_t)*LA_JS_MAX_AXES); p+= sizeof(int16_t)*LA_JS_MAX_AXES;
    memcpy(c->ButtonsMap,p,sizeof(u8bit)*LA_JS_MAX_BUTTONS); p+= sizeof(u8bit)*LA_JS_MAX_BUTTONS;
    memcpy(c->AxisMap,p,sizeof(u8bit)*LA_JS_MAX_AXES); p+= sizeof(u8bit)*LA_JS_MAX_AXES;
}


void la_AddButtonProp(laPropContainer* pc, char* id, char* id_map, char* name, char* desc, int i, int array_len, char* array_prefix){
    laProp* p=laAddEnumProperty(pc, id, name, desc, LA_WIDGET_ENUM_HIGHLIGHT,
        array_prefix,0,0,0,offsetof(laController, ButtonValues[i]),0,0,array_len,0,0,0,0,0,0,LA_READ_ONLY|LA_UDF_IGNORE);
    laAddEnumItemAs(p,"IDLE", "Idle", "Button is not pressed", 0, 0);
    laAddEnumItemAs(p,"ACTIVE", "Active", "Button is pressed", 1, 0);
    p->ElementBytes=1;
    laAddIntProperty(pc,id_map,name,desc,0,
        array_prefix,0,0,0,1,0,0,offsetof(laController, ButtonsMap[i]),0,0,array_len,0,0,0,0,0,0,0,0)->ElementBytes = 1;
}
void la_AddAxisProp(laPropContainer* pc, char* id, char* name, char* id_min, char* id_max, char* id_cmin, char* id_cmax, char* id_map, char* desc, int i, int array_len, char* array_prefix){
    laAddIntProperty(pc,id,name,desc,array_len>1?LA_WIDGET_VALUE_METER_2D:LA_WIDGET_METER_TYPE2,
        array_prefix,0,32767,-32768,1,0,0,offsetof(laController, AxisValues[i]),0,0,array_len,0,0,0,0,0,0,0,LA_READ_ONLY|LA_UDF_IGNORE)->ElementBytes = 2;
    laAddIntProperty(pc,id_map,name,desc,0,
        array_prefix,0,0,0,1,0,0,offsetof(laController, AxisMap[i]),0,0,array_len,0,0,0,0,0,0,0,0)->ElementBytes = 1;
    if(id_min) laAddIntProperty(pc,id_min,id_min,desc,0,
        array_prefix,0,-25000,-32768,1,0,0,offsetof(laController, AxisLimitMins[i]),0,0,array_len,0,0,0,0,0,0,0,0)->ElementBytes = 2;
    if(id_max) laAddIntProperty(pc,id_max,id_max,desc,0,
        array_prefix,0,32767,25000,1,0,0,offsetof(laController, AxisLimitMaxes[i]),0,0,array_len,0,0,0,0,0,0,0,0)->ElementBytes = 2;
    if(id_cmin) laAddIntProperty(pc,id_cmax,id_cmax,desc,0,
        array_prefix,0,-25000,-32768,1,0,0,offsetof(laController, AxisCenterMins[i]),0,0,array_len,0,0,0,0,0,0,0,0)->ElementBytes = 2;
    if(id_cmax) laAddIntProperty(pc,id_cmin,id_cmin,desc,0,
        array_prefix,0,32767,25000,1,0,0,offsetof(laController, AxisCenterMaxes[i]),0,0,array_len,0,0,0,0,0,0,0,0)->ElementBytes = 2;
}

#define ADD_BUTTON_PROP(pc,id,name,desc,i,array_len,array_prefix) \
    la_AddButtonProp(pc,id,id "_map",name,desc,i,array_len,array_prefix)

#define ADD_AXIS_PROP(pc,id,name,desc,i,array_len,array_prefix) \
    la_AddAxisProp(pc,id,name,id "_min",id "_max",id "_cmin",id "_cmax", id "_map",desc,i,array_len,array_prefix)

void la_AddGenericButtonProps(laPropContainer* pc){
    for(int i=0;i<LA_JS_MAX_BUTTONS;i++){ char* name=LA_JS_BTN_NAMES[i]; la_AddButtonProp(pc,name,LA_JS_BTN_MAP_NAMES[i],name,name,i,0,0); }
}
void la_AddGenericAxisProps(laPropContainer* pc){
    for(int i=0;i<LA_JS_MAX_AXES;i++){ char* name=LA_JS_AXIS_NAMES[i]; la_AddAxisProp(pc,name, name, LA_JS_AXIS_LMIN_NAMES[i], LA_JS_AXIS_LMAX_NAMES[i],
        LA_JS_AXIS_CMIN_NAMES[i],LA_JS_AXIS_CMAX_NAMES[i], LA_JS_AXIS_MAP_NAMES[i], name,i,0,0); }
}

void laui_GenericJoystick(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context);
void laui_X56Throttle(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context);
void laui_X56Stick(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context);

laPropContainer* LA_PC_JS_GENERIC;
laPropContainer* LA_PC_JS_X56_THROTTLE;
laPropContainer* LA_PC_JS_X56_STICK;

laPropContainer* laget_ControllerType(laController* c){
    switch(c->InternalType){
    default: case 0: return LA_PC_JS_GENERIC;
    case LA_JS_TYPE_X56_THROTTLE: return LA_PC_JS_X56_THROTTLE;
    case LA_JS_TYPE_X56_STICK:    return LA_PC_JS_X56_STICK;
    }
}

void la_RegisterControllerProps(){
    for(int i=0;i<LA_JS_MAX_AXES;i++){ sprintf(LA_JS_AXIS_NAMES[i],"a%d",i);
        sprintf(LA_JS_AXIS_MAP_NAMES[i],"a%d_map",i);
        sprintf(LA_JS_AXIS_CMAX_NAMES[i],"a%d_cmax",i);
        sprintf(LA_JS_AXIS_CMIN_NAMES[i],"a%d_cmin",i);
        sprintf(LA_JS_AXIS_LMAX_NAMES[i],"a%d_max",i);
        sprintf(LA_JS_AXIS_LMIN_NAMES[i],"a%d_min",i);
    }
    for(int i=0;i<LA_JS_MAX_BUTTONS;i++){ sprintf(LA_JS_BTN_NAMES[i],"b%d",i); sprintf(LA_JS_BTN_MAP_NAMES[i],"b%d_map",i); }

    laCreateOperatorType("LA_refresh_controllers", "Refresh Controllers", "Look for connected controllers",0,0,0,OPINV_RefreshControllers,0,U'🗘',0);
    laCreateOperatorType("LA_remove_controller", "Remove Controller", "Remove controller config",0,0,0,OPINV_RemoveController,OPMOD_RemoveController,U'🞫',0);

    laPropContainer* pc; laProp* p;

    pc=laAddPropertyContainer("la_controller", "Controller", "A joystick/gamepad controller", U'🕹', laui_GenericJoystick, sizeof(laController), lapost_Controller,0,1);
    LA_PC_JS_GENERIC = pc;
    laAddStringProperty(pc,"name","Name","Name of the controller", LA_WIDGET_STRING_PLAIN,0,0,0,1,offsetof(laController,Name),0,0,0,0,LA_READ_ONLY|LA_AS_IDENTIFIER);
    laAddStringProperty(pc,"path","Path","Device path to the controller", LA_WIDGET_STRING_PLAIN,0,0,0,1,offsetof(laController,Path),0,0,0,0,LA_READ_ONLY);
    laAddIntProperty(pc,"user_assigned_id", "ID", "User assigned ID", 0,0,0,0,0,0,0,0,offsetof(laController, UserAssignedID),0,0,0,0,0,0,0,0,0,0,0);
    laAddIntProperty(pc,"error", "Error", "Device error", 0,0,0,0,0,0,0,0,offsetof(laController, Error),0,0,0,0,0,0,0,0,0,0,0);
    laAddOperatorProperty(pc,"remove","Remove","Remove this controller config","LA_remove_controller",0,0);
    la_AddGenericButtonProps(pc);
    la_AddGenericAxisProps(pc);

    pc=laAddPropertyContainer("la_controller_x56_throttle", "X56 Throttle", "X56 Throttle", 0,laui_X56Throttle,sizeof(laController),0,0,1);
    LA_PC_JS_X56_THROTTLE = pc;
    laAddStringProperty(pc,"name","Name","Name of the controller", LA_WIDGET_STRING_PLAIN,0,0,0,1,offsetof(laController,Name),0,0,0,0,LA_READ_ONLY|LA_AS_IDENTIFIER);
    laAddSubGroup(pc,"base","Base","Generic controller status", "la_controller",0,0,0,0,0,0,0,0,0,0,0,LA_UDF_LOCAL);
    ADD_AXIS_PROP(pc,"thr1","THR1","Throttle 1",0,0,0);
    ADD_AXIS_PROP(pc,"thr2","THR2","Throttle 1",1,0,0);
    ADD_AXIS_PROP(pc,"wf","WF","Wheel F (big wheel)",2,0,0);
    ADD_AXIS_PROP(pc,"ball","Ball","Thumb ball",3,2,"Left/Right,Up/Down");
    ADD_AXIS_PROP(pc,"wg","WG","Wheel G (smaller wheel)",5,0,0);
    ADD_AXIS_PROP(pc,"r4","RTY4","Rotary 4",6,0,0);
    ADD_AXIS_PROP(pc,"r3","RTY3","Rotary 3",7,0,0);
    ADD_BUTTON_PROP(pc,"be","E","Button E (thumb flat switch)", 0,0,0);
    ADD_BUTTON_PROP(pc,"bf","F","Button F (big push down switch)", 1,0,0);
    ADD_BUTTON_PROP(pc,"bg","G","Button G (smaller up-side-down switch)", 2,0,0);
    ADD_BUTTON_PROP(pc,"bi","I","Button I (left reverser)", 3,0,0);
    ADD_BUTTON_PROP(pc,"bh","H","Button H (right reverser)", 4,0,0);
    ADD_BUTTON_PROP(pc,"sw1","SW1","Switch 1", 5,0,0);
    ADD_BUTTON_PROP(pc,"sw2","SW2","Switch 2", 6,0,0);
    ADD_BUTTON_PROP(pc,"sw3","SW3","Switch 3", 7,0,0);
    ADD_BUTTON_PROP(pc,"sw4","SW4","Switch 4", 8,0,0);
    ADD_BUTTON_PROP(pc,"sw5","SW5","Switch 5", 9,0,0);
    ADD_BUTTON_PROP(pc,"sw6","SW6","Switch 6", 10,0,0);
    ADD_BUTTON_PROP(pc,"t1_up","T1+","Toggle 1+", 11,0,0);
    ADD_BUTTON_PROP(pc,"t1_dn","T1-","Toggle 1-", 12,0,0);
    ADD_BUTTON_PROP(pc,"t2_up","T2+","Toggle 2+", 13,0,0);
    ADD_BUTTON_PROP(pc,"t2_dn","T2-","Toggle 2-", 14,0,0);
    ADD_BUTTON_PROP(pc,"t3_up","T3+","Toggle 3+", 15,0,0);
    ADD_BUTTON_PROP(pc,"t3_dn","T3-","Toggle 3-", 16,0,0);
    ADD_BUTTON_PROP(pc,"t4_up","T4+","Toggle 4+", 17,0,0);
    ADD_BUTTON_PROP(pc,"t4_dn","T4-","Toggle 4-", 18,0,0);
    ADD_BUTTON_PROP(pc,"h3","H3","Hat 3 (Upper round hat)", 19,4,"N,E,S,W");
    ADD_BUTTON_PROP(pc,"h4","H4","Hat 4 (lower jagged hat)", 23,4,"N,E,S,W");
    ADD_BUTTON_PROP(pc,"pinky_up","P+","Pinky up", 27,0,0);
    ADD_BUTTON_PROP(pc,"pinky_dn","P-","Pinky down", 28,0,0);
    ADD_BUTTON_PROP(pc,"dial_fwd","D+","Dial forward", 29,0,0);
    ADD_BUTTON_PROP(pc,"dial_back","D-","Dial backward", 30,0,0);
    ADD_BUTTON_PROP(pc,"bball","BP","Ball push", 31,0,0);
    ADD_BUTTON_PROP(pc,"slider","SLD","Slider", 32,0,0);
    ADD_BUTTON_PROP(pc,"mode","Mode","Mode switch", 33,3,"M1,M2,S1");


    pc=laAddPropertyContainer("la_controller_x56_stick", "X56 Stick", "X56 Stick", 0,laui_X56Stick,sizeof(laController),0,0,1);
    LA_PC_JS_X56_STICK = pc;
    laAddStringProperty(pc,"name","Name","Name of the controller", LA_WIDGET_STRING_PLAIN,0,0,0,1,offsetof(laController,Name),0,0,0,0,LA_READ_ONLY|LA_AS_IDENTIFIER);
    laAddSubGroup(pc,"base","Base","Generic controller status", "la_controller",0,0,0,0,0,0,0,0,0,0,0,LA_UDF_LOCAL);
    ADD_AXIS_PROP(pc,"stick","Stick","Main stick",0,2,"Left/Right,Up/Down");
    ADD_AXIS_PROP(pc,"ball","Ball","Ball stick",2,2,"Left/Right,Up/Down");
    ADD_AXIS_PROP(pc,"rudder","Rudder","Ruder twist",4,0,0);
    ADD_AXIS_PROP(pc,"pov","POV","POV hat",5,2,"Left/Right,Up/Down");
    ADD_BUTTON_PROP(pc,"trigger","Trigger","Trigger", 0,0,0);
    ADD_BUTTON_PROP(pc,"ba","A","Button A", 1,0,0);
    ADD_BUTTON_PROP(pc,"bb","B","Button B (Side of stick)", 2,0,0);
    ADD_BUTTON_PROP(pc,"bball","BP","Ball push", 3,0,0);  ADD_BUTTON_PROP(pc,"bc","BC","Button C (ball push)", 3,0,0);
    ADD_BUTTON_PROP(pc,"pinky","PK","Pinky small button", 4,0,0); ADD_BUTTON_PROP(pc,"bd","BD","Button D pinky small button", 4,0,0);
    ADD_BUTTON_PROP(pc,"pinkyl","PKL","Pinky lever", 5,0,0);
    ADD_BUTTON_PROP(pc,"h1","H1","Hat 1 (Upper round hat)", 6,4,"N,E,S,W");
    ADD_BUTTON_PROP(pc,"h2","H2","Hat 2 (lower jagged hat)", 10,4,"N,E,S,W");
    //button 14-16 not sure where it is....
}

void laui_GenericJoystick(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil); laUiItem* b,*b1;
    char buf[32];

    laShowItem(uil,c,This,"name")->Flags|=LA_TEXT_ALIGN_CENTER;
    b=laBeginRow(uil,c,0,0);
    laShowItem(uil,c,This,"user_assigned_id");
    b1=laOnConditionThat(uil,c,laPropExpression(This,"error"));{
        laShowLabel(uil,c,"Device error.",0,0)->Expand=1;
    }laElse(uil,b1);{
        laShowItem(uil,c,This,"path")->Expand=1;
        laShowItem(uil,c,This,"remove");
    }laEndCondition(uil,b1);
    laEndRow(uil,b);
    laShowSeparator(uil,c);

    laShowLabel(uil,c,"Axes",0,0);
    for(int i=0;i<8;i++){
        if(!(i%2)){ b=laBeginRow(uil,c,0,0); }
        sprintf(buf,"a%d",i);
        laShowLabel(uil,c,buf,0,0); laShowItem(uil,c,This,buf)->Expand=1;
        if(i%2){ laEndRow(uil,b); }
    }
    b1=laOnConditionToggle(uil,c,0,0,0,0,0); strSafeSet(&b1->ExtraInstructions,"text=More axes..."); b1->Flags|=LA_TEXT_ALIGN_LEFT;
    for(int i=8;i<LA_JS_MAX_AXES;i++){
        if(!(i%2)){ b=laBeginRow(uil,c,0,0); }
        sprintf(buf,"a%d",i);
        laShowLabel(uil,c,buf,0,0); laShowItem(uil,c,This,buf)->Expand=1;
        if(i%2){ laEndRow(uil,b); }
    }
    laEndCondition(uil,b1);
    
    laShowSeparator(uil,c);

    laShowLabel(uil,c,"Buttons",0,0);
    for(int i=0;i<16;i++){
        if(!(i%2)){ b=laBeginRow(uil,c,0,0); }
        sprintf(buf,"b%d",i);
        laShowLabel(uil,c,buf,0,0); laShowItem(uil,c,This,buf)->Expand=1;
        if(i%2){ laEndRow(uil,b); }
    }
    b1=laOnConditionToggle(uil,c,0,0,0,0,0); strSafeSet(&b1->ExtraInstructions,"text=More buttons..."); b1->Flags|=LA_TEXT_ALIGN_LEFT;
    for(int i=8;i<LA_JS_MAX_BUTTONS;i++){
        if(!(i%2)){ b=laBeginRow(uil,c,0,0); }
        sprintf(buf,"b%d",i);
        laShowLabel(uil,c,buf,0,0); laShowItem(uil,c,This,buf)->Expand=1;
        if(i%2){ laEndRow(uil,b); }
    }
    laEndCondition(uil,b1);
}

void laui_X56Throttle(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil),*cl, *cr, *crl,*crr, *rc, *vc, *rcl,*rcr;
    laSplitColumn(uil,c,0.17); cl=laLeftColumn(c,0); cr=laRightColumn(c,0);
    laSplitColumn(uil,cr,0.4); crl=laLeftColumn(cr,0); crr=laRightColumn(cr,0);
    laSplitColumn(uil,crr,0.6); rc=laLeftColumn(crr,10); vc=laRightColumn(crr,0);
    laSplitColumn(uil,rc,0.4); rcl=laLeftColumn(rc,2); rcr=laRightColumn(rc,0);
    laUiItem* b,*ui,*g; laUiList*gu;

    laUiItem* JB=laShowInvisibleItem(uil,c,This,"base");

    laShowItem(uil,c,&JB->PP, "name")->Flags|=LA_TEXT_ALIGN_CENTER;
    b=laBeginRow(uil,c,0,0);
    laShowItem(uil,c,&JB->PP, "user_assigned_id");
    laUiItem* b1=laOnConditionThat(uil,c,laPropExpression(&JB->PP, "error"));{
        laShowLabel(uil,c,"Device error.",0,0)->Expand=1;
    }laElse(uil,b1);{
        laShowItem(uil,c,&JB->PP, "path")->Expand=1;
        laShowItem(uil,c,&JB->PP, "remove");
    }laEndCondition(uil,b1);
    laEndRow(uil,b);
    laShowSeparator(uil,c);

    laShowItem(uil,cl,This,"pinky_up");
    laShowItem(uil,cl,This,"pinky_dn");
    laShowSeparator(uil,cl);
    laShowItem(uil,cl,This,"dial_fwd");
    laShowItem(uil,cl,This,"dial_back");

    b=laBeginRow(uil,crl,0,0);
    ui=laShowItem(uil,crl,This,"bi");ui->Expand=1;
    ui=laShowItem(uil,crl,This,"bh");ui->Expand=1;
    laEndRow(uil,b);
    b=laBeginRow(uil,crl,0,0);
    ui=laShowItem(uil,crl,This,"thr1");ui->Expand=1;ui->Extent=15;ui->Flags|=LA_UI_FLAGS_TRANSPOSE;
    laShowCompoundValue(ui,LA_SLOT_MARKER_1,This,"thr1_min");
    laShowCompoundValue(ui,LA_SLOT_MARKER_2,This,"thr1_max");
    ui=laShowItem(uil,crl,This,"thr2");ui->Expand=1;ui->Extent=15;ui->Flags|=LA_UI_FLAGS_TRANSPOSE;
    laShowCompoundValue(ui,LA_SLOT_MARKER_1,This,"thr2_min");
    laShowCompoundValue(ui,LA_SLOT_MARKER_2,This,"thr2_max");
    laEndRow(uil,b);
    b=laBeginRow(uil,crl,1,1);
    laShowItem(uil,crl,This,"thr1_min")->Flags|=LA_UI_FLAGS_KNOB; laShowItem(uil,crl,This,"thr1_max")->Flags|=LA_UI_FLAGS_KNOB;
    //laShowSeparator(uil,crl)->Expand=1;
    laShowItem(uil,crl,This,"thr2_min")->Flags|=LA_UI_FLAGS_KNOB; laShowItem(uil,crl,This,"thr2_max")->Flags|=LA_UI_FLAGS_KNOB;
    laEndRow(uil,b);

    laShowItem(uil,rcl,This,"bf");
    laShowItem(uil,rcr,This,"wf");
    laShowItem(uil,rcl,This,"bg");
    laShowItem(uil,rcr,This,"wg");
    laShowSeparator(uil,cl);
    laShowItem(uil,rcl,This,"slider");
    laShowSeparator(uil,rcl);
    laShowItem(uil,rcl,This,"be");
    laShowItem(uil,rcl,This,"bball");
    laShowItem(uil,rcr,This,"ball")->Extent=0;

    laShowItem(uil,rc,This,"h3")->Flags|=LA_UI_FLAGS_TRANSPOSE;
    laShowItem(uil,rc,This,"h4")->Flags|=LA_UI_FLAGS_TRANSPOSE;
    
    laShowItem(uil,vc,This,"t4_up");
    laShowItem(uil,vc,This,"t4_dn"); laShowSeparator(uil,vc);
    laShowItem(uil,vc,This,"t3_up");
    laShowItem(uil,vc,This,"t3_dn"); laShowSeparator(uil,vc);
    laShowItem(uil,vc,This,"t2_up");
    laShowItem(uil,vc,This,"t2_dn"); laShowSeparator(uil,vc);
    laShowItem(uil,vc,This,"t1_up");
    laShowItem(uil,vc,This,"t1_dn"); laShowSeparator(uil,vc);

    laShowItem(uil,vc,This,"r3");
    laShowItem(uil,vc,This,"r4");

    laShowSeparator(uil,c);

    laShowLabel(uil,cl,"Mode",0,0)->Flags|=LA_TEXT_ALIGN_CENTER;
    laShowItem(uil,cl,This,"mode");

    laShowLabel(uil,cr,"Switches",0,0)->Flags|=LA_TEXT_ALIGN_CENTER;

    b=laBeginRow(uil,cr,0,0);
    laShowItem(uil,cr,This,"sw1")->Expand=1;
    laShowItem(uil,cr,This,"sw3")->Expand=1;
    laShowItem(uil,cr,This,"sw5")->Expand=1;
    laEndRow(uil,b);
    b=laBeginRow(uil,cr,0,0);
    laShowItem(uil,cr,This,"sw2")->Expand=1;
    laShowItem(uil,cr,This,"sw4")->Expand=1;
    laShowItem(uil,cr,This,"sw6")->Expand=1;
    laEndRow(uil,b);
    

}
void laui_X56Stick(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil),*cl, *cc, *cr;
    laSplitColumn(uil,c,0.2); cl=laLeftColumn(c,10); cr=laRightColumn(c,0);
    laSplitColumn(uil,cr,0.8); cc=laLeftColumn(cr,0); cr=laRightColumn(cr,10);
    laUiItem* b;

    laUiItem* JB=laShowInvisibleItem(uil,c,This,"base");

    laShowItem(uil,c,&JB->PP, "name")->Flags|=LA_TEXT_ALIGN_CENTER;
    b=laBeginRow(uil,c,0,0);
    laShowItem(uil,c,&JB->PP, "user_assigned_id");
    laUiItem* b1=laOnConditionThat(uil,c,laPropExpression(&JB->PP, "error"));{
        laShowLabel(uil,c,"Device error.",0,0)->Expand=1;
    }laElse(uil,b1);{
        laShowItem(uil,c,&JB->PP, "path")->Expand=1;
        laShowItem(uil,c,&JB->PP, "remove");
    }laEndCondition(uil,b1);
    laEndRow(uil,b);
    laShowSeparator(uil,c);

    laShowItem(uil,cl,This,"ba");
    laShowItem(uil,cl,This,"pov");

    laShowSeparator(uil,cl);

    laShowLabel(uil,cl,"Thumb",0,0)->Flags|=LA_TEXT_ALIGN_CENTER;
    laShowItem(uil,cl,This,"bc");
    laShowItem(uil,cl,This,"ball");

    laShowSeparator(uil,cl);

    laShowLabel(uil,cl,"Pinky",0,0)->Flags|=LA_TEXT_ALIGN_CENTER;
    laShowItem(uil,cl,This,"pinkyl");
    laShowItem(uil,cl,This,"pinky");


    laShowItem(uil,cr,This,"bb");
    laShowSeparator(uil,cr);

    laShowItem(uil,cc,This,"trigger");
    laShowItem(uil,cc,This,"stick");
    laShowItem(uil,cc,This,"rudder");

    laShowLabel(uil,cr,"H1",0,0)->Flags|=LA_TEXT_ALIGN_CENTER;
    laShowItem(uil,cr,This,"h1");

    laShowSeparator(uil,cr);

    laShowLabel(uil,cr,"H2",0,0)->Flags|=LA_TEXT_ALIGN_CENTER;
    laShowItem(uil,cr,This,"h2");
}
