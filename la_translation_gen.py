import sys,re

if(len(sys.argv))<2:
    print("Provide a file!")
    exit(1)

try:
    f=open(sys.argv[1])
    t=open("GeneratedTranslations.c",'w')
    lines = f.readlines()
    t.write("static const char *entries[]={\n")
    for l in lines:
        ma=re.search(r"(.*?)\s*\|\s*(.*)",l)
        if ma:
            t.write("\"%s\",\"%s\",\n"%(ma.group(1),ma.group(2)))
    t.write("0,0};\nfor(int i=0;;i++){if(!entries[i*2])break;\ntransNewEntry(entries[i*2],entries[i*2+1]);\n}")
except:
    print("Error parsing file")
    exit(1)
