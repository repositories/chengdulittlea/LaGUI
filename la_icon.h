#pragma once

// Latin-1 Supplement
#ifndef _LA_ICO_LATIN_1_SUPPLEMENT
#define _LA_ICO_LATIN_1_SUPPLEMENT \
" «»×÷ÁÂÄÀÅÃÆÇÐÉÊËÈÍÎÏÌÑÓÔÖÒØÕÞÚÛÜÙÝáâäàå" \
"ãæçðéêëèíîïìñóôöòøõþßúûüùýÿªº¡¿·¶§©®°¢£¥" \
"¨´¯¸ ©®÷" \
"¬× ±§"
;
#endif

// Latin Extended-A
#ifndef _LA_ICO_LATIN_EXTENDED_A
#define _LA_ICO_LATIN_EXTENDED_A \
"ĂĀĄĆČĊĎĐĚĖĒĘĞĢĠĦĲİĪĮĶĹĽĻŁŃŇŅŊŐŌŒŔŘŖŚŠŞŤŬ" \
"ŰŪŲŮŴŶŸŹŽŻăāąćčċďđěėēęğģġħıĳīįķĺľļłńňņŋő" \
"ōœŕřŗśšşťŭűūųůŵŷźžż"
;
#endif

// Latin Extended-B
#ifndef _LA_ICO_LATIN_EXTENDED_B
#define _LA_ICO_LATIN_EXTENDED_B \
"ǍȘȚǎȷșț"
;
#endif

// Spacing Modifier Letters
#ifndef _LA_ICO_SPACING_MODIFIER_LETTERS
#define _LA_ICO_SPACING_MODIFIER_LETTERS \
"˙˝ˆˇ˘˚˜˛ˉ"
;
#endif

// Combining Diacritical Marks
#ifndef _LA_ICO_COMBINING_DIACRITICAL_MARKS
#define _LA_ICO_COMBINING_DIACRITICAL_MARKS \
"̧̨̦̰̈̀́̋̄̇̂̌̆̊̃̒̂̈̇̅̃"
;
#endif

// Greek and Coptic
#ifndef _LA_ICO_GREEK_AND_COPTIC
#define _LA_ICO_GREEK_AND_COPTIC \
"ΑΒΧΔΕΗΓΙΚΛΜΝΩΟΦΠΨΡΣΤΘϴΥΞΖαβχδεϵηγικϰλμνω" \
"οφϕπϖψρϱσςτθϑυξζ"
;
#endif

// Latin Extended Additional
#ifndef _LA_ICO_LATIN_EXTENDED_ADDITIONAL
#define _LA_ICO_LATIN_EXTENDED_ADDITIONAL \
"ẞẂẄẀỲẃẅẁỳ"
;
#endif

// General Punctuation
#ifndef _LA_ICO_GENERAL_PUNCTUATION
#define _LA_ICO_GENERAL_PUNCTUATION \
"…“”‘’•–—‚„‹›•‍‼⁉‶′‵⁗″‴‷"
;
#endif

// Currency Symbols
#ifndef _LA_ICO_CURRENCY_SYMBOLS
#define _LA_ICO_CURRENCY_SYMBOLS \
"€"
;
#endif

// Combining Diacritical Marks for Symbols
#ifndef _LA_ICO_COMBINING_DIACRITICAL_MARKS_FOR_SYMBOLS
#define _LA_ICO_COMBINING_DIACRITICAL_MARKS_FOR_SYMBOLS \
"⃠⃝⃟⃣⃢⃞⃤⃣⃢⃣⃚⃙⃦⃪⃫⃒⃥⃘⃓⃮⃭⃯⃬⃨⃧⃔⃕⃜⃖⃐⃡⃗⃑⃛⃩"
;
#endif

// Letterlike Symbols
#ifndef _LA_ICO_LETTERLIKE_SYMBOLS
#define _LA_ICO_LETTERLIKE_SYMBOLS \
"™™ℹℬℭℂⅅℰℱℾℌℍℋℑℐℒℳℕℙℿℚℜℝℛℨℤℵℶℸⅆⅇℯℽℷℊⅈⅉℴℼℎ" \
"⅀"
;
#endif

// Number Forms
#ifndef _LA_ICO_NUMBER_FORMS
#define _LA_ICO_NUMBER_FORMS \
"ⅧⅪⅬⅤⅮↁⅣⅭⅨⅠↃↈↀⅦⅥⅩↂⅯⅢⅫⅡⅷⅺⅼↆↇⅴⅾⅳⅽⅸⅰⅿⅶⅵↅⅹⅲ↋ⅻ" \
"ⅱ↊"
;
#endif

// Arrows
#ifndef _LA_ICO_ARROWS
#define _LA_ICO_ARROWS \
"↓←↔↗↖→↘↙↑↕⇧⇨⇩⇦⇫⇬⇭⇮⇯⇰⇳⇪↯↔↕↖↗↘↙↩↪↺↵↻⇣⇵↓⇃⇂⇓" \
"⇊↧↲↳↡⇆←↽↼⇠⇐⇇⇍⇺↤↩↫⇽⇋↔⇔⇎⇼⇿↮⇹↭⇜↚⇤↢↹⇚↞⇷↜↗⇗↖⇱" \
"⇖↸⇟⇞→⇀⇁⇴↴⇢⇒⇉⇏⇻↦↪↬⇾⇄⇌⇝↛⇥↣⇛↠⇸↝↘⇲⇘↙⇙⇶↶↷⇅↑↿↾" \
"⇡⇑⇈↕⇕↨↥↰↱↟"
;
#endif

// Mathematical Operators
#ifndef _LA_ICO_MATHEMATICAL_OPERATORS
#define _LA_ICO_MATHEMATICAL_OPERATORS \
"−⋅∙⊙⋄∘⋆≌≊∠∳≐≈≒≆⊦∗⊛≃∵≬⋈∙∲∱≔∁≅∍⊳⊵⋺⋽⋻∮∐≘∛⋏⋎" \
"⊝∬≜⋄≏∣∕⋇⋫⋭∤⊮⋠⊬⋡⋅∸∔⋒⋐⋑⋓⊫⋱∈∊⋵⋲⋶⋹⋸⋳⋮∅∎⋕⊜≕≝⋝" \
"⋜⋞⋟≚≡≍≙∹∃⊩∜⊘≑≎∺∇≩⋧⋗≥⋛≳≷≧⊹∻⊷≓∆∞∫⊺∩∾⋉⋋≨⋦⋖≤" \
"⋚≲≶≦∧∨∡≞⋯−⊖∓≂⊧≫≪⊸⊗⊌⊍⊎⋂⋀⋁⋃⊼⊯≇⊈⊉≵≴≸⊽⊲⊴≉≄∌∉" \
"≠≭≯≱≹≢≮≰⋪⋬∦⊀≁⋢⋣⊄⊁⊅⊭⊙⊶∟∥∂⋔⊕≺⋨≼≾⊰∏∷∝≟√∶⊆⊇∽" \
"⋍⊾⋊⋌⊢⊿≗≖∘⊚∖∼∿⋾⋼⋷⋴∢⊓⊔⊡⊟⊞⊠⊏⊑⋤⊐⊒⋥≛⋆≣⊂⊊≻⋩≽≿⊱" \
"∋∑⊃⊋∯⊤⊣∄∴∭≋⊪⊨∪∀⋰⊥⋙⋘∰≀⊻⋿"
;
#endif

// Miscellaneous Technical
#ifndef _LA_ICO_MISCELLANEOUS_TECHNICAL
#define _LA_ICO_MISCELLANEOUS_TECHNICAL \
"⏦⌮⎇〈〉⌒⌬⏣⌞⌍⌟⌌⎋⎚⎄⌲⎁⌃⌴⌵⌭⏨⎖⌀⌱⎓⎂⌄⎊⏉⏁⏄⏇⏚⌁⏧⎃⎆⏥⌢" \
"⏛⎈⎉⎯⌂⎎⎀⌈⌊⏑⏒⏔⏙⏓⏘⏗⏖⏕⎍⎘⎏⎐⎑⎒⌆⎗⎙⌅⌉⌋⍼⌔⌓⌳⌣⎔⌑⏍⏤⌯" \
"⌕⌜⌏⌎⌝⌰⎌⏊⏂⏅⏈⏌⎿⏋⎾⏆⏀⏐⏃⌗⌇⎅⏢⌤⏺⏪⏮⏷⏴⏵⏶⏩⏭⏯⏹⏸⌛⏳⌧⌫" \
"⌦⏏⌨⌥⌘⏎⍽⏱⏲⌚⌖⍾⍻⏿⏻⏼⏽⎔⏾⍿⌚⌛⌨⏏⏩⏪⏫⏬⏭⏮⏯⏰⏱⏲⏳⏸⏹⏺⌶⍺" \
"⍶⍀⍉⍥⌾⍟⌽⍜⍪⍢⍒⍋⍙⍫⍚⍱⍦⍎⍊⍖⍷⍩⍳⍸⍤⍛⍧⍅⍵⍹⎕⍂⌼⍠⍔⍍⌺⌹⍗⍌" \
"⌸⍄⌻⍇⍃⍯⍰⍈⍁⍐⍓⍞⍘⍴⍆⍮⌿⌷⍣⍭⍨⍲⍝⍡⍕⍑⍏⍬⌞⌟⏟⏝⎵⎶⏡⎪⎯⌡⎮⌠" \
"⌈⌊⎩⎨⎧⎢⎣⎡⌐⎜⎝⎛⎟⎠⎞⌉⌋⍼⎭⎬⎫⎥⎦⎤⎳⎲⌜⌝⏞⏜⎴⏠⌙⎰⎱⏐"
;
#endif

// Control Pictures
#ifndef _LA_ICO_CONTROL_PICTURES
#define _LA_ICO_CONTROL_PICTURES \
"␣␢␆␈␇␘␍␐␡␥␔␑␓␒␙␃␗␄␅␛␜␌␝␉␊␕␤␀␞␏␎␠␁␂␚␦␖␟␋"
;
#endif

// Optical Character Recognition
#ifndef _LA_ICO_OPTICAL_CHARACTER_RECOGNITION
#define _LA_ICO_OPTICAL_CHARACTER_RECOGNITION \
"⑇⑄⑅⑆⑁⑉⑈⑊⑂⑀⑃"
;
#endif

// Enclosed Alphanumerics
#ifndef _LA_ICO_ENCLOSED_ALPHANUMERICS
#define _LA_ICO_ENCLOSED_ALPHANUMERICS \
"ⒶⒷⒸⒹⒺⒻⒼⒽⒾⒿⓀⓁⓂⓃⓄⓅⓆⓇⓈⓉⓊⓋⓌⓍⓎⓏⓐ⒜ⓑ⒝ⓒ⒞ⓓ⒟ⓔ⒠⑧⓼⑻⒏" \
"ⓕ⒡⑤⓹⑸⒌④⓸⑷⒋ⓖ⒢ⓗ⒣ⓘ⒤ⓙ⒥ⓚ⒦ⓛ⒧ⓜ⒨ⓝ⒩⑨⓽⑼⒐ⓞ⒪①⓵⑴⓲⑱⒅⒙⓯" \
"⑮⒂⒖⓮⑭⒁⒕⓳⑲⒆⒚⓫⑪⑾⒒⒈⓱⑰⒄⒘⓰⑯⒃⒗⓭⑬⒀⒔⓬⑫⑿⒓⑩⓾⑽⒑ⓟ⒫ⓠ⒬" \
"ⓡ⒭ⓢ⒮⑦⓻⑺⒎⑥⓺⑹⒍ⓣ⒯③⓷⑶⒊②⓶⑵⒉⓴⑳⒇⒛ⓤ⒰ⓥ⒱ⓦ⒲ⓧ⒳ⓨ⒴ⓩ⒵⓿⓪" \
"Ⓜ⑴⑵"
;
#endif

// Geometric Shapes
#ifndef _LA_ICO_GEOMETRIC_SHAPES
#define _LA_ICO_GEOMETRIC_SHAPES \
"◌◊●○◯◐◑◒◓◖◗◔◕◴◵◶◷◍◌◉◎◦◘◙◚◛◠◡◜◝◞◟◆◇◈▰▱▮▬▭" \
"▯■□▢▣▤▥▦▧▨▩▪▫◧◨◩◪◫◰◱◲◳◻◼◽◾▲▶▼◀△▷▽◁◬◭◮►◄▻" \
"◅▴▸▾◂▵▹▿◃◥◢◣◤◹◿◺◸▪▫▶◀◻◼◽◾◌▽◁◊◻▷△▯"
;
#endif

// Miscellaneous Symbols
#ifndef _LA_ICO_MISCELLANEOUS_SYMBOLS
#define _LA_ICO_MISCELLANEOUS_SYMBOLS \
"☬⚗⚓☥♒♈☊⛢⚛♬⛨⚑⚸♻☻☤♋♑⛫⚳⚷⛪⚰☌☧⚔⛾☋⚮⚢⚣♁♱☫⛴☽⛳♭⚜⚘" \
"⛲⛽⚱⚙⛮⛭♊⚒☭⛼⛣⛬⚩⛸⚤⛧⛻☩⚵♃☾⛦♌♎☨⚥⚧⚦⛯⚭♂☿⛰♮♆⚲♪♫⛎☍" \
"☦⚝⚴♽☮⛤♾⛹♓♇♩⚻♼♲♺♳♴♵♶♷♸♹⛥♐⛵☓♄⚖♏⚺⚼⚹♯⛩⛷⛶⚕⚚☪♉" \
"⛺⛱⚯♅♀⚨⚶♍♰☸⚐⛿☹☺☯☀☁☂☃☄☇☈☉☔★☆☎☏☐☑☒☕☖☗☚☛☜☝☞☟" \
"☠☡☢☣☰☱☲☳☴☵☶☷♨♿⚆⚇⚈⚉⚊⚋⚌⚍⚎⚏⚞⚟⚡⚪⚫⚬⚽⚾⚿⛀⛁⛂⛃⛄⛅⛆" \
"⛇⛈⛉⛊⛋☼☘☙♔♕♖♗♘♙♚♛♜♝♞♟♠♡♢♣♤♥♦♧⚀⚁⚂⚃⚄⚅⚠⛌⛍⛏⛐⛑" \
"⛒⛓⛔⛕⛖⛗⛘⛙⛚⛛⛜⛝⛞⛟⛠⛡☀☁☂☃☄☎☑☔☕☘☝☠☢☣☦☪☮☯☸☹☺♀♂♈" \
"♉♊♋♌♍♎♏♐♑♒♓♟♠♣♥♦♨♻♾♿⚒⚓⚔⚕⚖⚗⚙⚛⚜⚠⚡⚧⚪⚫⚰⚱⚽⚾⛄⛅" \
"⛈⛎⛏⛑⛓⛔⛩⛪⛰⛱⛲⛳⛴⛵⛷⛸⛹⛺⛽♭♮♯"
;
#endif

// Dingbats
#ifndef _LA_ICO_DINGBATS
#define _LA_ICO_DINGBATS \
"✝❽➑➇❺➎➄❹➍➃✠❾➒➈❶➊➀❿➓➉✟❼➐➆✞❻➏➅✡❸➌➂❷➋➁❰❮❱❯❲" \
"❳❪❫❴❬❨❩❵❭➔➘➙➚➛➜➝➞➟➠➢➣➤➥➦➧➨➩➪➫➬➭➮➯➱➲➳➶➵➴➹" \
"➸➷➺➻➼➽➾➡❖✙✚✛✜✁✃✄✆✇✈✉✌✍✎✏✐✑✒✓✔✕✖✗✘✢✣✤✥✦✧✩" \
"✪✫✬✭✮✯✰✱✲✳✴✵✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❓" \
"❗❘❙❚❛❜❝❞❟❠❡❢❣❤❥❦❧✀✂✋✂✅✈✉✊✋✌✍✏✒✔✖✝✡✨✳✴❄❇❌" \
"❎❓❔❕❗❣❤➕➖➗➡➰➿"
;
#endif

// Miscellaneous Mathematical Symbols-A
#ifndef _LA_ICO_MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A
#define _LA_ICO_MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A \
"⟑⟒⟍⟗⟙⟘⟚⟛⟜⟕⟨⟪⟮⟅⟦⟬⟌⟞⟝⟓⟠⟃⟄⟇⟂⟈⟖⟩⟫⟯⟆⟧⟭⟋⟎⟏⟉⟀⟟⟔" \
"⟊⟡⟢⟣⟐⟤⟥⟁"
;
#endif

// Supplemental Arrows-A
#ifndef _LA_ICO_SUPPLEMENTAL_ARROWS_A
#define _LA_ICO_SUPPLEMENTAL_ARROWS_A \
"⟲⟳⟱⟵⟸⟽⟻⟷⟺⟴⟹⟾⟼⟿⟶⟰"
;
#endif

// Braille Patterns
#ifndef _LA_ICO_BRAILLE_PATTERNS
#define _LA_ICO_BRAILLE_PATTERNS \
"⠀⠁⠃⠇⠏⠟⠿⡿⣿⢿⡟⣟⢟⠯⡯⣯⢯⡏⣏⢏⠗⠷⡷⣷⢷⡗⣗⢗⠧⡧⣧⢧⡇⣇⢇⠋⠛⠻⡻⣻" \
"⢻⡛⣛⢛⠫⡫⣫⢫⡋⣋⢋⠓⠳⡳⣳⢳⡓⣓⢓⠣⡣⣣⢣⡃⣃⢃⠅⠍⠝⠽⡽⣽⢽⡝⣝⢝⠭⡭⣭⢭" \
"⡍⣍⢍⠕⠵⡵⣵⢵⡕⣕⢕⠥⡥⣥⢥⡅⣅⢅⠉⠙⠹⡹⣹⢹⡙⣙⢙⠩⡩⣩⢩⡉⣉⢉⠑⠱⡱⣱⢱⡑" \
"⣑⢑⠡⡡⣡⢡⡁⣁⢁⠂⠆⠎⠞⠾⡾⣾⢾⡞⣞⢞⠮⡮⣮⢮⡎⣎⢎⠖⠶⡶⣶⢶⡖⣖⢖⠦⡦⣦⢦⡆" \
"⣆⢆⠊⠚⠺⡺⣺⢺⡚⣚⢚⠪⡪⣪⢪⡊⣊⢊⠒⠲⡲⣲⢲⡒⣒⢒⠢⡢⣢⢢⡂⣂⢂⠄⠌⠜⠼⡼⣼⢼" \
"⡜⣜⢜⠬⡬⣬⢬⡌⣌⢌⠔⠴⡴⣴⢴⡔⣔⢔⠤⡤⣤⢤⡄⣄⢄⠈⠘⠸⡸⣸⢸⡘⣘⢘⠨⡨⣨⢨⡈⣈" \
"⢈⠐⠰⡰⣰⢰⡐⣐⢐⠠⡠⣠⢠⡀⣀⢀"
;
#endif

// Supplemental Arrows-B
#ifndef _LA_ICO_SUPPLEMENTAL_ARROWS_B
#define _LA_ICO_SUPPLEMENTAL_ARROWS_B \
"⤢⤡⤴⤵⥀⤻⥁⤓⤈⥿⥥⥯⥡⥙⥝⥕⤶⤷⤋⥱⤯⤬⥸⥃⥳⤟⤙⥷⥺⤝⥆⥐⥋⥊⥎⤆⤛⤂⤌⥼" \
"⥧⥫⥞⥖⥢⥪⥦⥚⥒⤄⤹⤎⥈⥶⤿⤾⤢⤨⤱⤮⤤⤡⤧⤲⤣⥵⥂⥴⤠⤚⥇⤞⤑⥅⤕⤔⤇⤜⥰⤃" \
"⤍⥽⥩⥭⥟⥗⥨⥬⥤⥛⥓⤸⤵⤴⤏⤅⤁⤖⤘⤗⤀⤐⤫⤰⥄⤩⤭⤥⤪⤦⥹⥻⥲⤺⤽⤼⤒⤉⥑⥍" \
"⥌⥏⥾⥮⥣⥠⥘⥜⥔⤊⥉⤳"
;
#endif

// Miscellaneous Mathematical Symbols-B
#ifndef _LA_ICO_MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B
#define _LA_ICO_MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B \
"⧫⦁⦿⦟⦞⦤⦼⦶⧹⧸⧓⧭⧪⧗⧫⧑⧒⦿⦺⧂⧃⦵⦻⦙⧟⧺⦕⦖⧨⧩⦴⦱⦳⦲⧣⧤⧳⧱⧯⧲" \
"⧰⧮⧦⧁⧥⧜⧡⧞⧏⦑⦓⦗⧚⧼⦏⦍⦋⦃⧘⧀⦛⦫⦪⦯⦭⦮⦬⦩⦨⦝⧿⦧⦦⦷⦹⦸⧵⧷⦣⦥" \
"⦰⦜⧎⦒⦔⦘⧛⧽⦎⦐⦌⦄⧙⧴⧌⧢⧶⦠⦡⧠⧆⧅⧄⧈⧇⧧⧝⧔⧕⧾⧊⧍⧋⧻⦀⦢⧉⦽⧐⦚" \
"⦾⧬⧖⦅⦆⦁⦂⦉⦇⦊⦈"
;
#endif

// Supplemental Mathematical Operators
#ifndef _LA_ICO_SUPPLEMENTAL_MATHEMATICAL_OPERATORS
#define _LA_ICO_SUPPLEMENTAL_MATHEMATICAL_OPERATORS \
"⩯⨿⨑⩰⨐⩍⫏⫑⫐⫒⩌⩐⩭⨸⫮⩴⫪⪚⪙⪜⪛⫺⫹⩓⩔⪢⪡⪣⪻⫽⩎⩏⫬⪼⫫⫥⫣⫱⫙⩱" \
"⩳⪮⩦⩷⩮⩸⪪⪬⨍⫝̸⪌⪒⪐⪎⪔⪊⪈⪥⪧⪩⪆⩾⪂⪄⪀⪤⩺⩼⩧⨕⨏⨎⨙⨗⨛⨘⨜⨚⨼⩉" \
"⩇⩋⩀⩄⩃⨝⨞⫼⪫⪭⪋⪑⪏⪍⪓⪉⪇⪦⪨⪅⩽⪁⪃⩿⩹⩻⨔⨒⨓⩑⩞⩠⩜⩚⩟⩙⩒⩢⩣⩝" \
"⩛⫦⨺⨩⨪⨫⨬⨊⨷⨴⨵⨻⨶⨰⨱⨀⨁⨂⨅⨆⨉⨃⨄⫿⫝⫲⫳⫡⫚⩲⨭⨮⨹⨨⨣⨥⨧⨤⨦⨢" \
"⪷⪳⪹⪵⪯⪱⨌⨖⫭⨽⨲⫟⫧⫞⫠⫩⫨⪠⪟⩬⪞⪝⪖⪘⪕⪗⩘⩗⨳⫍⫎⫕⫓⫉⫅⫋⫇⫃⪽⫁" \
"⪿⪸⪴⪺⪶⪰⪲⨋⫔⫖⫘⫗⫊⫆⫌⫈⫄⪾⫂⫀⩶⩪⩫⫛⫶⩨⩩⫸⫷⫻⫴⫵⩵⩕⩖⨇⨈⩈⩆⩊" \
"⩅⩁⩂⨯⩡⫤⫢⫯⫰⫾⩤⩥⨾⨟⨠⨡"
;
#endif

// Miscellaneous Symbols and Arrows
#ifndef _LA_ICO_MISCELLANEOUS_SYMBOLS_AND_ARROWS
#define _LA_ICO_MISCELLANEOUS_SYMBOLS_AND_ARROWS \
"⬀⬂⬃⬁⬄⬆⬈⮕⬊⬇⬋⬅⬉⬌⬍⬖⬗⬘⬙⬚⮍⮎⭯⮌⮏⭛⭝⮽⭞⮨⮩⮮⮬⮯⮭⮪⮫⯁⮟⬢" \
"⬬⬤⬛⮜⬥⯆⯇⬧⯈⯅⯄⬟⮞⭓⬩⬪⭑⯀⮝⬮⬝⯋⮿⮾⭮⮃⭣⭽⭳⮠⮡⭭⭍⮇⮔⭘⭗⭙⭕⭖" \
"⬣⯃⭤⭠⭺⭰⮦⮤⭪⮀⮄⯌⬒⬔⮒⮓⭧⭷⭦⭶⮐⮑⮰⮱⮶⮴⮷⮵⮲⮳⭢⭼⭲⮧⮥⭬⮂⮆⯍⯏" \
"⭏⭟⭎⭚⭜⭨⭸⭩⭹⯐⭾⭿⮛⮙⮘⮚⯊⬓⬕⯂⯑⮹⭥⭡⭻⭱⮢⮣⭫⮸⮁⮅⯎⬡⬭⬜⬦⬨⭐⬠" \
"⭔⬫⭒⬯⬞⮋⮈⮊⮉⮗⮺⮻⮼⯉⯒⯓⯔⯕⯖⯗⯘⯙⯚⯛⯜⯝⯞⯟⯠⯡⯢⯣⯤⯥⯦⯧⯨⯩⯪⯫" \
"⯬⯭⯮⯯⯰⯱⯲⯳⯴⯵⯶⯷⯸⯹⯺⯻⯼⯽⯿⬅⬆⬇⬛⬜⭐⭕⬿⭀⭊⭂⭋⬾⬐⬑⬲⬸⬺⬹⭅⬶" \
"⬵⬻⬽⬼⬴⬷⬰⬳⯾⭁⭇⭈⭌⭃⭄⭆⬎⬏⬱⭉"
;
#endif

// CJK Symbols and Punctuation
#ifndef _LA_ICO_CJK_SYMBOLS_AND_PUNCTUATION
#define _LA_ICO_CJK_SYMBOLS_AND_PUNCTUATION \
"〰〽"
;
#endif

// Enclosed CJK Letters and Months
#ifndef _LA_ICO_ENCLOSED_CJK_LETTERS_AND_MONTHS
#define _LA_ICO_ENCLOSED_CJK_LETTERS_AND_MONTHS \
"㊗㊙"
;
#endif

// Yijing Hexagram Symbols
#ifndef _LA_ICO_YIJING_HEXAGRAM_SYMBOLS
#define _LA_ICO_YIJING_HEXAGRAM_SYMBOLS \
"䷀䷁䷂䷃䷄䷅䷆䷇䷈䷉䷊䷋䷌䷍䷎䷏䷐䷑䷒䷓䷔䷕䷖䷗䷘䷙䷚䷛䷜䷝䷞䷟䷠䷡䷢䷣䷤䷥䷦䷧" \
"䷨䷩䷪䷫䷬䷭䷮䷯䷰䷱䷲䷳䷴䷵䷶䷷䷸䷹䷺䷻䷼䷽䷾䷿"
;
#endif

// Variation Selectors
#ifndef _LA_ICO_VARIATION_SELECTORS
#define _LA_ICO_VARIATION_SELECTORS \
"︎️"
;
#endif

// Halfwidth and Fullwidth Forms
#ifndef _LA_ICO_HALFWIDTH_AND_FULLWIDTH_FORMS
#define _LA_ICO_HALFWIDTH_AND_FULLWIDTH_FORMS \
"｛｝"
;
#endif

// Specials
#ifndef _LA_ICO_SPECIALS
#define _LA_ICO_SPECIALS \
"￹￺￻"
;
#endif

// Ancient Greek Numbers
#ifndef _LA_ICO_ANCIENT_GREEK_NUMBERS
#define _LA_ICO_ANCIENT_GREEK_NUMBERS \
"𐅵𐅶𐅷𐅸𐆊𐆋𐅀𐅁𐅂𐅃𐅄𐅅𐅆𐅇𐅈𐅉𐅊𐅋𐅌𐅍𐅎𐅏𐅐𐅑𐅒𐅓𐅔𐅕𐅖𐅗𐅘𐅙𐅚𐅛𐅜𐅝𐅞𐅟𐅠𐅡" \
"𐅢𐅣𐅤𐅥𐅦𐅧𐅨𐅩𐅪𐅫𐅬𐅭𐅮𐅯𐅰𐅱𐅲𐅳𐅴𐅹𐅺𐅻𐅼𐅽𐅾𐅿𐆀𐆁𐆂𐆃𐆄𐆅𐆆𐆇𐆈𐆉𐆌𐆍𐆎"
;
#endif

// Ancient Symbols
#ifndef _LA_ICO_ANCIENT_SYMBOLS
#define _LA_ICO_ANCIENT_SYMBOLS \
"𐆠𐆐𐆑𐆒𐆓𐆔𐆕𐆖𐆗𐆘𐆙𐆚𐆛𐆜"
;
#endif

// Phaistos Disc
#ifndef _LA_ICO_PHAISTOS_DISC
#define _LA_ICO_PHAISTOS_DISC \
"𐇐𐇑𐇒𐇓𐇔𐇕𐇖𐇗𐇘𐇙𐇚𐇛𐇜𐇝𐇞𐇟𐇠𐇡𐇢𐇣𐇤𐇥𐇦𐇧𐇨𐇩𐇪𐇫𐇬𐇭𐇮𐇯𐇰𐇱𐇲𐇳𐇴𐇵𐇶𐇷" \
"𐇸𐇹𐇺𐇻𐇼𐇽"
;
#endif

// Coptic Epact Numbers
#ifndef _LA_ICO_COPTIC_EPACT_NUMBERS
#define _LA_ICO_COPTIC_EPACT_NUMBERS \
"𐋡𐋢𐋣𐋤𐋥𐋦𐋧𐋨𐋩𐋪𐋫𐋬𐋭𐋮𐋯𐋰𐋱𐋲𐋳𐋴𐋵𐋶𐋷𐋸𐋹𐋺𐋻𐋠"
;
#endif

// Rumi Numeral Symbols
#ifndef _LA_ICO_RUMI_NUMERAL_SYMBOLS
#define _LA_ICO_RUMI_NUMERAL_SYMBOLS \
"𐹠𐹡𐹢𐹣𐹤𐹥𐹦𐹧𐹨𐹩𐹪𐹫𐹬𐹭𐹮𐹯𐹰𐹱𐹲𐹳𐹴𐹵𐹶𐹷𐹸𐹹𐹺𐹻𐹼𐹽𐹾"
;
#endif

// Mayan Numerals
#ifndef _LA_ICO_MAYAN_NUMERALS
#define _LA_ICO_MAYAN_NUMERALS \
"𝋠𝋡𝋢𝋣𝋤𝋥𝋦𝋧𝋨𝋩𝋪𝋫𝋬𝋭𝋮𝋯𝋰𝋱𝋲𝋳"
;
#endif

// Tai Xuan Jing Symbols
#ifndef _LA_ICO_TAI_XUAN_JING_SYMBOLS
#define _LA_ICO_TAI_XUAN_JING_SYMBOLS \
"𝌀𝌁𝌂𝌃𝌄𝌅𝌆𝌇𝌈𝌉𝌊𝌋𝌌𝌍𝌎𝌏𝌐𝌑𝌒𝌓𝌔𝌕𝌖𝌗𝌘𝌙𝌚𝌛𝌜𝌝𝌞𝌟𝌠𝌡𝌢𝌣𝌤𝌥𝌦𝌧" \
"𝌨𝌩𝌪𝌫𝌬𝌭𝌮𝌯𝌰𝌱𝌲𝌳𝌴𝌵𝌶𝌷𝌸𝌹𝌺𝌻𝌼𝌽𝌾𝌿𝍀𝍁𝍂𝍃𝍄𝍅𝍆𝍇𝍈𝍉𝍊𝍋𝍌𝍍𝍎𝍏" \
"𝍐𝍑𝍒𝍓𝍔𝍕𝍖"
;
#endif

// Counting Rod Numerals
#ifndef _LA_ICO_COUNTING_ROD_NUMERALS
#define _LA_ICO_COUNTING_ROD_NUMERALS \
"𝍠𝍡𝍢𝍣𝍤𝍥𝍦𝍧𝍨𝍩𝍪𝍫𝍬𝍭𝍮𝍯𝍰𝍱𝍲𝍳𝍴𝍵𝍶𝍷𝍸"
;
#endif

// Mathematical Alphanumeric Symbols
#ifndef _LA_ICO_MATHEMATICAL_ALPHANUMERIC_SYMBOLS
#define _LA_ICO_MATHEMATICAL_ALPHANUMERIC_SYMBOLS \
"𝔄𝐀𝑨𝓐𝔸𝐴𝒜𝔅𝕭𝐁𝑩𝓑𝔹𝐵𝐂𝑪𝓒𝐶𝒞𝔇𝐃𝑫𝓓𝔻𝐷𝒟𝔈𝐄𝑬𝓔𝔼𝐸𝔉𝐅𝑭𝓕𝔽𝐹𝔊𝐆" \
"𝑮𝓖𝔾𝐺𝒢𝐇𝑯𝓗𝐻𝐈𝑰𝓘𝕀𝐼𝔍𝐉𝑱𝓙𝕁𝐽𝒥𝔎𝐊𝑲𝓚𝕂𝐾𝒦𝔏𝐋𝑳𝓛𝕃𝐿𝔐𝐌𝑴𝓜𝕄𝑀" \
"𝔑𝕹𝐍𝑵𝓝𝑁𝒩𝔒𝐎𝑶𝓞𝕆𝑂𝒪𝔓𝐏𝑷𝓟𝑃𝒫𝔔𝐐𝑸𝓠𝑄𝒬𝕽𝐑𝑹𝓡𝑅𝔖𝐒𝑺𝓢𝕊𝑆𝒮𝔗𝐓" \
"𝑻𝓣𝕋𝑇𝒯𝔘𝐔𝑼𝓤𝕌𝑈𝒰𝔙𝐕𝑽𝓥𝕍𝑉𝒱𝔚𝐖𝑾𝓦𝕎𝑊𝒲𝔛𝐗𝑿𝓧𝕏𝑋𝒳𝔜𝐘𝒀𝓨𝕐𝑌𝒴" \
"𝖅𝐙𝒁𝓩𝑍𝒵𝔞𝐚𝒂𝓪𝕒𝑎𝒶𝔟𝐛𝒃𝓫𝕓𝑏𝒷𝔠𝐜𝒄𝓬𝕔𝑐𝒸𝔡𝐝𝒅𝓭𝕕𝑑𝒹𝔢𝐞𝒆𝓮𝕖𝑒" \
"𝔣𝐟𝒇𝓯𝕗𝑓𝒻𝔤𝐠𝒈𝓰𝕘𝑔𝔥𝐡𝒉𝓱𝕙𝒽𝔦𝐢𝒊𝓲𝕚𝑖𝒾𝔧𝐣𝒋𝓳𝕛𝑗𝒿𝔨𝐤𝒌𝓴𝕜𝑘𝓀" \
"𝔩𝐥𝒍𝓵𝕝𝑙𝓁𝔪𝐦𝒎𝓶𝕞𝑚𝓂𝔫𝐧𝒏𝓷𝕟𝑛𝓃𝔬𝐨𝒐𝓸𝕠𝑜𝔭𝐩𝒑𝓹𝕡𝑝𝓅𝔮𝐪𝒒𝓺𝕢𝑞" \
"𝓆𝔯𝐫𝒓𝓻𝕣𝑟𝓇𝔰𝐬𝒔𝓼𝕤𝑠𝓈𝔱𝐭𝒕𝓽𝕥𝑡𝓉𝔲𝕬𝕮𝕯𝕰𝕱𝕲𝕳𝕴𝕵𝕶𝕷𝕸𝕺𝕻𝕼𝕾𝕿" \
"𝖀𝖁𝖂𝖃𝖄𝖆𝖇𝖈𝖉𝖊𝖋𝖌𝖍𝖎𝖏𝖐𝖑𝖒𝖓𝖔𝖕𝖖𝖗𝖘𝖙𝖚𝖛𝖜𝖝𝖞𝖟𝖠𝖡𝖢𝖣𝖤𝖥𝖦𝖧𝖨" \
"𝖩𝖪𝖫𝖬𝖭𝖮𝖯𝖰𝖱𝖲𝖳𝖴𝖵𝖶𝖷𝖸𝖹𝖺𝖻𝖼𝖽𝖾𝖿𝗀𝗁𝗂𝗃𝗄𝗅𝗆𝗇𝗈𝗉𝗊𝗋𝗌𝗍𝗎𝗏𝗐" \
"𝗑𝗒𝗓𝗔𝗕𝗖𝗗𝗘𝗙𝗚𝗛𝗜𝗝𝗞𝗟𝗠𝗡𝗢𝗣𝗤𝗥𝗦𝗧𝗨𝗩𝗪𝗫𝗬𝗭𝗮𝗯𝗰𝗱𝗲𝗳𝗴𝗵𝗶𝗷𝗸" \
"𝗹𝗺𝗻𝗼𝗽𝗾𝗿𝘀𝘁𝘂𝘃𝘄𝘅𝘆𝘇𝘈𝘉𝘊𝘋𝘌𝘍𝘎𝘏𝘐𝘑𝘒𝘓𝘔𝘕𝘖𝘗𝘘𝘙𝘚𝘛𝘜𝘝𝘞𝘟𝘠" \
"𝘡𝘢𝘣𝘤𝘥𝘦𝘧𝘨𝘩𝘪𝘫𝘬𝘭𝘮𝘯𝘰𝘱𝘲𝘳𝘴𝘵𝘶𝘷𝘸𝘹𝘺𝘻𝘼𝘽𝘾𝘿𝙀𝙁𝙂𝙃𝙄𝙅𝙆𝙇𝙈" \
"𝙉𝙊𝙋𝙌𝙍𝙎𝙏𝙐𝙑𝙒𝙓𝙔𝙕𝙖𝙗𝙘𝙙𝙚𝙛𝙜𝙝𝙞𝙟𝙠𝙡𝙢𝙣𝙤𝙥𝙦𝙧𝙨𝙩𝙪𝙫𝙬𝙭𝙮𝙯𝙰" \
"𝙱𝙲𝙳𝙴𝙵𝙶𝙷𝙸𝙹𝙺𝙻𝙼𝙽𝙾𝙿𝚀𝚁𝚂𝚃𝚄𝚅𝚆𝚇𝚈𝚉𝚊𝚋𝚌𝚍𝚎𝚏𝚐𝚑𝚒𝚓𝚔𝚕𝚖𝚗𝚘" \
"𝚙𝚚𝚛𝚜𝚝𝚞𝚟𝚠𝚡𝚢𝚣𝚤𝚥𝚨𝚩𝚪𝚫𝚬𝚭𝚮𝚯𝚰𝚱𝚲𝚳𝚴𝚵𝚶𝚷𝚸𝚹𝚺𝚻𝚼𝚽𝚾𝚿𝛀𝛁𝛂" \
"𝛃𝛄𝛅𝛆𝛇𝛈𝛉𝛊𝛋𝛌𝛍𝛎𝛏𝛐𝛑𝛒𝛓𝛔𝛕𝛖𝛗𝛘𝛙𝛚𝛛𝛜𝛝𝛞𝛟𝛠𝛡𝛢𝛣𝛤𝛥𝛦𝛧𝛨𝛩𝛪" \
"𝛫𝛬𝛭𝛮𝛯𝛰𝛱𝛲𝛳𝛴𝛵𝛶𝛷𝛸𝛹𝛺𝛻𝛼𝛽𝛾𝛿𝜀𝜁𝜂𝜃𝜄𝜅𝜆𝜇𝜈𝜉𝜊𝜋𝜌𝜍𝜎𝜏𝜐𝜑𝜒" \
"𝜓𝜔𝜕𝜖𝜗𝜘𝜙𝜚𝜛𝜜𝜝𝜞𝜟𝜠𝜡𝜢𝜣𝜤𝜥𝜦𝜧𝜨𝜩𝜪𝜫𝜬𝜭𝜮𝜯𝜰𝜱𝜲𝜳𝜴𝜵𝜶𝜷𝜸𝜹𝜺" \
"𝜻𝜼𝜽𝜾𝜿𝝀𝝁𝝂𝝃𝝄𝝅𝝆𝝇𝝈𝝉𝝊𝝋𝝌𝝍𝝎𝝏𝝐𝝑𝝒𝝓𝝔𝝕𝝖𝝗𝝘𝝙𝝚𝝛𝝜𝝝𝝞𝝟𝝠𝝡𝝢" \
"𝝣𝝤𝝥𝝦𝝧𝝨𝝩𝝪𝝫𝝬𝝭𝝮𝝯𝝰𝝱𝝲𝝳𝝴𝝵𝝶𝝷𝝸𝝹𝝺𝝻𝝼𝝽𝝾𝝿𝞀𝞁𝞂𝞃𝞄𝞅𝞆𝞇𝞈𝞉𝞊" \
"𝞋𝞌𝞍𝞎𝞏𝞐𝞑𝞒𝞓𝞔𝞕𝞖𝞗𝞘𝞙𝞚𝞛𝞜𝞝𝞞𝞟𝞠𝞡𝞢𝞣𝞤𝞥𝞦𝞧𝞨𝞩𝞪𝞫𝞬𝞭𝞮𝞯𝞰𝞱𝞲" \
"𝞳𝞴𝞵𝞶𝞷𝞸𝞹𝞺𝞻𝞼𝞽𝞾𝞿𝟀𝟁𝟂𝟃𝟄𝟅𝟆𝟇𝟈𝟉𝟊𝟋𝟎𝟏𝟐𝟑𝟒𝟓𝟔𝟕𝟖𝟗𝟘𝟙𝟚𝟛𝟜" \
"𝟝𝟞𝟟𝟠𝟡𝟢𝟣𝟤𝟥𝟦𝟧𝟨𝟩𝟪𝟫𝟬𝟭𝟮𝟯𝟰𝟱𝟲𝟳𝟴𝟵𝟶𝟷𝟸𝟹𝟺𝟻𝟼𝟽𝟾𝟿𝐮𝒖𝓾𝕦𝑢" \
"𝓊𝔳𝐯𝒗𝓿𝕧𝑣𝓋𝔴𝐰𝒘𝔀𝕨𝑤𝓌𝔵𝐱𝒙𝔁𝕩𝑥𝓍𝔶𝐲𝒚𝔂𝕪𝑦𝓎𝔷𝐳𝒛𝔃𝕫𝑧𝓏"
;
#endif

// Arabic Mathematical Alphabetic Symbols
#ifndef _LA_ICO_ARABIC_MATHEMATICAL_ALPHABETIC_SYMBOLS
#define _LA_ICO_ARABIC_MATHEMATICAL_ALPHABETIC_SYMBOLS \
"𞻱𞻰𞸏𞸀𞸁𞸜𞸙𞸃𞺯𞺡𞺹𞺣𞺰𞺻𞺧𞺢𞺷𞺫𞺬𞺭𞺲𞺳𞺱𞺮𞺴𞺨𞺵𞺸𞺶𞺥𞺩𞺺𞺦𞸐𞸞𞸛𞸇𞸯𞸡𞸹" \
"𞸰𞸻𞸧𞸤𞸢𞸪𞸷𞸫𞸬𞸭𞸲𞸱𞸮𞸴𞸵𞸶𞸩𞸂𞸊𞸗𞸋𞺏𞺀𞺁𞺙𞺃𞺐𞺛𞺇𞺄𞺂𞺗𞺋𞺌𞺍𞺒𞺓𞺑𞺎𞺔" \
"𞺈𞺕𞺘𞺖𞺅𞺉𞺚𞺆𞸌𞸍𞸝𞸒𞸟𞸓𞸑𞸎𞸔𞹯𞹡𞹹𞹼𞹾𞹰𞹻𞹧𞹤𞹢𞹪𞹷𞹬𞹭𞹲𞹱𞹮𞹴𞹨𞹵𞹶𞹩𞹺" \
"𞸈𞹏𞹙𞹝𞹟𞹛𞹇𞹂𞹗𞹋𞹍𞹒𞹑𞹎𞹔𞹉𞸕𞸘𞸖𞸅𞸉𞸚𞸆"
;
#endif

// Mahjong Tiles
#ifndef _LA_ICO_MAHJONG_TILES
#define _LA_ICO_MAHJONG_TILES \
"🀀🀁🀂🀃🀄🀅🀆🀇🀈🀉🀊🀋🀌🀍🀎🀏🀐🀑🀒🀓🀔🀕🀖🀗🀘🀙🀚🀛🀜🀝🀞🀟🀠🀡🀢🀣🀤🀥🀦🀧" \
"🀨🀩🀪🀫🀄"
;
#endif

// Domino Tiles
#ifndef _LA_ICO_DOMINO_TILES
#define _LA_ICO_DOMINO_TILES \
"🀰🀱🀲🀳🀴🀵🀶🀷🀸🀹🀺🀻🀼🀽🀾🀿🁀🁁🁂🁃🁄🁅🁆🁇🁈🁉🁊🁋🁌🁍🁎🁏🁐🁑🁒🁓🁔🁕🁖🁗" \
"🁘🁙🁚🁛🁜🁝🁞🁟🁠🁡🁢🁣🁤🁥🁦🁧🁨🁩🁪🁫🁬🁭🁮🁯🁰🁱🁲🁳🁴🁵🁶🁷🁸🁹🁺🁻🁼🁽🁾🁿" \
"🂀🂁🂂🂃🂄🂅🂆🂇🂈🂉🂊🂋🂌🂍🂎🂏🂐🂑🂒🂓"
;
#endif

// Playing Cards
#ifndef _LA_ICO_PLAYING_CARDS
#define _LA_ICO_PLAYING_CARDS \
"🂠🂡🂢🂣🂤🂥🂦🂧🂨🂩🂪🂫🂬🂭🂮🂱🂲🂳🂴🂵🂶🂷🂸🂹🂺🂻🂼🂽🂾🂿🃁🃂🃃🃄🃅🃆🃇🃈🃉🃊" \
"🃋🃌🃍🃎🃏🃑🃒🃓🃔🃕🃖🃗🃘🃙🃚🃛🃜🃝🃞🃟🃠🃡🃢🃣🃤🃥🃦🃧🃨🃩🃪🃫🃬🃭🃮🃯🃰🃱🃲🃳" \
"🃴🃵🃏"
;
#endif

// Enclosed Alphanumeric Supplement
#ifndef _LA_ICO_ENCLOSED_ALPHANUMERIC_SUPPLEMENT
#define _LA_ICO_ENCLOSED_ALPHANUMERIC_SUPPLEMENT \
"🅐🄰🄐🅰🅑🄱🄑🅱🅒🄲🄒🅲🅓🄳🄓🅳🅔🄴🄔🅴🅕🄵🄕🅵🅖🄶🄖🅶🅗🄷🄗🅷🅊🅘🄸🄘🅸🅙🄹🄙" \
"🅹🅚🄺🄚🅺🅛🄻🄛🅻🅜🄼🄜🅼🅋🅝🄽🄝🅽🅞🄾🄞🅾🅟🄿🄟🅿🅎🅠🅀🄠🆀🅡🅁🄡🆁🅢🅂🄢🆂🅌" \
"🅍🅣🅃🄣🆃🅤🅄🄤🆄🅥🅅🄥🆅🅦🅆🄦🆆🅏🅧🅇🄧🆇🅨🅈🄨🆈🅩🅉🄩🆉🄯🄉🄆🄅🄊🄂🅪🅫🅬🄈" \
"🄇🄄🄃🄪🄫🄬🄭🄮🆊🆋🆌🆍🆎🆏🆐🆛🆜🆝🆞🆟🆠🆡🆢🆣🆤🆥🆦🆧🆨🆩🆪🆫🆬🄌🄋🄁🄀🅰🅱🅾" \
"🅿🆎🆑🆒🆓🆔🆕🆖🆗🆘🆙🆚🇦🇧🇨🇩🇪🇫🇬🇭🇮🇯🇰🇱🇲🇳🇴🇵🇶🇷🇸🇹🇺🇻🇼🇽🇾🇿"
;
#endif

// Enclosed Ideographic Supplement
#ifndef _LA_ICO_ENCLOSED_IDEOGRAPHIC_SUPPLEMENT
#define _LA_ICO_ENCLOSED_IDEOGRAPHIC_SUPPLEMENT \
"🈁🈂🈚🈯🈲🈳🈴🈵🈶🈷🈸🈹🈺🉐🉑"
;
#endif

// Miscellaneous Symbols and Pictographs
#ifndef _LA_ICO_MISCELLANEOUS_SYMBOLS_AND_PICTOGRAPHS
#define _LA_ICO_MISCELLANEOUS_SYMBOLS_AND_PICTOGRAPHS \
"🕈🕇🕉🕏🕆🎟🖂🗶🗳🗹🗷🗵🗴📊🏖🎜🎝🐦🖣🌢🖿🖪🖜🏲🖈🖝🏶🕱🕿🖢💣🕮📚🎕🏗🕫🕬📷🏕🗙" \
"🕯🗃🗂🐈📉📈💹🐿🏙🖁🎬🏛📋🕗🕣🕚🕦🕔🕠🕓🕟🕘🕤🕐🕜🕖🕢🕕🕡🕙🕥🕒🕞🕛🕧🕑🕝🗘📪📫" \
"🌩🌧🌨🌪🍸🗜🎛💳🕂🕁🗡🕶🗛🏚🏜🏝🖥🗔🗎🖻🖹🖺🐕🕊👂🌎🌏🌍🗋🗅🗇🗆🗌🗍🖄👽👁👓🏭👪" \
"🖷🗄🎞📽🐟🖅🌫🗀🍽🖾🖼🖽🌕🏌🎓🖴🎧🎔🕳🌶🏠🏘📥🗚🕹🖦🏷🌜🗮🕻🔍🗨🗬🖎🎚🗸🗲🗱🖇🗢" \
"🔒🖊🖍🖋🖌🖉🔾🕴🕰🗖🎖🗕💰🗰🎘🏞🕲🕃🕄🗈🗊🗉👌🗝🖳🖯🗁🔓📭📬💿🖸📤🗗📦🗏🗟📟🗐🖆" \
"🎭💻🖩📾🖨🖶🏎🏍📻🖐🎗🖑🖓🖒🖔🗯🕽🕨🕩🕪🗩🗭🕭🗞🏵🖵🖡🖚🖛🖠🖟🖘🖙🖞🕵🏔🏂🖬🔈🔇" \
"🔉🔊🗣🕷🕸🗓🗒🏟🖃🗠🎙🏄🏊🕅🖭🖀🕼📺🌡🖱🖧🗤🗥🗦🗧🗫👎👍🖲🏆🖏🖰🗪🔿📹🎮🗑🏳🏋👇" \
"🖗🖫👈🏱👉🌣🌥🌦🕾👆🌤🌬🖮🗺🕀🔃🌀🌁🌂🌃🌄🌅🌆🌇🌈🌉🌊🌋🌌🌍🌎🌏🌐🌑🌒🌓🌔🌕🌖🌗" \
"🌘🌙🌚🌛🌜🌝🌞🌟🌠🌡🌤🌥🌦🌧🌨🌩🌪🌫🌬🌭🌮🌯🌰🌱🌲🌳🌴🌵🌶🌷🌸🌹🌺🌻🌼🌽🌾🌿🍀🍁" \
"🍂🍃🍄🍅🍆🍇🍈🍉🍊🍋🍌🍍🍎🍏🍐🍑🍒🍓🍔🍕🍖🍗🍘🍙🍚🍛🍜🍝🍞🍟🍠🍡🍢🍣🍤🍥🍦🍧🍨🍩" \
"🍪🍫🍬🍭🍮🍯🍰🍱🍲🍳🍴🍵🍶🍷🍸🍹🍺🍻🍼🍽🍾🍿🎀🎁🎂🎃🎄🎅🎆🎇🎈🎉🎊🎋🎌🎍🎎🎏🎐🎑" \
"🎒🎓🎖🎗🎙🎚🎛🎞🎟🎠🎡🎢🎣🎤🎥🎦🎧🎨🎩🎪🎫🎬🎭🎮🎯🎰🎱🎲🎳🎴🎵🎶🎷🎸🎹🎺🎻🎼🎽🎾" \
"🎿🏀🏁🏂🏃🏄🏅🏆🏇🏈🏉🏊🏋🏌🏍🏎🏏🏐🏑🏒🏓🏔🏕🏖🏗🏘🏙🏚🏛🏜🏝🏞🏟🏠🏡🏢🏣🏤🏥🏦" \
"🏧🏨🏩🏪🏫🏬🏭🏮🏯🏰🏳🏴🏵🏷🏸🏹🏺🏻🏼🏽🏾🏿🐀🐁🐂🐃🐄🐅🐆🐇🐈🐉🐊🐋🐌🐍🐎🐏🐐🐑" \
"🐒🐓🐔🐕🐖🐗🐘🐙🐚🐛🐜🐝🐞🐟🐠🐡🐢🐣🐤🐥🐦🐧🐨🐩🐪🐫🐬🐭🐮🐯🐰🐱🐲🐳🐴🐵🐶🐷🐸🐹" \
"🐺🐻🐼🐽🐾🐿👀👁👂👃👄👅👆👇👈👉👊👋👌👍👎👏👐👑👒👓👔👕👖👗👘👙👚👛👜👝👞👟👠👡" \
"👢👣👤👥👦👧👨👩👪👫👬👭👮👯👰👱👲👳👴👵👶👷👸👹👺👻👼👽👾👿💀💁💂💃💄💅💆💇💈💉" \
"💊💋💌💍💎💏💐💑💒💓💔💕💖💗💘💙💚💛💜💝💞💟💠💡💢💣💤💥💦💧💨💩💪💫💬💭💮💯💰💱" \
"💲💳💴💵💶💷💸💹💺💻💼💽💾💿📀📁📂📃📄📅📆📇📈📉📊📋📌📍📎📏📐📑📒📓📔📕📖📗📘📙" \
"📚📛📜📝📞📟📠📡📢📣📤📥📦📧📨📩📪📫📬📭📮📯📰📱📲📳📴📵📶📷📸📹📺📻📼📽📿🔀🔁🔂" \
"🔃🔄🔅🔆🔇🔈🔉🔊🔋🔌🔍🔎🔏🔐🔑🔒🔓🔔🔕🔖🔗🔘🔙🔚🔛🔜🔝🔞🔠🔡🔢🔣🔤🔥🔦🔧🔨🔩🔪🔫" \
"🔬🔭🔮🔯🔰🔱🔲🔳🔴🔵🔶🔷🔸🔹🔺🔻🔼🔽🕉🕊🕋🕌🕍🕎🕐🕑🕒🕓🕔🕕🕖🕗🕘🕙🕚🕛🕜🕝🕞🕟" \
"🕠🕡🕢🕣🕤🕥🕦🕧🕯🕰🕳🕴🕵🕶🕷🕸🕹🕺🖇🖊🖋🖌🖍🖐🖕🖖🖤🖥🖨🖱🖲🖼🗂🗃🗄🗑🗒🗓🗜🗝" \
"🗞🗡🗣🗨🗯🗳🗺🗻🗼🗽🗾🗿🔟"
;
#endif

// Emoticons
#ifndef _LA_ICO_EMOTICONS
#define _LA_ICO_EMOTICONS \
"😐😀😁😂😃😄😅😆😇😈😉😊😋😌😍😎😏😐😑😒😓😔😕😖😗😘😙😚😛😜😝😞😟😠😡😢😣😤😥😦" \
"😧😨😩😪😫😬😭😮😯😰😱😲😳😴😵😶😷😸😹😺😻😼😽😾😿🙀🙁🙂🙃🙄🙅🙆🙇🙈🙉🙊🙋🙌🙍🙎" \
"🙏"
;
#endif

// Ornamental Dingbats
#ifndef _LA_ICO_ORNAMENTAL_DINGBATS
#define _LA_ICO_ORNAMENTAL_DINGBATS \
"🙾🙯🙴🙹🙳🙦🙞🙤🙜🙻🙱🙧🙟🙥🙝🙨🙩🙬🙲🙢🙒🙚🙠🙐🙘🙿🙮🙷🙶🙸🙺🙰🙪🙫🙣🙓🙛🙡🙑🙙" \
"🙵🙖🙔🙗🙕🙭🙽🙼"
;
#endif

// Transport and Map Symbols
#ifndef _LA_ICO_TRANSPORT_AND_MAP_SYMBOLS
#define _LA_ICO_TRANSPORT_AND_MAP_SYMBOLS \
"🛩🚑🚼🛏🛎🚲🛉🛈🛋🛲🛊🛠🚹🚇🛥🛣🚭🛪🛢🚘🚍🛱🚔🛳🛇🛤🛰🛡🛍🛆🛧🛦🛨🚺🛓🛔🛕🛖🛗🛷" \
"🛸🛹🛺🛻🛼🚀🚁🚂🚃🚄🚅🚆🚇🚈🚉🚊🚋🚌🚍🚎🚏🚐🚑🚒🚓🚔🚕🚖🚗🚘🚙🚚🚛🚜🚝🚞🚟🚠🚡🚢" \
"🚣🚤🚥🚦🚧🚨🚩🚪🚫🚬🚭🚮🚯🚰🚱🚲🚳🚴🚵🚶🚷🚸🚹🚺🚻🚼🚽🚾🚿🛀🛁🛂🛃🛄🛜🛅🛋🛌🛍🛎" \
"🛏🛐🛑🛒🛕🛖🛗🛝🛞🛟🛠🛡🛢🛣🛤🛥🛩🛫🛬🛰🛳🛴🛵🛶🛷🛸🛹🛺🛻🛼"
;
#endif

// Alchemical Symbols
#ifndef _LA_ICO_ALCHEMICAL_SYMBOLS
#define _LA_ICO_ALCHEMICAL_SYMBOLS \
"🜁🝪🜶🜷🝅🝛🜫🜆🜇🜈🜉🜅🜺🝗🜽🝫🝬🜾🜏🝂🝃🝄🝙🝐🝌🝎🜓🜥🜠🜣🜤🜞🝥🝦🝧🝨🝩🝰🝡🝢" \
"🝠🜃🜂🜚🝉🝲🝳🝖🝮🜡🜜🜝🜪🝓🜸🜐🜑🜒🝱🝯🜕🝆🜎🝘🝋🝚🝟🝣🝤🝁🜀🜻🜼🜲🜳🜴🜵🜰🜱🜟" \
"🝭🜘🜙🜹🜔🜭🜦🝏🜛🝔🝇🝒🝜🝝🜬🜢🜮🜧🝞🜍🜿🝀🜩🝈🝑🝍🝕🜨🜊🜋🜌🜯🜖🜗🜄🝊"
;
#endif

// Geometric Shapes Extended
#ifndef _LA_ICO_GEOMETRIC_SHAPES_EXTENDED
#define _LA_ICO_GEOMETRIC_SHAPES_EXTENDED \
"🞃🞀🞙🞟🞂🞄🞍🞗🞝🞌🞁🞘🞞🞽🞱🞤🞫🞷🞆🞐🞜🞴🞧🞮🞺🞉🞓🟆🟏🟑🞾🟊🞲🞬🟌🞸🟓🟔🞇🞑" \
"🞻🟉🞯🟄🞢🞩🞵🟀🟒🞎🞅🟎🞼🞰🟅🟇🞣🞪🟋🞶🟁🟃🞏🟈🞋🟍🞖🞡🞨🟂🞥🟐🞿🞳🞦🞭🞹🞈🞒🞊" \
"🞛🞚🞠🞕🞔🟕🟖🟗🟘🟠🟡🟢🟣🟤🟥🟦🟧🟨🟩🟪🟫🟠🟡🟢🟣🟤🟥🟦🟧🟨🟩🟪🟫🟰"
;
#endif

// Supplemental Arrows-C
#ifndef _LA_ICO_SUPPLEMENTAL_ARROWS_C
#define _LA_ICO_SUPPLEMENTAL_ARROWS_C \
"🠗🠋🠇🢛🠃🠿🠷🡇🡃🡓🠻🠫🠯🠧🠣🠳🢓🢗🠓🢜🢞🢟🢝🠛🠟🠘🠜🠚🠞🠙🠝🠔🠈🠄🢘🠀🢨🢠🠼🠴" \
"🢪🡄🡀🢤🡘🢦🡐🠸🢢🠨🠬🠤🠠🠰🢐🢔🠐🡕🡔🠖🠊🠆🢚🠂🢩🢡🠾🠶🢫🡆🡂🢧🢥🡒🠺🢣🠪🠮🠦🠢" \
"🠲🢒🢖🠒🡖🡗🠕🠉🠅🢙🠁🠽🡙🠵🡅🡁🡑🠹🠩🠭🠥🠡🠱🢑🢕🠑🢬🢭🡫🡻🡣🡳🢃🡨🡸🡠🡰🢀🡭🡽" \
"🡥🡵🢅🡬🡼🡤🡴🢄🡪🡺🡢🡲🢂🡮🡾🡦🡶🢆🡯🡿🡧🡷🢇🡩🡹🡡🡱🢁🢰🢱"
;
#endif

// Supplemental Symbols and Pictographs
#ifndef _LA_ICO_SUPPLEMENTAL_SYMBOLS_AND_PICTOGRAPHS
#define _LA_ICO_SUPPLEMENTAL_SYMBOLS_AND_PICTOGRAPHS \
"🤻🥆🤌🤍🤎🤏🤐🤑🤒🤓🤔🤕🤖🤗🤘🤙🤚🤛🤜🤝🤞🤟🤠🤡🤢🤣🤤🤥🤦🤧🤨🤩🤪🤫🤬🤭🤮🤯🤰🤱" \
"🤲🤳🤴🤵🤶🤷🤸🤹🤺🤼🤽🤾🤿🥀🥁🥂🥃🥄🥅🥇🥈🥉🥊🥋🥌🥍🥎🥏🥐🥑🥒🥓🥔🥕🥖🥗🥘🥙🥚🥛" \
"🥜🥝🥞🥟🥠🥡🥢🥣🥤🥥🥦🥧🥨🥩🥪🥫🥬🥭🥮🥯🥰🥱🥲🥳🥴🥵🥶🥷🥸🥹🥺🥻🥼🥽🥾🥿🦀🦁🦂🦃" \
"🦄🦅🦆🦇🦈🦉🦊🦋🦌🦍🦎🦏🦐🦑🦒🦓🦔🦕🦖🦗🦘🦙🦚🦛🦜🦝🦞🦟🦠🦡🦢🦣🦤🦥🦦🦧🦨🦩🦪🦫" \
"🦬🦭🦮🦯🦰🦱🦲🦳🦴🦵🦶🦷🦸🦹🦺🦻🦼🦽🦾🦿🧀🧁🧂🧃🧄🧅🧆🧇🧈🧉🧊🧋🧌🧍🧎🧏🧐🧑🧒🧓" \
"🧔🧕🧖🧗🧘🧙🧚🧛🧜🧝🧞🧟🧠🧡🧢🧣🧤🧥🧦🧧🧨🧩🧪🧫🧬🧭🧮🧯🧰🧱🧲🧳🧴🧵🧶🧷🧸🧹🧺🧻" \
"🧼🧽🧾🧿"
;
#endif

// Chess Symbols
#ifndef _LA_ICO_CHESS_SYMBOLS
#define _LA_ICO_CHESS_SYMBOLS \
"🨀🨁🨂🨃🨄🨅🨆🨇🨈🨉🨊🨋🨌🨍🨎🨏🨐🨑🨒🨓🨔🨕🨖🨗🨘🨙🨚🨛🨜🨝🨞🨟🨠🨡🨢🨣🨤🨥🨦🨧" \
"🨨🨩🨪🨫🨬🨭🨮🨯🨰🨱🨲🨳🨴🨵🨶🨷🨸🨹🨺🨻🨼🨽🨾🨿🩀🩁🩂🩃🩄🩅🩆🩇🩈🩉🩊🩋🩌🩍🩎🩏" \
"🩐🩑🩒🩓🩠🩡🩢🩣🩤🩥🩦🩧🩨🩩🩪🩫🩬🩭"
;
#endif

// Symbols and Pictographs Extended-A
#ifndef _LA_ICO_SYMBOLS_AND_PICTOGRAPHS_EXTENDED_A
#define _LA_ICO_SYMBOLS_AND_PICTOGRAPHS_EXTENDED_A \
"🩰🩱🩲🩳🩴🩸🩹🩺🪀🪁🪂🪃🪄🪅🪆🪐🪑🪒🪓🪔🪕🪖🪗🪘🪙🪚🪛🪜🪝🪞🪟🪠🪡🪢🪣🪤🪥🪦🪧🪨" \
"🪰🪱🪲🪳🪴🪵🪶🫀🫁🫂🫐🫑🫒🫓🫔🫕🫖🪻🪇🪈🪼🫏🪿🩶🩷🩵🪯🫚🫛🫎🪽🪭🩰🩱🩲🩳🩴🩸🩹🩺" \
"🩻🩼🪀🪁🪂🪃🪄🪅🪆🪐🪑🪒🪮🪓🪔🪕🪖🪗🪘🪙🪚🪛🪜🪝🪞🪟🪠🪡🪢🪣🪤🪥🪦🪧🪨🪩🪪🪫🪬🪰" \
"🪱🪲🪳🪴🪵🪶🪷🪸🪹🪺🫀🫁🫂🫃🫄🫅🫐🫑🫒🫓🫔🫕🫖🫗🫘🫙🫠🫡🫢🫣🫤🫥🫨🫦🫧🫰🫱🫲🫳🫴" \
"🫵🫶🫸🫷"
;
#endif

// Symbols for Legacy Computing
#ifndef _LA_ICO_SYMBOLS_FOR_LEGACY_COMPUTING
#define _LA_ICO_SYMBOLS_FOR_LEGACY_COMPUTING \
"🬀🬁🬂🬃🬄🬅🬆🬇🬈🬉🬊🬋🬌🬍🬎🬏🬐🬑🬒🬓🬔🬕🬖🬗🬘🬙🬚🬛🬜🬝🬞🬟🬠🬡🬢🬣🬤🬥🬦🬧" \
"🬨🬩🬪🬫🬬🬭🬮🬯🬰🬱🬲🬳🬴🬵🬶🬷🬸🬹🬺🬻🬼🬽🬾🬿🭀🭁🭂🭃🭄🭅🭆🭇🭈🭉🭊🭋🭌🭍🭎🭏" \
"🭐🭑🭒🭓🭔🭕🭖🭗🭘🭙🭚🭛🭜🭝🭞🭟🭠🭡🭢🭣🭤🭥🭦🭧🭨🭩🭪🭫🭬🭭🭮🭯🭰🭱🭲🭳🭴🭵🭶🭷" \
"🭸🭹🭺🭻🭼🭽🭾🭿🮀🮁🮂🮃🮄🮅🮆🮇🮈🮉🮊🮋🮌🮍🮎🮏🮐🮑🮒🮔🮕🮖🮗🮘🮙🮚🮛🮜🮝🮞🮟🮠" \
"🮡🮢🮣🮤🮥🮦🮧🮨🮩🮪🮫🮬🮭🮮🮯🮰🮱🮲🮳🮴🮵🮶🮷🮸🮹🮺🮻🮼🮽🮾🮿🯀🯁🯂🯃🯄🯅🯆🯇🯈" \
"🯉🯊🯰🯱🯲🯳🯴🯵🯶🯷🯸🯹"
;
#endif

// Tags
#ifndef _LA_ICO_TAGS
#define _LA_ICO_TAGS \
"󠀰󠀱󠀲󠀳󠀴󠀵󠀶󠀷󠀸󠀹󠁡󠁢󠁣󠁤󠁥󠁦󠁧󠁨󠁩󠁪󠁫󠁬󠁭󠁮󠁯󠁰󠁱󠁲󠁳󠁴󠁵󠁶󠁷󠁸󠁹󠁺󠁿"
;
#endif

// Supplementary Private Use Area-A
#ifndef _LA_ICO_SUPPLEMENTARY_PRIVATE_USE_AREA_A
#define _LA_ICO_SUPPLEMENTARY_PRIVATE_USE_AREA_A \
"󾠬󾠮󾠯󾠰󾠱󾠲󾠳󾠴󾠵󾠶󾠷󾓭󾓨󾓫󾓧󾓩󾓥󾓮󾓬󾓪󾓦"
;
#endif

// Generated 7470 characters
// Glyphs with no names: 0
// 