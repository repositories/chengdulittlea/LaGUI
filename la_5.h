/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#define _BSD_SOURCE  1
#define _SVID_SOURCE 1

#define BYTE uint8_t

#ifdef __cplusplus
    extern "C" {
#endif

#include "la_util.h"

#include "la_interface.h"

#include "la_data.h"
//#include "la_icon.h"
#include "la_tns.h"

#ifdef __cplusplus
    }
#endif

#include "math.h"
//#include "LA_AV.h"
//#include <direct.h>
//#include <io.h>
#include <stddef.h> //offsetof
#include <stdio.h>
#include <threads.h>

#ifdef __linux__
#include <unistd.h>
#include <dirent.h>
#include <mntent.h>
#include <sys/statvfs.h>
#endif

#include <locale.h>
#include <sys/stat.h>
#include <time.h>

#if defined (LAGUI_ANDROID) || defined (LA_USE_GLES)
#define NANOVG_GLES3
#else
#define NANOVG_GL3
#endif
#include "nanovg.h"
#include "nanovg_gl.h"
