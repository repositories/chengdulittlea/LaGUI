/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifdef LAGUI_ANDROID
    #include <GLES3/gl32.h>
    #include <EGL/egl.h>
    #include <android/input.h>
    #include <android/keycodes.h> 
#else
    #ifdef LA_USE_GLES
    #include <EGL/egl.h>
    #include <GL/gl.h>
    #else
    #include <GL/glew.h>
    #include <GL/gl.h>
    #endif
#endif

#ifdef __linux__
#ifndef LAGUI_ANDROID
    #include <X11/Xlib.h>
    #include <X11/Xutil.h>
    #include <X11/Xresource.h>
    #include <X11/Xlocale.h>
    #include <X11/XKBlib.h>
    #include <X11/Xatom.h>
    #include <GL/glx.h>
    #include <GL/glext.h>
#endif
#endif
#ifdef _WIN32
#include <GL/wglew.h>
#include <shlobj_core.h>
#endif

#include <threads.h>
#include "la_5.h"
#include "la_data.h"
//#include "llvm-c/Analysis.h"
//#include "llvm-c/BitWriter.h"
//#include "llvm-c/Core.h"
//#include "llvm-c/ExecutionEngine.h"
//#include "llvm-c/Target.h"
//#include <Windows.h>

#define LA_VERSION_MAIN 5
#define LA_VERSION_SUB 3

#define LA_ACTUATOR_CAPABILITY_MAIN 5
#define LA_ACTUATOR_CAPABILITY_SUB 0

#define LA_NODE_MAIN_MODULE_NAME "LA_NODE_MAIN_MODULE"

#define LA_GUI_WNDCLASS_NAME "LA_WINDOW_CLASS"
#define LA_ARROW 0
#define LA_CROSS 1
#define LA_LEFT_AND_RIGHT 2
#define LA_UP_AND_DOWN 3
#define LA_HAND 4
#define LA_MOVE 5
#define LA_CORNER 6

#define EVT_WND_MOVE -1
#define EVT_WND_SIZE_BEGIN -2
#define EVT_WND_SIZE_FINISH -3
#define EVT_WND_SIZE -4
#define EVT_WND_SHOW -5

#define EVT_FEEDBACK 8192

#define LA_KEYBOARD_EVENT (1<<0)
#define LA_MOUSE_EVENT    (1<<1)
#define LA_STATE_MOVE     (1<<2)
#define LA_STATE_DOWN     (1<<3)
#define LA_STATE_UP       (1<<4)

#define LA_TIME_EVENT     (1<<5)
#define LA_TIME_IDLE      (LA_TIME_EVENT|(1<<6))
#define LA_TIME_DELAY     (LA_TIME_EVENT|(1<<7))

#define LA_KEY_MOUSE_LEFT   (1<<8)
#define LA_KEY_MOUSE_MIDDLE (1<<9)
#define LA_KEY_MOUSE_RIGHT  (1<<10)
#define LA_KEY_MOUSE_SCROLL (1<<11)

#define LA_KEY_BACKSPACE (1<<12)
#define LA_KEY_ESCAPE    (1<<13)
#define LA_KEY_ENTER     (1<<14)
#define LA_INPUT         (1<<15)
#define LA_KEY_ARRLEFT   (1<<16)
#define LA_KEY_ARRRIGHT  (1<<17)
#define LA_KEY_ARRUP     (1<<18)
#define LA_KEY_ARRDOWN   (1<<19)
#define LA_KEY_SHIFT     (1<<20)
#define LA_KEY_CTRL      (1<<21)
#define LA_KEY_ALT       (1<<22)
#define LA_KEY_DELETE    (1<<23)
#define LA_KEY_TAB       (1<<24)

#define LA_KEY_EXTRA     (1<<25)
#define LA_KEY_NUM1      (LA_KEY_EXTRA+1)
#define LA_KEY_NUM2      (LA_KEY_EXTRA+2)
#define LA_KEY_NUM3      (LA_KEY_EXTRA+3)
#define LA_KEY_NUM4      (LA_KEY_EXTRA+4)
#define LA_KEY_NUM5      (LA_KEY_EXTRA+5)
#define LA_KEY_NUM6      (LA_KEY_EXTRA+6)
#define LA_KEY_NUM7      (LA_KEY_EXTRA+7)
#define LA_KEY_NUM8      (LA_KEY_EXTRA+8)
#define LA_KEY_NUM9      (LA_KEY_EXTRA+9)
#define LA_KEY_NUM0      (LA_KEY_EXTRA+10)
#define LA_KEY_NUMPLUS   (LA_KEY_EXTRA+11)
#define LA_KEY_NUMMINUS  (LA_KEY_EXTRA+12)
#define LA_KEY_NUMMULT   (LA_KEY_EXTRA+13)
#define LA_KEY_NUMDIVIDE (LA_KEY_EXTRA+14)
#define LA_KEY_NUMDOT    (LA_KEY_EXTRA+15)
#define LA_KEY_NUMENTER  (LA_KEY_EXTRA+16)

#define LA_KEY_PANNING    (1<<26)

#define LA_PASTE    (1<<29)
#define LA_OPERATOR_EVENT (1<<30)
#define LA_SIGNAL_EVENT   (1<<31)

#ifdef _WIN32
#define XK_F1  1
#define XK_F2  2
#define XK_F3  3
#define XK_F4  4
#define XK_F5  5
#define XK_F6  6
#define XK_F7  7
#define XK_F8  8
#define XK_F9  9
#define XK_F10 10
#define XK_F11 11
#define XK_F12 12
#endif
#if defined(LA_LINUX) || defined(_WIN32)
#define LA_KEY_F1        XK_F1
#define LA_KEY_F2        XK_F2
#define LA_KEY_F3        XK_F3
#define LA_KEY_F4        XK_F4
#define LA_KEY_F5        XK_F5
#define LA_KEY_F6        XK_F6
#define LA_KEY_F7        XK_F7
#define LA_KEY_F8        XK_F8
#define LA_KEY_F9        XK_F9
#define LA_KEY_F10       XK_F10
#define LA_KEY_F11       XK_F11
#define LA_KEY_F12       XK_F12
#endif
#ifdef LAGUI_ANDROID
#define LA_KEY_F1        AKEYCODE_F1
#define LA_KEY_F2        AKEYCODE_F2
#define LA_KEY_F3        AKEYCODE_F3
#define LA_KEY_F4        AKEYCODE_F4
#define LA_KEY_F5        AKEYCODE_F5
#define LA_KEY_F6        AKEYCODE_F6
#define LA_KEY_F7        AKEYCODE_F7
#define LA_KEY_F8        AKEYCODE_F8
#define LA_KEY_F9        AKEYCODE_F9
#define LA_KEY_F10       AKEYCODE_F10
#define LA_KEY_F11       AKEYCODE_F11
#define LA_KEY_F12       AKEYCODE_F12
#endif

#define LA_KEY_SPECIALS (LA_KEY_CTRL|LA_KEY_SHIFT|LA_KEY_ALT)

#define LA_MOUSEMOVE (LA_MOUSE_EVENT | LA_STATE_MOVE)
#define LA_EMPTY LA_MOUSEMOVE

#define LA_MOUSEDOWN (LA_MOUSE_EVENT | LA_STATE_DOWN)
#define LA_MOUSEUP (LA_MOUSE_EVENT | LA_STATE_UP)
#define LA_WHEEL (LA_MOUSE_EVENT | LA_KEY_MOUSE_SCROLL)

#define LA_L_MOUSE_DOWN (LA_MOUSE_EVENT | LA_STATE_DOWN | LA_KEY_MOUSE_LEFT)
#define LA_L_MOUSE_UP (LA_MOUSE_EVENT | LA_STATE_UP | LA_KEY_MOUSE_LEFT)
#define LA_M_MOUSE_DOWN (LA_MOUSE_EVENT | LA_STATE_DOWN | LA_KEY_MOUSE_MIDDLE)
#define LA_M_MOUSE_UP (LA_MOUSE_EVENT | LA_STATE_UP | LA_KEY_MOUSE_MIDDLE)
#define LA_R_MOUSE_DOWN (LA_MOUSE_EVENT | LA_STATE_DOWN | LA_KEY_MOUSE_RIGHT)
#define LA_R_MOUSE_UP (LA_MOUSE_EVENT | LA_STATE_UP | LA_KEY_MOUSE_RIGHT)
#define LA_MOUSE_WHEEL_DOWN (LA_MOUSE_EVENT | LA_STATE_DOWN | LA_KEY_MOUSE_SCROLL)
#define LA_MOUSE_WHEEL_UP (LA_MOUSE_EVENT | LA_STATE_UP | LA_KEY_MOUSE_SCROLL)
#define LA_KEY_DOWN (LA_KEYBOARD_EVENT | LA_STATE_DOWN)
#define LA_KEY_UP (LA_KEYBOARD_EVENT | LA_STATE_UP)
#define LA_PANNING_LEFT (LA_KEYBOARD_EVENT | LA_KEY_PANNING | LA_KEY_ARRLEFT)
#define LA_PANNING_RIGHT (LA_KEYBOARD_EVENT | LA_KEY_PANNING | LA_KEY_ARRRIGHT)
#define LA_PANNING_UP (LA_KEYBOARD_EVENT | LA_KEY_PANNING | LA_KEY_ARRUP)
#define LA_PANNING_DOWN (LA_KEYBOARD_EVENT | LA_KEY_PANNING | LA_KEY_ARRDOWN)

#define LA_ONLY_BITS_AND(orig, yours) \
    ((orig & yours) == yours)

NEED_STRUCTURE(laUiList);
NEED_STRUCTURE(laUiItem);
NEED_STRUCTURE(laColumn);

typedef void (*laUiDrawFunc)(void *, int);               //Instance,Theme
typedef void (*laPanelDrawFunc)(void *, void *, void *); //SYSWINDOW,Instance,Theme
//typedef void(*laUiDefineFunc)(laUiList*,laPropPack*,laPropPack*,laColumn*);//uil,collection inst+"This" inst,actuator inst,Extra column list.
typedef void (*laOperatorArgumentParserFunc)(laPropPack*, laStringSplitor *, uint32_t *, char *); //this,ss,icon id,display string buf
typedef int (*laUiGetHeightFunc)(laUiItem *);
typedef int (*laUiGetMinWidthFunc)(laUiItem *);
typedef int (*laUiConditionFunc)(laPropPack *); //maybe inst+actuator,replaceInst
typedef void (*laUiDestroyFunc)(laUiItem *);
typedef void (*laUiInitFunc)(laUiItem *);
typedef void (*laUiRefreshFunc)(laUiItem *);
typedef void (*laLayoutBkgDrawFunc)(void *, void *l);

typedef void (*laPreFrameF)();
typedef void (*laPreDrawF)();
typedef void (*laPostFrameF)();
typedef void (*laCleanupF)();

STRUCTURE(laEvent){
    laListItem Item;

    laWindow* window;

    int type;
    int state;
    int x, y;
    int key;
    int SpecialKeyBit;

    int p1, p2;
    uint32_t Input;

    int GoodPressure,IsEraser,HasTwist;
    real Pressure,Orientation,Deviation,Twist,Hover;

    void *Localized;
    laOperatorType* Operator; int OperatorBase; char* OperatorInstructions;
};

typedef int (*laInputProcessF)(laEvent* e);

NEED_STRUCTURE(laWindow)
NEED_STRUCTURE(laRoot)
NEED_STRUCTURE(laPanel)
NEED_STRUCTURE(laUiItem)
NEED_STRUCTURE(laUiList)
NEED_STRUCTURE(laAnimationAction)
NEED_STRUCTURE(laUiDescriptor)
NEED_STRUCTURE(laUiConditionNode)

STRUCTURE(laSharedTypeItem){
    laListItem Item;
    char ID[64];
    void *Pointer;
};

NEED_STRUCTURE(laTranslation);
NEED_STRUCTURE(laTheme);

STRUCTURE(laKeyMapper){
    laListItem Item;//unused;
    laListHandle Items;
};

STRUCTURE(laLogEntry){
    laListItem Item;
    laSafeString* Content;
    int Type;
};

NEED_STRUCTURE(laManagedUDF);

STRUCTURE(laNodeOutSocket){
    void* Data;
    void* Parent;
    laSafeString* Label;
    int DataType, ArrLen;
    int RuntimeX, RuntimeY, RuntimePX, RuntimePY;
    void* Duplicated;
};
STRUCTURE(laNodeInSocket){
    laNodeOutSocket* Source;
    laSafeString* Label;
    int DataType, ArrLen; int ColorId;
    int RuntimeX, RuntimeY, RuntimePX, RuntimePY;
};

NEED_STRUCTURE(laBaseNodeType);
NEED_STRUCTURE(laRackPage);
NEED_STRUCTURE(laNodeCategory);

STRUCTURE(laRackPageCollection){
    laRackPage* CurrentPage;
    laListHandle Pages;
};

typedef laBaseNodeType* (*laGetBaseNodeTypeF)(char* str);

NEED_STRUCTURE(laConfirmData);

STRUCTURE(laExtraPreferencePage){
    laListItem Item;
    const char* Name;
    laUiDefineFunc Func;
};

STRUCTURE(laProgressDisplay){
    char String[256];
    SYSWINDOW w;
#ifdef LA_LINUX
    GC gc;
    XGCValues gc_values;
#endif
#ifdef _WIN32
    HBRUSH brush_bg;
    HBRUSH brush_fg;
    HPEN pen_fg;
    HDC hdc;
    thrd_t th;
#endif
    laTimeRecorder TimeCalled;
    real p1,p2;
    int Called,Shown;
};

STRUCTURE(laInitArguments){
    int GLMajor,GLMinor,BufferSamples;
    int GLESMajor,GLESMinor;
    int UseColorManagement;
    int HasWorldObjects;
    int HasAction;
    int HasTextureInspector;
    int HasHistories;
    int HasTerminal;
    int HasAudio;
    int GLDebug;
    int EnableLogStdOut;
};

NEED_STRUCTURE(laAnimation);
NEED_STRUCTURE(laAudio);
NEED_STRUCTURE(laInputMappingBundle);

#define LA_LUT_PRECISION 33
#define LA_LUT_PIXCOUNT (LA_LUT_PRECISION * LA_LUT_PRECISION * LA_LUT_PRECISION)

STRUCTURE(LA){
    laListItem Hyper;

    laListHandle Logs;
    real PerfCounter[32][32];
    laSafeString* PerfLogs[32];
    int NextPerfCounter,NextPerfGroup,ShowPerf;
    laTimeRecorder FrameStartTime, FrameEndTime;

    char* TerminalInput[512];
    laListHandle TerminalHistories;
    int TerminalIncomplete;
    int IsTerminalEnter;

#ifdef LA_WITH_LUAJIT
    lua_State *L;
#endif

    laListHandle Controllers; int NextControllerID;
    u8bit LastControllerKey, LastControllerKeyDevice, ControllerHasNewKey;
    u8bit LastControllerAxis, LastControllerAxisDevice, ControllerHasNewAxis;

    laEvent* CurrentInputEvent;
    laInputProcessF InputProcess;
    laListHandle Windows;
    laListHandle WastedPanels;
    laListHandle Screens;
    int AutoSwitchColorSpace;

    laConfirmData *InvokeConfirmData;
    
    laPreFrameF  PreFrame;
    laPreDrawF   PreDraw;
    laPostFrameF PostFrame;
    laCleanupF Cleanup;

    real IdleStart, DelayStart;
    int DelayTriggered;
    int IdleTriggered;
    int ReTriggerOperators;
    laPanel* DockingPanel;

    char SysFontDir[512];

    laInitArguments InitArgs;

    char *InputBuf; int InputBufNext, InputBufMax;
    int32_t *InputBufU; int InputBufUNext, InputBufUMax;
    laSafeString* PasteString;
#ifdef LA_LINUX
    Atom MsgDelWindow;
    Atom bufid;
    Atom fmtid;
    Atom propid;
    Atom incrid;
	Atom targets_atom;
	Atom text_atom;
	Atom UTF8;
	Atom selection;
    Atom wmstate,wmfullscr,wmfullscroff;
#endif
    laSafeString* CopyPending;

    int GLMajor,GLMinor,GLESMajor,GLESMinor,BufferSamples;
    GLuint TempVAO;
    SYSTEMDISPLAY* dpy;
    SYSGLCONTEXT glc;
#ifdef _WIN32
    HINSTANCE hinstance;
    HDC hdc;
    int WinTabAvailable, WinTabOpened, WinTabMaxPenPressure, WinTabMaxEraserPressure, WinTabMaxHover;
#endif
#ifdef LA_LINUX
    Window* win;
    XVisualInfo* xvi;
#ifdef LA_USE_GLES
    EGLConfig BestFBC;
    EGLDisplay egl_dpy;
#else
    GLXFBConfig BestFBC;
#endif
    Colormap cmap;
    XIM im;
    XIC ic;
    int xi_opcode;
#endif
#ifdef LAGUI_ANDROID
    SYSWINDOW* win;
    EGLConfig BestFBC;
    EGLDisplay egl_dpy;
    EGLSurface egl_surf;
    struct android_app* app;
    struct android_poll_source *event_source;
    int AppWidth,AppHeight;
    int AppReady,AppEnabled;
    int ContextRebindRequired;
    int GotFilePermission;
    AAssetManager *AssetManager;
    char *InternalDataPath;
    char *ExternalDataPath;
    char *AndroidLastPath;
#endif
    int InkOrWinTab;
    int WacomDeviceStylus; real StylusPressure, StylusOrientation, StylusDeviation, StylusMaxPressure;
    int WacomDeviceEraser; real EraserPressure, EraserOrientation, EraserDeviation, EraserMaxPressure;
    int StylusHasTwist; real StylusTwist; real StylusHover;
    int PointerIsEraser,IsPen;
    int evLastX,evLastY;

    int EnableColorManagement;

    int EnableLogStdOut;

    int EnableGLDebug;
    int GLDebugSync;
    int GLDebugLevel;
    int GLDebugNeedsUpdate;

    int CurrentCursor;
    laWindow *CurrentWindow;
    laPanel *CurrentPanel;

    laListHandle DeferredRedrawList;

    laProgressDisplay Progress;

    laAudio* Audio;

    int InitDone;
    int IsReadingUDF;
    int SavePreferenceOnExit;

    laKeyMapper KeyMap;

    laHash256 OperatorTypeHash;

    //laHash256 CustomCommandHash;
    laSafeString* SignalFilter;
    laListHandle CustomSignals;
    laSafeString* OperatorFilter;
    laListHandle OperatorList;
    laSafeString* ThumbnailProp;

    laListHandle ExtraExtensions;
    laListHandle ExtraPreferencePaths;
    laListHandle ExtraPreferencePages;
    laUiDefineFunc PreferencePageDisplay;
    laUiDefineFunc PreferencePageInput;
    laUiDefineFunc PreferencePageResource;
    laUiDefineFunc PreferencePageTheme;

    laPropContainer *GeneralIntSub;
    laPropContainer *GeneralFloatSub;
    laPropContainer *GeneralEnumSub;
    laPropContainer *GeneralIntArraySub;
    laPropContainer *GeneralFloatArraySub;
    laPropContainer *GeneralEnumArraySub;
    laPropContainer *GeneralStringSub;
    laPropContainer *GeneralOperatorSub;
    laPropContainer *GeneralCollectionSub;
    laPropContainer *GeneralRawSub;

    laPropContainer *ContainerInt;
    laPropContainer *ContainerFloat;
    laPropContainer *ContainerEnum;
    laPropContainer *ContainerString;
    laPropContainer *ContainerOperator;
    laPropContainer *ContainerCollection;

    laPropContainer *_CONTAINER_SETTING;
    laProp          *_PROP_SETTING;
    laPropContainer *_CONTAINER_SETTING_PROPAGATE_TO;
    void            *_CONTAINER_SETTING_INSTANCE;
    laListHandle PropContainers;
    laRoot DataRoot;
    laPanel *PropMatcherContextP;
    laPanel* PendingSplash;

    laTheme*     CurrentTheme;
    laListHandle Themes;
    laListHandle UiTypes;
    
    laListHandle InitPanelTemplates;
    laListHandle PanelTemplates;
    laListHandle View2DTemplates;
    laUiDefineFunc MenuButtons;
    laUiDefineFunc MenuExtras;
    laUiDefineFunc AboutContent;
    laUiDefineFunc AboutVersion;
    laUiDefineFunc AboutAuthor;
    const char* MenuProgramName;

    SYSLOCK MemLock;
    SYSLOCK OpsLock;
    laHash256 GlobalMemPool;
    int ByteCount;
    int TotalByteCount;

    laHash65536* DBInstMemLeft;// list item pointers
    laHash65536 DBInst2;
    laHash65536 DBInst1;
    laHash65536 PtrSync;
    laListHandle PtrSyncAddressCommands;
    laListHandle PtrSyncHyper2Commands;
    laListHandle PostReadNodes;
    laListHandle RenewHyper2s;
    laListHandle SharedTypePointerSync;

    laListHandle PendingResourceRequests;
    laListHandle ResourceFolders;
    laListHandle ResourceRegistries;
    laListHandle UDFResources;
    laListHandle ManagedUDFs;
    laListHandle ManagedSaveProps;
    laManagedUDF* DummyManageUDF;
    laManagedUDF* DummyManageUDFSingle;
    laManagedUDF* DummyManageUDFSingleForce;
    int          ManagerDefaultView;
    int          SetUDFPending;
    int          ManagerFilterInstances;
    laPropContainer* SaverDummyContainer;

    laListHandle DBInstPendingAcquireDBP;
    laListHandle DBInstPendingAcquireDiffCMD;

    laListHandle DiffTouched;
    laListHandle Differences;
    laDiff* HeadDifference;
    laDBInst RootDBInst;
    laListHandle DBRecordedProps;
    laHash65536* DBInstLink;

    laDiffPushEverythingF PushEverything;
    laPreSaveF PreSave;
    laPostSaveF PostSave;

    laAnimation* Animation;

    real FontSize;
    real MarginSize;
    int UiRowHeight;
    int ScaledUiRowHeight;
    int ScaledMargin;
    real UiScale;
    void *ToPanel;

    int NextX, NextY, NextW, NextH;
    laNodeInSocket* tNodeIn;
    laNodeOutSocket* tNodeOut;


    laAuthorInfo Author;
    laListHandle ExternalAuthors;

    laSafeString *WorkingDirectory;

    //SYSLOCK csNotifier;
    laListHandle ThreadNotifiers;

    //preference:

    laInputMappingBundle* InputMapping;

    //color
    real ColorPickerGamma;
    real ViewportHalftoneFactor;
    real ViewportHalftoneSize;

    int NextWireColor, WireColorSlices;
    real WireThickness;
    real WireSaggyness;
    real* WireColorCache;

    //user interactions:
    int TopFramerate;
    int ValuatorThreshold;
    int ScrollingSpeed;
    int TooltipCloseDistance;
    real ZoomSpeed2D;
    real IdleTime;
    real DelayTime;

    //translations:
    laTranslation Translation;

    //display:
    real FloatingAlpha;
    int SolidShadowLength;
    int EnableAnimation;
    real AnimationSpeed;
    real PanelAnimationSpeed;
    int PanelMultisample;
    real LastFrameTime;
    real TimeAccum;
    
    //animation:
    int DefaultKeyFrameMode;

    //laThemeColor *ConditionStackColors[6];

    laListHandle MediaFiles;

    int GraphNeedsRebuild; // current mechanism only allows global.
    int InputNeedsEval;
    
    laListHandle NodeCategories;
    laNodeCategory* CurrentNodeCategory;
    int FilterNodeCategory;

    laBaseNodeType** NodeTypes; int NodeTypeNext, NodeTypeMax;
    laUiDefineFunc ExtraAddInputNodes;
    laUiDefineFunc ExtraAddDriverNodes;
    laGetBaseNodeTypeF ExtraGetInputNodeType;
    laGetBaseNodeTypeF ExtraGetDriverNodeType;

    void* ProofingLUT[3]; //srgb/clay/d65p3
    int LutNeedsRefreshing, CurrentLut;
    GLuint LutTexture;

    //laHash256 RootNodes;
    //LLVMContextRef llvmContext;
    //LLVMModuleRef llvmModule;

    //SYSLOCK csMediaPlay;

    int example_int;
    laSafeString* example_string;
    int DetachedViewSwitch;
};

#define LA_KM_SEL_UI_EXTRA 1
#define LA_KM_SEL_PANEL 2

STRUCTURE(laKeyMapItem){
    laListItem Item;

    int SpecialKeyBits;
    int EventType;
    int Key;

    char SelectBase;

    laPropPack Base;
    laPropPack Action;

    laSafeString *Operation;

    laSafeString *Instructions;
};

STRUCTURE(laScreen){
    laListItem Item;
    laSafeString* Name;
    laSafeString* Description;
    int RoughDPI;
    int ColorSpace;
    int x,y,w,h;
};

NEED_STRUCTURE(laLayout);
NEED_STRUCTURE(laBlock);
NEED_STRUCTURE(laPanel);
NEED_STRUCTURE(laUiItem);
STRUCTURE(laWindow){
    laListItem Item;

    SYSWINDOW* win;
    SYSGLCONTEXT glc;
#if defined(LA_USE_GLES) && !defined(LAGUI_ANDROID)
    EGLSurface egl_surf;
#endif
    int GLDebugNeedsUpdate;
    GLuint vao;
    struct NVGcontext* nvg;
#ifdef _WIN32
    HDC hdc;
    HIMC himc;
#endif

    int X,Y,W,H;
    int CW,CH;
    int Activated;
    int Shown;
    int IsDocking;
    int OutputColorSpace,OutputProofing;
    int OutputShowStripes;
    int UseComposing; real ComposingGamma,ComposingBlackpoint;
    int IsFullScreen;
    int Redraw;
    int RedrawTouched;

    laPropPack PP;
    laPropStep FakePS;

    laSafeString *Title;
    laWindow* DockingFrom;

    laListHandle EventList;
    laSafeString *OperatorHints;

    laListHandle Layouts;
    laLayout *CurrentLayout;
    laBlock *MaximizedBlock;
    laPanel *MaximizedUiPanel;
    laUiItem *MaximizedUi;

    laListHandle Panels;
    laListHandle FadingPanels;

    laListHandle Operators;
    laListHandle PendingOperators;

    laUiDrawFunc Draw;
};

STRUCTURE(laBlock){
    int X, Y, W, H;
    laBlock *B1;
    laBlock *B2;
    laBlock *parent;
    laPanel *CurrentPanel;
    laListHandle Panels;
    int Vertical;
    int Folded;
    real SplitRatio; // From Left/Top;
    laPropPack PP;
    laPropPack FakePS;
};

#define LA_BLOCK_DROP_LOCATION_L 1
#define LA_BLOCK_DROP_LOCATION_R 2
#define LA_BLOCK_DROP_LOCATION_U 3
#define LA_BLOCK_DROP_LOCATION_B 4

STRUCTURE(laLayout){
    laListItem Item;

    laBlock *OperatingBlock;
    laBlock *FirstBlock;

    laSafeString *ID;

    laLayoutBkgDrawFunc Draw;

    int      IsMoving;
    int      IsBlockHeaderClicked,LastX,LastY;
    int      DropLocation;
    laPanel *ClickedPanel;
    laBlock *DropToBlock;
    laBlock *OnBlockSeperator;
    laBlock *MovingBlock, *HeaderBlock;
};

STRUCTURE(laUiList){
    laListItem Item;
    int L, R, U, B;
    int TL, TR, TU, TB;
    int FL,FR,FU,FB; // Final cropped border in panel coord
    int LimH; // After UI calc
    real PanX, PanY; // Pan Up/Left Is Positive
    laListHandle UiItems;
    laListHandle Columns;
    laListHandle *ShareColumns;
    laSafeString *TabName;
    char *Instance;
    short HeightCoeff;
    char ScrollerShownV;
    char ScrollerShownH;
    uint32_t Flags;
    int AllowScale;
    real Scale; real SaveScale;
};

STRUCTURE(laUiListRecord){
    laListItem Item;
    laUiList* uil;
    laUiItem* pui;
};

STRUCTURE(laColumn){
    laListItem Item;
    real PreWidth;
    real SP;
    int IL, IR;
    laColumn *Top;
    int MaxW;
    int B;
    laColumn *LS, *RS;
    laColumn *CopyRef;
};

STRUCTURE(laUiListDraw){
    laListHandle Items;
    laListHandle SocketRecord;
    laUiList*    WiresContainer;
};

STRUCTURE(laUiListDrawItem){
    laListItem Item;
    laUiList *Target;
    int L, R, U, B, XP, YP, DifX, DifY;
    real Scale;
};

STRUCTURE(laBoxedTheme){
    laListItem Item;
    laSafeString *Name;

    real NormalY, Normal[4], Inactive[4];
    real ActiveY, Active[4];
    real BorderY, Border[4];

    real TextY, Text[4];
    real TextActiveY, TextActive[4], TextInactive[4], TextInvert[4];
    real Alpha;

    int TextShadow;
    int BoxStyle;
    int ColorSelection;
    int NoDecalInvert;

    laBoxedTheme **BackRef;
    laTheme* Parent;
};

STRUCTURE(laSocketRecord){
    laListItem Item;
    laNodeInSocket* In;
    laNodeOutSocket* Out;
};

extern laPropContainer* LA_PC_SOCKET_IN;
extern laPropContainer* LA_PC_SOCKET_OUT;
extern laPropContainer* LA_PC_MAPPER;
extern laPropContainer* LA_PROP_SOCKET_SOURCE;
extern laPropContainer* LA_PROP_SOCKET_OUT;
extern laPropContainer* LA_PC_RACK_PAGE;
extern laProp* LA_PROP_CONTROLLER;

extern unsigned char DATA_LUT_PROOF_SRGB[];
extern unsigned char DATA_LUT_PROOF_CLAY[];
extern unsigned char DATA_LUT_PROOF_D65P3[];

#define LA_UI_NORMAL  0
#define LA_UI_ACTIVE   (1<<0)
#define LA_UI_EDITING  (1<<1)
#define LA_UI_DISABLED (1<<30)

#define LA_BT_NORMAL LA_UI_NORMAL
#define LA_BT_ACTIVE LA_UI_ACTIVE
#define LA_BT_NO_DECAL (1<<5)
#define LA_BT_BORDER   (1<<6)
#define LA_BT_TEXT     (1<<7)
#define LA_BT_TEXT_NORMAL (LA_BT_TEXT|LA_UI_NORMAL)
#define LA_BT_TEXT_ACTIVE (LA_BT_TEXT|LA_UI_ACTIVE)
#define LA_BT_DISABLED LA_UI_DISABLED
#define LA_BT_VERTEX   (1<<8)
#define LA_BT_EDGE     (1<<9)
#define LA_BT_FACE     (1<<10)
#define LA_BT_SELECTED (1<<11)
#define LA_BT_SVERTEX  (LA_BT_VERTEX|LA_BT_SELECTED)
#define LA_BT_SEDGE    (LA_BT_EDGE|LA_BT_SELECTED)
#define LA_BT_SFACE    (LA_BT_FACE|LA_BT_SELECTED)
#define LA_BT_WARNING  (1<<12)
#define LA_UI_WARNING LA_BT_WARNING
#define LA_BT_SHADOW   (1<<13)

#define LA_UI_TAG_IS_OFFSCREEN  (1<<0)
#define LA_UI_TAG_CAVANS_SCALED (1<<1)
#define LA_UI_TAG_NEED_REBUILD  (LA_UI_TAG_IS_OFFSCREEN | LA_UI_TAG_CAVANS_SCALED)
#define LA_UI_TAG_SCISSOR       (1<<2)

STRUCTURE(laTheme){
    laListItem Item;
    laSafeString *Name;
    laListHandle Colors;
    laListHandle BoxedThemes;
    int abc;
    //
    real ColorBkg[4];
    real Color[4];
    real ColorB[4];
    real ColorC[4];
    real AccentColor[4];
    real WarningColor[4];
    real SelectionColor[4];
    real CursorColor[4];
    real InactiveMix;
    real InactiveSaturation;
    real CursorAlpha;
    real SelectionAlpha;
    real ShadowAlpha;
    real TextShadowAlpha;

    real WireSaturation;
    real WireTransparency;
    real WireBrightness;

    real VertexColor[4];
    real EdgeColor[4];
    real SVertexColor[4];
    real SEdgeColor[4];
    real SFaceColor[4];
    real ShadowColor[4];
    real TextShadowColor[4];
    real EdgeBrightness, EdgeTransparency;
    real VertexBrightness, VertexTransparency;
    real SelectedVertexTransparency;
    real SelectedEdgeTransparency;
    real SelectedFaceTransparency;
};

NEED_STRUCTURE(laOperatorType);

STRUCTURE(laUiType){
    laListItem Item;

    const char *Identifier;

    int ForType;
    const char *TargetSub;

    //laPropStep      FakePs;
    laSubProp* FakeProp;
    laPropContainer* ExtraProps;

    laBoxedTheme **Theme;
    laUiDrawFunc Draw;
    const char *OperatorID;
    laOperatorType *OperatorType;

    laUiGetMinWidthFunc GetMinWidth;
    laUiGetHeightFunc GetHeight;

    laUiDestroyFunc Destroy;
    laUiInitFunc Init;

    laKeyMapper KeyMapper;

    int Tag;
};

NEED_STRUCTURE(laUiItem);
STRUCTURE(laGeneralUiExtraData){
    int LastX, LastY;
    int TargetIndexVali;
    real TargetIndexValf;
    int On; //On:Modifying
    int Dragging;
    laStringEdit *Edit;
    void *Ptr1;
    void *Ptr2;
    laUiItem* ui;
};

STRUCTURE(laMultiStringExtra){
    laGeneralUiExtraData* general;
    int PanX, PanY;
};

NEED_STRUCTURE(laCanvasExtra);

STRUCTURE(la3DObjectDrawExtra){
    int DisplayMode;
    int MeshEditType;
    tnsMatrix44d mViewProj;
    int W,H,Is3D;
    real PointScale;
};

typedef void(*laCanvasDrawFunc)(laBoxedTheme* bt, void* DataInstance, laCanvasExtra* Extra);
typedef int (*laModalFunc)(laOperator *, laEvent *);

STRUCTURE(laCanvasTemplate){
    laListItem Item;
    laSafeString *Identifier;
    laCanvasDrawFunc Draw;
    laUiDrawFunc SecondDraw;
    laKeyMapper KeyMapper;
    char *TargetContainerID;
    laPropContainer *TargetContainer;
    laPropContainer *ExtraProps;
    laSubProp       *FakeProp;
    laModalFunc      ExtraModal;
    //float           LimitYScale;
    laUiInitFunc     Init;
    laUiDestroyFunc  Destroy;
};


#define LA_CANVAS_CURSOR_CROSS 1
#define LA_CANVAS_CURSOR_BOX   2
#define LA_CANVAS_CURSOR_ARROW 3
#define LA_CANVAS_CURSOR_X     4
#define LA_CANVAS_CURSOR_Y     5

#define LA_CANVAS_SELECT_MODE_VERTS 0
#define LA_CANVAS_SELECT_MODE_EDGES 1
//#define LA_CANVAS_SELECT_MODE_FACES 2
#define LA_CANVAS_SELECT_MODE_KNIFE 3

#define LA_CANVAS_SELECT_THROUGH_OFF 0
#define LA_CANVAS_SELECT_THROUGH_ON  1

#define LA_CANVAS_DISPLAY_SOLID 0
#define LA_CANVAS_DISPLAY_MATERIAL 1

#ifdef LAGUI_ANDROID
#define LA_PANEL_DEFAULT_GL_FORMAT GL_RGBA4
#else
#define LA_PANEL_DEFAULT_GL_FORMAT GL_RGBA
#endif

void la_CanvasDefaultOverlay(laUiItem* ui, int h);
void la_CanvasDrawOverlay(laUiItem *ui, int h);
void la_CanvasInit(laUiItem *ui);
void la_CanvasDestroy(laUiItem *ui);

STRUCTURE(laCanvasExtra){
    laListItem Item;
    tnsOffscreen *OffScr;
    tnsOffscreen *DeferredOffScr;

    laUiItem *ParentUi;

    real ZoomX,ZoomY;

    real PanX,PanY;

    int DrawCursor;
    real OnX, TargetX;
    real OnY, TargetY;
    real ClickedX,ClickedY;

    int LW,ShowLegend,UiMode;

    int HeightCoeff;
    int ShowDetails;

    int ImageDrawAlpha;
    int ImageDrawBorder;

    int AdaptiveLineWidth;
    int LineWidthWarning;

    int ClearBackground;

    int SelectMode;
    int SelectThrough;
    int EditCenter;
    int DeltaMode;
    real BL, BR, BU, BB;

    int Dragging;
    int TargetIndexVali;

    //3d==========

    tnsOffscreen *OffScrShadow;

    tnsCamera *ViewingCamera; //Always create this one;
    tnsCamera *UsingCamera;
    int Layers[20];

    int AsPlayer;

    int ShowFloorGrid;
    int ShowAxis[3];
    int DisplayMode;
};

NEED_STRUCTURE(laUiConditionNode);
STRUCTURE(laConditionUiExtraData){
    int a;
    laUiItem *EndUi;
    laUiItem *ElseUi;
    laUiConditionNode *Expression;
    //char              Remove;
    char IsTrue;
};

STRUCTURE(laUiTemplate){
    laListItem Item;
    laSafeString *Title;
    laSafeString *Identifier;
    laSafeString *CategoryName;
    laUiDefineFunc          Define;
    laUiDefineFunc          Header;
    laPanelDetachedPropFunc PropFunc;
    int DefaultGLFormat;
    int DefaultW_RH,DefaultH_RH;
    laKeyMapper KeyMap;
};

#define LA_SLOT_MARKER_1 0
#define LA_SLOT_MARKER_2 1
#define LA_SLOT_MARKER_3 2
#define LA_SLOT_MARKER_4 3

STRUCTURE(laCompoundPP){
    laListItem Item;
    int Slot;
    laPropPack PP;
};

STRUCTURE(laUiItem){
    laListItem Item;

    laColumn *C;
    int L, R, U, B;
    int TL, TR, TU, TB;
    int AnimationDistinguish;
    int State;
    
    uint64_t Flags; // expand and even (in row control ui)
    laPropPack ExtraPP;
    laPropStep FakePs;
    laGeneralUiExtraData *Extra;

    short Expand;
    short Extent;
    laPropPack PP;
    laListHandle CompoundPPs;
    laSafeString *Display;
    laSafeString *ExtraInstructions;

    laUiType *Type;
    laCanvasTemplate *CanvasTemplate;

    laUiList *Page; // reuse as row begin ref
    laOperatorType *AT; //Probably share in the future;
    laStringSplitor *Instructions;

    laListHandle Subs;

    laUiDefineFunc Template;
    int TemplateContext;

    laListHandle SubPanels;
};

STRUCTURE(laWidget){
    laUiType* Type;
    int       Flags;
};

#define LA_UI_FLAGS_TRANSPOSE (1<<0)
#define LA_UI_FLAGS_EXPAND    (1<<1)
#define LA_UI_FLAGS_ICON      (1<<2)
#define LA_UI_FLAGS_CYCLE     (1<<3)
#define LA_UI_FLAGS_NO_DECAL  (1<<4)
#define LA_UI_FLAGS_NO_EVENT  (1<<5)
#define LA_UI_FLAGS_NO_CONFIRM (1<<6)
#define LA_TEXT_ALIGN_LEFT    (1<<7)
#define LA_TEXT_ALIGN_RIGHT   (1<<8)
#define LA_TEXT_ALIGN_CENTER  (1<<9)
#define LA_TEXT_ALIGN_AUTO    (1<<10)
#define LA_TEXT_USE_NEWLINE   (1<<11)
#define LA_TEXT_LINE_WRAP     (1<<12)
#define LA_TEXT_REVERT_Y      (1<<13)
#define LA_TEXT_MONO          (1<<14)
#define LA_UI_FLAGS_PREFER_BOTTOM (1<<15)
#define LA_UI_FLAGS_NO_GAP    (1<<16)
#define LA_UI_FLAGS_UNDERNEATH (1<<17)
#define LA_UI_FLAGS_NO_HEIGHT (LA_UI_FLAGS_NO_GAP|LA_UI_FLAGS_NO_DECAL|LA_UI_FLAGS_UNDERNEATH)
#define LA_UI_FLAGS_NO_OVERLAY (1<<18)
#define LA_TEXT_OVERFLOW_ARROW (1<<19)
#define LA_UI_FLAGS_DISABLED   (1<<20)
#define LA_UI_FLAGS_HIGHLIGHT  (1<<21)
#define LA_UI_FLAGS_NODE_CONTAINER (1<<22)
#define LA_UI_COLLECTION_NO_HIGHLIGHT LA_TEXT_ALIGN_LEFT
#define LA_TEXT_ONE_LINE       (1<<23)
#define LA_UI_FLAGS_NO_SCROLL_INACTIVE (1<<24)
#define LA_UI_FLAGS_COLOR_SPACE_CLAY (1<<25)
#define LA_UI_FLAGS_COLOR_SPACE_D65_P3 LA_UI_FLAGS_NO_SCROLL_INACTIVE
#define LA_UI_FLAGS_EXIT_WHEN_TRIGGERED LA_UI_FLAGS_COLOR_SPACE_CLAY
#define LA_TEXT_LCD_16         (1<<27)
#define LA_TEXT_LCD_7          (1<<28)
#define LA_UI_FLAGS_NO_TOOLTIP LA_UI_FLAGS_PREFER_BOTTOM
#define LA_UI_IMAGE_FULL_W     LA_TEXT_MONO
#define LA_UI_MIN_WIDTH        LA_UI_FLAGS_COLOR_SPACE_CLAY
#define LA_UI_FLAGS_NO_LABEL   (1<<29)
#define LA_UI_COLLECTION_SIMPLE_SELECTOR LA_UI_FLAGS_CYCLE
#define LA_UI_FLAGS_WARNING    (LA_UI_FLAGS_CYCLE)
#define LA_TEXT_SHADOW         LA_UI_FLAGS_NO_OVERLAY
#define LA_UI_FLAGS_MENU_FLOAT16 LA_UI_FLAGS_NO_SCROLL_INACTIVE

#define LA_UI_FLAGS_INT_ICON  (LA_UI_FLAGS_NO_DECAL|LA_UI_FLAGS_NO_EVENT|LA_UI_FLAGS_ICON)
#define LA_UI_FLAGS_PLAIN     (LA_UI_FLAGS_NO_DECAL|LA_UI_FLAGS_NO_EVENT)
#define LA_TEXT_ALIGN (LA_TEXT_ALIGN_LEFT|LA_TEXT_ALIGN_CENTER|LA_TEXT_ALIGN_RIGHT|LA_TEXT_ALIGN_AUTO)

#define LA_UI_FLAGS_COLORFUL LA_UI_FLAGS_ICON

#define LA_UI_FLAGS_TERMINAL_INPUT LA_UI_FLAGS_COLOR_SPACE_CLAY
#define LA_UI_FLAGS_IMMEDIATE_INPUT LA_UI_FLAGS_CYCLE

#define LA_UI_FLAGS_MOMENTARY (LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_NODE_CONTAINER)
#define LA_UI_FLAGS_KNOB LA_UI_FLAGS_NODE_CONTAINER

#define LA_UI_FLAGS_VERTICAL_SLIDER LA_UI_FLAGS_CYCLE

#define LA_UI_SOCKET_LABEL_N LA_TEXT_ALIGN_LEFT
#define LA_UI_SOCKET_LABEL_S LA_TEXT_ALIGN_RIGHT
#define LA_UI_SOCKET_LABEL_W LA_TEXT_ALIGN_CENTER
#define LA_UI_SOCKET_LABEL_E LA_TEXT_ALIGN_AUTO

#define LA_UI_COLLECTION_MANAGER_FILTER LA_TEXT_ONE_LINE

#define LA_TEXT_ROTATE_N   (1ll<<30)
#define LA_TEXT_ROTATE_W   (1ll<<31)
#define LA_TEXT_ROTATE_S   (1ll<<32)

extern laWidget* LA_WIDGET_FIXED_GROUP;
extern laWidget* LA_WIDGET_TAB;
extern laWidget* LA_WIDGET_COLLECTION;
extern laWidget* LA_WIDGET_COLLECTION_ITEM;
extern laWidget* LA_WIDGET_COLLECTION_SELECTOR;
extern laWidget* LA_WIDGET_COLLECTION_SINGLE;
extern laWidget* LA_WIDGET_CONDITION_TOGGLE;
extern laWidget* LA_WIDGET_COLUMN_ADJUSTER;
extern laWidget* LA_WIDGET_COLUMN_VIEWER;
extern laWidget* LA_WIDGET_BUTTON;
extern laWidget* LA_WIDGET_BUTTON_NO_CONFIRM;
extern laWidget* LA_WIDGET_LABEL;
extern laWidget* LA_WIDGET_INT;
extern laWidget* LA_WIDGET_INT_PLAIN;
extern laWidget* LA_WIDGET_INT_PLAIN_ICON;
extern laWidget* LA_WIDGET_METER_TYPE1;
extern laWidget* LA_WIDGET_METER_TYPE2;
extern laWidget* LA_WIDGET_VALUE_METER_2D;
extern laWidget* LA_WIDGET_FLOAT;
extern laWidget* LA_WIDGET_FLOAT_PLAIN;
extern laWidget* LA_WIDGET_FLOAT_COLOR;
extern laWidget* LA_WIDGET_FLOAT_COLOR_HCY;
extern laWidget* LA_WIDGET_ENUM_SELECTOR;
extern laWidget* LA_WIDGET_ENUM_SELECTOR_ICON;
extern laWidget* LA_WIDGET_ENUM_CYCLE;
extern laWidget* LA_WIDGET_ENUM_CYCLE_ICON;
extern laWidget* LA_WIDGET_ENUM_ICON_PLAIN;
extern laWidget *LA_WIDGET_ENUM_HIGHLIGHT;
extern laWidget* LA_WIDGET_STRING;
extern laWidget* LA_WIDGET_STRING_PLAIN;
extern laWidget* LA_WIDGET_STRING_MULTI;
extern laWidget *LA_WIDGET_STRING_MONO_PLAIN;
extern laWidget* LA_WIDGET_MENU_ROOT;
extern laWidget* LA_WIDGET_ALIGN;
extern laWidget* LA_WIDGET_3D_VIEW;
extern laWidget* LA_WIDGET_2D_VIEW;
extern laWidget *LA_WIDGET_NODE_SOCKET;
extern laWidget *LA_WIDGET_HEIGHT_ADJUSTER;
extern laWidget *LA_WIDGET_RAW;
extern laWidget *LA_WIDGET_MAPPER;
extern laWidget *LA_WIDGET_IMAGE;
extern laWidget *LA_WIDGET_SCOPE;
extern laWidget *LA_WIDGET_KNOB;
extern laWidget *LA_WIDGET_MOUSE_ACTION_REPORTER;

#define LA_CONDITION_TRUE 1
#define LA_CONDITION_FALSE 2
#define LA_CONDITION_GE 3
#define LA_CONDITION_GT 4
#define LA_CONDITION_EQ 5
#define LA_CONDITION_LT 6
#define LA_CONDITION_LE 7
#define LA_CONDITION_AND 8
#define LA_CONDITION_OR 9
#define LA_CONDITION_NOT 10
#define LA_CONDITION_INT 11
#define LA_CONDITION_FLOAT 12
#define LA_CONDITION_STRING 13
#define LA_CONDITION_PROP 14

STRUCTURE(laUiConditionNode){
    char a;
    int Type;
    laUiConditionNode *Expression1; // As Int Value
    laUiConditionNode *Expression2;
    laUiDescriptor *Base;
    char IsExtra;
    int IntValue;
    real FloatValue;
    laSafeString *String; //used as prop path in template
    laPropPack PP;
};

#define LA_PANEL_NORMAL 0
#define LA_PANEL_FLOATING_PASSIVE 2
#define LA_PANEL_FLOATING_TOP 3
#define LA_PANEL_NO_PARENT_MENU 4

#define LA_TAG_REDRAW 1
#define LA_TAG_RECALC 2
#define LA_TAG_ANIMATION 4
#define LA_TAG_RECALC_SCROLLER 8

#define LA_PANEL_ANIMATION_DROP_DOWN 1
#define LA_PANEL_ANIMATION_EXPAND 2
#define LA_PANEL_ANIMATION_DISSOVE 3
#define LA_PANEL_ANIMATION_COLLAPSE 4
#define LA_PANEL_ANIMATION_MINIMIZE 5
#define LA_PANEL_ANIMATION_FLASH 126
#define LA_PANEL_ANIMATION_DESTROY 127

NEED_STRUCTURE(laOperator);
NEED_STRUCTURE(laOperator);

STRUCTURE(laPanel){
    laListItem Item;

    int __SAFE__;

    int X, Y, W, H;
    int MaxW, MaxH, MinW, MinH;
    int TX, TY, TW, TH;
    int TitleWidth;

    char AnimationMode;
    real AnimationRatio;

    //int         PanX, PanY;

    int Show;
    short Refresh;
    short ShowCorner;
    unsigned int FrameDistinguish;

    char BoundUi;
    char IsMenuPanel;
    char LaterDestroy;
    char CloseWhenMovedOut;
    int Mode;
    int NoConfirm;

    int SL, SR, ST, SB; //Snap To Edge

    laSafeString *Title;
    laUiTemplate *PanelTemplate;

    laPropPack PP;
    laPropStep FakePS;

    //laSafeString *_ToolboxName;
    //int _ToolboxLayout;
    laPropContainer* PropLinkContainer;
    laSubProp* PropLinkFakeProp;
    laPropPack PropLinkPP;
    laPropStep PropLinkFakePS;

    laKeyMapper KeyMap;

    laPanelDrawFunc Draw;
    laBoxedTheme **BT;

    tnsOffscreen *OffScr;

    laUiList UI;
    laUiList TitleBar;

    laUiList *MenuRefer;
    laUiDefineFunc UiDefine;

    laListHandle SubPanels;
    laPanel *Parent;
    laBlock *Block;

    laOperator *ParentOperator;

    u64bit Tag;
};

STRUCTURE(laPanelMessage){
    laListItem Item;
    laSafeString *Message;
};
STRUCTURE(laPanelMessageList){
    laListHandle Msg;
};

NEED_STRUCTURE(laOperator);

typedef int (*laInitFunc)(laOperator *);
typedef int (*laRefreshFunc)(laOperator *);
typedef int (*laCheckFunc)(laPropPack *, laStringSplitor *); //this, ui->instructions amd ap->instructions
typedef void (*laExitFunc)(laOperator *, int);         //true means Quit, false means Cancel
typedef int (*laInvokeFunc)(laOperator *, laEvent *);
typedef int (*laModalFunc)(laOperator *, laEvent *);
typedef int (*laSystemFunc)(laOperator *, laEvent *);

#define LA_CANCEL 0
#define LA_PASS_ON (1<<10)
#define LA_BLOCK (1<<11)
#define LA_FINISH (1<<12)
#define LA_CONFIRM (1<<13)

#define LA_CANCELED 0
//#define LA_PRE_INVOKE 16384
#define LA_FINISHED (LA_FINISH | LA_BLOCK)
#define LA_FINISHED_PASS (LA_FINISH | LA_PASS_ON)
#define LA_RUNNING LA_BLOCK
#define LA_RUNNING_PASS (LA_BLOCK | LA_PASS_ON)

#define LA_OPERATOR_CALLS_SHUTOFF (LA_FINISHED|(1<<5))

#define LA_EXTRA_TO_PANEL 1
#define LA_EXTRA_CONFIRM 2
#define LA_ACTUATOR_SYSTEM 4
#define LA_ACTUATOR_HIDDEN 8

#define LA_CONFIRM_OK (1<<10)
#define LA_CONFIRM_CANCEL (1<<11)
#define LA_CONFIRM_IGNORE (1<<12)
#define LA_CONFIRM_DATA (1<<13)
#define LA_CONFIRM_CUSTOM_STRING (1<<14)
#define LA_CONFIRM_TEXT_EDIT (1<<15)

STRUCTURE(laOperatorType){
    laListItem Item;
    laListItem ListItem;

    const char *Identifier;
    const char *Name;
    const char *Description;

    const char *ExtraInstructions;

    laInitFunc Init;
    laExitFunc Exit;
    laInvokeFunc Invoke;
    laModalFunc Modal;
    laCheckFunc Check;

    uint32_t IconID;

    laPropContainer* PC;
    laUiDefineFunc UiDefine;

    laOperatorArgumentParserFunc ParseArgs;

    laListHandle Properties;

    int ExtraMark;
};

typedef void (*laConfirmDataDestroyFunc)(void *);

STRUCTURE(laConfirmData){
    int Mode;
    int IData;
    real FData;
    void *CustomData;
    char *StrData;
    void* PointerData;
    laPropContainer* PointerType;
    laConfirmDataDestroyFunc Destroy;
};

STRUCTURE(laOperator){
    laListItem Item;

    int State;
    void *Instance;

    void *CustomData;

    const char *ExtraInstructions;
    laStringSplitor *ExtraInstructionsP;

    laPropPack PP;
    laPropPack *This;

    laConfirmData *ConfirmData;
    laConfirmData *NextConfirmData;

    laOperator *Child;
    laListHandle PropertyStorage;
    laOperatorType *Type;

    laPropPack *CreatedThis;

    int Using;
    int StopNow;
    int ModalOver;

    laPanel *ToPanel;
    laPanel *OperatorPanel;
    laListHandle LocalUiLists;

    laSafeString* RuntimeHint;
};

#define LA_INPUT_DEVICE_KEYBOARD 0
#define LA_INPUT_DEVICE_MOUSE 1
#define LA_INPUT_DEVICE_JOYSTICK 2

STRUCTURE(laInputMapping){
    laListItem Item;
    laSafeString* Name;
    laListHandle Entries;
};
STRUCTURE(laInputMappingEntry){
    laListItem Item;
    int Disabled;
    laInputMapping* Parent;
    int DeviceType;
    int JoystickDevice; int Axis, Button;
    laSafeString* Key; int KeyValue;
    int SpecialKeyBits;
    laSafeString* Signal; int SignalValue;
    int UseOperator;
    laSafeString* Operator,*OperatorName,*OperatorArguments;
    laOperatorType* OperatorType;
    int OperatorBase;
};
STRUCTURE(laInputMappingBundle){
    void* _pad;
    laListHandle InputMappings;
    laInputMapping* CurrentInputMapping;
    laListHandle Toolboxes;
    laInputMapping* CurrentToolbox;
    int ToolboxLayout;
};

#define LA_SIGNAL_NEW 10000
#define LA_SIGNAL_DELETE 10001
#define LA_SIGNAL_CONFIRM 10002
#define LA_SIGNAL_UNDO 10003
#define LA_SIGNAL_REDO 10004
#define LA_SIGNAL_OPEN 10005
#define LA_SIGNAL_SAVE 10006
#define LA_SIGNAL_SAVE_AS 10007
#define LA_SIGNAL_LAYOUT_NEXT 10008
#define LA_SIGNAL_LAYOUT_PREV 10009
#define LA_SIGNAL_FULLSCREEN 10010

STRUCTURE(laCustomSignal){
    laListItem Item;
    //laListItem Item2;
    laSafeString* Name;
    int Signal;
};

#define LA_JS_MAX_AXES 64
#define LA_JS_MAX_BUTTONS 128
STRUCTURE(laController){
    laListItem Item;
    laSafeString* Name;
    laSafeString* Path;
    int fd; //device;
    int NumButtons; int NumAxes; int UserAssignedID; int Error;
    int InternalType; // used to identify models and use specific props.
    int16_t AxisValues[LA_JS_MAX_AXES],SaveAxisValues[LA_JS_MAX_AXES];
    int AxisMaxes[LA_JS_MAX_AXES];
    int AxisMins[LA_JS_MAX_AXES];
    int16_t AxisLimitMaxes[LA_JS_MAX_AXES]; // calibrated max;
    int16_t AxisLimitMins[LA_JS_MAX_AXES];
    int16_t AxisCenterMaxes[LA_JS_MAX_AXES]; // calibrated center;
    int16_t AxisCenterMins[LA_JS_MAX_AXES];
    char ButtonValues[LA_JS_MAX_BUTTONS];

    u8bit ButtonsMap[LA_JS_MAX_BUTTONS];
    u8bit AxisMap[LA_JS_MAX_AXES];
    laSafeString* ButtonNames[LA_JS_MAX_BUTTONS];
    laSafeString* AxisNames[LA_JS_MAX_AXES];
};

void la_RefreshControllers();
void la_InitControllers();
void la_RemoveDuplicatedControllers();
void la_UpdateControllerStatus();
void la_RegisterControllerProps();
laPropContainer* laget_ControllerType(laController* c);
laController* la_FindControllerWithID(int id);

char* laControllerIDGetAxisName(int id, int axis);
char* laControllerIDGetButtonName(int id, int button);
int laControllerIDGetAxis(int id, char* name);
int laControllerIDGetButton(int id, char* name);

void la_RegisterBasicNodes();

#define LA_DAG_FLAG_ERR  0
#define LA_DAG_FLAG_TEMP 1
#define LA_DAG_FLAG_PERM 2

int laRebuildPageEval(laRackPage* rp);
int laRunPage(laRackPage* rp, uint64_t mask);

void laGraphRequestRebuild();
void laMappingRequestEval();

int la_RunDrivers();

void lapost_Node(laBaseNode *bn);

laBaseNode* la_CreateDriverNode(laNodeRack* ir, laBaseNodeType* NodeType);

void* laget_InstanceSelf(void* instace);
laPropContainer* laget_BaseNodeType(laBaseNode* bn);
void laRegisterNode(laBaseNodeType* type, laPropContainer* pc,
                    laBaseNodeInitF init, laBaseNodeDestroyF destroy, laBaseNodeVisitF visit, laBaseNodeEvalF eval, laBaseNodeCopyF copy,
                    int nodesize, char* udf_string, char* type_string, char* UiText, int icon);

void latouched_NodeInSocket(void* unused, int hint);

#define LA_BASE_NODE_HEADER(uil,c,This) \
    {laSplitColumn(uil,c,0.2);laColumn*c1=laLeftColumn(c,1);c=laRightColumn(c,0);\
    laShowHeightAdjuster(uil,c1,This,"base.__gap",0);}

#define LA_IDN_REGISTER(uitext,icon,a,pc,funcname,type) \
    laRegisterNode(&a,pc,funcname##Init,funcname##Destroy,funcname##Visit,funcname##Eval,funcname##Copy,sizeof(type),"UDFRES_" #type, #type, uitext,icon);

#define LA_SRC_AND_PARENT(socket) \
    ((socket)->Source && (socket)->Source->Parent)

#define LA_VISIT_NODE(node,vi) \
    {if(node){int result=node->Type->Visit(node,vi); if(result!=LA_DAG_FLAG_PERM) return LA_DAG_FLAG_ERR;} }

#define LA_GUARD_THIS_NODE(n,vi) \
    {uint64_t br=vi->Branch; if((n->Base.Branch&br)==br) return LA_DAG_FLAG_PERM; if((n->Base.BranchTemp&br)==br) return LA_DAG_FLAG_ERR; n->Base.BranchTemp|=br; }

#define LA_ADD_THIS_NODE(n,vi) \
    {uint64_t br=vi->Branch; if(!n->Base.Branch){ lstAppendPointer(vi->l,n); } n->Base.Branch|=br; n->Base.BranchTemp&=(~br); }

#define LA_GET_SRC_AS_VALUE(var, socket) \
    {if(socket->Source&&socket->Source->DataType&LA_PROP_FLOAT) var=*((real*)socket->Source->Data);\
    if(socket->Source&&socket->Source->DataType&(LA_PROP_INT|LA_PROP_ENUM)) var=*((int*)socket->Source->Data);}

#define LA_IDN_OLD_DUPL(socket) \
    old->socket->Duplicated=new->socket;
#define LA_IDN_NEW_LINK(socket) \
    { memAssignRef(new->socket,&new->socket->Source,old->socket->Source?old->socket->Source->Duplicated:0); new->socket->ColorId=old->socket->ColorId; }


extern laBaseNodeType LA_IDN_KEYBOARD;
extern laBaseNodeType LA_IDN_MOUSE;
extern laBaseNodeType LA_IDN_CONTROLLER;

extern laBaseNodeType LA_IDN_VISUALIZER;
extern laBaseNodeType LA_IDN_SPLIT;
extern laBaseNodeType LA_IDN_SWITCH;
extern laBaseNodeType LA_IDN_COMBINE;
extern laBaseNodeType LA_IDN_VALUES;
extern laBaseNodeType LA_IDN_MATRIX;
extern laBaseNodeType LA_IDN_MATH;

extern laBaseNodeType TNS_IDN_TRANSFORM;
extern laBaseNodeType TNS_IDN_MAKE_TRANSFORM;

extern laNodeCategory* LA_NODE_CATEGORY_INPUT;
extern laNodeCategory* LA_NODE_CATEGORY_MATH;
extern laNodeCategory* LA_NODE_CATEGORY_ROUTE;
extern laNodeCategory* LA_NODE_CATEGORY_DRIVER;
extern laNodeCategory* LA_NODE_CATEGORY_AUX;
extern laNodeCategory* LA_NODE_CATEGORY_COLOR;

#define LA_VALUES_NODE_FLOAT 0
#define LA_VALUES_NODE_INT 1
#define LA_VALUES_NODE_ENUM 2

#define LA_MATRIX_NODE_OP_MUL 0
#define LA_MATRIX_NODE_OP_INV 1

#define LA_MATH_NODE_OP_ADD 0
#define LA_MATH_NODE_OP_SUB 1
#define LA_MATH_NODE_OP_MUL 2
#define LA_MATH_NODE_OP_DIV 3
#define LA_MATH_NODE_OP_POW 4
#define LA_MATH_NODE_OP_LOG 5
#define LA_MATH_NODE_OP_SIN 6
#define LA_MATH_NODE_OP_COS 7
#define LA_MATH_NODE_OP_TAN 8
#define LA_MATH_NODE_OP_ASIN 9
#define LA_MATH_NODE_OP_ACOS 10
#define LA_MATH_NODE_OP_ATAN 11
#define LA_MATH_NODE_OP_ATAN2 12
#define LA_MATH_NODE_OP_MOD 13

#define LA_VECTOR_MATH_OP_ADD 0
#define LA_VECTOR_MATH_OP_SUB 1
#define LA_VECTOR_MATH_OP_MUL 2
#define LA_VECTOR_MATH_OP_DIV 3
#define LA_VECTOR_MATH_OP_POW 4
#define LA_VECTOR_MATH_OP_LOG 5
#define LA_VECTOR_MATH_OP_DOT 6
#define LA_VECTOR_MATH_OP_CROSS 7
#define LA_VECTOR_MATH_OP_LEN 8
#define LA_VECTOR_MATH_OP_NORMALIZE 9

NEED_STRUCTURE(tnsObject)
STRUCTURE(laRackPage){
    laListItem Item;
    laSafeString* Name;
    laListHandle Racks;
    laListHandle Eval;
    laListHandle AlwaysBranchers;
    laSafeString* Script;
    tnsObject* ParentObject;
    int RackType;
    int TriggerMode;
    int UseScript;
};
#define LA_RACK_TYPE_INPUT (1<<0)
#define LA_RACK_TYPE_DRIVER (1<<1)
#define LA_RACK_TYPE_ALL 0xfffffff
STRUCTURE(laNodeRack){
    laListItem Item;
    laSafeString* Name;
    laListHandle Nodes;
    laRackPage* ParentPage;
    int RackType;
    laNodeRack* Duplicated;
};
STRUCTURE(laKeyboardNode){
    laBaseNode Base;
    laNodeOutSocket* IsKey; int iIsKey;
    laNodeOutSocket* KeyDown; int iKeyDown;
    laNodeOutSocket* Key; int iKey;
};
NEED_STRUCTURE(laInputControllerNode);
STRUCTURE(laInputControllerNodeSocket){
    laInputControllerNode* Parent;
    laSafeString* Which;
    laNodeOutSocket* Out;
    int Offset;
    int IntVal[8]; real RealVal[8];
};
STRUCTURE(laInputControllerNode){
    laBaseNode Base;
    laInputControllerNodeSocket Sockets[8];
    int UserID;
    int Mode;// btn/axis;
};
STRUCTURE(laInputVisualizerNode){
    laBaseNode Base;
    laNodeInSocket* In;
    int IntVal[8]; real RealVal[8];
};
STRUCTURE(laSplitNodeOutSocket){
    laNodeOutSocket* Out;
};
STRUCTURE(laSplitNode){
    laBaseNode Base;
    laNodeInSocket* In;
    laSplitNodeOutSocket Out[8];
    int IntVal[8]; real RealVal[8];
    int ArrLen;
};
STRUCTURE(laSwitchNodeInSocket){
    laNodeInSocket* In;
};
STRUCTURE(laSwitchNode){
    laBaseNode Base;
    laSwitchNodeInSocket In[8];
    laNodeOutSocket* Out;
    laNodeInSocket* SwitchIn;
    real TempVal;
    int Switch;
    int MaxUsed;
    laRackPage* Page;
    uint64_t BranchSW;
};
STRUCTURE(laCombineNode){
    laBaseNode Base;
    laSwitchNodeInSocket In[8];
    laNodeOutSocket* Out;
    laNodeOutSocket* OutInt;
    laNodeOutSocket* OutEnum;
    real Values[8]; int ValuesI[8];
};
STRUCTURE(laValuesNode){
    laBaseNode Base;
    laSplitNodeOutSocket Out[8];
    real Values[8]; int ValuesI[8], ValuesE[8];
    int Modes[8];
};
STRUCTURE(laMatrixNode){
    laBaseNode Base;
    laNodeInSocket *InL,*InR;
    laNodeOutSocket* Out;
    tnsMatrix44d Mat;
    int Operation;
};
STRUCTURE(laMathNode){
    laBaseNode Base;
    laNodeInSocket *InL,*InR;
    laNodeOutSocket* Out,*OutInt;
    real Value; int ValueI;
    real ValueL, ValueR;
    int Operation;
};
STRUCTURE(laValueMapperPoint){
    laListItem Item;
    real x,y;
};
STRUCTURE(laValueMapper){
    real pad;
    laListHandle Points;
    real InRange[2],OutRange[2];
};
STRUCTURE(laMapperNode){
    laBaseNode Base;
    laNodeInSocket *In, *InMin, *InMax, *OutMin, *OutMax;
    laNodeOutSocket* Out;
    real rIn,rOut;
    laValueMapper* Mapper;
    int ClampInput, ClampOutput;
};
STRUCTURE(laRandomNode){
    laBaseNode Base;
    laNodeOutSocket* Out; real rOut;
    laNodeInSocket *InMin, *InMax;
    real Min,Max;
};
STRUCTURE(laVectorMathNode){
    laBaseNode Base;
    laNodeInSocket *InL,*InR;
    laNodeOutSocket* Out;
    real rOut[4];
    int Operation;
};
STRUCTURE(laCommentNode){
    laBaseNode Base;
    laSafeString* Content;
};
STRUCTURE(laRGB2OKHSLNode){
    laBaseNode Base;
    laNodeInSocket *In;
    laNodeOutSocket* OutH;
    laNodeOutSocket* OutS;
    laNodeOutSocket* OutL;
    real RGB[3];
    real rOut[3];
};
STRUCTURE(laOKHSL2RGBNode){
    laBaseNode Base;
    laNodeInSocket *InH;
    laNodeInSocket *InS;
    laNodeInSocket *InL;
    laNodeOutSocket* Out;
    real rOut[3];
};
STRUCTURE(laLoopNode){
    laBaseNode Base;
    laNodeInSocket *InIndex;
    laNodeInSocket *InIterations;
    laNodeInSocket *Branch;
    laNodeInSocket *Prev;
    laNodeOutSocket* Next;
    int Iterations;
    laRackPage* Page;
    uint64_t BranchControl;
};
STRUCTURE(laLoopIndexNode){
    laBaseNode Base;
    laNodeOutSocket* Out;
    int Iteration;
};

STRUCTURE(laNodeCategory){
    laListItem Item;
    laSafeString* Name;
    laListHandle NodeTypes;//lip
    laUiDefineFunc Ui;
    int For;
};

#define LA_RACK_TYPE_AUDIO (1<<2)

#define LA_SYNTH_PLEN 512

extern laPropContainer* LA_PC_SYNTH;

STRUCTURE(laSynth){
    laListItem Item;
    laSafeString* Name;
    laRackPage* Page;
    real Length;
    u16bit Poly,MaxPoly;

    SYSLOCK Lock;
    int Playing;
    real EvalTime;
    int EvalSamples;

    int PolyCount;
    laListHandle PolyphonicInstances;
};

STRUCTURE(laSynthNodeInput){
    laBaseNode Base;
    laNodeOutSocket* OutTime;
    laNodeOutSocket* OutTrigger;
    laNodeOutSocket* OutL;
    laNodeOutSocket* OutR;
    real* rTime;
    real* rTrigger;
    real* rSamplesL;
    real* rSamplesR;
};

STRUCTURE(laSynthNodePulse){
    laBaseNode Base;
    laNodeInSocket* InTrigger; int iTrigger;
    laNodeInSocket* InWidth; real rWidth,rInfluence;
    laNodeInSocket* InGate; real rGate;
    laNodeOutSocket* Out; real *rOut;
    int Triggered; real EvalSamples;
};

STRUCTURE(laSynthNodeFM){
    laBaseNode Base;
    int Slow;
    real Frequency,rInfluence;
    laNodeInSocket* InFrequency;
    laNodeOutSocket* Out;
    //runtime
    real Phase;
    real* OutSamples;
};

STRUCTURE(laSynthNodeNoise){
    laBaseNode Base;
    laNodeOutSocket* OutWhite;
    laNodeOutSocket* OutPink;
    real* OutWhiteSamples;
    real* OutPinkSamples;
    //runtime
    real PinkStates[3];
    real PinkA[3],PinkP[3];
};

STRUCTURE(laSynthNodeVCA){
    laBaseNode Base;
    real Amp, rInfluence;
    laNodeInSocket* In;
    laNodeInSocket* InAmp;
    laNodeOutSocket* Out;
    real* OutSamples;
};

STRUCTURE(laSynthNodeScope){
    laBaseNode Base;
    int Time;
    int Gain1,Gain2;
    real Offset1,Offset2;
    real Brightness1,Brightness2;
    laNodeInSocket* In1;
    laNodeInSocket* In2;
    real *Display1,*Display2; // each PLEN*2, upper/lower bounds.
    int NextSample;
    SYSLOCK Lock;
    laSynth* FromSynth;
};

NEED_STRUCTURE(laAudioChannel);

STRUCTURE(laSynthNodeOutput){
    laBaseNode Base;
    laNodeInSocket* InL,*InR; int IsMono;
    laSafeString* SendName;
    /* Runtime */
    laAudioChannel* Send;
};

STRUCTURE(laSynthNodeEnvelope){
    laBaseNode Base;
    laNodeInSocket* Trigger,*Gate; real rGate; int bTrigger; int Triggered;
    laNodeInSocket* Restart;
    laNodeInSocket* Attack;  real rAttack,iAttack;
    laNodeInSocket* Delay;   real rDelay,iDelay;
    laNodeInSocket* Sustain; real rSustain,iSustain;
    laNodeInSocket* Release; real rRelease,iRelease;
    laNodeOutSocket* Out; real* OutSamples;
    real Time,ReleaseTime,AtLevel;
};

STRUCTURE(laSynthNodeQuantize){
    laBaseNode Base;
    laNodeInSocket* In;
    laNodeOutSocket* Out;
    real* OutSamples;
    int EnabledBits[12];
};

STRUCTURE(laSynthNodeDriver){
    laBaseNode Base;
    laNodeInSocket* Prev; laNodeOutSocket* Next;
    laNodeInSocket* InTransport; int iTransport;
    laNodeInSocket* InReset; int iReset,AlreadyReset; // on rising edge
    laSynth* Target;
};

STRUCTURE(laSynthNodeTrigger){
    laBaseNode Base;
    laNodeInSocket* Prev; laNodeOutSocket* Next;
    laNodeInSocket* InTrigger; int Triggered,TriggerEdge,iTrigger;
    laSynth* Target;
};

#include "miniaudio.h"

STRUCTURE(laAudioChannel){
    laListItem Item;
    laSafeString* Name;
    real Volume;
    laSafeString* OutputChannel;
    real SamplesL[LA_SYNTH_PLEN];
    real SamplesR[LA_SYNTH_PLEN];
    int IsMono;
};

STRUCTURE(laAudioDevice){
    laListItem Item;
    int MiniAudioID;
    laSafeString Name;
};

typedef int (*laAudioCallbackF)(int Paused, const float* input, float* output, uint32_t framecount);
STRUCTURE(laAudio){
    int Paused;
    
    laListHandle Synths;
    laSynth* CurrentSynth;

    laListHandle Channels;

    laListHandle PolyLaterTrigger;

    SYSLOCK AudioStatusLock;
    ma_backend MiniAudioBackend;
    ma_context MiniAudioContext;
    ma_device AudioDevice;
    laAudioDevice* UsingDevice;
    laListHandle AudioDevices;
    laSynth* AudioEvalSynth;
    real AudioFrameInterval;
    real* InputSamplesL,*InputSamplesR;
    real *OutputSamplesL,*OutputSamplesR;
    int NextAudioSample,AudioSampleRate;//,NextInputSample;
    
    laAudioCallbackF Callback;
};

void laSetAudioCallback(laAudioCallbackF Callback);
int laget_SynthPlaying(laSynth* ss);
void* laget_FirstAudioDevice(void* unused1,void* unused2);
void* laget_FirstSynth(void* unused1,void* unused2);
void laset_CurrentSynth(laAudio* a,laSynth* s);
void laset_CurrentAudioDevice(laAudio* a,laAudioDevice* ad);

void laRemoveAudioChannel(laAudioChannel* ac);
laAudioChannel* laNewAudioChannel(char* Name);
laAudioChannel* laGetAudioChannel(char* Name);

void la_AudioPreFrame();

void laset_MiniAudioBackend(void* unused, int backend);

void laRebuildSynthGraphs(int current_only);
void laInitAudio();
void laDeinitAudio();

void laStartAudio();
void laPauseAudio();
void laStopAudio();
void laResetAudio();

laSynth* laFindSynth(const char* Name);
void la_SynthAddPolyInstance(laSynth* s);
void la_SynthRemovePolyInstance(laSynth* s, laSynth* inst);
void laSynthTriggerNew(laSynth* s);
void laSynthTriggerLater(laSynth* s);

#define LA_INPUT_CONTROLLER_NODE_MODE_BTN 0
#define LA_INPUT_CONTROLLER_NODE_MODE_AXIS 1

laValueMapper* laValueMapperInit();
void laValueMapperDestroy(laValueMapper* vm);
laValueMapper* laValueMapperCopy(laValueMapper* new_optional, laValueMapper* vm);
real laValueMapperEvaluate(laValueMapper* vm, real x, real* force_InMin, real* force_InMax, real* force_OutMin, real* force_OutMax, int ClampIn, int ClampOut);

#define LA_PROGRESS_REPAINT_MESSAGE (WM_APP+1)

void laShowProgress(real p1, real p2);
void laHideProgress();

void logPrintT(int Type, char* format, ...);
void logPrint(char* format, ...);
void logPrintNew(char* format, ...);
void logClear();

void laPerfRecordV(char* format, va_list v);
void laPerfRecord(char* format, ...);

#ifdef _WIN32
LRESULT CALLBACK LA_WindowProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
LRESULT CALLBACK LA_ProgressWindowProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
#endif

void la_RegisterMainOperators();
void la_RegisterUiOperatorsBasic();
void la_RegisterViewerOperators();
void la_RegisterBuiltinOperators();
void la_RegisterModellingOperators();
void la_RegisterUiTypesBasic();
void la_RegisterUiTypesViewerWidgets();
void la_RegisterBuiltinTemplates();
void la_RegisterWindowKeys();
void la_RegisterGeneralProps();
void la_RegisterInternalProps();
void la_RegisterDefaultSignals();

void la_RegisterAnimationResources();

void la_RegenerateWireColors();


void laui_IntPropInfo(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *ExtraColumns, int context);
void laui_FloatPropInfo(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *ExtraColumns, int context);

void laui_RackPage(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context);
void laui_DefaultPropDetails(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context);
void laui_DefaultNodeOperationsPropUiDefine(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED, int context);
void laui_DefaultPropUiDefine(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED, int context);
void laui_StringPropUiDefine(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context);
void laui_IdentifierOnly(laUiList *uil, laPropPack *This, laPropPack *OP_UNUSED, laColumn *Extra, int context);
void laui_SubPropInfoDefault(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_SubPropSelection(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_LinkerSelectionProp(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_LinkerSelectionInstance(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_DefaultOperatorPanelTitleBar(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_ColumnItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context);
void laui_DefaultMenuButtonsFileEntries(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context);
void laui_DefaultMenuButtonsEditEntries(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context);
void laui_DefaultMenuButtonsOptionEntries(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context);
void laui_DefaultMenuButtons(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context);
void laui_DefaultMenuExtras(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context);
void laui_DefaultMenuBarActual(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context);
void laui_DefaultSubWindowMenuBarActual(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context);
void laui_ThemeListItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context);
void laui_Theme(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context);
void laui_ThemePreview(laUiList *uil, laPropPack *Base, laPropPack *This, laColumn *UNUSED_Colums, int context);
void laui_BoxedThemeItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context);
void laui_PropertyContainerList(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context);
void laui_WindowListItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context);
void laui_LayoutListItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context);
void laui_PanelListItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context);
void laui_InputSocketItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context);
void laui_OutputSocketItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context);
void laui_FileBrowserFileItem(laUiList *uil, laPropPack *This, laPropPack *OP_UNUSED, laColumn *Extra, int context);
void laui_FileBrowserDiskItem(laUiList *uil, laPropPack *This, laPropPack *OP_UNUSED, laColumn *Extra, int context);
void laui_LinkerPanel(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_ManagedUDFItem(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_ManagedUDFOps(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_ManagedProp(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_ManagedSavePanel(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_ManagedUDFPanel(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_OperatorTypeEntry(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *ExtraColumns, int context);
void laui_FileBrowserFileList(laUiList *uil, laPropPack *THIS_UNUSED, laPropPack *Operator, laColumn *UNUSED, int context);
void laui_DataRestorePage(laUiList *uil, laPropPack *THIS_UNUSED, laPropPack *Operator, laColumn *UNUSED, int context);
void laui_ResourceFolderItem(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_LogItem(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);
void laui_NodeCategory(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context);

void tnsui_CameraObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context);
void tnsui_LightObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context);
void tnsui_MeshObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context);
void tnsui_ShapeObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context);
void tnsui_InstancerObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context);
void tnsui_BaseObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context);
void tnsui_RootObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context);

void tnsui_RootObjectMenuUi(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED, int context);

int OPINV_UiItem(laOperator *a, laEvent *e);
int OPEXT_UiItem(laOperator *a, int ExitCode);

int la_ArrayGetHeight(laUiItem *ui);
int la_ValueGetHeight(laUiItem*ui);
int la_3DViewGetHeight(laUiItem *ui);
int la_CanvasGetHeight(laUiItem *ui);
int la_EnumGetHeight(laUiItem *ui);
int la_ColorPickerGetHeight(laUiItem *ui);
int la_SymbolGetHeight(laUiItem *ui);
int la_GroupGetHeight(laUiItem *ui);

void la_DrawBorder(int L, int R, int U, int B);
void la_DrawBox(int L, int R, int U, int B);
void la_DrawOuterShadowLength(int L, int R, int U, int B, int sw);
void la_DrawOuterShadow(int L, int R, int U, int B);
void la_DrawInnerShadowLength(int L, int R, int U, int B, int sw);
void la_DrawInnerShadow(int L, int R, int U, int B);
void la_DrawBoxAutoArrayStyle(int L, int R, int U, int B, laBoxedTheme* bt, int State, real* ReplaceColor, int IsVertical, int IsFirst, int BoxStyle);
void la_DrawBoxAutoArray(int L, int R, int U, int B, laBoxedTheme* bt, int State, real* ReplaceColor, int IsVertical, int IsFirst);
void la_DrawBoxAuto(int L, int R, int U, int B, laBoxedTheme* bt, int State, real* ReplaceColor);
void la_DrawBoxAutoFillArray(int L, int R, int U, int B, laBoxedTheme* bt, int State, real* ReplaceColor, int IsVertical, int IsFirst);
void la_DrawBoxAutoFill(int L, int R, int U, int B, laBoxedTheme* bt, int State, real* ReplaceColor);
void la_DrawBoxAutoBorderArray(int L, int R, int U, int B, laBoxedTheme* bt, int State, int IsVertical, int IsFirst);
void la_DrawBoxAutoBorder(int L, int R, int U, int B, laBoxedTheme* bt, int State);
int la_GetBoxOffset(laBoxedTheme* bt, int State);

laPanel *la_FindFreePanelByTemplate(laWindow *w, const laUiTemplate *uit);
laUiItem *la_FindUiWithMark(laUiList *uil, char *mark);
laUiList *la_FindSubListWithInstance(laUiItem *ui, void *Instance);

void* laget_CurrentRackPage(laRackPageCollection* c);
void laset_CurrentRackPage(laRackPageCollection* c,laRackPage* rp);

void la_BeginNVG(struct NVGcontext* vg,laCanvasExtra* e,int W,int H, int Is2D);
void la_SetCanvasOrtho(laCanvasExtra* e,int W, int H);

#define LA_FILETYPE_UNKNOWN 0
#define LA_FILETYPE_UDF 1
#define LA_FILETYPE_DOCUMENT 2
#define LA_FILETYPE_IMAGE 3
#define LA_FILETYPE_AUDIO 4
#define LA_FILETYPE_VIDEO 5
#define LA_FILETYPE_COMPRESSED 6
#define LA_FILETYPE_FONT 7
#define LA_FILETYPE_VECTOR 8
#define LA_FILETYPE_WEBPAGE 9
#define LA_FILETYPE_META 10
#define LA_FILETYPE_BLEND 11
#define LA_FILETYPE_PDF 13
#define LA_FILETYPE_EXEC 14
#define LA_FILETYPE_SYS 15
#define LA_FILETYPE_FOLDER 16
#define LA_FILETYPE_LASDEXCHANGE 17

#define LA_FILE_SELECT_FILE 0
#define LA_FILE_SELECT_FOLDER 1

STRUCTURE(laExtensionType){
    laListItem Item;
    int FileType;
    const char* Extension;
};

STRUCTURE(laFileItem){
    laListItem Hyper;
    int IsFolder;
    char Name[1024];
    int Size;
    uint64_t Timestamp;
    laTimeInfo TimeModified;
    int Type;
};

STRUCTURE(laDiskItem){
    laListItem Hyper;
    char Path[1024];
    real Total_GB;
    real Free_GB;
};
STRUCTURE(laBookmarkedFolder){
    laListItem Item;
    char Name[128];
    char Path[1024];
};

#define LA_FILE_SORT_NAME     0
#define LA_FILE_SORT_NAME_REV 1
#define LA_FILE_SORT_TIME     2
#define LA_FILE_SORT_TIME_REV 3
#define LA_FILE_SORT_SIZE     4
#define LA_FILE_SORT_SIZE_REV 5

STRUCTURE(laFileBrowser){
    laListItem Hyper;
    char Path[2048];
    char FileName[512];
    char TempStr[512];
    char MD5[128];
    char FilterName[512];
    tnsImage* Thumbnail;
    laDiskItem *RootDisk;
    laFileItem *Active;
    laListHandle FileList;
    laListHandle Bookmarks;
    laListHandle Disks;

    int SelectFolder;
    int WarnFileExists;
    int StatusWaitingWarning;
    char UseExtension[64];
    int UseType;
    int FilterType;
    int ShowBackups;
    int ShowThumbnail;
    int SortBy;
    laStringSplitor* ss_filter_extensions;
};

STRUCTURE(laUDFPreviewExtra){
    laUDF *UDF;
    int Opened;
    int Append;
};

STRUCTURE(laManagedSaveExtra){
    int ShowPage;
    int OnExit;
};

#define LA_UDF_SHARE_POINTER(id) \
    la_UDFAppendSharedTypePointer("" #id, id)

void la_MakeTranslations_zh_hans();
void la_MakeTranslations_es_ES();

laCanvasTemplate *la_GetCanvasTemplate(char *TargetContainerID, char* TemplateID);
laPropContainer* laUiHasExtraProps(laUiType *ut, int size, int Hyper);
laPropContainer* laCanvasHasExtraProps(laCanvasTemplate *ct, int size, int Hyper);
laUiType *la_RegisterUiType(const char *Identifer, int ForType, const char *UseOperator, laBoxedTheme **bt, laUiDrawFunc *Draw, laUiGetHeightFunc GetHeight, laUiInitFunc Init, laUiDestroyFunc Destroy);

void la_NotifyGLDebugChanges();

void laSetWindowCursor(int id);
void laRenameWindow(laWindow* wnd, char* name);

#ifdef LA_LINUX
int la_GetDPI(Window* root_win);
void la_ScanWacomDevices(SYSTEMDISPLAY *display, int deviceid);
#endif
#ifdef _WIN32
int la_GetDPI(HWND win);
void la_OpenWacomWinTab(HWND hwnd);
#endif

void laSetFontFolderPath(char* absolute);
void laSetDefaultInitArguments(laInitArguments* ia);
void laSetCompleteInitArguments(laInitArguments* ia);
void laProcessInitArguments(int argc, char* argv[],laInitArguments* ia);
int laGetReady();
int laGetReadyWith(laInitArguments* ia);
void laShutoff(int SavePreferences);
int laRestoreFactorySettings();
void laSaveUserPreferences();
void laEnsureUserPreferences();
void laAddExtraExtension(int FileType, ...);
void laAddExtraPreferencePath(const char* path);
void laAddExtraPreferencePage(const char* name, laUiDefineFunc Func);
void laSetFrameCallbacks(laPreFrameF PreFrame, laPreDrawF PreDraw, laPostFrameF PostFrame);
void laSetCleanupCallback(laCleanupF Cleanup);
void laSetInputProcessCallback(laInputProcessF InputProcess);
void laSetProofingLut(void* data, int which);
void laSetMenuBarTemplates(laUiDefineFunc MenuButtons, laUiDefineFunc MenuExtras, const char* ProgramName);
void laSetAboutTemplates(laUiDefineFunc AboutContent, laUiDefineFunc AboutVersion, laUiDefineFunc AboutAuthor);
void laSetPreferenceTemplates(laUiDefineFunc PreferencePageDisplay, laUiDefineFunc PreferencePageInput, laUiDefineFunc PreferencePageResource, laUiDefineFunc PreferencePageTheme);

void la_InputMappingGetKeyName(int key, int special, char* name);
int la_InputMappingGetKeyFromName(char* name, int* special);
laCustomSignal* laFindSignal(char* Name);
laCustomSignal* laFindSignalByID(int ID);
void laInputMappingUpdateSignal(laInputMappingEntry* ime);
laInputMapping* laNewInputMapping(char* Name);
laInputMappingEntry* laNewInputMappingEntry(laInputMapping* im, int DeviceType, int JoystickDevice, char* Key, int SpecialKeyBit, char* Signal);
laInputMappingEntry* laNewInputMappingEntryP(laInputMapping* im, int DeviceType, int JoystickDevice, char* Key, int SpecialKeyBit, int Signal);
laInputMappingEntry* laNewInputMappingMouseEntryP(laInputMapping* im, int MouseEventType, int SpecialKeyBit, int Signal);
void laRemoveInputMappingEntry(laInputMapping* im, laInputMappingEntry* e);
void laRemoveInputMapping(laInputMapping* im);

laInputMapping* laNewToolbox(char* Name);
void laRemoveToolbox(laInputMapping* im);

laCustomSignal* laNewCustomSignal(char* Name, int Signal);
void laRemoveCustomSignal(laCustomSignal* cs);

void la_RemoveScreen(laScreen*s);
laScreen* laGetWindowScreen(laWindow* w);
laWindow *laDesignWindow(int X, int Y, int W, int H);
laLayout *laDesignLayout(laWindow *w, char *Title);
void laDestroyLayout(laWindow *w, laLayout* l);
void laFoldBlockTitle(laBlock* b);
void laUnfoldBlockTitle(laBlock* b);
void laMaximizeBlock(laBlock* b);
void laRestoreToLayout();
void laMaximizeCanvasUI(laUiItem* ui, laPanel* UiParentPanel);
void laRestoreCanvasUI();
void laHideMenuBar();
void laShowMenuBar();
void laSplitBlockHorizon(laBlock *b, real Percentage);
void laSplitBlockVertical(laBlock *b, real Percentage);
void laCombineChildBlocks(laBlock *b);
laBlock *laBlock1(laBlock *b);
laBlock *laBlock2(laBlock *b);
void laSwapSubBlocks(laBlock *b);
void laDestroyBlocksRecursive(laBlock *Root);
laPanel* laTearOffPanel(laBlock* b, laPanel* p_if_set);
void laDockPanel(laWindow* from, laBlock* b, laPanel* p);
laPanel *laCreatePanel(laBlock *b, char *template_id);
laPanel *laCreateTopPanel(laWindow *w, char *template_id, int X, int Y, int W, int H, int MaxW, int MaxH, int MinW, int MinH, int SnapL, int SnapR, int SnapT, int SnapB);
void laShowPanel(laPanel *p);
void laShowPanelWithDropDownEffect(laPanel *p);
void laShowPanelWithExpandEffect(laPanel *p);
void laHidePanel(laPanel *p);
void laHidePanelWithDissoveEffect(laPanel *p);
void laActivatePanel(char* TemplateID, int x, int y);
void laPanPanel(laPanel *p, int DeltaX, int DeltaY);
void laPanUiListFree(laUiList *uil, int X, int Y);
int laPanUiList(laUiList *uil, int X, int Y, int L, int R, int U, int B);
int laPanUiListAuto(laUiList *uil, int X, int Y, int L, int R, int U, int B);
int laScaleUiList(laUiList *uil, real factor, int L, int R, int U, int B);
laPanel *laDesignPropPanel(char *Title, int X, int Y, int W, int H,
                           laUiDefineFunc Define, laPropPack *This, laPropPack *OperatorProps);
laPanel *laDesignOperatorPanel(char *Title, int X, int Y, int W, int H, int MaxW, int MaxH, int MinW, int MinH, int SnapL, int SnapR, int SnapT, int SnapB,
                               laUiDefineFunc Define, laPropPack *This, laPropPack *OperatorProps);
void laDeferedDestroyPanel(laPanel *p);
void laDestroySinglePanel(laPanel *p, int immediate);
void laEnsurePanelInBound(laPanel *p,laUiList* uil);
int laEnclosePanelContent(laPanel *p, laUiList *uil);
laPanel *laEnableIdlePanel(laPanel *Attachment, laOperator *a, laPropPack *OperatorProps, laUiDefineFunc ReplaceUiDefine, laPropPack *This,
                               int L, int R, int B, int MaxGH, int MaxW, laEvent *e);
laPanel *laEnableSplashPanel(laUiDefineFunc ReplaceUiDefine, int L, int R, int B, int MaxGH, int MaxW, laEvent* e);
laPanel *laEnablePropertyPanel(laPanel *Attachment, laOperator *a, laPropPack *OperatorProps, laUiDefineFunc ReplaceUiDefine, laUiDefineFunc FallBackUiDefine, laPropPack *This,
                               int L, int R, int B, int MaxGH, int MaxW, laEvent *e);
laPanel *laEnableEmptyPropertyPanel(laPanel *Attachment, laOperator *a, int L, int R, int U, int MaxGH, laEvent *e);
laPanel *laEnableMenuPanel(laPanel *Attachment, laOperator *a, laUiList *MenuRefer, laPropPack *This,
                           int L, int R, int B, int MaxGH, int MaxW, laEvent *e);
laPanel *laDefineAndEnableMenuPanel(laPanel *Attachment, laOperator *a, laPropPack *This,
                                    int L, int B, int MaxGH, int MaxW, laEvent *e);
laPanel *laEnableOperatorPanel(laOperator *For, laPropPack *This, int X, int Y, int W, int H, int MaxW, int MaxH, int MinW, int MinH, int SnapL, int SnapR, int SnapT, int SnapB, laEvent *e);
laPanel *laEnableYesNoPanel(laOperator *a, laPanel *Attachment, char *Title, char *Message, int X, int Y, int W, laEvent *e);
laPanel *laEnableMessagePanel(laOperator *a, laPanel *Attachment, char *Title, char *Message, int X, int Y, int W, laEvent *e);

void laOperatorModalOver(laOperator* For);

void la_PropPanelUserRemover(void* This, laItemUserLinker* iul);

void la_EnsurePanelExtras(laPanel *p);

void laDeferredRedraw(laPanel* p);
void laRedrawAllWindows();
void laRefreshWindow();
void laRedrawCurrentWindow();
void laRedrawPanel(laPanel* p);
void laRecalcPanel(laPanel* p);
void laRedrawCurrentPanel();
void laRecalcCurrentPanel();
void laRecalcPanelImmediate(laPanel *p);
void laRecalcCurrentPanelImmediate();
int laNonFixedPanelExists(laPanel *p);
int laIsInPanel(laPanel *p, int PanelX, int PanelY);
int laIsCloseToPanel(laPanel *p, int PanelX, int PanelY);
void laPopPanel(laPanel *p);
int laPanelOverlappingEachOther(laPanel *p1, laPanel *p2);
void laUnlinkSharedPanel(laPanel *p);
int laIsPanelCovered(laPanel *p);
int laIsTopPanel(laPanel *p);
laPanel *laDetectPanel(int X, int Y);
void laWindowToLocal(laOperator *a, laPanel *p, int *x, int *y);
void laPanelToLocal(laOperator *a, int *x, int *y);
void laLocalToWindow(laOperator *a, laPanel *p, int *x, int *y);
void laLocalToPanel(laOperator *a, int *x, int *y);
void laSetNextMenuPos(int X, int Y, int W, int H);
int laIsInBlock(laBlock *b, int X, int Y);
int laIsInBlockHeader(laBlock *b, int X, int Y);
int laIsInBlockBotton1(laBlock *b, int X, int Y);
int laIsInBlockBotton2(laBlock *b, int X, int Y);
laBlock *laDetectBlockRecursive(laBlock *b, int X, int Y);

void la_DoUiScissor(laUiItem* ui, int* savex, int* savey, int* savew, int* saveh,
    int* savel, int* saver, int* saveu, int* saveb);

laUiList *laPrepareUi(laPanel *p);
laColumn *laFirstColumn(laUiList *uil);
laColumn *laSplitColumn(laUiList *uil, laColumn *c, real Percent);
laColumn *laSplitColumnDescriptor(laUiDescriptor *ud, laColumn *c, real Percent);
laColumn *laLeftColumn(laColumn *c, int MaxWidth);
laColumn *laRightColumn(laColumn *c, int MaxWidth);

int laCheckAndMergeSubColumnsUiList(laUiList *TopUil, laColumn *c, int DoMerge);
int laCheckAndMergeSubColumnsDescriptor(laUiDescriptor *TopDescriptor, laColumn *c, int DoMerge);

laUiConditionNode *laTrue();
laUiConditionNode *laFalse();
laUiConditionNode *laPropExpression(laPropPack *Base, char *Prop);
laUiConditionNode *laIntExpression(int Value);
laUiConditionNode *laFloatExpression(real Value);
laUiConditionNode *laStringExpression(char *Content);
laUiConditionNode *laAnd(laUiConditionNode *Expression1, laUiConditionNode *Expression2);
laUiConditionNode *laOr(laUiConditionNode *Expression1, laUiConditionNode *Expression2);
laUiConditionNode *laNot(laUiConditionNode *Expression1);
laUiConditionNode *laEqual(laUiConditionNode *Expression1, laUiConditionNode *Expression2);
laUiConditionNode *laGreaterThan(laUiConditionNode *Expression1, laUiConditionNode *Expression2);
laUiConditionNode *laLessThan(laUiConditionNode *Expression1, laUiConditionNode *Expression2);
laUiConditionNode *laGreaterEqual(laUiConditionNode *Expression1, laUiConditionNode *Expression2);
laUiConditionNode *laLessEqual(laUiConditionNode *Expression1, laUiConditionNode *Expression2);

laUiItem *laShowLabel(laUiList *uil, laColumn *c, const char *Content, laUiDefineFunc Template, laWidget* Widget);
laUiItem *laShowLabelDynamic(laUiList *uil, laColumn *c, const char *Content, laUiDefineFunc Template, laWidget* Widget);
laUiItem *laShowIcon(laUiList *uil, laColumn *c, laPropPack *Base, const char *Path, laWidget* Widget);
laUiItem *laShowInvisibleItem(laUiList *uil, laColumn *c, laPropPack *Base, const char *Path);
laUiItem *laShowItem(laUiList *uil, laColumn *c, laPropPack *Base, const char *Path);
laUiItem *laShowItemFull(laUiList *uil, laColumn *c, laPropPack *Base, const char *Path, laWidget* Widget, char* instructions, laUiDefineFunc Template, int TemplateContext);
laUiItem *laShowImage(laUiList *uil, laColumn *c, tnsImage* Image, int Height);
laUiItem *laShowNodeSocket(laUiList *uil, laColumn *c, laPropPack *Base, const char *Path, char* instructions);
laUiItem *laShowHeightAdjuster(laUiList *uil, laColumn *c, laPropPack *Base, const char *Path, char* instructions);
laUiItem *laShowDetachedItem(laPanel *p, laUiList *uil, laColumn *c, laPropPack *Base, const char *Path, const char *Rename, laUiDefineFunc Template, laWidget* Widget);
laUiItem *laShowCanvas(laUiList *uil, laColumn *c, laPropPack *Base, const char *Path, const char *id2DTemplate, int Height);
laUiItem *laShow3DCanvasCombo(laUiList *uil, laColumn *c, laPropPack *Base, const char *Path, int Height, laPropPack* Detached);
laUiItem *laShow2DContainerItem(laUiList *uil, laColumn *c, laPropPack *Base, const char *Path, int Height);
void laDefault3DViewOverlay(laUiItem *ui);
void laDefault2DViewOverlayRight(laUiItem *ui);
laUiItem *laShowColumnAdjuster(laUiList *uil, laColumn *c);
laPropPack* laShowCompoundValue(laUiItem* ui, int slot, laPropPack *Base, const char *Path);
laUiItem *laShowMouseActionReporter(laUiList* uil, laColumn* c, int Height, const char* Display, const char* instructions); 
laUiItem *laBeginRow(laUiList *uil, laColumn *c, int Expand, int Even);
laUiItem *laEndRow(laUiList *uil, laUiItem* Begin);
laUiItem *laMakeGroup(laUiList *uil, laColumn *c, const char *Name, laWidget* Widget);
laUiItem *laMakeFoldableGroup(laUiList *uil, laColumn *c, const char *Name, laWidget* Widget, int DefaultFolded, int ButtonFlags);
laUiItem *laMakeEmptyGroup(laUiList *uil, laColumn *c, const char *Name, laWidget* Widget);
void laEndFoldableGroup(laUiList *sub, laUiItem *group);
laUiItem *laMakeTab(laUiList *uil, laColumn *c, laWidget* Widget);
laUiList *laAddTabPage(laUiItem *ui, const char *Name);
laUiItem *laOnConditionThat(laUiList *uil, laColumn *c, laUiConditionNode *Expression);
laUiItem *laOnConditionToggle(laUiList *uil, laColumn *col, laUiDefineFunc define, int Remove, laPropPack *ExtraBase, laPropPack *ExtraThis, laWidget* Widget);
laUiItem *laEndCondition(laUiList *uil, laUiItem *Beginner);
laUiItem *laElse(laUiList *uil, laUiItem *Beginner);
laUiList* laMakeMenuPageEx(laUiList* uil, laColumn* c, const char* Title, int flags);
laUiList *laMakeMenuPage(laUiList *uil, laColumn *c, const char *Title);
laUiItem *laShowSeparator(laUiList *uil, laColumn *widest);
void laFixHeight(laUiList *uil, short Rows);

void la_ConditionerInit(laUiItem *ui, laUiConditionNode *Expression);

void laMakeExtraColumns(laUiItem *ui, u8bit HowMany, ...);

int la_UiInBoundEx(laUiItem *ui, laUiListDraw *uild);

void la_ClearUiSpecificData(laUiItem *ui);
laUiItem *la_UpdatePropDisplay(laUiItem *ui, laPropPack *Base, const char *Path, laUiDefineFunc Template, laWidget* Widget, char* instructions);
laUiItem *la_UpdateLabelDisplay(laUiItem *ui, laUiDefineFunc Template, char *Content);
laUiItem *la_ShowGeneralPropItem(laUiList *uil, laColumn *c, laPropPack *Base, laProp *GeneralProp, laProp *p, laUiDefineFunc Template, laWidget* Widget);

void laFinalizeUiTemplates();
laUiTemplate *laFindUiTemplate(char *Identifier);

void la_DestroyUiTemplate(laUiTemplate* uit);
void la_DestroyCanvasTemplate(laCanvasTemplate* uit);
laUiTemplate *laRegisterUiTemplate(char *Identifier, char* Title, laUiDefineFunc func,laPanelDetachedPropFunc PropFunc, laUiDefineFunc header, char* NewCategory, int DefaultGLFormat, int DefaultW_RH, int DefaultH_RH);
laCanvasTemplate *laRegisterCanvasTemplate(char *Identifier, char *ForContainer, laModalFunc ExtraModal, laCanvasDrawFunc Func, laUiDrawFunc SecondDraw, laUiInitFunc CustomInit, laUiDestroyFunc CustomDestroy);

laTheme *laDuplicateTheme(laTheme* from);
void la_DestroyTheme(laTheme* t);
laTheme *laDesignTheme(const char *Name);
laBoxedTheme *laDesignBoxedTheme(laTheme *t, const char *Name, laBoxedTheme** BackRef, int ColorSelection,
                                 real NormalY,real ActiveY,real BorderY,real TextY,real TextActiveY, real Alpha,
                                 int BoxStyle, int TextShadow);
laTheme *laGetTheme(const char *ThemeName);
laBoxedTheme *laGetBoxedTheme(const char *ThemeName, const char *BoxName);
real* laThemeColor(laBoxedTheme* bt, int which);
real* laAccentColor(int which);

laTheme* la_CreateClassicLightTheme();
laTheme* la_CreateClassicDarkTheme();
void la_RefreshBoxedThemeColor(laBoxedTheme* bt);
void la_RefreshThemeColorSelf(laTheme* th);
void la_RefreshThemeColor(laTheme* th);

#define LA_COLOR4(c) (c)[0], (c)[1], (c)[2], (c)[3]
#define LA_COLOR3(c) (c)[0], (c)[1], (c)[2]
#define LA_VEC2(c) (c)[0], (c)[1]
#define LA_SET3(c, a1,a2,a3) { (c)[0]=a1; (c)[1]=a2; (c)[2]=a3; }
#define LA_COLOR3_TO_HEX(c) (((int)((c)[0]*255)<<16)|((int)((c)[1]*255)<<8)|((int)((c)[2]*255)<<0))
#define LA_COLOR3_TO_RGB(c) (RGB((int)((c)[0]*255),(int)((c)[1]*255),(int)((c)[2]*255)))

int laIsInUiItem(laUiItem *ui, laOperator* a, int x, int y);
int laIsInBound(int x, int y, int l, int r, int u, int b);
laUiItem *la_DetectUiItemRecursive(laUiList *uil, int x, int y, int LimB, laListHandle *LocalBuf, int Deep);
laUiItem *la_DetectSocketRecursive(laUiList* uil, int x, int y, int LimB, laPropContainer* PCInOrOut);
laUiList *la_DetectUiListRecursive(laUiList *uil, int x, int y, int LimH,
                                   laUiItem **ParentUi, laUiList **ScrollUil, laUiList **ContainerParent, int InToContainerUI, laUiItem *Exception);
laUiList *la_DetectUiListRecursiveDeep(laUiList *uil, int x, int y, int LimH, laUiItem **ParentUi, laUiList **ScrollUil,
                                       laUiList **ContainerParent, int InToContainerUI, laUiItem *Exception, laListHandle* levels);

void laStartWindow(laWindow *w);
void laMainLoop();

void la_DestroyOperatorType(laOperatorType* at);
laOperatorType *laCreateOperatorType(const char *ID, const char *Name, const char *Description,
                                     laCheckFunc Check, laInitFunc Init, laExitFunc Exit, laInvokeFunc Invoke, laModalFunc Modal,
                                     uint32_t IconID, int ExtraMark);

laPropContainer* laDefineOperatorProps(laOperatorType* ot, int HyperLevel);

#define LA_VERIFY_THIS_TYPE(this,pc,pc_identifier) \
    ((this)&&(this)->EndInstance&& \
    (this)->LastPs->p->PropertyType==LA_PROP_SUB&& \
    ((pc)=la_EnsureSubTarget((this)->LastPs->p,(this)->EndInstance))&&strSame((pc)->Identifier,pc_identifier))

laNodeOutSocket* laCreateOutSocket(void* NodeParentOptional, char* label, int DataType);
laNodeInSocket* laCreateInSocket(char* label, int DataType);
void laDestroyInSocket(laNodeInSocket* s);
void laDestroyOutSocket(laNodeOutSocket* s);
laNodeCategory* laEnsureNodeCategory(char* Name,laUiDefineFunc* Ui,int ForRackTypes);
void laNodeCategoryAddNodeTypes(laNodeCategory* nc, ...);
void laDestroyRack(laNodeRack* rr);
laRackPage* laDuplicateRackPage(laRackPage* new_optional, laRackPage* from);


void laFreeKeyMapItem(laKeyMapItem* kmi);
laKeyMapItem *laAssignNewKey(laKeyMapper *km, char *Path, char *Operation, char SelectBase, int SpecialKeyBits, int EventType, int Key, char *ExtraInstructions);
int laKeyMapExecuteEvent(laOperator *from, laKeyMapper *km, laEvent *e);
int laKeyMapExecuteEventEx(laOperator *from, laPropPack *UiExtra, laKeyMapper *km, laEvent *e);

laOperatorType *laGetOperatorType(const char *ID);
void laSetOperatorLocalizer(void *ToPanel);

int laOperatorAvailable(char *ID, laPropPack *This, laStringSplitor *Instructions);
int laOperatorAvailableP(laOperatorType *at, laPropPack *This, laStringSplitor *Instructions);
int laOperatorAvailableSafe(char *ID, laPropPack *This, void *Real_FromInstance, laStringSplitor *Instructions);
int laOperatorAvailablePSafe(laOperatorType *at, laPropPack *This, void *Real_FromInstance, laStringSplitor *Instructions);

void laSendOperatorTriggerEvent();
void laRetriggerOperators();

int la_GenericTopPanelProcessing(laOperator* a, laEvent* e);

int OPMOD_FinishOnData(laOperator* a, laEvent* e);
void OPEXT_FreeUserData(laOperator* a, int mark_unused);

void laHideCursor();
void laShowCursor();

int la_UiOperatorExists(void *inst);
int laOperatorExistsT(laOperatorType* at);
int laOperatorExists(const char* ID);
int laInvokeUi(laOperator *From, char *ID, laEvent *e, void *inst, laListHandle *Locals, int IgnoreLocals);
int laInvokeUiP(laOperator *From, laOperatorType *at, laEvent *e, void *inst, laListHandle *Locals, int IgnoreLocals);
int laInvoke(laOperator *From, char *ID, laEvent *e, laPropPack *This, char *args, char *args2);
int laInvokeP(laOperator *From, laOperatorType *at, laEvent *e, laPropPack *This, char *args, char *args2);
int laInvokePCreateThis(laOperator *From, laOperatorType *at, laEvent *e, laPropPack *OrigionalThis, void *FromInstance, char *args, char *args2);
void laRequestDelayEvent(real Seconds);

int la_UiStillInService(void *UiInstance);
void la_StopUiOperatorService(void *UiInstance);
void la_StopAllOperators();

int la_ProcessTextEdit(laEvent *e, laStringEdit *se, laUiItem* ui);

void laConfirmPointer(laOperator *a, void* Data,laPropContainer* PointerType, int mode);
void laConfirmInt(laOperator *a, int Data, int mode);
void laConfirmFloat(laOperator *a, real Data, int mode);
void laConfirmString(laOperator *a, char *Str, int mode);
int laConfirmSameDataIfAny(laOperator *a);
void laConfirmUserData(laOperator *a, void *UserData, laConfirmDataDestroyFunc Destroy, int mode);
int laGetConfirmInt(laOperator *a);
real laGetConfirmFloat(laOperator *a);
void laGetConfirmString(laOperator *a, char *buf);
void *laGetConfirmUserData(laOperator *a);
//int laInvokeSub(laProp* From, char * ID, laEvent* e, void* inst);

void la_DisplayKeyboard(bool pShow);
void la_HideNavBar();

void laFinalizeOperators();

int laWaitFor(laOperator *Who, int State);

void la_PrintOperatorStack();

void laui_DefaultPanelTitleBar(laUiList *uil, laPropPack *Base, laPropPack *Extra, laUiDefineFunc header);
void laui_DefaultOperatorPanelTitleBar(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context);;
void laui_DefaultSubWindowMenuBarActual(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context);;
void laui_DefaultMenuBarActual(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context);;
void laui_DefaultMenuBar(laWindow *w);
void laui_DefaultSubWindowMenuBar(laWindow *w);
void laui_DefaultPropUiDefine(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED, int context);;
void laui_PropOperatorUiDefine(laUiList *uil, laPropPack *This, laPropPack *Unused, laColumn *UNUSED, int context);

int laCopyToClipboard(unsigned char * text);
void laDisableIME(laWindow* w);
void laEnableIME(laWindow* w);

#ifdef LAGUI_ANDROID
bool la_check_permission(const char* permission);
void la_request_permission(const char* permission);
extern LA MAIN; // because of progress bar
#define LA_ACQUIRE_GLES_CONTEXT \
        eglMakeCurrent(MAIN.egl_dpy,MAIN.egl_surf,MAIN.egl_surf,MAIN.glc)
#define LA_LEAVE_GLES_CONTEXT \
        eglMakeCurrent(MAIN.egl_dpy,0,0,0)
#endif

#ifdef _WIN32
#define LA_ACQUIRE_DC \
    MAIN.Progress.hdc=GetDC(MAIN.Progress.w);
#define LA_LEAVE_DC \
    ReleaseDC(MAIN.Progress.w, MAIN.Progress.hdc);
#endif

#ifndef LA_ACQUIRE_DC
#define LA_ACQUIRE_DC
#endif
#ifndef LA_LEAVE_DC
#define LA_LEAVE_DC
#endif
#ifndef LA_ACQUIRE_GLES_CONTEXT
#define LA_ACQUIRE_GLES_CONTEXT
#endif
#ifndef LA_LEAVE_GLES_CONTEXT
#define LA_LEAVE_GLES_CONTEXT
#endif


extern laUiType *_LA_UI_FIXED_GROUP;
extern laUiType *_LA_UI_TAB;
extern laUiType _LA_UI_CONDITION;      //NO PTR
extern laUiType _LA_UI_CONDITION_END;  //NO PTR
extern laUiType _LA_UI_CONDITION_ELSE; //NO PTR
extern laUiType _LA_UI_ROW_BEGIN;      //NO PTR
extern laUiType _LA_UI_ROW_END;        //NO PTR
extern laUiType _LA_UI_INVISIBLE;      //NO PTR
extern laUiType *_LA_UI_COLLECTION;
extern laUiType *_LA_UI_COLLECTION_ITEM;
extern laUiType *_LA_UI_COLLECTION_SELECTOR;
extern laUiType *_LA_UI_COLLECTION_SINGLE;
extern laUiType *_LA_UI_BUTTON;
extern laUiType *_LA_UI_LABEL;
extern laUiType *_LA_UI_INT;
extern laUiType *_LA_UI_METER_TYPE1;
extern laUiType *_LA_UI_METER_TYPE2;
extern laUiType *_LA_UI_VALUE_METER_2D;
extern laUiType *_LA_UI_FLOAT;
extern laUiType *_LA_UI_FLOAT_COLOR;
extern laUiType *_LA_UI_FLOAT_COLOR_HCY;
extern laUiType *_LA_UI_ENUM_SELECTOR;
extern laUiType *_LA_UI_MENU_ROOT;
extern laUiType *_LA_UI_CONDITION_TOGGLE;
extern laUiType *_LA_UI_STRING;
extern laUiType *_LA_UI_STRING_MULTI;
extern laUiType *_LA_UI_ALIGN;
extern laUiType *_LA_UI_CANVAS;
extern laUiType *_LA_UI_COLUMN_ADJUSTER;
extern laUiType *_LA_UI_COLUMN_VIEWER;
extern laUiType *_LA_UI_NODE_SOCKET;
extern laUiType *_LA_UI_HEIGHT_ADJUSTER;
extern laUiType *_LA_UI_RAW;
extern laUiType *_LA_UI_MAPPER;
extern laUiType *_LA_UI_IMAGE;
extern laUiType *_LA_UI_SCOPE;
extern laUiType *_LA_UI_MOUSE_ACTION_REPORTER;
 
extern laUiDescriptor _LA_UI_DESCRIPTOR_REF_UI;
extern laUiDescriptor _LA_UI_DESCRIPTOR_REF_UIE;
extern laUiDescriptor _LA_UI_DESCRIPTOR_REF_P;
extern laUiDescriptor _LA_UI_DESCRIPTOR_REF_PE;
extern laUiDescriptor _LA_UI_DESCRIPTOR_REF_BASE;
extern laUiDescriptor _LA_UI_DESCRIPTOR_REF_ACT;

extern laUiDefineFunc _LA_SUBPROP_DONT_CARE;
 
extern laBoxedTheme *_LA_THEME_PANEL;
extern laBoxedTheme *_LA_THEME_VALUATOR;
extern laBoxedTheme *_LA_THEME_BUTTON;
extern laBoxedTheme *_LA_THEME_STRING;
extern laBoxedTheme *_LA_THEME_SELECTOR;
extern laBoxedTheme *_LA_THEME_COLLECTION_SELECTOR;
extern laBoxedTheme *_LA_THEME_LABEL;
extern laBoxedTheme *_LA_THEME_TAB;
extern laBoxedTheme *_LA_THEME_COLLECTION_GROUP;
extern laBoxedTheme *_LA_THEME_COLLECTION_ITEM;
extern laBoxedTheme *_LA_THEME_FLOATING_PANEL;
extern laBoxedTheme *_LA_THEME_3D_VIEW;
extern laBoxedTheme *_LA_THEME_2D_VIEW;
extern laBoxedTheme *_LA_THEME_SOCKET;

extern laProp *_LA_PROP_PANEL;
extern laProp *_LA_PROP_BLOCK;
extern laProp *_LA_PROP_WINDOW;
extern laProp *_LA_PROP_UI_ITEM;
extern laProp *_LA_PROP_FAILED_ITEM;
extern laProp *_LA_PROP_TRASH_ITEM;
extern laProp *_LA_PROP_NODE_GRAPH;
extern laPropContainer *_LA_PROP_3D_EXTRA;
extern laPropContainer *_LA_PROP_2D_EXTRA;
extern laPropContainer *_LA_PROP_FILE_BROWSER;
 
extern laProp _P_LA_USE_INSTANCE_ONLY;
#define _LA_USE_INSTANCE_ONLY (&_P_LA_USE_INSTANCE_ONLY)

#define LA_RH0 (MAIN.UiRowHeight)
#define LA_RH (MAIN.ScaledUiRowHeight)
#define LA_2RH (MAIN.ScaledUiRowHeight*2)
#define LA_RH2 ((int)(MAIN.ScaledUiRowHeight/2))
#define LA_SCROLL_W ((int)(MAIN.ScaledUiRowHeight * 0.67))
#define LA_SEAM_W ((int)(MAIN.ScaledUiRowHeight * 0.15))
#define LA_SHADOW_W ((int)(MAIN.ScaledUiRowHeight / 10.0f))

#define LA_M (MAIN.ScaledMargin)
