/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _GNU_SOURCE 1

#include "la_5.h"

#include <math.h>
#include "freetype/ftadvanc.h"

#ifdef LA_WITH_PNG
#include <png.h>
#endif

#define HF 0.5

#ifdef LA_USE_GLES
void la_glUniform1f(int location, float v0){
    glUniform1fv(location,1,&v0);
}
#endif

void tnsGet2DTextureSubImage(tnsTexture* t, int xoffset, int yoffset, uint32_t width, uint32_t height, int format, int type, size_t bufSize, void *pixels){
    int texture=t->GLTexHandle;
#ifdef LA_USE_GLES
    int offscreen_framebuffer;
    glGenFramebuffers(1, &offscreen_framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, offscreen_framebuffer);
    tnsBindTexture(t);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(status != GL_FRAMEBUFFER_COMPLETE) {
        logPrint("Failed to make complete framebuffer object in la_glGetTextureSubImage()", status);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, offscreen_framebuffer);
    glViewport(0, 0, t->Width, t->Height);
    glReadPixels(xoffset, yoffset, width, height, format, type, pixels);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDeleteFramebuffers(1, &offscreen_framebuffer);
    tnsUnbindTexture();
#else
    glGetTextureSubImage(texture,0,xoffset,yoffset,0,width,height,1,format,type,bufSize,pixels);
#endif
}
void tnsClearTextureImage(tnsTexture* t, int tex_format, int tex_bits_type){
    int texture=t->GLTexHandle;
#ifdef LA_USE_GLES
    int offscreen_framebuffer;
    glGenFramebuffers(1, &offscreen_framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, offscreen_framebuffer);
    tnsBindTexture(t);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(status != GL_FRAMEBUFFER_COMPLETE) {
        logPrint("Failed to make complete framebuffer object in tnsClearTextureImage()", status);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, offscreen_framebuffer);
    glViewport(0, 0, t->Width, t->Height);
    glClearColor(0,0,0,0); glClear(GL_COLOR_BUFFER_BIT);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDeleteFramebuffers(1, &offscreen_framebuffer);
    tnsUnbindTexture();
#else
    glClearTexImage(t->GLTexHandle,0,tex_format,tex_bits_type,0);
#endif
}


//  1  2
// 7 9 11     13  14
//  3  4
// 8 10 12    15  16
//  5  6
real TNS_LCD_SEG_16[16][4]={
    {0,0,HF,0},{HF,0,1,0},{0,0.5,HF,0.5},{HF,0.5,1,0.5},{0,1,HF,1},{HF,1,1,1},
    {0,0,0,HF},{0,HF,0,1},{0.5,0,0.5,HF},{0.5,HF,0.5,1},{1,0,1,HF},{1,HF,1,1},
    {0,0,HF,HF},{1,0,HF,HF},{0,1,HF,HF},{1,1,HF,HF},
};
char TNS_LCD_MAP_16[][16]={
    {1,1,0,0,1,1,1,1,0,0,1,1,0,1,1,0},{0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0},//0-9
    {1,1,1,1,1,1,0,1,0,0,1,0,0,0,0,0},{1,1,1,1,1,1,0,0,0,0,1,1,0,0,0,0},
    {0,0,1,1,0,0,1,0,1,1,0,0,0,0,0,0},{1,1,1,1,1,1,1,0,0,0,0,1,0,0,0,0},
    {1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0},{1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0},
    {1,1,1,1,1,1,1,1,0,0,1,1,0,0,0,0},{1,1,1,1,1,1,1,0,0,0,1,1,0,0,0,0},
    {1,1,1,1,0,0,1,1,0,0,1,1,0,0,0,0},{1,1,0,1,1,1,0,0,1,1,1,1,0,0,0,0},//A
    {1,1,0,0,1,1,1,1,0,0,0,0,0,0,0,0},{1,1,0,0,1,1,0,0,1,1,1,1,0,0,0,0},
    {1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0},{1,1,1,1,0,0,1,1,0,0,0,0,0,0,0,0},
    {1,1,0,1,1,1,1,1,0,0,0,1,0,0,0,0},{0,0,1,1,0,0,1,1,0,0,1,1,0,0,0,0},
    {1,1,0,0,1,1,0,0,1,1,0,0,0,0,0,0},{1,1,0,0,1,0,0,0,1,1,0,0,0,0,0,0},
    {0,0,1,0,0,0,1,1,0,0,0,0,0,1,0,1},{0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,1,1,0,0,1,1,1,1,0,0},{0,0,0,0,0,0,1,1,0,0,1,1,1,0,0,1},
    {1,1,0,0,1,1,1,1,0,0,1,1,0,0,0,0},{1,1,1,1,0,0,1,1,0,0,1,0,0,0,0,0},
    {1,1,0,0,1,1,1,1,0,0,1,1,0,0,0,1},{1,1,1,1,0,0,1,1,0,0,1,0,0,0,0,1},
    {1,1,0,1,1,1,0,0,0,0,0,1,1,0,0,0},{1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0},
    {0,0,0,0,1,1,1,1,0,0,1,1,0,0,0,0},{0,0,0,0,0,0,1,1,0,0,0,0,0,1,1,0},
    {0,0,0,0,0,0,1,1,0,0,1,1,0,0,1,1},{0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1},
    {0,0,0,0,0,0,0,0,0,1,0,0,1,1,0,0},{1,1,0,0,1,1,0,0,0,0,0,0,0,1,1,0},
    {0,0,1,0,1,1,0,1,0,1,0,0,0,0,0,0},{0,0,0,1,1,1,0,0,1,1,0,1,0,0,0,0},//a
    {0,0,1,1,1,1,0,1,0,0,0,0,0,0,0,0},{0,0,1,0,1,1,0,1,1,1,0,0,0,0,0,0},
    {0,0,1,0,1,1,0,1,0,0,0,0,0,0,1,0},{0,1,1,1,1,0,0,0,1,1,0,0,0,0,0,0},
    {0,1,0,1,1,1,0,1,1,0,1,1,0,0,0,0},{0,0,1,0,0,1,1,1,0,1,0,0,0,0,0,0},
    {1,0,1,0,0,1,0,0,0,1,0,0,0,0,0,0},{0,1,0,1,1,1,0,0,0,0,0,1,0,0,0,0},
    {0,0,1,1,0,0,0,1,1,0,0,0,0,0,0,1},{1,0,0,0,0,1,0,0,1,1,0,0,0,0,0,0},
    {0,0,1,1,0,0,0,1,0,1,0,1,0,0,0,0},{0,0,1,0,0,1,0,1,0,1,0,0,0,0,0,0},
    {0,0,1,1,1,1,0,1,0,0,0,1,0,0,0,0},{1,1,0,1,0,0,0,0,1,1,1,0,0,0,0,0},
    {1,1,0,0,0,0,1,0,1,1,0,0,0,0,0,0},{0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0},
    {0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,1},{0,0,1,1,0,1,0,0,1,1,0,0,0,0,0,0},
    {0,0,0,0,1,1,0,1,0,1,0,0,0,0,0,0},{0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0},
    {0,0,0,0,0,0,0,1,0,0,0,1,0,0,1,1},{0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1},
    {0,0,0,1,1,1,0,0,1,0,1,1,0,0,0,0},{0,0,1,0,1,1,0,0,0,0,0,0,0,0,1,0},
};

//   1
// 4   6
//   2
// 5   7
//   3
real TNS_LCD_SEG_7[7][4]={ {0,0,1,0},{0,0.5,1,0.5},{0,1,1,1},
    {0,0,0,HF},{0,HF,0,1},{1,0,1,HF},{1,HF,1,1},
};
char TNS_LCD_MAP_7[][7]={
    {1,0,1,1,1,1,1},{0,0,0,0,0,1,1},{1,1,1,0,1,1,0},{1,1,1,0,0,1,1},{0,1,0,1,0,1,1},// 0-4
    {1,1,1,1,0,0,1},{1,1,1,1,1,0,1},{1,0,0,0,0,1,1},{1,1,1,1,1,1,1},{1,1,1,1,0,1,1},// 6-9
};

#undef HF

int tKnlAttatchShader(tnsShader *tns);
tnsShader *tKnlFindShader1i(int CustomIndex);
void tKnlPushMatrix();
void tKnlPopMatrix();

laListHandle *tKnlGetTextureList();

void tnsLoadIdentity44d(tnsMatrix44d m);
void tnsMakeOrthoMatrix44d(tnsMatrix44d mProjection, real xMin, real xMax, real yMin, real yMax, real zMin, real zMax);
void tnsMakePerspectiveMatrix44d(tnsMatrix44d mProjection, real fFov_rad, real fAspect, real zMin, real zMax);
void tnsMakeTranslationMatrix44d(tnsMatrix44d mTrans, real x, real y, real z);
void tnsMakeRotationMatrix44d(tnsMatrix44d m, real angle_rad, real x, real y, real z);
void tnsMakeScaleMatrix44d(tnsMatrix44d m, real x, real y, real z);
void tnsMultiply44d(tnsMatrix44d result, tnsMatrix44d l, tnsMatrix44d r);

void tnsInitFirstLevel(tnsMatrixStack *tms);
tnsMatrixStackItem *tKnlGetCurrentMatStackItem();

void tnsAttach2DOffscreenBuffer(tnsOffscreen *target, GLuint attatchment, tnsTexture *use);
void tnsDetach2DOffscreenBuffer(tnsOffscreen *target, GLuint which_attach_point);

//=========================================================================================

tnsMain *T;
real DefaultZRange[2] = {0.1, 1000};

//=======================================[SYS]
extern LA MAIN;

void InitGLRenderEnviornment(){
    glEnable(GL_SCISSOR_TEST);
};
#ifdef __linux__
void tnsContextMakeCurrent(SYSGLCONTEXT context, SYSWINDOW win, void* gles_surf){
#ifdef LA_USE_GLES
    eglMakeCurrent(MAIN.egl_dpy,gles_surf,gles_surf,context);
    if(gles_surf){ EGLSurface* surf=gles_surf; T->CurrentSurface=(*surf); }else{ T->CurrentSurface=0; }
#else
    glXMakeContextCurrent(MAIN.dpy, win, win, context);
#endif
    T->CurrentContext=context; T->CurrentWindow=win; tnsUnbindTexture(); tnsUseNoTexture();
};
#endif
#ifdef _WIN32
void tnsContextMakeCurrent(SYSGLCONTEXT context, SYSTEMDC hdc,void* unused){
    wglMakeCurrent(hdc, context); T->CurrentContext=context; T->CurrentDC=hdc;
    tnsUnbindTexture(); tnsUseNoTexture();
};
#endif
#ifdef LA_USE_GLES
void tnsContextMakeFBOCurrent(tnsOffscreen* off){
    tnsContextMakeCurrent(off->FboContext,off->FboWindow,off->FboSurface);
}
void tnsContextMakeWindowCurrent(laWindow* w){
#ifdef LAGUI_ANDROID
    T->CurrentContext=MAIN.glc; T->CurrentSurface=MAIN.egl_surf;
    if (eglMakeCurrent(MAIN.egl_dpy, MAIN.egl_surf, MAIN.egl_surf, MAIN.glc) == EGL_FALSE) { logPrint("Android can't make current"); return; }
#else
    tnsContextMakeCurrent(w->glc,w->win,w->egl_surf);
#endif
}
#else
void tnsContextMakeFBOCurrent(tnsOffscreen* off){
#ifdef _WIN32
    tnsContextMakeCurrent(off->FboContext,off->FboDC,0);
#else
    tnsContextMakeCurrent(off->FboContext,off->FboWindow,0);
#endif
}
void tnsContextMakeWindowCurrent(laWindow* w){
#ifdef _WIN32
    tnsContextMakeCurrent(w->glc,w->hdc,0);
#else
    tnsContextMakeCurrent(w->glc,w->win,0);
#endif
}
#endif
void tnsSwitchToCurrentWindowContext(laWindow* w){
    tnsContextMakeWindowCurrent(w);
};

void tnsDeleteContext(SYSGLCONTEXT glc){
#ifdef __linux__
    #ifdef LA_USE_GLES
        eglDestroyContext(MAIN.egl_dpy,glc);
    #else
        glXDestroyContext(MAIN.dpy,glc);
    #endif
#endif
#ifdef _WIN32
    wglDeleteContext(glc);
#endif
    for(tnsOffscreen* o=T->Offscreens.pFirst;o;o=o->Item.pNext){
        if(o->FboContext==glc){ o->FboContext=0; }
    }
}
void tnsBindVertexArray(GLuint vao){
    T->CurrentVAO=vao; glBindVertexArray(vao);
}

void tnsViewportWithScissor(int x, int y, int w, int h){
    tnsShader *current_shader = 0;
    glEnable(GL_SCISSOR_TEST);
    glViewport(x, y, w, h); glScissor(x, y, w, h);
    T->vsx=x;T->vsy=y;T->vsw=w;T->vsh=h;
    if ((current_shader = T->CurrentShader)){
        tnsResetViewMatrix();
        tnsShaderApplyView(current_shader, tnsGetViewMatrix());
        tnsResetModelMatrix();
        tnsShaderApplyModel(current_shader, tnsGetModelMatrix());
    }
}
void tnsViewport(int x, int y, int w, int h){
    glViewport(x, y, w, h);
}
//Must At Origion!

//void UseWindowMatrix_WithScissor(laWndPlacement* wp){
//	tnsViewportWithScissor(0, 0, VAL2_CLIENT_W_H(*wp));
//	SetMatrix2Di(0, VAL2_CLIENT_W_H(*wp), 0);
//};
//void UseBlockMatrix_WithScissor(laBlockPlacement* bp){
//	tnsViewportWithScissor(bp->UpperLeftX,
//		bp->ClientHeight - bp->LowerRightY,
//		bp->LowerRightX - bp->UpperLeftX,
//		bp->LowerRightY - bp->UpperLeftY);
//	SetMatrix2Di(0, bp->LowerRightX - bp->UpperLeftX,
//		0, bp->UpperLeftY - bp->LowerRightY);
//};

void DrawWireRect2dp(real x, real y, real x2, real y2){
    real Verts[8];
    tnsMakeQuad2d(Verts, x, y, x2, y, x2, y2, x, y2);
    tnsVertexArray2d(Verts, 4);
    tnsPackAs(GL_LINE_LOOP);
};
void DrawWireRect4ds(real x, real y, real w, real h){
    real Verts[8];
    tnsMakeQuad2d(Verts, x, y, x + w, y, x + w, y + h, x, y + h);
    tnsVertexArray2d(Verts, 4);
    tnsPackAs(GL_LINE_LOOP);
};
void DrawWireRect4drs(real x, real y, real w, real h){
    real Verts[8];
    tnsMakeQuad2d(Verts, x, y, x + w, y, x + w, y - h, x, y - h);
    tnsVertexArray2d(Verts, 4);
    tnsPackAs(GL_LINE_LOOP);
};
void DrawSoildRect4drs(real x, real y, real w, real h){
    real Verts[8];
    tnsMakeQuad2d(Verts, x, y, x + w, y, x + w, y - h, x, y - h);
    tnsVertexArray2d(Verts, 4);
    tnsPackAs(GL_TRIANGLE_FAN);
}
void DrawWireCross4ds(real x, real y, real w, real h){
    real Verts[8];
    tnsMakeQuad2d(Verts, x, y, x + w, y + h, x + w, y, x, y + h);
    tnsVertexArray2d(Verts, 4);
    tnsPackAs(GL_LINES);
};
void DrawWireCross4drs(real x, real y, real w, real h){
    real Verts[8];
    tnsMakeQuad2d(Verts, x, y, x + w, y - h, x + w, y, x, y - h);
    tnsVertexArray2d(Verts, 4);
    tnsPackAs(GL_LINES);
};
void DrawGrid6f(real L, real R, real U, real B, real stepX, real stepY){
    //do nothing;
}

//=======================================[Shader]

int tnsNextPowOf2(int i){
    int result = 2;
    while (result < i){
        result *= 2;
    }
    return result;
}

void tnsShaderMakeIndex(tnsShader *tns){
    int program;

    if (!tns) return;

    program = tns->glProgramID;

    tns->iModel = glGetUniformLocation(program, "mModel");
    tns->iProjection = glGetUniformLocation(program, "mProjection");
    tns->iProjectionInverse = glGetUniformLocation(program, "mProjectionInverse");
    tns->iView = glGetUniformLocation(program, "mView");
    //tns->iNormal = glGetUniformLocation(program, "mNormalScaler");
    tns->iShadow = glGetUniformLocation(program, "mShadow");

    tns->iVertex = glGetAttribLocation(program, "vVertex");
    tns->iNormal = glGetAttribLocation(program, "vNormal");
    tns->iColor = glGetAttribLocation(program, "vColor");
    tns->iUV = glGetAttribLocation(program, "vUV");

    tns->iTexColor = glGetUniformLocation(program, "TexColor");
    tns->iTexColorU = glGetUniformLocation(program, "TexColorUI");
    tns->iTexColorMS = glGetUniformLocation(program, "TexColorMS");
    tns->iTexLut=glGetUniformLocation(program, "TexLut");
    tns->iTexColor2 = glGetUniformLocation(program, "TexColor2");
    tns->iTexColor2U = glGetUniformLocation(program, "TexColor2UI");
    tns->iTexNormal = glGetUniformLocation(program, "TexNormal");
    tns->iTexGPos = glGetUniformLocation(program, "TexGPos");
    tns->iMultiplyColor = glGetUniformLocation(program, "MultiplyColor");
    tns->iTextureMode = glGetUniformLocation(program, "TextureMode");
    tns->iColorMode = glGetUniformLocation(program, "ColorMode");
    tns->iHCYGamma = glGetUniformLocation(program, "HCYGamma");
    tns->iSampleAmount = glGetUniformLocation(program, "SampleAmount");
    tns->iUseNormal = glGetUniformLocation(program, "UseNormal");
    tns->iOutputColorSpace=glGetUniformLocation(program, "OutputColorSpace");
    tns->iInputColorSpace=glGetUniformLocation(program, "InputColorSpace");
    tns->iTexIsUI=glGetUniformLocation(program, "TexIsUI");
    tns->iTex2IsUI=glGetUniformLocation(program, "Tex2IsUI");
    tns->iUseLut=glGetUniformLocation(program, "UseLut");
    tns->iShowStripes=glGetUniformLocation(program, "ShowStripes");
    tns->iComposing=glGetUniformLocation(program, "Composing");
    tns->iComposingGamma=glGetUniformLocation(program, "ComposingGamma");
    tns->iComposingBlackpoint=glGetUniformLocation(program, "ComposingBlackpoint");
    tns->iDoOffset = glGetUniformLocation(program, "DoOffset");
    tns->iUseHalftone = glGetUniformLocation(program, "UseHalftone");
    tns->iHalftoneSize = glGetUniformLocation(program, "HalftoneSize");

    tns->uViewDir = glGetUniformLocation(program, "uViewDir");
    tns->uViewPos = glGetUniformLocation(program, "uViewPos");
    tns->uNear = glGetUniformLocation(program, "uNear");
    tns->uFar = glGetUniformLocation(program, "uFar");
    tns->uFOV = glGetUniformLocation(program, "uFOV");
}
void tnsShaderApplyProjection(tnsShader *tns, tnsMatrix44d m){
    tnsMatrix44f mf;
    if (T->BindedShader != tns){ glUseProgram(tns->glProgramID); T->BindedShader = tns; }
    tnsConvert44df(m, mf);
    if(tns->iProjection>-1) glUniformMatrix4fv(tns->iProjection, 1, 0, mf);
}
void tnsShaderApplyProjectionInverse(tnsShader *tns, tnsMatrix44d m){
    tnsMatrix44f mf;
    tnsMatrix44d i;
    if (T->BindedShader != tns){ glUseProgram(tns->glProgramID); T->BindedShader = tns; }
    tnsInverse44d(i, m);
    tnsConvert44df(i, mf);
    if(tns->iProjectionInverse>-1) glUniformMatrix4fv(tns->iProjectionInverse, 1, 0, mf);
}
void tnsShaderApplyModel(tnsShader *tns, tnsMatrix44d m){
    tnsMatrix44f mf;
    if (T->BindedShader != tns){ glUseProgram(tns->glProgramID); T->BindedShader = tns; }
    tnsConvert44df(m, mf);
    if(tns->iModel>-1) glUniformMatrix4fv(tns->iModel, 1, 0, mf);
}
void tnsShaderApplyView(tnsShader *tns, tnsMatrix44d m){
    tnsMatrix44f mf;
    if (T->BindedShader != tns){ glUseProgram(tns->glProgramID); T->BindedShader = tns; }
    tnsConvert44df(m, mf);
    if(tns->iView>-1) glUniformMatrix4fv(tns->iView, 1, 0, mf);
}
void tnsShaderApplyNormalScaler(tnsShader *tns, tnsMatrix44d m){
    tnsMatrix44f mf;
    if (tns->iNormal == -1) return;
    if (T->BindedShader != tns){ glUseProgram(tns->glProgramID); T->BindedShader = tns; }
    tnsConvert44df(m, mf);
    glUniformMatrix4fv(tns->iNormal, 1, 0, mf);
}
void tnsShaderApplyShadowMatrix(tnsShader *tns, tnsMatrix44d m){
    tnsMatrix44f mf;
    if (tns->iShadow == -1) return;
    if (T->BindedShader != tns){ glUseProgram(tns->glProgramID); T->BindedShader = tns; }
    tnsConvert44df(m, mf);
    glUniformMatrix4fv(tns->iShadow, 1, 0, mf);
}

char* tnsEnsureShaderCommoms(const char* Content, const char* Library, const char* Material){
    char* c=0,*c1=0;
    c1=strSub(Content,"#with TNS_SHADER_COLOR_COMMON",TNS_SHADER_COLOR_COMMON);
    c=strSub(c1,"#with LA_SHADER_LIB_FXAA",LA_SHADER_LIB_FXAA); free(c1);
    c1=strSub(c,"#with TNS_SHADER_MATERIAL",Material?Material:""); free(c);
    c=strSub(c1,"#with TNS_SHADER_LIBRARY",Library?Library:""); free(c1);
#ifdef LA_USE_GLES
    const char uint_texture_selection[] = "#define GLES_UINT_TEXTURE";
#else
    const char uint_texture_selection[] = "";
#endif
    c1=strSub(c,"#with TNS_GLES_UINT_TEXTURE",uint_texture_selection); free(c);
    return c1;
}
int tnsNewVertexShader(const char *Content){
    int status = 0;
    char error[8192]={0};
    GLuint VertexShaderObject;
    tnsShader *s = 0;

    if (!Content) return -1;

    VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    char* UseContent=tnsEnsureShaderCommoms(Content,0,0);
    glShaderSource(VertexShaderObject, 1, &UseContent, 0);
    glCompileShader(VertexShaderObject);
    glGetShaderiv(VertexShaderObject, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE){
        glGetShaderInfoLog(VertexShaderObject, sizeof(error), 0, error); logPrint("Vertex shader error:\n%s", error);
        glDeleteShader(VertexShaderObject); free(UseContent);
        return -1;
    } else {
        glGetShaderInfoLog(VertexShaderObject, sizeof(error), 0, error); if(error[0]) logPrint("Vertex shader info:\n%s", error);
    }
    free(UseContent);

    return VertexShaderObject;
}
int tnsNewFragmentShaderMaterial(const char *Content, const char* Library, const char* Material){
    int status = 0;
    char error[8192]={0};
    GLuint FragmentShaderObject;
    tnsShader *s = 0;

    if (!Content) return -1;

    FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    char* UseContent=tnsEnsureShaderCommoms(Content,Library,Material);
    glShaderSource(FragmentShaderObject, 1, &UseContent, 0);
    glCompileShader(FragmentShaderObject);
    glGetShaderiv(FragmentShaderObject, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE){
        glGetShaderInfoLog(FragmentShaderObject, sizeof(error), 0, error); printf("Fragment shader error:\n%s", error);
        glDeleteShader(FragmentShaderObject); free(UseContent);
        return -1;
    } else {
        glGetShaderInfoLog(FragmentShaderObject, sizeof(error), 0, error); if (error[0]) logPrint("Fragment shader info:\n%s", error);
    }
    free(UseContent);

    return FragmentShaderObject;
}
int tnsNewFragmentShader(const char *Content){
    return tnsNewFragmentShaderMaterial(Content,0,0);
}
int tnsNewGeometryShader(const char *Content){
#ifndef LAGUI_ANDROID
    int status = 0;
    char error[8192]={0};
    GLuint GeometryShaderObject;
    tnsShader *s = 0;

    if (!Content) return -1;

    GeometryShaderObject = glCreateShader(GL_GEOMETRY_SHADER);

    char* UseContent=tnsEnsureShaderCommoms(Content,0,0);
    glShaderSource(GeometryShaderObject, 1, &UseContent, 0);
    glCompileShader(GeometryShaderObject);
    glGetShaderiv(GeometryShaderObject, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE){
        glGetShaderInfoLog(GeometryShaderObject, sizeof(error), 0, error); logPrint("Geometry shader error:\n%s", error);
        glDeleteShader(GeometryShaderObject); free(UseContent);
        return -1;
    } else {
        glGetShaderInfoLog(GeometryShaderObject, sizeof(error), 0, error); if(error[0]) logPrint("Geometry shader info:\n%s", error);
    }
    free(UseContent);
    
    return GeometryShaderObject;
#endif
    return 0;
}

tnsShader *tnsNewShaderProgram(int VertexShaderID, int FragmentShaderID, int GeometryShaderID){
    int vso = VertexShaderID;
    int fso = FragmentShaderID;
    int gso = GeometryShaderID;
    tnsShader *tns = 0;
    int status = 0;
    char error[8192];

    if (!vso || !fso) return 0;

    tns = CreateNew(tnsShader);
    tns->vtShaderID = vso;
    tns->fgShaderID = fso;
    tns->glProgramID = glCreateProgram();
    glAttachShader(tns->glProgramID, vso);
    glAttachShader(tns->glProgramID, fso);
    if (GeometryShaderID > -1){ glAttachShader(tns->glProgramID, gso); tns->gsShaderID=gso; }
    glLinkProgram(tns->glProgramID);
    glGetProgramiv(tns->glProgramID, GL_LINK_STATUS, &status);
    if (status == GL_FALSE){
        glGetProgramInfoLog(tns->glProgramID, sizeof(error), 0, error);
        printf("Shader Linking error:\n%s", error);
        glDetachShader(tns->glProgramID, vso);
        glDetachShader(tns->glProgramID, fso);
        glDeleteShader(vso);
        glDeleteShader(fso);
        glDeleteProgram(tns->glProgramID);
        FreeMem(tns);
        return 0;
    }
    glUseProgram(tns->glProgramID);

    tnsShaderMakeIndex(tns);

    return tns;
}
int tnsEnableShader(int index){
    tnsMatrixStackItem *tmsi;
    tnsShader *tns = tKnlFindShader1i(index);
    if (!tns){
        glUseProgram(0);
        T->CurrentShader = 0;
        T->BindedShader = 0;
        return 0;
    }
    glUseProgram(tns->glProgramID);
    T->CurrentShader = tns;
    T->BindedShader = tns;

    tmsi = tKnlGetCurrentMatStackItem();
    tnsShaderApplyProjection(tns, tmsi->projection);
    tnsShaderApplyView(tns, tmsi->view);
    tnsShaderApplyModel(tns, tmsi->model); tnsUseShader(tns);
    return 1;
}
int tnsEnableShaderv(tnsShader *shader){
    tnsMatrixStackItem *tmsi;
    tnsShader *s = shader;
    if (!s){ glUseProgram(0); T->CurrentShader = 0;T->BindedShader = 0; return 0; }
    glUseProgram(s->glProgramID);
    T->CurrentShader = s;
    T->BindedShader = s;

    tmsi = tKnlGetCurrentMatStackItem();
    tnsShaderApplyProjection(s, tmsi->projection);
    tnsShaderApplyProjectionInverse(s, tmsi->projection);
    tnsShaderApplyView(s, tmsi->view);
    tnsShaderApplyModel(s, tmsi->model); tnsUseShader(s);

    if(s->iUseNormal>-1) glUniform1i(s->iUseNormal,T->SetUseNormal);
    if(s->uViewPos>-1) glUniform3fv(s->uViewPos,1,T->SetViewPos);
    if(s->uViewDir>-1) glUniform3fv(s->uViewDir,1,T->SetViewDir);
    if(s->iUseHalftone>-1) glUniform1f(s->iUseHalftone,T->SetUseHalftone);
    if(s->iHalftoneSize>-1) glUniform1f(s->iHalftoneSize,T->SetHalftoneSize);
    //if(s->iTextureMode) glUniform1i(s->iTextureMode,T->StateTextureMode);
    //if(cs->iSampleAmount) glUniform1i(cs->iSampleAmount);
    
    return 1;
}
void tnsUseShader(tnsShader *shader){
    T->StateShader = shader;
}
void tnsDeleteShaderProgram(tnsShader* s){
    tnsUseShader(0); tnsEnableShaderv(0);
    if(s->vtShaderID>-1) glDeleteShader(s->vtShaderID);
    if(s->fgShaderID>-1) glDeleteShader(s->fgShaderID);
    if(s->gsShaderID>-1) glDeleteShader(s->gsShaderID);
    glDeleteProgram(s->glProgramID);
    free(s);
}

void tnsUseImmShader(){
    T->StateShader = T->immShader;
}
void tnsUseTransparentGridShader(){
    T->StateShader = T->TransparentGridShader;
}
void tnsUseExtraBufferShader(){
    T->StateShader = T->ExtraBuffersShader;
}
void tnsUseRayShader(){
    T->StateShader = T->RayShader;
}
void tnsUseShadowShader(){
    T->StateShader = T->ShadowShader;
}
void tnsUseSceneShader(){
    T->StateShader = T->SceneShader;
}

//=======================================[MAT]

int tnsLineIntersectTest2d(tnsVector2d a1, tnsVector2d a2, tnsVector2d b1, tnsVector2d b2, double *aRatio){
    double k1, k2;
    double x;
    double y;
    double Ratio;
    double xDiff = (a2[0] - a1[0]); // +DBL_EPSILON;
    double xDiff2 = (b2[0] - b1[0]);

    if (xDiff == 0){
        if (xDiff2 == 0){
            *aRatio = 0;
            return 0;
        }
        double r2 = tnsGetRatiod(b1[0], b2[0], a1[0]);
        y = tnsLinearItp(b1[1], b2[1], r2);
        *aRatio = Ratio = tnsGetRatiod(a1[1], a2[1], y);
    }else{
        if (xDiff2 == 0){
            Ratio = tnsGetRatiod(a1[0], a2[0], b1[0]);
            //y = tnsLinearItp(a1[1], a2[1], r2);
            *aRatio = Ratio;
        }else{
            k1 = (a2[1] - a1[1]) / xDiff;
            k2 = (b2[1] - b1[1]) / xDiff2;

            if ((k1 == k2)) return 0;

            x = (a1[1] - b1[1] - k1 * a1[0] + k2 * b1[0]) / (k2 - k1);

            Ratio = (x - a1[0]) / xDiff;

            *aRatio = Ratio;
        }
    }

    if (b1[0] == b2[0]){
        y = tnsLinearItp(a1[1], a2[1], Ratio);
        if (y > TNS_MAX2(b1[1], b2[1]) || y < TNS_MIN2(b1[1], b2[1])) return 0;
    }else if (Ratio <= 0 || Ratio > 1 ||
             (b1[0] > b2[0] && x > b1[0]) ||
             (b1[0] < b2[0] && x < b1[0]) ||
             (b2[0] > b1[0] && x > b2[0]) ||
             (b2[0] < b1[0] && x < b2[0]))
        return 0;

    return 1;
}
double tnsGetLineZ(tnsVector3d L, tnsVector3d R, real Ratio){
    //double z = 1 / tnsLinearItp(1 / L[2], 1 / R[2], Ratio);
    double z = tnsLinearItp(L[2], R[2], Ratio);
    return z;
}
double tnsGetLineZPoint(tnsVector3d L, tnsVector3d R, tnsVector3d FromL){
    double r = (FromL[0] - L[0]) / (R[0] - L[0]);
    return tnsLinearItp(L[2], R[2], r);
    //return 1 / tnsLinearItp(1 / L[2], 1 / R[2], r);
}
double tnsGetRatio3d(tnsVector3d L, tnsVector3d R, tnsVector3d FromL){
    double r = (FromL[0] - L[0]) / (R[0] - L[0]);
    return r;
}

double tnsGetRatiod(real L, real R, real FromL){
    double r = (FromL - L) / (R - L);
    return r;
}

real tnsInterpolate(real L, real R, real T){
    return tnsLinearItp(L, R, T);
}
void tnsInterpolate2dv(real *L, real *R, real T, real *Result){
    Result[0] = tnsLinearItp(L[0], R[0], T);
    Result[1] = tnsLinearItp(L[1], R[1], T);
}
void tnsInterpolate3dv(real *L, real *R, real T, real *Result){
    Result[0] = tnsLinearItp(L[0], R[0], T);
    Result[1] = tnsLinearItp(L[1], R[1], T);
    Result[2] = tnsLinearItp(L[2], R[2], T);
}

void tnsInterpolateTripple2d(tnsVector2d v1, tnsVector2d v2, tnsVector2d v3, real ratio, tnsVector2d result){
    tnsVector2d i1,i2;
    tnsInterpolate2dv(v1,v2,ratio,i1);
    tnsInterpolate2dv(v2,v3,ratio,i2);
    tnsInterpolate2dv(i1,i2,ratio,result);
}

void tnsVectorMinus2d(tnsVector2d result, tnsVector2d l, tnsVector2d r){
    result[0] = l[0] - r[0];
    result[1] = l[1] - r[1];
}
void tnsVectorMinus3d(tnsVector3d result, tnsVector3d l, tnsVector3d r){
    result[0] = l[0] - r[0];
    result[1] = l[1] - r[1];
    result[2] = l[2] - r[2];
}
void tnsVectorSubtract3d(tnsVector3d l, tnsVector3d r){
    l[0] = l[0] - r[0];
    l[1] = l[1] - r[1];
    l[2] = l[2] - r[2];
}
void tnsVectorPlus3d(tnsVector3d result, tnsVector3d l, tnsVector3d r){
    result[0] = l[0] + r[0];
    result[1] = l[1] + r[1];
    result[2] = l[2] + r[2];
}
void tnsVectorAccum3d(tnsVector3d l, tnsVector3d r){
    l[0] = l[0] + r[0];
    l[1] = l[1] + r[1];
    l[2] = l[2] + r[2];
}
void tnsVectorAccum2d(tnsVector2d l, tnsVector2d r){
    l[0] = l[0] + r[0];
    l[1] = l[1] + r[1];
}
void tnsVectorNegate3d(tnsVector3d result, tnsVector3d l){
    result[0] = -l[0];
    result[1] = -l[1];
    result[2] = -l[2];
}
void tnsVectorNegateSelf3d(tnsVector3d l){
    l[0] = -l[0];
    l[1] = -l[1];
    l[2] = -l[2];
}
void tnsVectorCopy2d(tnsVector2d from, tnsVector2d to){
    to[0] = from[0];
    to[1] = from[1];
}
void tnsVectorCopy3d(tnsVector3d from, tnsVector3d to){
    to[0] = from[0];
    to[1] = from[1];
    to[2] = from[2];
}
void tnsVectorCopy4d(tnsVector4d from, tnsVector4d to){
    to[0] = from[0];
    to[1] = from[1];
    to[2] = from[2];
    to[3] = from[3];
}
void tnsVectorMultiSelf4d(tnsVector4d from, real num){
    from[0] *= num;
    from[1] *= num;
    from[2] *= num;
    from[3] *= num;
}
void tnsVectorMultiSelf3d(tnsVector3d from, real num){
    from[0] *= num;
    from[1] *= num;
    from[2] *= num;
}
void tnsVectorMultiSelf2d(tnsVector2d from, real num){
    from[0] *= num;
    from[1] *= num;
}
void tnsVectorMulti4d(tnsVector4d to, tnsVector4d from, real num){
    to[0]=from[0]*num;
    to[1]=from[1]*num;
    to[2]=from[2]*num;
    to[3]=from[3]*num;
}
void tnsVectorMulti3d(tnsVector3d to, tnsVector3d from, real num){
    to[0]=from[0]*num;
    to[1]=from[1]*num;
    to[2]=from[2]*num;
}
void tnsVectorMulti2d(tnsVector2d to, tnsVector2d from, real num){
    to[0]=from[0]*num;
    to[1]=from[1]*num;
}

real tnsDirectionToRad(tnsVector2d Dir){
    real arcc = acos(Dir[0]);
    real arcs = asin(Dir[1]);

    if (Dir[0] >= 0){
        if (Dir[1] >= 0) return arcc;
        else
            return TNS_PI * 2 - arcc;
    }else{
        if (Dir[1] >= 0) return arcs + TNS_PI / 2;
        else
            return TNS_PI + arcs;
    }
}
void tnsConvert44df(tnsMatrix44d from, tnsMatrix44f to){
    to[0] = from[0];
    to[1] = from[1];
    to[2] = from[2];
    to[3] = from[3];
    to[4] = from[4];
    to[5] = from[5];
    to[6] = from[6];
    to[7] = from[7];
    to[8] = from[8];
    to[9] = from[9];
    to[10] = from[10];
    to[11] = from[11];
    to[12] = from[12];
    to[13] = from[13];
    to[14] = from[14];
    to[15] = from[15];
}

void tnsLoadIdentity44d(tnsMatrix44d m){
    memset(m, 0, sizeof(tnsMatrix44d));
    m[0] = 1.0f;
    m[5] = 1.0f;
    m[10] = 1.0f;
    m[15] = 1.0f;
};

real tnsDistIdv2(real x1, real y1, real x2, real y2){
    real x = x2 - x1, y = y2 - y1;
    return sqrt((x * x + y * y));
}
real tnsDist3dv(tnsVector3d l, tnsVector3d r){
    real x = r[0] - l[0];
    real y = r[1] - l[1];
    real z = r[2] - l[2];
    return sqrt(x * x + y * y + z * z);
}
real tnsDist2dv(tnsVector2d l, tnsVector2d r){
    real x = r[0] - l[0];
    real y = r[1] - l[1];
    return sqrt(x * x + y * y);
}

real tnsLength3d(tnsVector3d l){
    return (sqrt(l[0] * l[0] + l[1] * l[1] + l[2] * l[2]));
}
real tnsLength2d(tnsVector3d l){
    return (sqrt(l[0] * l[0] + l[1] * l[1]));
}
void tnsNormalize3d(tnsVector3d result, tnsVector3d l){
    real r = sqrt(l[0] * l[0] + l[1] * l[1] + l[2] * l[2]);
    result[0] = l[0] / r;
    result[1] = l[1] / r;
    result[2] = l[2] / r;
}
void tnsNormalize2d(tnsVector2d result, tnsVector2d l){
    real r = sqrt(l[0] * l[0] + l[1] * l[1]);
    result[0] = l[0] / r;
    result[1] = l[1] / r;
}
void tnsNormalizeSelf2d(tnsVector3d result){
    real r = sqrt(result[0] * result[0] + result[1] * result[1]);
    result[0] /= r;
    result[1] /= r;
}
void tnsNormalizeSelf3d(tnsVector3d result){
    real r = sqrt(result[0] * result[0] + result[1] * result[1] + result[2] * result[2]);
    result[0] /= r;
    result[1] /= r;
    result[2] /= r;
}
real tnsDot3d(tnsVector3d l, tnsVector3d r, int normalize){
    tnsVector3d ln, rn;
    if (normalize){
        tnsNormalize3d(ln, l);
        tnsNormalize3d(rn, r);
        return (ln[0] * rn[0] + ln[1] * rn[1] + ln[2] * rn[2]);
    }
    return (l[0] * r[0] + l[1] * r[1] + l[2] * r[2]);
}
real tnsDot2d(tnsVector2d l, tnsVector2d r, int normalize){
    tnsVector3d ln, rn;
    if (normalize){
        tnsNormalize2d(ln, l);
        tnsNormalize2d(rn, r);
        return (ln[0] * rn[0] + ln[1] * rn[1]);
    }
    return (l[0] * r[0] + l[1] * r[1]);
}
real tnsVectorCross3d(tnsVector3d result, tnsVector3d l, tnsVector3d r){
    result[0] = l[1] * r[2] - l[2] * r[1];
    result[1] = l[2] * r[0] - l[0] * r[2];
    result[2] = l[0] * r[1] - l[1] * r[0];
    return tnsLength3d(result);
}
void tnsVectorCrossOnly3d(tnsVector3d result, tnsVector3d l, tnsVector3d r){
    result[0] = l[1] * r[2] - l[2] * r[1];
    result[1] = l[2] * r[0] - l[0] * r[2];
    result[2] = l[0] * r[1] - l[1] * r[0];
}
real tnsAngleRad3d(tnsVector3d from, tnsVector3d to, tnsVector3d PositiveReference){
    if (PositiveReference){
        tnsVector3d res;
        tnsVectorCross3d(res, from, to);
        if (tnsDot3d(res, PositiveReference, 1) > 0) return acosf(tnsDot3d(from, to, 1));
        else
            return -acosf(tnsDot3d(from, to, 1));
    }
    return acosf(tnsDot3d(from, to, 1));
}
void tnsApplyRotation33d(tnsVector3d result, tnsMatrix44d mat, tnsVector3d v){
    result[0] = mat[0] * v[0] + mat[1] * v[1] + mat[2] * v[2];
    result[1] = mat[3] * v[0] + mat[4] * v[1] + mat[5] * v[2];
    result[2] = mat[6] * v[0] + mat[7] * v[1] + mat[8] * v[2];
}
void tnsApplyRotation43d(tnsVector3d result, tnsMatrix44d mat, tnsVector3d v){
    result[0] = mat[0] * v[0] + mat[1] * v[1] + mat[2] * v[2];
    result[1] = mat[4] * v[0] + mat[5] * v[1] + mat[6] * v[2];
    result[2] = mat[8] * v[0] + mat[9] * v[1] + mat[10] * v[2];
}
//void tnsApplyRotation33d(tnsVector3d result, tnsMatrix44d mat, tnsVector3d v){
//    result[0] = mat[0] * v[0] + mat[3] * v[1] + mat[6] * v[2];
//    result[1] = mat[1] * v[0] + mat[4] * v[1] + mat[7] * v[2];
//    result[2] = mat[2] * v[0] + mat[5] * v[1] + mat[8] * v[2];
//}
//void tnsApplyRotation43d(tnsVector3d result, tnsMatrix44d mat, tnsVector3d v){
//    result[0] = mat[0] * v[0] + mat[4] * v[1] + mat[8] * v[2];
//    result[1] = mat[1] * v[0] + mat[5] * v[1] + mat[9] * v[2];
//    result[2] = mat[2] * v[0] + mat[6] * v[1] + mat[10] * v[2];
//}
void tnsApplyTransform43d(tnsVector3d result, tnsMatrix44d mat, tnsVector3d v){
    real w;
    result[0] = mat[0] * v[0] + mat[4] * v[1] + mat[8] * v[2] + mat[12] * 1;
    result[1] = mat[1] * v[0] + mat[5] * v[1] + mat[9] * v[2] + mat[13] * 1;
    result[2] = mat[2] * v[0] + mat[6] * v[1] + mat[10] * v[2] + mat[14] * 1;
    w = mat[3] * v[0] + mat[7] * v[1] + mat[11] * v[2] + mat[15] * 1;
    //result[0] /= w;
    //result[1] /= w;
    //result[2] /= w;
}
void tnsApplyNormalTransform43d(tnsVector3d result, tnsMatrix44d mat, tnsVector3d v){
    real w;
    result[0] = mat[0] * v[0] + mat[4] * v[1] + mat[8] * v[2] + mat[12] * 1;
    result[1] = mat[1] * v[0] + mat[5] * v[1] + mat[9] * v[2] + mat[13] * 1;
    result[2] = mat[2] * v[0] + mat[6] * v[1] + mat[10] * v[2] + mat[14] * 1;
}
void tnsApplyTransform44d(tnsVector4d result, tnsMatrix44d mat, tnsVector4d v){
    result[0] = mat[0] * v[0] + mat[4] * v[1] + mat[8] * v[2] + mat[12] * 1;
    result[1] = mat[1] * v[0] + mat[5] * v[1] + mat[9] * v[2] + mat[13] * 1;
    result[2] = mat[2] * v[0] + mat[6] * v[1] + mat[10] * v[2] + mat[14] * 1;
    result[3] = mat[3] * v[0] + mat[7] * v[1] + mat[11] * v[2] + mat[15] * 1;
}
void tnsApplyTransform44dTrue(tnsVector4d result, tnsMatrix44d mat, tnsVector4d v){
    result[0] = mat[0] * v[0] + mat[4] * v[1] + mat[8] * v[2] + mat[12] * v[3];
    result[1] = mat[1] * v[0] + mat[5] * v[1] + mat[9] * v[2] + mat[13] * v[3];
    result[2] = mat[2] * v[0] + mat[6] * v[1] + mat[10] * v[2] + mat[14] * v[3];
    result[3] = mat[3] * v[0] + mat[7] * v[1] + mat[11] * v[2] + mat[15] * v[3];
}

void tnsRemoveTranslation44d(tnsMatrix44d result, tnsMatrix44d mat){
    tnsLoadIdentity44d(result);
    result[0] = mat[0];
    result[1] = mat[1];
    result[2] = mat[2];
    result[4] = mat[4];
    result[5] = mat[5];
    result[6] = mat[6];
    result[8] = mat[8];
    result[9] = mat[9];
    result[10] = mat[10];
}
void tnsClearTranslation44d(tnsMatrix44d mat){
    mat[3] = 0;
    mat[7] = 0;
    mat[11] = 0;
}

void tnsCopyMatrix44d(tnsMatrix44d from, tnsMatrix44d to);
void tnsExtractXYZEuler44d(tnsMatrix44d _mat, real *xyz_result){
    real xRot, yRot, zRot;
    tnsMatrix44d mat; tnsCopyMatrix44d(_mat,mat);
    tnsNormalizeSelf3d(&mat[0]);
    tnsNormalizeSelf3d(&mat[4]);
    tnsNormalizeSelf3d(&mat[8]);

    if (mat[2] < 1){
        if (mat[2] > -1){
            yRot = asin(mat[2]);
            xRot = atan2(-mat[6], mat[10]);
            zRot = atan2(-mat[1], mat[0]);
        }else{
            yRot = -TNS_PI / 2;
            xRot = -atan2(-mat[4], mat[5]);
            zRot = 0;
        }
    }else{
        yRot = TNS_PI / 2;
        xRot = atan2(-mat[4], mat[5]);
        zRot = 0;
    }

    xyz_result[0] = -xRot;
    xyz_result[1] = -yRot;
    xyz_result[2] = -zRot;
}
void tnsExtractLocation44d(tnsMatrix44d mat, real *xyz_result){
    xyz_result[0] = mat[12];
    xyz_result[1] = mat[13];
    xyz_result[2] = mat[14];
}
void tnsExtractScale44d(tnsMatrix44d mat, real *xyz_result){
    xyz_result[0] = tnsLength3d(&mat[0]);
    xyz_result[1] = tnsLength3d(&mat[4]);
    xyz_result[2] = tnsLength3d(&mat[8]);
}

#define L(row, col) l[(col << 2) + row]
#define R(row, col) r[(col << 2) + row]
#define P(row, col) result[(col << 2) + row]

void tnsPrintMatrix44d(tnsMatrix44d l){
    int i, j;
    for (i = 0; i < 4; i++){
        for (j = 0; j < 4; j++){
            printf("%.5f ", L(i, j));
        }
        printf("\n");
    }
}

void tnsCopyMatrix44d(tnsMatrix44d from, tnsMatrix44d to){
    memcpy(to, from, sizeof(tnsMatrix44d));
}
void tnsMultiply44d(tnsMatrix44d result, tnsMatrix44d l, tnsMatrix44d r){
    int i;
    for (i = 0; i < 4; i++){
        real ai0 = L(i, 0), ai1 = L(i, 1), ai2 = L(i, 2), ai3 = L(i, 3);
        P(i, 0) = ai0 * R(0, 0) + ai1 * R(1, 0) + ai2 * R(2, 0) + ai3 * R(3, 0);
        P(i, 1) = ai0 * R(0, 1) + ai1 * R(1, 1) + ai2 * R(2, 1) + ai3 * R(3, 1);
        P(i, 2) = ai0 * R(0, 2) + ai1 * R(1, 2) + ai2 * R(2, 2) + ai3 * R(3, 2);
        P(i, 3) = ai0 * R(0, 3) + ai1 * R(1, 3) + ai2 * R(2, 3) + ai3 * R(3, 3);
    }
};
void tnsInverse44d(tnsMatrix44d inverse, tnsMatrix44d mat){
    int i, j, k;
    double temp;
    tnsMatrix44d tempmat;
    real max;
    int maxj;

    tnsLoadIdentity44d(inverse);

    tnsCopyMatrix44d(mat, tempmat);

    for (i = 0; i < 4; i++){
        /* Look for row with max pivot */
        max = fabsf(tempmat[i * 5]);
        maxj = i;
        for (j = i + 1; j < 4; j++){
            if (fabsf(tempmat[j * 4 + i]) > max){
                max = fabsf(tempmat[j * 4 + i]);
                maxj = j;
            }
        }

        /* Swap rows if necessary */
        if (maxj != i){
            for (k = 0; k < 4; k++){
                real t;
                t = tempmat[i * 4 + k];
                tempmat[i * 4 + k] = tempmat[maxj * 4 + k];
                tempmat[maxj * 4 + k] = t;

                t = inverse[i * 4 + k];
                inverse[i * 4 + k] = inverse[maxj * 4 + k];
                inverse[maxj * 4 + k] = t;
            }
        }

        //if (UNLIKELY(tempmat[i][i] == 0.0f)) {
        //	return false;  /* No non-zero pivot */
        //}

        temp = (double)tempmat[i * 5];
        for (k = 0; k < 4; k++){
            tempmat[i * 4 + k] = (real)((double)tempmat[i * 4 + k] / temp);
            inverse[i * 4 + k] = (real)((double)inverse[i * 4 + k] / temp);
        }
        for (j = 0; j < 4; j++){
            if (j != i){
                temp = tempmat[j * 4 + i];
                for (k = 0; k < 4; k++){
                    tempmat[j * 4 + k] -= (real)((double)tempmat[i * 4 + k] * temp);
                    inverse[j * 4 + k] -= (real)((double)inverse[i * 4 + k] * temp);
                }
            }
        }
    }
}
void tnsMakeTranslationMatrix44d(tnsMatrix44d mTrans, real x, real y, real z){
    tnsLoadIdentity44d(mTrans);
    mTrans[12] = x;
    mTrans[13] = y;
    mTrans[14] = z;
}
void tnsMakePerspectiveMatrix44d(tnsMatrix44d mProjection, real fFov_rad, real fAspect, real zMin, real zMax){
    real yMax = zMin * tanf(fFov_rad * 0.5f);
    real yMin = -yMax;
    real xMin = yMin * fAspect;
    real xMax = -xMin;

    tnsLoadIdentity44d(mProjection);

    mProjection[0] = (2.0f * zMin) / (xMax - xMin);
    mProjection[5] = (2.0f * zMin) / (yMax - yMin);
    mProjection[8] = (xMax + xMin) / (xMax - xMin);
    mProjection[9] = (yMax + yMin) / (yMax - yMin);
    mProjection[10] = -((zMax + zMin) / (zMax - zMin));
    mProjection[11] = -1.0f;
    mProjection[14] = -((2.0f * (zMax * zMin)) / (zMax - zMin));
    mProjection[15] = 0.0f;
}
void tnsMakeZTrackingMatrix44d(tnsMatrix44d mat, tnsVector3d this, tnsVector3d that, tnsVector3d up){
    tnsVector4d fwd, l, t, rt;
    fwd[3] = l[3] = t[3] = rt[3] = 1;
    t[0] = up[0];
    t[1] = up[1];
    t[2] = up[2];
    fwd[0] = that[0] - this[0];
    fwd[1] = that[1] - this[1];
    fwd[2] = that[2] - this[2];
    tnsNormalizeSelf3d(fwd);
    tnsVectorCross3d(l, fwd, t);
    tnsNormalizeSelf3d(l);
    tnsVectorCross3d(rt, l, fwd);
    tnsNormalizeSelf3d(rt);

    tnsLoadIdentity44d(mat);

    mat[0] = l[0];
    mat[1] = l[1];
    mat[2] = l[2];

    mat[4] = rt[0];
    mat[5] = rt[1];
    mat[6] = rt[2];

    mat[8] = -fwd[0];
    mat[9] = -fwd[1];
    mat[10] = -fwd[2];
}
void tnsMakeZTrackingMatrixDelta44d(tnsMatrix44d mat, tnsVector3d delta, tnsVector3d up){
    tnsVector4d fwd, l, t, rt;
    fwd[3] = l[3] = t[3] = rt[3] = 1;
    t[0] = up[0];
    t[1] = up[1];
    t[2] = up[2];
    fwd[0] = delta[0];
    fwd[1] = delta[1];
    fwd[2] = delta[2];

    tnsLoadIdentity44d(mat);

    tnsVectorCross3d(l, t, fwd);
    tnsVectorCross3d(rt, fwd, l);

    tnsNormalizeSelf3d(l);
    tnsNormalizeSelf3d(rt);
    tnsNormalizeSelf3d(fwd);

    mat[0] = l[0];
    mat[1] = l[1];
    mat[2] = l[2];

    mat[4] = rt[0];
    mat[5] = rt[1];
    mat[6] = rt[2];

    mat[8] = fwd[0];
    mat[9] = fwd[1];
    mat[10] = fwd[2];
}
void tnsMakeOrthoMatrix44d(tnsMatrix44d mProjection, real xMin, real xMax, real yMin, real yMax, real zMin, real zMax){
    tnsLoadIdentity44d(mProjection);

    mProjection[0] = 2.0f / (xMax - xMin);
    mProjection[5] = 2.0f / (yMax - yMin);
    mProjection[10] = -2.0f / (zMax - zMin);
    mProjection[12] = -((xMax + xMin) / (xMax - xMin));
    mProjection[13] = -((yMax + yMin) / (yMax - yMin));
    mProjection[14] = -((zMax + zMin) / (zMax - zMin));
    mProjection[15] = 1.0f;
}
void tnsMakeRotationMatrix44d(tnsMatrix44d m, real angle_rad, real x_, real y_, real z_){
    float x = x_;
    float y = y_;
    float z = z_;

    float c = cos(angle_rad);
    float s = sin(angle_rad);
    tnsMatrix44d d={
       x*x*(1.0f-c)+c,   x*y*(1.0f-c)-z*s, x*z*(1.0f-c)+y*s, 0.0f,
       y*x*(1.0f-c)+z*s, y*y*(1.0f-c)+c,   y*z*(1.0f-c)-x*s, 0.0f,
       z*x*(1.0f-c)-y*s, z*y*(1.0f-c)+x*s, z*z*(1.0f-c)+c,   0.0f,
       0.0f,             0.0f,             0.0f,             1.0f };
    memcpy(m,d,sizeof(tnsMatrix44d));
}
void tnsMakeRotationXMatrix44d(tnsMatrix44d m, real angle_rad){
    tnsLoadIdentity44d(m);
    m[5] = cos(angle_rad);
    m[6] = sin(angle_rad);
    m[9] = -sin(angle_rad);
    m[10] = cos(angle_rad);
}
void tnsMakeRotationYMatrix44d(tnsMatrix44d m, real angle_rad){
    tnsLoadIdentity44d(m);
    m[0] = cos(angle_rad);
    m[2] = -sin(angle_rad);
    m[8] = sin(angle_rad);
    m[10] = cos(angle_rad);
}
void tnsMakeRotationZMatrix44d(tnsMatrix44d m, real angle_rad){
    tnsLoadIdentity44d(m);
    m[0] = cos(angle_rad);
    m[1] = sin(angle_rad);
    m[4] = -sin(angle_rad);
    m[5] = cos(angle_rad);
}
void tnsMakeScaleMatrix44d(tnsMatrix44d m, real x, real y, real z){
    tnsLoadIdentity44d(m);
    m[0] = x; m[5] = y; m[10] = z;
}
void tnsMakeViewportMatrix44d(tnsMatrix44d m, real w, real h, real Far, real Near){
    tnsLoadIdentity44d(m);
    m[0] = w / 2;
    m[5] = h / 2;
    m[10] = (Far - Near) / 2;
    m[12] = w / 2;
    m[13] = h / 2;
    m[14] = (Far + Near) / 2;
    m[15] = 1;
    //m[0] = 2/w;
    //m[5] = 2/h;
    //m[10] = 1;
    //m[12] = 2/w;
    //m[13] = 2/h;
    //m[14] = 1;
    //m[15] = 1;
}

int tnsIntersectPlaneRay(tnsVector3d n, tnsVector3d p0, tnsVector3d l0, tnsVector3d l, real* t){
    float denom = tnsDot3d(n, l, 0);
    if (denom > 1e-6){
        tnsVector3d p0l0; tnsVectorMinus3d(p0l0,p0 ,l0);
        (*t) = tnsDot3d(p0l0, n, 0) / denom;
        return ((*t) >= 0);
    }
    return 0;
}

void tnsInitFirstLevel(tnsMatrixStack *tms){
    tnsLoadIdentity44d(tms->level[0].model);
    tnsLoadIdentity44d(tms->level[0].projection);
    tnsLoadIdentity44d(tms->level[0].view);
}

//-------------------Export

void tnsOrtho(real xMin, real xMax, real yMin, real yMax, real zMin, real zMax){
    tnsShader *current_shader = 0;
    real *mat = tnsGetProjectionMatrix();

    T->vol=xMin; T->vor=xMax; T->vou=yMax; T->vob=yMin;

    tnsMakeOrthoMatrix44d(mat, xMin, xMax, yMin, yMax, zMin, zMax);

    if (current_shader = T->CurrentShader){
        tnsShaderApplyProjection(current_shader, mat);
    }
}
void tnsPerspective(real fFov_rad, real fAspect, real zMin, real zMax){
    tnsShader *current_shader = 0;
    real *mat = tnsGetProjectionMatrix();

    tnsMakePerspectiveMatrix44d(mat, fFov_rad, fAspect, zMin, zMax);

    if (current_shader = T->CurrentShader){
        tnsShaderApplyProjection(current_shader, mat);
    }
}

void tnsTranslate3d(real x, real y, real z){
    tnsShader *current_shader = 0;
    real *mat = tnsGetModelMatrix();
    tnsMatrix44d transmat, result;

    tnsMakeTranslationMatrix44d(transmat, x, y, z);
    tnsMultiply44d(result, mat, transmat);
    memcpy(mat, result, sizeof(tnsMatrix44d));

    if (current_shader = T->CurrentShader){
        tnsShaderApplyModel(current_shader, result);
    }
}
void tnsPreTranslate3d(real x, real y, real z){
    tnsShader *current_shader = 0;
    real *mat = tnsGetModelMatrix();
    tnsMatrix44d transmat, result;

    tnsMakeTranslationMatrix44d(transmat, x, y, z);
    tnsMultiply44d(result, mat, transmat);
    memcpy(mat, result, sizeof(tnsMatrix44d));
}
void tnsRotate4d(real degrees, real x, real y, real z){
    tnsShader *current_shader = 0;
    real *mat = tnsGetModelMatrix();
    tnsMatrix44d rotmat, result;

    tnsMakeRotationMatrix44d(rotmat, rad(degrees), x, y, z);
    tnsMultiply44d(result, mat, rotmat);
    memcpy(mat, result, sizeof(tnsMatrix44d));

    if (current_shader = T->CurrentShader){
        tnsShaderApplyModel(current_shader, result);
    }
}
void tnsPreRotate4d(real degrees, real x, real y, real z){
    tnsShader *current_shader = 0;
    real *mat = tnsGetModelMatrix();
    tnsMatrix44d rotmat, result;

    tnsMakeRotationMatrix44d(rotmat, rad(degrees), x, y, z);
    tnsMultiply44d(result, mat, rotmat);
    memcpy(mat, result, sizeof(tnsMatrix44d));
}
void tnsScale3d(real x, real y, real z){
    tnsShader *current_shader = 0;
    real *mat = tnsGetModelMatrix();
    tnsMatrix44d scalemat, normal_scaler, result;

    tnsMakeScaleMatrix44d(scalemat, x, y, z);

    tnsMultiply44d(result, mat, scalemat);
    memcpy(mat, result, sizeof(tnsMatrix44d));

    if (current_shader = T->CurrentShader){
        tnsShaderApplyModel(current_shader, result);
    }
}
void tnsPreScale3d(real x, real y, real z){
    tnsShader *current_shader = 0;
    real *mat = tnsGetModelMatrix();
    tnsMatrix44d scalemat, normal_scaler, result;

    tnsMakeScaleMatrix44d(scalemat, x, y, z);

    tnsMultiply44d(result, mat, scalemat);
    memcpy(mat, result, sizeof(tnsMatrix44d));
}

void tKnlPopMatrix();
void tKnlPushMatrix();
void tnsPushMatrix(){
    tKnlPushMatrix();
};
void tnsPopMatrix(){
    tKnlPopMatrix();
}

//========================================[KNL]

void tnsSetRayShaderUniformTextures(tnsOffscreen* doff){
    if(!doff)return;
    tnsActiveTexture(GL_TEXTURE0); glBindTexture(doff->pColor[0]->GLTexType, doff->pColor[0]->GLTexHandle);
    tnsActiveTexture(GL_TEXTURE1); glBindTexture(doff->pColor[1]->GLTexType, doff->pColor[1]->GLTexHandle);
    tnsActiveTexture(GL_TEXTURE2); glBindTexture(doff->pColor[2]->GLTexType, doff->pColor[2]->GLTexHandle);
    glUniform1i(T->RayShader->iTexColor, 0); glUniform1i(T->RayShader->iTexNormal, 1); glUniform1i(T->RayShader->iTexGPos, 2);
}

void tnsInit(){
    T = memAcquire(sizeof(tnsMain));
    T->World=memAcquire(sizeof(tnsWorld));
}
void tnsInitRenderKernel(int matrixStackLevel){
    tnsCommand *c;
    GLuint m_nQuadVAO;

    T->GLVersionStr = glGetString(GL_VERSION);
    T->GLVendorStr = glGetString(GL_VENDOR);
    T->GLRendererStr = glGetString(GL_RENDERER);
    T->GLSLVersionStr = glGetString(GL_SHADING_LANGUAGE_VERSION);

    T->stack.max_level = matrixStackLevel;
    T->stack.level = CreateNewBuffer(tnsMatrixStackItem, matrixStackLevel);
    T->NextShaderIndex = 1;
    T->BindedShader = -1;
    tnsInitFirstLevel(&T->stack);

    T->StateLineWidth=T->SetLineWidth=1; T->StatePointSize=T->SetPointSize=1;

    arrEnsureLength(&T->Vert, T->NextVert, &T->MaxVert, sizeof(GLfloat));
    glGenBuffers(1, &T->VertBufObject);
    glBindBuffer(GL_ARRAY_BUFFER, T->VertBufObject);
    glBufferData(GL_ARRAY_BUFFER, T->MaxVert * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);

    arrEnsureLength(&T->Color, T->NextColor, &T->MaxColor, sizeof(GLfloat));
    glGenBuffers(1, &T->ColorBufObject);
    glBindBuffer(GL_ARRAY_BUFFER, T->ColorBufObject);
    glBufferData(GL_ARRAY_BUFFER, T->MaxColor * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);

    arrEnsureLength(&T->Normal, T->NextNormal, &T->MaxNormal, sizeof(GLfloat));
    glGenBuffers(1, &T->NormalBufObject);
    glBindBuffer(GL_ARRAY_BUFFER, T->NormalBufObject);
    glBufferData(GL_ARRAY_BUFFER, T->MaxNormal * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);

    arrEnsureLength(&T->TexCoord, T->NextTexCoord, &T->MaxTexCoord, sizeof(GLfloat));
    glGenBuffers(1, &T->TexCoordBufObject);
    glBindBuffer(GL_ARRAY_BUFFER, T->TexCoordBufObject);
    glBufferData(GL_ARRAY_BUFFER, T->MaxTexCoord * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);

    arrEnsureLength(&T->Index, T->NextIndex, &T->MaxIndex, sizeof(GLuint));
    glGenBuffers(1, &T->IndexBufObject);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, T->IndexBufObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, T->MaxIndex * sizeof(GLuint), 0, GL_DYNAMIC_DRAW);

    arrEnsureLength(&T->DrawingCommand, T->NextCommand, &T->MaxCommand, sizeof(tnsCommand));
}
void tnsInitBuiltinShaders(){
    T->immShader = tnsNewShaderProgram(
        tnsNewVertexShader(LA_IMM_VERTEX_SHADER),tnsNewFragmentShader(LA_IMM_FRAGMENT_SHADER),-1);

    T->TEST_MatcapShader = tnsNewShaderProgram(
        tnsNewVertexShader(TNS_VERTEX_SIMPLE_MATCAP),
        tnsNewFragmentShader(TNS_FRAGMENT_SIMPLE_MATCAP), -1);

    T->TransparentGridShader = tnsNewShaderProgram(
        tnsNewVertexShader(TNS_VERTEX_GRID),
        tnsNewFragmentShader(TNS_FRAGMENT_TRANSPARNT_GRID), -1);

    T->RayShader = tnsNewShaderProgram(
        tnsNewVertexShader(LA_RAY_VERTEX_SHADER),
        tnsNewFragmentShader(LA_RAY_FRAGMENT_SHADER), -1);

    T->ShadowShader = tnsNewShaderProgram(
        tnsNewVertexShader(LA_CASCADE_SHADOW_VERTEX_SHADER),
        tnsNewFragmentShader(LA_CASCADE_SHADOW_FRAGMENT_SHADER), -1);

    T->SceneShader = tnsNewShaderProgram(
        tnsNewVertexShader(LA_SCENE_VERTEX_SHADER),
        tnsNewFragmentShader(LA_SCENE_FRAGMENT_SHADER), -1);

    T->SelectionShader = tnsNewShaderProgram(
        tnsNewVertexShader(LA_SELECTION_VERTEX_SHADER),
        tnsNewFragmentShader(LA_SELECTION_FRAGMENT_SHADER), -1);

    T->FloorShader = tnsNewShaderProgram(
        tnsNewVertexShader(LA_FLOOR_VERTEX_SHADER),
        tnsNewFragmentShader(LA_FLOOR_FRAGMENT_SHADER), -1);

    tnsUseShader(T->immShader);
    tnsEnableShaderv(T->immShader);
}
void tnsInitWindowDefaultRenderConfig(){
    tnsInitBuiltinShaders();
};
void tnsDeleteBuiltinShaders(){
    tnsDeleteShaderProgram(T->immShader); T->immShader=0;
    tnsDeleteShaderProgram(T->TEST_MatcapShader); T->TEST_MatcapShader=0;
    tnsDeleteShaderProgram(T->TransparentGridShader); T->TransparentGridShader=0;
    tnsDeleteShaderProgram(T->SelectionShader); T->SelectionShader=0;
    tnsDeleteShaderProgram(T->FloorShader); T->FloorShader=0;
    tnsDeleteShaderProgram(T->SceneShader); T->SceneShader=0;
    tnsDeleteShaderProgram(T->ShadowShader); T->ShadowShader=0;
    tnsDeleteShaderProgram(T->RayShader); T->RayShader=0;
}
void tnsQuitFontManager();
void tnsQuit(){
    tnsQuitFontManager();

    tnsObject*o; 
    while(o=lstPopItem(&T->World->AllObjects)){ tnsDestroyObject(o); }
    while(o=lstPopItem(&T->World->RootObjects)){ tnsDestroyRootObject(o); }
    //memFreeRemainingLeftNodes();

    tnsDeleteBuiltinShaders();

    tnsOffscreen* off; while(off=T->Offscreens.pFirst){ tnsDelete2DOffscreen(off); }
    tnsTexture* t; while(t=T->Textures.pFirst){ tnsDeleteTexture(t); }
    
    arrFree(&T->Vert, &T->MaxVert);
    arrFree(&T->Color, &T->MaxColor);
    arrFree(&T->Normal, &T->MaxNormal);
    arrFree(&T->TexCoord, &T->MaxTexCoord);
    arrFree(&T->Index, &T->MaxIndex);
    arrFree(&T->DrawingCommand, &T->DrawingCommand);

    glDeleteBuffers(1, &T->VertBufObject);
    glDeleteBuffers(1, &T->ColorBufObject);
    glDeleteBuffers(1, &T->NormalBufObject);
    glDeleteBuffers(1, &T->TexCoordBufObject);
    glDeleteBuffers(1, &T->IndexBufObject);

    FreeMem(T->stack.level);
    memFree(T->World);
    memFree(T);
}

void tnsRestoreFromNanoVG(){
    glBindVertexArray(T->CurrentVAO);
    tnsUseImmShader(); tnsEnableShaderv(T->immShader);
    glActiveTexture(GL_TEXTURE0); tnsActiveTexture(GL_TEXTURE0);
    glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA,GL_ONE,GL_ONE_MINUS_SRC_ALPHA);
}

tnsMatrixStackItem *tKnlGetCurrentMatStackItem(){
    return &T->stack.level[T->stack.current_level];
}
real *tnsGetModelMatrix(){
    return tKnlGetCurrentMatStackItem()->model;
}
real *tnsGetViewMatrix(){
    return tKnlGetCurrentMatStackItem()->view;
}
real *tnsGetProjectionMatrix(){
    return tKnlGetCurrentMatStackItem()->projection;
}
void tnsGetMVMatrix(tnsMatrix44d r){
    tnsMatrixStackItem *tmsi=tKnlGetCurrentMatStackItem();
    tnsMatrix44d tmp;
    tnsMultiply44d(r, tmsi->model, tmsi->view); 
}
void tnsGetMVPMatrix(tnsMatrix44d r){
    tnsMatrixStackItem *tmsi=tKnlGetCurrentMatStackItem();
    tnsMatrix44d tmp;
    tnsMultiply44d(tmp, tmsi->model, tmsi->view); 
    tnsMultiply44d(r, tmp, tmsi->projection); 
}
void tnsResetModelMatrix(){
    tnsLoadIdentity44d(tnsGetModelMatrix());
}
void tnsResetViewMatrix(){
    tnsLoadIdentity44d(tnsGetViewMatrix());
}
void tnsResetProjectionMatrix(){
    tnsLoadIdentity44d(tnsGetProjectionMatrix());
}
int tKnlAttatchShader(tnsShader *tns){
    lstAppendItem(&T->Shaders, tns);
    tns->CustomID = T->NextShaderIndex;
    T->NextShaderIndex++;
    return tns->CustomID;
};
int CMP_NUM_IsThisShader(tnsShader *tns, int *index){
    return (tns->CustomID == *index);
}
tnsShader *tKnlFindShader1i(int CustomIndex){
    return lstFindItem(&CustomIndex, CMP_NUM_IsThisShader, &T->Shaders);
}
void tKnlPushMatrix(){
    tnsMatrixStack *tms = &T->stack;
    tnsShader *current_shader = T->CurrentShader;

    if (tms->current_level == tms->max_level || !current_shader) return;

    memcpy(&tms->level[tms->current_level + 1], &tms->level[tms->current_level], sizeof(tnsMatrixStackItem));

    tms->current_level++;

    //tnsShaderApplyModel(current_shader,tms->level[tms->current_level].model);
    //tnsShaderApplyView(current_shader,tms->level[tms->current_level].view);
    //tnsShaderApplyProjection(current_shader,tms->level[tms->current_level].projection);
};
void tKnlPopMatrix(){
    tnsMatrixStack *tms = &T->stack;
    tnsShader *current_shader = T->CurrentShader;

    if (tms->current_level == 0 || !current_shader) return;

    tms->current_level--;

    tnsShaderApplyModel(current_shader, tms->level[tms->current_level].model);
    tnsShaderApplyView(current_shader, tms->level[tms->current_level].view);
    tnsShaderApplyProjection(current_shader, tms->level[tms->current_level].projection);
}
laListHandle *tKnlGetTextureList(){
    return &T->Textures;
}

tnsBatch *tnsCreateBatch(u32bit NumVert, int Dimension, float *Data, int NormalDimension, float *Normal, int ColorDimension, float *Colors){
    tnsBatch *b = CreateNew(tnsBatch);
    b->Dimension = Dimension;
    b->NormalDimension=NormalDimension;
    b->ColorDimension=ColorDimension;
    b->NumVert = NumVert;

    glGenBuffers(1, &b->VBO);
    glBindBuffer(GL_ARRAY_BUFFER, b->VBO);
    glBufferData(GL_ARRAY_BUFFER, NumVert * Dimension * sizeof(GLfloat), Data, GL_DYNAMIC_DRAW);

    if (Normal){
        glGenBuffers(1, &b->NBO); glBindBuffer(GL_ARRAY_BUFFER, b->NBO);
        glBufferData(GL_ARRAY_BUFFER, NumVert * NormalDimension * sizeof(GLfloat), Normal, GL_DYNAMIC_DRAW);
        b->HasNormal=1;
    }
    if (Colors){
        glGenBuffers(1, &b->CBO); glBindBuffer(GL_ARRAY_BUFFER, b->CBO);
        glBufferData(GL_ARRAY_BUFFER, NumVert * ColorDimension * sizeof(GLfloat), Colors, GL_DYNAMIC_DRAW);
        b->HasColor=1;
    }

    return b;
}
tnsBatch *tnsCreateBatchi(u32bit NumVert, int Dimension, int *Data){
    tnsBatch *b = CreateNew(tnsBatch);
    b->Dimension = Dimension;
    b->NumVert = NumVert;

    glGenBuffers(1, &b->VBO);
    glBindBuffer(GL_ARRAY_BUFFER, b->VBO);
    glBufferData(GL_ARRAY_BUFFER, NumVert * Dimension * sizeof(GLuint), Data, GL_DYNAMIC_DRAW);

    return b;
}
void tnsCommandUseUniformColor(tnsBatchCommand*c, real* color){
    tnsVectorCopy4d(color, c->UniformColor);
    c->UseUniformColor=1;
}
void tnsCommandUseMaterial(tnsBatchCommand*c, tnsMaterial* material){
    c->Material=material;
}
void tnsCommandUseWidth(tnsBatchCommand*c, real width){
    c->Width=width;
}
void tnsCommandOverrideColorArray(tnsBatchCommand*c, int VertCount, int ColorDimension, float* colors){
    if(!colors) return;
    c->OverrideColorArray=1; 
    glGenBuffers(1, &c->CBO); glBindBuffer(GL_ARRAY_BUFFER, c->CBO);
    glBufferData(GL_ARRAY_BUFFER, VertCount* ColorDimension * sizeof(GLfloat), colors, GL_DYNAMIC_DRAW);
    c->ColorDimension = ColorDimension;
}
tnsBatchCommand *tnsCreateCommand(tnsBatch *b, const char* name, u32bit ElementCount, int Dimension, GLenum DrawAs, u32bit *Elements, int HiddenByDefault){
    tnsBatchCommand *bc = CreateNew(tnsBatchCommand);
    bc->ElementCount = ElementCount;
    bc->DrawAs = DrawAs;
    bc->Dimension = Dimension;
    bc->name=name;
    bc->HiddenByDefault = HiddenByDefault;

    if(Elements){
        glGenBuffers(1, &bc->EBO); glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bc->EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, ElementCount * Dimension * sizeof(GLuint), Elements, GL_DYNAMIC_DRAW);
        bc->DrawElements=1;
    }else{ bc->EBO=-1; }
    //int a = GetLastError();
    lstAppendItem(&b->Branches, bc);

    return bc;
}
void tnsDeleteBatch(tnsBatch *b){
    tnsBatchCommand *bc, *NextBc;

    glDeleteBuffers(1, &b->VBO);
    if (b->NBO > -1) glDeleteBuffers(1, &b->NBO);
    if (b->CBO > -1) glDeleteBuffers(1, &b->CBO);

    for (bc = b->Branches.pFirst; bc; bc = NextBc){
        NextBc = bc->Item.pNext;
        lstRemoveItem(&b->Branches, bc);
        if (bc->EBO > -1) glDeleteBuffers(1, &bc->EBO);
        if (bc->CBO > -1) glDeleteBuffers(1, &bc->CBO);
        FreeMem(bc);
    }

    FreeMem(b);
}
void tnsDrawBatchInitArrayStates(tnsBatch* batch){
    tnsShader* cs=T->BindedShader;

	glBindBuffer(GL_ARRAY_BUFFER, batch->VBO);
	glEnableVertexAttribArray(cs->iVertex);
	glVertexAttribPointer(cs->iVertex, batch->Dimension, GL_FLOAT, 0, 0, 0);

    if(cs->iNormal>=0){
        if(batch->HasNormal){
            glBindBuffer(GL_ARRAY_BUFFER, batch->NBO); glEnableVertexAttribArray(cs->iNormal);
            glVertexAttribPointer(cs->iNormal, batch->NormalDimension, GL_FLOAT, 0, 0, 0); tnsUniformUseNormal(cs,T->StateUseNormal);
        }else{ glDisableVertexAttribArray(cs->iNormal); glVertexAttrib3f(cs->iNormal,0,0,1); tnsUniformUseNormal(cs,0); }
    }
    if(cs->iColor>=0){
        if(batch->HasColor){
            glBindBuffer(GL_ARRAY_BUFFER, batch->CBO); glEnableVertexAttribArray(cs->iColor);
            glVertexAttribPointer(cs->iColor, batch->ColorDimension, GL_FLOAT, 0, 0, 0);
        }
    }
    if(cs->iUV>=0){ glDisableVertexAttribArray(cs->iUV); }
    tnsUniformUseTexture(cs,0,0,0,0);
}
int tnsDrawBatch(tnsBatch* batch, const char* OverrideCommand, real* OverrideUniformColor, int OverrideAsArray) {
	if (!batch) return 0;
    int Drawn=0; tnsShader *LastShader=T->BindedShader,*SaveShader=T->BindedShader; int NeedInit=1;

    int IsOverrideColor=0; int PointSizeChanged=0,LineWidthChanged=0;
	for (tnsBatchCommand* bc = batch->Branches.pFirst; bc; bc = bc->Item.pNext) {
        if(OverrideCommand && !strSame(OverrideCommand, bc->name)){ continue; }
        if(!OverrideCommand && bc->HiddenByDefault){ continue; }
        if(NeedInit || (bc->Material && LastShader!=bc->Material->Shader) || ((!bc->Material)&&LastShader!=SaveShader)){
            if(bc->Material && bc->Material->Shader){ LastShader=bc->Material->Shader;
                tnsUseShader(LastShader); tnsEnableShaderv(LastShader);
            }else{
                tnsUseShader(SaveShader); tnsEnableShaderv(SaveShader);
            }
            tnsDrawBatchInitArrayStates(batch); NeedInit=0;
        }
        tnsShader* cs=T->BindedShader;
        if(cs->iColor>-1){
            if(bc->OverrideColorArray){
                glBindBuffer(GL_ARRAY_BUFFER, bc->CBO); glEnableVertexAttribArray(cs->iColor);
                glVertexAttribPointer(cs->iColor, bc->ColorDimension, GL_FLOAT, 0, 0, 0); IsOverrideColor=1;
            }else{
                if(batch->HasColor){ if(IsOverrideColor){ glBindBuffer(GL_ARRAY_BUFFER, batch->CBO); glEnableVertexAttribArray(cs->iColor);
                    glVertexAttribPointer(cs->iColor, batch->ColorDimension, GL_FLOAT, 0, 0, 0); }
                }else{ glDisableVertexAttribArray(cs->iColor); glVertexAttrib4fv(cs->iColor,T->StateColor); }
            }
            if(bc->UseUniformColor || OverrideUniformColor){ glDisableVertexAttribArray(cs->iColor); IsOverrideColor=1;
                if(OverrideUniformColor){ glVertexAttrib4f(cs->iColor,LA_COLOR4(OverrideUniformColor)); }
                else glVertexAttrib4f(cs->iColor,LA_COLOR4(bc->UniformColor)); 
            }
        }
        if(bc->DrawAs == GL_POINTS && bc->Width>1e-5){ glPointSize(bc->Width); PointSizeChanged=1; }
        if((bc->DrawAs == GL_LINE_STRIP||bc->DrawAs == GL_LINES||bc->DrawAs == GL_LINE_LOOP) && bc->Width>1e-5){
            glLineWidth(bc->Width); LineWidthChanged=1;
        }
        int DrawElements=OverrideAsArray?0:bc->DrawElements;
        if(DrawElements){
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bc->EBO);
            glDrawElements(bc->DrawAs, bc->ElementCount*bc->Dimension, GL_UNSIGNED_INT, 0);
        }else{ glDrawArrays(bc->DrawAs,0,bc->ElementCount); }
        Drawn++;
        if(PointSizeChanged){ glPointSize(1); } if(LineWidthChanged){ glLineWidth(1); }
	}

    if(SaveShader!=LastShader){ tnsUseShader(SaveShader); tnsEnableShaderv(SaveShader); }

	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    return Drawn;
}

//==========================*=======================[Texture]

int tnsGetTextureMemoryComponetCount(tnsTexture *t){
    int Comp = 0;
    int CompSize;
    switch (t->GLTexBitsType){
    case GL_RGB:
        Comp = 3;
        break;
    case GL_RGBA:
        Comp = 4;
        break;
    }
    return t->Width * t->Height * Comp;
}

int tnsInit2DTexture(tnsTexture *t, GLint glInternalFormat, int w, int h, int Multisample){
    if (!t) return 0;

    if(0/*glInternalFormat==GL_DEPTH_COMPONENT32F*/){
        t->Width = w;
        t->Height = h;
        t->GLTexBitsType = GL_DEPTH_COMPONENT32F;
        t->GLTexType = GL_RENDERBUFFER;
        t->Multisample = Multisample;
        glGenRenderbuffers(1, &t->GLTexHandle);
        
        tnsConfigure2DTexture(t);
    }else{
        t->Width = w;
        t->Height = h;
        t->GLTexBitsType = glInternalFormat;
#ifdef LAGUI_ANDROID
        t->GLTexType = GL_TEXTURE_2D;
#else
        t->GLTexType = Multisample?GL_TEXTURE_2D_MULTISAMPLE:GL_TEXTURE_2D;
        t->Multisample = Multisample;
#endif
        glGenTextures(1, &t->GLTexHandle);

        tnsConfigure2DTexture(t);
    }

    return 1;
}
int tnsInit3DTexture(tnsTexture *t, GLint glInternalFormat, int w, int h, int slices){
    if (!t) return 0;
    t->Width = w;
    t->Height = h;
    t->GLTexBitsType = glInternalFormat;
    t->GLTexType = GL_TEXTURE_3D;
    glGenTextures(1, &t->GLTexHandle);
    tnsConfigure3DTexture(t);
    return 1;
}

void tnsConfigure2DTexture(tnsTexture *t){
    tnsBindTexture(t);
    if(t->GLTexType==GL_RENDERBUFFER){
        if(t->Multisample){ glRenderbufferStorageMultisample(GL_RENDERBUFFER, t->Multisample, t->GLTexBitsType, t->Width, t->Height); }
        else { glRenderbufferStorage(GL_RENDERBUFFER, t->GLTexBitsType, t->Width, t->Height); }
    }else{
        int isDepth=t->GLTexBitsType==GL_DEPTH_COMPONENT||t->GLTexBitsType==GL_DEPTH_COMPONENT16||
            t->GLTexBitsType==GL_DEPTH_COMPONENT24||t->GLTexBitsType==GL_DEPTH_COMPONENT32F;
        int format=isDepth?GL_DEPTH_COMPONENT:(t->GLTexBitsType==GL_R8?GL_RED:(t->GLTexBitsType==GL_RG8?GL_RG:(t->GLTexBitsType==GL_RGB8?GL_RGB:GL_RGBA)));
        int type=isDepth?GL_UNSIGNED_INT:GL_UNSIGNED_BYTE;
        //if(t->GLTexBitsType==GL_RGBA4){ format=GL_RGB; type=GL_UNSIGNED_SHORT_4_4_4_4; printf("ye\n"); }
        if(t->GLTexBitsType==GL_RGBA16UI){ format=GL_RGBA_INTEGER; type=GL_UNSIGNED_SHORT; t->IsUIntTexture=1; }
        elif(t->GLTexBitsType==GL_RGBA32UI){ format=GL_RGBA_INTEGER; type=GL_UNSIGNED_INT; t->IsUIntTexture=1; }
        elif(t->GLTexBitsType==GL_R32UI){ format=GL_RED_INTEGER; type=GL_UNSIGNED_INT; t->IsUIntTexture=1; }
        elif(t->GLTexBitsType==GL_RGBA16F){ format=GL_RGBA; type=GL_FLOAT; }
        elif(t->GLTexBitsType==GL_DEPTH_STENCIL){ format=GL_DEPTH_STENCIL; type=GL_UNSIGNED_INT_24_8; t->GLTexBitsType=GL_DEPTH24_STENCIL8; }
#ifdef LAGUI_ANDROID
        if(isDepth){t->GLTexBitsType=GL_DEPTH_COMPONENT24; type=GL_UNSIGNED_INT;}
        t->Multisample=0;
#endif
        if(t->Multisample){
#ifndef LAGUI_ANDROID
            glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, t->Multisample, t->GLTexBitsType, t->Width, t->Height, GL_TRUE);
#endif
        }else{ glTexImage2D(GL_TEXTURE_2D, 0, t->GLTexBitsType, t->Width, t->Height, 0, format, type, 0);
            if(t->IsUIntTexture){
                glTexStorage2D(GL_TEXTURE_2D, 1,t->GLTexBitsType,t->Width, t->Height);
            }
            //if(t->GLTexBitsType==GL_RGBA4){ glTexStorage2D(GL_TEXTURE_2D,0,GL_RGBA4,t->Width,t->Height); }
            int a=glGetError();
            int clamp = GL_CLAMP_TO_EDGE;
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, clamp);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, clamp);
            int filter = GL_LINEAR; if (t->IsUIntTexture) { filter = GL_NEAREST; }
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
            //glTexEnvi(GL_TEXTURE_2D, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        }
    }
    tnsUnbindTexture();
}
void tnsConfigure3DTexture(tnsTexture *t){
    tnsBindTexture(t);
    glTexImage3D(GL_TEXTURE_3D, 0, t->GLTexBitsType, t->Width, t->Height, t->Slices, 0, GL_RGBA, GL_FLOAT, 0);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        //glTexEnvi(GL_TEXTURE_2D, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    tnsUnbindTexture();
}

void tnsReconfigureTextureParameters(int Multisample){
    laListHandle* l = tKnlGetTextureList();
    for(tnsOffscreen* o=T->Offscreens.pFirst;o;o=o->Item.pNext){ tnsTexture* t;
        for(int i=0;i<4;i++){
            t=o->pColor[i];
            if(t){
                int recreate=0;
#ifndef LAGUI_ANDROID
                t->Multisample = Multisample;
                if(t->Multisample){ if(t->GLTexType==GL_TEXTURE_2D){t->GLTexType=GL_TEXTURE_2D_MULTISAMPLE; recreate=1;}}
                else { if(t->GLTexType==GL_TEXTURE_2D_MULTISAMPLE){t->GLTexType=GL_TEXTURE_2D; recreate=1;}}
#endif
                if(recreate){
                    glDeleteTextures(1, &t->GLTexHandle);
                    glGenTextures(1, &t->GLTexHandle);
                }
                T->TexColor=0;
                tnsConfigure2DTexture(t);
                tnsAttach2DOffscreenBuffer(o, GL_COLOR_ATTACHMENT0+i, t);
            }
        }
        t=o->pDepth;
        if(t){
            int recreate=0; 
#ifndef LAGUI_ANDROID
            if((!t->Multisample && Multisample)||(t->Multisample && !Multisample)){ recreate=1; }
            if(Multisample){ t->GLTexType=GL_TEXTURE_2D_MULTISAMPLE; }else{ t->GLTexType=GL_TEXTURE_2D; }
            t->Multisample = Multisample;
#endif
            if(recreate){
                glDeleteTextures(1, &t->GLTexHandle);
                glGenTextures(1, &t->GLTexHandle);
            } T->TexColor=0;
            tnsConfigure2DTexture(t);
            tnsAttach2DOffscreenBuffer(o, GL_DEPTH_ATTACHMENT, t);
        }
    }
}

tnsTexture *tnsCreate2DTexture(GLint glInternalFormat, int w, int h, int Multisample){
    tnsTexture *tex = memAcquire(sizeof(tnsTexture));
    tnsInit2DTexture(tex, glInternalFormat, w, h, Multisample);
    laNotifyUsers("tns.texture_list");
    lstAppendItem(tKnlGetTextureList(), tex);
    return tex;
};
tnsTexture *tnsCreate3DTexture(GLint glInternalFormat, int w, int h, int slices){
    tnsTexture *tex = memAcquire(sizeof(tnsTexture));
    tnsInit3DTexture(tex, glInternalFormat, w, h, slices);
    laNotifyUsers("tns.texture_list");
    lstAppendItem(tKnlGetTextureList(), tex);
    return tex;
};
void tnsCopyScreenTo2DTexture(tnsTexture *target, int x_lower_left, int y_lower_left, int w, int h){
    if(target->GLTexType!=GL_TEXTURE_2D) return;
    tnsBindTexture(target);
    glReadBuffer(GL_BACK);
    glCopyTexSubImage2D(target->GLTexType, 0, 0, 0, x_lower_left, y_lower_left, w, h);
    tnsUnbindTexture();
}

void tnsUniformUseOffset(tnsShader* s, int use){
    glUniform1i(s->iDoOffset,use);
}

void tnsUseMaskTexture(tnsTexture *t){
    if(!t){T->StateTextureMode=0; return;}
    T->StateTexColor = t; T->StateTextureMode=1;
}
#define TEX_MODE_IS_MIXING(T) \
    (T->StateTextureMode>=5&&T->StateTextureMode<=100)
void tnsUseTexture(tnsTexture *t){
    if(!t){ T->StateTextureMode=0; return; }
    if(t->GLTexType == GL_TEXTURE_2D){
        T->StateTexColor = t;
        switch(t->ChromaCompression){
            case TNS_CHROMA_COMPRESSION_YUYV: T->StateTextureMode = 101; break;
            case TNS_CHROMA_COMPRESSION_UYVY: T->StateTextureMode = 102; break;
            default: T->StateTextureMode = TEX_MODE_IS_MIXING(T)?T->StateTextureMode:2;
        }
    }
#ifndef LAGUI_ANDROID
    else if(t->GLTexType == GL_TEXTURE_2D_MULTISAMPLE){ T->StateTexColor = t; T->StateTextureMode=3; }
#endif
}
void tnsUseTexture2(tnsTexture* t, int mixmode){
    if(!t){ T->StateTextureMode=TEX_MODE_IS_MIXING(T)?2:0; return; }
    if(t->GLTexType == GL_TEXTURE_2D){ T->StateTexColor2=t; T->StateTextureMode=5+mixmode; }
}
void tnsUseNoTexture(){
    tnsUseTexture(0);
}
void tnsUseMultiplyColor(int enable){
    T->StateMultiplyColor=enable?1:0;
}

void tnsUniformUseNormal(tnsShader* s, int Use){
    if(T->SetUseNormal!=Use){ T->SetUseNormal=Use; glUniform1i(s->iUseNormal,Use); }
}
void tnsUseNormal(int Use){ T->StateUseNormal=Use; }
void tnsUniformUseHalftone(tnsShader* s, real Use){
    if(T->SetUseHalftone!=Use){ T->SetUseHalftone=Use; glUniform1f(s->iUseHalftone,Use); }
}
void tnsUniformHalftoneSize(tnsShader* s, real Use){
    if(T->SetHalftoneSize!=Use){ T->SetHalftoneSize=Use; glUniform1f(s->iHalftoneSize,Use); }
}
void tnsUseHalftone(real Factor){ T->StateUseHalftone=Factor; }

void tnsActiveTexture(GLenum tex){
    if (T->GlTextureSets != tex) glActiveTexture(tex);
    T->GlTextureSets = tex;
}
void tnsBindTexture(tnsTexture *t){
    if (!t) return;
    if(t->IsUIntTexture){ tnsActiveTexture(GL_TEXTURE2); glBindTexture(t->GLTexType, t->GLTexHandle); T->TexColor=t; return; }
    if(t->GLTexType == GL_TEXTURE_2D){ tnsActiveTexture(GL_TEXTURE0); glBindTexture(t->GLTexType, t->GLTexHandle); T->TexColor=t;}
#ifndef LAGUI_ANDROID
    elif(t->GLTexType == GL_TEXTURE_2D_MULTISAMPLE){ tnsActiveTexture(GL_TEXTURE1); glBindTexture(t->GLTexType, t->GLTexHandle); T->TexColor=t;}
#endif
#ifndef LA_USE_GLES
    elif(t->GLTexType == GL_RENDERBUFFER){ glBindRenderbufferEXT(GL_RENDERBUFFER, t->GLTexHandle); T->TexRenderbuffer = t;}
#endif
    elif(t->GLTexType == GL_TEXTURE_3D){ tnsActiveTexture(GL_TEXTURE0); glBindTexture(t->GLTexType, t->GLTexHandle); T->TexColor=t;}
}
void tnsUnbindTexture(){
#ifndef LA_USE_GLES
    if(T->TexRenderbuffer){ if(T->TexRenderbuffer->GLTexType == GL_RENDERBUFFER){ glBindRenderbufferEXT(GL_RENDERBUFFER, 0); T->TexRenderbuffer=0;}}
#endif
    if(T->TexColor){
        if(T->TexColor->IsUIntTexture){ tnsActiveTexture(GL_TEXTURE2); glBindTexture(T->TexColor->GLTexType, 0); T->TexColor=0; return; }
        if(T->TexColor->GLTexType == GL_TEXTURE_2D){tnsActiveTexture(GL_TEXTURE0);}
#ifndef LAGUI_ANDROID
        else if(T->TexColor->GLTexType == GL_TEXTURE_2D_MULTISAMPLE){tnsActiveTexture(GL_TEXTURE1);}
#endif
        else if(T->TexColor->GLTexType == GL_TEXTURE_3D){tnsActiveTexture(GL_TEXTURE0);}
        glBindTexture(T->TexColor->GLTexType, 0); T->TexColor=0;
    }
    if(T->TexColor2){
        tnsActiveTexture(GL_TEXTURE4);  glBindTexture(T->TexColor2->GLTexType, 0); T->TexColor2=0;
    }
}
void tnsBindTexture2(tnsTexture *t){
    if ((!t) || T->TexColor2==t) return;
    if(t->GLTexType == GL_TEXTURE_2D){ tnsActiveTexture(GL_TEXTURE4); glBindTexture(t->GLTexType, t->GLTexHandle); T->TexColor2=t;}
}
void tnsUnbindTexture2(){
    if(T->TexColor2){
        tnsActiveTexture(GL_TEXTURE4); glBindTexture(T->TexColor2->GLTexType, 0); T->TexColor2=0;
    }
}
void tnsUniformUseTexture(tnsShader* s, int mode, int sample, int TexIsUInt, int Tex2IsUInt){
    if(s->StateTextureMode != mode){ s->StateTextureMode=mode; glUniform1i(s->iTextureMode,mode); }
    if(mode==3 && s->StateSampleAmount != sample){ s->StateSampleAmount=sample, glUniform1i(s->iSampleAmount,sample); }
    if(s->StateTexIsUI != TexIsUInt){ s->StateTexIsUI=TexIsUInt; glUniform1i(s->iTexIsUI,TexIsUInt); }
    if(s->StateTex2IsUI != Tex2IsUInt){ s->StateTex2IsUI=Tex2IsUInt; glUniform1i(s->iTex2IsUI,TexIsUInt); }
}
void tnsUniformUseMultiplyColor(tnsShader* s, int enable){
    int mode=enable?1:0;
    if(s->StateMultiplyColor != mode){ s->StateMultiplyColor=mode; glUniform1i(s->iMultiplyColor,mode); }
}
void tnsUniformColorMode(tnsShader* s, int mode){
    glUniform1i(s->iColorMode,mode);
}
void tnsUniformHCYGamma(tnsShader* s, float Gamma){
    glUniform1f(s->iHCYGamma,Gamma);
}
void tnsUniformInputColorSpace(tnsShader* s, int ColorSpace){
    glUniform1i(s->iInputColorSpace,ColorSpace);
}
void tnsUniformOutputColorSpace(tnsShader* s, int ColorSpace){
    glUniform1i(s->iOutputColorSpace,ColorSpace);
}
void tnsUniformShowColorOverflowStripes(tnsShader* s, int Show){
    glUniform1i(s->iShowStripes,Show);
}
void tnsUniformColorComposing(tnsShader* s, int Composing, real gamma, real blackpoint, int UseLut){
    glUniform1i(s->iComposing,Composing); glUniform1f(s->iComposingGamma,gamma);
    glUniform1f(s->iComposingBlackpoint,blackpoint);
    glUniform1i(s->iUseLut,UseLut);
}

void tnsDraw2DTextureDirectly(tnsTexture *t, real x, real y, real w, real h){
    real Verts[8];
    real UV[8] = {
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f};
    tnsMakeQuad2d(Verts, x, y, x + w, y, x + w, y + h, x, y + h);
    tnsUseTexture(t);
    tnsVertexArray2d(Verts, 4);
    tnsTexCoordArray2d(UV, 4);
    tnsPackAs(GL_TRIANGLE_FAN);
}
void tnsDraw2DTextureArg(tnsTexture *t,
                         real x_upper_right, real y_upper_right, int w, int h,
                         real *MultiplyColor,
                         real LPadding, real RPadding, real TPadding, real BPadding){
    real Verts[8];
    real UV[8] = {
        0.0f + LPadding, 1.0f - TPadding,
        1.0f - RPadding, 1.0f - TPadding,
        1.0f - RPadding, 0.0f + BPadding,
        0.0f + LPadding, 0.0f + BPadding};
    tnsMakeQuad2d(Verts,
                  x_upper_right, y_upper_right,
                  x_upper_right + w, y_upper_right,
                  x_upper_right + w, y_upper_right + h,
                  x_upper_right, y_upper_right + h);

    if (MultiplyColor){ tnsColor4dv(MultiplyColor); tnsUseMultiplyColor(1); }

    tnsUseTexture(t);
    tnsVertexArray2d(Verts, 4);
    tnsTexCoordArray2d(UV, 4);
    tnsPackAs(GL_TRIANGLE_FAN);
    tnsUseMultiplyColor(0);
}
void tnsDeleteTexture(tnsTexture *t){
    laListHandle *lst = tKnlGetTextureList();

    tnsUnbindTexture();

    if (!t) return;

    if (t->GLTexType == GL_RENDERBUFFER){
#ifndef LA_USE_GLES
        glBindRenderbufferEXT(GL_RENDERBUFFER, t->GLTexHandle);
        glDeleteRenderbuffersEXT(1, &t->GLTexHandle);
#endif
    }else{
        //glBindTexture(t->GLTexType, 0);
        glDeleteTextures(1, &t->GLTexHandle);
    }
    laNotifyUsers("tns.texture_list");
    lstRemoveItem(lst, t);
    if (t->DrawData) FreeMem(t->DrawData);
    memFree(t);
}

int tnsTextureMemorySize(tnsTexture *t, int Mem){
    int ElemSize;

    if (Mem) return t->Width * t->Height * sizeof(void *);

    switch (t->GLTexBitsType){
    default:
    case GL_R8:
        ElemSize = sizeof(char) * 1;
        break;
    case GL_RG8:
        ElemSize = sizeof(char) * 2;
        break;
    case GL_RGB8:
        ElemSize = sizeof(char) * 3;
        break;
    case GL_RGBA8:
        ElemSize = sizeof(char) * 4;
        break;
    case GL_RGBA32F:
        ElemSize = sizeof(float) * 4;
        break;
    case GL_DEPTH_COMPONENT32F:
        ElemSize = sizeof(float);
        break;
    }

    t->ElemSize = ElemSize;

    return t->Width * t->Height * ElemSize;
}

#ifdef LA_WITH_PNG

tnsImage* tnsNewImage(void* MemPNG){
    tnsImage* im=memAcquire(sizeof(tnsImage));
    lstAppendItem(&T->Images,im);
    im->MemPNG=MemPNG; return im;
}
STRUCTURE(tnsPNGRead){
    unsigned char* data;
    size_t NextData;
};
static void _tns_png_read(png_struct *ps, png_byte *data, png_size_t length){
    tnsPNGRead *PNGRead = (tnsPNGRead*)png_get_io_ptr(ps);
    memcpy(data,&PNGRead->data[PNGRead->NextData],length);
    PNGRead->NextData+=length;
}
void tns_ImageToTexture(tnsImage* im){
    if(!im) return;
    if(!im->MemPNG) return;
    png_structp png_ptr=0;
    png_infop info_ptr=0;
    tnsPNGRead PNGRead={0};
    png_ptr = png_create_read_struct (PNG_LIBPNG_VER_STRING,0,0,0); if (!png_ptr) { goto cleanup_png_read; }
    info_ptr = png_create_info_struct(png_ptr); if (!info_ptr) { goto cleanup_png_read; }
    if (setjmp(png_jmpbuf(png_ptr))) { goto cleanup_png_read; }
    PNGRead.data=im->MemPNG; png_set_read_fn(png_ptr, &PNGRead, _tns_png_read);
    png_read_info(png_ptr, info_ptr);
    png_set_swap(png_ptr);
    if (png_get_interlace_type (png_ptr, info_ptr) != PNG_INTERLACE_NONE){ goto cleanup_png_read; }

    png_byte ColorType = png_get_color_type(png_ptr, info_ptr);
    png_byte BitDepth = png_get_bit_depth(png_ptr, info_ptr);
    int HasAlpha = ColorType & PNG_COLOR_MASK_ALPHA;
    if (ColorType == PNG_COLOR_TYPE_PALETTE) { png_set_palette_to_rgb(png_ptr); }
    //if (ColorType == PNG_COLOR_TYPE_GRAY && BitDepth < 8) { png_set_expand_gray_1_2_4_to_8(png_ptr); }
    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS)) { png_set_tRNS_to_alpha(png_ptr); HasAlpha = 1; }
    if (BitDepth>8) {png_set_strip_16(png_ptr);} if (BitDepth<8) { png_set_expand(png_ptr); }
    if (!HasAlpha) { png_set_add_alpha(png_ptr, 0xFFFF, PNG_FILLER_AFTER); }
    if (ColorType == PNG_COLOR_TYPE_GRAY || ColorType == PNG_COLOR_TYPE_GRAY_ALPHA) { png_set_gray_to_rgb(png_ptr); }
    png_read_update_info(png_ptr, info_ptr);
    if (png_get_bit_depth(png_ptr, info_ptr)!=8) { goto cleanup_png_read; }
    if (png_get_color_type(png_ptr, info_ptr) != PNG_COLOR_TYPE_RGB_ALPHA){ goto cleanup_png_read; }
    if (png_get_channels(png_ptr, info_ptr) != 4) { goto cleanup_png_read; }

    int W = png_get_image_width(png_ptr, info_ptr);
    int H = png_get_image_height(png_ptr, info_ptr);
    
    unsigned char* buf=calloc(W*4,H*sizeof(unsigned char));

    for(int i=0;i<H;i++){ png_read_row(png_ptr, &buf[((H-i-1)*W)*4], NULL); }
    im->Texture=tnsCreate2DTexture(GL_RGBA8,W,H,0);
    tnsBindTexture(im->Texture); glTexSubImage2D(GL_TEXTURE_2D,0,0,0,W,H,GL_RGBA,GL_UNSIGNED_BYTE,buf);
    tnsUnbindTexture();

    free(buf);

cleanup_png_read:
    if(png_ptr && info_ptr) png_destroy_read_struct(&png_ptr,&info_ptr,0);
}
void tnsUseImage(tnsImage* im){
    if(!im) return;
    if(im->UserCount==0 || !im->Texture){
        if(!im->MemPNG){
            if(im->Texture){ tnsDeleteTexture(im->Texture); im->Texture=0; }
        }else{
            tns_ImageToTexture(im);
        }
    }
    im->UserCount++;
}
void tnsRefreshImage(tnsImage* im,void* data){
    if(!im) return;
    if(im->MemPNG){free(im->MemPNG);}
    im->MemPNG=data; tnsDeleteTexture(im->Texture); im->Texture=0;
}
void tnsStopUsingImage(tnsImage* im){
    if(!im) return;
    im->UserCount--;
    if(im->UserCount<=0){ im->UserCount=0; tnsDeleteTexture(im->Texture); im->Texture=0; }
}
void tnsRemoveImage(tnsImage* im){
    if(!im) return;
    while(im->UserCount) tnsStopUsingImage(im);
    if(im->MemPNG) free(im->MemPNG);
    lstRemoveItem(&T->Images,im); memFree(im);
}

#endif //png

//====================================================[NEW RENDER KERNEL]
//=================[Immediate-style api]

void tnsColor4d(real r, real g, real b, real a){ tnsVectorSet4(T->StateColor,r,g,b,a); }
void tnsColor4dv(real *rgba){ tnsVectorSet4v(T->StateColor,rgba); }
void tnsLineWidth(real Width){ if(TNS_FLOAT_CLOSE_ENOUGH_WIDER(Width,T->StateLineWidth)) return;
    T->StateLineWidth=Width;
}
void tnsPointSize(real PointSize){ if(TNS_FLOAT_CLOSE_ENOUGH_WIDER(PointSize,T->StatePointSize)) return;
    T->StatePointSize=PointSize;
}

void tnsVertex3d(real x, real y, real z){
    tnsCommand *c = &T->DrawingCommand[T->NextCommand];
    int vend = c->VertEnd;

    c->UseVert = 1;

    if (!c->Dimensions) c->Dimensions = 3;

    if(arrEnsureLength(&T->Vert, T->NextVert+c->Dimensions, &T->MaxVert, sizeof(GLfloat))){
        glBindBuffer(GL_ARRAY_BUFFER, T->VertBufObject);
        glBufferData(GL_ARRAY_BUFFER, T->MaxVert * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);
    }

    GLfloat *varr = T->Vert;
    if (c->Dimensions == 3){
        varr[vend] = x;
        varr[vend + 1] = y;
        varr[vend + 2] = z;
        c->NumVert++;
        T->NextVert += 3;
        c->VertEnd += 3;
    }else{
        varr[vend] = x;
        varr[vend + 1] = y;
        c->NumVert++;
        T->NextVert += 2;
        c->VertEnd += 2;
    }
}
void tnsVertex2d(real x, real y){
    tnsCommand *c = &T->DrawingCommand[T->NextCommand];
    int vend = c->VertEnd;

    c->UseVert = 1;

    if (!c->Dimensions) c->Dimensions = 2;
    
    if(arrEnsureLength(&T->Vert, T->NextVert+c->Dimensions, &T->MaxVert, sizeof(GLfloat))){
        glBindBuffer(GL_ARRAY_BUFFER, T->VertBufObject);
        glBufferData(GL_ARRAY_BUFFER, T->MaxVert * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);
    }

    GLfloat *varr = T->Vert;
    if (c->Dimensions == 2){
        varr[vend] = x;
        varr[vend + 1] = y;
        c->NumVert++;
        T->NextVert += 2;
        c->VertEnd += 2;
    }else{
        tnsVertex3d(x, y, 0.0f);
    }
}
void tnsVertexArray2d(real *verts, int amount){
    tnsCommand *c = &T->DrawingCommand[T->NextCommand];
    int trans = 2 * amount;
    int vend = c->VertEnd;

    c->UseVert = 1;

    if (!c->Dimensions) c->Dimensions = 2;

    if(arrEnsureLength(&T->Vert, T->NextVert+c->Dimensions*amount, &T->MaxVert, sizeof(GLfloat))){
        glBindBuffer(GL_ARRAY_BUFFER, T->VertBufObject);
        glBufferData(GL_ARRAY_BUFFER, T->MaxVert * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);
    }

    GLfloat *varr = T->Vert;
    if (c->Dimensions == 2){
        int i;
        for (i = 0; i < trans; i++){
            varr[vend] = verts[i];
            vend++;
        }
        //memcpy(&varr[vend], verts, trans*sizeof(real));
        c->VertEnd += trans;
        c->NumVert += amount;
        T->NextVert += trans;
    }
}
void tnsVertexArray3d(real *verts, int amount){
    tnsCommand *c = &T->DrawingCommand[T->NextCommand];
    int trans = 3 * amount;
    int vend = c->VertEnd;

    c->UseVert = 1;

    if (!c->Dimensions) c->Dimensions = 3;

    if(arrEnsureLength(&T->Vert, T->NextVert+c->Dimensions*amount, &T->MaxVert, sizeof(GLfloat))){
        glBindBuffer(GL_ARRAY_BUFFER, T->VertBufObject);
        glBufferData(GL_ARRAY_BUFFER, T->MaxVert * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);
    }

    GLfloat *varr = T->Vert;
    if (c->Dimensions == 3){
        int i;
        for (i = 0; i < trans; i++){
            varr[vend] = verts[i];
            vend++;
        }
        //memcpy(&varr[vend], verts, trans*sizeof(real));
        c->VertEnd += trans;
        c->NumVert += amount;
        T->NextVert += trans;
    }
}
void tnsColorArray4d(real *colors, int amount){
    tnsCommand *c = &T->DrawingCommand[T->NextCommand];
    int trans = 4 * amount;
    int ofst = c->ColorEnd;

    c->UseColor = 1;

    if(arrEnsureLength(&T->Color, T->NextColor+4*amount, &T->MaxColor, sizeof(GLfloat))){
        glBindBuffer(GL_ARRAY_BUFFER, T->ColorBufObject);
        glBufferData(GL_ARRAY_BUFFER, T->MaxColor * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);
    }

    GLfloat *carr = T->Color;
    int i;
    for (i = 0; i < trans; i++){
        carr[ofst] = colors[i];
        ofst++;
    }
    //memcpy(&T->Color[c->ColorEnd], colors, trans*sizeof(real));
    c->ColorEnd += trans;
    T->NextColor += trans;
}
void tnsNormalArray3d(real *normals, int amount){
    tnsCommand *c = &T->DrawingCommand[T->NextCommand];
    int trans = 3 * amount;
    int ofst = c->NormalEnd;

    c->UseNormal = 1;

    if(arrEnsureLength(&T->Normal, T->NextNormal+3*amount, &T->MaxNormal, sizeof(GLfloat))){
        glBindBuffer(GL_ARRAY_BUFFER, T->NormalBufObject);
        glBufferData(GL_ARRAY_BUFFER, T->MaxNormal * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);
    }

    GLfloat *narr = T->Normal;
    int i;
    for (i = 0; i < trans; i++){
        narr[ofst] = normals[i];
        ofst++;
    }
    //memcpy(&T->Normal[c->NormalEnd], normals, trans*sizeof(real));
    c->NormalEnd += trans;
    T->NextNormal += trans;
}
void tnsTexCoordArray2d(real *coords, int amount){
    tnsCommand *c = &T->DrawingCommand[T->NextCommand];
    int trans = 2 * amount;
    int ofst = c->TexCoordEnd;

    c->UseTexCoord = 1;
    c->UVDimensions = 2;

    if(arrEnsureLength(&T->TexCoord, T->NextTexCoord+2*amount, &T->MaxTexCoord, sizeof(GLfloat))){
        glBindBuffer(GL_ARRAY_BUFFER, T->TexCoordBufObject);
        glBufferData(GL_ARRAY_BUFFER, T->MaxTexCoord * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);
    }

    GLfloat *carr = T->TexCoord;
    int i;
    for (i = 0; i < trans; i++){
        carr[ofst] = coords[i];
        ofst++;
    }
    //memcpy(&T->TexCoord[c->TexCoordEnd], coords, trans*sizeof(real));
    c->TexCoordEnd += trans;
    T->NextTexCoord += trans;
}
void tnsTexCoordArray3d(real *coords, int amount){
    tnsCommand *c = &T->DrawingCommand[T->NextCommand];
    int trans = 3 * amount;
    int ofst = c->TexCoordEnd;

    c->UseTexCoord = 1;
    c->UVDimensions = 3;

    if(arrEnsureLength(&T->TexCoord, T->NextTexCoord+3*amount, &T->MaxTexCoord, sizeof(GLfloat))){
        glBindBuffer(GL_ARRAY_BUFFER, T->TexCoordBufObject);
        glBufferData(GL_ARRAY_BUFFER, T->MaxTexCoord * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);
    }

    GLfloat *carr = T->TexCoord;
    int i;
    for (i = 0; i < trans; i++){ carr[ofst] = coords[i]; ofst++; }
    c->TexCoordEnd += trans;
    T->NextTexCoord += trans;
}
void tnsIndexArray(GLuint *index, short amount){
    tnsCommand *c = &T->DrawingCommand[T->NextCommand];

    //if (c->UseIndex) return;
    c->UseIndex = 1;

    if(arrEnsureLength(&T->Index, T->NextIndex+amount, &T->MaxIndex, sizeof(GLuint))){
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, T->IndexBufObject);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, T->MaxIndex * sizeof(GLuint), 0, GL_DYNAMIC_DRAW);
    }

    memcpy(&T->Index[c->IndexEnd], index, amount * sizeof(GLuint));
    c->IndexEnd += amount;
    c->NumIndex += amount;
    T->NextIndex += amount;
}

void tnsPackAs(GLenum Mode){
    arrEnsureLength(&T->DrawingCommand, T->NextCommand+1, &T->MaxCommand, sizeof(tnsCommand));
    tnsCommand *c = &T->DrawingCommand[T->NextCommand];
    tnsCommand *nc;

    T->NextCommand++;
    nc = &T->DrawingCommand[T->NextCommand];

    memcpy(c->UniformColor, T->StateColor, sizeof(GLfloat) * 4);

#ifndef LAGUI_ANDROID
    if (Mode == GL_QUAD_STRIP || Mode == GL_QUADS) Mode=GL_TRIANGLE_STRIP;
#endif
    //if (Mode == GL_QUADS) Mode=GL_TRIANGLE_STRIP;
    c->Mode = Mode;
    c->ReplaceShader = T->StateShader;
    c->ColorTexture = T->StateTexColor;
    c->ColorTexture2 = T->StateTexColor2;
    c->TextureMode = T->StateTextureMode;
    c->MultiplyColor = T->StateMultiplyColor;
    c->LineWidth=T->StateLineWidth;
    c->PointSize=T->StatePointSize;
    c->UseHalftone = T->StateUseHalftone;

    memset(nc, 0, sizeof(tnsCommand));
    nc->VertBegin = nc->VertEnd = c->VertEnd;
    nc->NormalBegin = nc->NormalEnd = c->NormalEnd;
    nc->ColorBegin = nc->ColorEnd = c->ColorEnd;
    nc->TexCoordBegin = nc->TexCoordEnd = c->TexCoordEnd;
    nc->IndexBegin = nc->IndexEnd = c->IndexEnd;

#ifdef LAGUI_ANDROID
    if(T->NextVert > 255){ tnsFlush(); }
#endif
}
void tnsFlush(){
    tnsShader *cs = T->CurrentShader;
    tnsCommand *tc = &T->DrawingCommand[0];
    tnsCommand *c = tc;
    int PrevDimensions = 0;
    int LastVertBegin = 0;

    if (!c || !cs) return;

    if (T->NextVert){ glBindBuffer(GL_ARRAY_BUFFER, T->VertBufObject);
        glBufferSubData(GL_ARRAY_BUFFER, 0, T->NextVert * sizeof(GLfloat), T->Vert);
    }
    if (T->NextColor){ glBindBuffer(GL_ARRAY_BUFFER, T->ColorBufObject);
        glBufferSubData(GL_ARRAY_BUFFER, 0, T->NextColor * sizeof(GLfloat), T->Color);
    }
    if (T->NextNormal){ glBindBuffer(GL_ARRAY_BUFFER, T->NormalBufObject);
        glBufferSubData(GL_ARRAY_BUFFER, 0, T->NextNormal * sizeof(GLfloat), T->Normal);
    }
    if (T->NextTexCoord){ glBindBuffer(GL_ARRAY_BUFFER, T->TexCoordBufObject);
        glBufferSubData(GL_ARRAY_BUFFER, 0, T->NextTexCoord * sizeof(GLfloat), T->TexCoord);
    }

    for (int i=0;i<T->NextCommand;i++){ c=&T->DrawingCommand[i];
        if (c->LineWidth && c->LineWidth!=T->SetLineWidth){ glLineWidth(c->LineWidth); T->SetLineWidth=c->LineWidth; }
        if (c->LineWidth && c->PointSize!=T->SetPointSize){
            glPointSize(c->PointSize); T->SetPointSize=c->PointSize;
        }
        if (c->ReplaceShader && c->ReplaceShader != T->CurrentShader){
            tnsEnableShaderv(c->ReplaceShader); cs = c->ReplaceShader;
            if (!cs) continue;
        }

        glBindBuffer(GL_ARRAY_BUFFER, T->VertBufObject);
        if (c->UseVert){
            glEnableVertexAttribArray(cs->iVertex);
            glVertexAttribPointer(cs->iVertex, (c->Dimensions ? c->Dimensions : PrevDimensions),
                                  GL_FLOAT, 0, 0, c->VertBegin * sizeof(GLfloat));
            LastVertBegin = c->VertBegin;
        }else{
            glEnableVertexAttribArray(cs->iVertex);
            glVertexAttribPointer(cs->iVertex, (c->Dimensions ? c->Dimensions : PrevDimensions),
                                  GL_FLOAT, 0, 0, LastVertBegin * sizeof(GLfloat));
        }

        PrevDimensions = (c->Dimensions ? c->Dimensions : PrevDimensions);

        if (c->UseIndex){
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, T->IndexBufObject);
            glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, c->NumIndex * sizeof(GLuint), &T->Index[c->IndexBegin]);
        }

        if (cs->iNormal != -1){
            glBindBuffer(GL_ARRAY_BUFFER, T->NormalBufObject);
            if (c->UseNormal){
                glEnableVertexAttribArray(cs->iNormal); tnsUniformUseNormal(cs,1);
            }
            glVertexAttribPointer(cs->iNormal, 3, GL_FLOAT, 0, 0, c->NormalBegin * sizeof(GLfloat));
            if (!c->UseNormal || c->Dimensions == 2){
                glDisableVertexAttribArray(cs->iNormal); tnsUniformUseNormal(cs,0);
                glVertexAttrib3f(cs->iNormal, 0, 0, -1);
            }
        }

        if (cs->iColor != -1){
            glBindBuffer(GL_ARRAY_BUFFER, T->ColorBufObject);
            if (c->UseColor){
                glEnableVertexAttribArray(cs->iColor);
                glVertexAttribPointer(cs->iColor, 4, GL_FLOAT, 0, 0, c->ColorBegin * sizeof(GLfloat));
            }else{
                glDisableVertexAttribArray(cs->iColor);
                glVertexAttrib4fv(cs->iColor, c->UniformColor);
            }
        }

        if (cs->iUV != -1){
            if (c->UseTexCoord){
                glBindBuffer(GL_ARRAY_BUFFER, T->TexCoordBufObject);
                if (c->UseTexCoord){ glEnableVertexAttribArray(cs->iUV);
                    glVertexAttribPointer(cs->iUV, c->UVDimensions, GL_FLOAT, 0, 0, c->TexCoordBegin * sizeof(GLfloat)); }
                else{ glDisableVertexAttribArray(cs->iUV); }
            }
        }

        if (c->TextureMode && c->ColorTexture && cs->iTexColor!=-1){
            tnsBindTexture(c->ColorTexture);
            if(c->ColorTexture2) tnsBindTexture2(c->ColorTexture2);
            tnsUniformUseTexture(cs, c->TextureMode, c->ColorTexture->Multisample,
                c->ColorTexture?c->ColorTexture->IsUIntTexture:0, c->ColorTexture2?c->ColorTexture2->IsUIntTexture:0);
        }else{
            tnsUniformUseTexture(cs,0,0,0,0); //tnsUnbindTexture(); 
        }

        if(cs->iMultiplyColor != -1){ tnsUniformUseMultiplyColor(cs, c->MultiplyColor); }
        if(cs->iUseHalftone != -1){
            tnsUniformUseHalftone(cs, c->UseHalftone);
            tnsUniformHalftoneSize(cs, MAIN.ViewportHalftoneSize);
        }

        if (c->UseIndex){
            glDrawElements(c->Mode, c->NumIndex, GL_UNSIGNED_INT, 0);
        }else{
            glDrawArrays(c->Mode, 0, c->NumVert);
        }
    }

    T->NextCommand=0;
    c = &T->DrawingCommand[0];

    memset(c, 0, sizeof(tnsCommand));

    c->ColorBegin = c->ColorEnd = T->NextColor = 0;
    c->NormalBegin = c->NormalEnd = T->NextNormal = 0;
    c->TexCoordBegin = c->TexCoordEnd = T->NextTexCoord = 0;
    c->VertBegin = c->VertEnd = T->NextVert = 0;
    c->IndexBegin = c->IndexEnd = T->NextIndex = 0;

    //must --why?
    //T->BindedShader = 0;
};

//============================================================================================[offscr]

const GLuint TNS_ATTACHMENT_ARRAY_NONE[] = {GL_NONE};
const GLuint TNS_ATTACHMENT_ARRAY[] = {GL_COLOR_ATTACHMENT0};
const GLuint TNS_ATTACHMENT_ARRAY_1[] = {GL_COLOR_ATTACHMENT1};
const GLuint TNS_ATTACHMENT_ARRAY_2[] = {GL_COLOR_ATTACHMENT2};
const GLuint TNS_ATTACHMENT_ARRAY_1_2[] = {GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
const GLuint TNS_ATTACHMENT_ARRAY_0_1_2[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
const GLenum TNS_WINDOW_DRAWBUFFER_ARRAY[] = {GL_BACK};

tnsOffscreen *tnsCreateOffscreenHandle(){
    if(!T->CurrentContext){ logPrintNew("tnsCreateOffscreenHandle() called without GL Context. Exiting"); exit(0); }
    tnsOffscreen *toff = CreateNew(tnsOffscreen); toff->FboContext=T->CurrentContext;
#ifdef LA_USE_GLES
    toff->FboSurface=T->CurrentSurface;
#endif
#ifdef _WIN32
    toff->FboDC=T->CurrentDC;
#endif
#ifdef __linux__
    toff->FboWindow=T->CurrentWindow;
#endif
    glGenFramebuffers(1, &toff->FboHandle);
    lstAppendItem(&T->Offscreens, toff);
    return toff;
}
void tnsAttach2DOffscreenBuffer(tnsOffscreen *target, GLuint attatchment, tnsTexture *use){
    if (!target || !use || target->FboHandle == -1 || use->GLTexHandle == -1) return;

    if (attatchment >= GL_COLOR_ATTACHMENT0 && attatchment <= GL_COLOR_ATTACHMENT15){
        //if (target->pColor[attatchment - GL_COLOR_ATTACHMENT0]) return;

        glBindFramebuffer(GL_FRAMEBUFFER, target->FboHandle);
        tnsBindTexture(use);
        glFramebufferTexture2D(GL_FRAMEBUFFER, attatchment, use->GLTexType, use->GLTexHandle, 0);
        target->pColor[attatchment - GL_COLOR_ATTACHMENT0] = use;
        tnsUnbindTexture();
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }elif (attatchment == GL_DEPTH_ATTACHMENT || attatchment==GL_STENCIL_ATTACHMENT){
        //if (target->pDepth) return;
        glBindFramebuffer(GL_FRAMEBUFFER, target->FboHandle);
        tnsBindTexture(use);
        glFramebufferTexture2D(GL_FRAMEBUFFER, attatchment, use->GLTexType, use->GLTexHandle, 0);
        //glBindRenderbufferEXT(GL_RENDERBUFFER, use->GLTexHandle);
        //glBindFramebuffer(GL_FRAMEBUFFER, target->FboHandle);
        //glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, use->GLTexHandle);
        target->pDepth = use;
        tnsUnbindTexture();
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        //glBindRenderbufferEXT(GL_RENDERBUFFER, 0);
        //glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
    GLenum result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    //if (result == GL_FRAMEBUFFER_COMPLETE) { printf("Framebuffer complete!\n"); }
    //else { printf("Framebuffer incomplete!\n"); }
}
void tnsDetach2DOffscreenBuffer(tnsOffscreen *target, GLuint which_attach_point){
    if (which_attach_point >= GL_COLOR_ATTACHMENT0 && which_attach_point <= GL_COLOR_ATTACHMENT15){
        if (target->pColor[which_attach_point - GL_COLOR_ATTACHMENT0] == 0) return;

        glBindFramebuffer(GL_FRAMEBUFFER, target->FboHandle);
        glFramebufferTexture2D(GL_FRAMEBUFFER, which_attach_point, GL_TEXTURE_2D, 0, 0);

        tnsDeleteTexture(target->pColor[which_attach_point - GL_COLOR_ATTACHMENT0]);

        target->pColor[which_attach_point - GL_COLOR_ATTACHMENT0] = 0;
    }elif (which_attach_point == GL_DEPTH_ATTACHMENT || which_attach_point==GL_STENCIL_ATTACHMENT){
        if (target->pDepth) return;

        glBindFramebuffer(GL_FRAMEBUFFER, target->FboHandle);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0);

        tnsDeleteTexture(target->pDepth);

        target->pDepth = 0;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    tnsUnbindTexture();
}

tnsOffscreen *tnsCreate2DOffscreen(int glInternalFormat, int w, int h, int Multisample, int WithDepth, int WithStencil){
    tnsOffscreen *toff = tnsCreateOffscreenHandle();
    tnsTexture *color; tnsTexture *depth;

    if(glInternalFormat){
        color = tnsCreate2DTexture(glInternalFormat, w, h, Multisample);
        tnsAttach2DOffscreenBuffer(toff, GL_COLOR_ATTACHMENT0, color);
    }
    if(WithDepth || WithStencil){
        int format=GL_DEPTH_COMPONENT;
        if(WithStencil){ format=GL_DEPTH_STENCIL; }
        depth = tnsCreate2DTexture(format, w, h, Multisample);
        tnsAttach2DOffscreenBuffer(toff, GL_DEPTH_ATTACHMENT, depth);
        if(WithStencil){ tnsAttach2DOffscreenBuffer(toff, GL_STENCIL_ATTACHMENT, depth); }
    }

    return toff;
}
tnsOffscreen *tnsCreateDeferredOffscreen(int w, int h, int Multisample){
    tnsOffscreen *toff = tnsCreateOffscreenHandle();
    tnsTexture *color,*normal,*gpos; tnsTexture *depth;

    color  = tnsCreate2DTexture(GL_RGBA8, w, h, Multisample);  tnsAttach2DOffscreenBuffer(toff, GL_COLOR_ATTACHMENT0, color);
    normal = tnsCreate2DTexture(GL_RGB8, w, h, Multisample);   tnsAttach2DOffscreenBuffer(toff, GL_COLOR_ATTACHMENT1, normal);
    gpos   = tnsCreate2DTexture(GL_RGB32F, w, h, Multisample); tnsAttach2DOffscreenBuffer(toff, GL_COLOR_ATTACHMENT2, gpos);

    depth = tnsCreate2DTexture(GL_DEPTH_COMPONENT, w, h, Multisample); tnsAttach2DOffscreenBuffer(toff, GL_DEPTH_ATTACHMENT, depth);

    return toff;
}

void tnsRecreateFBO(tnsOffscreen *off){
    if(off->FboContext){
#ifdef __linux__
        SYSWINDOW sw=T->CurrentWindow; SYSGLCONTEXT sc=T->CurrentContext; void* gls=0;
#ifdef LA_USE_GLES
        gls=&T->CurrentSurface;
#endif
        tnsContextMakeFBOCurrent(off);
        glDeleteFramebuffers(1, &off->FboHandle);
        tnsContextMakeCurrent(sc,sw,gls);
        off->FboContext=sc; off->FboWindow=sw;
#endif
#ifdef _WIN32
        SYSTEMDC sd=T->CurrentDC; SYSGLCONTEXT sc=T->CurrentContext;
        tnsContextMakeFBOCurrent(off);
        glDeleteFramebuffers(1, &off->FboHandle);
        tnsContextMakeCurrent(sc,sd,0);
        off->FboContext=sc; off->FboDC=sd;
#endif
    }
    glGenFramebuffers(1, &off->FboHandle);
    if(off->pColor[0]) tnsAttach2DOffscreenBuffer(off, GL_COLOR_ATTACHMENT0, off->pColor[0]);
    if(off->pColor[1]) tnsAttach2DOffscreenBuffer(off, GL_COLOR_ATTACHMENT1, off->pColor[1]);
    if(off->pColor[2]) tnsAttach2DOffscreenBuffer(off, GL_COLOR_ATTACHMENT2, off->pColor[2]);
    if(off->pColor[3]) tnsAttach2DOffscreenBuffer(off, GL_COLOR_ATTACHMENT3, off->pColor[3]);
    if(off->pDepth){ tnsAttach2DOffscreenBuffer(off, GL_DEPTH_ATTACHMENT, off->pDepth);
        if(off->pDepth->GLTexBitsType==GL_DEPTH_STENCIL) tnsAttach2DOffscreenBuffer(off, GL_STENCIL_ATTACHMENT, off->pDepth);
    }
}
void tnsEnsureOffscreenContext(tnsOffscreen *off){
    if(off->FboContext!=T->CurrentContext){ tnsRecreateFBO(off); }
}
void tnsEnsureOffscreenStatus(tnsOffscreen *off, int w, int h){
    tnsTexture* t=off->pColor[0];
    tnsEnsureOffscreenContext(off);
    if(t->Width==w && t->Height==h) return;
    t->Width = w; t->Height = h; tnsConfigure2DTexture(t);
    if((t=off->pColor[1])){ t->Width = w; t->Height = h; tnsConfigure2DTexture(t); }
    if((t=off->pColor[2])){ t->Width = w; t->Height = h; tnsConfigure2DTexture(t); }
    if((t=off->pColor[3])){ t->Width = w; t->Height = h; tnsConfigure2DTexture(t); }
    if((t=off->pDepth)){ t->Width = w; t->Height = h; tnsConfigure2DTexture(t); }
}

void tnsDelete2DOffscreen(tnsOffscreen *o){
    if (!o) return;
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    tnsEnsureOffscreenContext(o);

    for(int i=0;i<16;i++){
        if(o->pColor[i]){
            tnsDetach2DOffscreenBuffer(o, GL_COLOR_ATTACHMENT0+i);
            tnsDeleteTexture(o->pColor[0]);
        }
    }
    
    if(o->pDepth){
        tnsDetach2DOffscreenBuffer(o, GL_DEPTH_ATTACHMENT);
        tnsDeleteTexture(o->pDepth);
    }

    glDeleteFramebuffers(1, &o->FboHandle);

    lstRemoveItem(&T->Offscreens, o);
    FreeMem(o);
}

void tnsDrawToOffscreen(tnsOffscreen *toff, int HowMany, GLuint *AttachmentArray){
    if (!toff) return;
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, toff->FboHandle); glClear(0);
    if(AttachmentArray==TNS_ATTACHMENT_ARRAY_NONE){glDrawBuffer(GL_NONE);}
    else glDrawBuffers((HowMany ? HowMany : 1), (AttachmentArray ? AttachmentArray : TNS_ATTACHMENT_ARRAY));
    glClear(0);
    T->IsOffscreen = 1;
    T->BindedShader = 0;
}
void tnsDrawToOffscreenOnlyBind(tnsOffscreen *toff){
    if (!toff){ glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); return; }
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, toff->FboHandle);
}
void tnsReadFromOffscreen(tnsOffscreen *toff){
    if (!toff){ glBindFramebuffer(GL_READ_FRAMEBUFFER, 0); return; }
    glBindFramebuffer(GL_READ_FRAMEBUFFER, toff->FboHandle);
}
void tnsDrawToScreen(){
    glBindFramebuffer(GL_FRAMEBUFFER, 0); glClear(0);
    //glDrawBuffer(GL_BACK); printf("%d\n", glGetError());
    T->IsOffscreen = 0;
    T->BindedShader = 0;
}

//===========================================================================[FONT]

tnsFontManager *FM;

void tnsSetuptnsFontManager(){
    FM = CreateNew(tnsFontManager);
    FM->BufferWidth = TNS_FONT_BUFFER_W_DEFAULT;
    FM->L = INT_MIN; FM->R = INT_MAX; FM->B = INT_MAX; FM->U = INT_MIN;
};
void tnsQuitFontManager(){
    tnsInvalidateFontCache();
    tnsFont*f=FM->UsingFont;
    for(int i=0;i<f->NumFaces;i++){
        FT_Done_Face(f->ftface[i]);
    }
    if(f->ftfacemono) FT_Done_Face(f->ftfacemono);
    FT_Done_FreeType(f->ftlib);
    free(f->characters);
    free(f->monocharacters);
    free(f);
    FreeMem(FM);
}

int next_p2(int a){
    int rval = 1;
    while (rval < a)
        rval <<= 1;
    return rval;
}
real tnsGetMonoFontAdvance(){
    return FM->UsingFont->MonoAdvance;
}
void tnsInvalidateFontCache(){
    tnsFont*f=FM->UsingFont;
    for(int i=0;i<TNS_UNICODE_COUNT;i++){ if(f->characters[i]){ free(f->characters[i]); f->characters[i]=0; } }
    for(int i=0;i<TNS_MONO_COUNT;i++){ if(f->monocharacters[i]){ free(f->monocharacters[i]); f->monocharacters[i]=0; } }
    f->CurrentX=f->CurrentY=0;
    f->height = LA_RH*(MAIN.FontSize/2.0f+0.5f);
    int GenHeight=LA_RH*MAIN.FontSize;
    for(int i=0;i<f->NumFaces;i++){
        FT_Set_Char_Size(f->ftface[i], 0, GenHeight << 6, 96, 96);
        FT_Glyph glyph; FT_Fixed half_adv=0;
        if(!FT_Get_Advance(f->ftface[i],'a',FT_LOAD_TARGET_NORMAL | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP, &half_adv)){
            if (FT_Load_Char(f->ftface[i], 'a', FT_LOAD_TARGET_NORMAL | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP)||
                FT_Get_Glyph(f->ftface[i]->glyph, &glyph)){ SEND_PANIC_ERROR("Monospace font doesn't contain character 'a'"); }
            f->MonoAdvance=(real)f->ftface[i]->glyph->advance.x/64.0;
            if(glyph) FT_Done_Glyph(glyph);
        }
    }
    if(f->ftfacemono){
        FT_Set_Char_Size(f->ftfacemono, 0, (GenHeight << 6)*f->MonoScale, 96, 96); FT_Glyph glyph;
        if (FT_Load_Char(f->ftfacemono, 'a', FT_LOAD_TARGET_NORMAL | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP)||
            FT_Get_Glyph(f->ftfacemono->glyph, &glyph)){ SEND_PANIC_ERROR("Monospace font doesn't contain character 'a'"); }
        f->MonoAdvance=(real)f->ftfacemono->glyph->advance.x/64.0;
        if(glyph) FT_Done_Glyph(glyph);
    }
#ifndef LA_USE_GLES
    if(glClearTexImage) glClearTexImage(f->TexBuffer.GLTexHandle, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
#endif
}
tnsFontSingleCharacter *tfntFetchCharTextureIDW(uint32_t ch, int UseMono);

#ifndef LAGUI_FONT_CUSTOM_PATH
#define LAGUI_FONT_CUSTOM_PATH "/usr/share/fonts/opentype/noto/"
#endif

const char* TNS_FONT_CUSTOM=LAGUI_FONT_CUSTOM_PATH;

const char* TNS_FONT_LOAD_OPTIONS[9]={"","fonts/","lagui/fonts/","../","../fonts/","../lagui/fonts/","../../","../../fonts/","../../lagui/fonts/"};
const char* TNS_FONT_LOAD_OPTIONS_FROM_HOME[2]={".local/share/fonts/lagui/",".local/share/fonts/"};

int tnsLoadSystemFontMono(char* from, char* mono){
    char buf[8192]; if(!FM->UsingFont || !mono || !mono[0]) return 0;
    tnsFont *f=FM->UsingFont;
    int GenHeight=LA_RH*MAIN.FontSize;

    FT_Fixed full_adv=0,half_adv=0;
    for(int i=0;i<f->NumFaces;i++){
        if(!FT_Get_Advance(f->ftface[i],U'我',FT_LOAD_TARGET_NORMAL | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP, &full_adv)) break;
    }
    for(int i=-2;i<11;i++){
        char* option;
        if(i==-2){ sprintf(buf,"%s%s%s",TNS_FONT_CUSTOM,TNS_FONT_CUSTOM[strlen(TNS_FONT_CUSTOM)-1]=='/'?"":"/",mono); }
        elif(i<9){ option=(i==-1)?from:TNS_FONT_LOAD_OPTIONS[i]; sprintf(buf,"%s%s%s",SSTR(MAIN.WorkingDirectory),option,mono); }
        else{ option=TNS_FONT_LOAD_OPTIONS_FROM_HOME[i-9]; sprintf(buf,"%s/%s%s",getenv("HOME"),option,mono); }
        FILE* fontfile=fopen(buf,"rb"); if(!fontfile) continue;
        fseek(fontfile,0,SEEK_END); int filesize=ftell(fontfile);
        void*fontdata=CreateNewBuffer(char,filesize);
        fseek(fontfile,0,SEEK_SET); fread(fontdata,filesize,1,fontfile);
        FT_Face face; FT_Long i,num_faces; FT_Open_Args args; args.flags=FT_OPEN_MEMORY;
        args.memory_base=fontdata; args.memory_size=filesize;
        if(FT_Open_Face(f->ftlib, &args, 0, &f->ftfacemono)) continue;
        FT_Select_Charmap(f->ftfacemono, FT_ENCODING_UNICODE);
        FT_Set_Char_Size(f->ftfacemono, 0, GenHeight << 6, 96, 96);
        logPrint("Loaded monospace font: %s\n",buf);
        if(!FT_Get_Advance(f->ftfacemono,'a',FT_LOAD_TARGET_NORMAL | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP, &half_adv)){
            f->MonoScale=(real)full_adv/(half_adv*2);
            FT_Set_Char_Size(f->ftfacemono, 0, (GenHeight << 6)*f->MonoScale, 96, 96);
            logPrintNew("Monospace font scale: %.2f\n",f->MonoScale); FT_Glyph glyph;
            if (FT_Load_Char(f->ftfacemono, 'a', FT_LOAD_TARGET_NORMAL | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP)||
                FT_Get_Glyph(f->ftfacemono->glyph, &glyph)){ SEND_PANIC_ERROR("Monospace font doesn't contain character 'a'"); }
            f->MonoAdvance=(real)f->ftfacemono->glyph->advance.x/64.0;
            if(glyph) FT_Done_Glyph(glyph);
        }
        return 1;
    }
    return 0;
};
int tnsLoadSystemFont(char* from, char* name){
    char buf[8192];

    tnsFont *f=FM->UsingFont; if(!f){
        f = CreateNew(tnsFont);
        f->characters=calloc(1, sizeof(tnsFontSingleCharacter*)*TNS_UNICODE_COUNT);
        f->monocharacters=calloc(1, sizeof(tnsFontSingleCharacter*)*TNS_MONO_COUNT);
        f->height = LA_RH*(MAIN.FontSize/2.0f+0.5f);
        tnsInit2DTexture(&f->TexBuffer, GL_R8, FM->BufferWidth, FM->BufferWidth, 0);
        lstAppendItem(&FM->Fonts, f); FM->UsingFont=f;
        if (FT_Init_FreeType(&f->ftlib)) SEND_PANIC_ERROR("Freetype Init Failed!");
        logPrintNew("Loading fonts...\n");
    }

    int GenHeight=LA_RH*MAIN.FontSize;
    for(int i=-2;i<11;i++){
        char* option;
        if(i==-2){ sprintf(buf,"%s%s%s",TNS_FONT_CUSTOM,TNS_FONT_CUSTOM[strlen(TNS_FONT_CUSTOM)-1]=='/'?"":"/",name); }
        elif(i<9){ option=(i==-1)?from:TNS_FONT_LOAD_OPTIONS[i]; sprintf(buf,"%s%s%s",SSTR(MAIN.WorkingDirectory),option,name); }
        else{ option=TNS_FONT_LOAD_OPTIONS_FROM_HOME[i-9]; sprintf(buf,"%s/%s%s",getenv("HOME"),option,name); }
        FILE* fontfile=fopen(buf,"rb"); if(!fontfile) continue;
        fseek(fontfile,0,SEEK_END); int filesize=ftell(fontfile);
        void*fontdata=CreateNewBuffer(char,filesize);
        fseek(fontfile,0,SEEK_SET); fread(fontdata,filesize,1,fontfile);
        FT_Face face; FT_Long i,num_faces; FT_Open_Args args; args.flags=FT_OPEN_MEMORY;
        args.memory_base=fontdata; args.memory_size=filesize;
        if(FT_Open_Face(f->ftlib, &args, -1, &face )) continue;
        num_faces = face->num_faces; FT_Done_Face(face); int found=0;
        for(int fa=0;fa<num_faces;fa++){
            if(FT_Open_Face(f->ftlib,&args,fa,&face)){ continue; }
            if(strstr(face->family_name,"SC")){ found=1; break; }
            FT_Done_Face(face);
        }
        if(found){ f->ftface[f->NumFaces]=face; }else{ if (FT_Open_Face(f->ftlib, &args, 0, &f->ftface[f->NumFaces])) continue; }
        FT_Select_Charmap(f->ftface[f->NumFaces], FT_ENCODING_UNICODE);
        FT_Set_Char_Size(f->ftface[f->NumFaces], 0, GenHeight << 6, 96, 96);
        f->NumFaces++; FT_Glyph glyph; FT_Fixed half_adv=0;
        if(!f->MonoAdvance && !FT_Get_Advance(f->ftface[f->NumFaces],'a',FT_LOAD_TARGET_NORMAL | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP, &half_adv)){
            FT_Set_Char_Size(f->ftface[f->NumFaces], 0, (GenHeight << 6)*f->MonoScale, 96, 96);
            if (FT_Load_Char(f->ftface[f->NumFaces], 'a', FT_LOAD_TARGET_NORMAL | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP)||
                FT_Get_Glyph(f->ftface[f->NumFaces]->glyph, &glyph)){ SEND_PANIC_ERROR("Monospace font doesn't contain character 'a'"); }
            f->MonoAdvance=(real)f->ftface[f->NumFaces]->glyph->advance.x/64.0;
            if(glyph) FT_Done_Glyph(glyph);
        }
        logPrint("Loaded font: %s\n",buf);
        return 1;
    }

    return 0;
};

int tfntBufferWidthEnough(int total_width, int current_width, int this_width){
    return (current_width + this_width +1 < total_width);
}

void tfntResizeFontTexture(tnsFont* f, int size){
    FM->BufferWidth=size;
    f->TexBuffer.Width=f->TexBuffer.Height=FM->BufferWidth;
    tnsConfigure2DTexture(&f->TexBuffer);
}

void tfntApplyCharacterBufferOffset(tnsFont *f, tnsFontSingleCharacter *fsc){
    if (!tfntBufferWidthEnough(FM->BufferWidth, f->CurrentX, fsc->width)){
        f->CurrentY += (f->height+1);
        f->CurrentX = 0;
    }
    fsc->bufferx = f->CurrentX;
    fsc->buffery = f->CurrentY;
    f->CurrentX += (fsc->width+1);
}

tnsFontSingleCharacter *tfntFetchCharTextureIDW(uint32_t ch, int UseMono){
    GLuint revel = 0;
    tnsFont *f = FM->UsingFont;
    tnsFontSingleCharacter **use_fsc=UseMono?f->monocharacters:f->characters, *fsc = 0;
    FT_Glyph glyph = 0;
    FT_BitmapGlyph bitmap_glyph;
    FT_Bitmap bm;
    FT_Face face;
    int w, h, i, j;
    GLubyte *buf = 0;

    if (!f) return 0;

    if(ch>TNS_MONO_COUNT){ UseMono=0; use_fsc=f->characters; }
    if(ch>TNS_UNICODE_COUNT){ return 0; }

    fsc = use_fsc[ch];
    if(!fsc){ fsc=use_fsc[ch]=calloc(1,sizeof(tnsFontSingleCharacter)); }

    if (revel = fsc->Generated) return fsc;

    if(UseMono && f->ftfacemono){
        face = f->ftfacemono;
        if(!FT_Get_Char_Index(face,ch)){return 0;}
        if (FT_Load_Char(face, ch, FT_LOAD_TARGET_NORMAL | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP)){ return 0; }
        if (FT_Get_Glyph(face->glyph, &glyph)){ return 0; }
    }else{
        for(int i=0;i<f->NumFaces;i++){
            if(!f->ftface[i]){continue;}
            face = f->ftface[i];
            if(!FT_Get_Char_Index(face,ch)){continue;}
            if (FT_Load_Char(face, ch, FT_LOAD_TARGET_NORMAL | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP)){ continue; }
            if (FT_Get_Glyph(face->glyph, &glyph)){ continue; }
            break;
        }
        if(!glyph){ return 0; }
    }

    FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);

    FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_NORMAL, 0, 1);
    bitmap_glyph = glyph;
    bm = bitmap_glyph->bitmap;

    w = bm.width;
    h = bm.rows;
    fsc->width = w;
    fsc->height = h;
    fsc->advx = (real)face->glyph->advance.x / 64.0f;
    fsc->advy = face->size->metrics.y_ppem;
    fsc->deltax = bitmap_glyph->left;
    fsc->deltay = bitmap_glyph->top - h;

    tfntApplyCharacterBufferOffset(f, fsc);

    if(fsc->buffery+h>=FM->BufferWidth){
        tfntResizeFontTexture(f,FM->BufferWidth*2); tnsInvalidateFontCache();
        FreeMem(buf); FT_Done_Glyph(glyph);
        return tfntFetchCharTextureIDW(ch,UseMono);
    }


    tnsBindTexture(&f->TexBuffer);
    //glBindTexture(GL_TEXTURE_2D, f->TexBuffer.GLTexHandle);

    buf = CreateNewBuffer(GLubyte, w * h);

    for (j = 0; j < h; j++){
        for (i = 0; i < w; i++){
            char _vl = bm.buffer[i + w * j];
            buf[i + (h - j - 1) * w] = _vl;
        }
    }

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexSubImage2D(GL_TEXTURE_2D, 0, fsc->bufferx, fsc->buffery, w, h, GL_RED, GL_UNSIGNED_BYTE, buf);

    FreeMem(buf);
    FT_Done_Glyph(glyph);

    fsc->Generated = 1;
    return fsc;
}
tnsFontSingleCharacter *tfntFetchCharacterW(uint32_t ch, int UseMono){
    tnsFontSingleCharacter* fsc = tfntFetchCharTextureIDW(ch, UseMono);
    if(fsc) return fsc;
    return tfntFetchCharTextureIDW('?', UseMono);
}

int CMP_NAME_IsThisFont(tnsFont *enumed, char *name){
    return (!strcmp(enumed->fontName, name));
};

void tnsPushStringClip(int L, int R, int U, int B){
    tnsStringClip *sc=memAcquire(sizeof(tnsStringClip)); lstPushItem(&FM->StringClips, sc);
    sc->L=FM->L; sc->R=FM->R; sc->U=FM->U; sc->B=FM->B;
    FM->L=L; FM->R=R; FM->U=U; FM->B=B;
}
void tnsPopStringClip(){
    tnsStringClip *sc=lstPopItem(&FM->StringClips);
    if(!sc){ FM->L = INT_MIN; FM->R = INT_MAX; FM->B = INT_MAX; FM->U = INT_MIN; }
    FM->L=sc->L; FM->R=sc->R; FM->U=sc->U; FM->B=sc->B;
};

int tnsStringGetDimension(char* content, uint32_t* contentU, int Count, int WLimit, int* Rows, int UseMono){
    if((!MAIN.CurrentWindow)||(!MAIN.CurrentWindow->win)||(!FM->UsingFont)) return 0;
    real sx = 0; int sy = FM->UsingFont->height; real MA=FM->UsingFont->MonoAdvance;
    int i, rows=1, advance=1;
    int C = 0;
    int len = contentU?strlenU(contentU):(content?strlen(content):0);
    char hb;
    int RestoreI;
    int MaxSX=0;

    if(!WLimit){WLimit=INT_MAX;}

    int UC=1;
    for (i = 0; i < len && UC; i+=advance){
        UC = contentU?contentU[i]:laToUnicode(&content[i], &advance);
        tnsFontSingleCharacter *fsc;
        if (UC == U'\n'){
            if(sx>MaxSX) MaxSX=sx;
            sx = 0; sy += LA_RH; rows++;
            continue;
        }else{
            fsc = tfntFetchCharacterW(UC, UseMono);
            if(!fsc){continue;};
            real dx=fsc->advx; if(UseMono){ dx/=MA; if(dx<1.01) dx=1; if(dx>1.01)dx=2; dx*=MA; }
            if(sx+dx > WLimit){ sx = 0; sy += LA_RH; rows++; }
            sx += dx;
            C += 1;
            if (Count && C == Count) break;
        }
    }
    if(sx==0 && rows>1){rows--;}
    if(sx>MaxSX) MaxSX=sx;
    if(Rows) (*Rows)=rows;
    return MaxSX+1;
}
int tnsStringGetWidth(char *content, int Count, int UseMono){
    return tnsStringGetDimension(content, 0, Count, 0, 0, UseMono);
}
int tnsStringGetWidthU(uint32_t *contentU, int Count, int UseMono){
    return tnsStringGetDimension(0, contentU, Count, 0, 0, UseMono);
}

int tnsDrawLCD7_ProgressSystem(real x, real y, real Percent){
    real hgap=LA_RH/15; real vgap=LA_RH/5;
    real MA=FM->UsingFont->MonoAdvance;
    real w=MA-hgap*2, h=LA_RH-vgap*2; y+=vgap; x+=hgap-MA; real shear=h/12;
    TNS_CLAMP(Percent,0,1);
    char str[10]; sprintf(str,"%d",(int)(Percent*100)); int len=strlen(str);
    for(int i=len-1;i>=0;i--){
        int uc=str[i];
        if(uc>='0' && uc<='9'){ uc-='0'; } 
        else { continue; }
        for(int i=0;i<7;i++){
            if(TNS_LCD_MAP_7[uc][i]){
                real* seg=TNS_LCD_SEG_7[i];
                real s1=tnsInterpolate(shear,-shear,seg[1]); real s2=tnsInterpolate(shear,-shear,seg[3]);
#ifdef LA_LINUX
                XDrawLine(MAIN.dpy,MAIN.Progress.w,MAIN.Progress.gc,
                    tnsInterpolate(x+s1,x+w+s1,seg[0]),tnsInterpolate(y,y+h,seg[1]),
                    tnsInterpolate(x+s2,x+w+s2,seg[2]),tnsInterpolate(y,y+h,seg[3]));
#endif
#ifdef _WIN32
                MoveToEx(MAIN.Progress.hdc,tnsInterpolate(x+s1,x+w+s1,seg[0]),tnsInterpolate(y,y+h,seg[1]),0);
                LineTo(MAIN.Progress.hdc,tnsInterpolate(x+s2,x+w+s2,seg[2]),tnsInterpolate(y,y+h,seg[3]));
#endif
            }
        }
        x-=MA;
    }
    return 1;
    return 0;
}
int tnsMakeLCD7(real x, real y, real w, real h, real shear, int ch){
    int uc=ch;
    if(ch>='0' && ch<='9'){ uc-='0'; } 
    else {return 0;}
    for(int i=0;i<7;i++){
        if(TNS_LCD_MAP_7[uc][i]){
            real* seg=TNS_LCD_SEG_7[i];
            real s1=tnsInterpolate(shear,-shear,seg[1]); real s2=tnsInterpolate(shear,-shear,seg[3]);
            tnsVertex2d(tnsInterpolate(x+s1,x+w+s1,seg[0]),tnsInterpolate(y,y+h,seg[1]));
            tnsVertex2d(tnsInterpolate(x+s2,x+w+s2,seg[2]),tnsInterpolate(y,y+h,seg[3]));
        }
    }
    return 1;
}
int tnsMakeLCD16(real x, real y, real w, real h, real shear, int ch){
    int uc=ch;
    if(ch>='0' && ch<='9'){ uc-='0'; }
    elif(ch>='A' && ch<='Z'){ uc-='A'; uc+=10; }
    elif(ch>='a' && ch<='z'){ uc-='a'; uc+=(10+26); }
    else {return 0;}
    for(int i=0;i<16;i++){
        if(TNS_LCD_MAP_16[uc][i]){
            real* seg=TNS_LCD_SEG_16[i];
            real s1=tnsInterpolate(shear,-shear,seg[1]); real s2=tnsInterpolate(shear,-shear,seg[3]);
            tnsVertex2d(tnsInterpolate(x+s1,x+w+s1,seg[0]),tnsInterpolate(y,y+h,seg[1]));
            tnsVertex2d(tnsInterpolate(x+s2,x+w+s2,seg[2]),tnsInterpolate(y,y+h,seg[3]));
        }
    }
    return 1;
}
void tnsDrawStringLCD(char *content, uint32_t* contentU, real Color[4], int L, int R, int T, uint64_t Flags, real Scale){
    if(Scale<1e-5) return;
    real MA=FM->UsingFont->MonoAdvance;
    real sx = L; int sy = T; real dx=MA; real hgap=LA_RH/15; real vgap=LA_RH/5;
    int i,advance=1;
    int len = contentU?strlenU(contentU):strlen(content);
    int RevY=(Flags&LA_TEXT_REVERT_Y); int TH=LA_RH-vgap*2; real shear=TH/12;
    if(RevY){ TH*=-1; vgap*=-1; } TH*=Scale;dx*=Scale;hgap*=Scale;vgap*=Scale;shear*=Scale;
    int OneLine=(Flags&LA_TEXT_ONE_LINE);
    int Use16=(Flags&LA_TEXT_LCD_16);

    tnsFlush();
    tnsUseNoTexture();

    int any=0, UC=1; int BreakNow=0;
    for (i = 0; i < len && UC; i+=advance){
        UC = contentU?contentU[i]:laToUnicode(&content[i], &advance);

        if (UC == U'\n'){ if(!OneLine){sx = L; sy += LA_RH; continue;}else{ UC=' '; } }

        if (sx + dx > R+1){
            if(Flags&LA_TEXT_LINE_WRAP){ sx=L; sy+=LA_RH; }else{ break; }
        }
        
        if(Use16){
            if(tnsMakeLCD16(sx+hgap, sy+vgap, dx-hgap*2, TH, shear, UC)) any=1;
        }else{
            if(tnsMakeLCD7(sx+hgap, sy+vgap, dx-hgap*2, TH, shear, UC)) any=1;
        }

        sx += dx;
        
        if(BreakNow){ break; }
    }
    if(any){ tnsColor4dv(Color); tnsPackAs(GL_LINES); }
}

void tns_RotateText(real* v, int detent,real x,real y){
    switch(detent){
        case 0: default: return;
        case 1: for(int i=0;i<6;i++){ v[i*2]-=x;v[i*2+1]-=y;  LA_SWAP(real,v[i*2],v[i*2+1]); v[i*2+1]*=-1; v[i*2]+=x;v[i*2+1]+=y; } return;
        case 2: for(int i=0;i<6;i++){ v[i*2]-=x;v[i*2+1]-=y;  v[i*2]*=-1;v[i*2+1]*=-1; v[i*2]+=x;v[i*2+1]+=y; } return;
        case 3: for(int i=0;i<6;i++){ v[i*2]-=x;v[i*2+1]-=y;  LA_SWAP(real,v[i*2],v[i*2+1]); v[i*2]*=-1; v[i*2]+=x;v[i*2+1]+=y; } return;
    }
}
int tns_ClipCharacterT2D(real* v, real* t){
    real l=v[0]; if(l>FM->R) return 0; real r=v[4]; if(r<FM->L) return 0;
    real u=v[1]; if(u>FM->B) return 0; real b=v[3]; if(b<FM->U) return 0;
    real tl=t[0],tr=t[4],tu=t[1],tb=t[3];
    if(l<FM->L){ real p=((real)FM->L-l)/(r-l); t[0]+=p*(tr-tl); t[6]=t[2]=t[0]; v[6]=v[2]=v[0]=FM->L; }
    if(r>FM->R){ real p=(r-(real)FM->R)/(r-l); t[4]-=p*(tr-tl); t[8]=t[10]=t[4]; v[8]=v[10]=v[4]=FM->R; }
    if(u<FM->U){ real p=((real)FM->U-u)/(b-u); t[1]+=p*(tb-tu); t[5]=t[9]=t[1]; v[5]=v[9]=v[1]=FM->U; }
    if(b>FM->B){ real p=(b-(real)FM->B)/(b-u); t[3]-=p*(tb-tu); t[7]=t[11]=t[3]; v[7]=v[11]=v[3]=FM->B; }
    return 1;
}
void tnsDrawStringM(char *content, uint32_t* contentU, real Color[4], int L, int R, int T, uint64_t Flags){
    if(Flags&LA_TEXT_SHADOW){
        int offset = FM->UsingFont->height*MAIN.UiScale/16.0f;
        tnsDrawStringM(content, contentU, laAccentColor(LA_BT_SHADOW|LA_BT_TEXT), L+offset, R+offset, T+offset, Flags&(~LA_TEXT_SHADOW));
    }
    if(!FM->UsingFont){return;} if(Flags&(LA_TEXT_LCD_16|LA_TEXT_LCD_7)){
        tnsDrawStringLCD(content,contentU,Color,L,R,T,Flags,1.0); return;
    }
    unsigned int fh=FM->UsingFont->height;
    real sx = L; int sy = (LA_RH!=LA_RH0)?(((((real)fh/LA_RH0-0.5)/MAIN.UiScale)+0.5)*LA_RH + T):(fh+T);
    real MA=FM->UsingFont->MonoAdvance;
    int i,advance=1;
    int len = contentU?strlenU(contentU):(content?strlen(content):0); if(!len) return;
    real xo, yo, xl, yl;
    real TexCoord[12];
    real VertArr[12];
    int rx=L+LA_RH0/2,ry=T+LA_RH0/2;
    int detent = (Flags&LA_TEXT_ROTATE_N)?1:((Flags&LA_TEXT_ROTATE_W)?2:((Flags&LA_TEXT_ROTATE_S)?3:0));
    int total_width = 0;
    tnsFont *f = FM->UsingFont;
    int FscHeight, RestoreI;
    int RevY=(Flags&LA_TEXT_REVERT_Y);
    int UseMono=(Flags&LA_TEXT_MONO);
    int OneLine=(Flags&LA_TEXT_ONE_LINE);

    if(Color) tnsColor4dv(Color);

    int any=0, UC=1; int BreakNow=0;
    for (i = 0; i < len && UC; i+=advance){
        UC = contentU?contentU[i]:laToUnicode(&content[i], &advance);
        tnsFontSingleCharacter *fsc;
        real cx, cy;

        if (UC == U'\n'){ if(!OneLine){sx = L; sy += LA_RH; continue;}else{ UC=' '; } }

        fsc = tfntFetchCharacterW(UC, Flags&LA_TEXT_MONO); if(!fsc){continue;}

        real dx=fsc->advx; if(UseMono){ dx/=MA; if(dx<1.01) dx=1; if(dx>1.01)dx=2; dx*=MA; }
        if (sx + dx > R+1){
            if(Flags&LA_TEXT_LINE_WRAP){
                sx=L; sy+=LA_RH;
            }else{
                if(Flags&LA_TEXT_OVERFLOW_ARROW){ fsc = tfntFetchCharacterW(U'▷', 0); if(!fsc){continue;} sx=R-fsc->advx; BreakNow=1; }
                else break;
            }
        }

        cx = sx + fsc->deltax;
        cy = sy - fsc->deltay;
        xo = (real)fsc->bufferx / (real)FM->BufferWidth;
        yo = (real)fsc->buffery / (real)FM->BufferWidth;
        xl = (real)(fsc->bufferx + fsc->width) / (real)FM->BufferWidth;
        yl = (real)(fsc->buffery + fsc->height) / (real)FM->BufferWidth;

        tnsMakeQuadT2d(TexCoord, xo, yl, xo, yo, xl, yl, xl, yo);
        if (RevY) tnsMakeQuadT2d(VertArr,
                          cx, 2 * T - cy + fsc->height,
                          cx, 2 * T - cy,
                          cx + fsc->width, 2 * T - cy + fsc->height,
                          cx + fsc->width, 2 * T - cy);
        else tnsMakeQuadT2d(VertArr,
                          cx, +cy - fsc->height,
                          cx, +cy,
                          cx + fsc->width, +cy - fsc->height,
                          cx + fsc->width, +cy);
        
        if(tns_ClipCharacterT2D(VertArr,TexCoord)){
            tns_RotateText(VertArr,detent,rx,ry);
            tnsUseMaskTexture(&f->TexBuffer);
            tnsVertexArray2d(VertArr, 6);
            tnsTexCoordArray2d(TexCoord, 6);
            any=1;
        }

        sx += dx;
        if(BreakNow){ break; }
    }
    if(any) tnsPackAs(GL_TRIANGLES);
}
void tnsDrawStringAutoM(char *content, uint32_t* contentU, real Color[4], int L, int R, int T, uint64_t Flags){
    int LL;
    uint64_t al = Flags&LA_TEXT_ALIGN;
    
    if (!content && !contentU) return;
    switch (al){
    case LA_TEXT_ALIGN_AUTO:
    case LA_TEXT_ALIGN_CENTER: LL = L+(R-L-tnsStringGetDimension(content, contentU, 0, 0, 0, Flags&LA_TEXT_MONO)) / 2; if(LL<L) LL=L; break;
    default:
    case LA_TEXT_ALIGN_LEFT: LL = L; break;
    case LA_TEXT_ALIGN_RIGHT: LL = R-(tnsStringGetDimension(content, contentU, 0, 0, 0, Flags&LA_TEXT_MONO)); if(LL<L) LL=L; break;
    }
    tnsDrawStringM(content, contentU, Color, LL, R, T, Flags);
}
void tnsDrawStringAuto(char *content, real Color[4], int L, int R, int T, uint64_t Flags){
    tnsDrawStringAutoM(content,0,Color,L,R,T,Flags);
}
void tnsDrawStringWithPriority(char *Label, char *MajorContent, real Color[4], int TextAlign, int L, int R, int T, uint64_t Flags){
    int W = R - L;
    int Str1W = tnsStringGetWidth(Label, 0, Flags&LA_TEXT_MONO);
    int Str2W = tnsStringGetWidth(MajorContent, 0, Flags&LA_TEXT_MONO);

    int rx=L+LA_RH0/2,ry=T+LA_RH0/2;
    int detent = (Flags&LA_TEXT_ROTATE_N)?1:((Flags&LA_TEXT_ROTATE_W)?2:((Flags&LA_TEXT_ROTATE_S)?3:0));
    int offx,offy;
    int remain = TNS_MAX2(0,W-Str2W);
    switch(detent){
        case 0: offx=0;offy=0; break;
        case 1: offx=remain; offy=remain; break;
        case 2: offx=remain-LA_RH*2; offy=0; break;
        case 3: offx=remain; offy=-remain; break;
    }
    
    if (Str1W + Str2W > W){
        if (Str2W < W){
            tnsDrawStringM(MajorContent, 0, Color, R - Str2W-offx, R-offx, T-offy, Flags);
            tnsDrawStringM(Label, 0, Color, L, R - Str2W, T, Flags);
        }else
            tnsDrawStringM(MajorContent, 0, Color, L-offx, R-offx, T-offy, Flags);
    }else{
        int LL = L, ML;
        switch (Flags&LA_TEXT_ALIGN){
        case LA_TEXT_ALIGN_CENTER:
            ML = L + Str1W + (W - (Str1W + Str2W)) / 2;
            break;
        case LA_TEXT_ALIGN_LEFT:
            ML = L + Str1W;
            break;
        default:
        case LA_TEXT_ALIGN_AUTO:
        case LA_TEXT_ALIGN_RIGHT:
            ML = R - Str2W;
            break;
        }

        tnsDrawStringM(Label, 0, Color, LL, R, T, Flags);
        tnsDrawStringM(MajorContent, 0, Color, ML-offx, R-offx, T-offy, Flags);
    }
}
void tnsDrawIcon(uint32_t ID, real Color[4], int L,int R, int T, uint64_t Flags){
    char buf[5]={0}; char* next; laToUTF8(ID,buf,&next);
    tnsDrawStringAuto(buf, Color, L,R,T,Flags);
}

//=====================================================================================[Object]

tnsObject *tnsFindObject(char *Name, tnsObject *FromObj){
    tnsObject *io; tnsObject *ro;
    laListHandle* l=FromObj?(&FromObj->ChildObjects):(&T->World->AllObjects);
    for (laListItemPointer*lip=l->pFirst;lip;lip=lip->pNext){
        io=((l==&T->World->AllObjects)?lip:lip->p); if (strSame(io->Name->Ptr, Name)){ return io; }
        if (ro = tnsFindObject(Name, io)) return ro;
    }
    return 0;
}

void tnsExtractDeltaTransformValue(tnsObject *o){
    if (!o) return;
    tnsExtractLocation44d(o->DeltaTransform, o->DLocation);
    tnsExtractXYZEuler44d(o->DeltaTransform, o->DRotation);
    tnsExtractScale44d(o->DeltaTransform, o->DScale);
}
void tnsExtractSelfTransformValue(tnsObject *o){
    if (!o) return;
    tnsExtractLocation44d(o->SelfTransform, o->Location);
    tnsExtractXYZEuler44d(o->SelfTransform, o->Rotation);
    tnsExtractScale44d(o->SelfTransform, o->Scale);
}
void tnsExtractGlobalTransformValue(tnsObject *o){
    if (!o) return;
    tnsExtractLocation44d(o->GlobalTransform, o->GLocation);
    tnsExtractXYZEuler44d(o->GlobalTransform, o->GRotation);
    tnsExtractScale44d(o->GlobalTransform, o->GScale);
}
void tnsCopyGlobalTransform(tnsObject *to, tnsObject *from){
    memcpy(to->GlobalTransform, from->GlobalTransform, sizeof(tnsMatrix44d));
    memcpy(to->GLocation, from->GLocation, sizeof(tnsVector3d));
    memcpy(to->GRotation, from->GRotation, sizeof(tnsVector3d));
    memcpy(to->GScale, from->GScale, sizeof(tnsVector3d));
    tnsGlobalTransformValueChanged(to);
}

void tnsSelfMatrixChanged(tnsObject* o, int ApplyToChild){
    tnsMatrix44d mix;
    if (!o->ParentObject){
        tnsMultiply44d(mix, o->SelfTransform, o->DeltaTransform);
        memcpy(o->GlobalTransform, mix, sizeof(tnsMatrix44d)); 
    }
    else{
        tnsMultiply44d(mix, o->ParentObject->GlobalTransform, o->SelfTransform);
        tnsMultiply44d(o->GlobalTransform, mix, o->DeltaTransform);
    }
    tnsExtractDeltaTransformValue(o);
    tnsExtractSelfTransformValue(o);
    tnsExtractGlobalTransformValue(o); tnsInvalidateEvaluation(o);
    if(ApplyToChild) for (laListItemPointer* li=o->ChildObjects.pFirst;li;li=li->pNext){ tnsSelfMatrixChanged(li->p,1); }
}
void tnsGlobalMatrixChanged(tnsObject* o, int ApplyToChild){
    tnsMatrix44d self,invd;
    if (!o->ParentObject){ memcpy(self, o->GlobalTransform, sizeof(tnsMatrix44d)); }
    else{ tnsMatrix44d inv;
        tnsInverse44d(inv, o->ParentObject->GlobalTransform);
        tnsMultiply44d(self, inv, o->GlobalTransform); }
    tnsInverse44d(invd, o->DeltaTransform);
    tnsMultiply44d(o->SelfTransform, self, invd); 
    tnsExtractSelfTransformValue(o);
    tnsExtractGlobalTransformValue(o); tnsInvalidateEvaluation(o);
    if(ApplyToChild) for (laListItemPointer* li=o->ChildObjects.pFirst;li;li=li->pNext){ tnsSelfMatrixChanged(li->p,1); }
}
void tnsGlobalMatrixChangedForDelta(tnsObject* o, int ApplyToChild){
    tnsMatrix44d invs, invp, tmp;
    if (!o->ParentObject){ tnsLoadIdentity44d(invp); }
    else{ tnsInverse44d(invp, o->ParentObject->GlobalTransform); }
    tnsInverse44d(invs, o->SelfTransform);
    tnsMultiply44d(tmp, invs, invp); 
    tnsMultiply44d(o->DeltaTransform, tmp, o->GlobalTransform); 
    tnsExtractDeltaTransformValue(o);
    //tnsExtractSelfTransformValue(o);
    tnsExtractGlobalTransformValue(o); tnsInvalidateEvaluation(o);
    if(ApplyToChild) for (laListItemPointer* li=o->ChildObjects.pFirst;li;li=li->pNext){ tnsSelfMatrixChanged(li->p,1); }
}
void tnsSelfTransformValueChanged(tnsObject* o){
    tnsMatrix44d Trans, Rot1, Rot2, Rot3, Scale, Res1, Res2;
    tnsLoadIdentity44d(o->SelfTransform);
    tnsMakeTranslationMatrix44d(Trans, LA_COLOR3(o->Location));
    if(o->Scale[0]<1e-5){ o->Scale[0]=1e-5; }
    if(o->Scale[1]<1e-5){ o->Scale[1]=1e-5; }
    if(o->Scale[2]<1e-5){ o->Scale[2]=1e-5; }
    tnsMakeScaleMatrix44d(Scale, LA_COLOR3(o->Scale));

    tnsMakeRotationXMatrix44d(Rot1, o->Rotation[0]);
    tnsMakeRotationYMatrix44d(Rot2, o->Rotation[1]);
    tnsMakeRotationZMatrix44d(Rot3, o->Rotation[2]);
    switch (o->RotationMode){
    case TNS_ROTATION_ZYX_EULER: tnsMultiply44d(Res1, Trans, Rot1); tnsMultiply44d(Res2, Res1, Rot2); tnsMultiply44d(Res1, Res2, Rot3); break;
    case TNS_ROTATION_XZY_EULER: tnsMultiply44d(Res1, Trans, Rot1); tnsMultiply44d(Res2, Res1, Rot3); tnsMultiply44d(Res1, Res2, Rot2); break;
    case TNS_ROTATION_YXZ_EULER: tnsMultiply44d(Res1, Trans, Rot2); tnsMultiply44d(Res2, Res1, Rot1); tnsMultiply44d(Res1, Res2, Rot3); break;
    case TNS_ROTATION_YZX_EULER: tnsMultiply44d(Res1, Trans, Rot2); tnsMultiply44d(Res2, Res1, Rot3); tnsMultiply44d(Res1, Res2, Rot1); break;
    case TNS_ROTATION_ZXY_EULER: tnsMultiply44d(Res1, Trans, Rot3); tnsMultiply44d(Res2, Res1, Rot1); tnsMultiply44d(Res1, Res2, Rot2); break;
    case TNS_ROTATION_XYZ_EULER: tnsMultiply44d(Res1, Trans, Rot3); tnsMultiply44d(Res2, Res1, Rot2); tnsMultiply44d(Res1, Res2, Rot1); break;
    }
    tnsMultiply44d(o->SelfTransform, Res1, Scale);
    tnsSelfMatrixChanged(o,1);
}
void tnsDeltaTransformValueChanged(tnsObject* o){
    tnsMatrix44d Trans, Rot1, Rot2, Rot3, Scale, Res1, Res2;
    tnsLoadIdentity44d(o->DeltaTransform);
    tnsMakeTranslationMatrix44d(Trans, LA_COLOR3(o->DLocation));
    tnsMakeScaleMatrix44d(Scale, LA_COLOR3(o->DScale));

    tnsMakeRotationXMatrix44d(Rot1, o->DRotation[0]);
    tnsMakeRotationYMatrix44d(Rot2, o->DRotation[1]);
    tnsMakeRotationZMatrix44d(Rot3, o->DRotation[2]);
    switch (o->RotationMode){
    case TNS_ROTATION_ZYX_EULER: tnsMultiply44d(Res1, Trans, Rot1); tnsMultiply44d(Res2, Res1, Rot2); tnsMultiply44d(Res1, Res2, Rot3); break;
    case TNS_ROTATION_XZY_EULER: tnsMultiply44d(Res1, Trans, Rot1); tnsMultiply44d(Res2, Res1, Rot3); tnsMultiply44d(Res1, Res2, Rot2); break;
    case TNS_ROTATION_YXZ_EULER: tnsMultiply44d(Res1, Trans, Rot2); tnsMultiply44d(Res2, Res1, Rot1); tnsMultiply44d(Res1, Res2, Rot3); break;
    case TNS_ROTATION_YZX_EULER: tnsMultiply44d(Res1, Trans, Rot2); tnsMultiply44d(Res2, Res1, Rot3); tnsMultiply44d(Res1, Res2, Rot1); break;
    case TNS_ROTATION_ZXY_EULER: tnsMultiply44d(Res1, Trans, Rot3); tnsMultiply44d(Res2, Res1, Rot1); tnsMultiply44d(Res1, Res2, Rot2); break;
    case TNS_ROTATION_XYZ_EULER: tnsMultiply44d(Res1, Trans, Rot3); tnsMultiply44d(Res2, Res1, Rot2); tnsMultiply44d(Res1, Res2, Rot1); break;
    }
    tnsMultiply44d(o->DeltaTransform, Res1, Scale);
    tnsSelfMatrixChanged(o,1);
}
void tnsGlobalTransformValueChanged(tnsObject* o){
    tnsMatrix44d Trans, Rot1, Rot2, Rot3, Scale, Res1, Res2;
    tnsLoadIdentity44d(o->GlobalTransform);
    tnsMakeTranslationMatrix44d(Trans, LA_COLOR3(o->GLocation));
    tnsMakeScaleMatrix44d(Scale, LA_COLOR3(o->GScale));

    tnsMakeRotationXMatrix44d(Rot1, o->GRotation[0]);
    tnsMakeRotationYMatrix44d(Rot2, o->GRotation[1]);
    tnsMakeRotationZMatrix44d(Rot3, o->GRotation[2]);
    switch (o->RotationMode){
    case TNS_ROTATION_ZYX_EULER: tnsMultiply44d(Res1, Trans, Rot1); tnsMultiply44d(Res2, Res1, Rot2); tnsMultiply44d(Res1, Res2, Rot3); break;
    case TNS_ROTATION_XZY_EULER: tnsMultiply44d(Res1, Trans, Rot1); tnsMultiply44d(Res2, Res1, Rot3); tnsMultiply44d(Res1, Res2, Rot2); break;
    case TNS_ROTATION_YXZ_EULER: tnsMultiply44d(Res1, Trans, Rot2); tnsMultiply44d(Res2, Res1, Rot1); tnsMultiply44d(Res1, Res2, Rot3); break;
    case TNS_ROTATION_YZX_EULER: tnsMultiply44d(Res1, Trans, Rot2); tnsMultiply44d(Res2, Res1, Rot3); tnsMultiply44d(Res1, Res2, Rot1); break;
    case TNS_ROTATION_ZXY_EULER: tnsMultiply44d(Res1, Trans, Rot3); tnsMultiply44d(Res2, Res1, Rot1); tnsMultiply44d(Res1, Res2, Rot2); break;
    case TNS_ROTATION_XYZ_EULER: tnsMultiply44d(Res1, Trans, Rot3); tnsMultiply44d(Res2, Res1, Rot2); tnsMultiply44d(Res1, Res2, Rot1); break;
    }
    tnsMultiply44d(o->GlobalTransform, Res1, Scale);
    tnsGlobalMatrixChanged(o,1);
}

void tnsSetCurrentRoot(tnsObject *o){
    if(o->Type!=TNS_OBJECT_ROOT) return;
    memAssignRef(T->World, &T->World->ActiveRoot, o);
}
void tnsInitObjectBase(tnsObject *o, tnsObject *under, char *Name, int Type,
                        real AtX, real AtY, real AtZ,
                        real RotX, real RotY, real RotZ, real RotW, u8bit RotationMode,
                        real Scale){
    if (!o) return;
    strSafeSet(&o->Name, Name);
    o->Type = Type;
    tnsVectorSet3(o->Location,AtX,AtY,AtZ);
    tnsVectorSet4(o->Rotation,RotX,RotY,RotZ,RotW); o->RotationMode = RotationMode;
    tnsVectorSet3(o->Scale,Scale,Scale,Scale);
    o->Show = 1;
    o->DrawMode = GL_LINE_LOOP;
    tnsLoadIdentity44d(o->DeltaTransform);
    tnsSelfTransformValueChanged(o);
    lstAppendItem(&T->World->AllObjects, o);
    if (under){ lstAppendPointer(&under->ChildObjects, o); int isroot=(under->Type==TNS_OBJECT_ROOT);
        tnsObject* root=isroot?under:under->InRoot; memAssignRef(o, &o->InRoot, root);
        if(!isroot){ o->ParentObject=under; }
    }
}
int tnsCheckParentable(tnsObject* child, tnsObject* parent){
    if(child==parent||parent->ParentObject==child)return 0; tnsObject* o=parent; while(o=o->ParentObject){ if(o==parent) return 0; } return 1;
}
void tnsParentObject(tnsObject *child, tnsObject *parent, int KeepTransform){
    if (!child || !parent || child->ParentObject==parent || parent->ParentObject==child || parent==child) return;

    if (child->ParentObject) lstRemovePointerLeave(&child->ParentObject->ChildObjects, child);
    else lstRemovePointerLeave(&child->InRoot->ChildObjects, child);

    lstAppendPointer(&parent->ChildObjects, child);
    memAssignRef(child, &child->ParentObject, parent);

    if(KeepTransform) tnsGlobalMatrixChanged(child, 1);
    else tnsSelfMatrixChanged(child, 1);
}
void tnsUnparentObject(tnsObject *o, int KeepTransform){
    if (!o || !o->ParentObject) return;
    lstRemovePointerLeave(&o->ParentObject->ChildObjects, o);
    lstAppendPointer(&o->InRoot->ChildObjects, o);
    memAssignRef(o, &o->ParentObject, 0);

    if(KeepTransform) tnsGlobalMatrixChanged(o,1);
    else tnsSelfMatrixChanged(o,1);
}
void tnsCopyObjectTransformationsLocal(tnsObject* to, tnsObject* from){
    tnsVectorCopy3d(from->Location, to->Location);
    tnsVectorCopy3d(from->Rotation, to->Rotation); to->RotationMode=from->RotationMode;
    tnsVectorCopy3d(from->Scale, to->Scale); 
    tnsSelfTransformValueChanged(to);
}
void tnsCopyObjectTransformationsGlobal(tnsObject* to, tnsObject* from){
    tnsVectorCopy3d(from->GLocation, to->GLocation);
    tnsVectorCopy3d(from->GRotation, to->GRotation); to->RotationMode=from->RotationMode;
    tnsVectorCopy3d(from->GScale, to->GScale); 
    tnsGlobalTransformValueChanged(to);
}
void tnsRotateObjectLocalValues(tnsObject *o, real x, real y, real z){
    o->Rotation[0] += x; o->Rotation[1] += y; o->Rotation[2] += z;
    tnsSelfTransformValueChanged(o);
}
void tnsSetObjectPositionLocal(tnsObject*o, real x, real y, real z){
    tnsVectorSet3(o->Location,x,y,z); tnsSelfTransformValueChanged(o);
}
void tnsResetObjectTransformations(tnsObject* o, int reset_loc,int reset_rot,int reset_sca){
    if(reset_loc){ tnsVectorSet3(o->Location,0,0,0); }
    if(reset_rot){ tnsVectorSet4(o->Rotation,0,0,0,0); }
    if(reset_sca){ tnsVectorSet3(o->Rotation,1,1,1);  }
    tnsSelfTransformValueChanged(o);
}
void tnsMoveObjectLocal(tnsObject *o, real x, real y, real z){
    tnsMatrix44d mat, res1, res2, res3;
    tnsMakeTranslationMatrix44d(res2, x, y, z);
    tnsMultiply44d(res1, o->SelfTransform, res2);
    memcpy(o->SelfTransform, res1, sizeof(tnsMatrix44d));
    tnsSelfMatrixChanged(o,1);
}
void tnsMoveObjectDelta(tnsObject *o, real x, real y, real z){
    tnsMatrix44d mat, res1, res2, res3;
    tnsMakeTranslationMatrix44d(res2, x, y, z);
    tnsMultiply44d(res1, o->DeltaTransform, res2);
    memcpy(o->DeltaTransform, res1, sizeof(tnsMatrix44d));
    tnsExtractDeltaTransformValue(o);
    tnsSelfMatrixChanged(o,1);
}
void tnsMoveObjectGlobal(tnsObject *o, real x, real y, real z){
    tnsMatrix44d mat, res1, res2, res3;
    tnsMakeTranslationMatrix44d(res2, x, y, z);
    tnsMultiply44d(res1, res2, o->GlobalTransform); //if reverse then local
    memcpy(o->GlobalTransform, res1, sizeof(tnsMatrix44d));
    tnsGlobalMatrixChanged(o,1);
}
void tnsMoveObjectGlobalForDelta(tnsObject *o, real x, real y, real z){
    tnsMatrix44d mat, res1, res2, res3;
    tnsMakeTranslationMatrix44d(res2, x, y, z);
    tnsMultiply44d(res1, res2, o->GlobalTransform); //if reverse then local
    memcpy(o->GlobalTransform, res1, sizeof(tnsMatrix44d));
    tnsGlobalMatrixChangedForDelta(o,1);
}
void tnsRotateObjectGlobal(tnsObject *o, real x, real y, real z, real angle, real cx,real cy,real cz){
    tnsMatrix44d tback,tfwd,rot,rot2,res1,res2;
    real xs, ys, zs;
    tnsMakeRotationMatrix44d(rot,angle,x,y,z);
    tnsMakeRotationMatrix44d(rot2,-angle,x,y,z);//why?

    tnsMakeTranslationMatrix44d(tfwd, o->GLocation[0],o->GLocation[1],o->GLocation[2]);
    tnsInverse44d(tback,tfwd);
    tnsMultiply44d(res1,o->GlobalTransform,tback);
    tnsMultiply44d(res2,rot,res1);
    tnsMultiply44d(o->GlobalTransform,res2,tfwd);

    tnsVector3d delta; delta[0]=o->GLocation[0]-cx; delta[1]=o->GLocation[1]-cy; delta[2]=o->GLocation[2]-cz;
    tnsVector3d c; c[0]=cx; c[1]=cy; c[2]=cz; tnsVector3d r;
    tnsApplyRotation43d(r,rot2,delta);
    tnsVectorPlus3d(&o->GlobalTransform[12],c,r);
    //memcpy(o->GlobalTransform, res1, sizeof(tnsMatrix44d));
    tnsGlobalMatrixChanged(o,1);
}
void tnsRotateObjectLocal(tnsObject *o, real x, real y, real z, real angle){
    tnsMatrix44d tback,tfwd,rot,res1,res2;
    //tnsVector3d gp,gpt; gp[0]=gcx; gp[1]=gcy; gp[2]=gcz;
    //tnsMakeTranslationMatrix44d(tfwd, gcx, gcy, gcz);
    //tnsInverse44d(tback,tfwd);
    tnsMakeRotationMatrix44d(rot,angle,x,y,z);
    //tnsMultiply44d(res1,o->SelfTransform,tback);
    //tnsMultiply44d(res2,res1,rot);
    //tnsMultiply44d(o->SelfTransform,res2,tfwd);
    tnsMultiply44d(res1,o->SelfTransform,rot);
    tnsCopyMatrix44d(res1,o->SelfTransform);
    tnsSelfMatrixChanged(o,1);
}
void tnsRotateObjectDelta(tnsObject *o, real x, real y, real z, real angle){
    tnsMatrix44d tback,tfwd,rot,res1,res2;
    //tnsVector3d gp,gpt; gp[0]=gcx; gp[1]=gcy; gp[2]=gcz;
    //tnsApplyTransform43d(gpt,o->DeltaTransform,gp);
    //tnsMakeTranslationMatrix44d(tfwd, gpt[0]-gcx, gpt[0]-gcy, gpt[0]-gcz);
    //tnsInverse44d(tback,tfwd);
    //tnsMakeRotationMatrix44d(rot,angle,x,y,z);
    //tnsMultiply44d(res1,o->SelfTransform,tback);
    //tnsMultiply44d(res2,res1,rot);
    //tnsMultiply44d(o->SelfTransform,res2,tfwd);
    tnsMakeRotationMatrix44d(rot,angle,x,y,z);
    tnsMultiply44d(res1,o->DeltaTransform,rot);
    tnsCopyMatrix44d(res1,o->DeltaTransform);
    tnsExtractDeltaTransformValue(o);
    tnsSelfMatrixChanged(o,1);
}
void tnsRotateObjectGlobalForDelta(tnsObject *o, real x, real y, real z, real angle, real cx,real cy,real cz){
    tnsMatrix44d tback,tfwd,rot,rot2,res1,res2;
    real xs, ys, zs;
    tnsMakeRotationMatrix44d(rot,angle,x,y,z);
    tnsMakeRotationMatrix44d(rot2,-angle,x,y,z);//why?

    tnsMakeTranslationMatrix44d(tfwd, o->GLocation[0],o->GLocation[1],o->GLocation[2]);
    tnsInverse44d(tback,tfwd);
    tnsMultiply44d(res1,o->GlobalTransform,tback);
    tnsMultiply44d(res2,rot,res1);
    tnsMultiply44d(o->GlobalTransform,res2,tfwd);

    tnsVector3d delta; delta[0]=o->GLocation[0]-cx; delta[1]=o->GLocation[1]-cy; delta[2]=o->GLocation[2]-cz;
    tnsVector3d c; c[0]=cx; c[1]=cy; c[2]=cz; tnsVector3d r;
    tnsApplyRotation43d(r,rot2,delta);
    tnsVectorPlus3d(&o->GlobalTransform[12],c,r);
    //memcpy(o->GlobalTransform, res1, sizeof(tnsMatrix44d));
    tnsGlobalMatrixChangedForDelta(o,1);
}
void tnsScaleObject(tnsObject *o, real x,real y,real z, real cx,real cy,real cz){
    tnsMatrix44d sca,res1,res2;
    tnsMakeScaleMatrix44d(sca,x,y,z);
    tnsMultiply44d(res1,o->GlobalTransform,sca);
    tnsVector3d delta; delta[0]=res1[12]-cx; delta[1]=res1[13]-cy; delta[2]=res1[14]-cz;
    tnsVector3d c; c[0]=cx; c[1]=cy; c[2]=cz;
    delta[0]*=x; delta[1]*=y; delta[2]*=z;
    tnsVectorPlus3d(&res1[12],c,delta);
    memcpy(o->GlobalTransform, res1, sizeof(tnsMatrix44d));
    tnsGlobalMatrixChanged(o,1);
}
void tnsScaleObjectDelta(tnsObject *o, real x,real y,real z, real cx,real cy,real cz){
    tnsMatrix44d sca,res1,res2;
    tnsMakeScaleMatrix44d(sca,x,y,z);
    tnsMultiply44d(res1,o->GlobalTransform,sca);
    tnsVector3d delta; delta[0]=res1[12]-cx; delta[1]=res1[13]-cy; delta[2]=res1[14]-cz;
    tnsVector3d c; c[0]=cx; c[1]=cy; c[2]=cz;
    delta[0]*=x; delta[1]*=y; delta[2]*=z;
    tnsVectorPlus3d(&res1[12],c,delta);
    memcpy(o->GlobalTransform, res1, sizeof(tnsMatrix44d));
    tnsGlobalMatrixChangedForDelta(o,1);
}
void tnsZoomViewingCamera(tnsCamera *c, real Ratio){
    if (c->FocusDistance < 0.1 && Ratio > 0) return;
    tnsMoveObjectLocal(c, 0, 0, -c->FocusDistance * Ratio);
    c->FocusDistance *= (1 - Ratio);
}
void tnsRotateViewingCamera(tnsCamera *c, real x, real z){
    tnsMoveObjectLocal(c, 0, 0, -c->FocusDistance);
    tnsRotateObjectLocalValues(c, x, 0, z);
    tnsMoveObjectLocal(c, 0, 0, c->FocusDistance);
}
void tnsGetCameraMovingDeltas(tnsCamera *c, int ViewportW, int ViewportH, real x, real y, tnsVector4d p){
    memset(p, 0, sizeof(tnsVector4d));
    tnsVector4d tp = {0};
    tnsMatrix44d combine, projection, projinv, vpinv, vp;

    tnsLoadIdentity44d(projection);
    tnsMakePerspectiveMatrix44d(projection, c->FOV, (real)ViewportW / (real)ViewportH, c->ZMin, c->ZMax);
    tnsMakeViewportMatrix44d(vp, ViewportW, ViewportH, c->ZMax, c->ZMin);
    tnsInverse44d(projinv, projection);
    tnsInverse44d(vpinv, vp);

    p[2] = -c->FocusDistance;
    p[3] = 1;
    tnsApplyTransform44d(tp, projection, p);
    tnsVectorMultiSelf3d(tp, 1 / tp[3]);
    tnsApplyTransform43d(p, vp, tp);
    p[0] += x;
    p[1] += y;

    tnsApplyTransform43d(tp, vpinv, p);
    tnsVectorMultiSelf3d(tp, tp[3]);
    tnsApplyTransform44d(p, projinv, tp);
}
void tnsTranslateViewingCamera(tnsCamera *c, int ViewportW, int ViewportH, real x, real y){
    tnsVector4d p={0};
    tnsGetCameraMovingDeltas(c,ViewportW, ViewportH, x,y,p);
    tnsMoveObjectLocal(c, p[0], p[1], 0);
}

tnsObject *tnsCreateRootObject(char *name, int Use2D){
    tnsWorld *w = T->World;
    tnsRootObject *o = memAcquireHyper(sizeof(tnsRootObject));

    o->Base.Type==TNS_OBJECT_ROOT;
    strSafeSet(&o->Base.Name, name);
    lstAppendItem(&w->RootObjects, o);
    memAssignRef(w,&w->ActiveRoot,o);

    o->Base.Drivers=memAcquire(sizeof(laRackPageCollection));
    o->Is2D=Use2D;

    laAnimationUpdateHolderList();

    return o;
}
void tnsDestroyRootObject(tnsObject *root){
    tnsWorld *w = T->World;
    memAssignRef(w,&w->ActiveRoot,root->Item.pPrev ? root->Item.pPrev : root->Item.pNext ? root->Item.pNext : 0);

    root->Evaluated.Done=root->EvaluatedPlay.Done=0;
    tnsFreeEvaluatedArray(&root->Evaluated);
    tnsClearPlayDuplicate(root);
    if(root->EvaluatedPlay.Commands) tnsFreeEvaluatedArray(&root->EvaluatedPlay);
    tnsFreeEvaluatedScene(&root->EvaluatedPlay);

    //destroy rack page; and then release memory;
    laRackPage*rp;
    while(rp=lstPopItem(&root->Drivers->Pages)){
        laNodeRack* rr; while(rr=lstPopItem(&rp->Racks)){ laDestroyRack(rr); }
        memLeave(rp);
    }
    memLeave(root->Drivers); // root->Drivers=0; never reset address for single, see la_GiveDiffCommand()

    tnsRootObject* ro=root; laAction* aa; laActionProp* ap;
    while(aa=ro->Actions.pFirst){ laAnimationRemoveAction(aa); }
    while(ap=lstPopItem(&ro->ActionProps)){ strSafeDestroy(&ap->CachedName); memLeave(ap); }

    lstRemoveItem(&w->RootObjects, root);
    tnsObject*co;
    while(co=lstPopPointerLeave(&root->ChildObjects)){ tnsDestroyObject(co); }
    strSafeDestroy(&root->Name);
    memLeave(root);

    laAnimationUpdateHolderList();
}
void tnsDestroyObject(tnsObject *o){
    if(o->Type==TNS_OBJECT_ROOT){ tnsDestroyRootObject(o); return; }

    tnsFreeEvaluatedArray(&o->Evaluated);
    tnsClearPlayDuplicate(o);
    if(o->EvaluatedPlay.Commands) tnsFreeEvaluatedArray(&o->EvaluatedPlay);
    tnsFreeEvaluatedScene(&o->EvaluatedPlay);

    tnsUnparentObject(o,1);
    while(o->ChildObjects.pFirst){ tnsUnparentObject(((laListItemPointer*)o->ChildObjects.pFirst)->p, 1); }

    lstRemoveItem(&T->World->AllObjects, o);

    if(o->InRoot){ lstRemovePointerLeave(&o->InRoot->ChildObjects, o);
        tnsInvalidateEvaluation(o);
        if(o->InRoot->Active==o){ memAssignRef(o->InRoot,&o->InRoot->Active,0); }
    }

    if(o->Type==TNS_OBJECT_MESH){ tnsMeshObject* mo=o;
        if(mo->v) arrFree(&mo->v, &mo->maxv); if(mo->e) arrFree(&mo->e, &mo->maxe);
        if(mo->f){ for(int i=0;i<mo->totf;i++){ free(mo->f[i].loop); mo->f[i].loop=0; } arrFree(&mo->f, &mo->maxf); }
        mo->totv=mo->tote=mo->totf=0;
        tnsInvalidateMeshBatch(o);
        tnsMaterialSlot* ms; while(ms=lstPopItem(&mo->Materials)){ memLeave(ms); } memAssignRef(mo,&mo->CurrentMaterial,0);
    }
    if(o->Type==TNS_OBJECT_SHAPE){ tnsShapeObject* so=o;
        tnsShape* s; while(s=lstPopItem(&so->Shapes)){ tnsSPoint* sp; while(sp=lstPopItem(&s->Points)){ memLeave(sp); } memLeave(s); }
        tnsMaterialSlot* ms; while(ms=lstPopItem(&so->Materials)){ memLeave(ms); } memAssignRef(so,&so->CurrentMaterial,0);
    }

    strSafeDestroy(&o->Name);
    memLeave(o);
}
tnsCamera *tnsCreateCamera(tnsObject *under, char *Name, real FOV,
                        real AtX, real AtY, real AtZ,
                        real RotX, real RotY, real RotZ,
                        real FocusDistance){
    tnsCamera *c; tnsWorld *w = T->World;

    c = memAcquireHyper(sizeof(tnsCamera));
    tnsInitObjectBase(&c->Base, under, Name, TNS_OBJECT_CAMERA, AtX, AtY, AtZ, RotX, RotY, RotZ, 1.0f, TNS_ROTATION_XYZ_EULER, 1.0f);
    c->FOV = FOV;
    c->CameraType = TNS_CAMERA_PERSP;
    c->ZMin = 0.1f;
    c->ZMax = 200.0f;
    c->FocusDistance = FocusDistance;
    c->OrthScale = 1.0f;
    if(under && under->Type==TNS_OBJECT_ROOT){ memAssignRef(under,&((tnsRootObject*)under)->ActiveCamera,c); }

    return c;
}
tnsObject *tnsCreateInstancer(tnsObject *under, char *Name, real AtX, real AtY, real AtZ){
    tnsObject *o; tnsWorld *w = T->World; if (!under) return 0;

    o = memAcquireHyper(sizeof(tnsInstancer));
    tnsInitObjectBase(o, under, Name, TNS_OBJECT_INSTANCER, AtX, AtY, AtZ, 0, 0, 0, 1.0f, TNS_ROTATION_XYZ_EULER, 1.0f);

    return o;
}
tnsLight *tnsCreateLight(tnsObject *under, char *Name, real AtX, real AtY, real AtZ, real Strength, int UniDirectional){
    tnsLight *l; tnsWorld *w = T->World; if (!under) return 0;
    tnsObject* root=under->Type==TNS_OBJECT_ROOT?under:under->InRoot;
    if (UniDirectional&&under){
        if(root){ if(root->ParentObject) return root->ParentObject; }
    }

    l = memAcquireHyper(sizeof(tnsLight));
    tnsInitObjectBase(&l->Base, under, Name, TNS_OBJECT_LIGHT, AtX, AtY, AtZ, 0, 0, 0, 1.0f, TNS_ROTATION_XYZ_EULER, 1.0f);
    l->Strength = Strength;
    l->UniDirectional = UniDirectional;

    // ?????????? WTF
    //if (root && UniDirectional && !root->ParentObject) memAssignRef(root, &root->ParentObject,l);
    return l;
}

tnsInstancer* tnsDuplicateInstancer(tnsInstancer* from){
    if(from->Base.Type!=TNS_OBJECT_INSTANCER) return 0; tnsInstancer* to = memAcquireHyper(sizeof(tnsInstancer));
    tnsInitObjectBase(to, from->Base.ParentObject?from->Base.ParentObject:from->Base.InRoot, from->Base.Name->Ptr, TNS_OBJECT_INSTANCER,0,0,0,0,0,0,0,0,1);
    tnsCopyObjectTransformationsLocal(to,from);
    to->Instance=from->Instance;
    return to;
}

int tnsSizeOfObject(tnsObject* o){
    switch(o->Type){
    case TNS_OBJECT_ROOT: return sizeof(tnsRootObject);
    case TNS_OBJECT_INSTANCER: return sizeof(tnsInstancer);
    case TNS_OBJECT_MESH: return sizeof(tnsMeshObject);
    case TNS_OBJECT_CAMERA: return sizeof(tnsCamera);
    case TNS_OBJECT_LIGHT: return sizeof(tnsLight);
    case TNS_OBJECT_SHAPE: return sizeof(tnsShapeObject);
    default: return sizeof(tnsObject);
    }
}
tnsObject* tnsDuplicateObject(tnsObject* o){
    tnsMeshObject* to = memAcquireHyper(tnsSizeOfObject(o));
    tnsInitObjectBase(to, o->ParentObject?o->ParentObject:o->InRoot, o->Name->Ptr, TNS_OBJECT_MESH,0,0,0,0,0,0,0,0,1);
    tnsCopyObjectTransformationsLocal(to,o);
    return to;
}

int tnsAnyObjectsSelected(tnsObject *root){
    for(laListItemPointer* lip=root->ChildObjects.pFirst;lip;lip=lip->pNext){ tnsObject* o=lip->p; if(o->Flags&TNS_OBJECT_FLAGS_SELECTED) return 1; 
        if(o->ChildObjects.pFirst && tnsAnyObjectsSelected(o)) return 1;
    } return 0;
}
void tnsDeselectAllObjects(tnsObject *root){
    for(laListItemPointer* lip=root->ChildObjects.pFirst;lip;lip=lip->pNext){ tnsObject* o=lip->p; o->Flags&=(~TNS_OBJECT_FLAGS_SELECTED);
        if(o->ChildObjects.pFirst)tnsDeselectAllObjects(o);
    }
    memAssignRef(root,&root->Active,0);
    tnsInvalidateEvaluation(root);
}
void tnsSelectAllObjects(tnsObject *root){
    for(laListItemPointer* lip=root->ChildObjects.pFirst;lip;lip=lip->pNext){ tnsObject* o=lip->p; o->Flags|=TNS_OBJECT_FLAGS_SELECTED;
        if(o->ChildObjects.pFirst)tnsSelectAllObjects(o);
    }
    tnsInvalidateEvaluation(root);
}
void tnsSelectObject(tnsObject* o, int Select, int Toggle){
    if(!o) return;
    if(Toggle) tnsSelectObject(o,(o->Flags&TNS_OBJECT_FLAGS_SELECTED?0:1),0);
    elif(Select) o->Flags|=TNS_OBJECT_FLAGS_SELECTED; else o->Flags&=(~TNS_OBJECT_FLAGS_SELECTED);
    tnsInvalidateEvaluation(o);
}

void tnsGetCameraProjection(tnsMatrix44d* mat, int w, int h, tnsCamera* Camera){
    tnsMatrix44d inv, result;
    tnsMakePerspectiveMatrix44d(mat, Camera->FOV, (real)w / (real)h, Camera->ZMin, Camera->ZMax);
}
void tnsGetCameraViewProjection(tnsMatrix44d* mat, int w, int h, tnsCamera* Camera){
    tnsMatrix44d inv, result;
    tnsMakePerspectiveMatrix44d(mat, Camera->FOV, (real)w / (real)h, Camera->ZMin, Camera->ZMax);
    tnsInverse44d(inv, Camera->Base.GlobalTransform);
    tnsMultiply44d(result, mat, inv);
    memcpy(mat, result, sizeof(tnsMatrix44d));
}
void tnsApplyCameraView(int W, int H, tnsCamera *Camera, tnsMatrix44d out_optional_view, tnsMatrix44d out_optional_projection){
    tnsMatrixStackItem *tmsi = tKnlGetCurrentMatStackItem();
    tnsShader *current_shader = 0;
    real *mat;
    tnsMatrix44d result, inv;

    if (!Camera) return;

    mat = tnsGetProjectionMatrix();
    tnsMakePerspectiveMatrix44d(mat, Camera->FOV, (real)W / (real)H, Camera->ZMin, Camera->ZMax);
    if (current_shader = T->CurrentShader){
        tnsShaderApplyProjection(current_shader, mat);
        tnsShaderApplyProjectionInverse(current_shader, mat);
        if(out_optional_projection){ tnsCopyMatrix44d(mat,out_optional_projection); }
    }

    tnsResetViewMatrix();
    mat = tnsGetViewMatrix();
    tnsInverse44d(inv, Camera->Base.GlobalTransform);
    tnsMultiply44d(result, mat, inv);
    memcpy(mat, result, sizeof(tnsMatrix44d));
    tnsVector3d dir={0,0,0}; tnsSetCameraViewDir(dir);

    if (current_shader = T->CurrentShader){
        tnsShaderApplyView(current_shader, result);
        if(out_optional_view){ tnsCopyMatrix44d(mat,out_optional_view); }
        if(current_shader->uViewPos>-1) glUniform3f(current_shader->uViewPos,LA_COLOR3(Camera->Base.GLocation));
        tnsVectorSet3v(T->SetViewPos,Camera->Base.GLocation);
    }
}
void tnsApplyShadowCameraView(tnsLight *Light){
    tnsMatrixStackItem *tmsi = tKnlGetCurrentMatStackItem();
    tnsShader *current_shader = 0;
    tnsMatrix44d mat;
    tnsMatrix44d result, inv;

    if (!Light || !Light->UniDirectional) return;

    //mat = tnsGetProjectionMatrix();
    tnsMakeOrthoMatrix44d(mat, -1000,1000,-1000,1000,-1000,1000);

    tnsInverse44d(inv, Light->Base.GlobalTransform);
    tnsMultiply44d(result, mat, inv);
    memcpy(mat, result, sizeof(tnsMatrix44d));

    if (current_shader = T->CurrentShader){
        tnsShaderApplyShadowMatrix(current_shader,mat);
    }
}
void tnsSetCameraViewDir(tnsVector3d dir){
    tnsVectorSet3v(T->SetViewDir, dir);
    if(T->BindedShader && T->BindedShader->uViewDir>-1){ glUniform3f(T->BindedShader->uViewDir,LA_COLOR3(dir)); }
}

void tnsApplyModelMatrix(tnsMatrix44d m){
    tnsShader *current_shader = 0; real *mat;
    if (!m) return;
    mat = tnsGetModelMatrix();
    memcpy(mat, m, sizeof(tnsMatrix44d));
    if (current_shader = T->CurrentShader){ tnsShaderApplyModel(current_shader, mat); }
}

void tnsLookAt(tnsObject *o, tnsVector3d Target, tnsVector3d Up){
    tnsVector4d target = {0};
    tnsMatrix44d mat, res, res2;
    real *d;
    tnsShader *current_shader = 0;

    d = tnsGetModelMatrix();

    tnsMakeZTrackingMatrix44d(mat, o->GLocation, target, Up);
    tnsLoadIdentity44d(res);
    tnsMakeTranslationMatrix44d(res,o->GLocation[0],o->GLocation[1],o->GLocation[2]);
    tnsMultiply44d(res2, res, mat);
    memcpy(o->SelfTransform, res2, sizeof(tnsMatrix44d));

    tnsSelfMatrixChanged(o,1);

    if (current_shader = T->CurrentShader){
        tnsShaderApplyModel(current_shader, d);
    }
}


void tnsDrawCamera(tnsObject *o){
    tnsCamera *c = o;
    real fov_2 = c->FOV / 2; real len=1;
    real ex, ey, ez; ey = len * sin(fov_2); ex = ey; ez = len * cos(fov_2);

    tnsVertex3d(ex, ey, -ez); tnsVertex3d(ex, -ey, -ez); tnsVertex3d(0.0, 0.0, 0.0);
    tnsVertex3d(ex, ey, -ez); tnsVertex3d(-ex, ey, -ez); tnsVertex3d(0.0, 0.0, 0.0);
    tnsVertex3d(-ex, -ey, -ez); tnsVertex3d(-ex, ey, -ez);
    tnsPackAs(GL_LINE_STRIP);
    tnsVertex3d(ex,ey*1.25,-ez); tnsVertex3d(0,ey*2,-ez); tnsVertex3d(-ex,ey*1.25,-ez); 
    tnsPackAs(GL_LINE_LOOP);
    tnsVertex3d(-ex, -ey, -ez); tnsVertex3d(ex, -ey, -ez);
    tnsPackAs(GL_LINES);
}
void tnsDrawCross(real x,real y,real z,real x1,real x2,real y1,real y2,real z1,real z2){
    tnsVertex3d(x+x1, y+0.0, z+0.0); tnsVertex3d(x+x2, y+0.0, z+0.0);
    tnsVertex3d(x+0.0,y+ y1, z+0.0); tnsVertex3d(x+0.0,y+ y2, z+0.0);
    tnsVertex3d(x+0.0,y+ 0.0,z+ z1); tnsVertex3d(x+0.0,y+ 0.0,z+ z2);
}

void tnsDrawEmptySelectionID(tnsEvaluatedInstance* ei, void* unused){
    int i=ei->InstanceSelectionID; real color[4]={0,0,0,1}; TNS_ID_TO_COLOR(color,i); tnsColor4dv(color);
    tnsDrawCross(0,0,0,-0.2,1,-0.2,1,-0.2,1); tnsPackAs(GL_LINES); tnsFlush();
}
void tnsDrawEmptyOutline(tnsEvaluatedInstance* ei, void* unused){
    real* color=(ei->IsActive)?laAccentColor(LA_BT_TEXT):laAccentColor(LA_BT_NORMAL); tnsColor4dv(color);
    tnsDrawCross(0,0,0,-0.2,1,-0.2,1,-0.2,1); tnsLineWidth(3);tnsPackAs(GL_LINES); tnsLineWidth(1); tnsFlush();
}
void tnsDrawEmptyObject(tnsEvaluatedInstance* ei, void* unused){
    tnsColor4dv(laThemeColor(_LA_THEME_3D_VIEW,LA_BT_BORDER));
    tnsDrawCross(0,0,0,-0.2,1,-0.2,1,-0.2,1);tnsLineWidth(3); tnsPackAs(GL_LINES); tnsLineWidth(1); tnsFlush();
}
void tnsEvaluateEmptyObject(tnsObject* o, tnsEvaluateData* ed){
    if(ed->SceneEvaluateMode){ return; }
    tnsAddEvaluatedInstance(ed,o,tnsDrawEmptyObject,TNS_EVAL_LAYER_SOLID,0,0,0);
    if(ed->FillOutline && o->Flags&TNS_OBJECT_FLAGS_SELECTED){
        tnsAddEvaluatedInstance(ed,o,tnsDrawEmptyOutline,TNS_EVAL_LAYER_OUTLINE,ed->Active==o,0,0);
    }
    if(ed->FillSelectionID){
        tnsAddEvaluatedInstance(ed,o,tnsDrawEmptySelectionID,TNS_EVAL_LAYER_SELECTION,0,1,o->SelectID);
    }
}
void tnsPushEvaluateMatrixWith(tnsEvaluateData* ed, tnsMatrix44d mat){
    arrEnsureLength(&ed->MatArr,ed->NextMat,&ed->MaxMat,sizeof(tnsMatrix44d)); if(ed->NextMat<1) return;
    real* new=ed->MatArr[ed->NextMat];
    tnsMultiply44d(new, ed->MatArr[ed->NextMat-1], mat);
    ed->NextMat++;
}
void tnsPushEvaluate2DMatrixWith(tnsEvaluateData* ed,tnsInstancer* o){
    real* mat=o->Base.GlobalTransform; tnsMatrix44d tmat; int Hook=o->Hook;
    if(Hook){ real X=0,Y=0;
        if(Hook==TNS_INSTANCER_HOOK_TL||Hook==TNS_INSTANCER_HOOK_CL||Hook==TNS_INSTANCER_HOOK_BL){ X=-(real)ed->W/2/LA_RH; }
        if(Hook==TNS_INSTANCER_HOOK_TR||Hook==TNS_INSTANCER_HOOK_CR||Hook==TNS_INSTANCER_HOOK_BR){ X=(real)ed->W/2/LA_RH; }
        if(Hook==TNS_INSTANCER_HOOK_TL||Hook==TNS_INSTANCER_HOOK_TC||Hook==TNS_INSTANCER_HOOK_TR){ Y=(real)ed->H/2/LA_RH; }
        if(Hook==TNS_INSTANCER_HOOK_BL||Hook==TNS_INSTANCER_HOOK_BC||Hook==TNS_INSTANCER_HOOK_BR){ Y=-(real)ed->H/2/LA_RH; }
        X+=o->HookOffset[0]; Y+=o->HookOffset[1];
        tnsMakeTranslationMatrix44d(tmat,X,Y,0); mat=tmat;
    }
    arrEnsureLength(&ed->MatArr,ed->NextMat,&ed->MaxMat,sizeof(tnsMatrix44d)); if(ed->NextMat<1) return;
    real* new=ed->MatArr[ed->NextMat]; tnsCopyMatrix44d(mat,new); ed->NextMat++;
}
void tnsPopEvaluateMatrix(tnsEvaluateData* ed){ ed->NextMat--; }
void tnsEvaluateInstancerObject(tnsInstancer* o, tnsEvaluateData* ed){
    int origid=ed->OverrideID; ed->OverrideID = o->Base.SelectID;
    tnsEvaluateEmptyObject(o,ed);
    int origoutline=ed->FillOutline; ed->FillOutline=0;
    if(o->Instance){ int PrevState2D=ed->State2D;
        tnsRootObject* inst=o->Instance; if(inst->Base.Type==TNS_OBJECT_ROOT&&inst->Is2D){ ed->State2D=1; }
        if(ed->SceneEvaluateMode){ inst=tnsEnsurePlayDuplicate(inst); }
        for(laListItemPointer* lip=ed->ParentStack.pFirst;lip;lip=lip->pNext){
            if(lip->p == inst || lip->p==o->Instance){ goto eval_inst_cleanup; }
        }
        if(ed->State2D){ tnsPushEvaluate2DMatrixWith(ed,o); }
        else{ tnsPushEvaluateMatrixWith(ed,o->Base.GlobalTransform); }
        tnsEvaluateObjectTree(inst, ed, ed->SceneEvaluateMode);
        tnsPopEvaluateMatrix(ed);
        ed->State2D=PrevState2D;
    }
eval_inst_cleanup:
    ed->OverrideID=origid; ed->FillOutline=origoutline;
}

void tnsDrawCameraSelectionID(tnsEvaluatedInstance* ei, void* unused){
    int i=ei->InstanceSelectionID; real color[4]={0,0,0,1}; TNS_ID_TO_COLOR(color,i); tnsColor4dv(color);
    tnsDrawCamera(ei->Object); tnsFlush();
}
void tnsDrawCameraOutline(tnsEvaluatedInstance* ei, void* unused){
    real* color=(ei->IsActive)?laAccentColor(LA_BT_TEXT):laAccentColor(LA_BT_NORMAL); tnsColor4dv(color); tnsLineWidth(3);
    tnsDrawCamera(ei->Object); tnsLineWidth(1); tnsFlush();
}
void tnsDrawCameraObject(tnsEvaluatedInstance* ei, void* unused){
    tnsColor4dv(laThemeColor(_LA_THEME_3D_VIEW,LA_BT_BORDER)); tnsLineWidth(3);
    tnsDrawCamera(ei->Object); tnsLineWidth(1); tnsFlush();
}
void tnsEvaluateCameraObject(tnsObject* o, tnsEvaluateData* ed){
    if(ed->SceneEvaluateMode){ return; }
    tnsAddEvaluatedInstance(ed,o,tnsDrawCameraObject,TNS_EVAL_LAYER_SOLID,0,0,0);
    if(ed->FillOutline && o->Flags&TNS_OBJECT_FLAGS_SELECTED){
        tnsAddEvaluatedInstance(ed,o,tnsDrawCameraOutline,TNS_EVAL_LAYER_OUTLINE,ed->Active==o,0,0);
    }
    if(ed->FillSelectionID){
        tnsAddEvaluatedInstance(ed,o,tnsDrawCameraSelectionID,TNS_EVAL_LAYER_SELECTION,0,1,o->SelectID);
    }
}

void tnsRunNode(tnsEvaluatedNode* en, int func, tnsObject* root){
    T->Runtime.CurrentEN = en;
    tnsObject* ob=root; // should be the same as en->Target.
    for(laRackPage* rp=ob->Drivers->Pages.pFirst;rp;rp=rp->Item.pNext){
        if(!rp->Eval.pFirst || rp->TriggerMode!=func) continue;
        laRunPage(rp, 1);
    }
}

#ifdef LA_WITH_LUAJIT
void tnsLuaEnsureNode(lua_State* L,int index,tnsObject* o);
void tnsLuaRunNode(lua_State* L, tnsEvaluatedNode* en, char* func, tnsObject* root);
#endif
void tnsEvaluateThisObject(tnsObject *o, tnsEvaluateData* ed){
    tnsEvaluatedNode* CP,*CC;
    if(ed->SceneEvaluateMode){
        tnsEvaluateSyncNode(ed,o);
        if(o->Type==TNS_OBJECT_INSTANCER){
            CP=ed->Scene->CurrentParent; CC=ed->Scene->CurrentChild;
            ed->Scene->CurrentParent=ed->Scene->CurrentChild; ed->Scene->CurrentChild=ed->Scene->CurrentParent->Children.pFirst;
        }elif(o->Type==TNS_OBJECT_ROOT){ tnsEvaluatedNode* en=ed->Scene->CurrentChild;
            tnsRunNode(en,TNS_PAGE_TRIGGER_PROCESS,o);
#ifdef LA_WITH_LUAJIT
            tnsLuaEnsureNode(ed->L,en->LuaID,o);
            tnsLuaRunNode(ed->L,en,"run",o);
#endif
            if(en->ActionRetarget){
                laActionRetarget* ar;
                if(en->ActionRetarget->DetachedInNode){ ar = en->ActionRetarget; }
                else{ ar = ((tnsRootObject*)en->Target)->ActionRetarget; }
                laAnimationSyncRetarget(ar, MAIN.Animation->RunPlayHead);
                laAnimationEvaluateRetargetedActions(ar);
            }
        }
    }
    if (!o->Show) return;
    switch (o->Type){
    case TNS_OBJECT_MESH: tnsEvaluateMeshObject(o, ed); break;
    case TNS_OBJECT_INSTANCER:
        tnsEvaluateInstancerObject(o,ed);
        if(ed->SceneEvaluateMode){ ed->Scene->CurrentParent=CP; ed->Scene->CurrentChild=CC;}
        break;
    case TNS_OBJECT_CAMERA: tnsEvaluateCameraObject(o,ed); break;
    case TNS_OBJECT_SHAPE: tnsEvaluateShapeObject(o,ed); break;
    default: tnsEvaluateEmptyObject(o,ed); break;
    }
}

void tnsClearDriversEval(tnsObject* o){
    if(!o->Drivers) return;
    for(laRackPage* rp=o->Drivers->Pages.pFirst;rp;rp=rp->Item.pNext){
        while(lstPopPointer(&rp->Eval)); while(lstPopPointer(&rp->AlwaysBranchers));
    }
}
void tnsClearPlayDuplicate(tnsObject* o){
    if(!o || !o->PlayDuplicate) return;
    tnsClearDriversEval(o);
    while(lstPopPointer(&o->PlayDuplicate->ChildObjects));
    for(laListItemPointer* lip=o->ChildObjects.pFirst;lip;lip=lip->pNext){
        tnsObject*co=lip->p; tnsClearPlayDuplicate(co);
    }
    if(o->Type==TNS_OBJECT_ROOT){ tnsRootObject* ro=o; laAnimationClearRetarget(&ro->ActionRetarget); }
    elif(o->Type==TNS_OBJECT_INSTANCER){ tnsInstancer* oi=o; tnsClearPlayDuplicate(oi->Instance); }
    memFree(o->PlayDuplicate); o->PlayDuplicate=0;
}
void tnsFreeEvaluatedNode(tnsEvaluatedNode* n){
    tnsEvaluatedNode* en;
    while(en=lstPopItem(&n->Children)){ tnsFreeEvaluatedNode(en); }
    n->Target->LuaCacheID=0; laAnimationClearRetarget(&n->ActionRetarget);
    memFree(n);
}
void tnsFreeEvaluatedScene(tnsEvaluateData* ed){
    if(!ed->Scene) return;
    if(ed->Scene->Root) tnsFreeEvaluatedNode(ed->Scene->Root);
#ifdef LA_WITH_LUAJIT
    logPrintNew("Shutting down Lua for the evaluated scene.\n");
    lua_close(ed->L);
#endif
    memFree(ed->Scene); ed->Scene=0;
}
void tnsClearPlayState(tnsObject* o){
    tnsClearPlayDuplicate(o);
    if(o->EvaluatedPlay.Commands) tnsFreeEvaluatedArray(&o->EvaluatedPlay);
    tnsFreeEvaluatedScene(&o->EvaluatedPlay);
}
void tnsEnsureDriversEval(tnsObject* o){
    if(!o->Drivers) return;
    for(laRackPage* rp=o->Drivers->Pages.pFirst;rp;rp=rp->Item.pNext){
        laRebuildPageEval(rp);
    }
}
tnsObject* tnsEnsurePlayDuplicate(tnsObject* o){
    tnsEnsureDriversEval(o);
    if(o->Flags&TNS_OBJECT_FLAGS_PLAY_DUPLICATE) return o;
    if(o->PlayDuplicate) return o->PlayDuplicate;
    int obsize=tnsSizeOfObject(o);
    tnsObject* dup=memAcquireHyper(obsize);
    memcpy(dup,o,obsize); o->PlayDuplicate=dup;
    dup->ChildObjects.pFirst=dup->ChildObjects.pLast=0;
    for(laListItemPointer* lip=o->ChildObjects.pFirst;lip;lip=lip->pNext){
        tnsObject*co=lip->p; tnsEnsurePlayDuplicate(co);
        lstAppendPointer(&dup->ChildObjects,co->PlayDuplicate);
        co->PlayDuplicate->ParentObject=o->ParentObject?dup:0; //objects directly under root doesn't have parent object
    }
    if(o->Type==TNS_OBJECT_ROOT){ tnsRootObject* ro=o; laAnimationEnsureRetarget(o,&ro->Actions,&ro->ActionRetarget);
        ((tnsRootObject*)o->PlayDuplicate)->ActionRetarget=ro->ActionRetarget;
    }elif(o->Type==TNS_OBJECT_INSTANCER){ tnsInstancer* oi=o; tnsEnsurePlayDuplicate(oi->Instance); }
    dup->Flags|=TNS_OBJECT_FLAGS_PLAY_DUPLICATE;
    return o->PlayDuplicate;
}

#ifdef LA_WITH_LUAJIT

void tnsLuaPushTypedPointer(lua_State* L, char* ct, void* data){
    lua_getglobal(L,"ffi"); lua_getfield(L,-1,"cast"); lua_pushstring(L,ct); lua_pushlightuserdata(L,data);
    lua_call(L,2,1); lua_remove(L,-2);
}
void tnsLuaSetObject(lua_State* L, tnsObject*o){
    tnsLuaPushTypedPointer(L,"struct tnsObject *",o); lua_setfield(L,-2,"object");
}
void tnsLuaInit(lua_State* L){
    if(luaL_loadstring(L, LA_TNS_LIB_COMMON) || lua_pcall(L, 0, 0, 0)){ logPrint("    Error loading tns lua libs\n");
        lua_getglobal(L,"log"); lua_insert(L,-2); if(lua_pcall(L,1,0,0)){ lua_pop(L,1); }
    };
    lua_settop(L,0);
    lua_newtable(L); lua_setglobal(L,"scene");
    lua_getglobal(L,"scene"); lua_pushstring(L,"nodes"); lua_newtable(L); lua_settable(L,1);
    lua_pushnil(L); lua_setfield(L,1,"node"); lua_pop(L,1);
}
void tnsLuaEnsureNode(lua_State* L,int index,tnsObject* o){ lua_settop(L,0);
    lua_getglobal(L,"scene"); lua_getfield(L,1,"nodes");
    lua_pushinteger(L,index); lua_gettable(L,2); if(lua_isnil(L,3) || (!lua_istable(L,3))){
        lua_pushinteger(L,index); lua_newtable(L); lua_settable(L,2); lua_pop(L,1);
        lua_pushinteger(L,index); lua_gettable(L,2);
        lua_pushlightuserdata(L,0); lua_setfield(L,3,"object");
    }
    lua_settop(L,0);
}
void tnsLuaRemoveNode(lua_State* L,int index){ lua_settop(L,0);
    lua_getglobal(L,"scene"); lua_pushstring(L,"nodes"); lua_gettable(L,1);
    lua_pushinteger(L,index); lua_pushnil(L); lua_settable(L,2); lua_pop(L,2);
}
void tnsLuaRunNode(lua_State* L, tnsEvaluatedNode* en, char* func, tnsObject* root){ lua_settop(L,0); int index=en->LuaID;
    lua_getglobal(L,"scene"); lua_getfield(L,1,"nodes");
    lua_pushinteger(L,index); lua_gettable(L,-2); lua_setfield(L,1,"node");
    lua_getfield(L,1,"node"); tnsLuaSetObject(L,root);
    lua_getfield(L,-1,"scripts");
    if(lua_isnil(L,-1) || (!lua_istable(L,-1))){
        lua_pop(L,1); lua_newtable(L); lua_setfield(L,-2,"scripts"); lua_getfield(L,-1,"scripts");
    }
    if(!en->LuaLoaded){
        if(root->Drivers){
            if(root->LuaCacheID){
                int scrtable=lua_gettop(L);
                lua_pushinteger(L,root->LuaCacheID); lua_gettable(L,2); lua_getfield(L,-1,"scripts");
                lua_pushnil(L); while(lua_next(L,-2)){
                    lua_pushvalue(L,-2); lua_pushvalue(L,-2); lua_settable(L,scrtable);
                    lua_pop(L,1);
                }
                lua_pop(L,2);
            }else{
                for(laRackPage*rp=root->Drivers->Pages.pFirst;rp;rp=rp->Item.pNext){
                    if(!(rp->UseScript&&rp->Script&&rp->Script->Ptr)){ continue; }
                    char* debug_name=rp->Name&&rp->Name->Ptr?rp->Name->Ptr:"unamed_driver_page";
                    lua_pushinteger(L,lua_objlen(L,-1)+1);
                    if(!luaL_loadbuffer(L,rp->Script->Ptr,strlen(rp->Script->Ptr),debug_name)){
                        if(lua_pcall(L,0,1,0)){ lua_getglobal(L,"log"); lua_insert(L,-2); if(lua_pcall(L,1,0,0)){ lua_pop(L,1); } }
                        else{ lua_settable(L,-3); }
                    }else{ lua_getglobal(L,"log"); lua_insert(L,-2); if(lua_pcall(L,1,0,0)){ lua_pop(L,1); } lua_pop(L,1); /* pop index */ }
                }
                root->LuaCacheID=en->LuaID;
            }
        }
        en->LuaLoaded=1;
    }
    
    //lua_getglobal(L,"ddump"); lua_getglobal(L,"scene"); lua_call(L,1,0);

    lua_pushnil(L); while(lua_next(L,-2)){
        lua_getfield(L,-1,func); if((!lua_isnil(L,-1))&&lua_isfunction(L,-1)){
            lua_pushvalue(L,-2); if(lua_pcall(L,1,0,0)){ lua_getglobal(L,"log"); lua_insert(L,-2); if(lua_pcall(L,1,0,0)){ lua_pop(L,1); } }
        }else{ lua_pop(L,1); }
        lua_pop(L,1);
    }
    lua_pop(L,3);
}
int tnsLuaRunNodeEvent(lua_State* L, tnsEvaluatedNode* en, tnsObject* object, laEvent* e){ lua_settop(L,0); int index=en->LuaID;
    if(!en->LuaLoaded){ return 0; }
    lua_getglobal(L,"scene"); lua_getfield(L,1,"nodes");
    lua_pushinteger(L,index); lua_gettable(L,-2); lua_setfield(L,1,"node");
    lua_getfield(L,1,"node"); tnsLuaSetObject(L,object);
    lua_getfield(L,-1,"scripts");
    if(lua_isnil(L,-1) || (!lua_istable(L,-1))){ return 0; }

    lua_pushnil(L); while(lua_next(L,-2)){
        lua_getfield(L,-1,"input"); if((!lua_isnil(L,-1))&&lua_isfunction(L,-1)){
            lua_pushvalue(L,-2); tnsLuaPushTypedPointer(L,"laEvent*",e);
            if(lua_pcall(L,2,1,0)){ lua_getglobal(L,"log"); lua_insert(L,-2); if(lua_pcall(L,1,0,0)){ lua_pop(L,1); } lua_pop(L,1); continue; }
            int result = lua_tonumber(L,-1); lua_pop(L,1);
            if (result!=0){ lua_settop(L,0); return 1; }
        }else{ lua_pop(L,1); }
        lua_pop(L,1);
    }
    lua_pop(L,3);
    return 1;
}

#endif //luajit

void tnsAddEvaluatedInstance(tnsEvaluateData* ed, tnsObject* ob, tnsDrawEvaluatedInstanceF Draw, int Layer, 
    int IsActive, int MeshSelectionType, int InstanceSelectionID){
    tnsEvaluatedInstance* ei;
    if(Layer==TNS_EVAL_LAYER_SOLID){
        arrEnsureLength(&ed->Commands,ed->NextCommand,&ed->MaxCommand,sizeof(tnsEvaluatedInstance));
        ei=&ed->Commands[ed->NextCommand]; ed->NextCommand++;
    }elif(Layer==TNS_EVAL_LAYER_BACKDROP){
        arrEnsureLength(&ed->Backdrop,ed->NextBackdrop,&ed->MaxBackdrop,sizeof(tnsEvaluatedInstance));
        ei=&ed->Backdrop[ed->NextBackdrop]; ed->NextBackdrop++;
    }elif(Layer==TNS_EVAL_LAYER_OUTLINE){
        arrEnsureLength(&ed->Outlines,ed->NextOutline,&ed->MaxOutline,sizeof(tnsEvaluatedInstance));
        ei=&ed->Outlines[ed->NextOutline]; ed->NextOutline++;
    }elif(Layer==TNS_EVAL_LAYER_OVERLAY){
        arrEnsureLength(&ed->Overlays,ed->NextOverlay,&ed->MaxOverlay,sizeof(tnsEvaluatedInstance));
        ei=&ed->Overlays[ed->NextOverlay]; ed->NextOverlay++;
    }elif(Layer==TNS_EVAL_LAYER_SELECTION){
        arrEnsureLength(&ed->Selections,ed->NextSelection,&ed->MaxSelection,sizeof(tnsEvaluatedInstance));
        ei=&ed->Selections[ed->NextSelection]; ed->NextSelection++;
    }else{ return; }
    ei->IsActive=IsActive; ei->MeshSelectionType=MeshSelectionType;
    ei->InstanceSelectionID=ed->OverrideID?ed->OverrideID:InstanceSelectionID;
    ei->Draw=Draw; ei->Object=ob; ei->DrawnIn2D=ed->State2D; if(ed->State2D){ ed->Has2D=1; }
    tnsMultiply44d(ei->Mat,ed->MatArr[ed->NextMat-1],ob->GlobalTransform);
}
void tnsFreeEvaluatedArray(tnsEvaluateData* ed){
    ed->Done=0;
    arrFree(&ed->Backdrop,&ed->MaxBackdrop);
    arrFree(&ed->Commands,&ed->MaxCommand);
    arrFree(&ed->Outlines,&ed->MaxOutline);
    arrFree(&ed->Overlays,&ed->MaxOverlay);
    arrFree(&ed->Selections,&ed->MaxSelection);
}
void tnsInvalidateEvaluation(tnsObject* o){ if(!o) return;
    if(o->InRoot) o->InRoot->Evaluated.Done=0; else o->Evaluated.Done=0;
}
void tnsInvalidatePlayEvaluation(tnsObject* o){
    if(o->InRoot) o->InRoot->EvaluatedPlay.Done=0; else o->EvaluatedPlay.Done=0;
}
void tnsSetObjectTreeEvaluationArgs(tnsObject* from, tnsObject* Active, int FillOutline, int FillSelectionID, int W, int H){
    tnsEvaluateData* ed=&from->Evaluated; int set=0;
#define SETARG(a)\
    if(ed->a==0 && a!=0){ set=1; } ed->a=a;
    SETARG(Active); SETARG(FillOutline); SETARG(FillSelectionID); SETARG(W); SETARG(H);
    if(set) ed->Done=0;
}
tnsEvaluatedNode* tnsAcquireEvaluateNode(tnsEvaluateData* ed){
    if(!ed->Scene->WastedEvaluateNodes.pFirst){ tnsEvaluatedNode* en=memAcquire(sizeof(tnsEvaluatedNode));
        ed->Scene->NextLuaID++; en->LuaID=ed->Scene->NextLuaID; return en; }
    else{ return lstPopItem(&ed->Scene->WastedEvaluateNodes); }
}
void tnsEvaluateNewNode(tnsEvaluateData* ed, tnsObject* ob){
    tnsEvaluatedScene* s=ed->Scene;
    tnsEvaluatedNode* en=tnsAcquireEvaluateNode(ed); en->Target=ob;
    if(ob->Type==TNS_OBJECT_ROOT){ tnsRootObject* ro=ob;
        en->ActionRetarget=laAnimationCopyRetargetFrom(ro->ActionRetarget); en->ActionRetarget->DetachedInNode = 1;
    }
    tnsMultiply44d(en->Mat,ed->MatArr[ed->NextMat-1],ob->GlobalTransform);
    if(s->CurrentChild) lstInsertItemBefore(&s->CurrentParent->Children,en,s->CurrentChild);
    else lstAppendItem(&s->CurrentParent->Children,en);
    s->CurrentChild = en;
}
int tnsEvaluateTryRelinkExistingNode(tnsEvaluateData* ed, tnsObject* ob){
    tnsEvaluatedScene* s=ed->Scene;
    if(!s->CurrentChild) return 0;
    int done=0; for(tnsEvaluatedNode* en=s->CurrentChild->Item.pNext;en;en=en->Item.pNext){
        if(ob==en->Target){ done=1; lstRemoveItem(&s->CurrentParent->Children,en);
            lstInsertItemBefore(&s->CurrentParent->Children,en,s->CurrentChild); break; }
    }
    return done;
}
void tnsEvaluateEndNode(tnsEvaluateData* ed, tnsEvaluatedNode* en){
    tnsEvaluatedNode* sen;
    while(sen=lstPopItem(&en->Children)){ tnsEvaluateEndNode(ed,sen); }
    if(en->Target->Type==TNS_OBJECT_ROOT){ tnsRootObject* ro=en->Target;
        laAnimationClearRetarget(&en->ActionRetarget);
    }
    int luaid=en->LuaID; memset(en,0,sizeof(tnsEvaluatedNode)); //memFree(sen);
    en->LuaID=luaid;
#ifdef LA_WITH_LUAJIT
    if(en->LuaID!=0) tnsLuaRemoveNode(ed->L,en->LuaID);
#endif
    lstAppendItem(&ed->Scene->WastedEvaluateNodes,en);
}
void tnsEvaluateSyncNode(tnsEvaluateData* ed, tnsObject* ob){
    tnsEvaluatedScene* s=ed->Scene;
    if(ob && ((!s->CurrentChild) || (s->CurrentChild->Target!=ob))){
        if(!tnsEvaluateTryRelinkExistingNode(ed,ob)) tnsEvaluateNewNode(ed,ob);
    }
    if((!ob) && s->CurrentChild){ tnsEvaluatedNode* en=s->CurrentChild->Item.pPrev;
        if(en) while(en->Item.pNext){ tnsEvaluatedNode* den=en->Item.pNext;
            lstRemoveItem(&s->CurrentParent->Children,den); tnsEvaluateEndNode(ed,den); }
        s->CurrentChild=0;
    }
}
void tnsEnsureEvaluatedScene(tnsEvaluateData* ed, tnsObject* root){
    if(!ed->Scene){
        ed->Scene=memAcquire(sizeof(tnsEvaluatedScene));
#ifdef LA_WITH_LUAJIT
        logPrintNew("Starting Lua for evaluated scene.\n");
        ed->L = lua_open(); la_luaLoadLibs(ed->L);
        la_luaPrintStatus(ed->L); tnsLuaInit(ed->L);
#else
        logPrintNew("Starting evaluated scene without lua.\n");
#endif
    } 
    if(!ed->Scene->Root){ 
        ed->Scene->Root=tnsAcquireEvaluateNode(ed);
        ed->Scene->NextLuaID++; ed->Scene->Root->LuaID=ed->Scene->NextLuaID;
        if(root->Type==TNS_OBJECT_ROOT){ tnsRootObject* ro=root;
            ed->Scene->Root->ActionRetarget=laAnimationCopyRetargetFrom(ro->ActionRetarget);
        }
    }
    ed->Scene->CurrentChild=ed->Scene->Root;
    ed->Scene->CurrentChild->Target=root; tnsLoadIdentity44d(ed->Scene->CurrentChild->Mat);
}
void tnsPrintEvaluatedNode(tnsEvaluatedNode* en,int level){
    for(int i=0;i<level;i++){ printf("  "); }
    printf("%s\n",en->Target->Name?en->Target->Name->Ptr:"?");
    for(tnsEvaluatedNode* sen=en->Children.pFirst;sen;sen=sen->Item.pNext){
        tnsPrintEvaluatedNode(sen,level+1);
    }
    if(level==0){printf("\n");}
}
void tnsEvaluateObjectTree(tnsObject* from, tnsEvaluateData* UseED, int EvaluatePlay){
    if(!from) return;
    tnsObject* UseRoot=from->InRoot?from->InRoot:from;
    tnsEvaluateData* ed=UseED?UseED:(EvaluatePlay?&UseRoot->EvaluatedPlay:&UseRoot->Evaluated);
    ed->SceneEvaluateMode=EvaluatePlay;
    if(ed->Done) return;
    elif(!UseED){ ed->NextCommand=ed->NextOverlay=ed->NextSelection=ed->NextOutline=ed->NextMat=0;
        if(!ed->Backdrop) arrInitLength(&ed->Backdrop,16,&ed->MaxBackdrop,sizeof(tnsEvaluatedInstance));
        if(!ed->Commands) arrInitLength(&ed->Commands,16,&ed->MaxCommand,sizeof(tnsEvaluatedInstance));
        if(!ed->Outlines) arrInitLength(&ed->Outlines,16,&ed->MaxOutline,sizeof(tnsEvaluatedInstance));
        if(!ed->Overlays) arrInitLength(&ed->Overlays,16,&ed->MaxOverlay,sizeof(tnsEvaluatedInstance));
        if(!ed->Selections) arrInitLength(&ed->Selections,16,&ed->MaxSelection,sizeof(tnsEvaluatedInstance));
        if(!ed->MatArr) arrInitLength(&ed->MatArr,8,&ed->MaxMat,sizeof(tnsMatrix44d));
        tnsLoadIdentity44d(ed->MatArr[0]); ed->NextMat=1; ed->Has2D=0;
    }
    tnsEvaluatedNode* CP,*CC;
    if(ed->SceneEvaluateMode){
        from=tnsEnsurePlayDuplicate(from);
        if(!UseED){ tnsEnsureEvaluatedScene(ed,from); }
        CP=ed->Scene->CurrentParent; CC=ed->Scene->CurrentChild;
    }
    lstPushPointer(&ed->ParentStack,from);

    tnsEvaluateThisObject(from, ed);
    
    if(ed->SceneEvaluateMode){ ed->Scene->CurrentParent=ed->Scene->CurrentChild; ed->Scene->CurrentChild=ed->Scene->CurrentParent->Children.pFirst; }

    for (laListItemPointer* lip=from->ChildObjects.pFirst;lip;lip=lip->pNext){
        tnsObject *o=lip->p; if (o){ tnsEvaluateObjectTree(o,ed,EvaluatePlay); }
        if(ed->SceneEvaluateMode){ ed->Scene->CurrentChild=ed->Scene->CurrentChild?ed->Scene->CurrentChild->Item.pNext:0; }
    }

    lstPopPointer(&ed->ParentStack);
    if(ed->SceneEvaluateMode){ ed->Scene->CurrentParent=CP; ed->Scene->CurrentChild=CC; }
    if(!UseED){ ed->Done=1;
        //if(ed->Scene){ tnsPrintEvaluatedNode(ed->Scene->Root,0);}
    }
}
int tnsEvaluateObjectTreeEvent(tnsEvaluateData* ed_play, tnsEvaluatedNode* en, laEvent* e){
    int stop=0;
    for(tnsEvaluatedNode* lip=en->Children.pFirst;lip;lip=lip->Item.pNext){
        if(tnsEvaluateObjectTreeEvent(ed_play, lip, e)){ stop=1; }
    }
    if(en->Target->Type==TNS_OBJECT_ROOT){
        if(e->type&LA_KEYBOARD_EVENT){
            tnsRunNode(en,TNS_PAGE_TRIGGER_KEYBOARD,en->Target);
        }elif(e->type&LA_MOUSE_EVENT){
            tnsRunNode(en,TNS_PAGE_TRIGGER_MOUSE,en->Target);
        }
#ifdef LA_WITH_LUAJIT
        if(!stop){ return tnsLuaRunNodeEvent(ed_play->L,en,en->Target,e); }
#endif
    }
    return 0;
}
void tnsDrawLayer(tnsEvaluateData* ed,int Layer,void* CustomData){
    tnsEvaluatedInstance* ei; int next=0;
    int No2D=Layer&TNS_EVAL_LAYER_NO_2D; int Do2D=Layer&TNS_EVAL_LAYER_SHOW_2D;
    Layer&=(~(TNS_EVAL_LAYER_SHOW_2D|TNS_EVAL_LAYER_NO_2D));
    if(Layer==TNS_EVAL_LAYER_SOLID){ ei=ed->Commands; next=ed->NextCommand; }
    elif(Layer==TNS_EVAL_LAYER_BACKDROP){ ei=ed->Backdrop; next=ed->NextBackdrop; }
    elif(Layer==TNS_EVAL_LAYER_OUTLINE){ ei=ed->Outlines; next=ed->NextOutline; }
    elif(Layer==TNS_EVAL_LAYER_OVERLAY){ ei=ed->Overlays; next=ed->NextOverlay; }
    elif(Layer==TNS_EVAL_LAYER_SELECTION){ ei=ed->Selections; next=ed->NextSelection; }
    else{ return; } if(!next){ return; }
    for(int i=0;i<next;i++){
        if(No2D && ei->Object->Type==TNS_OBJECT_SHAPE){ ei++; continue; }
        if((Do2D && (!ei->DrawnIn2D)) || ((!Do2D) && ei->DrawnIn2D)){ ei++; continue; }
        tnsPushMatrix(); tnsApplyModelMatrix(ei->Mat);
        ei->Draw(ei,CustomData);
        tnsPopMatrix();
        ei++;
    }
}
void tnsDrawObjectTree(tnsObject* from, int Layers,void* CustomData, int DrawRuntime){
    if(!from) return;
    tnsEvaluateData* ed=DrawRuntime?&from->EvaluatedPlay:&from->Evaluated;
    if(!ed->Done) return; int Do2D=Layers&TNS_EVAL_LAYER_SHOW_2D;
    if(Layers&TNS_EVAL_LAYER_BACKDROP){ tnsDrawLayer(ed,TNS_EVAL_LAYER_BACKDROP|Do2D,CustomData); }
    if(Layers&TNS_EVAL_LAYER_SOLID){ tnsDrawLayer(ed,TNS_EVAL_LAYER_SOLID|Do2D,CustomData); }
    if(Layers&TNS_EVAL_LAYER_OUTLINE){ tnsDrawLayer(ed,TNS_EVAL_LAYER_OUTLINE|Do2D,CustomData);}
    if(Layers&TNS_EVAL_LAYER_OVERLAY){ tnsDrawLayer(ed,TNS_EVAL_LAYER_OVERLAY|Do2D,CustomData);}
    if(Layers&TNS_EVAL_LAYER_SELECTION){ tnsDrawLayer(ed,TNS_EVAL_LAYER_SELECTION|Do2D,CustomData); }
}

void tnsDrawSingleObjectOrigin(tnsObject* o){
    real* c=laThemeColor(_LA_THEME_3D_VIEW,LA_BT_BORDER); tnsColor4d(LA_COLOR3(c),0.5);
    tnsPointSize(8); tnsVertex3d(0,0,0); tnsPackAs(GL_POINTS); 
    tnsFlush(); tnsPointSize(1);
}
void tnsDrawObjectOrigins(tnsObject *from, tnsObject *active, int AllOrigins){
    tnsObject *o; if(!from) return;
    tnsVector4d pos;
    for (laListItemPointer* lip=from->ChildObjects.pFirst;lip;lip=lip->pNext){
        o=lip->p; if((!AllOrigins)){
            if((o!=active) && (!(o->Flags&TNS_OBJECT_FLAGS_SELECTED))) continue;
            if(o->Type==TNS_OBJECT_MESH && ((tnsMeshObject*)o)->Mode==TNS_MESH_EDIT_MODE){ continue; }
        }
        if(T->BindedShader==T->SelectionShader){  int i=o->SelectID; real color[4]={0,0,0,1}; TNS_ID_TO_COLOR(color,i); tnsColor4dv(color); }
        else{
            if(o==active){ tnsColor4dv(laAccentColor(LA_BT_TEXT_ACTIVE)); }
            elif(o->Flags&TNS_OBJECT_FLAGS_SELECTED){ tnsColor4dv(laAccentColor(LA_BT_NORMAL)); }
            else{ real* c=laThemeColor(_LA_THEME_3D_VIEW,LA_BT_BORDER); tnsColor4d(LA_COLOR3(c),0.5); }
        }
        tnsVertex3d(LA_COLOR3(o->GLocation)); tnsPackAs(GL_POINTS);
    }
}
void tnsDrawCursor(tnsObject* root){
    if(!root || root->Type!=TNS_OBJECT_ROOT) return;
    tnsMatrix44d vp; tnsVector4d t; tnsVector4d t1={0,0,0,1};
    tnsMultiply44d(vp,tnsGetProjectionMatrix(),tnsGetViewMatrix());
    tnsApplyTransform44d(t,vp,root->GLocation); real w=t[3]*0.05;
    tnsDrawCross(LA_COLOR3(root->GLocation),-w,w,-w,w,-w,w); tnsColor4d(0,0,0,0.5);
    tnsPackAs(GL_LINES); glLineWidth(5);
    tnsFlush(); w*=0.9;
    tnsDrawCross(LA_COLOR3(root->GLocation),-w,w,-w,w,-w,w); tnsColor4dv(laThemeColor(_LA_THEME_3D_VIEW,LA_BT_BORDER));
    tnsPackAs(GL_LINES); glLineWidth(2);
    tnsFlush(); glLineWidth(1);
}

//===================================================================[Material]

tnsMaterial *tnsCreateMaterial(char *Name){
    tnsMaterial *m; if(!Name || !Name[0]) return 0;
    m = memAcquireHyper(sizeof(tnsMaterial));
    strSafeSet(&m->Name, Name); tnsVectorSet4(m->Color,0.8,0.8,0.8,1); tnsVectorSet4(m->Color2,0.8,0.8,0.8,1);
    m->Page=memAcquire(sizeof(laRackPage));
    lstAppendItem(&T->World->Materials, m);
    return m;
}
tnsMaterial *tnsFindMaterial(char *name){
    tnsMaterial *m;
    for (m = T->World->Materials.pFirst; m; m = m->Item.pNext){ if(strSame(m->Name->Ptr, name)) return m; }
    return 0;
}
void tnsRemoveMaterial(tnsMaterial* mat){
    lstRemoveItem(&T->World->Materials,mat);
    laNodeRack* rr; while(rr=lstPopItem(&mat->Page->Racks)){ laDestroyRack(rr); }
    strSafeDestroy(&mat->Page->Name); memLeave(mat->Page);
    for(tnsObject* o=T->World->AllObjects.pFirst;o;o=o->Item.pNext){
        if(o->Type==TNS_OBJECT_MESH){ tnsMeshObject* mo=o;
            for(tnsMaterialSlot* msi=mo->Materials.pFirst;msi;msi=msi->Item.pNext){
                if(msi->Material==mat){ memAssignRef(msi,&msi->Material,0); }
            }
        }elif(o->Type==TNS_OBJECT_SHAPE){ tnsShapeObject* so=o;
            for(tnsMaterialSlot* msi=so->Materials.pFirst;msi;msi=msi->Item.pNext){
                if(msi->Material==mat){ memAssignRef(msi,&msi->Material,0); }
            }
        }
    }
    tnsRefreshMaterialLibraries();
    memLeave(mat);
}
tnsMaterialSlot* tnsNewMaterialSlot(tnsObject* o){
    if(o->Type==TNS_OBJECT_MESH){ tnsMeshObject* mo=o; short nextid=0,found=1;
        while(found){ found=0;
            for(tnsMaterialSlot* msi=mo->Materials.pFirst;msi;msi=msi->Item.pNext){
                if(nextid==msi->Index){ found=1; nextid++; continue; }
            }
        }
        tnsMaterialSlot* ms=memAcquire(sizeof(tnsMaterialSlot)); lstAppendItem(&mo->Materials,ms);
        ms->Index=nextid; memAssignRef(ms,&ms->Parent,mo); memAssignRef(mo,&mo->CurrentMaterial,ms);
        return ms;
    }elif(o->Type==TNS_OBJECT_SHAPE){ tnsShapeObject* so=o; short nextid=0,found=1;
        while(found){ found=0;
            for(tnsMaterialSlot* msi=so->Materials.pFirst;msi;msi=msi->Item.pNext){
                if(nextid==msi->Index){ found=1; nextid++; continue; }
            }
        }
        tnsMaterialSlot* ms=memAcquire(sizeof(tnsMaterialSlot)); lstAppendItem(&so->Materials,ms);
        ms->Index=nextid; memAssignRef(ms,&ms->Parent,so); memAssignRef(so,&so->CurrentMaterial,ms);
        return ms;
    }
    return 0;
}
void tnsRemoveMaterialSlot(tnsObject* o, tnsMaterialSlot* ms){
    if(o->Type==TNS_OBJECT_MESH){ tnsMeshObject* mo=o;
        memAssignRef(mo,&mo->CurrentMaterial,ms->Item.pNext?ms->Item.pNext:ms->Item.pPrev);
        lstRemoveItem(&mo->Materials,ms); memLeave(ms);
    }elif(o->Type==TNS_OBJECT_SHAPE){ tnsShapeObject* so=o;
        memAssignRef(so,&so->CurrentMaterial,ms->Item.pNext?ms->Item.pNext:ms->Item.pPrev);
        lstRemoveItem(&so->Materials,ms); memLeave(ms);
    }
}
void tnsAssignMaterialSlot(tnsObject* o, tnsMaterialSlot* ms){
    if(o->Type==TNS_OBJECT_MESH){ tnsMeshObject* mo=o;
        if(mo->Mode==TNS_MESH_OBJECT_MODE){
            for(int i=0;i<mo->totf;i++){ mo->f[i].mat=ms->Index; }
        }elif(mo->Mode==TNS_MESH_EDIT_MODE){
            for(tnsMFace* f=mo->mf.pFirst;f;f=f->Item.pNext){ if(f->flags&TNS_MESH_FLAG_SELECTED) f->mat=ms->Index; }
        }
    }elif(o->Type==TNS_OBJECT_SHAPE){ tnsShapeObject* so=o;
        if(so->Mode==TNS_MESH_OBJECT_MODE){
            for(tnsShape*s=so->Shapes.pFirst;s;s=s->Item.pNext){ s->mat=ms->Index; }
        }elif(so->Mode==TNS_MESH_EDIT_MODE){
            for(tnsShape*s=so->Shapes.pFirst;s;s=s->Item.pNext){ if(tnsShapePointAnySelected(s)) s->mat=ms->Index; }
        }
    }
}
void tns_InvalidateMeshWithMaterial(tnsMaterial* m);
void tnsRefreshMaterialLibraries(){
    strSafeDestroy(&T->World->MaterialLibraries);
    for(tnsMaterial* mat=T->World->Materials.pFirst;mat;mat=mat->Item.pNext){
        if(!mat->AsLibrary || !mat->Page->UseScript) continue;
        if(mat->Page->Script) strSafeAppend(&T->World->MaterialLibraries,mat->Page->Script->Ptr);
        tnsEnsureMaterialShader(mat,1);
    }
    for(tnsMaterial* mat=T->World->Materials.pFirst;mat;mat=mat->Item.pNext){
        if(mat->AsLibrary || !mat->Page->UseScript) continue;
        tnsEnsureMaterialShader(mat,1); tns_InvalidateMeshWithMaterial(mat);
    }
    laNotifyUsers("tns.world");
}
void tnsEnsureMaterialShader(tnsMaterial* mat, int Refresh){
    if(Refresh){
        if(mat->Shader && mat->Shader!=T->immShader){ tnsDeleteShaderProgram(mat->Shader); mat->Shader=0; }
    }
    if((!(mat->Page->Script&&mat->Page->Script->Ptr))||mat->AsLibrary){ mat->Shader=T->immShader; return; }
    char* str=mat->Page->Script->Ptr;
    char* lib=T->World->MaterialLibraries?T->World->MaterialLibraries->Ptr:0;
    mat->Shader = tnsNewShaderProgram(
        tnsNewVertexShader(LA_IMM_VERTEX_SHADER),tnsNewFragmentShaderMaterial(LA_OBJECT_FRAGMENT_SHADER,lib,str),-1);
    if(!mat->Shader){ mat->Shader=T->immShader; }
}

void tnsPrintMaterials(){
    for(tnsMaterial* m=T->World->Materials.pFirst;m;m=m->Item.pNext){
        printf("%s %d %d\n",(m->Name&&m->Name->Ptr?m->Name->Ptr:""),m,m->Page->Racks.pFirst);
    }
}

//==================================================================[Util]

#define MAX3(a, b, c) \
    a > b ? (a > c ? a : c) : (b > c ? b : c)

#define MIN3(a, b, c) \
    a < b ? (a < c ? a : c) : (b < c ? b : c)

extern LA MAIN;

void tnssRGB2XYZ(tnsVector3d rgb,tnsVector3d xyz){
	tnsMatrix44d mat={0.4124564,0.3575761,0.1804375,
				      0.2126729,0.7151522,0.0721750,
				      0.0193339,0.1191920,0.9503041};
    tnsApplyRotation33d(xyz,mat,rgb);
}
void tnsClay2XYZ(tnsVector3d rgb,tnsVector3d xyz){
	tnsMatrix44d mat={0.5767309,0.1855540,0.1881852,
				      0.2973769,0.6273491,0.0752741,
				      0.0270343,0.0706872,0.9911085};
    tnsApplyRotation33d(xyz,mat,rgb);
}
void tnsXYZ2sRGB(tnsVector3d xyz,tnsVector3d rgb){
	tnsMatrix44d mat={3.2404542,-1.5371385,-0.4985314,
				      -0.9692660,1.8760108,0.0415560,
				      0.0556434,-0.2040259,1.0572252};
    tnsApplyRotation33d(rgb,mat,xyz);
}
void tnsXYZ2Clay(tnsVector3d xyz,tnsVector3d rgb){
	tnsMatrix44d mat={2.0413690,-0.5649464,-0.3446944,
                      -0.9692660,1.8760108,0.0415560,
                      0.0134474,-0.1183897,1.0154096};
    tnsApplyRotation33d(rgb,mat,xyz);
}
void tnsRGB2Clay(tnsVector3d rgb, tnsVector3d clay){
    tnsVector3d xyz; tnssRGB2XYZ(rgb,xyz);
    tnsXYZ2Clay(xyz,clay);
}
void tnsClay2RGB(tnsVector3d clay, tnsVector3d rgb){
    tnsVector3d xyz; tnsClay2XYZ(clay,xyz);
    tnsXYZ2sRGB(xyz,rgb);
}

static real _srgb_transfer_function(real a){
	return .0031308f >= a ? 12.92f * a : 1.055f * powf(a, .4166666666666667f) - .055f;
}
static real _srgb_transfer_function_inv(real a){
	return .04045f < a ? powf((a + .055f) / 1.055f, 2.4f) : a / 12.92f;
}
void tns2LogsRGBSingle(real* a){
    *a=_srgb_transfer_function(*a);
}
void tns2LinearsRGBSingle(real* a){
    *a=_srgb_transfer_function_inv(*a);
}
void tns2LogsRGB(real* srgb){
    srgb[0]=_srgb_transfer_function(srgb[0]);
    srgb[1]=_srgb_transfer_function(srgb[1]);
    srgb[2]=_srgb_transfer_function(srgb[2]);
}
void tns2LinearsRGB(real* srgb){
    srgb[0]=_srgb_transfer_function_inv(srgb[0]);
    srgb[1]=_srgb_transfer_function_inv(srgb[1]);
    srgb[2]=_srgb_transfer_function_inv(srgb[2]);
}
void tnsRGB2OKLAB(real* rgb, real* oklab){
    real l = 0.4122214708f * rgb[0] + 0.5363325363f * rgb[1] + 0.0514459929f * rgb[2];
	real m = 0.2119034982f * rgb[0] + 0.6806995451f * rgb[1] + 0.1073969566f * rgb[2];
	real s = 0.0883024619f * rgb[0] + 0.2817188376f * rgb[1] + 0.6299787005f * rgb[2];

    real l_ = cbrt(l); real m_ = cbrt(m); real s_ = cbrt(s);

    oklab[0]=0.2104542553f*l_ + 0.7936177850f*m_ - 0.0040720468f*s_;
    oklab[1]=1.9779984951f*l_ - 2.4285922050f*m_ + 0.4505937099f*s_;
    oklab[2]=0.0259040371f*l_ + 0.7827717662f*m_ - 0.8086757660f*s_;
}
void tnsOKLAB2RGB(real* oklab, real* rgb){
    real l_ = oklab[0] + 0.3963377774f * oklab[1] + 0.2158037573f * oklab[2];
    real m_ = oklab[0] - 0.1055613458f * oklab[1] - 0.0638541728f * oklab[2];
    real s_ = oklab[0] - 0.0894841775f * oklab[1] - 1.2914855480f * oklab[2];

    real l = l_*l_*l_; real m = m_*m_*m_; real s = s_*s_*s_;

    rgb[0]=+4.0767416621f * l - 3.3077115913f * m + 0.2309699292f * s;
    rgb[1]=-1.2684380046f * l + 2.6097574011f * m - 0.3413193965f * s;
    rgb[2]=-0.0041960863f * l - 0.7034186147f * m + 1.7076147010f * s;
}
static real _compute_max_saturation(real a, real b){
	real k0, k1, k2, k3, k4, wl, wm, ws;
	if (-1.88170328f * a - 0.80936493f * b > 1){
		k0 = +1.19086277f; k1 = +1.76576728f; k2 = +0.59662641f; k3 = +0.75515197f; k4 = +0.56771245f;
		wl = +4.0767416621f; wm = -3.3077115913f; ws = +0.2309699292f;
	}
	else if (1.81444104f * a - 1.19445276f * b > 1){
		k0 = +0.73956515f; k1 = -0.45954404f; k2 = +0.08285427f; k3 = +0.12541070f; k4 = +0.14503204f;
		wl = -1.2684380046f; wm = +2.6097574011f; ws = -0.3413193965f;
	}
	else{
		k0 = +1.35733652f; k1 = -0.00915799f; k2 = -1.15130210f; k3 = -0.50559606f; k4 = +0.00692167f;
		wl = -0.0041960863f; wm = -0.7034186147f; ws = +1.7076147010f;
	}
	real S = k0 + k1 * a + k2 * b + k3 * a * a + k4 * a * b;
	real k_l = +0.3963377774f * a + 0.2158037573f * b;
	real k_m = -0.1055613458f * a - 0.0638541728f * b;
	real k_s = -0.0894841775f * a - 1.2914855480f * b;
	{
		real l_ = 1.f + S * k_l;
		real m_ = 1.f + S * k_m;
		real s_ = 1.f + S * k_s;

		real l = l_ * l_ * l_;
		real m = m_ * m_ * m_;
		real s = s_ * s_ * s_;

		real l_dS = 3.f * k_l * l_ * l_;
		real m_dS = 3.f * k_m * m_ * m_;
		real s_dS = 3.f * k_s * s_ * s_;

		real l_dS2 = 6.f * k_l * k_l * l_;
		real m_dS2 = 6.f * k_m * k_m * m_;
		real s_dS2 = 6.f * k_s * k_s * s_;

		real f = wl * l + wm * m + ws * s;
		real f1 = wl * l_dS + wm * m_dS + ws * s_dS;
		real f2 = wl * l_dS2 + wm * m_dS2 + ws * s_dS2;

		S = S - f * f1 / (f1 * f1 - 0.5f * f * f2);
	}

	return S;
}
static void _find_cusp(real a, real b, real *l, real *c)
{
	real S_cusp = _compute_max_saturation(a, b);
    real oklab[3]={1, S_cusp * a, S_cusp * b}; real rgb_at_max[3];
    tnsOKLAB2RGB(oklab,rgb_at_max);
	real L_cusp = cbrt(1.f / TNS_MAX3(rgb_at_max[0], rgb_at_max[1], rgb_at_max[2])); 
	real C_cusp = L_cusp * S_cusp;
	*l=L_cusp; *c=C_cusp;
}
static real _find_gamut_intersection(real a, real b, real L1, real C1, real L0, real cusp_L, real cusp_C)
{
	real t;
	if (((L1 - L0) * cusp_C - (cusp_L - L0) * C1) <= 0.f) {
		t = cusp_C * L0 / (C1 * cusp_L + cusp_C * (L0 - L1));
	}else{
		t = cusp_C * (L0 - 1.f) / (C1 * (cusp_L - 1.f) + cusp_C * (L0 - L1));
		{
			real dL = L1 - L0; real dC = C1;

			real k_l = +0.3963377774f * a + 0.2158037573f * b;
			real k_m = -0.1055613458f * a - 0.0638541728f * b;
			real k_s = -0.0894841775f * a - 1.2914855480f * b;

			real l_dt = dL + dC * k_l;
			real m_dt = dL + dC * k_m;
			real s_dt = dL + dC * k_s;
			{
				real L = L0 * (1.f - t) + t * L1;
				real C = t * C1;

				real l_ = L + C * k_l;
				real m_ = L + C * k_m;
				real s_ = L + C * k_s;

				real l = l_ * l_ * l_;
				real m = m_ * m_ * m_;
				real s = s_ * s_ * s_;

				real ldt = 3 * l_dt * l_ * l_;
				real mdt = 3 * m_dt * m_ * m_;
				real sdt = 3 * s_dt * s_ * s_;

				real ldt2 = 6 * l_dt * l_dt * l_;
				real mdt2 = 6 * m_dt * m_dt * m_;
				real sdt2 = 6 * s_dt * s_dt * s_;

				real r = 4.0767416621f * l - 3.3077115913f * m + 0.2309699292f * s - 1;
				real r1 = 4.0767416621f * ldt - 3.3077115913f * mdt + 0.2309699292f * sdt;
				real r2 = 4.0767416621f * ldt2 - 3.3077115913f * mdt2 + 0.2309699292f * sdt2;

				real u_r = r1 / (r1 * r1 - 0.5f * r * r2);
				real t_r = -r * u_r;

				real g = -1.2684380046f * l + 2.6097574011f * m - 0.3413193965f * s - 1;
				real g1 = -1.2684380046f * ldt + 2.6097574011f * mdt - 0.3413193965f * sdt;
				real g2 = -1.2684380046f * ldt2 + 2.6097574011f * mdt2 - 0.3413193965f * sdt2;

				real u_g = g1 / (g1 * g1 - 0.5f * g * g2);
				real t_g = -g * u_g;

				real b = -0.0041960863f * l - 0.7034186147f * m + 1.7076147010f * s - 1;
				real b1 = -0.0041960863f * ldt - 0.7034186147f * mdt + 1.7076147010f * sdt;
				real b2 = -0.0041960863f * ldt2 - 0.7034186147f * mdt2 + 1.7076147010f * sdt2;

				real u_b = b1 / (b1 * b1 - 0.5f * b * b2);
				real t_b = -b * u_b;

				t_r = u_r >= 0.f ? t_r : FLT_MAX;
				t_g = u_g >= 0.f ? t_g : FLT_MAX;
				t_b = u_b >= 0.f ? t_b : FLT_MAX;

				t += fmin(t_r, fmin(t_g, t_b));
			}
		}
	}

	return t;
}
static real _toe(real x) {
	const real k_1 = 0.206f; const real k_2 = 0.03f; const real k_3 = (1.f + k_1) / (1.f + k_2);
	return 0.5f * (k_3 * x - k_1 + sqrt((k_3 * x - k_1) * (k_3 * x - k_1) + 4 * k_2 * k_3 * x));
}
static real _toe_inv(real x) {
	const real k_1 = 0.206f; const real k_2 = 0.03f; const real k_3 = (1.f + k_1) / (1.f + k_2);
	return (x * x + k_1 * x) / (k_3 * (x + k_2));
}
static void _to_ST(real cusp_L, real cusp_C, real* _s, real* _t) {
	real L = cusp_L; real C = cusp_C;
	*_s=C / L; *_t=C / (1 - L);
}
static void _get_ST_mid(real a_, real b_, real *s,real *t){
	*s = 0.11516993f + 1.f / (
		+7.44778970f + 4.15901240f * b_
		+ a_ * (-2.19557347f + 1.75198401f * b_
			+ a_ * (-2.13704948f - 10.02301043f * b_
				+ a_ * (-4.24894561f + 5.38770819f * b_ + 4.69891013f * a_
					)))
		);
	*t = 0.11239642f + 1.f / (
		+1.61320320f - 0.68124379f * b_
		+ a_ * (+0.40370612f + 0.90148123f * b_
			+ a_ * (-0.27087943f + 0.61223990f * b_
				+ a_ * (+0.00299215f - 0.45399568f * b_ - 0.14661872f * a_
					)))
		);
}
static void _get_Cs(real L, real a_, real b_,real *rC_0, real *rC_mid, real *rC_max){
	real cusp_L,cusp_C; _find_cusp(a_, b_,&cusp_L,&cusp_C);

	real C_max = _find_gamut_intersection(a_, b_, L, 1, L,cusp_L,cusp_C);
	real _S,_T; _to_ST(cusp_L,cusp_C,&_S, &_T);
	real k = C_max / fmin((L * _S), (1 - L) * _T);
	real C_mid;{
		real _s, _t; _get_ST_mid(a_, b_,&_s,&_t);
		real C_a = L * _s; real C_b = (1.f - L) * _t;
		C_mid = 0.9f * k * sqrt(sqrt(1.f / (1.f / (C_a * C_a * C_a * C_a) + 1.f / (C_b * C_b * C_b * C_b))));
	}

	real C_0;{
		real C_a = L * 0.4f;
		real C_b = (1.f - L) * 0.8f;
		C_0 = sqrt(1.f / (1.f / (C_a * C_a) + 1.f / (C_b * C_b)));
	}

	*rC_0=C_0; *rC_mid=C_mid; *rC_max=C_max;
}
void tnsHCY2RGBLinear(real *hcy, real *rgb){
	real h = hcy[0]; real s = hcy[1]; real l = hcy[2];

	if (l >= 1.0f){ tnsVectorSet3(rgb,1,1,1); return; }
	else if (l <= 0.0f){ tnsVectorSet3(rgb,0,0,0); return; }

	real a_ = cos(2.f * TNS_PI * h);
	real b_ = sin(2.f * TNS_PI * h);
	real L = _toe_inv(l);

	real C_0, C_mid, C_max;
    _get_Cs(L, a_, b_,&C_0,&C_mid,&C_max);

	real mid = 0.8f; real mid_inv = 1.25f;
	real C, t, k_0, k_1, k_2;

	if (s < mid){
		t = mid_inv * s;
		k_1 = mid * C_0;
		k_2 = (1.f - k_1 / C_mid);
		C = t * k_1 / (1.f - k_2 * t);
	}else{
		t = (s - mid)/ (1 - mid);
		k_0 = C_mid;
		k_1 = (1.f - mid) * C_mid * C_mid * mid_inv * mid_inv / C_0;
		k_2 = (1.f - (k_1) / (C_max - C_mid));
		C = k_0 + t * k_1 / (1.f - k_2 * t);
	}

    real okl[3]={L, C * a_, C * b_};
	tnsOKLAB2RGB(okl, rgb);
}
void tnsRGB2HCYLinear(real *rgb, real *hcy){
	real lab[3]; real lrgb[3];
    lrgb[0]=rgb[0]; lrgb[1]=rgb[1]; lrgb[2]=rgb[2];
    tnsRGB2OKLAB(lrgb,lab);

    if(lab[0]>=1.0f-1e-4){ tnsVectorSet3(hcy,0,0,1); return; }
    if(lab[0]<=1e-4){ tnsVectorSet3(hcy,0,0,0); return; }

	real C = sqrt(lab[1]*lab[1] + lab[2]*lab[2]);
	real a_ = (!C)?0:(lab[1] / C);
	real b_ = (!C)?0:(lab[2] / C);

	real L = lab[0];
	real h = 0.5f + 0.5f * atan2(-lab[2], -lab[1]) / TNS_PI;

	real C_0, C_mid, C_max;
    _get_Cs(L, a_, b_,&C_0,&C_mid,&C_max);

	real mid = 0.8f; real mid_inv = 1.25f;

	real s;
	if (C < C_mid){
		real k_1 = mid * C_0;
		real k_2 = (1.f - k_1 / C_mid);
		real t = C / (k_1 + k_2 * C);
		s = t * mid;
	}else{
		real k_0 = C_mid;
		real k_1 = (1.f - mid) * C_mid * C_mid * mid_inv * mid_inv / C_0;
		real k_2 = (1.f - (k_1) / (C_max - C_mid));
		real t = (C - k_0) / (k_1 + k_2 * (C - k_0));
		s = mid + (1.f - mid) * t;
	}

	real l = _toe(L);
	hcy[0]=h; hcy[1]=s; hcy[2]=l;
}
void tnsHCY2RGB(real *hcy, real *rgb){
    tnsHCY2RGBLinear(hcy,rgb);
    rgb[0]=_srgb_transfer_function(rgb[0]);
    rgb[1]=_srgb_transfer_function(rgb[1]);
    rgb[2]=_srgb_transfer_function(rgb[2]);
}
void tnsRGB2HCY(real *rgb, real *hcy){
	real lrgb[3];
    lrgb[0]=_srgb_transfer_function_inv(rgb[0]);
    lrgb[1]=_srgb_transfer_function_inv(rgb[1]);
    lrgb[2]=_srgb_transfer_function_inv(rgb[2]);
    tnsRGB2HCYLinear(lrgb,hcy);
}
static float hue2rgb(float p, float q, float t) {
  if (t < 0) t += 1; if (t > 1) t -= 1;
  if (t < 1./6) return p + (q - p) * 6 * t;
  if (t < 1./2) return q;
  if (t < 2./3) return p + (q - p) * (2./3 - t) * 6;
  return p;
}
void tnsRGB2HSL(real *rgb, real *hsl) {
    float max = TNS_MAX3(rgb[0],rgb[1],rgb[2]);
    float min = TNS_MIN3(rgb[0],rgb[1],rgb[2]);
    hsl[0]=hsl[1]=hsl[2]=(max+min) / 2;
    if (max == min) { hsl[0]=hsl[1]=0; }
    else {
        float d = max - min;
        hsl[1] = (hsl[2] > 0.5) ? d / (2 - max - min) : d / (max + min);
        if (max == rgb[0]) { hsl[0] = (rgb[1] - rgb[2]) / d + (rgb[1] < rgb[2] ? 6 : 0); }
        else if (max == rgb[1]) { hsl[0] = (rgb[2] - rgb[0]) / d + 2; }
        else if (max == rgb[2]) { hsl[0] = (rgb[0] - rgb[1]) / d + 4; }
        hsl[0] /= 6;
    }
}
void tnsHSL2RGB(real *hsl, real *rgb) {
  if(hsl[1]==0) {rgb[0]=rgb[1]=rgb[2]=hsl[2];}
  else {
    float q = hsl[2] < 0.5 ? hsl[2] * (1 + hsl[1]) : hsl[2] + hsl[1] - hsl[2] * hsl[1];
    float p = 2 * hsl[2] - q;
    rgb[0] = hue2rgb(p, q, hsl[0] + 1./3);
    rgb[1] = hue2rgb(p, q, hsl[0]);
    rgb[2] = hue2rgb(p, q, hsl[0] - 1./3);
  }
}
// https://web.archive.org/web/20180423091842/http://www.equasys.de/colorconversion.html
void tnsYUV2RGB(real* yuv,real* rgb){
    yuv[0]=1.1643*(yuv[0]-0.0625);
    yuv[1]-=0.5; yuv[2]-=0.5;
    rgb[0] = (yuv[0] + 1.793 * yuv[2]);
    rgb[1] = (yuv[0] - 0.213 * yuv[1] - 0.533 * yuv[2]);
    rgb[2] = (yuv[0] + 2.112 * yuv[1]);
}

void tnsClearAll(){
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
}
void tnsClearColorv(real *rgba){
    glClearColor(rgba[0], rgba[1], rgba[2], rgba[3]);
}
void tnsClearColor(real r, real g, real b, real a){
    glClearColor(r, g, b, a);
}

//Assuming *result is long enough
void tnsMakeIndexUInt(unsigned int *result, int num, ...){
    int i;
    va_list list;

    va_start(list, num);

    for (i = 0; i < num; i++){
        result[i] = va_arg(list, unsigned int);
    }

    va_end(list);
}
void tnsMakeTriangle(real *arr, real x1, real y1, real x2, real y2, real x3, real y3){
    arr[0] = x1;
    arr[1] = y1;
    arr[2] = x2;
    arr[3] = y2;
    arr[4] = x3;
    arr[5] = y3;
}
void tnsMakeQuad2d(real *arr, real x1, real y1, real x2, real y2, real x3, real y3, real x4, real y4){
    arr[0] = x1;
    arr[1] = y1;
    arr[2] = x2;
    arr[3] = y2;
    arr[4] = x3;
    arr[5] = y3;
    arr[6] = x4;
    arr[7] = y4;
}
void tnsMakeQuadT2d(real *arr, real x1, real y1, real x2, real y2, real x3, real y3, real x4, real y4){
    arr[0] = x1;
    arr[1] = y1;
    arr[2] = x2;
    arr[3] = y2;

    arr[4] = x3;
    arr[5] = y3;
    arr[6] = x2;
    arr[7] = y2;
    
    arr[8] = x3;
    arr[9] = y3;
    arr[10] = x4;
    arr[11] = y4;
}
void tnsMakeQuad3d(real *arr, real x1, real y1, real z1, real x2, real y2, real z2, real x3, real y3, real z3, real x4, real y4, real z4){
    arr[0] = x1;
    arr[1] = y1;
    arr[2] = z1;
    arr[3] = x2;
    arr[4] = y2;
    arr[5] = z2;
    arr[6] = x3;
    arr[7] = y3;
    arr[8] = z3;
    arr[9] = x4;
    arr[10] = y4;
    arr[11] = z4;
}
void tnsMakeQuad4d(real *arr, real x1, real y1, real z1, real w1, real x2, real y2, real z2, real w2, real x3, real y3, real z3, real w3, real x4, real y4, real z4, real w4){
    arr[0] = x1;
    arr[1] = y1;
    arr[2] = z1;
    arr[3] = w1;
    arr[4] = x2;
    arr[5] = y2;
    arr[6] = z2;
    arr[7] = w2;
    arr[8] = x3;
    arr[9] = y3;
    arr[10] = z3;
    arr[11] = w3;
    arr[12] = x4;
    arr[13] = y4;
    arr[14] = z4;
    arr[15] = w4;
}
void tnsMakeCircle2d(real *arr, int slices, real ctrX, real ctrY, real r, int jump){
    int i;
    real radstep = 2 * TNS_PI / (real)slices;
    real rd = 0;
    real x, y;
    for (i = 0; i < slices; i++){
        x = ctrX + cos(rd) * r;
        y = ctrY + sin(rd) * r;
        int idx=2 * i * (1+jump);
        arr[idx] = x;
        arr[idx+1] = y;
        rd += radstep;
    }
}
void tnsMakeArc2d(real *arr, int slices, real ctrX, real ctrY, real r, real rad_begin, real rad_end){
    int i;
    real radstep = (rad_end - rad_begin) / (real)slices;
    real rd = rad_begin + TNS_PI * 2;
    real x, y;
    for (i = 0; i <= slices; i++){
        x = ctrX + cos(rd) * r;
        y = ctrY + sin(rd) * r;
        arr[2 * i] = x;
        arr[2 * i + 1] = y;
        rd += radstep;
    }
}

void tnsMakeBridgedIndex(unsigned int *result, int num, int revert, int begin){
    int i;
    if (!revert){
        for (i = 0; i < num; i++){
            result[i * 2] = begin + i;
            result[i * 2 + 1] = begin + i + num;
        }
    }else{
        for (i = 0; i < num; i++){
            result[i * 2] = begin + i;
            result[i * 2 + 1] = begin - i + num * 2;
        }
    }
}

void tnsMakeRing2d(real *arr, int *index, int slices, real ctrX, real ctrY, real r1, real r2){
    tnsMakeCircle2d(arr, slices, ctrX, ctrY, r1,0);
    tnsMakeCircle2d(&arr[slices*2], slices, ctrX, ctrY, r2,0);
    tnsMakeBridgedIndex(index, slices, 0, 0);
    index[slices*2]=0; index[slices*2+1]=slices;
}

void tnsMakeLinerGradient3d(real *arr, int num_points, real r0, real g0, real b0, real r1, real g1, real b1){
    int i = 0;
    real sr = (r1 - r0) / (real)num_points, sg = (g1 - g0) / (real)num_points, sb = (b1 - b0) / (real)num_points;
    for (i; i < num_points; i++){
        int a = 3 * i;
        arr[a] = r0 + sr * i;
        arr[a + 1] = g0 + sg * i;
        arr[a + 2] = b0 + sb * i;
    }
}
void tnsMakeLinerGradient4d(real *arr, int num_points, real r0, real g0, real b0, real a0, real r1, real g1, real b1, real a1){
    int i = 0;
    real sr = (r1 - r0) / (real)num_points, sg = (g1 - g0) / (real)num_points, sb = (b1 - b0) / (real)num_points, sa = (a1 - a0) / (real)num_points;
    for (i; i < num_points; i++){
        int a = 4 * i;
        arr[a] = r0 + sr * i;
        arr[a + 1] = g0 + sg * i;
        arr[a + 2] = b0 + sb * i;
        arr[a + 3] = a0 + sa * i;
    }
}
void tnsMakeLinerGradient3dv(real *arr, int num_points, real *rgb0, real *rgb1){
    int i = 0;
    real sr = (rgb1[0] - rgb0[0]) / (real)num_points, sg = (rgb1[1] - rgb0[1]) / (real)num_points, sb = (rgb1[2] - rgb0[2]) / (real)num_points;
    for (i; i < num_points; i++){
        int a = 3 * i;
        arr[a] = rgb0[0] + sr * i;
        arr[a + 1] = rgb0[1] + sg * i;
        arr[a + 2] = rgb0[2] + sb * i;
    }
}
void tnsMakeLinerGradient4dv(real *arr, int num_points, real *rgb0, real *rgb1){
    int i = 0;
    real sr = (rgb1[0] - rgb0[0]) / (real)(num_points - 1), sg = (rgb1[1] - rgb0[1]) / (real)(num_points - 1), sb = (rgb1[2] - rgb0[2]) / (real)(num_points - 1), sa = (rgb1[3] - rgb0[3]) / (real)(num_points - 1);
    for (i; i < num_points; i++){
        int a = 4 * i;
        arr[a] = rgb0[0] + sr * i;
        arr[a + 1] = rgb0[1] + sg * i;
        arr[a + 2] = rgb0[2] + sb * i;
        arr[a + 3] = rgb0[3] + sa * i;
    }
}

void tnsMakeFoucsSquare(int L, int R, int U, int B, int W){
    int w = W;
    //int al = Len + w;
    real v[16];

    //v[0] = L - w; v[1] = U - l;
    //v[2] = L - w; v[3] = U + w;
    //v[4] = L + l; v[4] = U + w;
    //v[6] = L + l; v[7] = U - w;
    //v[8] = L + w; v[9] = U - w;
    //v[10] = L + w; v[11] = U - l;

    //v[12] = R - l; v[13] = U + w;
    //v[14] = R + w; v[15] = U + w;
    //v[16] = R + w; v[17] = U - l;
    //v[18] = R - w; v[19] = U - l;
    //v[20] = R - w; v[21] = U - w;
    //v[22] = R - l; v[23] = U - w;

    tnsMakeQuad2d(v, L, U, R, U, R, B, L, B);
    tnsVertexArray2d(v, 4);
    tnsPackAs(GL_LINE_LOOP);
    tnsMakeQuad2d(&v[8], L + W, U - W, R - W, U - W, R - W, B + W, L + W, B + W);
    tnsVertexArray2d(&v[8], 4);
    tnsPackAs(GL_LINE_LOOP);
}

void tnsDrawFloor(real* CamPosition, real Far, int *ShowAxis){
    real OrigFar=Far;
    real height=CamPosition[2];
    real dist=sqrt(fabs(Far*Far-height*height));
    real spanstart=Far/10000;
    real spanl=1e-3,spanh=1e-3;
    while((spanl*10)<spanstart){ spanl*=10; } spanh=spanl;
    while((spanh*10)<Far){ spanh*=10; }
    float usefar=Far;

    tnsFlush();

    glEnable(GL_BLEND); glDepthMask(GL_FALSE); glEnable(GL_DEPTH_TEST);
    tnsUseShader(T->FloorShader);
    tnsEnableShaderv(T->FloorShader);
    //glUniform3f(T->FloorShader->uViewPos,1,1,1);
    usefar=Far; glUniform1fv(T->FloorShader->uFar,1,&usefar);
    
    real sp=spanh;
    while(sp>=spanl){
        real xmin=((int)((CamPosition[0]-dist-sp)/sp))*sp,xmax=((int)((CamPosition[0]+dist+sp)/sp))*sp;
        real ymin=((int)((CamPosition[1]-dist-sp)/sp))*sp,ymax=((int)((CamPosition[1]+dist+sp)/sp))*sp;
        real ux=xmin; while(ux<xmax){ tnsVertex2d(ux,ymin),tnsVertex2d(ux,ymax); ux+=sp; }
        real uy=ymin; while(uy<ymax){ tnsVertex2d(xmin,uy),tnsVertex2d(xmax,uy); uy+=sp; }
        tnsPackAs(GL_LINES);
        tnsFlush();
        Far/=3; if(Far<height) break;
        dist=sqrt(fabs(Far*Far-height*height)); sp/=10;
    usefar=Far; glUniform1fv(T->FloorShader->uFar,1,&usefar);
    }

    //for (i; i < Lim; i++){
    //    if (i == Span && ShowAxis[0]) continue;
    //    tnsVertex3d(-Dist, i * Size - Dist, 0);
    //    tnsVertex3d(Dist, i * Size - Dist, 0);
    //}
//
    //for (i = 0; i < Lim; i++){
    //    if (i == Span && ShowAxis[1]) continue;
    //    tnsVertex3d(i * Size - Dist, -Dist, 0);
    //    tnsVertex3d(i * Size - Dist, Dist, 0);
    //}

    //tnsPackAs(GL_LINES);

    usefar=OrigFar; glUniform1fv(T->FloorShader->uFar,1,&usefar);

    if (ShowAxis[0]){ tnsColor4d(1, 0, 0, 1);
        tnsVertex3d(-OrigFar, 0, 0); tnsVertex3d(OrigFar, 0, 0);
        tnsPackAs(GL_LINES);
    }
    if (ShowAxis[1]){ tnsColor4d(0, 1, 0, 1);
        tnsVertex3d(0, -OrigFar, 0); tnsVertex3d(0, OrigFar, 0);
        tnsPackAs(GL_LINES);
    }
    if (ShowAxis[2]){ tnsColor4d(0, 0, 1, 1);
        tnsVertex3d(0, 0, -OrigFar); tnsVertex3d(0, 0, OrigFar);
        tnsPackAs(GL_LINES);
    }
    tnsFlush();

    tnsUseShader(T->immShader);
    tnsEnableShaderv(T->immShader);
    glDepthMask(GL_TRUE);
}

void tnsDraw2DGrid10(real L, real R, real U, real B, real xmin, real xmax, real ymin, real ymax, real MostDenseW, real MostDenseH,
                     real* color4, real AlphaFactor, int ShowGrid, int TextAlign){
    real span; real W=R-L, H=B-U, rangeX=xmax-xmin, rangeY=ymax-ymin, dW=rangeX/W, dH=rangeY/H;
    real MinSpanW=fabs(MostDenseW*dW), MinSpanH=fabs(MostDenseH*dH);
    
    for(int i=0;i<20;i++){ span=1e-5*pow(10,i); if(span>MinSpanW) break; }
    real startx=((real)((int)(xmin/span)))*span; real x=startx;
    while(x<xmax){
        real lx=tnsInterpolate(L,R,tnsGetRatiod(xmin,xmax,x));
        tnsVertex2d(lx,U);tnsVertex2d(lx,B); x+=span;
    } tnsColor4d(LA_COLOR3(color4),color4[3]*AlphaFactor*AlphaFactor*AlphaFactor); tnsPackAs(GL_LINES);
    x=startx; while(x<xmax){ if((((int)round(x/span))%5)){ x+=span; continue; }
        real lx=tnsInterpolate(L,R,tnsGetRatiod(xmin,xmax,x));
        tnsVertex2d(lx,U);tnsVertex2d(lx,B); x+=span;
    } tnsColor4d(LA_COLOR3(color4),color4[3]*AlphaFactor*AlphaFactor); tnsPackAs(GL_LINES);
    x=startx; while(x<xmax){ if((((int)round(x/span))%10)){ x+=span; continue; }
        real lx=tnsInterpolate(L,R,tnsGetRatiod(xmin,xmax,x));
        tnsVertex2d(lx,U);tnsVertex2d(lx,B); x+=span;
    } tnsColor4d(LA_COLOR3(color4),color4[3]*AlphaFactor); tnsPackAs(GL_LINES);

    for(int i=0;i<20;i++){ span=1e-5*pow(10,i); if(span>MinSpanH) break; }
    real starty=((real)((int)(ymin/span)))*span; real y=starty;
    while(y<ymax){
        real ly=tnsInterpolate(B,U,tnsGetRatiod(ymin,ymax,y));
        tnsVertex2d(L,ly);tnsVertex2d(R,ly); y+=span;
    } tnsColor4d(LA_COLOR3(color4),color4[3]*AlphaFactor*AlphaFactor*AlphaFactor); tnsPackAs(GL_LINES);
    y=starty; while(y<ymax){ if((((int)round(y/span))%5)){ y+=span; continue; }
        real ly=tnsInterpolate(B,U,tnsGetRatiod(ymin,ymax,y));
        tnsVertex2d(L,ly);tnsVertex2d(R,ly); y+=span;
    } tnsColor4d(LA_COLOR3(color4),color4[3]*AlphaFactor*AlphaFactor); tnsPackAs(GL_LINES);
    y=starty; while(y<ymax){ if((((int)round(y/span))%10)){ y+=span; continue; }
        real ly=tnsInterpolate(B,U,tnsGetRatiod(ymin,ymax,y));
        tnsVertex2d(L,ly);tnsVertex2d(R,ly); y+=span;
    } tnsColor4d(LA_COLOR3(color4),color4[3]*AlphaFactor); tnsPackAs(GL_LINES);
}
