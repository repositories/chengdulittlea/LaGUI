/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../la_5.h"

extern LA MAIN;
extern struct _tnsMain *T;

void la_DefaultOperatorParser(laPropPack* parent, laStringSplitor *ss, uint32_t *IconID, char *DisplayString){
    char *StrArg;
    laStringPart *sp;
    if (ss && ss->parts.pFirst){
        if (StrArg = strGetArgumentString(ss, "text")){
            strCopyFull(DisplayString, StrArg);
        }
        if (StrArg = strGetArgumentString(ss, "icon")){
            int adv=0; *IconID = laToUnicode(StrArg, &adv);
        }
    }
}
void la_PanelActiviatorParser(laPropPack* parent, laStringSplitor *ss, uint32_t *IconID, char *DisplayString){
    char *StrArg;
    la_DefaultOperatorParser(parent,ss,IconID,DisplayString);
    if (ss && ss->parts.pFirst){
        if (StrArg = strGetArgumentString(ss, "text")){
            strCopyFull(DisplayString, StrArg);
        }else if (StrArg = strGetArgumentString(ss, "panel_id")){
            laUiTemplate* uit = laFindUiTemplate(StrArg);
            strCopyFull(DisplayString, transLate("Show "));
            strAppend(DisplayString, uit->Title->Ptr);
        }
    }else{
        strCopyFull(DisplayString, transLate("Activate A Panel"));
    }
}
void la_RunToolboxEntryParser(laPropPack* parent, laStringSplitor *ss, uint32_t *IconID, char *DisplayString){
    char *StrArg;
    la_DefaultOperatorParser(parent, ss,IconID,DisplayString);
    if(parent && parent->EndInstance){
        laInputMappingEntry* ime=parent->EndInstance;
        char* display=SSTR(ime->Key), *opname=SSTR(ime->OperatorName);
        if(ime->UseOperator){
            sprintf(DisplayString,"%s",display[0]?display:opname); 
        }else{
            sprintf(DisplayString,"%s",display[0]?display:SSTR(ime->Signal)); 
        }
    }
}

laProp *la_PropLookup(laListHandle *lst, const char *ID);
void la_EnsurePanelSnapping(laPanel *p, int CW, int CH);
void la_RecalcBlockRecursive(laBlock *b, int X, int Y, int W, int H);
void la_ConditionNodeFreeRecursive(laUiConditionNode *ucn);
void la_SendSignalEvent(SYSWINDOW* hwnd, int signal);
void la_SendPasteEvent(SYSWINDOW* hwnd);

int OPMOD_FinishOnData(laOperator* a, laEvent* e){
    if(a->ConfirmData){
        return LA_FINISHED_PASS;
    }
    return LA_RUNNING;
}
void OPEXT_FreeUserData(laOperator* a, int mark_unused){
    if(a->CustomData) memFree(a->CustomData);
}
int OPCHK_AlwaysTrue(laPropPack *pp, laStringSplitor *ss){
    return 1;
}

int OPINV_DoNothing(laOperator *a, laEvent *e){
    return LA_FINISHED;
}
int OPINV_PureYesNo(laOperator *a, laEvent *e){
    laEnableYesNoPanel(a, 0, "Please Consider:", "Are You Sure?", 80, 80, 250, e);
    return LA_FINISHED;
}

#define DEFINE_FORMAT(str, type) \
    if (!strcmp(format, str)) return type;
int la_DetectFileItemType(char* format){
    for(laExtensionType* et=MAIN.ExtraExtensions.pFirst;et;et=et->Item.pNext){ DEFINE_FORMAT(et->Extension, et->FileType); }

    DEFINE_FORMAT("lasdexchange", LA_FILETYPE_LASDEXCHANGE);

    DEFINE_FORMAT("odt", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("ods", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("odp", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("txt", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("c", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("cpp", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("cxx", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("cs", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("pas", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("h", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("hpp", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("hxx", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("inl", LA_FILETYPE_DOCUMENT);

    DEFINE_FORMAT("doc", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("docx", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("xls", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("xlsx", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("ppt", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("pptx", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("rtf", LA_FILETYPE_DOCUMENT);
    DEFINE_FORMAT("md", LA_FILETYPE_DOCUMENT);

    DEFINE_FORMAT("jpg", LA_FILETYPE_IMAGE);
    DEFINE_FORMAT("jpeg", LA_FILETYPE_IMAGE);
    DEFINE_FORMAT("png", LA_FILETYPE_IMAGE);
    DEFINE_FORMAT("tga", LA_FILETYPE_IMAGE);
    DEFINE_FORMAT("bmp", LA_FILETYPE_IMAGE);
    DEFINE_FORMAT("exr", LA_FILETYPE_IMAGE);
    DEFINE_FORMAT("psd", LA_FILETYPE_IMAGE);
    DEFINE_FORMAT("kra", LA_FILETYPE_IMAGE);
    DEFINE_FORMAT("dng", LA_FILETYPE_IMAGE);
    DEFINE_FORMAT("cr2", LA_FILETYPE_IMAGE);
    DEFINE_FORMAT("dds", LA_FILETYPE_IMAGE);

    DEFINE_FORMAT("mp3", LA_FILETYPE_AUDIO);
    DEFINE_FORMAT("wav", LA_FILETYPE_AUDIO);
    DEFINE_FORMAT("ape", LA_FILETYPE_AUDIO);
    DEFINE_FORMAT("flac", LA_FILETYPE_AUDIO);
    DEFINE_FORMAT("cue", LA_FILETYPE_AUDIO);
    DEFINE_FORMAT("wma", LA_FILETYPE_AUDIO);
    DEFINE_FORMAT("aac", LA_FILETYPE_AUDIO);
    DEFINE_FORMAT("m4a", LA_FILETYPE_AUDIO);
    DEFINE_FORMAT("ogg", LA_FILETYPE_AUDIO);

    DEFINE_FORMAT("mp4", LA_FILETYPE_VIDEO);
    DEFINE_FORMAT("mov", LA_FILETYPE_VIDEO);
    DEFINE_FORMAT("avi", LA_FILETYPE_VIDEO);
    DEFINE_FORMAT("avchd", LA_FILETYPE_VIDEO);
    DEFINE_FORMAT("mkv", LA_FILETYPE_VIDEO);
    DEFINE_FORMAT("qt", LA_FILETYPE_VIDEO);
    DEFINE_FORMAT("flv", LA_FILETYPE_VIDEO);

    DEFINE_FORMAT("zip", LA_FILETYPE_COMPRESSED);
    DEFINE_FORMAT("rar", LA_FILETYPE_COMPRESSED);
    DEFINE_FORMAT("gz", LA_FILETYPE_COMPRESSED);
    DEFINE_FORMAT("bz", LA_FILETYPE_COMPRESSED);
    DEFINE_FORMAT("7z", LA_FILETYPE_COMPRESSED);
    DEFINE_FORMAT("iso", LA_FILETYPE_COMPRESSED);
    DEFINE_FORMAT("dmg", LA_FILETYPE_COMPRESSED);

    DEFINE_FORMAT("ttf", LA_FILETYPE_FONT);
    DEFINE_FORMAT("otf", LA_FILETYPE_FONT);
    DEFINE_FORMAT("woff", LA_FILETYPE_FONT);

    DEFINE_FORMAT("svg", LA_FILETYPE_VECTOR);
    DEFINE_FORMAT("dwg", LA_FILETYPE_VECTOR);
    DEFINE_FORMAT("dxf", LA_FILETYPE_VECTOR);
    DEFINE_FORMAT("cdr", LA_FILETYPE_VECTOR);

    DEFINE_FORMAT("htm", LA_FILETYPE_WEBPAGE);
    DEFINE_FORMAT("html", LA_FILETYPE_WEBPAGE);
    DEFINE_FORMAT("xhtml", LA_FILETYPE_WEBPAGE);

    DEFINE_FORMAT("xml", LA_FILETYPE_META);
    DEFINE_FORMAT("dat", LA_FILETYPE_META);
    DEFINE_FORMAT("json", LA_FILETYPE_META);

    DEFINE_FORMAT("blend", LA_FILETYPE_BLEND);

    DEFINE_FORMAT("pdf", LA_FILETYPE_PDF);

    DEFINE_FORMAT("exe", LA_FILETYPE_EXEC);

    DEFINE_FORMAT("dll", LA_FILETYPE_SYS);
    DEFINE_FORMAT("sys", LA_FILETYPE_SYS);

    return 0;
}
int la_AcceptFileFormat(laFileBrowser* fb, char* format){
    if(!format || !format[0]) return 1;
    char buf[2048];
    if(fb->FilterType){
        for(laExtensionType* et=MAIN.ExtraExtensions.pFirst;et;et=et->Item.pNext){
            if(et->FileType==fb->FilterType && strSame(format, et->Extension)) return 1;
            if(fb->ShowBackups){
                sprintf(buf,"%s~",et->Extension);
                if(strSame(format,buf)) return 1;
            }
        }
        return 0;
    }
    if(!fb->ss_filter_extensions) return 1;
    for(laStringPart* sp=fb->ss_filter_extensions->parts.pFirst;sp;sp=sp->Item.pNext){
        if(!strcmp(sp->Content, format)) return 1;
        if(fb->ShowBackups){
            sprintf(buf,"%s~",sp->Content);
            if(strSame(format,buf)) return 1;
        }
    }
    return 0;
}

typedef int (*FileSortCompF)(laFileItem* f1,laFileItem* f2);
#if defined (_WIN32) || defined (LAGUI_ANDROID)
#define strverscmp strcmp
#endif
int la_filecompname(laFileItem* f1,laFileItem* f2){ return strverscmp(f2->Name, f1->Name); }
int la_filecompnamerev(laFileItem* f1,laFileItem* f2){ return strverscmp(f1->Name, f2->Name); }
int la_filecomptime(laFileItem* f1,laFileItem* f2){ return f1->Timestamp - f2->Timestamp; }
int la_filecomptimerev(laFileItem* f1,laFileItem* f2){ return f2->Timestamp - f1->Timestamp; }
int la_filecompsize(laFileItem* f1,laFileItem* f2){ return f1->Size - f2->Size; }
int la_filecompsizerev(laFileItem* f1,laFileItem* f2){ return f2->Size - f1->Size; }
#ifdef _WIN32
#undef strverscmp
#endif
void la_SortFiles(laFileBrowser* fb, laListHandle* files, int IsDir){
    laListHandle tmp={0}; laFileItem* fi;
    FileSortCompF comp = 0;
    switch(fb->SortBy){
    case LA_FILE_SORT_NAME: default: comp=la_filecompname; break;
    case LA_FILE_SORT_NAME_REV: comp=la_filecompnamerev; break;
    case LA_FILE_SORT_TIME: comp=IsDir?la_filecompname:la_filecomptime; break;
    case LA_FILE_SORT_TIME_REV: comp=IsDir? la_filecompnamerev:la_filecomptimerev; break;
    case LA_FILE_SORT_SIZE: comp=IsDir? la_filecompname :la_filecompsize; break;
    case LA_FILE_SORT_SIZE_REV: comp=IsDir?la_filecompnamerev:la_filecompsizerev; break;
    }
    while(fi=lstPopItem(files)){ int inserted=0;
        for(laFileItem* fii=tmp.pFirst;fii;fii=fii->Hyper.pNext){
            int a=comp(fii,fi);
            if(a<0){ lstInsertItemBefore(&tmp,fi,fii); inserted=1; break; }
        }
        if(!inserted){ lstAppendItem(&tmp, fi); }
    }
    files->pFirst=tmp.pFirst; files->pLast=tmp.pLast;
}
void la_FileBrowserRebuildList(laFileBrowser *fb){
    laFileItem *fi = 0;
    laDiskItem *dl = 0;
    laListHandle Files = {0};
    char Lookup[2048]={0};
    char Final[2048]={0};
    char DiskLabels[256] = {0};
    char *pd = DiskLabels;
    int len = strlen(fb->Path);
    int NumDisks = 0;
    u64bit FreeAvailable = 0;
    u64bit FreeSpace_UNUSED = 0;
    u64bit TotalSpace = 0;
    real Ratio = 0;

#ifdef __linux__

#ifdef LAGUI_ANDROID
#define versionsort alphasort
#endif

    if (fb->Path[len - 1] != U'/') strcat(fb->Path, "/");
    struct dirent **NameList=0;
    int NumFiles=scandir(fb->Path,&NameList,0,versionsort);

    while (fi = lstPopItem(&fb->FileList)) memFree(fi);

    for(int i=0;i<NumFiles;i++){
        struct dirent* d = NameList[i];
        if(!strcmp(d->d_name, ".") || !strcmp(d->d_name, "..")){continue;}
        if(fb->FilterName[0] && !strcasestr(d->d_name,fb->FilterName)){continue;}
        struct stat s;
        sprintf(Final, "%s%s",fb->Path,d->d_name);
        stat(Final, &s);
        if (S_ISDIR(s.st_mode)){
            fi = memAcquireSimple(sizeof(laFileItem));
            strcpy(fi->Name, d->d_name);
            fi->IsFolder = 1;
            fi->Type = LA_FILETYPE_FOLDER;
            lstAppendItem(&fb->FileList, fi);
        }elif (!fb->SelectFolder){
            char *format = strGetLastSegment(d->d_name, '.');
            if(!la_AcceptFileFormat(fb,format)){ continue; }

            fi = memAcquireSimple(sizeof(laFileItem));
            strcpy(fi->Name, d->d_name);
            fi->Size = s.st_size;
            fi->Type = la_DetectFileItemType(format);

            fi->Timestamp = s.st_ctime;
            struct tm *t = localtime(&s.st_ctime);
            fi->TimeModified.Year = t->tm_year+1900;
            fi->TimeModified.Month = t->tm_mon+1;;
            fi->TimeModified.Day = t->tm_mday;
            fi->TimeModified.Hour = t->tm_hour;
            fi->TimeModified.Minute = t->tm_min;
            fi->TimeModified.Second = t->tm_sec;

            lstAppendItem(&Files, fi);
            //lstAppendItem(&fb->FileList, fi);
        }
    }
    for (int i = 0; i < NumFiles; i++) { free(NameList[i]); } free(NameList);
#endif
#ifdef _WIN32
    WIN32_FIND_DATA FindFileData;
    HANDLE hFind;
    SYSTEMTIME stUTC, stLocal;
    strCopyFull(Lookup, fb->Path);
    if (Lookup[len - 1] != U'\\') strcat(Lookup, "\\*.*"); else strcat(Lookup, "*.*");
    hFind = FindFirstFile(Lookup, &FindFileData);

    while (fi = lstPopItem(&fb->FileList)) memFree(fi);

    if (hFind == INVALID_HANDLE_VALUE){ return; }

    char buf[1024]; if (fb->FilterName[0]) { strToLower(fb->FilterName); }
    while (1) {
        if (fb->FilterName[0]) {
            strcpy(buf, FindFileData.cFileName); strToLower(buf);
            if (!strstr(buf, fb->FilterName)) { goto findnext; }
        }
        if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
            if (FindFileData.cFileName[0] != '.') {
                fi = memAcquireSimple(sizeof(laFileItem));
                strcpy(fi->Name, FindFileData.cFileName);
                fi->IsFolder = 1;
                fi->Type = LA_FILETYPE_FOLDER;
                lstAppendItem(&fb->FileList, fi);
            }
        }elif(!fb->SelectFolder) {
            char* format = strGetLastSegment(FindFileData.cFileName, '.');
            if (!la_AcceptFileFormat(fb, format)) { goto findnext; }

            fi = memAcquireSimple(sizeof(laFileItem));
            strCopyFull(fi->Name, FindFileData.cFileName);
            fi->Size = FindFileData.nFileSizeLow;
            fi->Type = la_DetectFileItemType(format);

            FileTimeToSystemTime(&(FindFileData.ftLastWriteTime), &stUTC);
            SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

            fi->Timestamp = (uint64_t)FindFileData.ftLastWriteTime.dwHighDateTime|((uint64_t)(FindFileData.ftLastWriteTime.dwLowDateTime)<<32);
            fi->TimeModified.Year = stLocal.wYear;
            fi->TimeModified.Month = stLocal.wMonth;
            fi->TimeModified.Day = stLocal.wDay;
            fi->TimeModified.Hour = stLocal.wHour;
            fi->TimeModified.Minute = stLocal.wMinute;
            fi->TimeModified.Second = stLocal.wSecond;

            lstAppendItem(&Files, fi);
        }
findnext:
        if (!FindNextFile(hFind, &FindFileData))
            break;
    }
#endif
    
    la_SortFiles(fb, &fb->FileList,1);
    la_SortFiles(fb, &Files,0);
    lstCombineLists(&fb->FileList, &Files);

    while (dl = lstPopItem(&fb->Disks)) memFree(dl);

#ifdef _WIN32
    NumDisks = GetLogicalDriveStrings(256, DiskLabels) / 4;
    while (*pd){
        char Name[3] = "*:";
        Name[0] = *pd;
        if (GetDiskFreeSpaceEx(Name, &FreeAvailable, &TotalSpace, &FreeSpace_UNUSED)){
            dl = memAcquire(sizeof(laDiskItem));
            dl->Path[0] = *pd;
            dl->Total_GB = (real)TotalSpace / 1073741824.0f;   //B->GB
            dl->Free_GB = (real)FreeAvailable / 1073741824.0f; //B->GB

            if (Name[0] == fb->Path[0]) fb->RootDisk = dl;

            lstAppendItem(&fb->Disks, dl);
        }
        pd += 4;
    }
#endif

#ifdef LA_LINUX
    /* Loop over mount points, not caring about gvfs network drives. */
    struct mntent *mnt;
    FILE *fp;
    fp = setmntent(MOUNTED, "r");
    if (!fp) {
        logPrintNew("Could not get a list of mounted file-systems\n");
    }
    else {
        while ((mnt = getmntent(fp))) {
            if (strPrefix(mnt->mnt_dir, "/boot")) continue;
            if (!strPrefix(mnt->mnt_fsname, "/dev")) continue;
            if (strPrefix(mnt->mnt_fsname, "/dev/loop")) continue;

            dl = memAcquire(sizeof(laDiskItem));

            strcpy(dl->Path, mnt->mnt_dir);

            struct statvfs stats={0};
            statvfs(mnt->mnt_dir, &stats);
            dl->Total_GB = (real)stats.f_frsize*stats.f_blocks / 1073741824.0f;
            dl->Free_GB = (real)stats.f_bsize*stats.f_bavail / 1073741824.0f;

            if (strPrefix(fb->Path, dl->Path)) fb->RootDisk = dl;
            
            lstAppendItem(&fb->Disks, dl);
        }
        if (endmntent(fp) == 0) {
            logPrintNew("Could not close the list of mounted file-systems\n");
        }
    }
#endif

    fb->Active = 0;
}
laFileBrowser *la_FileBrowserInit(laOperator *a){
    laFileBrowser *fb = memAcquire(sizeof(laFileBrowser));
    char* arg=0;

#ifdef LAGUI_ANDROID
    strcpy(fb->Path, MAIN.ExternalDataPath);
    laBookmarkedFolder* bf=memAcquireSimple(sizeof(laBookmarkedFolder));
    strcpy(bf->Path,fb->Path); strcpy(bf->Name,strGetLastSegment(fb->Path,'/ ')); lstAppendItem(&fb->Bookmarks,bf);
    bf=memAcquireSimple(sizeof(laBookmarkedFolder));
    strcpy(bf->Path,MAIN.InternalDataPath); strcpy(bf->Name,"Internal"); lstAppendItem(&fb->Bookmarks,bf);

    if(!MAIN.GotFilePermission){
        MAIN.GotFilePermission = la_check_permission("android.permission.WRITE_EXTERNAL_STORAGE");
        if(!MAIN.GotFilePermission){
            la_request_permission("android.permission.WRITE_EXTERNAL_STORAGE");
        }
    }

#else
    strcpy(fb->Path, SSTR(MAIN.WorkingDirectory));
#endif

    if (strArgumentMatch(a->ExtraInstructionsP, "select", "folder")){ fb->SelectFolder = LA_FILE_SELECT_FOLDER; }
    if (strArgumentMatch(a->ExtraInstructionsP, "warn_file_exists", "true")){ fb->WarnFileExists = 1; }
    if ((arg=strGetArgumentString(a->ExtraInstructionsP, "filter_extensions"))){ fb->ss_filter_extensions=strSplitPath(arg,','); }
    if ((arg=strGetArgumentString(a->ExtraInstructionsP, "use_extension"))){ strcpy(fb->UseExtension, arg); }
    if ((arg=strGetArgumentString(a->ExtraInstructionsP, "filter_type"))){ sscanf(arg,"%d",&fb->FilterType); }
    if ((arg=strGetArgumentString(a->ExtraInstructionsP, "use_type"))){ sscanf(arg,"%d",&fb->UseType); }

#ifdef LA_LINUX
    char BookmarkPath[1024];
    strcat(strcpy(BookmarkPath, getenv("HOME")), "/.config/gtk-3.0/bookmarks");
    FILE* f=fopen(BookmarkPath, "r");
    if(f){ char entry[1024]={0};
        while(fgets(entry,1024,f)){ laBookmarkedFolder* bf=memAcquireSimple(sizeof(laBookmarkedFolder));
            entry[strlen(entry)-1]=0;
            strcpy(bf->Path,&entry[7]);
            strcpy(bf->Name,strGetLastSegment(&entry[7],'/')); lstAppendItem(&fb->Bookmarks,bf);
        }
        fclose(f);
    }
#endif

#ifdef _WIN32
    TCHAR szPath[MAX_PATH];
#define ADD_SPECIAL_FOLDER(entry) \
    if(SUCCEEDED(SHGetFolderPath(NULL, \
        entry, NULL, 0, szPath))) { \
        laBookmarkedFolder* bf = memAcquireSimple(sizeof(laBookmarkedFolder)); \
        strcpy(bf->Path, szPath); \
        strcpy(bf->Name, strGetLastSegment(szPath,'\\')); lstAppendItem(&fb->Bookmarks, bf); \
    }
    ADD_SPECIAL_FOLDER(CSIDL_DESKTOP);
    ADD_SPECIAL_FOLDER(CSIDL_PERSONAL);
    ADD_SPECIAL_FOLDER(CSIDL_MYPICTURES);
#endif


    la_FileBrowserRebuildList(fb);
    fb->FileName[0] = 0;

    memAssignRef(fb, &fb->Thumbnail, tnsNewImage(0));
    fb->ShowThumbnail = 1;

    return fb;
}
void la_FileBrowserGetFullPath(laFileBrowser *fb,char* buf){
    buf[0]=0; int plen;
    if (!fb->SelectFolder && fb->FileName[0] == U'\0') return;
    plen = strlen(fb->Path);
    if (fb->Path[plen - 1] != LA_PATH_SEP) strcat(fb->Path, LA_PATH_SEPSTR);
    strCopyFull(buf, fb->Path);
    strcat(buf, fb->FileName);
}
void la_FileBrowserRefreshThumbnail(laFileBrowser* fb){
    if(MAIN.ThumbnailProp && fb->Active && fb->Active->Type==LA_FILETYPE_UDF){
        char* prop=SSTR(MAIN.ThumbnailProp); if(prop[0]){
            char fbuf[2048];la_FileBrowserGetFullPath(fb,fbuf);
            FILE* fpudf=fopen(fbuf,"rb"); if(fpudf){
                void* data=0; size_t size=0;
                if(laExtractQuickRaw(fpudf,prop,&data,&size)){
                    tnsRefreshImage(fb->Thumbnail,data);
                    fclose(fpudf); return;
                }
                fclose(fpudf);
            }
        }
    }
#ifdef LA_LINUX
    char buf[2048]="file://"; char md5[128];
    la_FileBrowserGetFullPath(fb,buf+strlen(buf));
    md5String(buf,md5); toHexString(md5,fb->MD5);
    sprintf(buf,"%s/.cache/thumbnails/normal/%s.png", getenv("HOME"),fb->MD5);
    FILE *fp=fopen(buf,"rb"); char* data=0;
    if(!fp){ 
        sprintf(buf, "%s/.cache/thumbnails/large/%s.png", getenv("HOME"), fb->MD5);
        fp = fopen(buf, "rb");
    }
    if(fp){
        fseek(fp, 0, SEEK_END); u64bit SeekEnd = ftell(fp); fseek(fp, 0, SEEK_SET);
        data = calloc(1, SeekEnd); fread(data, SeekEnd, 1, fp);
        tnsRefreshImage(fb->Thumbnail,data);
    }else{
        tnsRefreshImage(fb->Thumbnail,0);
    }
#endif
}
void laset_FileBrowserSelectFile(laFileBrowser *fb, laFileItem *fi, int State){
    int len;
    if (fb->Active == fi){
        if (fi->IsFolder){
            len = strlen(fb->Path);
            if (fb->Path[len - 1] != LA_PATH_SEP) strcat(fb->Path, LA_PATH_SEPSTR);
            strcat(fb->Path, fi->Name);
            la_FileBrowserRebuildList(fb);
            fb->FileName[0] = 0; fb->FilterName[0]=0;
            laRecalcCurrentPanelImmediate();
        }
    }else{
        if (!fi->IsFolder){
            strCopyFull(fb->FileName, fi->Name);
        }
        fb->Active = fi;
        la_FileBrowserRefreshThumbnail(fb);
    }
}
int la_FileBrowserConfirm(laOperator *a, laFileBrowser *fb){
    char buf[2048];
    la_FileBrowserGetFullPath(fb,buf);
    laConfirmString(a, buf, LA_CONFIRM_OK);
    return 1;
}
void *laget_FileBrowserFirstFile(laFileBrowser *fb, void* unused){
    return fb->FileList.pFirst;
}
void *laget_FileBrowserActiveFile(laFileBrowser *fb){
    return fb->Active;
}
void laget_FileBrowserDiskID(laDiskItem *di, char *result, char** move){
#ifdef _WIN32
    result[0] = di->Path[0];
    result[1] = U':';
    result[2] = LA_PATH_SEP;
    result[3] = U'\0';
#endif
#ifdef LA_LINUX
    *move = di->Path;
#endif
}
void laget_FileBrowserDiskCapacity(laDiskItem *di, char *result, char** move){
    sprintf(result, "%.1lf/%.1lfGB",di->Free_GB,di->Total_GB);
}
void laset_FileBrowserActiveDisk(laFileBrowser *fb, laDiskItem *di, int UNUSED_State){
    fb->RootDisk = di;
    char path[1024]; char* _path=path;
    laget_FileBrowserDiskID(di, path, &_path);
    strcpy(fb->Path,_path);
    la_FileBrowserRebuildList(fb);
    fb->FileName[0] = 0;
}
void laset_FileBrowserPath(laFileBrowser *fb, char *content){
    if(!strSame(content, fb->Path)){
        strCopyFull(fb->Path, content); la_FileBrowserRebuildList(fb); fb->FileName[0] = 0; laRecalcCurrentPanel();
    }
}
void laset_FileBrowserBookmark(laFileBrowser *fb, laBookmarkedFolder *bf){
    strcpy(fb->Path,bf->Path); la_FileBrowserRebuildList(fb); fb->FileName[0] = 0;
}
void laset_FileBrowserFileName(laFileBrowser *fb, char *content){
    strCopyFull(fb->FileName, content); int file_okay=0;
    char* ext=strGetLastSegment(fb->FileName,'.');
    if(fb->UseExtension[0] && strcmp(ext,fb->UseExtension)){ strcat(fb->FileName,"."); strcat(fb->FileName,fb->UseExtension); }
    else if(fb->UseType){ int file_okay=0; laExtensionType* FirstET=0;
        for(laExtensionType*et=MAIN.ExtraExtensions.pFirst;et;et=et->Item.pNext){
            if((!FirstET) && et->FileType==fb->UseType) FirstET=et;
            if(et->FileType==fb->UseType && strSame(ext,et->Extension)){ file_okay=1; break; }
        }
        if((!file_okay) && FirstET){ strcat(fb->FileName,"."); strcat(fb->FileName,FirstET->Extension); }
    }
    la_FileBrowserRebuildList(fb);
    laRecalcCurrentPanel();
}
void laset_FileBrowserFilterName(laFileBrowser *fb, char *content){
    strCopyFull(fb->FilterName, content); int file_okay=0;
    la_FileBrowserRebuildList(fb); laRecalcCurrentPanel();
}
void* laget_FileBrowserAcceptedExtensionsFrist(laFileBrowser* fb, laPropIterator* pi){
    if(!fb->UseType){ return 0; }
    for(laExtensionType*et=MAIN.ExtraExtensions.pFirst;et;et=et->Item.pNext){
        if(et->FileType==fb->UseType){ return et; }
    }
    return 0;
}
void* laget_FileBrowserAcceptedExtensionsNext(laExtensionType* et, laPropIterator* pi){
    for(laExtensionType*iet=et->Item.pNext;iet;iet=iet->Item.pNext){
        if(et->FileType==iet->FileType){ return iet; }
    }
    return 0;
}
void* laset_FileBrowserExtension(laFileBrowser* fb, laExtensionType* et){
    if(fb->UseType && fb->FileName[0] && et){ char* ext=strGetLastSegment(fb->FileName,'.');
        if(!ext){ sprintf(fb->FileName,".%s",ext); }
        elif(strcmp(et->Extension,ext)){ sprintf(ext,"%s",et->Extension); }
        la_FileBrowserRebuildList(fb); laRecalcCurrentPanel();
    }
    return 0;
}
void laset_FileBrowserShowBackups(laFileBrowser* fb, int show){
    fb->ShowBackups = show; la_FileBrowserRebuildList(fb); laRecalcCurrentPanel();
}
void laset_FileBrowserShowThumbnail(laFileBrowser* fb, int show){
    fb->ShowThumbnail = show; if(show){ la_FileBrowserRefreshThumbnail(fb); } laRecalcCurrentPanel();
}
void laset_FileBrowserSortName(laFileBrowser* fb, int s){
    if(fb->SortBy==LA_FILE_SORT_NAME) fb->SortBy=LA_FILE_SORT_NAME_REV;
    else fb->SortBy=LA_FILE_SORT_NAME; la_FileBrowserRebuildList(fb); laRecalcCurrentPanel();
}
int laget_FileBrowserSortName(laFileBrowser*fb){
    if(fb->SortBy==LA_FILE_SORT_NAME) return 1; if(fb->SortBy==LA_FILE_SORT_NAME_REV) return 2; return 0;
}
void laset_FileBrowserSortTime(laFileBrowser* fb, int s){
    if(fb->SortBy==LA_FILE_SORT_TIME) fb->SortBy=LA_FILE_SORT_TIME_REV;
    else fb->SortBy=LA_FILE_SORT_TIME; la_FileBrowserRebuildList(fb); laRecalcCurrentPanel();
}
int laget_FileBrowserSortTime(laFileBrowser*fb){
    if(fb->SortBy==LA_FILE_SORT_TIME) return 1; if(fb->SortBy==LA_FILE_SORT_TIME_REV) return 2; return 0;
}
void laset_FileBrowserSortSize(laFileBrowser* fb, int s){
    if(fb->SortBy==LA_FILE_SORT_SIZE) fb->SortBy=LA_FILE_SORT_SIZE_REV;
    else fb->SortBy=LA_FILE_SORT_SIZE; la_FileBrowserRebuildList(fb); laRecalcCurrentPanel();
}
int laget_FileBrowserSortSize(laFileBrowser*fb){
    if(fb->SortBy==LA_FILE_SORT_SIZE) return 1; if(fb->SortBy==LA_FILE_SORT_SIZE_REV) return 2; return 0;
}
void laget_FileBrowserFileSizeString(laFileItem* fi, char* content, char** move){
    if(fi->Size >= (1<<30)){
        sprintf(content, "%.3lf GB", (real)fi->Size/(1<<30));
    }elif(fi->Size >= (1<<20)){
        sprintf(content, "%.3lf MB", (real)fi->Size/(1<<20));
    }elif(fi->Size >= (1<<10)){
        sprintf(content, "%.3lf KB", (real)fi->Size/(1<<10));
    }else{
        sprintf(content, "%d %s", fi->Size, transLate("Bytes"));
    }
}
void la_FileBrowserUpLevel(laFileBrowser *fb){
    char *p = fb->Path;
    char *LastP = 0;
    int Count = 0;
    for (p; *p; p++){
        if (*p && *p == LA_PATH_SEP && p[1]!=0){
            LastP = p;
            Count++;
        }
    }
    if (Count > 1) *LastP = 0;
    else if(LastP) *(LastP + 1) = 0;
    la_FileBrowserRebuildList(fb); fb->FileName[0] = 0;
}
void la_FileBrowserNewDirectory(laFileBrowser* fb,char* path){
    char* p=fb->Path;
    int len =strlen(p);
#ifdef LA_LINUX
    if (p[len - 1] != U'/') strcat(p, "/");
    char usepath[2048]; sprintf(usepath,"%s%s",p,path);
    if(!mkdir(usepath,S_IRWXU | S_IRWXG | S_IRWXO)){
        strcpy(fb->Path,usepath);
    }
#endif
#ifdef _WIN32
    if (p[len - 1] != U'\\' && p[len - 1] != U'/') strcat(p, "\\");
    char usepath[2048]; sprintf(usepath,"%s%s",p,path);
    if(!_mkdir(usepath)){
        strcpy(fb->Path,usepath);
    }
#endif
    la_FileBrowserRebuildList(fb); laRecalcCurrentPanel();
}
int OPINV_FileBrowser(laOperator *a, laEvent *e){
    a->CustomData = la_FileBrowserInit(a);
    laEnableOperatorPanel(a, 0, LA_RH2, LA_RH2, 500, 500, 0, 0, 0, 0, LA_RH2, LA_RH2, LA_RH2, LA_RH2, e);
    return LA_RUNNING;
}
void OPEXT_FileBrowser(laOperator *a, int mark){
    laFileBrowser *fb = a->CustomData;
    void* f;
    while (f=lstPopItem(&fb->Disks)) memFree(f);
    while (f=lstPopItem(&fb->FileList)) memFree(f);
    while (f=lstPopItem(&fb->Bookmarks)) memFree(f);
    strDestroyStringSplitor(&fb->ss_filter_extensions);
    tnsRemoveImage(fb->Thumbnail);
    memFree(fb);
}
int OPMOD_FileBrowser(laOperator *a, laEvent *e){
    laFileBrowser *fb = a->CustomData;

    if (a->ConfirmData){
        if (a->ConfirmData->Mode == LA_CONFIRM_CANCEL){
            if(fb->StatusWaitingWarning){ fb->StatusWaitingWarning=0; return LA_RUNNING; }
            laConfirmSameDataIfAny(a); return LA_FINISHED_PASS;
        }
        if (a->ConfirmData->Mode == LA_CONFIRM_OK){
            la_FileBrowserConfirm(a, fb); return LA_FINISHED_PASS;
        }
    }

    if (e->type == LA_KEY_DOWN && e->key==LA_KEY_ESCAPE){
        laConfirmInt(a, 0, LA_CONFIRM_CANCEL);
        return LA_FINISHED;
    }

    return LA_RUNNING;
}
int OPCHK_IsFileBrowser(laPropPack *This, laStringSplitor *ss){
    if (This && This->LastPs->p->SubProp == _LA_PROP_FILE_BROWSER) return 1;
    return 0;
}
int OPINV_FileBrowserUpLevel(laOperator *a, laEvent *e){
    if (a->This){
        la_FileBrowserUpLevel(a->This->EndInstance);
        laNotifyUsersPPPath(a->This, "path");
        laRecalcCurrentPanel();
    }
    return LA_FINISHED_PASS;
}
int OPINV_FileBrowserRefresh(laOperator *a, laEvent *e){
    if (a->This){
        la_FileBrowserRebuildList(a->This->EndInstance); laRecalcCurrentPanel();
        laNotifyUsersPPPath(a->This, "path");
    }
    return LA_FINISHED_PASS;
}
int OPINV_FileBrowserNewDirectory(laOperator *a, laEvent *e){
    if (a->This){
        laEnableOperatorPanel(a,a->This,e->x,e->y,200,200,400,400,0,0,0,0,0,0,e);
        return LA_RUNNING;
    }
    return LA_FINISHED;
}
int OPMOD_FileBrowserNewDirectory(laOperator *a, laEvent *e){
    laFileBrowser* fb=a->This->EndInstance;
    if (a->ConfirmData){
        if (a->ConfirmData->Mode == LA_CONFIRM_OK){
            la_FileBrowserNewDirectory(fb,fb->TempStr);
        }
        return LA_FINISHED;
    }
    return LA_RUNNING;
}
int OPCHK_FileBrowserCanConfirm(laPropPack *This, laStringSplitor *ss){
    if(!OPCHK_IsFileBrowser(This,ss)) return 0;
    laFileBrowser* fb=This->EndInstance;
    if(fb->SelectFolder) return 1;
    if(fb->FileName[0]) return 1;
    return 0;
}
int OPINV_FileBrowserConfirm(laOperator *a, laEvent *e){
    if (a->This){
        laFileBrowser* fb=a->This->EndInstance;
        if(fb->WarnFileExists){
            char path[2048]; la_FileBrowserGetFullPath(fb, path);
#ifdef LA_LINUX
            if(access(path, F_OK)==0)
#endif
#ifdef _WIN32
            if (PathFileExists(path))
#endif
            {
                laEnableYesNoPanel(a,0,"File exists","Selected file already exists. Overwrite the file?",e->x-LA_RH,e->y-LA_RH,200,e);
                fb->StatusWaitingWarning=1; return LA_RUNNING;
            }
        }
        laConfirmInt(a, 0, LA_CONFIRM_OK); return LA_FINISHED_PASS;
    }
    return LA_FINISHED_PASS;
}
int OPMOD_FileBrowserConfirm(laOperator *a, laEvent *e){
    laFileBrowser* fb=a->This->EndInstance;
    if (a->ConfirmData){
        if (a->ConfirmData->Mode == LA_CONFIRM_CANCEL){
            if(fb->StatusWaitingWarning){ fb->StatusWaitingWarning=0; return LA_FINISHED; }
        }
        if (a->ConfirmData->Mode == LA_CONFIRM_OK){
            laConfirmInt(a, 0, LA_CONFIRM_OK); return LA_FINISHED_PASS;
        }
        return LA_FINISHED;
    }
    return LA_RUNNING;
}

void laui_FileBrowserNewDirectory(laUiList *uil, laPropPack *This, laPropPack *Operator, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil);
    laShowItem(uil,c,This,"temp_str");
    laUiItem* row=laBeginRow(uil,c,0,0);
    laShowSeparator(uil,c)->Expand=1;
    laShowItem(uil,c,0,"LA_confirm")->Flags|=LA_UI_FLAGS_HIGHLIGHT;
    laEndRow(uil,row);
}

void OPEXT_UDFOperation(laOperator *a, laEvent *e){
    laUDFPreviewExtra *upe = a->CustomData;
    memFree(upe);
}
int OPINV_UDFRead(laOperator *a, laEvent *e){
    laInvoke(a, "LA_file_dialog", e, 0, "filter_type=1;", 0);
    a->CustomData = memAcquireSimple(sizeof(laUDFPreviewExtra));
    laUDFPreviewExtra *upe = a->CustomData;
    if(strArgumentMatch(a->ExtraInstructionsP,"mode","append")){ upe->Append=1; }
    return LA_RUNNING;
}
int OPMOD_UDFRead(laOperator *a, laEvent *e){
    laUDFPreviewExtra *upe = a->CustomData;
    if (a->ConfirmData){
        if (a->ConfirmData->StrData){
            laManagedUDF* m;
            upe->UDF = laOpenUDF(a->ConfirmData->StrData, 1, 0, &m);
            if (upe->UDF){
                laFreeNewerDifferences();
                laFreeOlderDifferences(1);
                laExtractUDF(upe->UDF, m, upe->Append?LA_UDF_MODE_APPEND:LA_UDF_MODE_OVERWRITE);
                laCloseUDF(upe->UDF);
                laRecordEverythingAndPush(); laNotifyUsers("la.differences");
                return LA_FINISHED;
            }
        }
        return LA_FINISHED;
    }

    return LA_RUNNING;
}
int OPINV_UDFSaveInstance(laOperator *a, laEvent *e){
    laInvoke(a, "LA_file_dialog", e, 0, 0, 0);
    a->CustomData = memAcquireSimple(sizeof(laUDFPreviewExtra));
    return LA_RUNNING;
}
int OPMOD_UDFSaveInstance(laOperator *a, laEvent *e){
    laUDFPreviewExtra *upe = a->CustomData;
    laPropPack* pp=a->This; if(!pp||!pp->EndInstance) return LA_CANCELED;
    if (a->ConfirmData){
        if (a->ConfirmData->StrData){
            upe->UDF = laPrepareUDF(a->ConfirmData->StrData);
            if (upe->UDF){
                laWritePropP(upe->UDF, pp);
                laPackUDF(upe->UDF, 0, 0);
                return LA_FINISHED;
            }
        }
    }

    return LA_FINISHED;
}

void* laget_FirstManagedUDF(void* unused, void* unused_pi){ return MAIN.ManagedUDFs.pFirst; }

void laset_ManagedSavePage(laManagedSaveExtra* mse, int p){
    mse->ShowPage=p;
    laRegisterModifications(0,0,0,0);
}

void OPEXT_ManagedSave(laOperator *a, laEvent *e){
    laManagedSaveExtra *upe = a->CustomData;
    memFree(upe);
    MAIN.SetUDFPending=0;
}
int OPINV_ManagedSave(laOperator *a, laEvent *e){
    int OnExit=0;
    if(a->ExtraInstructionsP){
        if(strSame(strGetArgumentString(a->ExtraInstructionsP, "quiet"),"true")){
            int empty=0; laRegisterModifications(0,1,&empty,0);
            if(strSame(strGetArgumentString(a->ExtraInstructionsP, "ignore_unassigned"),"true")) empty=0;
            int modified_only=0; if(strSame(strGetArgumentString(a->ExtraInstructionsP, "modified_only"),"true")) modified_only=1;
            if(!empty){
                if(!laSaveManagedUDF(modified_only)){
                    laEnableMessagePanel(0,0,"Caution",
                        "Not all files have been successfully written.\nSee messages in terminal for details.\n",e->x,e->y,400,e);
                }
                return LA_FINISHED;
            }
        }
        if(strSame(strGetArgumentString(a->ExtraInstructionsP, "on_exit"),"true")){ OnExit=1; }
    }

    if(laOperatorExistsT(a->Type)) return LA_CANCELED;

    a->CustomData = memAcquire(sizeof(laManagedSaveExtra));
    laManagedSaveExtra* mse=a->CustomData;
    mse->OnExit=OnExit;
    mse->ShowPage = MAIN.ManagerDefaultView;
    
    laEnableOperatorPanel(a, 0, LA_RH2,LA_RH2,400,400,0,0,0,0,LA_RH2,LA_RH2,LA_RH2,LA_RH2,e);
    return LA_RUNNING;
}
int OPMOD_ManagedSave(laOperator *a, laEvent *e){
    laUDFPreviewExtra *upe = a->CustomData;

    if (a->ConfirmData){
        if(a->ConfirmData->Mode==LA_CONFIRM_CUSTOM_STRING && strSame(a->ConfirmData->StrData,"DISCARD_AND_QUIT")){
            return LA_OPERATOR_CALLS_SHUTOFF;
        }
        if(a->ConfirmData->Mode==LA_CONFIRM_CANCEL) return LA_FINISHED;
        return LA_RUNNING;
    }

    return LA_RUNNING;
}

int OPINV_ManagedSaveNewFile(laOperator *a, laEvent *e){
    if(MAIN.SetUDFPending) return LA_FINISHED;
    MAIN.SetUDFPending=1;
    a->CustomData = memAcquireSimple(sizeof(laUDFPreviewExtra));
    laInvoke(a, "LA_file_dialog", e, 0, "use_type=1;filter_type=1", 0);
    return LA_RUNNING;
}
int OPMOD_ManagedSaveNewFile(laOperator *a, laEvent *e){
    laUDFPreviewExtra *upe = a->CustomData;

    if (a->ConfirmData){
        if(a->ConfirmData->Mode==LA_CONFIRM_CANCEL){ MAIN.SetUDFPending=0; return LA_FINISHED; }
        if(a->ConfirmData->Mode==LA_CONFIRM_OK){
            if(a->ConfirmData->StrData){
                char* path=a->ConfirmData->StrData;
                if(la_FindManagedUDF(path)){
                    laEnableMessagePanel(a, 0, "Duplicated files", "The file you chose already exists in the managed file list.",e->x,e->y,0,e);
                    return LA_RUNNING;
                }
                laManagedUDF* m=MAIN.DummyManageUDF;
                m->udf = laPrepareUDF(path); strSafeSet(&m->BaseName, strGetLastSegment(path,LA_PATH_SEP));
                m->udf->Managed=1;
                la_MakeDummyManagedUDF();
                laNotifyUsers("la.managed_udfs"); laNotifyUsers("la.managed_props");
            }
            MAIN.SetUDFPending=0;
            return LA_FINISHED;
        }
        return LA_RUNNING;
    }

    return LA_RUNNING;
}

int OPINV_UDFManager(laOperator *a, laEvent *e){
    laRegisterModifications(0,0,0,0);
    laActivatePanel("LAUI_data_manager", e->x, e->y);
    return LA_FINISHED;
}

int OPINV_AddResourceFolder(laOperator *a, laEvent *e){
    laAddResourceFolder(0);
    laNotifyUsers("la.user_preferences.resource_folders");
    return LA_FINISHED;
}
int OPINV_RemoveResourceFolder(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED;
    laResourceFolder* rf=a->This->EndInstance;
    laRemoveResourceFolder(rf);
    laRefreshUDFRegistries();
    laNotifyUsers("la.user_preferences.resource_folders");
    return LA_FINISHED;
}

int OPCHK_Undo(laPropPack *This, laStringSplitor *ss){
    laDiff* diff=MAIN.HeadDifference; if(!diff) return 0;
    diff=diff->Item.pPrev; if(!diff) return 0;
    return 1;
}
int OPCHK_Redo(laPropPack *This, laStringSplitor *ss){
    laDiff* diff=MAIN.HeadDifference; if(!diff) return 0;
    if(diff==MAIN.Differences.pLast) return 0;
    return 1;
}
int OPINV_Undo(laOperator *a, laEvent *e){
    laUndo();
    laPrintDBInstInfo();
    return LA_FINISHED;
}
int OPINV_Redo(laOperator *a, laEvent *e){
    laRedo();
    laPrintDBInstInfo();
    return LA_FINISHED;
}

int OPINV_UDFPropagate(laOperator *a, laEvent *e){
    laProp* p=a->This?a->This->LastPs->p:0; if(!p||p->PropertyType!=LA_PROP_SUB) return LA_FINISHED;
    void* instance=a->This->EndInstance; if(!instance) return LA_FINISHED;
    laPropContainer* pc=la_EnsureSubTarget(p,instance);

    int force=0;
    if(strSame(strGetArgumentString(a->ExtraInstructionsP,"force"),"true")){ force=1; }

    laPropagateUDF(pc, instance, force);
    laNotifyUsers("la.managed_props");

    return LA_FINISHED;
}

int OPINV_Nop(laOperator *a, laEvent *e){
    return LA_FINISHED;
}

int OPINV_SendSignal(laOperator *a, laEvent *e){
    const char* sig=strGetArgumentString(a->ExtraInstructionsP,"signal"); if((!sig) || (!sig[0])) return LA_FINISHED;
    laCustomSignal* cs=laFindSignal(sig);
    if(cs){ la_SendSignalEvent(e->window->win, cs->Signal); }
    return LA_FINISHED;
}

int OPINV_NewToolbox(laOperator *a, laEvent *e){
    laNewToolbox("New Toolbox"); laNotifyUsers("la.input_mapping"); return LA_FINISHED;
}
int OPINV_RemoveToolbox(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMapping* im=a->This->EndInstance;
    char* buf[256];sprintf(buf,"%s \"%s\".",transLate("Will remove toolbox"),SSTR(im->Name));
    laEnableYesNoPanel(a, 0, "Confirm?", buf, e->x, e->y, 200, e);
    return LA_RUNNING;
}
int OPMOD_RemoveToolbox(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMapping* im=a->This->EndInstance;
    if(a->ConfirmData){
        if(a->ConfirmData->Mode == LA_CONFIRM_OK){
            laRemoveToolbox(im); laNotifyUsers("la.input_mapping");
        }
        return LA_FINISHED;
    }
    return LA_RUNNING;
}
int OPINV_RunToolboxEntry(laOperator *a, laEvent* e){
    laInputMappingEntry* ime = a->This?a->This->EndInstance:0; if(!ime) return LA_CANCELED;
    if(ime->UseOperator && ime->OperatorType){
        laInvokeP(a, ime->OperatorType, e, 0, SSTR(ime->OperatorArguments), 0);
    }else{
        la_SendSignalEvent(e->window->win, ime->SignalValue);
    }
    return LA_FINISHED;
}

int OPINV_NewInputMapping(laOperator *a, laEvent *e){
    laNewInputMapping("New Mapping"); laNotifyUsers("la.input_mapping"); return LA_FINISHED;
}
int OPINV_RemoveInputMapping(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMapping* im=a->This->EndInstance;
    char* buf[256];sprintf(buf,"%s \"%s\".",transLate("Will remove input mapping"),SSTR(im->Name));
    laEnableYesNoPanel(a, 0, "Confirm?", buf, e->x, e->y, 200, e);
    return LA_RUNNING;
}
int OPINV_NewInputMappingEntry(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMapping* im=a->This->EndInstance;
    laInputMappingEntry* ime=laNewInputMappingEntry(im,0,0,"",0,"none");
    if(strArgumentMatch(a->ExtraInstructionsP,"position","top")){
        lstRemoveItem(&im->Entries,ime);
        lstPushItem(&im->Entries,ime);
    }
    laNotifyUsers("la.input_mapping"); return LA_FINISHED;
}
int OPINV_RemoveInputMappingEntry(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMappingEntry* ie=a->This->EndInstance;
    laEnableYesNoPanel(a, 0, "Confirm?", "Will remove this key map entry", e->x, e->y, 200, e);
    return LA_RUNNING;
}
int OPINV_ClearInputMappingFields(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMappingEntry* ime=a->This->EndInstance;
    strSafeDestroy(&ime->Operator);strSafeDestroy(&ime->OperatorName);strSafeDestroy(&ime->OperatorArguments);
    strSafeDestroy(&ime->Key); strSafeDestroy(&ime->Signal);
    ime->OperatorBase=ime->OperatorType=0; ime->KeyValue=ime->SpecialKeyBits=0; ime->SignalValue=0;
    ime->JoystickDevice=ime->Axis=ime->Button=0; ime->DeviceType=0; ime->Disabled=0;
    return LA_FINISHED;
}

int OPMOD_RemoveInputMapping(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMapping* im=a->This->EndInstance;
    if(a->ConfirmData){
        if(a->ConfirmData->Mode == LA_CONFIRM_OK){
            laRemoveInputMapping(im); laNotifyUsers("la.input_mapping");
        }
        return LA_FINISHED;
    }
    return LA_RUNNING;
}
int OPMOD_RemoveInputMappingEntry(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMappingEntry* ie=a->This->EndInstance;
    if(a->ConfirmData){
        if(a->ConfirmData->Mode == LA_CONFIRM_OK){
            laRemoveInputMappingEntry(ie->Parent,ie); laNotifyUsers("la.input_mapping");
        }
        return LA_FINISHED;
    }
    return LA_RUNNING;
}

int OPINV_InputMappingEntrySelectSignal(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMappingEntry* ime=a->This->EndInstance;
    laEnableOperatorPanel(a,a->This,e->x-LA_RH*5,e->y-LA_RH,LA_RH*15,LA_RH*15,LA_RH*30,LA_RH*30,0,0,0,0,0,0,e);
    return LA_RUNNING;
}
int OPMOD_InputMappingEntrySelectSignal(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMappingEntry* ime=a->This->EndInstance;
    if(a->ConfirmData){
        if(a->ConfirmData->Mode == LA_CONFIRM_DATA && a->ConfirmData->PointerData && strSame(a->ConfirmData->PointerType->Identifier,"la_custom_signal")){
            laCustomSignal* cs=a->ConfirmData->PointerData;
            ime->SignalValue = cs->Signal; strSafeSet(&ime->Signal,SSTR(cs->Name));
            laNotifyUsers("la.input_mapping");
        }
        return LA_FINISHED;
    }
    return LA_RUNNING;
}
void laui_InputMappingEntrySignalSelector(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn* c=laFirstColumn(uil);

    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowLabel(uil,c,"🔍",0,0);
    laUiItem* ui=laShowItem(uil,c,0,"la.signal_filter");ui->Expand=1;ui->Flags|=LA_UI_FLAGS_IMMEDIATE_INPUT;
    laEndRow(uil,b);
    laUiItem* g=laMakeEmptyGroup(uil,c,0,0); laUiList* guil=g->Page; laColumn* gc=laFirstColumn(guil); guil->HeightCoeff=10;
    laShowItemFull(guil,gc,0,"la.filtered_signals",LA_WIDGET_COLLECTION,0,0,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
}

int OPINV_InputMappingEntrySelectOperator(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMappingEntry* ime=a->This->EndInstance;
    laEnableOperatorPanel(a,a->This,e->x-LA_RH*5,e->y-LA_RH,LA_RH*15,LA_RH*20,LA_RH*30,LA_RH*35,0,0,0,0,0,0,e);
    return LA_RUNNING;
}
int OPMOD_InputMappingEntrySelectOperator(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMappingEntry* ime=a->This->EndInstance;
    if(a->ConfirmData){
        if(a->ConfirmData->Mode == LA_CONFIRM_DATA && a->ConfirmData->PointerData && strSame(a->ConfirmData->PointerType->Identifier,"la_operator_type")){
            laOperatorType* at=a->ConfirmData->PointerData;
            strSafeSet(&ime->Operator,at->Identifier);
            strSafeSet(&ime->OperatorName,at->Name);
            ime->OperatorType = at;
            laNotifyUsers("la.input_mapping");
        }
        return LA_FINISHED;
    }
    return LA_RUNNING;
}
void laui_InputMappingEntryOperatorSelector(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn* c=laFirstColumn(uil);

    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowLabel(uil,c,"🔍",0,0);
    laUiItem* ui=laShowItem(uil,c,0,"la.operator_filter");ui->Expand=1;ui->Flags|=LA_UI_FLAGS_IMMEDIATE_INPUT;
    laEndRow(uil,b);
    laUiItem* g=laMakeEmptyGroup(uil,c,0,0); laUiList* guil=g->Page; laColumn* gc=laFirstColumn(guil); guil->HeightCoeff=20;
    laShowItemFull(guil,gc,0,"la.filtered_operators",LA_WIDGET_COLLECTION,0,laui_OperatorTypeEntry,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
}

STRUCTURE(laKeyDetectorData){
    int pad;
    int DeviceType;
    int Key;
    int SpecialKeyBit;
    int Button, Axis, Device;
    laSafeString* Str;
};
void OPEXT_InputMappingEntrySelectKey(laOperator* a,int exitmode){
    laKeyDetectorData* kdd=a->CustomData;
    strSafeDestroy(&kdd->Str);
    memFree(kdd);
}
int OPINV_InputMappingEntrySelectKey(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMappingEntry* ime=a->This->EndInstance;
    a->CustomData = memAcquire(sizeof(laKeyDetectorData));
    laEnableOperatorPanel(a,a->This,e->x-LA_RH*5,e->y-LA_RH,LA_RH*10,LA_RH*10,LA_RH*20,LA_RH*20,LA_RH*10,0,0,0,0,0,e);
    laOperatorModalOver(a);
    return LA_RUNNING;
}
int OPMOD_InputMappingEntrySelectKey(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_CANCELED; laInputMappingEntry* ime=a->This->EndInstance;
    laKeyDetectorData* kdd=a->CustomData; if(!kdd){ return LA_FINISHED; }
    char buf[64],*_next=buf;
    if(e->type == LA_KEY_DOWN){
        kdd->Key = e->key; la_InputMappingGetKeyName(kdd->Key,e->SpecialKeyBit,buf); strSafeSet(&kdd->Str,buf); kdd->DeviceType=LA_INPUT_DEVICE_KEYBOARD;
        laNotifyInstanceUsers(kdd); kdd->SpecialKeyBit=e->SpecialKeyBit;
        return LA_RUNNING;
    }elif(e->type == LA_EMPTY){
        if(MAIN.ControllerHasNewAxis){
            kdd->Axis = MAIN.LastControllerAxis; kdd->Device = MAIN.LastControllerAxisDevice; kdd->Button =-1; kdd->DeviceType=LA_INPUT_DEVICE_JOYSTICK;
            strSafeSet(&kdd->Str,laControllerIDGetAxisName(kdd->Device,kdd->Axis)); laNotifyInstanceUsers(kdd);
        }elif(MAIN.ControllerHasNewKey){
            kdd->Button = MAIN.LastControllerKey; kdd->Device = MAIN.LastControllerKeyDevice; kdd->Axis = -1; kdd->DeviceType=LA_INPUT_DEVICE_JOYSTICK;
            strSafeSet(&kdd->Str,laControllerIDGetButtonName(kdd->Device,kdd->Button)); laNotifyInstanceUsers(kdd);
        }
    }
    if(a->ConfirmData){
        if(a->ConfirmData->Mode == LA_CONFIRM_CUSTOM_STRING){
            char* ReportedEvent=a->ConfirmData->StrData;
            strSafeSet(&kdd->Str,ReportedEvent);
            kdd->DeviceType = LA_INPUT_DEVICE_MOUSE;
            kdd->Key = la_InputMappingGetKeyFromName(ReportedEvent,&kdd->SpecialKeyBit);
            laNotifyInstanceUsers(kdd);
            return LA_RUNNING_PASS;
        }
        if(a->ConfirmData->Mode == LA_CONFIRM_OK){
            strSafeSet(&ime->Key,SSTR(kdd->Str));
            if(kdd->DeviceType==LA_INPUT_DEVICE_JOYSTICK){ ime->DeviceType = LA_INPUT_DEVICE_JOYSTICK; ime->Axis=kdd->Axis; ime->Button=kdd->Button; ime->JoystickDevice = kdd->Device; }
            elif(kdd->DeviceType==LA_INPUT_DEVICE_MOUSE){ ime->DeviceType = LA_INPUT_DEVICE_MOUSE; ime->KeyValue = kdd->Key; ime->SpecialKeyBits=kdd->SpecialKeyBit; }
            else{ ime->DeviceType = LA_INPUT_DEVICE_KEYBOARD; ime->KeyValue = kdd->Key; ime->SpecialKeyBits=kdd->SpecialKeyBit; }
            laNotifyInstanceUsers(ime);
        }
        return LA_FINISHED_PASS;
    }
    return LA_RUNNING_PASS;
}
void laui_InputMappingEntryKeySelector(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn* c=laFirstColumn(uil);
    laUiItem* b;

    laShowLabel(uil,c,"Press a key:",0,0)->Flags|=LA_UI_FLAGS_DISABLED;
    laShowItem(uil,c,OperatorInst,"pressed_string")->Flags|=LA_UI_FLAGS_PLAIN;

    laShowLabel(uil,c,"Or click mouse button in:",0,0)->Flags|=LA_UI_FLAGS_DISABLED;
    laShowMouseActionReporter(uil,c,3,0,0)->Flags|=LA_TEXT_ALIGN_CENTER;

    b=laBeginRow(uil,c,0,0);
    laShowSeparator(uil,c)->Expand=1;
    laShowItem(uil,c,0,"LA_confirm");
    laEndRow(uil,b);
}
void laget_KeyDetectorPressedString(laKeyDetectorData* kdd, char* out, char** pivot){
    if(kdd->DeviceType==LA_INPUT_DEVICE_JOYSTICK){
        if(kdd->Axis>=0){ sprintf(out, "Controller %d axis \"%s\" (0x%0x)", kdd->Device, SSTR(kdd->Str), kdd->Axis); }
        else{ sprintf(out, "Controller %d button \"%s\" (0x%0x)", kdd->Device, SSTR(kdd->Str), kdd->Button); }
    }elif(kdd->DeviceType==LA_INPUT_DEVICE_MOUSE){
        sprintf(out, "Mouse \"%s\" (0x%0x)", SSTR(kdd->Str), kdd->Key);
    }else{
        sprintf(out, "Keyboard \"%s\" (0x%0x)", SSTR(kdd->Str), kdd->Key);
    }
}


int OPINV_SaveUserPreferences(laOperator *a, laEvent *e){
    laSaveUserPreferences(); return LA_FINISHED;
}
int OPMOD_RestoreFactorySettings(laOperator* a, laEvent* e){
    if (a->ConfirmData){
        if (a->ConfirmData->Mode == LA_CONFIRM_OK){
            if (laRestoreFactorySettings()){
                laEnableMessagePanel(0, 0, "Success", "Restart the program for changes to take effect.", e->x, e->y, 200, 0);
                MAIN.SavePreferenceOnExit = 0;
            }else{
                laEnableMessagePanel(0, 0, "Problem", "Unable to remove preference file.", e->x, e->y, 200, 0);
            }
        }
        return LA_FINISHED;
    }
    return LA_RUNNING;
}
int OPINV_RestoreFactorySettings(laOperator* a, laEvent* e){
    laEnableYesNoPanel(a, 0, "Confirm?", "This will remove the preference file.\nChanges take effect on restating the program.", e->x, e->y, 200, e);
    return LA_RUNNING;
}

int OPCHK_TerminateProgram(laPropPack *This, laStringSplitor *Instructions){
    return 1;
}
int OPINV_TerminateProgram(laOperator *a, laEvent *e){
    int empty=0; int mod=laRegisterModifications(1,1,&empty,0);
    if(mod || empty){ laInvoke(0, "LA_managed_save", 0,0,"on_exit=true;",0); return LA_FINISHED; }

    return LA_OPERATOR_CALLS_SHUTOFF;
}

int OPCHK_PropSetValue(laPropPack *This, laStringSplitor *Instructions){
    if (This && (!This->LastPs->p->ReadOnly)) return 1;
    else return 0;
}
int OPCHK_StringSetValue(laPropPack *This, laStringSplitor *Instructions){
    if (This && (This->LastPs->p->PropertyType == LA_PROP_STRING) && (!This->LastPs->p->ReadOnly)) return 1;
    else return 0;
}
int OPCHK_CollectionSetValue(laPropPack *This, laStringSplitor *Instructions){
    laSubProp*sp=This->LastPs->p;
    if (This && (This->LastPs->p->PropertyType == LA_PROP_SUB) && (!This->LastPs->p->ReadOnly) &&
        (sp->Base.UDFIsRefer && (!sp->Base.UDFIsSingle) && (sp->Base.Offset>=0||sp->Set||sp->SetState))) return 1;
    else return 0;
}
int OPINV_CollectionClearSelection(laOperator *a, laEvent *e){
    if(!a->This) return LA_CANCELED; laSubProp* sp=a->This->LastPs->p;
    laSetActiveInstance(sp, a->This->LastPs->UseInstance, 0);
    return LA_FINISHED;
}
int OPINV_PropSetDefault(laOperator *a, laEvent *e){
    if(!a->This) return LA_CANCELED; laProp* p=a->This->LastPs->p;
    if (p->PropertyType==LA_PROP_ENUM){
        laEnumProp *ep = a->This->LastPs->p; laSetEnum(a->This, ep->DefVal); }
    elif (p->PropertyType==(LA_PROP_ENUM|LA_PROP_ARRAY)){
        laEnumProp *ep = a->This->LastPs->p; laSetEnumArrayAll(a->This, ep->DefVal); }
    elif (p->PropertyType==LA_PROP_INT){
        laIntProp *ip = a->This->LastPs->p; laSetInt(a->This, ip->DefVal);}
    elif (p->PropertyType==(LA_PROP_INT|LA_PROP_ARRAY)){ laIntProp *ip = a->This->LastPs->p;
        if (ip->DefArr) laSetIntArrayAllArray(a->This, ip->DefArr); else laSetIntArrayAll(a->This, ip->DefVal); }
    elif (p->PropertyType==LA_PROP_FLOAT){
        laFloatProp *ip = a->This->LastPs->p; laSetFloat(a->This, ip->DefVal); }
    elif (p->PropertyType==(LA_PROP_FLOAT|LA_PROP_ARRAY)){ laFloatProp *ip = a->This->LastPs->p;
        if (ip->DefArr) laSetFloatArrayAllArray(a->This, ip->DefArr); else laSetFloatArrayAll(a->This, ip->DefVal); }
    elif (p->PropertyType==LA_PROP_STRING){laStringProp *sp = a->This->LastPs->p;
        if (sp->DefStr) laSetString(a->This, sp->DefStr); else laSetString(a->This, ""); }
    return LA_FINISHED;
}
int OPINV_PropSetMax(laOperator *a, laEvent *e){
    if(!a->This) return LA_CANCELED; laProp* p=a->This->LastPs->p;
    if (p->PropertyType==LA_PROP_INT){
        laIntProp *ip = a->This->LastPs->p; laSetInt(a->This, ip->Max); }
    elif (p->PropertyType==(LA_PROP_INT|LA_PROP_ARRAY)){ 
        laIntProp *ip = a->This->LastPs->p; laSetIntArrayAll(a->This, ip->Max); }
    elif (p->PropertyType==LA_PROP_FLOAT){
        laFloatProp *ip = a->This->LastPs->p; laSetFloat(a->This, ip->Max); }
    elif (p->PropertyType==(LA_PROP_FLOAT|LA_PROP_ARRAY)){
        laFloatProp *ip = a->This->LastPs->p; laSetFloatArrayAll(a->This, ip->Max); }
    return LA_FINISHED;
}
int OPINV_PropSetMin(laOperator *a, laEvent *e){
    if(!a->This) return LA_CANCELED; laProp* p=a->This->LastPs->p;
    if (p->PropertyType==LA_PROP_INT){
        laIntProp *ip = a->This->LastPs->p; laSetInt(a->This, ip->Min); }
    elif (p->PropertyType==(LA_PROP_INT|LA_PROP_ARRAY)){ 
        laIntProp *ip = a->This->LastPs->p; laSetIntArrayAll(a->This, ip->Min); }
    elif (p->PropertyType==LA_PROP_FLOAT){
        laFloatProp *ip = a->This->LastPs->p; laSetFloat(a->This, ip->Min); }
    elif (p->PropertyType==(LA_PROP_FLOAT|LA_PROP_ARRAY)){
        laFloatProp *ip = a->This->LastPs->p; laSetFloatArrayAll(a->This, ip->Min); }
    return LA_FINISHED;
}

int OPINV_StringGetFolderPath(laOperator *a, laEvent *e){
    if (a->This && (a->This->LastPs->p->PropertyType & LA_PROP_STRING)){

        laInvoke(a, "LA_file_dialog", e, 0, "select=folder;", 0);

        return LA_RUNNING;
    }
    return LA_FINISHED;
}
int OPINV_StringGetFilePath(laOperator *a, laEvent *e){
    if (a->This && (a->This->LastPs->p->PropertyType & LA_PROP_STRING)){

        laInvoke(a, "LA_file_dialog", e, 0, 0, 0);

        return LA_RUNNING;
    }
    return LA_FINISHED;
}
int OPMOD_StringGetFolderOrFilePath(laOperator *a, laEvent *e){
    if (a->ConfirmData){
        if (a->ConfirmData->StrData){
            laStringProp *sp = a->This->LastPs->p;

            laSetString(a->This, a->ConfirmData->StrData);

            return LA_FINISHED_PASS;
        }
        return LA_FINISHED_PASS;
    }
    return LA_RUNNING;
}

int OPINV_StringCopy(laOperator *a, laEvent *e){
    laPropPack* pp=a->This; if(!a->This) return LA_FINISHED;

    char _buf[256]={0}; char* buf=_buf;
    laGetString(pp,_buf,&buf);
    laCopyToClipboard(buf);

    return LA_FINISHED;
}
int OPINV_StringPaste(laOperator *a, laEvent *e){
    if (a->This && (a->This->LastPs->p->PropertyType & LA_PROP_STRING)){
        laStringProp *sp = a->This->LastPs->p;
        if (sp->DefStr) laSetString(a->This, sp->DefStr);
        else laSetString(a->This, "");
    }
    return LA_FINISHED;
}

int OPCHK_CombineChildBlocks(laPropPack *This, laStringSplitor *Instructions){
    laLayout *l = MAIN.CurrentWindow->CurrentLayout;
    laBlock *b = l->OnBlockSeperator;
    if (!(b)) return 0;
    return 1;
}
int OPINV_CombineChildBlocks(laOperator *a, laEvent *e){
    laLayout *l = MAIN.CurrentWindow->CurrentLayout;
    laBlock *b = l->OnBlockSeperator;
    if (!b) b = laDetectBlockRecursive(l->FirstBlock, e->x, e->y);
    if (b){
        laCombineChildBlocks(b);
        return LA_FINISHED;
    }
    return LA_CANCELED;
}

int OPCHK_PropInsertKey(laPropPack *This, laStringSplitor *Instructions){
    if (MAIN.Animation->CurrentAction && This && This->LastPs->p->Keyable) return 1;
    else return 0;
}
int OPINV_PropInsertKey(laOperator *a, laEvent *e){
    int err; laAction* aa=MAIN.Animation->CurrentAction;
    laAnimationInsertKeyFrame(aa,a->This->LastPs->UseInstance,a->This->LastPs->p,&err);
    if(err==1){
        laEnableMessagePanel(a,0,"Error","Action holder info missing.\nThis should not happen.",e->x,e->y,100,e);
    }elif(err==2){
        char msg[1024]; char _id[128]="unknown container",*id=_id,_idc[128]="unknown object",*idc=_idc;
        laTryGetInstanceIdentifier(aa->HolderInstance,aa->HolderContainer,_id,&id);
        laTryGetInstanceIdentifier(a->This->LastPs->UseInstance,a->This->LastPs->p->Container,_idc,&idc);
        sprintf(msg,"Can't insert key frame into current action.\n\"%s\" doesn't belong to \"%s\".",idc,id);
        laEnableMessagePanel(a,0,"Error",msg,e->x,e->y,100,e);
    }
    return LA_FINISHED;
}

STRUCTURE(laNewPanelData){
    laUiTemplate* SelectedTemplate;
    laBlock* b;
};

void laui_TitleOnly(laUiList *uil, laPropPack *This, laPropPack *OP_UNUSED, laColumn *Extra, int context){
    laColumn *col = Extra, *c, *cl, *cr, *crl, *crr, *cll, *clr, *clrl, *clrr, *clrrl, *clrrr;
    laUiItem *ui;
    c = laFirstColumn(uil);
    laShowItemFull(uil, c, This, "title", LA_WIDGET_STRING_PLAIN, 0, 0, 0);
}
laUiTemplate* laget_FirstPanelTemplate(void* unused1, void* unused2){
    return MAIN.PanelTemplates.pFirst;
}
void laset_NewPanelSetTemplate(laNewPanelData *np, laUiTemplate *uit, int State){
    np->SelectedTemplate = uit;
}
laUiTemplate* laget_NewPanelGetActiveTemplate(laNewPanelData* np, laUiTemplate* uit){
    return np->SelectedTemplate;
}
void laui_PanelTemplateSelect(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED, int context){
    laColumn *c = laFirstColumn(uil);
    laUiItem* ui=laShowItem(uil, c, OperatorProps, "template");ui->Extent=2; ui->Flags|=LA_UI_FLAGS_NO_DECAL;
}

int OPINV_Fullscreen(laOperator *a, laEvent *e){
    laWindow* w=MAIN.CurrentWindow;
    int full=1;
    if(strArgumentMatch(a->ExtraInstructionsP,"restore","true")){ full=0; }
    if(strArgumentMatch(a->ExtraInstructionsP,"toggle","true")){ full=w->IsFullScreen?0:1; }
#ifdef LA_LINUX
    XClientMessageEvent msg = {
        .type = ClientMessage, .display = MAIN.dpy, .window = w->win,
        .message_type = XInternAtom(MAIN.dpy, "_NET_WM_STATE", True), .format = 32,
        .data = {.l = {full, XInternAtom(MAIN.dpy, "_NET_WM_STATE_FULLSCREEN", True), None, 0, 1 }}
    };
    XSendEvent(MAIN.dpy, XRootWindow(MAIN.dpy, XDefaultScreen(MAIN.dpy)), False, SubstructureRedirectMask | SubstructureNotifyMask, (XEvent*) &msg);
#endif
#ifdef _WIN32
    DWORD dwStyle = GetWindowLong(w->win, GWL_STYLE);
    static WINDOWPLACEMENT g_wpPrev = { sizeof(g_wpPrev) };
    if (full){
        MONITORINFO mi = { sizeof(mi) };
        if (GetWindowPlacement(w->win, &g_wpPrev) &&
            GetMonitorInfo(MonitorFromWindow(w->win,
                MONITOR_DEFAULTTOPRIMARY), &mi)) {
            SetWindowLong(w->win, GWL_STYLE,
                dwStyle & ~WS_OVERLAPPEDWINDOW);
            SetWindowPos(w->win, HWND_TOP,
                mi.rcMonitor.left, mi.rcMonitor.top,
                mi.rcMonitor.right - mi.rcMonitor.left,
                mi.rcMonitor.bottom - mi.rcMonitor.top,
                SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
        }
    }else{
        SetWindowLong(w->win, GWL_STYLE,dwStyle|WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(w->win, &g_wpPrev);
        SetWindowPos(w->win,NULL,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_FRAMECHANGED);
    }
#endif
    w->IsFullScreen=full;
    laRedrawCurrentWindow();
    return LA_FINISHED;
}
int OPINV_NewLayout(laOperator *a, laEvent *e){
    laWindow* w=MAIN.CurrentWindow;
    laDesignLayout(w, "New Layout");
    laRedrawCurrentWindow();
    return LA_FINISHED;
}
int OPCHK_RemoveLayout(laPropPack *This, laStringSplitor *Instructions){
    laWindow* w=MAIN.CurrentWindow; if(w->Layouts.pFirst==w->Layouts.pLast) return 0;
    return 1;
}
int OPINV_RemoveLayout(laOperator *a, laEvent *e){
    laWindow* w=MAIN.CurrentWindow; if(w->Layouts.pFirst==w->Layouts.pLast) return LA_FINISHED;
    laDestroyLayout(w,w->CurrentLayout); laRedrawCurrentWindow();
    return LA_FINISHED;
}
int OPINV_NewPanel(laOperator *a, laEvent *e){
    laNewPanelData* np= memAcquire(sizeof(laNewPanelData));
    a->CustomData = np;
    laEnableOperatorPanel(a, 0, e->x-50,e->y-50,500,500,10000,0,0,0,0,0,0,0,e);
    return LA_RUNNING;
}
int OPMOD_NewPanel(laOperator *a, laEvent *e){
    laLayout *l = MAIN.CurrentWindow->CurrentLayout;
    laNewPanelData* np=a->CustomData;

    if(!a->ConfirmData) return LA_RUNNING;

    if(a->ConfirmData->Mode == LA_CONFIRM_CANCEL||a->ConfirmData->Mode == LA_CONFIRM_OK){ if(np) memFree(np);  return LA_CANCELED; }

    if(a->ConfirmData->Mode == LA_CONFIRM_DATA){
        if (!np || !np->SelectedTemplate){ if(np) memFree(np); return LA_CANCELED; }
        laPanel *p = la_FindFreePanelByTemplate(MAIN.CurrentWindow, np->SelectedTemplate);
        if (!p){
            p = laCreateTopPanel(MAIN.CurrentWindow, np->SelectedTemplate->Identifier->Ptr, e->x, e->y,0,0,0,0,0,0,0,0,0,0);
        }
        laShowPanelWithExpandEffect(p); laPopPanel(p); memFree(np);
        return LA_FINISHED;
    }

    return LA_RUNNING;
}
int OPINV_BlockFoldTitle(laOperator *a, laEvent *e){
    laBlock* b=a->This?a->This->EndInstance:0;
    if(!b) return LA_CANCELED;

    if(strSame(strGetArgumentString(a->ExtraInstructionsP, "show"), "true")) laUnfoldBlockTitle(b);
    else laFoldBlockTitle(b);
    
    return LA_FINISHED;
}
int OPINV_BlockMaximize(laOperator *a, laEvent *e){
    laBlock* b=a->This?a->This->EndInstance:0;

    if(!b || strSame(strGetArgumentString(a->ExtraInstructionsP, "restore"), "true")) laRestoreToLayout();
    else laMaximizeBlock(b);
    
    return LA_FINISHED;
}
int OPINV_CanvasUiMaximize(laOperator *a, laEvent *e){
    laUiItem* ui=a->This?a->This->EndInstance:0;
    laPanel* p=MAIN.CurrentPanel;

    if(!ui || !p || (!ui->Type->Tag&LA_UI_TAG_IS_OFFSCREEN) || MAIN.CurrentWindow->MaximizedUi ||
        strSame(strGetArgumentString(a->ExtraInstructionsP, "restore"), "true")) laRestoreCanvasUI();
    else laMaximizeCanvasUI(ui,p);
    
    return LA_FINISHED;
}
int OPINV_HideMenuBar(laOperator *a, laEvent *e){
    if(!MAIN.CurrentWindow->MaximizedUi || strSame(strGetArgumentString(a->ExtraInstructionsP, "restore"), "true")) laShowMenuBar();
    else laHideMenuBar();
    
    return LA_FINISHED;
}
int OPINV_BlockTearOffPanel(laOperator *a, laEvent *e){
    laLayout *l = MAIN.CurrentWindow->CurrentLayout;
    laBlock* b = l->OperatingBlock;
    if (!b) b = laDetectBlockRecursive(l->FirstBlock, e->x, e->y);
    if(!b) return LA_CANCELED;
    
    laTearOffPanel(b,0);
    
    return LA_FINISHED;
}
int OPCHK_BlockHasMorePanels(laPropPack *This, laStringSplitor *Instructions){
    laPanel*p = This?This->LastPs->UseInstance:0;
    if(p && p->Mode == LA_PANEL_FLOATING_TOP) return 1;

    laLayout *l = MAIN.CurrentWindow->CurrentLayout;
    laBlock *b = l->OperatingBlock;
    if (b && b->Panels.pFirst == b->Panels.pLast) return 0;
    return 1;
}
int OPINV_BlockClosePanel(laOperator *a, laEvent *e){
    laPanel*p = a->This?a->This->EndInstance:0;
    if(p && p->Mode == LA_PANEL_FLOATING_TOP){
        laDestroySinglePanel(p,0);
        return LA_FINISHED;
    }

    laLayout *l = MAIN.CurrentWindow->CurrentLayout;
    laBlock *b = l->OperatingBlock;
    int x = e->x, y = e->y;
    if (!b) b = laDetectBlockRecursive(l->FirstBlock, e->x, e->y);
    if (b){
        if (b->CurrentPanel){
            //laLocalToWindow(a, MAIN.CurrentPanel, &x, &y);
            laEnableYesNoPanel(a, 0, "Caution!", "Are you sure to destroy this panel?", e->x - 250, e->y, 250, e);
            return LA_RUNNING;
        }
    }
    return LA_CANCELED;
}
int OPMOD_BlockClosePanel(laOperator *a, laEvent *e){
    laLayout *l = MAIN.CurrentWindow->CurrentLayout;
    laBlock *b = l->OperatingBlock;
    if (e->type == LA_KEY_DOWN && e->key==LA_KEY_ESCAPE) return LA_FINISHED;
    if (a->ConfirmData){
        if (!b) b = laDetectBlockRecursive(l->FirstBlock, e->x, e->y);
        if (!b) return LA_CANCELED;
        if (a->ConfirmData->Mode == LA_CONFIRM_OK){
            laDestroySinglePanel(b->CurrentPanel,1);
            la_RecalcBlockRecursive(b, b->X, b->Y, b->W, b->H);
        }
        return LA_FINISHED;
    }else
        return LA_RUNNING;
}
void laui_BlockEdgeMenu(laOperator *WindowAct, laLayout *l, laBlock *b, laEvent *e){
    laPanel *p;
    laColumn *c, *cl, *cr;
    laUiList *uil;
    p = laDefineAndEnableMenuPanel(0, WindowAct, 0, e->x, e->y, 500, 200, e);
    uil = laPrepareUi(p);
    c = laFirstColumn(uil);
    laSplitColumn(uil, c, 0.5);
    cl = laLeftColumn(c, 0);
    cr = laRightColumn(c, 0);

    laShowItem(uil, c, 0, "LA_combine_child_blocks");

    laEnclosePanelContent(p,uil);
}
laBlock* la_MakeDropBlock(laBlock* DropToBlock, int DropLocation){
    switch (DropLocation){
    case 0: return DropToBlock;
    case LA_BLOCK_DROP_LOCATION_U:
        laSplitBlockVertical(DropToBlock, 0.5); laSwapSubBlocks(DropToBlock);
        return DropToBlock->B1;
    case LA_BLOCK_DROP_LOCATION_B:
        laSplitBlockVertical(DropToBlock, 0.5);
        return DropToBlock->B2;
    case LA_BLOCK_DROP_LOCATION_L:
        laSplitBlockHorizon(DropToBlock, 0.5); laSwapSubBlocks(DropToBlock);
        return DropToBlock->B1;
    case LA_BLOCK_DROP_LOCATION_R:
        laSplitBlockHorizon(DropToBlock, 0.5);
        return DropToBlock->B2;
    }
    return DropToBlock;
}
void la_StartDocking(laWindow* from, laPanel* p){
    for(laWindow* w=MAIN.Windows.pFirst;w;w=w->Item.pNext){  w->DockingFrom = from; w->IsDocking=1; } MAIN.DockingPanel=p;
}
void la_StopDocking(){
    for(laWindow* w=MAIN.Windows.pFirst;w;w=w->Item.pNext){  w->DockingFrom = 0; w->IsDocking=0; } MAIN.DockingPanel=0;
}
void la_ClearDockingTarget(){
    for(laWindow* w=MAIN.Windows.pFirst;w;w=w->Item.pNext){  w->CurrentLayout->DropToBlock=0; }
}
int OPINV_DockPanel(laOperator* a, laEvent* e){
    laPanel*p = a->This?a->This->EndInstance:0;
    if(!p) return LA_CANCELED;

    la_StartDocking(MAIN.CurrentWindow, p);
    laRestoreToLayout();
    laRestoreCanvasUI();

    return LA_FINISHED;
}
int OPINV_NewWindow(laOperator* a, laEvent* e){
    laWindow* w = a->This?a->This->EndInstance:MAIN.CurrentWindow;
    
    laWindow* nw =laDesignWindow(w->X,w->Y,w->W,w->H);
    laLayout* l=(w->Layouts.pFirst!=w->Layouts.pLast)?w->CurrentLayout:0;

    if (l && !strSame(strGetArgumentString(a->ExtraInstructionsP, "clean"), "true")){
        laLayout* nextl=l->Item.pPrev?l->Item.pPrev:l->Item.pNext;
        memAssignRef(w, &w->CurrentLayout, nextl);
        lstRemoveItem(&w->Layouts, l); lstAppendItem(&nw->Layouts, l);
        memAssignRef(nw, &nw->CurrentLayout, l);
        laRenameWindow(nw, l->ID->Ptr);
        laRenameWindow(w, w->CurrentLayout->ID->Ptr);
    }else{
        laDesignLayout(nw, "Empty Layout");
    }
    laStartWindow(nw);
    laRedrawAllWindows();

    return LA_FINISHED;
}

void *la_OnBlockFoldedHeader(laBlock *b, laEvent *e){
    int at;
    laBlock *bb=0; int NoSubs = (b->B1==0);
    if(NoSubs){
        if (e->x>=b->X && e->x<=b->X+b->W && e->y>=b->Y-LA_SEAM_W && e->y<=b->Y+LA_SEAM_W){
            return b;
        }
        return 0;
    }
    bb=la_OnBlockFoldedHeader(b->B1,e); if(bb) return bb;
    bb=la_OnBlockFoldedHeader(b->B2,e); if(bb) return bb;
    return 0;
}
void *la_OnBlockSeperator(laBlock *b, laEvent *e){
    int at;
    laBlock *bb;
    if (!b->B1) return 0;
    if (b->Vertical){
        at = b->H * b->SplitRatio + b->Y;
        if (e->x>=b->X && e->x<=b->X+b->W && e->y >= at - LA_SEAM_W && e->y <= at + LA_SEAM_W){
            return b;
        }
    }else{
        at = b->X + b->W * b->SplitRatio;
        if (e->y>=b->Y && e->y<=b->Y+b->H && e->x >= at - LA_SEAM_W && e->x <= at + LA_SEAM_W){
            return b;
        }
    }
    if (bb = la_OnBlockSeperator(b->B1, e)) return bb;
    else return la_OnBlockSeperator(b->B2, e);
}
int la_DetectBlockDropLocation(laBlock *b, int X, int Y){
    laPanel *p = b->Panels.pFirst;
    if (!p) return 0;

    real L = tnsGetRatiod(p->X, p->X + p->W, X);
    real U = tnsGetRatiod(p->Y, p->Y + p->H, Y);
    if (L > U){
        if (U < 0.25){
            if (L < 0.75) return LA_BLOCK_DROP_LOCATION_U;
            else
                return LA_BLOCK_DROP_LOCATION_R;
        }else{
            if (L < 0.75) return 0;
            else
                return LA_BLOCK_DROP_LOCATION_R;
        }
    }else{
        if (U > 0.75){
            if (L < 0.25) return LA_BLOCK_DROP_LOCATION_L;
            else
                return LA_BLOCK_DROP_LOCATION_B;
        }else{
            if (L < 0.25) return LA_BLOCK_DROP_LOCATION_L;
            else
                return 0;
        }
    }
}
void laui_BlockMenu(laOperator *WindowAct, laWindow* w, laLayout *l, laBlock *b, laEvent *e){
    laPanel *p;
    laColumn *c;
    laUiList *uil;
    p = laDefineAndEnableMenuPanel(0, WindowAct, 0, b->X, b->Y + LA_RH + 1, 500, 500, e);
    uil = laPrepareUi(p);
    c = laFirstColumn(uil);
    laPanel* forp=b->CurrentPanel;

    laUiItem* b1=laBeginRow(uil,c,0,0);
    laShowItem(uil,c,&b->PP,"fold");
    laUiItem* b2=laOnConditionThat(uil,c,laPropExpression(&w->PP, "maximized_block"));{
        laShowItemFull(uil,c,&b->PP,"maximize",0,"restore=true;text=Restore;",0,0);
    }laElse(uil,b2);{
        laShowItem(uil,c,&b->PP,"maximize");
    }laEndCondition(uil,b2);
    laEndRow(uil,b1);

    laShowSeparator(uil,c);

    if(forp->PanelTemplate && forp->PanelTemplate->Header){
        laShowLabel(uil, c, "Panel properties:", 0, 0);
        forp->PanelTemplate->Header(uil, &forp->PP, &forp->PropLinkPP, c, 0);
    }else{
        if(forp->PropLinkPP.LastPs->p->SubProp->Props.pFirst){
            for(laProp* p = forp->PropLinkPP.LastPs->p->SubProp->Props.pFirst;p;p=p->Item.pNext){
                if(p->PropertyType&LA_PROP_SUB){
                    laShowItemFull(uil, c, &forp->PropLinkPP, p->Identifier, LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0);
                }else{ laShowItem(uil, c, &forp->PropLinkPP, p->Identifier); }
            }
        }else{
            laShowLabel(uil, c, "Panel doesn't have configurable property.", 0, 0);
        }
    }

    //laShowItem(uil, c, 0, "LA_new_panel");
    //laShowItem(uil, c, 0, "LA_block_close_panel");
    //laShowItem(uil, c, 0, "LA_block_tear_off_panel");

    laEnclosePanelContent(p,uil);
}
int la_ProcessBlockEvent(laOperator *a, laBlock *b, laEvent *e){
    int Executed = 0, Ret=1;
    int L = LA_RH+LA_SEAM_W, R, tw = 0;
    real ratio = 1.0f;
    laPanel *p = b->Panels.pFirst;
    laGeneralUiExtraData *uid = a->CustomData;
    laLayout *l;
    if(!b->Panels.pFirst){return 0;}
    if (laIsInBlockHeader(b, e->x, e->y) || MAIN.CurrentWindow->CurrentLayout->ClickedPanel){
        MAIN.CurrentWindow->CurrentLayout->OperatingBlock = b;

        if (b->CurrentPanel){
            if (e->type == LA_MOUSE_WHEEL_DOWN && b->CurrentPanel->Item.pNext){
                b->CurrentPanel = b->CurrentPanel->Item.pNext; Executed = 1;
            }elif (e->type == LA_MOUSE_WHEEL_UP && b->CurrentPanel->Item.pPrev){
                b->CurrentPanel = b->CurrentPanel->Item.pPrev; Executed = 1;
            }
        }

        if (e->type == LA_MOUSEMOVE){
            if (MAIN.CurrentWindow->CurrentLayout->ClickedPanel){
                if (abs(e->x - uid->LastX) > LA_RH*3 || abs(e->y - uid->LastY) > LA_RH*3){
                    l = MAIN.CurrentWindow->CurrentLayout;
                    l->DropToBlock = b;
                    l->DropLocation = la_DetectBlockDropLocation(b, e->x, e->y);
                    laRefreshWindow();
                }
            }
        }elif (e->type == LA_L_MOUSE_UP){
            l = MAIN.CurrentWindow->CurrentLayout;
            if (l->DropToBlock){
                l->DropToBlock = la_MakeDropBlock(l->DropToBlock,l->DropLocation);
                p = l->ClickedPanel;
                if(p->Block == l->DropToBlock){
                    if(laTearOffPanel(p->Block, p)) Executed=1;
                }else{
                    p->Block->CurrentPanel = p->Item.pPrev ? p->Item.pPrev : (p->Item.pNext ? p->Item.pNext : 0);
                    lstRemoveItem(&p->Block->Panels, p); laBlock* orig_block=p->Block->parent;
                    int clear=0; if(!lstHaveItemInList(&p->Block->Panels)){clear=1;}
                    lstPushItem(&l->DropToBlock->Panels, p);
                    laUnfoldBlockTitle(l->DropToBlock);
                    p->Block = l->DropToBlock;
                    if(clear){
                        laCombineChildBlocks(orig_block);
                        if(!lstFindItem(orig_block->CurrentPanel,nutSameAddress,&orig_block->Panels)){
                            orig_block->CurrentPanel = orig_block->Panels.pFirst;
                        }
                    }
                    p->Block->CurrentPanel = p;
                    Executed = 1;
                    la_RecalcBlockRecursive(p->Block, p->Block->X, p->Block->Y, p->Block->W, p->Block->H);
                    if(orig_block!=p->Block){
                        la_RecalcBlockRecursive(orig_block, orig_block->X, orig_block->Y, orig_block->W, orig_block->H);
                    }
                }
            }
            MAIN.CurrentWindow->CurrentLayout->DropToBlock = 0;
            MAIN.CurrentWindow->CurrentLayout->ClickedPanel = 0;
            return 0;
        }

        if(!b->Folded){
            for (p = b->Panels.pFirst; p; p = p->Item.pNext){ tw += (p->TitleWidth + LA_SEAM_W*2); }
            if (tw > b->W - LA_SEAM_W*2 - LA_RH) ratio = (real)(b->W - LA_SEAM_W*2 - LA_RH) / tw;

            if ((e->type == LA_L_MOUSE_DOWN)){
                uid->LastX = e->x;
                uid->LastY = e->y;
                int LT=0,RT=0;
                for (p = b->Panels.pFirst; p; p = p->Item.pNext){
                    RT = LT + p->TitleWidth+LA_SEAM_W*2;
                    if (e->x >= b->X + L+LT * ratio && e->x < b->X + L+RT * ratio){
                        MAIN.CurrentWindow->CurrentLayout->ClickedPanel = p; b->CurrentPanel = p; Executed = 1;
                    }
                    LT = RT;
                }
            }

            if (!Executed && e->type == LA_L_MOUSE_DOWN && laIsInBlockBotton1(b, e->x, e->y)){
                laui_BlockMenu(a, MAIN.CurrentWindow, MAIN.CurrentWindow->CurrentLayout, b, e);
            }
        }

        if (Executed){
            laRecalcPanel(b->CurrentPanel);
            la_RecalcBlockRecursive(b, b->X, b->Y, b->W, b->H);
        }
        return Ret;
    }
    return 0;
}
int la_ProcessBlockEdgeEvent(laOperator *WindowAct, laLayout *l, laBlock *b, laEvent *e){
    laBlock *ob = l->MovingBlock;
    laWindow *w = WindowAct->Instance;

    if (e->type == LA_MOUSEMOVE && ob && l->MovingBlock && l->IsMoving==2){
        if (ob->Vertical) ob->SplitRatio = (real)(e->y - ob->Y) / (real)(ob->H);
        else ob->SplitRatio = (real)(e->x - ob->X) / (real)(ob->W);
        la_RecalcBlockRecursive(ob, ob->X, ob->Y, ob->W, ob->H);
        if (ob->B1->CurrentPanel) laRecalcPanel(ob->B1->CurrentPanel);
        if (ob->B2->CurrentPanel) laRecalcPanel(ob->B2->CurrentPanel);
        return 1;
    }
    laBlock* header_block=0;
    if((header_block=la_OnBlockFoldedHeader(b,e)) && (header_block->Folded)){
        if (header_block->CurrentPanel){
            if (e->type == LA_MOUSE_WHEEL_DOWN && header_block->CurrentPanel->Item.pNext){
                header_block->CurrentPanel = header_block->CurrentPanel->Item.pNext; laRedrawCurrentWindow(); return 1;
            }elif (e->type == LA_MOUSE_WHEEL_UP && header_block->CurrentPanel->Item.pPrev){
                header_block->CurrentPanel = header_block->CurrentPanel->Item.pPrev; laRedrawCurrentWindow(); return 1;
            }
        }

        if(l->IsBlockHeaderClicked==1){ header_block=l->HeaderBlock; }
        if(l->HeaderBlock!=header_block){ laRefreshWindow(); }
        l->HeaderBlock=header_block;
        if (e->type == LA_L_MOUSE_DOWN){
            if((l->IsBlockHeaderClicked==1) && (tnsDistIdv2(l->LastX,l->LastY,e->x,e->y)<=LA_SEAM_W)){
                l->IsBlockHeaderClicked=0; l->IsMoving=0; laUnfoldBlockTitle(header_block); return 1;
            }
            if(!l->IsBlockHeaderClicked){ l->IsBlockHeaderClicked = 1; l->LastX=e->x; l->LastY=e->y; laRefreshWindow(); }
        }
    }else{
        l->IsBlockHeaderClicked = 0; l->HeaderBlock=0; laRefreshWindow();
    }
    if ((l->IsMoving==1) || (ob = la_OnBlockSeperator(b, e))){
        if(l->IsMoving==1){ ob=l->MovingBlock; }
        if(l->MovingBlock!=ob){ laRefreshWindow(); }
        l->MovingBlock = ob; laSetWindowCursor(ob->Vertical?LA_UP_AND_DOWN:LA_LEFT_AND_RIGHT);
        if (e->type == LA_L_MOUSE_DOWN){
            if(!l->IsMoving){ l->IsMoving = 1; l->LastX=e->x; l->LastY=e->y; }
        }elif (e->type == LA_MOUSEMOVE){
            if(l->IsMoving==1){
                if(ob->Vertical){ if(abs(e->y-l->LastY)>2*LA_SEAM_W){ l->IsMoving=2; } }
                else{ if(abs(e->x-l->LastX)>2*LA_SEAM_W){ l->IsMoving=2; } }
            }
        }elif (e->type == LA_L_MOUSE_UP){
            l->IsMoving = 0; laRefreshWindow();
        }elif (e->type == LA_R_MOUSE_DOWN){
            l->OnBlockSeperator = ob;
            laui_BlockEdgeMenu(WindowAct, l, ob, e);
        }
        return 1;
    }
    
    l->IsMoving = 0;
    if(l->MovingBlock){ l->MovingBlock = 0; laRefreshWindow(); laSetWindowCursor(LA_ARROW); }
    return 0;
}

real la_ScrollerVerticalPan(int MousePanY, laPanel *p, laUiList *suil, laUiItem *pui){
    int DisplayH, TotalH;

    if (!pui){ DisplayH = p->H-suil->U; }
    else{ DisplayH = pui->Page?(pui->B-pui->Page->U):(pui->B - pui->U); }

    TotalH = suil->B - suil->U;

    return ((real)TotalH * (real)MousePanY / (real)DisplayH);
}
real la_ScrollerHorizontalPan(int MousePanX, laPanel *p, laUiList *suil, laUiItem *pui){
    int DisplayW, TotalW;

    DisplayW = pui->Page?(pui->R-pui->Page->L):(pui->R - pui->L);
    TotalW = suil->R - suil->L;

    return ((real)TotalW * (real)MousePanX / (real)DisplayW);
}

void laui_LayoutCycle(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED, int context){
    laColumn *c;

    c = laFirstColumn(uil);
    
    laShowItemFull(uil, c, 0, "la.windows.layouts", 0, 0, laui_IdentifierOnly, 0);
    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowItem(uil, c, 0, "LA_new_layout")->Expand=1;
    laShowItem(uil, c, 0, "LA_remove_layout")->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_NO_CONFIRM;
    laEndRow(uil,b);
}
int OPINV_SwitchLayout(laOperator *a, laEvent *e){
    laWindow *w = MAIN.CurrentWindow; if (!w) return LA_FINISHED;

    if (strSame(strGetArgumentString(a->ExtraInstructionsP, "show_list"), "true")){
        laPanel* p =laEnableOperatorPanel(a, 0, e->x-LA_RH, e->y-LA_RH, 500, 500, 0,0,0,0,0,0,0,0,e);
        return LA_RUNNING;
    }

    laLayout* newlayout;
    if (strSame(strGetArgumentString(a->ExtraInstructionsP, "reverse"), "true")) newlayout = w->CurrentLayout->Item.pPrev ? w->CurrentLayout->Item.pPrev : w->Layouts.pLast;
    else newlayout = w->CurrentLayout->Item.pNext ? w->CurrentLayout->Item.pNext : w->Layouts.pFirst;
    memAssignRef(w, &w->CurrentLayout, newlayout);

    laRenameWindow(w, SSTR(w->CurrentLayout->ID));

    la_StopAllOperators();
    laRetriggerOperators();

    laRedrawCurrentWindow();
    laNotifyUsers("la.windows.layouts");

    return LA_FINISHED;
}

int OPINV_DeleteTheme(laOperator *a, laEvent *e){
    laTheme* th=a->This?a->This->EndInstance:MAIN.CurrentTheme;
    char* buf[256];sprintf(buf,"%s \"%s\".",transLate("Will delete theme"),SSTR(th->Name));
    laEnableYesNoPanel(a, 0, "Confirm?", buf, e->x-LA_RH*5, e->y-LA_RH*2, 200, e);
    return LA_RUNNING;
}
int OPMOD_DeleteTheme(laOperator *a, laEvent *e){
    laTheme* th=a->This?a->This->EndInstance:MAIN.CurrentTheme;
    if(a->ConfirmData){
        if(a->ConfirmData->Mode == LA_CONFIRM_OK){
            laTheme* th=a->This?a->This->EndInstance:MAIN.CurrentTheme;
            if(!th || MAIN.Themes.pFirst == MAIN.Themes.pLast) return LA_CANCELED;
            la_DestroyTheme(th); la_RefreshThemeColor(MAIN.CurrentTheme);
            laNotifyUsers("la.themes"); laRedrawAllWindows();
        }
        return LA_FINISHED;
    }
    return LA_RUNNING;
}
int OPINV_NewTheme(laOperator *a, laEvent *e){
    if(strArgumentMatch(a->ExtraInstructionsP,"duplicate","true")){
        laTheme* th=a->This?a->This->EndInstance:MAIN.CurrentTheme; if(!th) return LA_CANCELED;
        laTheme* newth=laDuplicateTheme(th);
        laNotifyUsers("la.themes");
        return LA_FINISHED;
    }
    la_CreateClassicDarkTheme();
    laNotifyUsers("la.themes");
    return LA_FINISHED;
}

int la_GenericTopPanelProcessing(laOperator* a, laEvent* e){
    laPanel* p;
    if (p = laDetectPanel(e->x, e->y)){
        int x = e->x;
        int y = e->y;
        laWindowToLocal(0, p, &x, &y);
        if (laIsInPanel(p, x, y) /*&& p->Show*/){
            laSetOperatorLocalizer(p);
            laInvokeUi(a, "LA_panel_operator", e, p, 0, 1);
            return LA_RUNNING;
        }
    }
    return 0;
}
void la_GeneratePasteEvent(laWindow* w){
#ifdef LA_LINUX
    XConvertSelection(MAIN.dpy, MAIN.bufid, MAIN.fmtid, MAIN.propid, w->win, CurrentTime);
#endif
#ifdef _WIN32
    if (!MAIN.CurrentWindow) return;
    if (!IsClipboardFormatAvailable(CF_TEXT)) return;
    if (!OpenClipboard(GetDesktopWindow())) return;
    HGLOBAL hg = GetClipboardData(CF_TEXT);
    if(hg){
        LPCSTR strData = (LPCSTR)GlobalLock(hg);
        if (strData) {
            strSafeSet(&MAIN.PasteString, strData);
            la_SendPasteEvent(MAIN.CurrentWindow->win);
            GlobalUnlock(hg);
        }
    }
    CloseClipboard();
#endif
}
int OPINV_SystemPaste(laOperator *a, laEvent *e){ la_GeneratePasteEvent(MAIN.CurrentWindow); return LA_FINISHED; }
int OPINV_Window(laOperator *a, laEvent *e){
    laRetriggerOperators();
    return LA_RUNNING;
}
int OPMOD_Window(laOperator *a, laEvent *e){
    laPanel *p;
    laBlock *b = 0;
    laWindow *w = a->Instance;
    laGeneralUiExtraData *uid = a->CustomData;

    if (!uid) uid = memAcquireSimple(sizeof(laGeneralUiExtraData));
    a->CustomData = uid;

    if(e->type == LA_OPERATOR_EVENT && (!e->OperatorBase)){
        laInvokeP(a, e->Operator, e, 0, e->OperatorInstructions, 0);
        return LA_RUNNING;
    }

    if(laKeyMapExecuteEvent(a, &MAIN.KeyMap, e)) return LA_RUNNING_PASS;

    if(w->IsDocking && MAIN.DockingPanel){
        laRestoreToLayout();
        laRestoreCanvasUI();
        laLayout* l=MAIN.CurrentWindow->CurrentLayout;
        laBlock *Recieve = laDetectBlockRecursive(l->FirstBlock, e->x, e->y);
        if (Recieve){
            la_ClearDockingTarget(); l->DropToBlock = Recieve;
            l->DropLocation = la_DetectBlockDropLocation(Recieve, e->x, e->y);
            laRefreshWindow();
        }else{
            MAIN.CurrentWindow->CurrentLayout->DropToBlock = 0;
        }

        if(e->type==LA_L_MOUSE_UP && l->DropToBlock){
            l->DropToBlock = la_MakeDropBlock(l->DropToBlock, l->DropLocation);
            laDockPanel(w->DockingFrom, l->DropToBlock, MAIN.DockingPanel);
            la_StopDocking();la_ClearDockingTarget(); return LA_RUNNING;
        }

        if(e->type==LA_R_MOUSE_DOWN || (e->type==LA_KEY_DOWN&&e->key==LA_KEY_ESCAPE)){
            la_StopDocking();la_ClearDockingTarget(); return LA_RUNNING;
        }
        return LA_RUNNING;
    }

    if(w->MaximizedUi && w->MaximizedUiPanel){
        if(((e->y<LA_RH && e->x<LA_RH)||e->y<LA_SEAM_W) && uid->Dragging){ laShowMenuBar(); uid->Dragging=0; }
        if(!la_UiOperatorExists(w->MaximizedUi)){
            laSetOperatorLocalizer(w->MaximizedUiPanel);
            laInvokeUi(a, w->MaximizedUi->Type->OperatorID, e, w->MaximizedUi, 0, 1);
            return LA_RUNNING;
        }
        if(e->y>LA_2RH && e->x>LA_2RH){ if(!uid->Dragging)laRequestDelayEvent(0.5); uid->Dragging=1; }
        if(e->type==LA_TIME_DELAY){ laHideMenuBar(); }
    }else{
        if(MAIN.PendingSplash){
            laSetOperatorLocalizer(MAIN.PendingSplash);
            laInvokeUi(a, "LA_panel_operator", e, MAIN.PendingSplash, 0, 1);
            laRetriggerOperators();
            MAIN.PendingSplash=0; return LA_RUNNING;
        }

        laBlock* RootBlock=w->MaximizedBlock?w->MaximizedBlock:w->CurrentLayout->FirstBlock;
        if (la_ProcessBlockEdgeEvent(a, w->CurrentLayout, RootBlock, e)) return LA_RUNNING;

        b = w->MaximizedBlock?w->MaximizedBlock:laDetectBlockRecursive(w->CurrentLayout->FirstBlock, e->x, e->y);

        if (b && la_ProcessBlockEvent(a, b, e)) return LA_RUNNING;
        
        for (p = w->Panels.pFirst; p; p = p->Item.pNext){
            int x = e->x; int y = e->y;
            laWindowToLocal(0, p, &x, &y);
            if (laIsInPanel(p, x, y) && p->Show){
                laSetOperatorLocalizer(p);
                if (!p->LaterDestroy) laInvokeUi(a, "LA_panel_operator", e, p, 0, 1);
                return LA_RUNNING;
            }
        }
    }
    
    if(la_GenericTopPanelProcessing(a,e)) return LA_RUNNING;

    return LA_RUNNING;
}
int OPINV_Panel(laOperator *a, laEvent *e){
    if (!((laPanel *)a->Instance)->Show) return LA_FINISHED;

    a->CustomData = memAcquireSimple(sizeof(laGeneralUiExtraData));
    laRetriggerOperators();
    return LA_RUNNING;
}
void OPEXT_Panel(laOperator *a, int ExitCode){
    memFree(a->CustomData);
}
int la_ScrollPanel(laGeneralUiExtraData*ex, laPanel*p, laEvent* e){
    int ret=0;
    if(ex->TargetIndexVali==4 && e->type==LA_MOUSEMOVE){
        laUiList* uuil=ex->Ptr1; laUiItem* upui=ex->Ptr2;
        if(upui){ laPanUiListFree(uuil, ex->LastX-e->x, ex->LastY-e->y);
            //if(uuil->HeightCoeff){
                if(uuil->B-upui->Page->PanY<upui->TB-LA_M-LA_SCROLL_W){
                    upui->Page->PanY = (uuil->B-upui->TB+LA_M+LA_SCROLL_W);
                    if(upui->Page->PanY<0){upui->Page->PanY=0;}
                }
            //}
        }
        ex->LastX=e->x; ex->LastY=e->y;
        laRedrawCurrentPanel();
        ret= 1;
    }
    if(ex->TargetIndexVali==5 && e->type==LA_MOUSEMOVE){
        laUiList* uuil=ex->Ptr1; laUiItem* upui=ex->Ptr2;
        if(upui)laScaleUiList(uuil, -(ex->LastY-e->y)*0.005+1, upui->L, upui->R, upui->U, upui->B);
        ex->LastX=e->x; ex->LastY=e->y;
        laRecalcCurrentPanel();
        ret= 1;
    }
    if(e->type==LA_M_MOUSE_UP){ ex->TargetIndexVali=0; }
    if (e->type & LA_KEY_MOUSE_SCROLL || (e->type==LA_M_MOUSE_DOWN) || (e->key&LA_KEY_PANNING)){
        laUiItem *pui = 0; laListHandle levels={0}; int dir=0;
        if(e->key&LA_KEY_PANNING){
            if (e->key&LA_KEY_ARRDOWN) dir=1;
            elif (e->key&LA_KEY_ARRUP) dir=-1;
        }else{
            if (e->type & LA_STATE_DOWN) dir=1;
            elif (e->type & LA_STATE_UP) dir=-1;
        }
        laUiList *duil = la_DetectUiListRecursiveDeep(p->MenuRefer?p->MenuRefer:&p->UI, e->x, e->y, 10000, &pui, 0, 0, 0, 0, &levels);
        laUiListRecord* lip=levels.pLast; laUiList* uuil=lip->uil; laUiItem* upui=lip->Item.pPrev?((laUiListRecord*)lip->Item.pPrev)->pui:0; int ran=0;
        if(e->SpecialKeyBit == LA_KEY_CTRL){
            if(e->type==LA_M_MOUSE_DOWN){
                while (lip && upui && (!uuil->AllowScale)) { lip=lip->Item.pPrev; uuil=lip->uil;  upui=lip->Item.pPrev?((laUiListRecord*)lip->Item.pPrev)->pui:0; } 
                if(uuil) { ex->TargetIndexVali=5; ex->Ptr1=uuil; ex->Ptr2=upui; ex->LastX=e->x; ex->LastY=e->y; ret= 1;}
            }else{
            }
        }else{
            if(e->type==LA_M_MOUSE_DOWN){
                while (lip && upui && (!uuil->AllowScale)) { lip=lip->Item.pPrev; uuil=lip->uil;  upui=lip->Item.pPrev?((laUiListRecord*)lip->Item.pPrev)->pui:0; } 
                if(uuil) { ex->TargetIndexVali=4; ex->Ptr1=uuil; ex->Ptr2=upui; ex->LastX=e->x; ex->LastY=e->y; ret= 1;}
            }else{
                while (lip && upui){
                    if(uuil->AllowScale){ if((ran=laScaleUiList(uuil, 1.0f-dir*0.1, upui->L, upui->R, upui->U, upui->B))){ laRecalcCurrentPanel(); break;} }
                    else{ if((ran=laPanUiListAuto(uuil, 0, dir*MAIN.ScrollingSpeed*LA_RH,
                                uuil->L, upui->R-(uuil->ScrollerShownV?(LA_SCROLL_W+LA_M):0),
                                uuil->U, upui->B-(uuil->ScrollerShownH?(LA_SCROLL_W+LA_M):0)))) break; }
                    lip=lip->Item.pPrev; uuil=lip->uil;  upui=lip->Item.pPrev?((laUiListRecord*)lip->Item.pPrev)->pui:0; 
                }
                if(!ran){
                    laUiList* pan_uil=p->MenuRefer?p->MenuRefer:&p->UI;
                    laPanUiListAuto(pan_uil, 0, dir*MAIN.ScrollingSpeed*LA_RH, 0, p->W, pan_uil->U, p->H);
                }
                laRedrawCurrentPanel();
                ret= 1;
            }
        }
        while(duil=lstPopPointer(&levels));
    }
    return ret;
}
int OPMOD_Panel(laOperator *a, laEvent *e){
    int x = e->x;
    int y = e->y;
    laPanel *p = a->Instance, *dp;
    laUiItem *ui = 0;
    laGeneralUiExtraData *uid = a->CustomData;
    laListHandle Locals = {0};
    int RET=LA_RUNNING|(p->IsMenuPanel?0:LA_PASS_ON);

    if(MAIN.DockingPanel){ return LA_FINISHED; }

    if (!p->Show || (!laIsInPanel(p, x, y) && !uid->TargetIndexVali)){
        if(p->CloseWhenMovedOut==2){
            if(e->type&LA_STATE_DOWN){
                la_StopUiOperatorService(p); laDestroySinglePanel(p,0); return LA_FINISHED;
            }
            return LA_RUNNING;
        }
        if(p->ShowCorner){ p->ShowCorner=0; laSetWindowCursor(LA_ARROW); laRefreshWindow(); }
        return LA_FINISHED_PASS;
    }
    if(((e->type==LA_KEY_DOWN && e->key==LA_KEY_ESCAPE)||(e->type==LA_R_MOUSE_DOWN)) && p->CloseWhenMovedOut==2){
        la_StopUiOperatorService(p); laDestroySinglePanel(p,0); return LA_FINISHED;
    }

    int NoPrimaryUI=(p==MAIN.CurrentWindow->MaximizedUiPanel);

    if(e->type == LA_OPERATOR_EVENT && e->OperatorBase==LA_KM_SEL_PANEL){
        laInvokeP(a, e->Operator, e, &p->PP, e->OperatorInstructions, 0);
        return LA_RUNNING;
    }

    if(p->PanelTemplate && laKeyMapExecuteEvent(a, &p->PanelTemplate->KeyMap, e)) return RET;

    int IsTop=laIsTopPanel(p);
    if ((!IsTop) && (!uid->TargetIndexVali)){
        laLocalToWindow(0, p, &x, &y);
        dp = laDetectPanel(x, y);
        if (dp && dp->Mode && dp != p){
            return LA_FINISHED;
        }else if ((e->type & LA_MOUSEDOWN) == LA_MOUSEDOWN){
            laPopPanel(p); laRedrawCurrentWindow(); IsTop=1;
        }else if(p->Mode) return LA_RUNNING;
        laWindowToLocal(0, p, &x, &y);
    }

    if (e->type == LA_L_MOUSE_DOWN && (!NoPrimaryUI) && e->x + e->y < p->W + p->H - LA_SCROLL_W*2){
        laUiItem *pui = 0; laUiList *suil = 0;laListHandle levels={0};
        laUiList *duil = la_DetectUiListRecursiveDeep(&p->UI, e->x, e->y, 10000, &pui, &suil, 0, 0, 0, &levels);
        while(duil=lstPopPointer(&levels));
        if (suil){
            uid->Ptr1 = suil; uid->Ptr2 = pui;
            uid->TargetIndexVali = 3;
            uid->LastX = e->x; uid->LastY = e->y;
            uid->On=(!pui || e->x>pui->R-LA_SCROLL_W-2*LA_M)?1:0;
            return LA_RUNNING;
        }
    }

    if(la_ScrollPanel(uid,p,e))return LA_RUNNING;

    if ((!p->Mode) || (!uid->TargetIndexVali)){
        if(y<p->UI.U){ ui = la_DetectUiItemRecursive(&p->TitleBar, x, y, 10000, &Locals, 0); }
        elif (!ui && (!NoPrimaryUI)){
            lstClearPointer(&Locals);
            ui = la_DetectUiItemRecursive(&p->UI, x, y, 10000, &Locals, 0);
        }
    }
    if (ui && !a->Child && ui->Type->OperatorType && !la_UiOperatorExists(ui)){
        laSetOperatorLocalizer(p);
        //laPanelToLocal(a, &t, &UIB);
        if (!(laInvokeUiP(a, ui->Type->OperatorType, e, ui, &Locals, 0) & LA_FINISH)){
            laRetriggerOperators(); laPopPanel(p); IsTop=1; laRedrawCurrentWindow(); return LA_RUNNING;
        }
        lstClearPointer(&Locals);
        //return LA_RUNNING;
    }
    lstClearPointer(&Locals);
    
    if (p->Mode && e->type&LA_MOUSE_EVENT && !uid->TargetIndexVali && !p->IsMenuPanel){
        if (e->x + e->y > p->W + p->H - LA_SCROLL_W*2){
            if((!a->Item.pPrev) && (!p->ShowCorner)){ p->ShowCorner=1; laSetWindowCursor(LA_CORNER);  laRefreshWindow(); }
            if(e->type==LA_L_MOUSE_DOWN){ uid->TargetIndexVali = 2; uid->LastX=e->x;uid->LastY=e->y; }
            return LA_RUNNING;
        }else{
            if(p->ShowCorner){ p->ShowCorner=0; laSetWindowCursor(LA_ARROW); laRefreshWindow(); }
            if(e->type==LA_L_MOUSE_DOWN){  uid->TargetIndexVali = 1; uid->LastX=e->x;uid->LastY=e->y; return LA_RUNNING; }
        }
    }

    if (e->type == LA_MOUSEMOVE){
        if (uid->TargetIndexVali == 1){
            if (!p->SL && !p->SR) p->TX = p->X + e->x - uid->LastX;
            if (!p->ST && !p->SB) p->TY = p->Y + e->y - uid->LastY;
            laPopPanel(p); laRedrawCurrentWindow(); IsTop=1;
            laNotifyUsersPPPath(&p->PP, "position");
            la_EnsurePanelSnapping(p, MAIN.CurrentWindow->CW, MAIN.CurrentWindow->CH);
            laRecalcCurrentPanel();
            return LA_RUNNING;
        }elif (uid->TargetIndexVali == 2){
            p->TW += e->x - uid->LastX; p->TH += e->y - uid->LastY;
            uid->LastX = e->x; uid->LastY = e->y;
            p->BoundUi=0;
            laNotifyUsersPPPath(&p->PP, "size");
            la_EnsurePanelSnapping(p, MAIN.CurrentWindow->CW, MAIN.CurrentWindow->CH);
            laRecalcCurrentPanel();
            return LA_RUNNING;
        }elif (uid->TargetIndexVali == 3){
            laUiList *suil = uid->Ptr1; laUiItem *pui = uid->Ptr2;
            int IsVertical=uid->On;
            uid->TargetIndexValf+=IsVertical?la_ScrollerVerticalPan(e->y - uid->LastY, p, uid->Ptr1, uid->Ptr2):
                                        (pui?la_ScrollerHorizontalPan(e->x - uid->LastX, p, uid->Ptr1, uid->Ptr2):0);
            uid->Dragging = (int)uid->TargetIndexValf; uid->TargetIndexValf-=uid->Dragging;
            int pV=uid->Dragging*IsVertical, pH=uid->Dragging*(!IsVertical);
            if (!pui) laPanUiList(suil, pH, pV,0, p->W, suil->U, p->H-LA_M);
            else laPanUiList(suil, pH, pV, suil->L, pui->R-(suil->ScrollerShownV?(LA_SCROLL_W+LA_M):0),
                                           suil->U, pui->B-LA_M-(suil->ScrollerShownH?(LA_SCROLL_W+LA_M):0));
            uid->LastX = e->x; uid->LastY = e->y;
            laRedrawCurrentPanel();
            return LA_RUNNING;
        }
    }

    if (e->type == LA_L_MOUSE_UP){
        uid->TargetIndexVali = 0; laSetWindowCursor(LA_ARROW);
        return LA_RUNNING;
    }

    if(p->Mode&&e->type&LA_MOUSE_EVENT){ return LA_RUNNING; }

    return RET;
}
int OPMOD_MenuPanel(laOperator *a, laEvent *e){
    int x = e->x;
    int y = e->y;
    laPanel *p = a->Instance;
    laUiItem *ui;
    laListHandle Locals = {0};
    laGeneralUiExtraData *uid = a->CustomData;

    if(MAIN.DockingPanel){return LA_FINISHED; }

    int IsClose=laIsCloseToPanel(p,x,y);
    int IsIn=laIsInPanel(p, x, y);

    if(p->CloseWhenMovedOut && ((!IsClose) || (!IsIn && e->type==LA_TIME_IDLE) || e->type==LA_L_MOUSE_DOWN||e->type==LA_R_MOUSE_DOWN)){ 
        la_StopUiOperatorService(p);
        laDestroySinglePanel(p,0);
        return LA_FINISHED_PASS;
    }
    if ((e->type == LA_KEY_DOWN && e->key==LA_KEY_ESCAPE) || (e->type == LA_L_MOUSE_DOWN && !IsIn)){
        laConfirmInt(a, 1, LA_CONFIRM_DATA);
        la_StopUiOperatorService(p);
        laDestroySinglePanel(p,0);
        return LA_FINISHED_PASS;
    }

    if(p->PanelTemplate && laKeyMapExecuteEvent(a, &p->PanelTemplate->KeyMap, e)) return LA_RUNNING_PASS;

    if (e->type == LA_L_MOUSE_DOWN && e->y < p->H - LA_SCROLL_W){
        laUiItem *pui = 0; laUiList *suil = 0;laListHandle levels={0};
        laUiList *duil = la_DetectUiListRecursiveDeep(p->MenuRefer?p->MenuRefer:&p->UI, e->x, e->y, 10000, &pui, &suil, 0, 0, 0, &levels);
        while(duil=lstPopPointer(&levels));
        if (suil){
            uid->Ptr1 = suil; uid->Ptr2 = pui;
            uid->TargetIndexVali = 3;
            uid->LastX = e->x; uid->LastY = e->y;
            uid->On=(!pui || e->x>pui->R-LA_SCROLL_W-2*LA_M)?1:0;
            return LA_RUNNING;
        }
    }
    if (e->type == LA_L_MOUSE_UP && uid->TargetIndexVali == 3){
        uid->TargetIndexVali = 0;
        return LA_RUNNING;
    }

    if(la_ScrollPanel(uid,p,e))return LA_RUNNING;

    if (e->type == LA_MOUSEMOVE){
        if (uid->TargetIndexVali == 3){
            laUiList *suil = uid->Ptr1; laUiItem *pui = uid->Ptr2;
            int IsVertical=uid->On;
            uid->TargetIndexValf+=IsVertical?la_ScrollerVerticalPan(e->y - uid->LastY, p, uid->Ptr1, uid->Ptr2):
                                        (pui?la_ScrollerHorizontalPan(e->x - uid->LastX, p, uid->Ptr1, uid->Ptr2):0);
            uid->Dragging = (int)uid->TargetIndexValf; uid->TargetIndexValf-=uid->Dragging;
            int pV=uid->Dragging*IsVertical, pH=uid->Dragging*(!IsVertical);
            if (!pui) laPanUiList(suil, pH, pV,0, p->W, suil->U, p->H-LA_M);
            else laPanUiList(suil, pH, pV, suil->L, pui->R-(suil->ScrollerShownV?(LA_SCROLL_W+LA_M):0),
                                           suil->U, pui->B-LA_M-(suil->ScrollerShownH?(LA_SCROLL_W+LA_M):0));
            uid->LastX = e->x; uid->LastY = e->y;
            laRedrawCurrentPanel();
            return LA_RUNNING;
        }
    }

    ui = la_DetectUiItemRecursive(&p->UI, x, y, 10000, &Locals, 0);
    if (!ui){
        lstClearPointer(&Locals);
        ui = la_DetectUiItemRecursive(p->MenuRefer, x, y, 10000, &Locals, 0);
    }

    if (ui && !la_UiOperatorExists(ui)){
        laSetOperatorLocalizer(p);
        laInvokeUiP(a, ui->Type->OperatorType, e, ui, &Locals, 0);
    }

    lstClearPointer(&Locals);

    if (a->ConfirmData && (!p->NoConfirm)){
        laConfirmInt(a, a->ConfirmData->IData, a->ConfirmData->Mode);
        la_StopUiOperatorService(p);
        laDestroySinglePanel(p,0);
        return LA_FINISHED_PASS;
    }

    return LA_RUNNING;
}
int OPMOD_ModalPanel(laOperator *a, laEvent *e){
    int x = e->x;
    int y = e->y;
    laPanel *p = a->Instance;
    laUiItem *ui = 0;
    laListHandle Locals = {0};
    laGeneralUiExtraData *uid = a->CustomData;

    if(MAIN.DockingPanel){return LA_FINISHED; }

    if (e->type == LA_KEY_DOWN && e->key==LA_KEY_ESCAPE){
        laConfirmInt(a, 0, LA_CONFIRM_CANCEL);
        la_StopUiOperatorService(p);
        laDestroySinglePanel(p,0);
        return LA_FINISHED_PASS;
    }

    if (a->ConfirmData){
        laConfirmSameDataIfAny(a);
        if (a->ConfirmData->Mode == LA_CONFIRM_DATA||a->ConfirmData->Mode==LA_CONFIRM_CUSTOM_STRING){
             return LA_RUNNING_PASS;
        }
        if(!p->ParentOperator){
            la_StopUiOperatorService(p);
            laDestroySinglePanel(p,0);
            return LA_FINISHED_PASS;
        }
        return LA_RUNNING_PASS;
    }

    if (!laIsInPanel(p, e->x, e->y) && !uid->TargetIndexVali){ 
        if(e->type&LA_STATE_DOWN){
            p->AnimationRatio=0; p->AnimationMode=LA_PANEL_ANIMATION_FLASH; laRedrawCurrentPanel();
        }
        if(p->ShowCorner){ p->ShowCorner=0; laSetWindowCursor(LA_ARROW); laRefreshWindow(); } return LA_RUNNING; 
    }

    if(p->PanelTemplate && laKeyMapExecuteEvent(a, &p->PanelTemplate->KeyMap, e)) return LA_RUNNING_PASS;

    if (e->type == LA_L_MOUSE_DOWN && e->y < p->H -  LA_SCROLL_W){
        laUiItem *pui = 0; laUiList *suil = 0;laListHandle levels={0};
        laUiList *duil = la_DetectUiListRecursiveDeep(&p->UI, e->x, e->y, 10000, &pui, &suil, 0, 0, 0, &levels);
        while(duil=lstPopPointer(&levels));
        if (suil){
            uid->Ptr1 = suil; uid->Ptr2 = pui;
            uid->TargetIndexVali = 3;
            uid->LastX = e->x; uid->LastY = e->y;
            uid->On=(!pui || e->x>pui->R-LA_SCROLL_W-2*LA_M)?1:0;
            return LA_RUNNING;
        }
    }

    if(la_ScrollPanel(uid,p,e))return LA_RUNNING;

    if (!uid->TargetIndexVali){
        ui = la_DetectUiItemRecursive(&p->TitleBar, x, y, 10000, &Locals, 0);
        if (!ui){
            lstClearPointer(&Locals);
            ui = la_DetectUiItemRecursive(&p->UI, x, y, 10000, &Locals, 0);
        }
    }
    if (ui && !a->Child && ui->Type->OperatorType && !la_UiOperatorExists(ui)){
        laSetOperatorLocalizer(p);
        laInvokeUiP(a, ui->Type->OperatorType, e, ui, &Locals, 0);
        return LA_RUNNING;
    }
    lstClearPointer(&Locals);

    int insize=0; if (e->x + e->y > p->W + p->H - LA_SCROLL_W*2){ insize=1;
        if((!a->Item.pPrev) && (!p->ShowCorner)){ p->ShowCorner=1; laSetWindowCursor(LA_CORNER); laRefreshWindow(); }
             }else{ if(p->ShowCorner){ p->ShowCorner=0; laSetWindowCursor(LA_ARROW); laRefreshWindow(); } }
    if (e->type == LA_L_MOUSE_DOWN){
        uid->LastX = e->x;
        uid->LastY = e->y;
        if(insize){ uid->TargetIndexVali = 2; laSetWindowCursor(LA_CORNER); }
        else uid->TargetIndexVali = 1;
        return LA_RUNNING;
    }
    if (e->type == LA_MOUSEMOVE){
        if (uid->TargetIndexVali == 1){
            if (!p->SL && !p->SR) p->TX = p->X + e->x - uid->LastX;
            if (!p->ST && !p->SB) p->TY = p->Y + e->y - uid->LastY;
            laNotifyUsersPPPath(&p->PP, "position");
            la_EnsurePanelSnapping(p, MAIN.CurrentWindow->CW, MAIN.CurrentWindow->CH);
            laRecalcCurrentPanel();
            return LA_RUNNING;
        }elif (uid->TargetIndexVali == 2){
            p->TW += e->x - uid->LastX; p->TH += e->y - uid->LastY;
            uid->LastX = e->x; uid->LastY = e->y;
            p->BoundUi=0;
            laNotifyUsersPPPath(&p->PP, "size");
            la_EnsurePanelSnapping(p, MAIN.CurrentWindow->CW, MAIN.CurrentWindow->CH);
            laRecalcCurrentPanel();
            return LA_RUNNING;
        }elif (uid->TargetIndexVali == 3){
            laUiList *suil = uid->Ptr1; laUiItem *pui = uid->Ptr2;
            int IsVertical=uid->On;
            uid->TargetIndexValf+=IsVertical?la_ScrollerVerticalPan(e->y - uid->LastY, p, uid->Ptr1, uid->Ptr2):
                                        (pui?la_ScrollerHorizontalPan(e->x - uid->LastX, p, uid->Ptr1, uid->Ptr2):0);
            uid->Dragging = (int)uid->TargetIndexValf; uid->TargetIndexValf-=uid->Dragging;
            int pV=uid->Dragging*IsVertical, pH=uid->Dragging*(!IsVertical);
            if (!pui) laPanUiList(suil, pH, pV,0, p->W, suil->U, p->H-LA_M);
            else laPanUiList(suil, pH, pV, suil->L, pui->R-(suil->ScrollerShownV?(LA_SCROLL_W+LA_M):0),
                                           suil->U, pui->B-LA_M-(suil->ScrollerShownH?(LA_SCROLL_W+LA_M):0));
            uid->LastX = e->x; uid->LastY = e->y;
            laRedrawCurrentPanel();
            return LA_RUNNING;
        }
        if(uid->TargetIndexVali==2){laSetWindowCursor(LA_CORNER);}
    }
    if (e->type == LA_L_MOUSE_UP){
        uid->TargetIndexVali = 0; laSetWindowCursor(LA_ARROW);
        return LA_RUNNING;
    }

    return LA_RUNNING;
}

int OPCHK_IsPanel(laPropPack *This, laStringSplitor *ss){
    if (This && This->LastPs->p == _LA_PROP_PANEL) return 1;
    else{
        char *PanelInternalID = strGetArgumentString(ss, "panel_id");
        if (!PanelInternalID) return 0;
        else
            return 1;
    }
    return 0;
}
int OPCHK_IsPanelNoInstructions(laPropPack *This, laStringSplitor *ss){
    if (This && This->LastPs->p == _LA_PROP_PANEL) return 1;
    return 0;
}
int OPINV_HidePanel(laOperator *a, laEvent *e){
    laPanel *p = a->This?a->This->EndInstance:0;

    laHidePanelWithDissoveEffect(p);

    return LA_FINISHED;
}
int OPINV_ActivatePanel(laOperator *a, laEvent *e){
    laStringSplitor *ss = a->ExtraInstructionsP;
    char *TemplateID = strGetArgumentString(ss, "panel_id");
    if (!TemplateID) return LA_CANCELED;

    laActivatePanel(TemplateID, e->x, e->y);

    return LA_FINISHED;
}
int OPINV_PanPanel(laOperator *a, laEvent *e){
    laPanel *p = a->This?a->This->EndInstance:0;

    //laHidePanel(p);

    return LA_FINISHED;
}

#ifdef _WIN32
#define _WIN32_WINNT 0x0500
#include <wincon.h> 

int OPINV_ToggleSystemConsole(laOperator *a, laEvent *e){
    HWND wnd=GetConsoleWindow();
    int showhide=IsWindowVisible(wnd)?SW_HIDE:SW_SHOW;
    ShowWindow(wnd, showhide);
    return LA_FINISHED;
}
#endif

int OPCHK_IsHyper(laPropPack *This, laStringSplitor *ss){
    if (This && This->LastPs->p->Container->Hyper) return 1;
    return 0;
}
int OPINV_ViewHyperData(laOperator *a, laEvent *e){
    char buf[2048]={0};
    memHyperInfo(a->This, buf);
    printf("%s",buf);
    return LA_FINISHED;
}

int OPINV_TranslationDumpMisMatch(laOperator *a, laEvent *e){

    transDumpMissMatchRecord("TranslationDump.txt");
    laEnableMessagePanel(a, 0, "OK", "Untranslated strings exported to TranslationDump.txt", e->x - 250, e->y - 10, 300, e);

    return LA_FINISHED;
}

int OPINV_RefreshScreens(laOperator *a, laEvent *e){
#ifdef LA_LINUX
    la_GetDPI(MAIN.win);
#endif
#ifdef _WIN32
    la_GetDPI(MAIN.CurrentWindow);
#endif
    return LA_FINISHED;
}
int OPINV_RemoveScreenConfig(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_FINISHED;
    laEnableYesNoPanel(a, 0, "Confirm?", "Will remove this screen entry", e->x, e->y, 200, e);
    return LA_RUNNING;
}

int OPMOD_RemoveScreenConfig(laOperator *a, laEvent *e){
    if(!a->This || !a->This->EndInstance) return LA_FINISHED; laScreen* s=a->This->EndInstance;
    if(a->ConfirmData){
        if(a->ConfirmData->Mode == LA_CONFIRM_OK){
            la_RemoveScreen(s); laNotifyUsers("la.user_preferences.screens");
        }
        return LA_FINISHED;
    }
    return LA_FINISHED;
}


int OPINV_OpenInternetLink(laOperator *a, laEvent *e){
    char *link = strGetArgumentString(a->ExtraInstructionsP, "link");

    if (link) laOpenInternetLink(link);

    return LA_FINISHED;
}

void laget_PanelTemplateCategory(void* rack_unused, laUiTemplate* uit, char* copy, char** ptr);

void la_RegisterBuiltinOperators(){
    laPropContainer *pc; laProp *p;
    laOperatorType *at;
    laEnumProp *ep;

    laCreateOperatorType("LA_terminate_program", "Quit", "Terminate Program Immediately",
                          OPCHK_TerminateProgram, 0, 0, OPINV_TerminateProgram, 0, U'⏻', LA_ACTUATOR_SYSTEM);

    laCreateOperatorType("LA_nop", "Button", "Do nothing", 0, 0, 0, OPINV_Nop, 0, 0, LA_ACTUATOR_SYSTEM);

    laCreateOperatorType("LA_undo", "Undo", "Undo from recorded data state", OPCHK_Undo, 0, 0, OPINV_Undo, 0, U'⮌', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_redo", "Redo", "Redo using recorded data state", OPCHK_Redo, 0, 0, OPINV_Redo, 0, U'⮎', LA_ACTUATOR_SYSTEM);

    laCreateOperatorType("LA_refresh_screens", "Refresh Screens", "Refresh Screens", 0, 0, 0, OPINV_RefreshScreens, 0, U'⭯', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_remove_screen_config", "Remove Screen", "Remove this screen configuration", 0, 0, 0, OPINV_RemoveScreenConfig, OPMOD_RemoveScreenConfig, U'🞫', LA_ACTUATOR_SYSTEM);

    laCreateOperatorType("LA_translation_dump", "Dump Untranslated Text", "Dump Untranslated Text To File", 0, 0, 0, OPINV_TranslationDumpMisMatch, 0, U'📥', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_open_internet_link", "Goto", "Open Internet Link", 0, 0, 0, OPINV_OpenInternetLink, 0, U'🌐', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_system_paste", "SYSWINDOW Paste", "Generate a syetem paste event",  0, 0, 0, OPINV_SystemPaste, 0, U'📋', LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_string_copy", "Copy", "Copy string to clip board", 0, 0, 0, OPINV_StringCopy, 0, U'🗍', LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);

    laCreateOperatorType("LA_window_operator", "SYSWINDOW Operator", "Handle All Unhandled Events",  0, 0, 0, OPINV_Window, OPMOD_Window, U'🖦', LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);

    at = laCreateOperatorType("LA_switch_layout", "Switch Layout", "Cycle All Layouts In Current SYSWINDOW", 0, 0, 0, OPINV_SwitchLayout, OPMOD_FinishOnData, U'↔', LA_ACTUATOR_SYSTEM);
    at->UiDefine=laui_LayoutCycle;

    at = laCreateOperatorType("LA_new_window", "New SYSWINDOW", "Create a new window", 0, 0, 0, OPINV_NewWindow, 0, U'🗗', LA_ACTUATOR_SYSTEM);

    laCreateOperatorType("LA_combine_child_blocks", "Combine Child Block", "If Child Blocks Are Two Panel Blocks, Then Combine Them.",
                          OPCHK_CombineChildBlocks, 0, 0, OPINV_CombineChildBlocks, 0, U'□' , LA_ACTUATOR_SYSTEM);
    at = laCreateOperatorType("LA_new_panel", "New Panel", "Create a new panel",
                          0, 0, 0, OPINV_NewPanel, OPMOD_NewPanel, U'🞆', LA_ACTUATOR_SYSTEM);
    pc = laDefineOperatorProps(at, 0);
    p = laAddSubGroup(pc, "template", "Template", "Template selection used to create the new panel", "panel_template", 0, 0, laui_TitleOnly, -1, laget_FirstPanelTemplate, laget_NewPanelGetActiveTemplate, laget_ListNext, 0, 0, laset_NewPanelSetTemplate,0,0);
    laSubGroupExtraFunctions(p,0,0,0,0,laget_PanelTemplateCategory);
    at->UiDefine=laui_PanelTemplateSelect;

    laCreateOperatorType("LA_block_fold_title", "Fold Title", "Fold the tile bar of the block", 0, 0, 0, OPINV_BlockFoldTitle, 0, U'⯅', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_block_maximize", "Maximize", "Maximize this block", 0, 0, 0, OPINV_BlockMaximize, 0, U'⮼', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_block_tear_off_panel", "Tear Off", "Tear off current panel in the block",
                          OPCHK_BlockHasMorePanels, 0, 0, OPINV_BlockTearOffPanel, 0, U'🗗', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_block_close_panel", "Close Panel", "Close current panel in the block",
                          OPCHK_BlockHasMorePanels, 0, 0, OPINV_BlockClosePanel, OPMOD_BlockClosePanel, U'🞫', LA_ACTUATOR_SYSTEM);

    laCreateOperatorType("LA_canvas_ui_maximize", "Maximize", "Maximize this UI item", 0, 0, 0, OPINV_CanvasUiMaximize, 0, U'⮼', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_hide_menu_bar", "Hide Menu Bar", "Hide menu bar of the window", 0, 0, 0, OPINV_HideMenuBar, 0, U'⯅', LA_ACTUATOR_SYSTEM);

    laCreateOperatorType("LA_fullscreen", "Fullscreen", "Switch full screen for the window",
                          0, 0, 0, OPINV_Fullscreen, 0, U'🡵', LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_new_layout", "New Layout", "Create a new layout in the window",
                          0, 0, 0, OPINV_NewLayout, 0, U'🞦', LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_remove_layout", "Remove Layout", "Remove current layout in the window",
                          OPCHK_RemoveLayout, 0, 0, OPINV_RemoveLayout, 0, U'🞫', LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_panel_operator", "Panel Operator", "Handle Events On The Panel Level",
                          0, 0, OPEXT_Panel, OPINV_Panel, OPMOD_Panel, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_modal_panel_operator", "Modal Panel Operator", "Handle Events On Modal Panels Like Yes-No Boxes",
                          0, 0, OPEXT_Panel, OPINV_Panel, OPMOD_ModalPanel, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_menu_panel_operator", "Menu Panel Operator", "Handle Events On Menu Panel,Recieve And Dispatch Confirm Messages,Then Self-destroy.",
                          0, 0, OPEXT_Panel, OPINV_Panel, OPMOD_MenuPanel, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);

    laCreateOperatorType("LA_panel_activator", "Panel Activator", "Show A Panel When Invoked.Need An Extra Argument[panel_id]",
                          OPCHK_IsPanel, 0, 0, OPINV_ActivatePanel, 0, 0, LA_ACTUATOR_SYSTEM)
        ->ParseArgs = la_PanelActiviatorParser;
    laCreateOperatorType("LA_hide_panel", "Hide Panel", "Hide a panel",
                          OPCHK_IsPanel, 0, 0, OPINV_HidePanel, 0, 0, LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_dock_panel", "Dock Panel", "Dock a panel",
                          OPCHK_IsPanel, 0, 0, OPINV_DockPanel, 0, 0, LA_ACTUATOR_SYSTEM);
    
#ifdef _WIN32
    laCreateOperatorType("LA_toggle_system_console", "Toggle System Console", "Toggle Win32 console window",
                          0, 0, 0, OPINV_ToggleSystemConsole, 0, 0, LA_ACTUATOR_SYSTEM|LA_ACTUATOR_HIDDEN);
#endif

    laCreateOperatorType("LA_prop_restore_default", "Restore default value", "Restore property back to its default value", OPCHK_PropSetValue, 0, 0, OPINV_PropSetDefault, 0, U'⭯', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_prop_set_min", "Set Min Value", "Set property to its minimum value", OPCHK_PropSetValue, 0, 0, OPINV_PropSetMin, 0, 0, LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_prop_set_max", "Set Max Value", "Set property to its maximum value", OPCHK_PropSetValue, 0, 0, OPINV_PropSetMax, 0, 0, LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_string_set_default", "Set Default Value", "Set default string", OPCHK_StringSetValue, 0, 0, OPINV_PropSetDefault, 0, U'⭯', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_string_get_folder_path", "Get folder Path", "get folder path", OPCHK_StringSetValue, 0, 0, OPINV_StringGetFolderPath, OPMOD_StringGetFolderOrFilePath, U'📁', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_string_get_file_path", "Get folder Path", "get file path", OPCHK_StringSetValue, 0, 0, OPINV_StringGetFilePath, OPMOD_StringGetFolderOrFilePath, U'🖹', LA_ACTUATOR_SYSTEM);
    
    laCreateOperatorType("LA_collection_clear_selection", "Clear Selection", "Clear collection selection", OPCHK_CollectionSetValue, 0, 0, OPINV_CollectionClearSelection, 0, U'🧹', LA_ACTUATOR_SYSTEM);
    
    laCreateOperatorType("LA_prop_insert_key", "Insert Key Frame", "Insert key frame in the active action", OPCHK_PropInsertKey, 0, 0, OPINV_PropInsertKey, 0, U'🔑', LA_ACTUATOR_SYSTEM);

    laCreateOperatorType("LA_view_hyper_data", "View Hyper Data", "Show Properties Of Specific Data Block", OPCHK_IsHyper, 0, 0, OPINV_ViewHyperData, 0, U'🛈', LA_ACTUATOR_SYSTEM);
    
    at = laCreateOperatorType("LA_file_dialog_new_directory", "New Directory", "New Directory", OPCHK_IsFileBrowser, 0, 0, OPINV_FileBrowserNewDirectory, OPMOD_FileBrowserNewDirectory, U'📁', LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    at->UiDefine = laui_FileBrowserNewDirectory;
    laCreateOperatorType("LA_file_dialog_refresh", "Refresh", "Refresh List", OPCHK_IsFileBrowser, 0, 0, OPINV_FileBrowserRefresh, 0, U'🗘', LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_file_dialog_up", "Up", "Select Upper Folder Level", OPCHK_IsFileBrowser, 0, 0, OPINV_FileBrowserUpLevel, 0, U'🢰', LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_file_dialog_confirm", "Confirm", "Confirm selection", OPCHK_FileBrowserCanConfirm, 0, 0, OPINV_FileBrowserConfirm, OPMOD_FileBrowserConfirm, U'✔', LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    at = laCreateOperatorType("LA_file_dialog", "File Dialog", "Do File Operations", 0, 0, OPEXT_FileBrowser, OPINV_FileBrowser, OPMOD_FileBrowser, U'🗐', LA_ACTUATOR_SYSTEM);
    pc = laDefineOperatorProps(at, 1);
    at->UiDefine = laui_FileBrowserFileList;
    _LA_PROP_FILE_BROWSER = pc;

    laAddStringProperty(pc, "path", "Path", "Directort path", 0, 0, 0, "/", 0, offsetof(laFileBrowser, Path), 0, 0, laset_FileBrowserPath, 0, LA_UDF_LOCAL);
    laAddStringProperty(pc, "file_name", "File Name", "File name", 0, 0, 0, 0, 0, offsetof(laFileBrowser, FileName), 0, 0, laset_FileBrowserFileName, 0, LA_UDF_LOCAL);
    laAddStringProperty(pc, "temp_str", "Temp String", "Temp string", 0, 0, 0, "", 0, offsetof(laFileBrowser, TempStr), 0, 0, 0, 0, LA_UDF_LOCAL);
    laAddStringProperty(pc, "filter_name", "Filter Name", "Filter file name", 0, 0, 0, 0, 0, offsetof(laFileBrowser, FilterName), 0, 0, laset_FileBrowserFilterName, 0, LA_UDF_LOCAL);
    laAddSubGroup(pc, "file_list", "File List", "List Of Files And Directories Under A Specific Path", "file_item",0,0,laui_FileBrowserFileItem, -1, 0, laget_FileBrowserActiveFile, 0, 0, 0, laset_FileBrowserSelectFile, offsetof(laFileBrowser, FileList), 0);
    laAddSubGroup(pc, "disk_list", "Disk List", "List Of All Logical Drives (In Windows)", "disk_item",0, 0, 0, -1, 0, 0, 0, 0, 0, laset_FileBrowserActiveDisk, offsetof(laFileBrowser, Disks), 0);
    laAddSubGroup(pc, "bookmarks", "Bookmarks", "Bookmarked directories in GTK3", "bookmarked_folder",0, 0, 0, -1, 0, 0, 0, laset_FileBrowserBookmark, 0, 0, offsetof(laFileBrowser, Bookmarks), 0);
    laAddSubGroup(pc, "available_extensions", "Available Extensions", "List of all available extensions", "la_extension_type",0, 0, 0, -1, laget_FileBrowserAcceptedExtensionsFrist, 0, laget_FileBrowserAcceptedExtensionsNext,laset_FileBrowserExtension,0,0,0,0);
    laAddIntProperty(pc,"use_type","Use Type","Use file type",0,0,0,0,0,0,0,0,offsetof(laFileBrowser,UseType),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
    ep = laAddEnumProperty(pc, "select_what", "Select What", "Select folder or file", 0, 0, 0, 0, 0, offsetof(laFileBrowser, SelectFolder), 0, 0, 0, 0, 0, 0, 0, 0, 0,LA_READ_ONLY);{
        laAddEnumItemAs(ep, "file", "File", "File", LA_FILE_SELECT_FILE, U'📁');
        laAddEnumItemAs(ep, "folder", "Folder", "Folder", LA_FILE_SELECT_FOLDER, U'🖹');
    }
    laAddOperatorProperty(pc, "new_directory", "New Directory", "Create a new directory", "LA_file_dialog_new_directory", U'📁', 0);
    laAddOperatorProperty(pc, "refresh", "Refresh", "Refresh file list", "LA_file_dialog_refresh", U'🗘', 0);
    laAddOperatorProperty(pc, "folder_up", "Up", "Select Upper Folder Level In File Browsers", "LA_file_dialog_up", U'🢰', 0);
    laAddOperatorProperty(pc, "confirm", "Confirm", "Confirm selection", "LA_file_dialog_confirm", U'✔', 0);
    p = laAddPropertyContainer("disk_item", "Disk Item", "A logical drive (in Windows)", 0, laui_FileBrowserDiskItem, 0, 0, 0, 1);{
        laAddStringProperty(p, "id", "ID", "Disk identifier", 0, 0, 0, 0, 0, 0, 0, laget_FileBrowserDiskID, 0, 0, 0);
        laAddFloatProperty(p, "total_gb", "Total", "Disk total compacity in gigabytes", 0, 0, "GB", 0, 0, 0, 0, 0, offsetof(laDiskItem, Total_GB), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,LA_READ_ONLY);
        laAddFloatProperty(p, "free_gb", "Free", "Disk Free Space Size In Gigabytes", 0, 0, "GB", 0, 0, 0, 0, 0, offsetof(laDiskItem, Free_GB), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,LA_READ_ONLY);
        laAddStringProperty(p,"capacity","Capacity","Capacity of this disk",LA_WIDGET_STRING_PLAIN,0,0,0,0,0,0,laget_FileBrowserDiskCapacity,0,0,LA_READ_ONLY);
    }
    p = laAddPropertyContainer("file_item", "File Item", "A file item in a list", U'🖹', 0, 0, 0, 0, 0);{
        laAddStringProperty(p, "name", "Name", "The name of the file (with extension)", 0, 0, 0, 0, 0, offsetof(laFileItem, Name), 0, 0, 0, 0, LA_UDF_LOCAL);
        laAddIntProperty(p, "is_folder", "Is Folder", "File is a folder or actual file", 0, 0, 0, 1, 0, 0, 0, 0, offsetof(laFileItem, IsFolder), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,LA_READ_ONLY);
        laAddSubGroup(p, "time_modified", "Time Last Modified", "The Time When The File Was Last Modified", "time_info",0, 0, 0, offsetof(laFileItem, TimeModified), 0, 0, 0, 0, 0, 0, 0, LA_UDF_LOCAL);
        laAddIntProperty(p, "size", "Size", "File Size In Bytes", 0, 0, "Bytes", 0, 0, 0, 0, 0, offsetof(laFileItem, Size), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,LA_READ_ONLY);
        laAddStringProperty(p, "size_string", "Size String", "File size in convenient string", 0, 0, 0, 0, 0, 0, 0, laget_FileBrowserFileSizeString, 0, 0, LA_READ_ONLY);
        ep = laAddEnumProperty(p, "type", "Type", "General type of the file", 0, 0, 0, 0, 0, offsetof(laFileItem, Type), 0, 0, 0, 0, 0, 0, 0, 0, 0,LA_READ_ONLY);{
            laAddEnumItemAs(ep, "unknown", "Unknown", "Unknown/Uncategorized File Type", LA_FILETYPE_UNKNOWN, U'🖹');
            laAddEnumItemAs(ep, "udf", "UDF", "Uniform Data Format", LA_FILETYPE_UDF, U'📦');
            laAddEnumItemAs(ep, "document", "Document", "Document File", LA_FILETYPE_DOCUMENT, U'🖹');
            laAddEnumItemAs(ep, "image", "Image", "Image File", LA_FILETYPE_IMAGE, U'🖻');
            laAddEnumItemAs(ep, "audio", "Audio", "Audio File", LA_FILETYPE_AUDIO, U'𝄞');
            laAddEnumItemAs(ep, "video", "Video", "Video File", LA_FILETYPE_VIDEO, U'🎞');
            laAddEnumItemAs(ep, "compressed", "Compressed", "Compressed File", LA_FILETYPE_COMPRESSED, U'🗜');
            laAddEnumItemAs(ep, "font", "Font", "Font File", LA_FILETYPE_FONT, U'🗚');
            laAddEnumItemAs(ep, "vector", "Vector", "Vector File", LA_FILETYPE_VECTOR, U'🞎');
            laAddEnumItemAs(ep, "webpage", "Webpage", "Webpage", LA_FILETYPE_WEBPAGE, U'🌐');
            laAddEnumItemAs(ep, "meta", "Meta Data", "Meta Data Package", LA_FILETYPE_META, U'🖹');
            laAddEnumItemAs(ep, "blend", "Blend", "Blend File", LA_FILETYPE_BLEND, U'🖹');
            laAddEnumItemAs(ep, "pdf", "PDF", "PDF File", LA_FILETYPE_PDF, U'🖹');
            laAddEnumItemAs(ep, "exe", "Executable", "SYSWINDOW Executable", LA_FILETYPE_EXEC, U'🖦');
            laAddEnumItemAs(ep, "sys", "System", "System Files", LA_FILETYPE_SYS, U'🖹');
            laAddEnumItemAs(ep, "folder", "Folder", "Folder", LA_FILETYPE_FOLDER, U'📁');
            laAddEnumItemAs(ep, "lasdexchange", "LaSDExchange", "LA Scene Descriptive Exchange File", LA_FILETYPE_LASDEXCHANGE, 0);
        }
    }
    p = laAddPropertyContainer("bookmarked_folder", "Bookmarked_folder", "A bookmarked folder from GTK3", 0, laui_IdentifierOnly, sizeof(laBookmarkedFolder), 0, 0, 0);{
        laAddStringProperty(p, "name", "Name", "Bookmark name", 0, 0, 0, 0, 0, offsetof(laBookmarkedFolder, Name), 0, 0, 0, 0, LA_AS_IDENTIFIER|LA_UDF_LOCAL);
        laAddStringProperty(p, "path", "Path", "Bookmark path", 0, 0, 0, 0, 0, offsetof(laBookmarkedFolder, Path), 0, 0, 0, 0, LA_UDF_LOCAL);
    }
    ep = laAddEnumProperty(pc, "show_backups", "Show Backups", "Show backup files", 0, 0, 0, 0, 0, offsetof(laFileBrowser, ShowBackups), 0, laset_FileBrowserShowBackups, 0, 0, 0, 0, 0, 0, 0,0);{
        laAddEnumItemAs(ep, "NONE", "None", "Don't show backup files", 0, U'~');
        laAddEnumItemAs(ep, "SHOWN", "Shown", "Show backup files", 1, U'~');
    }
    ep = laAddEnumProperty(pc, "show_thumbnail", "Show Thumbnail", "Show thumbnail", 0, 0, 0, 0, 0, offsetof(laFileBrowser, ShowThumbnail), 0, laset_FileBrowserShowThumbnail, 0, 0, 0, 0, 0, 0, 0,0);{
        laAddEnumItemAs(ep, "NONE", "None", "Don't show thumbnail files", 0, U'🖼');
        laAddEnumItemAs(ep, "SHOWN", "Shown", "Show thumbnail files", 1, U'🖼');
    }
    ep = laAddEnumProperty(pc, "sort_name", "Name", "Sort by names",LA_WIDGET_ENUM_CYCLE, 0, 0, 0, 0, 0, laget_FileBrowserSortName,laset_FileBrowserSortName, 0, 0, 0, 0, 0, 0, 0,0);{
        laAddEnumItemAs(ep, "NONE", "None", "Don't sort", 0, 0);
        laAddEnumItemAs(ep, "SORT", "Sort", "Sort accending", 1, U'🔻');
        laAddEnumItemAs(ep, "REV", "Reversed", "Sort Descending", 2, U'🔺');
    }
    ep = laAddEnumProperty(pc, "sort_time", "Time", "Sort by times",LA_WIDGET_ENUM_CYCLE, 0, 0, 0, 0, 0, laget_FileBrowserSortTime,laset_FileBrowserSortTime, 0, 0, 0, 0, 0, 0, 0,0);{
        laAddEnumItemAs(ep, "NONE", "None", "Don't sort", 0, 0);
        laAddEnumItemAs(ep, "SORT", "Sort", "Sort accending", 1, U'🔻');
        laAddEnumItemAs(ep, "REV", "Reversed", "Sort Descending", 2, U'🔺');
    }
    ep = laAddEnumProperty(pc, "sort_size", "Size", "Sort by sizes",LA_WIDGET_ENUM_CYCLE, 0, 0, 0, 0, 0, laget_FileBrowserSortSize,laset_FileBrowserSortSize, 0, 0, 0, 0, 0, 0, 0,0);{
        laAddEnumItemAs(ep, "NONE", "None", "Don't sort", 0, 0);
        laAddEnumItemAs(ep, "SORT", "Sort", "Sort accending", 1, U'🔻');
        laAddEnumItemAs(ep, "REV", "Reversed", "Sort Descending", 2, U'🔺');
    }
    
    at = laCreateOperatorType("LA_udf_read", "Read", "Read a UDF file", 0, 0, OPEXT_UDFOperation, OPINV_UDFRead, OPMOD_UDFRead, U'📑', LA_ACTUATOR_SYSTEM);
    pc = laDefineOperatorProps(at, 0);
    at->UiDefine = laui_LinkerPanel;
    
    at = laCreateOperatorType("LA_udf_save_instance", "Save Instance", "Save a instance as a UDF block", 0, 0, OPEXT_UDFOperation, OPINV_UDFSaveInstance, OPMOD_UDFSaveInstance, U'📑', LA_ACTUATOR_SYSTEM);

    at = laCreateOperatorType("LA_managed_save", "Save as", "Save managed data blocks", 0, 0, OPEXT_ManagedSave, OPINV_ManagedSave, OPMOD_ManagedSave, U'🖫', LA_ACTUATOR_SYSTEM);
    pc = laDefineOperatorProps(at, 1);
    ep=laAddEnumProperty(pc, "show_page", "Show Page", "Show whether data blocks or UDF files", 0,0,0,0,0,offsetof(laManagedSaveExtra, ShowPage),0,laset_ManagedSavePage,0,0,0,0,0,0,0,0);
    laAddEnumItemAs(ep, "DATA_BLOCKS", "Data Blocks", "All data blocks", 0, 0);
    laAddEnumItemAs(ep, "FILES", "Files", "All Files", 1, 0);
    at->UiDefine = laui_ManagedSavePanel;
    
    laCreateOperatorType("LA_udf_propagate", "Propagate", "Propagate this file to all unassigned child nodes", 0, 0, 0, OPINV_UDFPropagate, 0, 0, LA_ACTUATOR_SYSTEM);
    
    laCreateOperatorType("LA_managed_save_new_file", "New File", "New managed UDF file", 0, 0, OPEXT_ManagedSave, OPINV_ManagedSaveNewFile, OPMOD_ManagedSaveNewFile, U'+', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_manage_udf", "UDF Manager", "Operations on all managed UDF files", 0, 0, 0, OPINV_UDFManager, 0, U'🔍', LA_ACTUATOR_SYSTEM);

    laCreateOperatorType("LA_add_resource_folder", "Add Resource Folder", "Add a resource folder entry for searching UDF references",
                               0, 0, 0, OPINV_AddResourceFolder, 0, U'🞧', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_remove_resource_folder", "Remove Resource Folder", "Remove a resource folder entry",
                               0, 0, 0, OPINV_RemoveResourceFolder, 0, U'🞫', LA_ACTUATOR_SYSTEM);
    
    laCreateOperatorType("LA_send_signal", "Send Signal", "Send signal to the UI", 0, 0, 0, OPINV_SendSignal, 0, 0, LA_ACTUATOR_SYSTEM);
    
    laCreateOperatorType("LA_run_toolbox_entry", "Run Entry", "Run toolbox entry", 0, 0, 0, OPINV_RunToolboxEntry, 0, 0, LA_ACTUATOR_SYSTEM)
        ->ParseArgs = la_RunToolboxEntryParser;
    laCreateOperatorType("LA_new_toolbox", "New Toolbox", "New custom toolbox", 0, 0, 0, OPINV_NewToolbox, 0, '+', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_remove_toolbox", "Remove Toolbox", "Remove custom toolbox", 0, 0, 0, OPINV_RemoveToolbox, OPMOD_RemoveToolbox, U'🞫', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_new_input_mapping", "New Mapping", "New input mapping", 0, 0, 0, OPINV_NewInputMapping, 0, '+', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_new_input_mapping_entry", "New Entry", "New input mapping entry", 0, 0, 0, OPINV_NewInputMappingEntry, 0, '+', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_remove_input_mapping", "Remove Mapping", "Remove input mapping", 0, 0, 0, OPINV_RemoveInputMapping, OPMOD_RemoveInputMapping, U'🞫', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_remove_input_mapping_entry", "Remove Entry", "Remove input mapping entry", 0, 0, 0, OPINV_RemoveInputMappingEntry, OPMOD_RemoveInputMappingEntry, U'🞫', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_reset_input_mapping_fields", "Reset Entry", "Reset mapping entry", 0, 0, 0, OPINV_ClearInputMappingFields, 0, U'⮌', LA_ACTUATOR_SYSTEM);
    at = laCreateOperatorType("LA_input_mapping_entry_select_signal", "Select Signal", "Select signal for this entry", 0, 0, 0, OPINV_InputMappingEntrySelectSignal, OPMOD_InputMappingEntrySelectSignal, U'⯆', LA_ACTUATOR_SYSTEM);
    at->UiDefine = laui_InputMappingEntrySignalSelector;
    at = laCreateOperatorType("LA_input_mapping_entry_select_operator", "Select Operator", "Select operator for this entry", 0, 0, 0, OPINV_InputMappingEntrySelectOperator, OPMOD_InputMappingEntrySelectOperator, U'⯆', LA_ACTUATOR_SYSTEM);
    at->UiDefine = laui_InputMappingEntryOperatorSelector;
    at = laCreateOperatorType("LA_input_mapping_entry_select_key", "Select Key", "Select key for this entry", 0, 0, OPEXT_InputMappingEntrySelectKey, OPINV_InputMappingEntrySelectKey, OPMOD_InputMappingEntrySelectKey, U'K', LA_ACTUATOR_SYSTEM);
    at->UiDefine = laui_InputMappingEntryKeySelector;
    pc = laDefineOperatorProps(at, 1);
    laAddStringProperty(pc,"pressed_string","Pressed String","String of the pressed key",LA_WIDGET_STRING_PLAIN,0,0,0,1,-1,0,laget_KeyDetectorPressedString,0,0,LA_READ_ONLY);
    
    laCreateOperatorType("LA_save_user_preferences", "Save Preferences", "Save user preferences", 0, 0, 0, OPINV_SaveUserPreferences, 0, 0, LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_restore_factory", "Restore Factory Settings", "Restore factory settings", 0, 0, 0, OPINV_RestoreFactorySettings, OPMOD_RestoreFactorySettings, 0, LA_ACTUATOR_SYSTEM);

    laCreateOperatorType("LA_confirm", "Confirm", "Confirm The Statement", 0, 0, 0, OPINV_DoNothing, 0, U'✔', LA_ACTUATOR_SYSTEM)
        ->ExtraInstructions = "feedback=CONFIRM;";
    laCreateOperatorType("LA_cancel", "Cancel", "Ignore The Statement", 0, 0, 0, OPINV_DoNothing, 0, U'🞫', LA_ACTUATOR_SYSTEM)
        ->ExtraInstructions = "feedback=CANCEL;";
    laCreateOperatorType("LA_pure_yes_no", "Yes Or No", "Show Yes Or No Box", 0, 0, 0, OPINV_PureYesNo, 0, U'❓', LA_ACTUATOR_SYSTEM);
    
    laCreateOperatorType("LA_delete_theme", "Delete Theme", "Delete a theme", 0, 0, 0, OPINV_DeleteTheme, OPMOD_DeleteTheme, U'🞫', LA_ACTUATOR_SYSTEM);
    laCreateOperatorType("LA_new_theme", "New Theme", "Create a new theme", 0, 0, 0, OPINV_NewTheme, 0, U'🞧', LA_ACTUATOR_SYSTEM);
}
