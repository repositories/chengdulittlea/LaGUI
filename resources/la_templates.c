/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../la_5.h"

extern LA MAIN;
extern struct _tnsMain *T;
extern const char* LA_UDF_EXTENSION_STRINGS[];

int laget_InstanceModified(void* instance);
void laget_TimeString(laTimeInfo *ti, char *result, char** resultdirect);

void laui_DefaultPropDetails(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn *c;
    laUiItem *b;
    c = laFirstColumn(uil);
    laSafeString* s=0;

    if(Extra&&Extra->EndInstance){ //button
        laGeneralUiExtraData* ex=Extra->EndInstance;
        laUiItem* ui=ex->ui;
        if(This&&This->LastPs&&This->EndInstance){
            laProp* p=This->LastPs->p; laPropContainer* pc=p->Container; void* inst=This->LastPs->UseInstance; 
            char FullPath[256]={0}; la_GetPropPackFullPath(This, FullPath);
            strSafePrint(&s,"%s", FullPath); if(ui->ExtraInstructions){ strSafePrint(&s,"(%s)", SSTR(ui->ExtraInstructions)); }
            laShowLabel(uil,c,p->Name,0,0);
            laShowSeparator(uil,c);
            laShowLabel(uil,c,p->Description,0,0)->Flags|=LA_TEXT_LINE_WRAP;
            laShowLabel(uil,c,SSTR(s),0,0)->Flags|=LA_TEXT_MONO|LA_UI_FLAGS_DISABLED;
        }else{
            laShowLabel(uil,c,ui->AT->Name,0,0);
            laShowSeparator(uil,c);
            laShowLabel(uil,c,ui->AT->Description,0,0)->Flags|=LA_TEXT_LINE_WRAP;
            strSafePrint(&s,"%s", ui->AT->Identifier); if(ui->ExtraInstructions){ strSafePrint(&s,"(%s)", SSTR(ui->ExtraInstructions)); }
            laShowLabel(uil,c,s->Ptr,0,0)->Flags|=LA_TEXT_MONO|LA_UI_FLAGS_DISABLED;
        }
    }else{
        if(!This || !This->LastPs){ laShowLabel(uil,c,"Can't access property details.",0,0); return; }

        char FullPath[256]={0}; la_GetPropPackFullPath(This, FullPath);
        laProp* p=This->LastPs->p; laPropContainer* pc=p->Container; void* inst=This->LastPs->UseInstance;

        laShowLabel(uil,c,p->Name,0,0);
        laShowSeparator(uil,c);
        laShowLabel(uil,c,p->Description,0,0)->Flags|=LA_TEXT_LINE_WRAP;
        if(p->PropertyType==(LA_PROP_ENUM)||(LA_PROP_ARRAY|LA_PROP_ENUM)){
            laEnumItem* ei=laGetEnumArrayIndexed(This,This->LastIndex);
            if(ei){ strSafeSet(&s,0);strSafePrint(&s,"Current: %s", ei->Description);laShowLabel(uil,c,s->Ptr,0,0)->Flags|=LA_UI_FLAGS_HIGHLIGHT|LA_TEXT_LINE_WRAP; }
        }
        laShowLabel(uil,c,FullPath,0,0)->Flags|=LA_TEXT_MONO|LA_UI_FLAGS_DISABLED;

        if(inst && !pc->OtherAlloc){
            laShowSeparator(uil,c); strSafeSet(&s,0); strSafePrint(&s,"[ %s > %s ]",
                pc->Name?pc->Name:pc->Identifier,p->Name?p->Name:p->Identifier, inst);
            int Level=0; laMemNodeHyper* m=memGetHead(inst,&Level);
            if(pc->SaverDummy){
                laSubProp*sp=pc->SaverDummy; laListHandle* a=(((char*)inst)+sp->ListHandleOffset); m=memGetHead(a->pFirst,&Level);
            }
            laListHandle* users=memGetUserList(inst);
            if(users){ int count,spec=laCountUserSpecific(users,p,&count); strSafePrint(&s," %d > %d Users",count,spec); }
            laShowLabel(uil,c,s->Ptr,0,0)->Flags|=LA_TEXT_MONO;

            if(Level==2 || pc->SaverDummy){
                if(m){
                    strSafeSet(&s,0);strSafePrint(&s, m->NUID.String);
                    if(laget_InstanceModified(inst)){ strSafePrint(&s, " 🌑 Modified"); }else{ strSafePrint(&s, " Clean"); }
                    laShowLabel(uil,c,s->Ptr,0,0)->Flags|=LA_TEXT_MONO;
                    laget_TimeString(&m->TimeCreated, FullPath, 0); 
                    strSafeSet(&s,0); strSafePrint(&s, "Created at %s", FullPath);
                    laShowLabel(uil,c,s->Ptr,0,0)->Flags|=LA_TEXT_MONO|LA_UI_FLAGS_DISABLED;
                }else{
                    laShowLabel(uil,c,"<Dummy File Unassigned>",0,0)->Flags|=LA_TEXT_MONO|LA_UI_FLAGS_DISABLED;
                }
            }

            if(m){
                if(Level==2){
                    laShowSeparator(uil,c);
                    if(m->FromFile){
                        //strSafeSet(&s,0); strSafePrint(&s,"File:\n%s", m->FromFile->BaseName);
                        //laShowLabel(uil,c,s->Ptr,0,0);
                        char safepath[256]; strEscapePath(safepath, m->FromFile->BaseName->Ptr);
                        strSafeSet(&s,0); strSafePrint(&s,"la.managed_udfs@basename=%s.__self", safepath);
                        laShowItemFull(uil,c,0,s->Ptr,0,0,laui_ManagedUDFItem,0)->Flags|=LA_UI_FLAGS_NO_GAP|LA_UI_FLAGS_NO_DECAL;
                    }else{
                        laShowLabel(uil,c,"<Not saved yet>",0,0)->Flags|=LA_UI_FLAGS_DISABLED;
                    }
                }
            }
            
        }
    }
    strSafeDestroy(&s);
}

void laui_DefaultNodeOperationsPropUiDefine(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED, int context){
    laColumn *c = laFirstColumn(uil); laColumn* cl,*cr; laSplitColumn(uil,c,0.5); cl=laLeftColumn(c,0);cr=laRightColumn(c,0);
    laShowItemFull(uil, c, This, "base.name",LA_WIDGET_STRING_PLAIN,0,0,0);
    laShowItemFull(uil, cl, This, "base.move",0,"direction=left;text=Move Left;icon=🡰;",0,0);
    laShowItemFull(uil, cr, This, "base.move",0,"text=Move Right;icon=🡲;",0,0);
    laShowItemFull(uil, c, This, "base.delete",0,0,0,0);
}
void laui_DefaultPropUiDefine(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED, int context){
    laColumn *c;
    laProp *p, *gp;

    if (OperatorProps && OperatorProps->LastPs){
        c = laFirstColumn(uil); gp = OperatorProps->LastPs->p;
        for (p = gp->SubProp->Props.pFirst; p; p = p->Item.pNext){
            if(p->PropertyType!=LA_PROP_OPERATOR) continue;
            laShowItem(uil, c, OperatorProps, p->Identifier);
        }
    }
    if (!This || !This->LastPs) return;

    c = laFirstColumn(uil); gp = This->LastPs->p;
    laPropContainer* pc=la_EnsureSubTarget(gp,This->EndInstance);
    laShowLabel(uil,c,gp->PropertyType==LA_PROP_SUB?pc->Name:gp->Name,0,0)->Flags|=LA_UI_FLAGS_DISABLED; 
    for (p = pc->Props.pFirst; p; p = p->Item.pNext){
        //la_ShowGeneralPropItem(uil, c, This, gp, p, 0, 0, 0);
        laShowItem(uil, c, This, p->Identifier);
    }
}
void laui_PropOperatorUiDefine(laUiList *uil, laPropPack *This, laPropPack *Unused, laColumn *UNUSED, int context){
    laColumn *c; laProp *p, *gp;

    if (This && This->LastPs){
        c = laFirstColumn(uil); gp = This->LastPs->p;
        laPropContainer* pc=la_EnsureSubTarget(gp,This->EndInstance);
        laShowLabel(uil,c,gp->PropertyType==LA_PROP_SUB?pc->Name:gp->Name,0,0)->Flags|=LA_UI_FLAGS_DISABLED; 
        for (p = pc->Props.pFirst; p; p = p->Item.pNext){
            if(p->PropertyType!=LA_PROP_OPERATOR) continue;
            laShowItem(uil, c, This, p->Identifier);
        }
    }
}
void laui_StringPropUiDefine(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn *c; laProp *p, *gp;

    if (!This || !This->LastPs) return;
    c = laFirstColumn(uil);
    gp = This->LastPs->p;

    if(Extra){
        laShowItemFull(uil, c, Extra, "copy_selection",0,"mode=CUT;text=Cut;icon=✀",0,0);
        laUiItem* b=laBeginRow(uil,c,0,0);
        laShowItemFull(uil, c, Extra, "copy_selection",0,"text=Copy",0,0)->Expand=1;
        laShowItemFull(uil, c, This, "copy",0,"text=All",0,0);
        laEndRow(uil,b);
    }else{
        laShowItem(uil, c, This, "copy");
    }
    laShowItem(uil, c, This, "paste");
    laShowSeparator(uil,c);
    laShowItem(uil, c, This, "restore");
    if(gp->Tag&LA_PROP_IS_FILE){ laShowItem(uil, c, This, "get_file_path"); }
    if(gp->Tag&LA_PROP_IS_FOLDER){ laShowItem(uil, c, This, "get_folder_path"); }
}

void laui_IdentifierOnly(laUiList *uil, laPropPack *This, laPropPack *OP_UNUSED, laColumn *Extra, int context){
    laColumn *col = Extra, *c, *cl, *cr, *crl, *crr, *cll, *clr, *clrl, *clrr, *clrrl, *clrrr;
    laUiItem *ui;

    c = laFirstColumn(uil);

    laShowItemFull(uil, c, This, "identifier", LA_WIDGET_STRING_PLAIN,0, 0, 0);
}

void laRebuildPathRecursive(laProp *p, char *buf){
    if (p->Container){
        laRebuildPathRecursive(p->Container, buf);
        sprintf(buf, ".%s", p->Identifier);
    }else
        sprintf(buf, "%s", p->Identifier);
}

void laui_IntPropInfo(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *ExtraColumns, int context){
    laColumn *col, *cl, *cr;
    laUiItem *b1,*b2;
    laUiList *tuil;

    col = laFirstColumn(uil);
    laSplitColumn(uil, col, 0.2);
    cl = laLeftColumn(col, 1);
    cr = laRightColumn(col, 0);

    laShowItemFull(uil, cr, Base, "base.identifier", LA_WIDGET_STRING_PLAIN,0, 0, 0);
    b1= laOnConditionToggle(uil, cl, 0,0,0,0,0);{
        for(laProp* p=MAIN.ContainerInt->Props.pFirst;p;p=p->Item.pNext){
            laShowItem(uil,cr,Base,p->Identifier);
        }
    }laEndCondition(uil,b1);
}
void laui_FloatPropInfo(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *ExtraColumns, int context){
    laColumn *col, *cl, *cr;
    laUiItem *b1,*b2;
    laUiList *tuil;

    col = laFirstColumn(uil);
    laSplitColumn(uil, col, 0.2);
    cl = laLeftColumn(col, 1);
    cr = laRightColumn(col, 0);

    laShowItemFull(uil, cr, Base, "base.identifier", LA_WIDGET_STRING_PLAIN,0, 0, 0);
    b1= laOnConditionToggle(uil, cl, 0,0,0,0,0);{
        for(laProp* p=MAIN.ContainerFloat->Props.pFirst;p;p=p->Item.pNext){
            laShowItem(uil,cr,Base,p->Identifier);
        }
    }laEndCondition(uil,b1);
}

void laui_SubPropInfoDefault(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn *c, *cl, *cr, *cll, *clr, *crl, *crr, *crrl, *crrr;
    laUiItem *bracket, *b1, *b2;
    laPropContainer *pc;
    laProp *p;
    char buf[LA_RAW_CSTR_MAX_LEN]={0};

    c = laFirstColumn(uil);
    laSplitColumn(uil, c, 0.2); cl = laLeftColumn(c, 1); cr = laRightColumn(c, 0);
    laSplitColumn(uil, cr, 0.5); crrl = laLeftColumn(cr, 1*6); crrr = laRightColumn(cr, 0);

    laPropContainer* subc=Base->LastPs->p->SubProp;
    if (!((laSubProp*)Base->LastPs->p)->GetType){
        if (!subc) subc = la_ContainerLookup(((laSubProp *)Base->LastPs->p)->TargetID);
        if (!subc) return; Base->LastPs->p->SubProp=subc;
    }else{
        subc=((laSubProp*)Base->LastPs->p)->GetType(Base->EndInstance);
    }

    if (!Base->LastPs->p->UDFIsRefer && !Base->LastPs->p->UDFNoCreate && !Base->LastPs->p->UDFIsSingle){
        laShowItem(uil, cr, Base, "identifier")->Flags|=LA_UI_FLAGS_PLAIN;
        bracket = laOnConditionToggle(uil, cl, 0, 0, 0, 0, 0);
    }
    for (p = subc->Props.pFirst; p; p = p->Item.pNext){
        if (!(p->PropertyType & LA_PROP_SUB)){
            buf[0] = 0;
            sprintf(buf, "la.prop_containers#%s.properties#%s.name", p->Container->Identifier, p->Identifier);
            laShowItemFull(uil, crrl, 0, buf, LA_WIDGET_STRING_PLAIN,0, 0, 0);
            laShowItem(uil, crrr, Base, p->Identifier);
            //buf[0] = 0; sprintf(buf, "la.prop_containers#%s.properties#%s.name", p->Container->Identifier, p->Identifier); laShowItemFull(uil, crl, 0, buf, 0, laui_SubPropInfoDefault, 0);
            //buf[0] = 0; sprintf(buf, "la.prop_containers#%s.properties#%s.description", p->Container->Identifier, p->Identifier); laShowItemFull(uil, crr, 0, buf, 0, laui_SubPropInfoDefault, 0);
            laShowSeparator(uil, c);
        }else{
            //buf[0] = 0; sprintf(buf, "la.prop_containers#%s.properties#%s.name", p->Container->Identifier, p->Identifier); laShowItemFull(uil, crrl, 0, buf, LA_WIDGET_STRING_PLAIN, 0, 0);
            //buf[0] = 0; sprintf(buf, "la.prop_containers#%s.properties#%s.name", p->Container->Identifier, p->Identifier); laShowItemFull(uil, crl, 0, buf, 0, laui_SubPropInfoDefault, 0);
            //buf[0] = 0; sprintf(buf, "la.prop_containers#%s.properties#%s.description", p->Container->Identifier, p->Identifier); laShowItemFull(uil, crr, 0, buf, 0, laui_SubPropInfoDefault, 0);
            if (p->UDFIsRefer) laShowLabel(uil, crrr, "🠞", 0, 0);
            elif (p->UDFNoCreate) laShowLabel(uil, crrr, "n", 0, 0);
            else laShowLabel(uil, crrr, "🗇", 0, 0);

            buf[0] = 0;
            sprintf(buf, "la.prop_containers#%s.icon_id", ((laSubProp *)p)->TargetID);
            laShowIcon(uil, cl, 0, buf, 0);

            b1 = laOnConditionToggle(uil, crrl, 0, 0, 0, 0, 0);{ b1->Flags|=LA_TEXT_ALIGN_LEFT;
                sprintf(buf, "text=...%s;", p->Name);
                strSafeSet(&b1->ExtraInstructions, buf);
                laUiDefineFunc f=laui_SubPropInfoDefault;
                //if(!context) f=((laSubProp*)p)->GetType?0:(p->UiDefine?p->UiDefine:(p->SubProp&&p->SubProp->UiDefine?p->SubProp->UiDefine:laui_SubPropInfoDefault));
                laShowItemFull(uil, cr, Base, p->Identifier, 0,0, f, context);
            }
            laEndCondition(uil, b1);
            laShowSeparator(uil, c);
        }
    }
    if (!Base->LastPs->p->UDFIsRefer && !Base->LastPs->p->UDFNoCreate && !Base->LastPs->p->UDFIsSingle){
        laEndCondition(uil, bracket);
    }
    //laShowItemFull(uil, crl, Base, "identifier", 0, 0, 0);
    //p = Base->LastPs->p->SubProp->Props.pFirst;
}
void laui_SubPropSelection(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn *c, *cl, *cr, *cll, *clr, *crl, *crr, *crrl, *crrr;
    laUiItem *bracket, *b1, *b2;
    laPropContainer *pc;
    laProp *p;
    laPropPack *pp;
    char buf[LA_RAW_CSTR_MAX_LEN]={0};

    c = laFirstColumn(uil);
    laSplitColumn(uil, c, 0.2);
    cl = laLeftColumn(c, 1);
    cr = laRightColumn(c, 0);
    laSplitColumn(uil, cr, 0.8);
    crl = laLeftColumn(cr, 0);
    crr = laRightColumn(cr, 100);
    laSplitColumn(uil, crl, 0.5);
    crrl = laLeftColumn(crl, 150);
    crrr = laRightColumn(crl, 0);

    if (!Base->LastPs->p->SubProp) Base->LastPs->p->SubProp = la_ContainerLookup(((laSubProp *)Base->LastPs->p)->TargetID);

    if (!Base->LastPs->p->UDFIsRefer){
        laShowItemFull(uil, cr, Base, "identifier", LA_WIDGET_STRING_PLAIN,0, 0, 0);
        bracket = laOnConditionToggle(uil, cl, 0, 0, 0, 0, 0);
    }

    for (p = Base->LastPs->p->SubProp->Props.pFirst; p; p = p->Item.pNext){
        if (p->PropertyType == LA_PROP_SUB){
            //buf[0] = 0; sprintf(buf, "la.prop_containers#%s.properties#%s.name", p->Container->Identifier, p->Identifier);
            //laShowItemFull(uil, crrl, 0, buf, LA_WIDGET_STRING_PLAIN, 0, 0);

            buf[0] = 0;
            sprintf(buf, "identifier=%s;", p->Identifier);
            laShowItemFull(uil, crrr, Base, "put_data_block", 0, buf, 0, 0);

            //laShowLabel(uil, crr, "PUT", 0, 0);
            b1 = laOnConditionToggle(uil, crrl, 0, 0, 0, 0, 0);{
                buf[0] = 0; sprintf(buf, "text=...%s;", p->Name); strSafeSet(&b1->ExtraInstructions, buf);
                laShowItemFull(uil, cr, Base, p->Identifier, 0,0, laui_SubPropSelection, 0)->Flags|=LA_TEXT_ALIGN_LEFT;
            }
            laEndCondition(uil, b1);

            laShowSeparator(uil, c);
        }
    }
    if (!Base->LastPs->p->UDFIsRefer){
        laEndCondition(uil, bracket);
    }
    //laShowItemFull(uil, crl, Base, "identifier", 0, 0, 0);
    //p = Base->LastPs->p->SubProp->Props.pFirst;
}

void laui_LinkerSelectionProp(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn *c, *cl, *cr, *cll, *clr, *crl, *crr, *crrl, *crrr;
    laUiItem *bracket, *b1, *b2;
    laPropContainer *pc;
    laProp *p;
    laPropPack *pp;
    char buf[LA_RAW_CSTR_MAX_LEN]={0};

    c = laFirstColumn(uil);
    laSplitColumn(uil, c, 0.4);
    cl = laLeftColumn(c, 1);
    cr = laRightColumn(c, 0);

    b1=laBeginRow(uil,cr,0,0);
    laShowItemFull(uil, cr, Base, "identifier", LA_WIDGET_STRING_PLAIN,0, 0, 0);
    laShowIcon(uil, cr, Base, "pp.last_ps.property.sub_icon_id", 0);
    laEndRow(uil,b1);

    bracket = laOnConditionToggle(uil, cl, 0, 0, 0, 0, 0);{
        laShowItemFull(uil, cr, Base, "instances", 0,0, laui_LinkerSelectionInstance, 0);
    }
    bracket->State=LA_BT_ACTIVE;
    laEndCondition(uil, bracket);
}
void laui_LinkerSelectionInstance(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn *c, *cl, *cr, *cll, *clr, *crl, *crr, *crrl, *crrr;
    laUiItem *bracket, *b1, *b2;
    laPropContainer *pc;
    laProp *p;
    laPropPack *pp;
    char buf[LA_RAW_CSTR_MAX_LEN]={0};

    c = laFirstColumn(uil);
    laSplitColumn(uil, c, 0.4);
    cl = laLeftColumn(c, 1);
    cr = laRightColumn(c, 0);

    laShowItemFull(uil, cr, Base, "identifier", LA_WIDGET_STRING_PLAIN,0, 0, 0);

    b1 = laOnConditionToggle(uil, cl, 0, 0, 0, 0, 0);{
        laShowItemFull(uil, cr, Base, "children", 0,0, laui_LinkerSelectionProp, 0);
    }
    laEndCondition(uil, b1);
}

void laui_DefaultPanelTitleBar(laUiList *uil, laPropPack *Base, laPropPack *Extra, laUiDefineFunc header){
    laColumn *c, *cl, *cr, *cll, *clr, *crl, *crr, *cb1, *cb2, *cb3;
    laUiItem *bracket, *b1, *b2;

    c = laFirstColumn(uil);
    laSplitColumn(uil, c, 0.3);
    cl = laLeftColumn(c, 1);
    cr = laRightColumn(c, 0);

    b1=laBeginRow(uil,cr,0,0);
    laUiItem* tu=laShowItemFull(uil, cr, Base, "title", LA_WIDGET_STRING_PLAIN,0, 0, 0);tu->Expand=1;tu->Flags|=LA_UI_FLAGS_NO_TOOLTIP;
    laShowItem(uil, cr, Base, "hide")->Flags|=LA_UI_FLAGS_ICON;
    laShowItem(uil, cr, Base, "dock")->Flags|=LA_UI_FLAGS_ICON;
    laShowItem(uil, cr, Base, "close")->Flags|=LA_UI_FLAGS_ICON;
    laEndRow(uil,b1);
    bracket = laOnConditionToggle(uil, cl, 0, 0, 0, 0, 0);{
        if(!Extra->LastPs->p->SubProp->Props.pFirst){
            laShowLabel(uil, cr, "Panel doesn't have configurable property.", 0, 0);
        }else{
            if(header){
                laShowLabel(uil, cr, "Panel properties:", 0, 0);
                header(uil, Base, Extra, cr, 0);
            }else{
                for(laProp* p = Extra->LastPs->p->SubProp->Props.pFirst;p;p=p->Item.pNext){
                    if(p->PropertyType&LA_PROP_SUB){
                        laShowItemFull(uil, cr, Extra, p->Identifier, LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0);
                    }else{ laShowItem(uil, cr, Extra, p->Identifier); }
                }
            }
        }
        //laShowItem(uil, cr, 0,"la.example_int");
        laShowSeparator(uil, c);
        //laShowLabel(uil, c, "Position:", 0, 0);
        //laShowItemFull(uil, c, Base, "position", LA_WIDGET_INT, 0, 0);
        //laShowLabel(uil, c, "Size:", 0, 0);
        //laShowItemFull(uil, c, Base, "size", LA_WIDGET_INT, 0, 0);
        //laShowLabel(uil, c, "Snapping:", 0, 0);
        //laShowItem(uil, c, Base, "snap_enable")->Flags=LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_TRANSPOSE;
        //laShowItemFull(uil, c, Base, "snap", LA_WIDGET_INT, 0, 0);
    }
    laEndCondition(uil, bracket);
}
void laui_DefaultOperatorPanelTitleBar(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn *c, *cl, *cr, *cll, *clr, *crl, *crr;
    laUiItem *bracket, *b1, *b2;

    c = laFirstColumn(uil);

    b1 = laBeginRow(uil,c,0,0);
    laUiItem* tu=laShowItemFull(uil, c, Base, "title", LA_WIDGET_STRING_PLAIN,0, 0, 0);tu->Expand=1;tu->Flags|=LA_UI_FLAGS_NO_TOOLTIP;
    laShowItem(uil, c, 0, "LA_cancel");//->Flags|=LA_UI_FLAGS_ICON;
    laEndRow(uil, b1);
}

void laui_ColumnItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context){
    laColumn *c0, *c, *cl, *cr, *cll, *clr, *crl, *crr, *clll, *cllr, *cll2, *clr2;
    laUiItem *bracket, *b1, *b2;

    c0 = laFirstColumn(uil);
    laSplitColumn(uil, c0, 0.8);
    cll = laLeftColumn(c0, 80);
    c = laRightColumn(c0, 0);
    laSplitColumn(uil, c, 0.7);
    cl = laLeftColumn(c, 0);
    cr = laRightColumn(c, 200);
    laSplitColumn(uil, cr, 0.5);
    crl = laLeftColumn(cr, 0);
    crr = laRightColumn(cr, 70);

    laSplitColumn(uil, cll, 0.5);
    clll = laLeftColumn(cll, 1);
    cllr = laRightColumn(cll, 0);

    laSplitColumn(uil, cl, 0.5);
    cll2 = laLeftColumn(cl, 0);
    clr2 = laRightColumn(cl, 70);

    //laShowItem(uil, c, Base, "self");

    bracket = laOnConditionThat(uil, crr, laPropExpression(0, "live_editor.current_ui")); // laOnConditionThat(uil, 0, "live_editor", la_LiveEditAllowShow);
    laShowItemFull(uil, cll, Base, "select_column", 0, "text=Select;", 0, 0);
    laEndCondition(uil, bracket);

    bracket = laOnConditionThat(uil, c, laPropExpression(Base, "sub_left"));{ //laOnConditionThat(uil, Base, 0, laui_SnapButtonShow); {
        laShowItem(uil, cll2, Base, "split_at");
        laShowItemFull(uil, clr2, Base, "merge_column", 0, "text=Merge;", 0, 0);

        b1 = laOnConditionThat(uil, c, laPropExpression(Base, "snap_state"));{
            laShowItem(uil, crl, Base, "snap_width");
        }
        laElse(uil, b1);{
            laShowLabel(uil, crl, "Align:", 0, 0)->Flags|=LA_TEXT_ALIGN_RIGHT;
        }
        laEndCondition(uil, b1);

        laShowItem(uil, crr, Base, "snap_state");

        laShowSeparator(uil, c0);

        b1 = laOnConditionToggle(uil, clll, 0, 0, 0, 0, 0);{
            laShowItemFull(uil, c, Base, "sub_left", LA_WIDGET_COLLECTION_SINGLE,0, laui_ColumnItem, 0);
        }
        laElse(uil, b1);{
            laShowLabel(uil, cllr, "Left", 0, 0);
        }
        laEndCondition(uil, b1);

        laShowSeparator(uil, c0);

        b1 = laOnConditionToggle(uil, clll, 0, 0, 0, 0, 0);{
            laShowItemFull(uil, c, Base, "sub_right", LA_WIDGET_COLLECTION_SINGLE,0, laui_ColumnItem, 0);
        }
        laElse(uil, b1);{
            laShowLabel(uil, cllr, "Right", 0, 0);
        }
        laEndCondition(uil, b1);
    }
    laElse(uil, bracket);{
        laShowItem(uil, c, Base, "split_column");
    }
    laEndCondition(uil, bracket);
}

void laui_DefaultMenuButtonsFileEntries(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context){
    laColumn* c=laFirstColumn(uil);
            
    laShowLabel(uil, c, "Function Test", 0, 0)->Flags|=LA_TEXT_MONO|LA_UI_FLAGS_DISABLED;
    laShowItem(uil, c, 0, "LA_pure_yes_no");
    laShowItem(uil, c, 0, "LA_file_dialog");
    laShowItem(uil, c, 0, "LA_udf_read");
    laShowItemFull(uil, c, 0, "LA_udf_read",0,"mode=append;text=Append",0,0);
    laShowItem(uil, c, 0, "LA_managed_save");
    laShowItem(uil, c, 0, "LA_manage_udf");

    laShowLabel(uil, c, "Other Entries", 0, 0)->Flags|=LA_TEXT_MONO|LA_UI_FLAGS_DISABLED;
    laShowItem(uil, c, 0, "LA_terminate_program");
}
void laui_DefaultMenuButtonsEditEntries(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context){
    laColumn* c=laFirstColumn(uil);
    laShowLabel(uil, c, "History", 0, 0)->Flags|=LA_TEXT_MONO|LA_UI_FLAGS_DISABLED;
    laShowItem(uil, c, 0, "LA_undo")->Flags|=LA_UI_FLAGS_NO_CONFIRM;
    laShowItem(uil, c, 0, "LA_redo")->Flags|=LA_UI_FLAGS_NO_CONFIRM;
    laShowSeparator(uil,c);
    laShowItemFull(uil, c, 0, "LA_panel_activator",0,"panel_id=LAUI_histories;text=Histories",0,0);
}
void laui_DefaultMenuButtonsOptionEntries(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context){
    laColumn* c=laFirstColumn(uil);
    laShowLabel(uil, c, "Settings", 0, 0)->Flags|=LA_TEXT_MONO|LA_UI_FLAGS_DISABLED;
    laShowItemFull(uil, c, 0, "LA_panel_activator", 0, "panel_id=LAUI_user_preferences;", 0, 0);
    laShowLabel(uil, c, "Information", 0, 0)->Flags|=LA_TEXT_MONO|LA_UI_FLAGS_DISABLED;
    laShowItemFull(uil, c, 0, "LA_panel_activator", 0, "panel_id=LAUI_about;text=About;", 0, 0);
#ifdef _WIN32
    laShowSeparator(uil,c);
    laShowItem(uil,c,0,"LA_toggle_system_console");
#endif
    if(MAIN.InitArgs.HasTerminal){
#ifndef _WIN32
        laShowSeparator(uil,c);
#endif
        laShowItemFull(uil, c, 0, "LA_panel_activator", 0, "panel_id=LAUI_terminal;", 0, 0);
    }
}
void laui_DefaultMenuButtons(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context){
    laUiList *muil; laColumn *mc,*c = laFirstColumn(uil);
    muil = laMakeMenuPage(uil, c, "File");{
        mc = laFirstColumn(muil); laui_DefaultMenuButtonsFileEntries(muil,pp,actinst,extracol,0);
    }
    if(MAIN.InitArgs.HasHistories){
        muil = laMakeMenuPage(uil, c, "Edit");{
            mc = laFirstColumn(muil); laui_DefaultMenuButtonsEditEntries(muil,pp,actinst,extracol,0);
        }
    }
    muil = laMakeMenuPage(uil, c, "Options"); {
        mc = laFirstColumn(muil); laui_DefaultMenuButtonsOptionEntries(muil,pp,actinst,extracol,0);
    }
}
void laui_DefaultMenuExtras(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context){
    laColumn *c = laFirstColumn(uil);
    laShowLabel(uil, c, MAIN.MenuProgramName, 0, 0)->Expand=1;
}

void laui_DefaultMenuBarActual(laUiList *uil, laPropPack *pp, laPropPack *actinst, laColumn *extracol, int context){
    laUiList *muil,*backuil;
    laColumn *c,*mc,*backc;
    laUiItem* bb,*mbb;

    c = laFirstColumn(uil);
    laUiItem* row=laBeginRow(uil,c,0,0);

    bb=laOnConditionThat(uil,c,laPropExpression(0,"la.windows.operator_hints"));{

        laShowItem(uil,c,0,"la.windows.operator_hints");

    }laElse(uil,bb);{
        laShowItem(uil, c, 0, "LA_new_panel")->Flags=LA_UI_FLAGS_ICON;

#ifdef LAGUI_ANDROID
        backc=c; backuil=uil;
        uil=laMakeMenuPageEx(uil,c,"Menu",/*LA_UI_FLAGS_NO_CONFIRM*/0); c=laFirstColumn(uil);
        mbb=laBeginRow(uil,c,0,0);
#endif

        if(MAIN.MenuButtons){ MAIN.MenuButtons(uil,0,0,0,0); }

#ifdef LAGUI_ANDROID
        laEndRow(uil,mbb); mbb=laBeginRow(uil,c,0,0);
#else
        laShowSeparator(uil,c);
#endif
        laUiItem* mui=laOnConditionThat(uil,c,laPropExpression(0,"la.windows.maximized_ui"));{
            laShowItemFull(uil,c,0,"LA_canvas_ui_maximize", 0, "icon=🡰;restore=true;text=Restore Layout", 0, 0);
        }laElse(uil,mui);{
            laUiItem* ui=laOnConditionThat(uil,c,laPropExpression(0,"la.windows.maximized_block"));{
                laShowItemFull(uil,c,0,"LA_block_maximize", 0, "icon=🡰;restore=true;text=Restore Layout", 0, 0);
            }laElse(uil,ui);{
                laShowItemFull(uil, c, 0, "LA_switch_layout", 0, "icon=🞐;show_list=true;", 0, 0)->Flags|=LA_UI_FLAGS_ICON;
                laShowItemFull(uil,c,0,"LA_switch_layout", 0, "icon=🡰;reverse=true;", 0, 0)->Flags|=LA_UI_FLAGS_ICON;
                laShowItemFull(uil,c,0,"LA_switch_layout", 0, "icon=🡲;", 0,0)->Flags|=LA_UI_FLAGS_ICON;
                laShowItem(uil, c, 0, "la.windows.layouts.identifier");
#ifndef LAGUI_ANDROID
                laShowItem(uil,c,0,"LA_new_window")->Flags|=LA_UI_FLAGS_ICON;
#endif
            }laEndCondition(uil, ui);
        }laEndCondition(uil,mui);

        laUiItem* uc=laOnConditionThat(uil,c,laPropExpression(0,"la.user_preferences.enable_color_management"));{
#ifdef LAGUI_ANDROID
        laEndRow(uil,mbb); mbb=laBeginRow(uil,c,0,0);
        laShowItem(uil,c,0,"la.windows.output_color_space");
#else
                laShowSeparator(uil,c);
                laShowItemFull(uil,c,0,"la.user_preferences.auto_switch_color_space",0,"icon=A",0,0)->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT|LA_UI_FLAGS_NO_CONFIRM;
                laUiItem* uc1=laOnConditionThat(uil,c,laPropExpression(0,"la.user_preferences.auto_switch_color_space"));{
                    laShowItem(uil,c,0,"la.windows.output_color_space")->Flags|=LA_UI_FLAGS_PLAIN;
                }laElse(uil,uc1);{
                    laShowItem(uil,c,0,"la.windows.output_color_space");
                }laEndCondition(uil, uc1);
#endif
                laShowItemFull(uil,c,0,"la.windows.output_show_overflow",0,"text=🟩;",0,0)->Flags|=LA_UI_FLAGS_NO_CONFIRM;
                laShowItemFull(uil,c,0,"la.windows.output_proofing",0,0,0,0)->Flags|=LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_ICON|LA_UI_FLAGS_HIGHLIGHT|LA_UI_FLAGS_NO_CONFIRM;
                laShowItemFull(uil,c,0,"la.windows.use_composing",0,"text=☀",0,0)->Flags|=LA_UI_FLAGS_NO_CONFIRM;
                laUiItem* cmp=laOnConditionThat(uil,c,laPropExpression(0,"la.windows.use_composing"));{
                    muil = laMakeMenuPageEx(uil, c, "⯆",LA_UI_FLAGS_NO_CONFIRM);{ mc = laFirstColumn(muil);
                        laUiItem*b,*ui;
                        b=laBeginRow(muil,mc,0,0); laShowLabel(muil,mc,"γ",0,0);
                        ui=laShowItemFull(muil, mc, 0, "la.windows.composing_gamma",0,"text=Gamma",0,0); ui->Expand=1;
                        laShowItem(muil,mc,&ui->PP,"restore")->Flags|=LA_UI_FLAGS_NO_CONFIRM|LA_UI_FLAGS_ICON; laEndRow(muil,b);
                        b=laBeginRow(muil,mc,0,0); laShowLabel(muil,mc,"◩",0,0);
                        ui=laShowItemFull(muil, mc, 0, "la.windows.composing_blackpoint",0,"text=Blackpoint",0,0); ui->Expand=1;
                        laShowItem(muil,mc,&ui->PP,"restore")->Flags|=LA_UI_FLAGS_NO_CONFIRM|LA_UI_FLAGS_ICON; laEndRow(muil,b);
                    }
                }laEndCondition(uil, cmp);

        }laEndCondition(uil, uc);

#ifdef LAGUI_ANDROID
        laEndRow(uil,mbb);
        c=backc; uil=backuil;
#endif

        if(MAIN.MenuExtras){ laShowSeparator(uil,c); MAIN.MenuExtras(uil,0,0,0,0); }

        laUiItem* b1=laOnConditionThat(uil,c,laPropExpression(0,"la.windows.panels_hidden"));
#ifndef LAGUI_ANDROID
            laShowSeparator(uil,c);
#endif
            muil = laMakeMenuPage(uil, c, "🡻 Minimized");{
                mc = laFirstColumn(muil);
                laShowItemFull(muil, mc, 0, "la.windows.panels_hidden", 0,0, laui_IdentifierOnly,0);
            }
        laEndCondition(uil, b1);

#ifndef LAGUI_ANDROID
        b1=laOnConditionThat(uil,c,laPropExpression(0,"la.windows.is_fullscreen"));{
            laShowItemFull(uil,c,0,"LA_fullscreen",0,"restore=true;text=Restore;icon=🡷",0,0)->Flags|=LA_UI_FLAGS_EXIT_WHEN_TRIGGERED;
        }laElse(uil,b1);{
            laShowItemFull(uil,c,0,"LA_fullscreen",0,0,0,0)->Flags|=LA_UI_FLAGS_EXIT_WHEN_TRIGGERED;
        }laEndCondition(uil, b1);
#endif

    }laEndCondition(uil,bb);

    laEndRow(uil,row);
}
void laui_DefaultMenuBar(laWindow *w){
    laPanel *p;

    p =laCreateTopPanel(w, 0, 0, 0, 0, LA_RH, 0, 0, 0, 0, -1, -1, -1, 0);
    p->IsMenuPanel = 1;
    laui_DefaultMenuBarActual(&p->TitleBar, &p->PP, &p->PropLinkPP, 0, 0);
}

void laui_ThemeListItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context){
    laColumn *col, *cl, *cr;
    laUiItem *bracket;

    col = laFirstColumn(uil);

    bracket = laBeginRow(uil,col,0,0);
    laShowItemFull(uil, col, Base, "name", LA_WIDGET_STRING_PLAIN,0, 0, 0)->Expand=1;
    laShowItemFull(uil, col, Base, "author", LA_WIDGET_STRING_PLAIN,0, 0, 0);
    laEndRow(uil,bracket);
}
void laui_Theme(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context){
    laColumn *col, *cl, *cr, *cll, *clr;
    laUiItem *sui;
    laUiList *tuil;
    laUiItem *ui,*b;

    //col = laFirstColumn(uil);
    //tuil = laMakeGroup(uil, col, "����ժҪ", 0, 0)->Page; {
    //	col = laFirstColumn(tuil);
    //	laSplitColumn(tuil, col, 0.7);
    //	cl = laLeftColumn(col, 0);
    //	cr = laRightColumn(col, 0);
    //	laSplitColumn(tuil, cr, 0.6);
    //	crl = laLeftColumn(cr, 0);
    //	crr = laRightColumn(cr, 70);
    //	laShowItemFull(tuil, cl, 0, "themes.name", 0, 0, 0);
    //	laShowItemFull(tuil, crl, 0, "themes.author", 0, 0, 0);
    //	laShowLabel(tuil, crr, 0, "EXEC", 0, 0, 0);
    //}

    col = laFirstColumn(uil); laSplitColumn(uil,col,0.6);
    cl=laLeftColumn(col,0); cr=laRightColumn(col,0);
    
    laShowLabel(uil,col,"Theme Details",0,0)->Flags|=LA_TEXT_ALIGN_CENTER;


    ui=laBeginRow(uil,col, 0,0);
    laShowItem(uil,col,Base, "name")->Expand=1;
    laShowItem(uil,col,Base, "delete");
    laEndRow(uil, ui);

    ui=laBeginRow(uil,col, 0,0);
    laShowItem(uil,col,Base, "save_as");
    laShowItem(uil,col,Base, "duplicate");
    laEndRow(uil, ui);
    
    laShowSeparator(uil,col);

    ui=laBeginRow(uil,col, 0,0);
    b=laOnConditionToggle(uil,col,0,0,0,0,0);{
        laShowLabel(uil,col,"Basics",0,0);
        laEndRow(uil, ui);
        laShowItem(uil, cl, Base, "color"); laShowLabel(uil, cr, "Base Color A",0,0);
        laShowItem(uil, cl, Base, "color_b"); laShowLabel(uil, cr, "B",0,0);
        laShowItem(uil, cl, Base, "color_c"); laShowLabel(uil, cr, "C",0,0);
        laShowItem(uil, cl, Base, "accent_color"); laShowLabel(uil, cr, "Accent Color",0,0);
        laShowItem(uil, cl, Base, "warning_color"); laShowLabel(uil, cr, "Warning Color",0,0);
        laShowItem(uil, cl, Base, "cursor_alpha");
        laShowItem(uil, cl, Base, "selection_alpha");
        laShowItem(uil, cl, Base, "shadow_alpha");
        laShowItem(uil, cl, Base, "text_shadow_alpha");
        laShowItem(uil, cr, Base, "inactive_mix");
        laShowItem(uil, cr, Base, "inactive_saturation");
    }laElse(uil,b);{
        laShowLabel(uil,col,"Basics",0,0); laEndRow(uil, ui);
    }laEndCondition(uil,b); 

    ui=laBeginRow(uil,col, 0,0);
    b=laOnConditionToggle(uil,col,0,0,0,0,0);{
        laShowLabel(uil, col, "Nodes",0,0);
        laEndRow(uil, ui);
        laShowItem(uil, cl, Base, "wire_brightness");
        laShowItem(uil, cl, Base, "wire_saturation");
        laShowItem(uil, cl, Base, "wire_transparency");
    }laElse(uil,b);{
        laShowLabel(uil,col,"Nodes",0,0); laEndRow(uil, ui);
    }laEndCondition(uil,b);

    if(MAIN.InitArgs.HasWorldObjects){
        ui=laBeginRow(uil,col, 0,0);
        b=laOnConditionToggle(uil,col,0,0,0,0,0);{
            laShowLabel(uil, col, "Meshes",0,0);
            laEndRow(uil, ui);
            laShowItem(uil, cl, Base, "edge_transparency");
            laShowItem(uil, cl, Base, "edge_brightness");
            laShowItem(uil, cr, Base, "vertex_transparency");
            laShowItem(uil, cr, Base, "vertex_brightness");
            laShowItem(uil, col, Base, "svertex_transparency");
            laShowItem(uil, col, Base, "sedge_transparency");
            laShowItem(uil, col, Base, "sface_transparency");   
        }laElse(uil,b);{
            laShowLabel(uil,col,"Meshes",0,0); laEndRow(uil, ui);
        }laEndCondition(uil,b);  
    }

    laShowSeparator(uil,col);

    laShowLabel(uil, col, "Widgets:",0,0);
    laShowItem(uil, col, Base, "boxed_themes")->Flags|=(LA_UI_FLAGS_NO_DECAL|LA_UI_FLAGS_NO_GAP);
}
void laui_ThemePreviewFrame(laUiList *uil, laPropPack *Base, laPropPack *UnusedThis, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil); laUiItem* b,*ui;
    laUiItem* gui=laMakeEmptyGroup(uil,c,"Boxed Theme",0); laUiList* gu=gui->Page; laColumn* gc=laFirstColumn(gu);
    gui->Flags|=LA_UI_FLAGS_NO_DECAL;
    laSplitColumn(gu,gc,0.4); laColumn* gcl=laLeftColumn(gc,0),*gcr=laRightColumn(gc,0);
    b=laBeginRow(gu,gc,0,0);
    laShowItemFull(gu,gc,0,"LA_nop",0,"text=⯆;",0,0)->Flags|=LA_UI_FLAGS_NO_EVENT;
    laShowLabel(gu,gc,"Dialog",0,0)->Expand=1;
    laShowItemFull(gu,gc,0,"LA_nop",0,"text=Close;icon=🞫;",0,0)->Flags|=LA_UI_FLAGS_NO_EVENT;
    laEndRow(gu,b);
    laShowSeparator(gu,gc);
    laShowLabel(gu,gcl,"Text",0,0);
    laShowLabel(gu,gcl,"Value",0,0);
    laShowItem(gu,gcr,0,"la.example_string")->Flags|=LA_TEXT_ONE_LINE|LA_UI_FLAGS_NO_EVENT;
    laShowItem(gu,gcr,0,"la.example_int")->Flags|=LA_UI_FLAGS_NO_EVENT;
    b=laBeginRow(gu,gc,0,0);
    laShowItemFull(gu,gc,0,"LA_nop",0,"text=Cancel;",0,0)->Flags|=LA_UI_FLAGS_NO_EVENT|LA_UI_FLAGS_WARNING;
    laShowSeparator(gu,gc)->Expand=1;
    laShowItem(gu,gc,0,"LA_nop")->Flags|=LA_UI_FLAGS_NO_EVENT;
    laShowItemFull(gu,gc,0,"LA_nop",0,"text=Confirm;",0,0)->Flags|=LA_UI_FLAGS_NO_EVENT|LA_UI_FLAGS_HIGHLIGHT;
    laEndRow(gu,b);
}
void laui_ThemePreview(laUiList *uil, laPropPack *Base, laPropPack *UnusedThis, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil); laUiItem* b,*ui;
    ui=laShowItem(uil,c,Base,"name"); ui->Expand=1;
    ui->Flags|=LA_UI_FLAGS_PLAIN|LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_NO_EVENT;
    ui=laShowItemFull(uil,c,Base,"preview",0,0,laui_ThemePreviewFrame,0);
    ui->Flags|=LA_UI_FLAGS_NO_EVENT;
}
void laui_BoxedThemeItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context){
    laColumn *col, *cl, *cr, *crl,*crr;
    laUiItem *bracket;
    laUiList *tuil;

    col = laFirstColumn(uil);
    laSplitColumn(uil, col, 0.8);
    cl = laLeftColumn(col, 1);
    cr = laRightColumn(col, 0);
    laSplitColumn(uil, cr, 0.5);
    crl = laLeftColumn(cr, 0);
    crr = laRightColumn(cr, 0);

    laShowItemFull(uil, cr, Base, "name", LA_WIDGET_STRING_PLAIN, 0 ,0,0);

    bracket = laOnConditionToggle(uil, cl, 0, 0, 0, 0, 0);{
        laUiItem* b=laBeginRow(uil,crl,1,0);
        laShowItem(uil, crl, Base, "color_selection")->Flags|=LA_UI_FLAGS_EXPAND;
        laShowItemFull(uil, crl, Base, "text_shadow",0,"text=Shadow",0,0)->Flags|=LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT;
        laShowItemFull(uil, crl, Base, "no_decal_invert",0,"text=Invert",0,0)->Flags|=LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT;
        laEndRow(uil,b);
        laShowItem(uil, crr, Base, "box_style")->Flags|=LA_UI_FLAGS_EXPAND;
        laShowItem(uil, crl, Base, "normal");
        laShowItem(uil, crr, Base, "active");
        laShowItem(uil, crl, Base, "border");
        laShowItem(uil, crr, Base, "alpha");
        laShowItem(uil, crl, Base, "text");
        laShowItem(uil, crr, Base, "text_active");
    }
    laEndCondition(uil, bracket);
}

void laui_PropertyContainerList(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context){
    laColumn *col, *cl, *cr, *crl, *crr, *cll, *clr, *crrl, *crrr;
    laUiItem *bracket;
    laUiList *tuil;

    col = laFirstColumn(uil);
    laSplitColumn(uil, col, 0.5);
    cl = laLeftColumn(col, 60);
    cr = laRightColumn(col, 0);
    laSplitColumn(uil, cr, 0.25);
    crl = laLeftColumn(cr, 0);
    crr = laRightColumn(cr, 0);
    laSplitColumn(uil, crr, 0.3);
    crrl = laLeftColumn(crr, 0);
    crrr = laRightColumn(crr, 0);

    laShowLabel(uil, cl, "Select", 0, 0);
    laShowLabel(uil, crl, "Identifier", 0, 0);
    laShowLabel(uil, crrl, "Name", 0, 0);
    laShowLabel(uil, crrr, "Description", 0, 0);
    laShowColumnAdjuster(uil, col);

    laShowItem(uil, col, Base, "properties");
}
void laui_WindowListItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context){
    laColumn *col, *cl, *cr, *cll, *clr;
    laUiItem *bracket;
    laUiList *tuil;

    cl = laFirstColumn(uil);
    laSplitColumn(uil, cl, 0.2);
    cll = laLeftColumn(cl, 1);
    clr = laRightColumn(cl, 0);

    laShowItem(uil, clr, Base, "title");

    bracket = laOnConditionToggle(uil, cll, 0, 0, 0, 0, 0);{
        laShowLabel(uil, clr, "Layouts", 0, 0);
        laShowItem(uil, clr, Base, "layouts");
        laShowLabel(uil, clr, "TopPanels", 0, 0);
        laShowItem(uil, clr, Base, "panels");
    }
    laEndCondition(uil, bracket);
}
void laui_LayoutListItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context){
    laColumn *col, *cl, *cr, *cll, *clr;
    laUiItem *bracket;
    laUiList *tuil;

    cl = laFirstColumn(uil);
    laSplitColumn(uil, cl, 0.2);
    cll = laLeftColumn(cl, 1);
    clr = laRightColumn(cl, 0);

    laShowItem(uil, clr, Base, "title");

    bracket = laOnConditionToggle(uil, cll, 0, 0, 0, 0, 0);{
        laShowItem(uil, clr, Base, "panels");
    }
    laEndCondition(uil, bracket);
}
void laui_PanelListItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context){
    laColumn *col, *cl, *cr, *cll, *clr;
    laUiItem *bracket;
    laUiList *tuil;

    cl = laFirstColumn(uil);
    laSplitColumn(uil, cl, 0.2);
    cll = laLeftColumn(cl, 1);
    clr = laRightColumn(cl, 0);

    laShowItem(uil, clr, Base, "title");

    bracket = laOnConditionToggle(uil, cll, 0, 0, 0, 0, 0);{

        laShowItemFull(uil, clr, Base, "size", LA_WIDGET_INT,0, 0, 0);
    }
    laEndCondition(uil, bracket);
}
void laui_UiTemplateListItemLiveEdit(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context){
    laColumn *col, *cl, *cr, *cll, *clr;
    laUiItem *bracket;
    laUiList *tuil;

    cl = laFirstColumn(uil);

    //bracket = laOnConditionThat(uil, cl, laNot(laPropExpression(Base, "for_panel"))); {

    bracket = laOnConditionThat(uil, cl, laAnd(laNot(laPropExpression(Base, "for_panel")), laOr(laNot(laPropExpression(Base, "target")), laEqual(laPropExpression(0, "live_editor.current_ui.prop_pack.last_ps.property.sub"), laPropExpression(Base, "target")))));{
        laShowItemFull(uil, cl, Base, "identifier", LA_WIDGET_STRING_PLAIN,0, 0, 0);
    }
    laElse(uil, bracket);{
        laShowItemFull(uil, cl, Base, "identifier", LA_WIDGET_STRING_PLAIN, "disabled=true;",0, 0);
    }
    laEndCondition(uil, bracket);
}
void laui_UiTemplateListItem(laUiList *uil, laPropPack *Base, laPropPack *UNUSED_This, laColumn *UNUSED_Colums, int context){
    laColumn *col, *cl, *cr, *cll, *clr, *clll, *cllr, *clrl, *clrr, *clrrl, *clrrr;
    laUiItem *bracket, *b1;
    laUiList *tuil;

    cl = laFirstColumn(uil);
    laSplitColumn(uil, cl, 0.2);
    cll = laLeftColumn(cl, LA_2RH);
    clr = laRightColumn(cl, 0);

    laSplitColumn(uil, cll, 0.5);
    clll = laLeftColumn(cll, 0);
    cllr = laRightColumn(cll, 0);

    laSplitColumn(uil, clr, 0.5);
    clrl = laLeftColumn(clr, 0);
    clrr = laRightColumn(clr, 200);

    laSplitColumn(uil, clrr, 0.2);
    clrrl = laLeftColumn(clrr, 1);
    clrrr = laRightColumn(clrr, 0);

    bracket = laOnConditionThat(uil, cll, laPropExpression(Base, "define_func"));{
        laShowLabel(uil, clll, "f", 0, 0);
    }
    laEndCondition(uil, bracket);

    bracket = laOnConditionThat(uil, cll, laPropExpression(Base, "descriptor"));{
        laShowLabel(uil, cllr, "🠞", 0, 0);
    }
    laEndCondition(uil, bracket);

    laShowItemFull(uil, clrl, Base, "identifier", LA_WIDGET_STRING_PLAIN,0, 0, 0);

    bracket = laOnConditionThat(uil, cll, laPropExpression(Base, "target_id"));{
        laShowIcon(uil, clrrl, Base, "target.icon_id", 0);
        laShowItemFull(uil, clrrr, Base, "target_id", LA_WIDGET_STRING_PLAIN,0, 0, 0);
    }
    laElse(uil, bracket);{
        b1 = laOnConditionThat(uil, cll, laPropExpression(Base, "for_panel"));{
            //laShowLabel(uil, clrrl, 0,, 0, 0);
            laShowLabel(uil, clrrr, "PANEL", 0, 0);
        }
        laElse(uil, b1);{
            laShowLabel(uil, clrrr, "-", 0, LA_WIDGET_STRING_PLAIN);
        }
        laEndCondition(uil, b1);
    }
    laEndCondition(uil, bracket);
}

void laui_PlainSceneListItem(laUiList *uil, laPropPack *This, laPropPack *OP_UNUSED, laColumn *Extra, int context){
    laColumn *col = Extra, *c, *cl, *cr, *crl, *crr, *cll, *clr, *clrl, *clrr, *clrrl, *clrrr;
    laUiItem *ui;

    //if (Extra)c = Extra;

    c = laFirstColumn(uil);

    laShowItemFull(uil, c, This, "name", LA_WIDGET_STRING_PLAIN, 0, 0, 0);
}

void laui_DataRestorePage(laUiList *uil, laPropPack *THIS_UNUSED, laPropPack *Operator, laColumn *UNUSED, int context){
    laColumn *col = laFirstColumn(uil), *c, *cl, *cr, *crl, *crr, *cll, *clr, *clrl, *clrr, *clrrl, *clrrr;
    laUiList *u;
    laUiItem *b;

    laShowItemFull(uil, col, 0, "la", 0, 0, laui_SubPropSelection, 0);
    laShowItemFull(uil, col, 0, "tns", 0, 0, laui_SubPropSelection, 0);
}
void laui_FileBrowserFileItem(laUiList *uil, laPropPack *This, laPropPack *OP_UNUSED, laColumn *Extra, int context){
    laColumn *c = Extra, *cl, *cr, *crl, *crr, *cll, *clr, *clrl, *clrr, *clrrl, *clrrr;
    laUiItem *ui;

    if (!c) { c = laFirstColumn(uil); }

    laSplitColumn(0, c, 0.1);
    cll = laLeftColumn(c, 1);
    clr = laRightColumn(c, 0);
    laSplitColumn(0, clr, 0.7);
    clrl = laLeftColumn(clr, 0);
    clrr = laRightColumn(clr, 0);
    laSplitColumn(0, clrr, 0.5);
    clrrl = laLeftColumn(clrr, 0);
    clrrr = laRightColumn(clrr, 0);

    laShowItemFull(uil, cll, This, "type", LA_WIDGET_ENUM_ICON_PLAIN, 0, 0, 0)->Flags|=LA_TEXT_ALIGN_RIGHT;
    //laShowItem(uil, cll, This, "is_folder");
    laShowItemFull(uil, clrl, This, "name", LA_WIDGET_STRING_PLAIN, 0, 0, 0);
    ui = laOnConditionThat(uil, clrr, laNot(laPropExpression(This, "is_folder")));{
        laShowItemFull(uil, clrrl, This, "time_modified.time_string", LA_WIDGET_STRING_PLAIN, 0, 0, 0);
        laShowItemFull(uil, clrrr, This, "size_string",LA_WIDGET_STRING_PLAIN,0,0,0)->Flags|=LA_TEXT_ALIGN_RIGHT;
    }
    laEndCondition(uil, ui);
    //laShowSeparator(uil,cll)
    //laShowItemFull(uil, clrrl, This, "time_modified.time_string", LA_WIDGET_STRING_PLAIN, 0, 0);
    //laShowItem(uil, clrrr, This, "size");
}
void laui_FileBrowserDiskItem(laUiList *uil, laPropPack *This, laPropPack *OP_UNUSED, laColumn *Extra, int context){
    laColumn *col = Extra, *c, *cl, *cr, *crl, *crr;
    laUiItem *ui;

    c = Extra;

    laShowItemFull(uil, c, This, "id", LA_WIDGET_STRING_PLAIN, 0, 0, 0);
    laShowItem(uil, c, This, "capacity")->Flags |= LA_UI_FLAGS_NO_DECAL|LA_TEXT_MONO|LA_TEXT_ALIGN_RIGHT;
    laShowSeparator(uil,c);
}
void laui_FileBrowserFileList(laUiList *uil, laPropPack *THIS_UNUSED, laPropPack *Operator, laColumn *UNUSED, int context){
    laColumn *col = laFirstColumn(uil), *c, *cl, *cr, *crl, *crr, *cll, *clr, *clrl, *clrr, *clrrl, *clrrr, *ulc;
    laUiList *u; laUiItem *b; laUiList *ul;

    laSplitColumn(uil, col, 0.25);
    cl = laLeftColumn(col, 10); cr = laRightColumn(col, 0);

    laUiItem* row=laBeginRow(uil,cl,0,0);
    laShowItemFull(uil, cl, Operator, "refresh", LA_WIDGET_BUTTON_NO_CONFIRM,0,0,0);
    laShowItemFull(uil, cl, Operator, "folder_up", LA_WIDGET_BUTTON_NO_CONFIRM,0,0,0)->Expand=1;
    laEndRow(uil,row);
    laShowItem(uil, cr, Operator, "path")->Expand=1;

    row=laBeginRow(uil,cl,0,0);
    laShowSeparator(uil,cl)->Expand=1;
    laShowItemFull(uil, cl, Operator, "new_directory", LA_WIDGET_BUTTON_NO_CONFIRM,0,0,0);
    laEndRow(uil,row);

    laSplitColumn(uil,cr,0.5);
    laColumn *fnamel=laLeftColumn(cr,0),*fnamer=laRightColumn(cr,15);

    laShowItem(uil, fnamel, Operator, "file_name");
    laUiItem* r=laBeginRow(uil,fnamer,0,0);
    laShowLabel(uil,fnamer,"🔍",0,0);
    laUiItem* ui=laShowItem(uil, fnamer, Operator, "filter_name"); ui->Expand=1; ui->Flags|=LA_UI_FLAGS_IMMEDIATE_INPUT;
    laShowItem(uil, fnamer, Operator, "confirm")->Flags|=LA_UI_FLAGS_HIGHLIGHT|LA_UI_FLAGS_NO_CONFIRM;
    laEndRow(uil,r);

    laShowSeparator(uil,col);

    laUiItem* left=laMakeEmptyGroup(uil, cl, "left", 0); ul=left->Page; left->Flags|=LA_UI_FLAGS_NO_DECAL; ul->HeightCoeff=-1;
    ulc=laFirstColumn(ul);

    laFileBrowser* fb=Operator->EndInstance;
    if(fb){
        laUiItem* b=laOnConditionThat(ul,ulc,laPropExpression(Operator,"show_thumbnail"));
        laShowImage(ul,ulc,fb->Thumbnail,5);
        laEndCondition(ul,b);
    }
    
    r=laBeginRow(ul,ulc,1,0);
    laShowItemFull(ul,ulc,Operator,"show_thumbnail",0,"text=Thumbnail",0,0)->Flags|=LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT;
    laShowItemFull(ul,ulc,Operator,"show_backups",0,"text=Backups",0,0)->Flags|=LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT;
    laEndRow(ul,r);

    b=laOnConditionThat(ul,ulc,laPropExpression(Operator,"use_type"));{
        u = laMakeGroup(ul, ulc, "Use Format", 0)->Page; c = laFirstColumn(u);
        laShowItemFull(u,c,Operator,"available_extensions",0,0,laui_IdentifierOnly,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
    }laEndCondition(ul,b);

    u = laMakeFoldableGroup(ul, ulc, "Bookmarks", 0, 0, 0)->Page; c = laFirstColumn(u);
    laShowItemFull(u,c,Operator,"bookmarks",0,0,laui_IdentifierOnly,0)->Flags|=LA_UI_FLAGS_NO_DECAL;

    u = laMakeFoldableGroup(ul, ulc, "Logic Drives",0 ,0, 0)->Page; c = laFirstColumn(u);
    laShowItem(u, c, Operator, "disk_list")->Flags|=LA_UI_FLAGS_NO_DECAL;

    b = laMakeGroup(uil, cr, "File List", 0);b->State=LA_UI_ACTIVE; u=b->Page;
    u->HeightCoeff = -1;
    c = laFirstColumn(u); laSplitColumn(u, c, 0.1); cll = laLeftColumn(c, 1); clr = laRightColumn(c, 0);
    laSplitColumn(u, clr, 0.7); clrl = laLeftColumn(clr, 0); clrr = laRightColumn(clr, 0);
    laSplitColumn(u, clrr, 0.5); clrrl = laLeftColumn(clrr, 0); clrrr = laRightColumn(clrr, 0);
    laShowColumnAdjuster(u, c);
    laShowItemFull(u, clrl,Operator, "sort_name",0,"text=File Name",0,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
    laShowItemFull(u, clrrl,Operator, "sort_time",0,"text=Last Modified On",0,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
    laShowItemFull(u, clrrr,Operator, "sort_size",0,"text=Size",0,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
    laShowItem(u, c, Operator, "file_list")->Flags|=LA_UI_FLAGS_NO_DECAL;
}
void laui_LinkerPanel(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn *c, *cl, *cr, *cll, *clr, *crl, *crr, *crrl, *crrr, *gc;
    laUiList *gu;
    laUiItem *bracket, *b1, *b2;
    laPropContainer *pc;
    laProp *p;
    laPropPack *pp;
    char buf[LA_RAW_CSTR_MAX_LEN]={0};

    c = laFirstColumn(uil);
    laSplitColumn(uil, c, 0.2);
    cl = laLeftColumn(c, 100);
    cr = laRightColumn(c, 0);
    laSplitColumn(uil, cr, 0.2);
    crl = laLeftColumn(cr, 1);
    crr = laRightColumn(cr, 0);

    gu = laMakeGroup(uil, c, " UDF File Content", 0)->Page;{
        gc = laFirstColumn(gu);
        laShowItem(gu, gc, OperatorInst, "root_nodes");
    }
}

void laui_ManagedUDFItem(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn *c=laFirstColumn(uil);
    laSplitColumn(uil,c,0.5);
    laColumn *cl=laLeftColumn(c, 1);
    laColumn *cr=laRightColumn(c,0);
    laUiItem* b2=laOnConditionThat(uil,cr,laPropExpression(Base,"udf"));{
        laUiItem* r=laBeginRow(uil,c,0,0);
        laUiItem* b=laOnConditionToggle(uil,c,0,0,0,0,0);strSafeSet(&b->ExtraInstructions,"text=🛈;");b->Flags|=LA_UI_FLAGS_PLAIN;{
            laShowItemFull(uil,c,Base,"udf.path",LA_WIDGET_STRING_PLAIN, 0 ,0,0);
        }laElse(uil,b);{
            laShowItemFull(uil,c,Base,"basename",LA_WIDGET_STRING_PLAIN, 0 ,0,0);
        }laEndCondition(uil,b);
        laEndRow(uil, r);
    }laElse(uil,b2);{
        laShowItemFull(uil,c,Base,"basename",LA_WIDGET_STRING_PLAIN, 0 ,0,0);
    }laEndCondition(uil,b2);
}
void laui_ManagedUDFOps(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn *c=laFirstColumn(uil);
    laSplitColumn(uil,c,0.3);
    laColumn *cl=laLeftColumn(c, 1);
    laColumn *cr=laRightColumn(c,0);
    laUiItem* b1=laOnConditionThat(uil, cr, laPropExpression(Base, "udf"));{
        laShowItem(uil,cl,Base,"udf.modified");
        laUiItem* b=laBeginRow(uil,cr,0,0);
        laUiItem* b2=laOnConditionToggle(uil,cr,0,0,0,0,0);strSafeSet(&b2->ExtraInstructions,"text=🛈;");b2->Flags|=LA_UI_FLAGS_PLAIN;{
            laUiItem* ui=laShowItemFull(uil,cr,Base,"udf.path",LA_WIDGET_STRING_PLAIN, 0 ,0,0);ui->Expand=1;strSafeSet(&ui->ExtraInstructions,"text=Change");
            laShowItem(uil,cr,&ui->PP,"get_file_path");
        }laElse(uil,b2);{
            laShowItemFull(uil,cr,Base,"basename", LA_WIDGET_STRING_PLAIN, 0 ,0,0)->Expand=1;
        }laEndCondition(uil,b2);
        laEndRow(uil, b);
    }laElse(uil,b1);{
        laShowItemFull(uil,cr,0,"LA_managed_save_new_file",0,"feedback=NONE;",0,0);
    }laEndCondition(uil,b1);
}
void laui_ManagedPropInstance(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    if(!Base ||! Base->EndInstance ||Base->LastPs->p->PropertyType!=LA_PROP_SUB) return;
    laColumn *c=laFirstColumn(uil);
    laSplitColumn(uil,c,0.3); laColumn *cl=laLeftColumn(c, 1); laColumn *cr=laRightColumn(c,0);
    laSplitColumn(uil,cr,0.6); laColumn *crl=laLeftColumn(cr, 0); laColumn *crr=laRightColumn(cr,20);

    laPropContainer* pc=la_EnsureSubTarget(Base->LastPs->p, Base->EndInstance);

    if(pc->UDFPropagate){
        char buf[128]; sprintf(buf,"Assign all \"%s\" into:",pc->Name);
        laShowLabel(uil,crl,buf,0,0)->Flags|=LA_TEXT_ALIGN_RIGHT|LA_UI_FLAGS_DISABLED;
        laShowItem(uil,crr,Base,"__single_udf_propagate");
        laShowLabel(uil,cl," ",0,0);
    }
    
    if(pc->OtherAlloc || Base->LastPs->p->UDFIsSingle){
        //laShowLabel(uil,cr,"(Item not allocated by memAcquire)",0,0)->Expand=1;
    }else{
        if(pc->Hyper==2){
            laShowItem(uil,cl,Base,"__modified"); laSplitColumn(uil,crl,0.4); laColumn *crll=laLeftColumn(crl, 0); laColumn *crlr=laRightColumn(crl,20);
            laUiItem* idui=laShowItem(uil,crll,Base,"identifier");idui->Flags|=LA_UI_FLAGS_PLAIN;idui->Expand=1;
            laShowItem(uil,crlr,Base,"__uid")->Flags|=LA_UI_FLAGS_NO_DECAL;
            if(pc->UDFPropagate){
                laColumn *crrl,*crrr,*udfc,*optionc; laSplitColumn(uil,crr,0.7); crrl=laLeftColumn(crr,0); crrr=laRightColumn(crr,1);
                laSplitColumn(uil,crrl,0.3); udfc=laLeftColumn(crrl,0); optionc=laRightColumn(crrl,8);
                laUiItem* uc=laOnConditionToggle(uil,crrr,0,0,0,0,0);{ strSafeSet(&uc->ExtraInstructions,"text=⮯;");
                    laShowItem(uil,udfc,Base,"__file");
                    laUiItem* sr=laBeginRow(uil,optionc,1,0);
                    laShowItemFull(uil,optionc,Base,"__udf_propagate",0,"force=true;text=Force;",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_NO_CONFIRM;
                    laShowItemFull(uil,optionc,Base,"__udf_propagate",0,0,0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_NO_CONFIRM;
                    laEndRow(uil,sr);
                }laElse(uil,uc);{
                    laShowItem(uil,crrl,Base,"__file");
                }laEndCondition(uil,uc);
            }else{
                laShowItem(uil,crr,Base,"__file");
            }
        }else{
            //laShowItem(uil,crl,Base,"identifier")->Flags|=LA_UI_FLAGS_PLAIN;
        }
    }
    for(laProp*p=pc->Props.pFirst;p;p=p->Item.pNext){
        if(p->PropertyType!=LA_PROP_SUB || p->UDFIsRefer) continue;
        la_EnsureSubTarget(p,0); 
        if(p->SubProp &&p->SubProp->Hyper!=2 &&(!p->UDFIsSingle)) continue;
        if(p->UDFNoCreate || p->UDFHideInSave) continue;
        if(p==pc->SaverDummy){
            laShowItem(uil,cl,Base,"__single_saver_dummy.__modified");
            laShowItem(uil,crl,Base,"identifier")->Flags|=LA_UI_FLAGS_PLAIN;
            laShowItem(uil,crr,Base,"__single_saver_dummy.__file");
            continue;
        }
        laUiItem* b=laOnConditionToggle(uil,c,0,0,0,0,0);{ strSafePrint(&b->ExtraInstructions,"text=📦 %s;",transLate(p->Name));
            b->Flags|=LA_UI_FLAGS_NO_DECAL|LA_TEXT_ALIGN_LEFT; b->State=LA_BT_ACTIVE;
            laShowItemFull(uil,cr,Base,p->Identifier,0, 0,laui_ManagedPropInstance,0)
                ->Flags|=LA_UI_FLAGS_NO_DECAL|LA_UI_COLLECTION_NO_HIGHLIGHT|LA_UI_COLLECTION_MANAGER_FILTER;
        }laEndCondition(uil,b);
    }
    if(pc->UDFPropagate){
        laShowSeparator(uil,c);
    }
}
void laui_ManagedProp(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn *c=laFirstColumn(uil),*cl,*cr,*cll,*clr;
    laPropPack PP={0}; if(!Base ||! Base->EndInstance) return;
    laManagedSaveProp* msp=Base->EndInstance;
    laSplitColumn(uil,c,0.6); cl=laLeftColumn(c,0); cr=laRightColumn(c,20);
    laSplitColumn(uil,cl,0.7); cll=laLeftColumn(cl,0); clr=laRightColumn(cl,0);

    laShowLabel(uil,cll,SSTR(msp->Path),0,0)->Flags|=LA_UI_FLAGS_DISABLED;
    
    laShowLabel(uil,clr,"UID",0,0)->Flags|=LA_UI_FLAGS_DISABLED|LA_TEXT_ALIGN_CENTER;
    laShowLabel(uil,cr,"File",0,0)->Flags|=LA_UI_FLAGS_DISABLED|LA_TEXT_ALIGN_CENTER;
    laShowItemFull(uil,c,0,SSTR(msp->Path),0, 0,laui_ManagedPropInstance,0)
        ->Flags|=LA_UI_FLAGS_NO_DECAL|LA_UI_COLLECTION_NO_HIGHLIGHT|LA_UI_COLLECTION_MANAGER_FILTER;

    laShowSeparator(uil,c);
}
void laui_ManagedSavePanel(laUiList *uil, laPropPack *Base, laPropPack *Operator, laColumn *ExtraColumns, int context){
    laManagedSaveExtra*mse=Operator->EndInstance;
    laColumn *c=laFirstColumn(uil);

    if(mse->OnExit){
        laShowLabel(uil,c,"You still have unsaved/unassigned datablocks:",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_TEXT_LINE_WRAP;
        laShowSeparator(uil,c);
    }

    laUiItem* r=laBeginRow(uil,c,0,0);
    laShowLabel(uil,c,"Viewing",0,0);
    laUiItem* btn=laShowItem(uil,c,Operator,"show_page");btn->Flags|=LA_UI_FLAGS_EXPAND;
    laUiItem* b=laOnConditionThat(uil,c,laNot(laPropExpression(&btn->PP,0)));{
        laShowSeparator(uil,c);
        laShowItem(uil,c,0,"la.user_preferences.manager_filter_instances")->Flags|=LA_UI_FLAGS_EXPAND;
    }laEndCondition(uil,b);
    laEndRow(uil,r);
    laUiItem* g=laMakeEmptyGroup(uil,c,"List",0);{ g->State=LA_UI_ACTIVE;
        laUiList* gu=g->Page; gu->HeightCoeff=mse->OnExit?-4:-3; laColumn* gc=laFirstColumn(gu);
        laUiItem* b=laOnConditionThat(gu,gc,laEqual(laPropExpression(Operator, "show_page"),laIntExpression(0)));{
            laShowItemFull(gu,gc,0,"la.managed_props",0, 0,laui_ManagedProp,0)->Flags|=LA_UI_FLAGS_NO_DECAL|LA_UI_COLLECTION_NO_HIGHLIGHT;
        }laElse(gu,b);{
            laShowItemFull(gu,gc,0,"la.managed_udfs",0,0,0,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
        }laEndCondition(gu,b);     
    }
    
    if(mse->OnExit){
        laShowLabel(uil,c,"If you exit the program now, you will lose those changes.",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_TEXT_LINE_WRAP;
    }

    r=laBeginRow(uil,c,0,0);
    if(mse->OnExit){
        laShowItemFull(uil,c,0,"LA_cancel",0,"text=Discard And Quit;feedback=DISCARD_AND_QUIT",0,0)->Flags|=LA_UI_FLAGS_WARNING;
    }
    laShowSeparator(uil,c)->Expand=1;
    laShowItemFull(uil,c,0,"LA_managed_save",0,"quiet=true;ignore_unassigned=true;text=Save All;feedback=SAVE_MODIFIED",0,0);
    laShowItemFull(uil,c,0,"LA_managed_save",0,"quiet=true;ignore_unassigned=true;modified_only=true;text=Save Modified;feedback=SAVE_MODIFIED",0,0)->Flags|=LA_UI_FLAGS_HIGHLIGHT;
    laEndRow(uil,r);
}

void laui_ResourceFolderItem(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn *c;

    c = laFirstColumn(uil);
    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowItem(uil, c, Base, "remove")->Flags|=LA_UI_FLAGS_ICON;
    laUiItem* ui=laShowItem(uil, c, Base, "path");
    ui->Expand=1;
    laShowItem(uil,c,&ui->PP,"get_folder_path")->Flags|=LA_UI_FLAGS_ICON;
    laEndRow(uil,b);
}

void laui_LogItem(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    if(!ExtraColumns){ ExtraColumns=laFirstColumn(uil); }
    laShowItem(uil, ExtraColumns, Base, "content")->Flags|=LA_TEXT_LINE_WRAP|LA_TEXT_USE_NEWLINE|LA_TEXT_MONO|LA_UI_FLAGS_NO_TOOLTIP;
}

void laui_NodeRack(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laNodeRack* r=This->EndInstance;
    laColumn* c=laFirstColumn(uil),*cl, *cr;
    laShowLabel(uil,c,"\n",0,0);
    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowItemFull(uil,c,This,"move",0,"icon=🡰;direction=left;",0,0)->Flags|=LA_UI_FLAGS_ICON;
    laUiItem* u=laShowItem(uil,c,This,"name"); u->Flags|=LA_UI_FLAGS_NO_DECAL|LA_TEXT_ALIGN_CENTER; u->Expand=1;
    laShowItemFull(uil,c,This,"move",0,"icon=🡲;direction=right;",0,0)->Flags|=LA_UI_FLAGS_ICON;
    laEndRow(uil,b);
    laShowLabel(uil,c,"\n",0,0);
    laShowItem(uil,c,This,"nodes")->Flags|=LA_UI_FLAGS_NO_DECAL;
    laShowLabel(uil,c,"\n",0,0);
    b=laBeginRow(uil,c,0,0);
    char args[64]=""; if(context) sprintf(args,"target=%d;",context);
    laShowItemFull(uil,c,This,"add_node",0,args,0,0)->Expand=1;
    laShowItem(uil,c,This,"delete")->Flags|=LA_UI_FLAGS_ICON;
    laShowItem(uil,c,This,"insert_rack");
    laEndRow(uil,b);
    laShowLabel(uil,c,"\n\n\n",0,0);
}
void laui_RackPage(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil); laRackPage* r=This->EndInstance;
    laUiItem* b=laOnConditionThat(uil,c,laPropExpression(This,"has_rack"));{
        laUiItem* g=laMakeGroup(uil,c,"Racks",0);{ g->Flags|=/*LA_UI_FLAGS_NO_GAP|LA_UI_FLAGS_NO_DECAL|*/LA_UI_FLAGS_NODE_CONTAINER|LA_UI_FLAGS_NO_GAP;
            laUiList* gu=g->Page; laColumn* gc=laFirstColumn(gu); gu->AllowScale=1; g->State=LA_UI_ACTIVE; gu->HeightCoeff=-1; 
            laUiItem* hui=laShowItemFull(gu,gc,This,"racks",0,0,laui_NodeRack,r?r->RackType:0); hui->Expand=15; hui->Flags|=LA_UI_FLAGS_NO_DECAL;
        }
    }laElse(uil,b);{
        laShowItemFull(uil,c,This,"add_rack",0,0,0,0);
    }laEndCondition(uil,b);
}
void laui_NodeCategory(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil); laNodeCategory* nc=This?This->EndInstance:0; if(!nc){ return; }
    if(nc->Ui){ nc->Ui(uil,This,Extra,0,context); return; }
    char buf[512]={0};
    for(laListItemPointer* lip=nc->NodeTypes.pFirst;lip;lip=lip->pNext){
        laBaseNodeType* nt=lip->p; logPrintNew("%d",nt);
        sprintf(buf,"feedback=%s;text=%s;icon=%lc;",nt->TypeName?nt->TypeName:" ",nt->Name?nt->Name:" ",nt->Icon?nt->Icon:L' ');
        laShowItemFull(uil,c,0,"LA_confirm",0,buf,0,0);
    }
}

void laui_Screen(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil); laSplitColumn(uil,c,0.25); laColumn*cl,*cr; cl=laLeftColumn(c,1); cr=laRightColumn(c,0);

    laUiItem* b=laBeginRow(uil,cr,0,0);
    laShowItem(uil,cr,This,"color_space");
    laShowItem(uil,cr,This,"name")->Expand=1;
    laShowItem(uil,cr,This,"remove")->Flags|=LA_UI_FLAGS_ICON;
    laEndRow(uil,b);
    b=laOnConditionToggle(uil,cl,0,0,0,0,0);
    laShowItem(uil,cr,This,"description");
    laEndCondition(uil,b);
}

void laui_UserPreference(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn* c = laFirstColumn(uil),*cl,*cr; laSplitColumn(uil,c,0.5);cl=laLeftColumn(c,0);cr=laRightColumn(c,0);
    laUiList *muil;
    laColumn *mc, *mcl, *mcr, *mcll, *mclr;
    laUiItem *b, *bracket;

    b=laBeginRow(uil,cl,0,0); 
    muil = laMakeMenuPage(uil, cr, "☰ Menu"); mc = laFirstColumn(muil);
    laShowItem(muil, mc, 0, "LA_restore_factory");
    laUiItem* ui=laShowLabel(uil,cl,"Save on exit:",0,0); ui->Expand=1; ui->Flags|=LA_TEXT_ALIGN_RIGHT;
    laShowItemFull(uil,cl,0,"la.user_preferences.save_preferences_on_exit",LA_WIDGET_ENUM_HIGHLIGHT,0,0,0)->Flags|=LA_UI_FLAGS_ICON;
    laEndRow(uil,b);
    laShowItem(uil, cr, 0, "LA_save_user_preferences")->Expand = 1;


    laUiItem* UP = laShowInvisibleItem(uil,c,0,"la.user_preferences");
    
    bracket = laMakeTab(uil, c, 0);{

        for(laExtraPreferencePage* epp=MAIN.ExtraPreferencePages.pFirst;epp;epp=epp->Item.pNext){
            muil = laAddTabPage(bracket, epp->Name); epp->Func(muil,0,0,0,0);
        }

        
        muil = laAddTabPage(bracket, "Display");{
            //muil->HeightCoeff = -1;
            mc = laFirstColumn(muil);
            laSplitColumn(muil, mc, 0.5);
            mcl = laLeftColumn(mc, 0);
            mcr = laRightColumn(mc, 0);

            if(MAIN.PreferencePageDisplay){
                MAIN.PreferencePageDisplay(muil,0,0,0,0); 
                laShowSeparator(muil, mc);
            }

            laShowLabel(muil, mc, "Translation:", 0, 0);
            laShowLabel(muil, mcl, "Enable Translation:", 0, 0);
            laShowItemFull(muil, mcr, &UP->PP, "enable_translation",LA_WIDGET_ENUM_CYCLE,0,0,0);
            b = laOnConditionThat(muil, mcl, laPropExpression(&UP->PP, "enable_translation"));{
            laShowLabel(muil, mcl, "Language:", 0, 0);
                laShowItemFull(muil, mcr, &UP->PP, "languages", LA_WIDGET_COLLECTION_SELECTOR, 0, 0, 0);
                laShowItem(muil, mcl, 0, "LA_translation_dump");
            }
            laEndCondition(muil, b);

            laShowSeparator(muil, mc);

            laShowLabel(muil, mc, "Interface:", 0, 0);
            laShowLabel(muil, mcl, "Row Height:", 0, 0);
            laShowItem(muil, mcr, &UP->PP, "interface_size");
            laShowLabel(muil, mcl, "Font Size:", 0, 0);
            laShowItem(muil, mcr, &UP->PP, "font_size");
            laShowLabel(muil, mcl, "Margin Size:", 0, 0);
            laShowItem(muil, mcr, &UP->PP, "margin_size");
#ifndef LA_USE_GLES
            laShowLabel(muil, mcl, "Panel Multisample:", 0, 0);
            laShowItem(muil, mcr, &UP->PP, "panel_multisample")->Flags|=LA_UI_FLAGS_EXPAND;
#endif
            laShowLabel(muil, mcl, "Top Framerate:", 0, 0);
            laShowItem(muil, mc, &UP->PP, "top_framerate");

            laShowSeparator(muil, mc);

            laShowLabel(muil, mc, "Color:", 0, 0);
            laShowItem(muil, mcl, &UP->PP, "color_picker_gamma");
            laShowItem(muil, mcr, &UP->PP, "enable_color_management");

#ifndef LAGUI_ANDROID
            laUiItem* bb=laOnConditionThat(muil,mc,laPropExpression(&UP->PP,"enable_color_management"));{
                laUiItem*bbr=laBeginRow(muil,mc,0,0);
                laShowLabel(muil, mcl, "Per screen config:", 0, 0)->Expand=1;
                laShowItem(muil,mc,0,"LA_refresh_screens")->Flags|=LA_UI_FLAGS_ICON;
                laShowItemFull(muil, mcr, &UP->PP, "auto_switch_color_space",0,"text=Auto Switch",0,0);
                laEndRow(muil,bbr);
                laShowItemFull(muil, mc, &UP->PP, "screens",0,0,laui_Screen,0);
            }laEndCondition(muil,bb);
#endif

            laShowSeparator(muil, mc);

            if(MAIN.InitArgs.HasWorldObjects){
                laShowLabel(muil, mc, "Viewport:", 0, 0);
                laShowItem(muil, mcl, &UP->PP, "viewport_halftone_factor");
                laShowItem(muil, mcr, &UP->PP, "viewport_halftone_size");
                laShowSeparator(muil, mc);
            }

            laShowLabel(muil, mc, "Nodes:", 0, 0);
            laShowItem(muil, mcr, &UP->PP, "wire_color_slices");
            laShowItem(muil, mcr, &UP->PP, "wire_thickness");
            laShowItem(muil, mcr, &UP->PP, "wire_saggyness");
        }
        
        muil = laAddTabPage(bracket, "Input");{
            //muil->HeightCoeff = -1;
            mc = laFirstColumn(muil);
            laSplitColumn(muil, mc, 0.5);
            mcl = laLeftColumn(mc, 0);
            mcr = laRightColumn(mc, 0);

            if(MAIN.PreferencePageInput){ MAIN.PreferencePageInput(muil,0,0,0,0); }

            laShowLabel(muil, mc, "User Interactions:", 0, 0);
            laShowItem(muil, mc, &UP->PP, "scroll_speed");
            laShowItem(muil, mcl, &UP->PP, "animation_speed");
            laShowItem(muil, mcr, &UP->PP, "panel_animation_speed");
            laShowItem(muil, mc, &UP->PP, "valuator_threshold");
            laShowItem(muil, mc, &UP->PP, "zoom_speed_2d");
            laShowItem(muil, mcl, &UP->PP, "tooltip_close_distance");
            laShowItem(muil, mcr, &UP->PP, "idle_time");

            laShowSeparator(muil,mc);

            laShowLabel(muil, mc, "Input device handling:", 0, 0);
            laShowLabel(muil, mcl, "Input Mapping:", 0, 0)->Flags|=LA_TEXT_ALIGN_RIGHT;
            laShowItemFull(muil, mcr, 0, "LA_panel_activator",0,"panel_id=LAUI_input_mapping;text=Configure",0,0);
            laShowLabel(muil, mcl, "Pads/Joysticks:", 0, 0)->Flags|=LA_TEXT_ALIGN_RIGHT;
            laShowItemFull(muil, mcr, 0, "LA_panel_activator",0,"panel_id=LAUI_controllers;text=Configure",0,0);

#ifndef LAGUI_ANDROID
            laShowSeparator(muil,mc);
            laShowLabel(muil, mc, "Wacom Devices:", 0, 0);
#ifdef _WIN32
            b=laBeginRow(muil,mcl,0,0); laShowSeparator(muil,mcl)->Expand=1;
            laShowLabel(muil, mcl, "Driver:", 0, 0)->Flags|=LA_TEXT_ALIGN_RIGHT;
            laShowItem(muil, mcl, 0, "LA_refresh_controllers")->Flags|=LA_UI_FLAGS_ICON;
            laEndRow(muil, b);
            laShowItem(muil, mcr, &UP->PP, "wacom_driver");
#else
            laShowItem(muil, mcl, &UP->PP, "wacom_device_stylus");
            laShowItem(muil, mcl, &UP->PP, "wacom_device_eraser");
            laShowItemFull(muil, mcr, 0, "LA_refresh_controllers",0,"text=Refresh",0,0);
#endif //!win32
#endif //!android
        }
        if(MAIN.InitArgs.HasAudio){
            muil = laAddTabPage(bracket, "Audio");{
                mc = laFirstColumn(muil); laSplitColumn(muil, mc, 0.5);
                mcl = laLeftColumn(mc, 0); mcr = laRightColumn(mc, 0);
                b=laBeginRow(muil,mcl,0,0);
                laShowLabel(muil,mcl,"Backend:",0,0);
                laShowItem(muil,mcl,0,"la.audio.backend")->Expand=1;
                laEndRow(muil,b);
                laShowLabel(muil,mc,"Device:",0,0);
                laShowItemFull(muil,mcl,0,"la.audio.current_audio_device",LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0);
                laShowItem(muil,mcr,0,"LA_refresh_audio_devices");
            }
        }

        muil = laAddTabPage(bracket, "Resource");{
            //muil->HeightCoeff = -1;
            mc = laFirstColumn(muil);

            laSplitColumn(muil, mc, 0.5);
            mcl = laLeftColumn(mc, 0);
            mcr = laRightColumn(mc, 0);

            if(MAIN.PreferencePageResource){ MAIN.PreferencePageResource(muil,0,0,0,0); }

            laShowLabel(muil, mcl, "UDF Manager Default View:", 0, 0);
            laShowItem(muil, mcr, &UP->PP, "manager_default_view")->Flags|=LA_UI_FLAGS_EXPAND;

            laShowSeparator(muil,mc);

            b=laBeginRow(muil,mc,0,0);
            laShowLabel(muil, mc, "Resource Folders", 0, 0)->Expand=1;
            laShowItem(muil, mc, 0, "LA_add_resource_folder");
            laEndRow(muil,b);
            laShowItem(muil, mc, &UP->PP, "resource_folders")->Flags|=LA_UI_FLAGS_NO_DECAL;
        }

        muil = laAddTabPage(bracket, "Theme");{
            //muil->HeightCoeff = -1;
            mc = laFirstColumn(muil);
            //laSplitColumn(muil,mc,0.4); mcl=laLeftColumn(mc,7);mcr=laRightColumn(mc,0);

            if(MAIN.PreferencePageTheme){ MAIN.PreferencePageTheme(muil,0,0,0,0); laShowSeparator(muil,mc); }

            laUiItem* gui=laMakeGroup(muil,mc,"Installed Themes",0); laUiList* gu=gui->Page; laColumn* gc=laFirstColumn(gu);
            gu->HeightCoeff=10;
            laUiItem* ui=laShowItemFull(gu, gc, 0, "la.themes", 0, 0,laui_ThemePreview,0);
            ui->Expand=10; ui->Flags|=LA_UI_FLAGS_NO_DECAL;

            laUiItem* b=laBeginRow(muil,mc,0,0);
            laShowSeparator(muil,mc)->Expand=1;
            laShowItem(muil,mc,0,"LA_new_theme");
            laEndRow(muil,b);

            //laShowItemFull(muil, mc, 0, "la.themes", LA_WIDGET_COLLECTION_SELECTOR, 0,laui_ThemeListItem,0);
            //laShowSeparator(muil,mc);
            laShowItemFull(muil, mc, 0, "la.themes", LA_WIDGET_COLLECTION_SINGLE, 0 ,0,0);//->Flags|=LA_UI_FLAGS_NO_DECAL;
        }

        muil = laAddTabPage(bracket, "System");{
            mc = laFirstColumn(muil); laSplitColumn(muil, mc, 0.5);
            mcl = laLeftColumn(mc, 0); mcr = laRightColumn(mc, 0);
            laShowLabel(muil,mcl,"OpenGL Debugging:",0,0);
            if(MAIN.InitArgs.GLDebug){
                laShowLabel(muil,mc,"OpenGL debugging is force enabled with `--gl-debug`",0,0)->Flags|=LA_UI_FLAGS_DISABLED;
            }else{
                laShowItem(muil,mcr,0,"la.user_preferences.enable_gl_debug");
                laUiItem* b=laOnConditionThat(muil,mc,laPropExpression(0,"la.user_preferences.enable_gl_debug"));{
                    laShowItem(muil,mcl,0,"la.user_preferences.gl_debug_sync");
                    laShowItem(muil,mcr,0,"la.user_preferences.gl_debug_level")->Flags|=LA_UI_FLAGS_EXPAND;
                }laEndCondition(muil,b);
            }
            laShowSeparator(muil,mc);
            laShowLabel(muil,mcl,"Logs:",0,0);
            laShowItem(muil,mcr,0,"la.user_preferences.enable_log_stdout");
            laShowItem(muil,mcr,0,"la.user_preferences.enable_performance_overlay");
        }
    }
}
void laui_About(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn *c, *cl, *cr, *tc, *gc, *gcl, *gcr, *tcl, *tcr;
    laUiItem *bracket, *t, *g;
    laPanel *p;
    laUiList *u, *gu;
    laPropContainer *pc;
    int h;
    laOperatorType *at;
    laUiList *first;
    char buf[2048]={0};

    c = laFirstColumn(uil);

    if(MAIN.AboutContent){
        MAIN.AboutContent(uil,Base,OperatorInst,ExtraColumns,0);
    }else{
        laShowLabel(uil, c, "LaGUI", 0, 0);
        laShowLabel(uil, c, "A graphical user interface application toolkit", 0, 0)->Flags|=LA_TEXT_LINE_WRAP;
        laShowLabel(uil, c, "(C)Yiming Wu", 0, 0);
    }

    t = laMakeTab(uil, c, 0);{
        first = u = laAddTabPage(t, "Version");
        tc = laFirstColumn(u);

        if(MAIN.AboutVersion){
            MAIN.AboutVersion(u,Base,OperatorInst,ExtraColumns,0);
        }

        g = laMakeGroup(u, tc, "LaGUI information", 0);
        gu = g->Page;{
            gc = laFirstColumn(gu);
            sprintf(buf, "LaGUI.%d.%d", LA_VERSION_MAIN, LA_VERSION_SUB);
            laShowLabel(gu, gc, buf, 0, 0)->Flags|=LA_TEXT_MONO;
            laShowLabel(gu, gc, LAGUI_GIT_BRANCH,0,0)->Flags|=LA_TEXT_MONO;
#ifdef LAGUI_GIT_HASH
            laShowLabel(gu, gc, LAGUI_GIT_HASH,0,0)->Flags|=LA_TEXT_MONO;
#endif
            sprintf(buf, "Built on:[UTC+8] %s %s", __DATE__, __TIME__);
            laShowLabel(gu, gc, buf, 0, 0)->Flags|=LA_TEXT_MONO;
            sprintf(buf, "UDF Identifier: %s",LA_UDF_IDENTIFIER);
            laShowLabel(gu, gc, buf, 0, 0)->Flags|=LA_TEXT_MONO;
            laShowLabel(gu, gc, "UDF Extensions:", 0, 0)->Flags|=LA_TEXT_MONO;
            laShowLabel(gu, gc, "🗸LA_UDF_BASICS", 0, 0)->Flags|=LA_TEXT_MONO;
            for(int i=0;i<64;i++){
                if(!LA_UDF_EXTENSION_STRINGS[i][0]) break; int gray=!((1<<i)&LA_UDF_EXTENSIONS_ENABLED);
                sprintf(buf,"%s%s",gray?"  ":"🗸",LA_UDF_EXTENSION_STRINGS[i]);
                laUiItem* ext=laShowLabel(gu, gc, buf, 0, 0); ext->Flags|=LA_TEXT_MONO;
                if(gray){ ext->Flags|=LA_UI_FLAGS_DISABLED; }
            }

        }
        
        g = laMakeGroup(u, tc, "Device Graphics", 0);
        gu = g->Page;{
            gc = laFirstColumn(gu);
            sprintf(buf, "OpenGL Version: %s", T->GLVersionStr);   laShowLabel(gu, gc, buf, 0, 0)->Flags|=LA_TEXT_MONO;
            sprintf(buf, "OpenGL Vendor: %s", T->GLVendorStr);     laShowLabel(gu, gc, buf, 0, 0)->Flags|=LA_TEXT_MONO;
            sprintf(buf, "OpenGL Renderer: %s", T->GLRendererStr); laShowLabel(gu, gc, buf, 0, 0)->Flags|=LA_TEXT_MONO;
            sprintf(buf, "GLSL Version: %s", T->GLSLVersionStr);   laShowLabel(gu, gc, buf, 0, 0)->Flags|=LA_TEXT_MONO;
        }

        u = laAddTabPage(t, "Authors");
        tc= laFirstColumn(u);

        if(MAIN.AboutAuthor){
            MAIN.AboutAuthor(u,Base,OperatorInst,ExtraColumns,0);
        }

        g = laMakeGroup(u, tc, "LaGUI", 0);
        gu = g->Page;{
            gc = laFirstColumn(gu);
            laShowLabel(gu,gc,"LaGUI application framework is made by Wu Yiming.",0,0)->Flags|=LA_TEXT_LINE_WRAP;
            laUiItem* b=laBeginRow(gu,gc,0,0);
            laShowItemFull(gu, gc, 0, "LA_open_internet_link", 0, "link=http://www.ChengduLittleA.com/lagui;text=Details", 0, 0);
            laShowItemFull(gu, gc, 0, "LA_open_internet_link", 0, "link=http://www.ChengduLittleA.com;text=Yiming's Blog", 0, 0);
            laEndRow(gu,b);
        }

        t->Page = first;
    }
}
void laui_UndoHistories(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn* c=laFirstColumn(uil),*cl,*cr; laSplitColumn(uil,c,0.5);cl=laLeftColumn(c,0);cr=laRightColumn(c,0);
    laShowItem(uil,cl,0,"LA_undo"); laShowItem(uil,cr,0,"LA_redo")->Flags|=LA_TEXT_ALIGN_RIGHT;
    laUiItem* g=laMakeGroup(uil,c,"123",0);{ g->State=LA_UI_ACTIVE; g->Flags|=LA_UI_FLAGS_PREFER_BOTTOM;
        laUiList* gu=g->Page; laColumn* gc=laFirstColumn(gu); gu->HeightCoeff=-1;
        laShowItem(gu,gc,0,"la.differences")->Flags|=LA_UI_FLAGS_NO_DECAL;
    }
}
void laui_Terminal(laUiList *uil, laPropPack *Base, laPropPack *OperatorInst, laColumn *ExtraColumns, int context){
    laColumn* c=laFirstColumn(uil),*cl,*cr; laSplitColumn(uil,c,0.4); cl=laLeftColumn(c,2); cr=laRightColumn(c,0);
    laUiItem* g=laMakeGroup(uil,c,"123",0);{ g->State=LA_UI_ACTIVE; g->Flags|=LA_UI_FLAGS_PREFER_BOTTOM;
        laUiList* gu=g->Page; laColumn* gc=laFirstColumn(gu); gu->HeightCoeff=-3;
        laShowItem(gu,gc,0,"la.logs")->Flags|=LA_UI_FLAGS_NO_DECAL;
    }
    laUiItem* b=laOnConditionThat(uil,c,laPropExpression(0,"la.terminal_incomplete"));{
        laShowLabel(uil,cl,"...",0,0)->Flags|=LA_TEXT_MONO|LA_TEXT_ALIGN_RIGHT;
    }laElse(uil,b);{
        laShowLabel(uil,cl,">>>",0,0)->Flags|=LA_TEXT_MONO|LA_TEXT_ALIGN_RIGHT;
    }laEndCondition(uil,b);
    laShowItem(uil,cr,0,"la.terminal_input")->Flags|=LA_UI_FLAGS_TERMINAL_INPUT|LA_TEXT_MONO;
}
void laui_IdleDataManager(laUiList *uil, laPropPack *Base, laPropPack *Extra, laColumn *ExtraColumns, int context){
    laColumn *c=laFirstColumn(uil);

    laUiItem* r=laBeginRow(uil,c,0,0);
    laShowLabel(uil,c,"Viewing",0,0);
    laUiItem* btn=laShowItem(uil,c,Extra,"show_page");btn->Flags|=LA_UI_FLAGS_EXPAND;
    laUiItem* b=laOnConditionThat(uil,c,laNot(laPropExpression(&btn->PP,0)));{
        laShowSeparator(uil,c);
        laShowItem(uil,c,0,"la.user_preferences.manager_filter_instances")->Flags|=LA_UI_FLAGS_EXPAND;
    }laEndCondition(uil,b);
    laEndRow(uil,r);
    laUiItem* g=laMakeEmptyGroup(uil,c,"List",0);{ g->State=LA_UI_ACTIVE;
        laUiList* gu=g->Page; gu->HeightCoeff=-3; laColumn* gc=laFirstColumn(gu);
        laUiItem* b=laOnConditionThat(gu,gc,laEqual(laPropExpression(Extra, "show_page"),laIntExpression(0)));{
            laShowItemFull(gu,gc,0,"la.managed_props",0, 0,laui_ManagedProp,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
        }laElse(gu,b);{
            laShowItemFull(gu,gc,0,"la.managed_udfs",0,0,0,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
        }laEndCondition(gu,b);     
    }
    
    r=laBeginRow(uil,c,0,0);
    laShowSeparator(uil,c)->Expand=1;
    laShowItemFull(uil,c,0,"LA_managed_save",0,"quiet=true;ignore_unassigned=true;text=Save All",0,0);
    laShowItemFull(uil,c,0,"LA_managed_save",0,"quiet=true;ignore_unassigned=true;modified_only=true;text=Save Modified",0,0)->Flags|=LA_UI_FLAGS_HIGHLIGHT;
    laEndRow(uil,r);
}
void lauidetached_IdleDataManager(laPanel* p){
    la_MakeDetachedProp(p, "la.user_preferences.manager_default_view", "show_page");
}
void laui_TextureInspector(laUiList *uil, laPropPack *Base, laPropPack *Extra, laColumn *ExtraColumns, int context){
    laColumn *c=laFirstColumn(uil),*cl,*cr;
    laSplitColumn(0, c, 0.5);
    cl = laLeftColumn(c, 0);
    cr = laRightColumn(c, 0);

    laShowLabel(uil,cl,"Viewing Texture:",0,0)->Flags|=LA_TEXT_ALIGN_RIGHT;
    laShowItemFull(uil,cr,Extra,"textures",LA_WIDGET_COLLECTION_SELECTOR,0,0,0);
    laUiItem* u2=laShowCanvas(uil,c,Extra,"textures",0,-1);
    laDefault2DViewOverlayRight(u2);
}
void lauidetached_TextureInspector(laPanel* p){
    la_MakeDetachedProp(p, "tns.texture_list", "textures");
}
void laui_GameController(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil),*cl, *cr;
    laSplitColumn(uil,c,0.5); cl=laLeftColumn(c,0); cr=laRightColumn(c,0);

    laShowInvisibleItem(uil,c,0,"la.controllers");
    
    laUiItem* b=laBeginRow(uil,cl,0,0);
    laUiItem* l=laShowLabel(uil,cl,"Controller:",0,0); l->Flags|=LA_TEXT_ALIGN_RIGHT; l->Expand=1;
    laShowItem(uil,cl,0,"LA_refresh_controllers")->Flags|=LA_UI_FLAGS_ICON;
    laEndRow(uil,b);

    laShowItemFull(uil,cr,Extra,"controllers",LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0);

    b=laOnConditionThat(uil,c,laPropExpression(Extra,"controllers"));{
        laShowItemFull(uil,c,Extra,"controllers",LA_WIDGET_COLLECTION_SINGLE,0,0,0);
    }laElse(uil,b);{
        laShowLabel(uil,c,"Please select a controller.",0,0)->Flags|=LA_TEXT_ALIGN_CENTER;
    }
}
void lauidetached_GameController(laPanel* p){
    la_MakeDetachedProp(p, "la.controllers", "controllers");
}

void laui_OperatorTypeEntry(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *ExtraColumns, int context){
    laColumn* c=ExtraColumns;
    laShowItem(uil,c,This,"name")->Flags|=LA_UI_FLAGS_PLAIN;
    laShowItem(uil,c,This,"identifier")->Flags|=LA_TEXT_MONO|LA_UI_FLAGS_DISABLED|LA_UI_FLAGS_PLAIN;
    laShowSeparator(uil,c);
}
void laui_InputMappingEntry(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *ExtraColumns, int context){
    laColumn* c=ExtraColumns;
    laColumn* cl=laLeftColumn(c,3),*cr=laRightColumn(c,0);
    laColumn* crl=laLeftColumn(cr,0),*crr=laRightColumn(cr,0);

    laUiItem* b,*b1,*b2,*b3,*b4;

#if 1
    b=laBeginRow(uil,cl,0,0);
    laShowItem(uil,cl,This,"__move");
    laShowItem(uil,crl,This,"disabled")->Flags=LA_UI_FLAGS_ICON|LA_UI_FLAGS_HIGHLIGHT|LA_UI_FLAGS_CYCLE;
    b1=laOnConditionToggle(uil,cl,0,0,0,0,0);{
        laEndRow(uil,b);
        laShowLabel(uil,cl,"Mode\n ",0,0)->Flags|=LA_TEXT_ALIGN_RIGHT|LA_UI_FLAGS_DISABLED|LA_TEXT_LINE_WRAP;

        b2=laBeginRow(uil,crl,0,0);{
            laShowItem(uil,crl,This,"use_operator")->Flags=LA_UI_FLAGS_ICON|LA_UI_FLAGS_PLAIN|LA_UI_FLAGS_DISABLED;
            b3=laOnConditionThat(uil,crl,laPropExpression(This,"use_operator"));{
                laUiItem* bui=laShowItemFull(uil,crl,This,"select_operator",0,"icon= ;",0,0);
                bui->Expand=1; bui->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_UNDERNEATH;
                laUiItem* ui=laShowItem(uil,crl,This,"operator");
                ui->Expand=1; ui->Flags|=LA_UI_FLAGS_PLAIN;
            }laElse(uil,b3);{
                laUiItem* bui=laShowItemFull(uil,crl,This,"select_signal",0,"icon= ;",0,0);
                bui->Expand=1; bui->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_UNDERNEATH;
                laUiItem* ui=laShowItem(uil,crl,This,"signal");
                ui->Expand=1; ui->Flags|=LA_UI_FLAGS_PLAIN;
            }laEndCondition(uil,b3);
        }laEndRow(uil,b2);
        
        b2=laBeginRow(uil,crr,0,0);{
            laUiItem* bui=laShowItemFull(uil,crr,This,"select_key",0,"icon= ;",0,0);
            bui->Expand=1; bui->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_UNDERNEATH;
            laUiItem* ui=laShowItem(uil,crr,This,"key");
            ui->Expand=1; ui->Flags|=LA_UI_FLAGS_PLAIN;
            laShowItem(uil,crr,This,"remove")->Flags|=LA_UI_FLAGS_ICON;
        }laEndRow(uil,b2);

        b2=laBeginRow(uil,crr,0,0);{
            laShowItem(uil,crr,This,"device_type")->Flags|=LA_UI_FLAGS_EXPAND;
        }laEndRow(uil,b2);
        b2=laBeginRow(uil,crr,0,0);{
            laShowSeparator(uil,crr)->Expand=1;
            laShowItem(uil,crr,This,"reset");
        }laEndRow(uil,b2);
        b2=laBeginRow(uil,crl,0,0);{
            b3=laBeginRow(uil,crl,0,0);{
                laUiItem* ui=laShowItem(uil,crl,This,"use_operator"); ui->Flags|=LA_UI_FLAGS_EXPAND|LA_UI_FLAGS_TRANSPOSE;
                b4=laOnConditionThat(uil,crr,laPropExpression(&ui->PP,""));{
                    laUiItem* ui1=laShowItem(uil,crl,This,"operator_base");
                    ui1->Flags|=LA_UI_FLAGS_EXPAND;ui1->Expand=10;
                    laEndRow(uil,b3);
                    laShowItem(uil,crl,This,"operator_arguments");
                    laShowLabel(uil,cl,"Args",0,0)->Flags|=LA_TEXT_ALIGN_RIGHT|LA_UI_FLAGS_DISABLED;
                }laElse(uil,b4);{
                    laEndRow(uil,b3);
                }laEndCondition(uil,b4);
            }
            laEndRow(uil,b3);
        }laEndRow(uil,b2);
        laShowSeparator(uil,c);
    }laElse(uil,b1);{
        laEndRow(uil,b);
        b2=laBeginRow(uil,crl,0,0);{
            laShowItem(uil,crl,This,"use_operator")->Flags=LA_UI_FLAGS_ICON|LA_UI_FLAGS_PLAIN;
            b3=laOnConditionThat(uil,crl,laPropExpression(This,"use_operator"));{
                laUiItem* ui=laShowItem(uil,crl,This,"operator_name");ui->Expand=1;ui->Flags|=LA_UI_FLAGS_PLAIN;
            }laElse(uil,b3);{
                laUiItem* ui=laShowItem(uil,crl,This,"signal");ui->Expand=1;ui->Flags|=LA_UI_FLAGS_PLAIN;
            }laEndCondition(uil,b3);
        }laEndRow(uil,b2);
        b2=laBeginRow(uil,crr,0,0);{
            laShowItem(uil,crr,This,"device_type")->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_PLAIN;
            //laUiItem* uib=laShowItemFull(uil,crr,This,"select_key",0,"icon= ;",0,0);
            //uib->Expand=1; uib->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_UNDERNEATH;
            laUiItem* ui=laShowItem(uil,crr,This,"key"); ui->Expand=1;ui->Flags|=LA_UI_FLAGS_PLAIN;
            laShowItem(uil,crr,This,"remove")->Flags|=LA_UI_FLAGS_ICON;
        }laEndRow(uil,b2);
    }laEndCondition(uil,b1);

#else
    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowItem(uil,c,This,"remove")->Flags|=LA_UI_FLAGS_ICON;
    laShowItem(uil,c,This,"device_type");
    laUiItem* b1=laOnConditionThat(uil,c,laEqual(laPropExpression(This,"device_type"),laIntExpression(LA_INPUT_DEVICE_JOYSTICK)));{
        laShowItem(uil,c,This,"joystick_device");
    }laEndCondition(uil,b1);
    laShowItem(uil,c,This,"key")->Expand=1;
    laShowItem(uil,c,This,"select_key")->Flags|=LA_UI_FLAGS_ICON;
    laShowItem(uil,c,This,"signal")->Expand=1;
    //laShowItemFull(uil,c,This,"signal_selection",LA_WIDGET_COLLECTION_SELECTOR,0,0,0)->Flags|=LA_UI_COLLECTION_SIMPLE_SELECTOR;
    laShowItem(uil,c,This,"select_signal")->Flags|=LA_UI_FLAGS_ICON;
    laEndRow(uil,b);
#endif
}
void laui_InputMapping(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil);laSplitColumn(uil,c,0.3);
    laColumn* cl=laLeftColumn(c,3),*cr=laRightColumn(c,0);laSplitColumn(uil,cr,0.55);
    laColumn* crl=laLeftColumn(cr,0),*crr=laRightColumn(cr,0);

    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowItemFull(uil,c,This,"new_entry",0,"position=top;",0,0);
    laEndRow(uil,b);
    laShowSeparator(uil,c);

    laShowColumnAdjuster(uil,c);
    laShowLabel(uil,cl,"✓",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED;
    laShowLabel(uil,crl,"Operation",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED;
    laShowLabel(uil,crr,"Input",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED;

    laShowItemFull(uil,c,This,"entries",0,0,laui_InputMappingEntry,0)->Flags|=LA_UI_FLAGS_NO_DECAL;

    laShowSeparator(uil,c);
    b=laBeginRow(uil,c,0,0);
    laShowItem(uil,c,This,"new_entry");
    laEndRow(uil,b);
}
void laui_InputMappingBundle(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil);
    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowItemFull(uil,c,0,"la.input_mapping.current",LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0)
        ->Flags|=LA_UI_COLLECTION_SIMPLE_SELECTOR;
    laUiItem* b3=laOnConditionThat(uil,c,laPropExpression(0,"la.input_mapping.current"));{
        laShowItem(uil,c,0,"la.input_mapping.current.name");
        laShowItem(uil,c,0,"LA_new_input_mapping")->Flags|=LA_UI_FLAGS_ICON;
        laShowSeparator(uil,c);
        laUiItem* cp=laShowInvisibleItem(uil,c,0,"la.input_mapping.current");
        laShowItem(uil,c,&cp->PP,"remove");
    }laElse(uil,b3);{
        laShowItem(uil,c,0,"LA_new_input_mapping");
    }laEndCondition(uil,b3);
    laEndRow(uil,b);
    b3=laOnConditionThat(uil,c,laPropExpression(0,"la.input_mapping.current"));{
        laUiItem* gui=laMakeEmptyGroup(uil,c,0,0); laUiList* guil=gui->Page; laColumn* gc=laFirstColumn(guil); guil->HeightCoeff=-1;
        laShowItemFull(guil,gc,0,"la.input_mapping.current",LA_WIDGET_COLLECTION_SINGLE,0,laui_InputMapping,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
    }laEndCondition(uil,b3);
}

void laui_ToolboxEntryButton(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil);
    laInputMappingEntry* ime=This?This->EndInstance:0; if(!ime){ laShowLabel(uil,c,"?",0,0); return; }
    laShowItem(uil,c,This,"run_entry");
}
void laui_ToolboxEntry(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *ExtraColumns, int context){
    laColumn* c=ExtraColumns;
    laColumn* cl=laLeftColumn(c,3),*cr=laRightColumn(c,0);

    laUiItem* b,*b1,*b2,*b3,*b4;

    b=laBeginRow(uil,cl,1,1);
    laShowItem(uil,cl,This,"__move");
    laShowItem(uil,cl,This,"key")->Flags|=LA_UI_FLAGS_PLAIN|LA_TEXT_ALIGN_CENTER;
    b1=laOnConditionToggle(uil,cl,0,0,0,0,0);{
        laEndRow(uil,b);
        laShowLabel(uil,cl,"Mode",0,0)->Flags|=LA_TEXT_ALIGN_RIGHT|LA_UI_FLAGS_DISABLED;

        b2=laBeginRow(uil,cr,0,0);{
            laShowItem(uil,cr,This,"use_operator")->Flags=LA_UI_FLAGS_ICON|LA_UI_FLAGS_PLAIN|LA_UI_FLAGS_DISABLED;
            b3=laOnConditionThat(uil,cr,laPropExpression(This,"use_operator"));{
                laUiItem* bui=laShowItemFull(uil,cr,This,"select_operator",0,"icon= ;",0,0);
                bui->Expand=1; bui->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_UNDERNEATH;
                laUiItem* ui=laShowItem(uil,cr,This,"operator");
                ui->Expand=1; ui->Flags|=LA_UI_FLAGS_PLAIN;
            }laElse(uil,b3);{
                laUiItem* bui=laShowItemFull(uil,cr,This,"select_signal",0,"icon= ;",0,0);
                bui->Expand=1; bui->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_UNDERNEATH;
                laUiItem* ui=laShowItem(uil,cr,This,"signal");
                ui->Expand=1; ui->Flags|=LA_UI_FLAGS_PLAIN;
            }laEndCondition(uil,b3);
            laShowItem(uil,cr,This,"remove")->Flags|=LA_UI_FLAGS_ICON;
        }laEndRow(uil,b2);
        

        //b2=laBeginRow(uil,crr,0,0);{
        //    laShowSeparator(uil,crr)->Expand=1;
        //    laShowItem(uil,crr,This,"reset");
        //}laEndRow(uil,b2);
        laUiItem* ui=laShowItem(uil,cr,This,"use_operator"); ui->Flags|=LA_UI_FLAGS_EXPAND;
        b4=laOnConditionThat(uil,cr,laPropExpression(&ui->PP,""));{\
            laShowLabel(uil,cl,"Args",0,0)->Flags|=LA_TEXT_ALIGN_RIGHT|LA_UI_FLAGS_DISABLED;
            laShowItem(uil,cr,This,"operator_arguments");
        }laEndCondition(uil,b4);

        laShowLabel(uil,cl,"Disp",0,0)->Flags|=LA_TEXT_ALIGN_RIGHT|LA_UI_FLAGS_DISABLED;
        laShowItem(uil,cr,This,"key");

        laShowSeparator(uil,c);

    }laElse(uil,b1);{
        laEndRow(uil,b);
        b2=laBeginRow(uil,cr,0,0);{
            laShowItem(uil,cr,This,"use_operator")->Flags=LA_UI_FLAGS_ICON|LA_UI_FLAGS_PLAIN;
            b3=laOnConditionThat(uil,cr,laPropExpression(This,"use_operator"));{
                laUiItem* ui=laShowItem(uil,cr,This,"operator_name");ui->Expand=1;ui->Flags|=LA_UI_FLAGS_PLAIN;
            }laElse(uil,b3);{
                laUiItem* ui=laShowItem(uil,cr,This,"signal");ui->Expand=1;ui->Flags|=LA_UI_FLAGS_PLAIN;
            }laEndCondition(uil,b3);
            laShowItem(uil,cr,This,"remove")->Flags|=LA_UI_FLAGS_ICON;
        }laEndRow(uil,b2);
    }laEndCondition(uil,b1);
}
void laui_ToolboxListing(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil);laSplitColumn(uil,c,0.25);
    laColumn* cl=laLeftColumn(c,3),*cr=laRightColumn(c,0);

    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowItemFull(uil,c,This,"new_entry",0,"position=top;",0,0);
    laEndRow(uil,b);
    laShowSeparator(uil,c);

    //laShowColumnAdjuster(uil,c);
    //laShowLabel(uil,cl,"✓",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED;
    //laShowLabel(uil,crl,"Operation",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED;
    //laShowLabel(uil,crr,"Input",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED;

    laShowItemFull(uil,c,This,"entries",0,0,laui_ToolboxEntry,0)->Flags|=LA_UI_FLAGS_NO_DECAL;

    laShowSeparator(uil,c);
    b=laBeginRow(uil,c,0,0);
    laShowItem(uil,c,This,"new_entry");
    laEndRow(uil,b);
}
void lauidetached_Toolbox(laPanel* p){
    la_MakeDetachedProp(p, "la.input_mapping.current_toolbox", "toolbox");
    la_MakeDetachedProp(p, "la.input_mapping.toolbox_layout","layout");
}
void laui_Toolbox(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil); laSplitColumn(uil,c,0.2);
    laColumn* cl=laLeftColumn(c,1), *cr=laRightColumn(c,0);
    laUiItem* cui=laShowInvisibleItem(uil,c,Extra,"layout");

    laUiItem* b0=laOnConditionThat(uil,c,laEqual(laPropExpression(&cui->PP,""),laIntExpression(0)));{
        laUiItem* ui=laShowItemFull(uil,c,Extra,"toolbox.entries",0,0,laui_ToolboxEntryButton,0);
        ui->Flags|=LA_UI_FLAGS_NO_DECAL;
        laUiItem* b=laBeginRow(uil,c,0,0);{
            laUiList* muil=laMakeMenuPageEx(uil,c,"☰",LA_UI_FLAGS_NO_DECAL); laColumn* mc=laFirstColumn(muil);
            laUiItem* b1=laBeginRow(muil,mc,0,0);{
                laShowItemFull(muil,mc, 0, "LA_panel_activator", 0, "panel_id=LAUI_toolbox_editor;icon=🖍;", 0, 0)->Flags|=LA_UI_FLAGS_ICON;
                laShowItemFull(muil,mc,Extra,"layout",0,0,0,0)->Flags|=LA_UI_FLAGS_EXPAND|LA_UI_FLAGS_NO_CONFIRM;
            }laEndRow(muil,b1);
            laShowLabel(muil,mc,"Select a toolbox:",0,0)->Flags|=LA_TEXT_MONO;
            laShowItemFull(muil,mc,Extra,"toolbox",LA_WIDGET_COLLECTION,0,laui_IdentifierOnly,0);
        }laEndRow(uil,b);
    }laElse(uil,b0);{
        laUiItem* b=laBeginRow(uil,cl,0,0);{
            laUiList* muil=laMakeMenuPageEx(uil,cl,"☰",LA_UI_FLAGS_NO_DECAL); laColumn* mc=laFirstColumn(muil);
            laUiItem* b1=laBeginRow(muil,mc,0,0);{
                laShowItemFull(muil,mc, 0, "LA_panel_activator", 0, "panel_id=LAUI_toolbox_editor;icon=🖍;", 0, 0)->Flags|=LA_UI_FLAGS_ICON;
                laShowItemFull(muil,mc,Extra,"layout",0,0,0,0)->Flags|=LA_UI_FLAGS_EXPAND|LA_UI_FLAGS_NO_CONFIRM;
            }laEndRow(muil,b1);
            laShowLabel(muil,mc,"Select a toolbox:",0,0)->Flags|=LA_TEXT_MONO;
            laShowItemFull(muil,mc,Extra,"toolbox",LA_WIDGET_COLLECTION,0,laui_IdentifierOnly,0);
        }laEndRow(uil,b);
    }laEndCondition(uil,b0);

#define ADD_WIDTH_OF(n) \
    b0=laOnConditionThat(uil,cr,laEqual(laPropExpression(&cui->PP,""),laIntExpression(n)));{ \
        laUiItem* ui=laShowItemFull(uil,cr,Extra,"toolbox.entries",0,0,laui_ToolboxEntryButton,0); \
        ui->Flags|=LA_UI_FLAGS_NO_DECAL;  ui->Expand=n; \
    }laEndCondition(uil,b0)
    
    ADD_WIDTH_OF(3);
    ADD_WIDTH_OF(5);
    ADD_WIDTH_OF(8);
    ADD_WIDTH_OF(13);
}
void laui_ToolboxEditor(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil);
    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowItemFull(uil,c,0,"la.input_mapping.current_toolbox",LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0)
        ->Flags|=LA_UI_COLLECTION_SIMPLE_SELECTOR;
    laUiItem* b3=laOnConditionThat(uil,c,laPropExpression(0,"la.input_mapping.current_toolbox"));{
        laShowItem(uil,c,0,"la.input_mapping.current_toolbox.name");
        laShowItem(uil,c,0,"LA_new_toolbox")->Flags|=LA_UI_FLAGS_ICON;
        laShowSeparator(uil,c);
        laUiItem* cp=laShowInvisibleItem(uil,c,0,"la.input_mapping.current_toolbox");
        laShowItem(uil,c,&cp->PP,"remove");
    }laElse(uil,b3);{
        laShowItem(uil,c,0,"LA_new_toolbox");
    }laEndCondition(uil,b3);
    laEndRow(uil,b);
    b3=laOnConditionThat(uil,c,laPropExpression(0,"la.input_mapping.current_toolbox"));{
        laUiItem* gui=laMakeEmptyGroup(uil,c,0,0); laUiList* guil=gui->Page; laColumn* gc=laFirstColumn(guil); guil->HeightCoeff=-1;
        laShowItemFull(guil,gc,0,"la.input_mapping.current_toolbox",LA_WIDGET_COLLECTION_SINGLE,0,laui_ToolboxListing,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
    }laEndCondition(uil,b3);
}

void lauidetached_Drivers(laPanel* p){
    la_MakeDetachedProp(p, "la.detached_view_switch", "detached");
    la_MakeDetachedProp(p, "tns.world.root_objects", "root_object");
}
void laui_DriverListItem(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil),*cl, *cr,*cll,*clr;
    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowItem(uil,c,This,"use_script")->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_NO_DECAL|LA_UI_FLAGS_CYCLE;
    laShowItem(uil,c,This,"identifier")->Flags|=LA_UI_FLAGS_PLAIN;
    laUiItem* b4=laOnConditionThat(uil,c,laNot(laPropExpression(This,"use_script")));{
        laShowItem(uil,c,This,"trigger")->Flags|=LA_UI_FLAGS_PLAIN;
    }laEndCondition(uil,b4);
    laEndRow(uil,b);
}
void laui_Drivers(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil),*cl, *cr,*cll,*clr;
    laSplitColumn(uil,c,0.35); cl=laLeftColumn(c,7); cr=laRightColumn(c,0);
    laSplitColumn(uil,cl,0.4); cll=laLeftColumn(cl,1); clr=laRightColumn(cl,0);

    laShowItemFull(uil,cll,Extra,"detached",0,0,0,0)->Flags|=LA_UI_FLAGS_HIGHLIGHT|LA_UI_FLAGS_ICON;

#define ADD_PAGE \
    laUiItem* b2=laOnConditionThat(uil,cl,laPropExpression(&rb->PP,""));{ \
        laUiItem* b=laBeginRow(uil,cr,0,0); \
        laUiItem* b3=laOnConditionThat(uil,cl,laPropExpression(&rb->PP,"drivers.current_page"));{ \
            laUiItem* cp=laShowItemFull(uil,cr,&rb->PP,"drivers.current_page",LA_WIDGET_COLLECTION_SELECTOR,0,laui_DriverListItem,0); \
                cp->Flags|=LA_UI_COLLECTION_SIMPLE_SELECTOR; \
            laShowItem(uil,cr,&cp->PP,"name"); \
            laShowItem(uil,cr,&rb->PP,"add_driver_page")->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_EXIT_WHEN_TRIGGERED; \
            laShowItem(uil,cr,&cp->PP,"remove_driver_page")->Flags|=LA_UI_FLAGS_ICON; \
            laShowSeparator(uil,cr); \
            laShowItemFull(uil,cr,&cp->PP,"use_script",0,"icon=📃",0,0) \
                ->Flags|=LA_UI_FLAGS_HIGHLIGHT|LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_ICON; \
            laUiItem* b4=laOnConditionThat(uil,cl,laNot(laPropExpression(&cp->PP,"use_script")));{ \
                laShowItem(uil,cr,&cp->PP,"trigger"); \
            }laEndCondition(uil,b4); \
        }laElse(uil,b3);{ \
            laShowItem(uil,cr,&rb->PP,"add_driver_page")->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_EXIT_WHEN_TRIGGERED; \
        }laEndCondition(uil,b3); \
        laEndRow(uil,b); \
    }laEndCondition(uil,b2); \
    b2=laOnConditionThat(uil,c,laPropExpression(&rb->PP,"drivers.current_page"));{ \
        laUiItem* b3=laOnConditionThat(uil,c,laPropExpression(&rb->PP,"drivers.current_page.use_script"));{ \
            laShowItemFull(uil,c,&rb->PP,"drivers.current_page.script",0,0,0,0)->Extent=-2; \
        }laElse(uil,b3);{ \
            laShowItemFull(uil,c,&rb->PP,"drivers.current_page",LA_WIDGET_COLLECTION_SINGLE,0,laui_RackPage,0)->Flags|=LA_UI_FLAGS_NO_DECAL; \
        }laEndCondition(uil,b3); \
    }laElse(uil,b2);{ \
        laShowLabel(uil,c,"Select or add a logic page.",0,0)->Flags|=LA_TEXT_ALIGN_CENTER; \
    }laEndCondition(uil,b2);

    laUiItem* b1=laOnConditionThat(uil,c,laPropExpression(Extra,"detached"));{
        laUiItem* rb=laShowItemFull(uil,clr,Extra,"root_object",LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0);
        ADD_PAGE
    }laElse(uil,b1);{
        laUiItem* rb=laShowItemFull(uil,clr,0,"tns.world.active_root",LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0);
        ADD_PAGE
    }laEndCondition(uil,b1);

#undef ADD_PAGE
}
void lauidetached_Materials(laPanel* p){
    la_MakeDetachedProp(p, "la.detached_view_switch", "detached");
    la_MakeDetachedProp(p, "tns.world.materials", "material");
}
void laui_Materials(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil),*cl, *cr,*cll,*clr;
    laSplitColumn(uil,c,0.3); cl=laLeftColumn(c,1); cr=laRightColumn(c,0);

    laShowItemFull(uil,cl,Extra,"detached",0,0,0,0)->Flags|=LA_UI_FLAGS_HIGHLIGHT|LA_UI_FLAGS_ICON;

#define ADD_PAGE(SEL) \
    laUiItem* b=laBeginRow(uil,cr,0,0); \
    laUiItem* b2=laOnConditionThat(uil,cr,laPropExpression(&rb->PP,""));{ \
        laUiItem* cp=laShowInvisibleItem(uil,cr,&rb->PP,"shader_page"); \
        if(SEL){ \
            laShowItemFull(uil,cr,&rb->PP,"",LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0) \
                ->Flags|=LA_UI_COLLECTION_SIMPLE_SELECTOR; \
        } \
        laShowItem(uil,cr,&rb->PP,"name"); \
        laShowItem(uil,cr,0,"M_new_material")->Flags|=LA_UI_FLAGS_ICON; \
        laShowItem(uil,cr,&rb->PP,"remove")->Flags|=LA_UI_FLAGS_ICON; \
        laShowSeparator(uil,cr)->Expand=1; \
        laShowItemFull(uil,cr,&cp->PP,"use_script",0,"icon=📃;text=GLSL",0,0) \
            ->Flags|=LA_UI_FLAGS_HIGHLIGHT|LA_UI_FLAGS_CYCLE; \
        laShowItem(uil,cr,&rb->PP,"refresh"); \
        laShowSeparator(uil,cr); \
        laShowItem(uil,cr,&rb->PP,"as_library")->Flags|=LA_UI_FLAGS_HIGHLIGHT|LA_UI_FLAGS_CYCLE; \
    }laElse(uil,b2);{ \
        if(SEL){ \
            laShowItemFull(uil,cr,&rb->PP,"",LA_WIDGET_COLLECTION_SELECTOR,"text=Select",laui_IdentifierOnly,0) \
                ->Flags|=LA_UI_COLLECTION_SIMPLE_SELECTOR|LA_UI_FLAGS_EXPAND; \
            laShowItem(uil,cr,0,"M_new_material")->Flags|=LA_UI_FLAGS_ICON; \
        } \
    }laEndCondition(uil,b2); \
    laEndRow(uil,b); \
    b2=laOnConditionThat(uil,c,laPropExpression(&rb->PP,"shader_page"));{ \
        laUiItem* b3=laOnConditionThat(uil,c,laPropExpression(&rb->PP,"shader_page.use_script"));{ \
            laShowItemFull(uil,c,&rb->PP,"shader_page.script",0,0,0,0)->Extent=-2; \
        }laElse(uil,b3);{ \
            laShowItemFull(uil,c,&rb->PP,"shader_page",LA_WIDGET_COLLECTION_SINGLE,0,laui_RackPage,0)->Flags|=LA_UI_FLAGS_NO_DECAL; \
        }laEndCondition(uil,b3); \
    }laElse(uil,b2);{ \
        laShowLabel(uil,c,"Select or add a shader page.",0,0)->Flags|=LA_TEXT_ALIGN_CENTER; \
    }laEndCondition(uil,b2);

    laUiItem* b1=laOnConditionThat(uil,c,laPropExpression(Extra,"detached"));{
        laUiItem* rb=laShowInvisibleItem(uil,cr,Extra,"material");
        ADD_PAGE(1)
    }laElse(uil,b1);{
        laShowInvisibleItem(uil,cr,0,"tns.world.active_root.active.as_mesh.materials"); // for switching updates.
        laUiItem* rb=laShowInvisibleItem(uil,cr,0,"tns.world.active_root.active.as_mesh.current_material.material");
        ADD_PAGE(0)
    }laEndCondition(uil,b1);

#undef ADD_PAGE
}

void laui_AnimationActionSimple(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *ExtraColumns, int context){
    laColumn* c=laFirstColumn(uil);
    laUiItem* b=laBeginRow(uil,c,0,0);{
        laUiItem* b2=laOnConditionThat(uil,c,laEqual(laPropExpression(This,"__self"),laPropExpression(0,"la.animation.current_action")));{
            laShowLabel(uil,c,"🠶",0,0)->Flags|=LA_TEXT_MONO|LA_TEXT_ALIGN_RIGHT;
        }laElse(uil,b2);{
            laShowLabel(uil,c,"  ",0,0)->Flags|=LA_TEXT_MONO;
        }laEndCondition(uil,b2);
        laUiItem* ui=laShowItemFull(uil,c,This,"play_head",LA_WIDGET_METER_TYPE1,0,0,0);
        ui->Flags|=LA_UI_FLAGS_NO_LABEL|LA_UI_FLAGS_UNDERNEATH; ui->Expand=1;
        laShowItem(uil,c,This,"name")->Flags|=LA_UI_FLAGS_NO_DECAL|LA_UI_FLAGS_NO_EVENT;
        laShowItemFull(uil,c,This,"solo",0,"icon=S;",0,0)->Flags=LA_UI_FLAGS_ICON|LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT;
        laShowItemFull(uil,c,This,"mute",0,"icon=M;",0,0)->Flags=LA_UI_FLAGS_ICON|LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT;
    }
    laEndRow(uil,b);
}
void laui_AnimationActionHolder(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *ExtraColumns, int context){
    laColumn* c=laFirstColumn(uil);
    laShowItemFull(uil,c,This,"identifier",LA_WIDGET_STRING_PLAIN,0,0,0);
    laShowItemFull(uil,c,This,"__actions__",0,0,laui_AnimationActionSimple,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
}
void laui_AnimationActionListItem(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *ExtraColumns, int context){
    laColumn* c=laFirstColumn(uil); laUiItem* ui;
    laUiItem* b=laBeginRow(uil,c,0,0);{
        //laUiItem* b2=laOnConditionThat(uil,c,laNot(laEqual(laPropExpression(This,"__self"),laPropExpression(0,"la.animation.current_action"))));{
        //    ui=laShowItemFull(uil,c,This,"play_head",LA_WIDGET_METER_TYPE1,0,0,0);
        //    ui->Flags|=LA_UI_FLAGS_NO_LABEL|LA_UI_FLAGS_UNDERNEATH; ui->Expand=1;
        //}laEndCondition(uil,b2);
        ui=laShowItem(uil,c,This,"name");
        ui->Flags|=LA_UI_FLAGS_NO_DECAL|LA_UI_FLAGS_NO_EVENT; ui->Expand=1;
    }
    laEndRow(uil,b);
}
void laui_AnimationActions(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil),*cl, *cr; laSplitColumn(uil,c,0.5); cl=laLeftColumn(c,0); cr=laRightColumn(c,5);
    laUiItem* b,*ui;

    ui=laShowItemFull(uil,cl,0,"la.animation.play_head",0,0,0,0);
    ui->Flags|=LA_UI_FLAGS_NO_LABEL|LA_TEXT_ALIGN_CENTER; ui->Expand=1;

    laUiItem* row=laBeginRow(uil,cr,0,0);
    laShowItemFull(uil,cr,0,"LA_animation_reset_time",0,0,0,0)->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_EXIT_WHEN_TRIGGERED;
    b=laOnConditionThat(uil,cr,laPropExpression(0,"la.animation.play_status"));{
        ui=laShowItemFull(uil,cr,0,"LA_animation_set_play_status",0,"text=❚❚;",0,0);
        ui->Flags|=LA_UI_FLAGS_EXIT_WHEN_TRIGGERED|LA_TEXT_ALIGN_CENTER; ui->Expand=1;
    }laElse(uil,b);{
        ui=laShowItemFull(uil,cr,0,"LA_animation_set_play_status",0,"icon=◀;mode=reverse;",0,0);
        ui->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_EXIT_WHEN_TRIGGERED; ui->Expand=1;
        ui=laShowItemFull(uil,cr,0,"LA_animation_set_play_status",0,"icon=▶;mode=forward;",0,0);
        ui->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_EXIT_WHEN_TRIGGERED; ui->Expand=1;
    }laEndCondition(uil,b);
    laEndRow(uil,row);

    ui=laMakeEmptyGroup(uil,c,"Use Format",0); laUiList* gu=ui->Page; laColumn* gc=laFirstColumn(gu);
    gu->HeightCoeff=-2; //ui->Flags|=LA_UI_FLAGS_NO_DECAL;
    for(laActionHolderPath* ahp=MAIN.Animation->ActionHolderPaths.pFirst;ahp;ahp=ahp->Item.pNext){
        laShowLabel(gu,gc,ahp->PP.LastPs->p->Name,0,0)->Flags|=LA_UI_FLAGS_DISABLED|LA_TEXT_MONO;
        laShowItemFull(gu,gc,0,ahp->OriginalPath,LA_WIDGET_COLLECTION,"feedback=NONE",laui_AnimationActionHolder,0)->Flags|=LA_UI_FLAGS_NO_DECAL|LA_UI_FLAGS_NO_GAP;
    }

    row=laBeginRow(uil,cl,0,0);
    laShowItemFull(uil,cl,0,"LA_animation_new_action",0,"text=New",0,0);
    laEndRow(uil,row);
}
void laui_AnimationActionChannels(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil),*cl, *cr, *cll,*clr, *crl, *crr;
    laSplitColumn(uil,c,0.3); cl=laLeftColumn(c,6); cr=laRightColumn(c,0);
    laSplitColumn(uil,cl,0.75); cll=laLeftColumn(cl,0); clr=laRightColumn(cl,1);
    laSplitColumn(uil,cr,0.75); crl=laLeftColumn(cr,0); crr=laRightColumn(cr,10);
    laUiItem* b,*ui,*b2,*b3;

    laUiItem* row=laBeginRow(uil,cll,0,0);
    laShowItemFull(uil,cll,0,"LA_animation_reset_time",0,"current=true;text=1",0,0)->Flags|=LA_UI_FLAGS_EXIT_WHEN_TRIGGERED;
    b=laOnConditionThat(uil,cll,laPropExpression(0,"la.animation.play_status"));{
        ui=laShowItemFull(uil,cll,0,"LA_animation_set_play_status",0,"text=❚❚;",0,0);
        ui->Flags|=LA_UI_FLAGS_EXIT_WHEN_TRIGGERED|LA_TEXT_ALIGN_CENTER; ui->Expand=1;
    }laElse(uil,b);{
        ui=laShowItemFull(uil,cll,0,"LA_animation_set_play_status",0,"icon=◀;mode=reverse;",0,0);
        ui->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_EXIT_WHEN_TRIGGERED; ui->Expand=1;
        ui=laShowItemFull(uil,cll,0,"LA_animation_set_play_status",0,"icon=▶;mode=forward;",0,0);
        ui->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_EXIT_WHEN_TRIGGERED; ui->Expand=1;
    }laEndCondition(uil,b);
    laEndRow(uil,row);

    b2=laOnConditionThat(uil,c,laPropExpression(0,"la.animation.current_action"));{
        laUiItem* ca=laShowInvisibleItem(uil,c,0,"la.animation.current_action");
        row=laBeginRow(uil,crl,0,0);
        ui=laShowItemFull(uil,crl,&ca->PP,"play_head",LA_WIDGET_METER_TYPE1,0,0,0);
        ui->Flags|=LA_UI_FLAGS_NO_LABEL|LA_UI_FLAGS_UNDERNEATH; ui->Expand=1;
        laShowItem(uil,crl,&ca->PP,"current_frame")->Flags|=LA_UI_FLAGS_NO_LABEL|LA_UI_FLAGS_NO_DECAL|LA_TEXT_ALIGN_CENTER;
        laShowSeparator(uil,crl);
        laShowItem(uil,crl,&ca->PP,"frame_count")->Flags|=LA_TEXT_ALIGN_CENTER;
        laShowItem(uil,crl,&ca->PP,"length");
        laEndRow(uil,row);
        b3=laBeginRow(uil,crr,0,0);
        laShowItem(uil,crr,&ca->PP,"name")->Expand=1;
        laShowItemFull(uil,crr,0,"LA_animation_select_action",0,0,0,0)->Flags|=LA_UI_FLAGS_ICON;
        laEndRow(uil,b3);
        b=laOnConditionToggle(uil,clr,0,0,0,0,0);{ strSafeSet(&b->ExtraInstructions,"text=☰");
            laShowItem(uil,cl,&ca->PP,"remove");
            row=laBeginRow(uil,cr,0,0);
            laShowItemFull(uil,cr,&ca->PP,"play_by_default",0,"text=Auto Play;",0,0)->Flags|=LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT;
            laShowItem(uil,cr,&ca->PP,"mix_mode")->Flags|=LA_UI_FLAGS_EXPAND;
            laShowItem(uil,cr,&ca->PP,"play_mode")->Flags|=LA_UI_FLAGS_EXPAND;
            laShowSeparator(uil,cr)->Expand=1;
            laShowItem(uil,cr,&ca->PP,"solo")->Flags|=LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT;
            laShowItem(uil,cr,&ca->PP,"mute")->Flags|=LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT;
            laEndRow(uil,row);
        }laEndCondition(uil,b);
    }laElse(uil,b2);{
        laShowLabel(uil,crl,"Please select an action.",0,0);
        laShowItemFull(uil,crr,0,"LA_animation_select_action",0,0,0,0);
    }laEndCondition(uil,b2);

    laShowCanvas(uil,c,0,"la.animation.current_action","la_AnimationActionDrawCanvas",-1);
}


void tnsui_CameraObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil);
    laShowLabel(uil,c,"Camera Object",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED; 
}
void tnsui_LightObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil);
    laShowLabel(uil,c,"Light Object",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED; 
}
void tnsui_Material(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil),*cl,*cr; laUiItem* b,*b1,*b2;
    laSplitColumn(uil,c,0.4); cl=laLeftColumn(c,8); cr=laRightColumn(c,0);
    if(!context){
        laShowLabel(uil,cl,"Name",0,0); laShowItem(uil,cr,This,"name");
    }
    b2=laOnConditionThat(uil,c,laPropExpression(This,"as_library"));{
        laShowLabel(uil,c,"Material is common library.",0,0);
    }laElse(uil,b2);{
        b=laBeginRow(uil,cl,0,0);
        laShowLabel(uil,cl,"Color",0,0)->Expand=1;
        b1=laOnConditionThat(uil,cl,laOr(laPropExpression(This,"shader_page.racks"),
                                        laPropExpression(This,"shader_page.use_script")));{
            laShowLabel(uil,cl,"🎨",0,0);
        }laEndCondition(uil,b1);
        laEndRow(uil,b);
        laShowItemFull(uil,cr,This,"color",LA_WIDGET_FLOAT_COLOR,0,0,0)->Flags|=LA_UI_FLAGS_COLORFUL;
        laShowItemFull(uil,cr,This,"colorful",LA_WIDGET_ENUM_HIGHLIGHT,0,0,0);
    }laEndCondition(uil,b2);

    laShowSeparator(uil,c);
    laShowItemFull(uil,c,This,"gradient_mode",0,0,0,0)->Flags|=LA_UI_FLAGS_EXPAND;
    b1=laOnConditionThat(uil,c,laPropExpression(This,"gradient_mode"));{
        laShowLabel(uil,cl,"End Color",0,0);
        laShowItemFull(uil,cr,This,"color2",LA_WIDGET_FLOAT_COLOR,0,0,0)->Flags|=LA_UI_FLAGS_COLORFUL;
        laShowLabel(uil,cl,"Center",0,0);
        laShowItemFull(uil,cr,This,"gradient_center",0,0,0,0);
        laShowLabel(uil,cl,"Size",0,0);
        laShowItemFull(uil,cr,This,"gradient_size",0,0,0,0);
        b2=laOnConditionThat(uil,c,laEqual(laPropExpression(This,"gradient_mode"),laIntExpression(TNS_GRADIENT_MODE_BOX)));{
        laShowItemFull(uil,cr,This,"gradient_box_r",0,0,0,0);
        laShowItemFull(uil,cr,This,"gradient_box_f",0,0,0,0);
        }laEndCondition(uil,b2);
    }laEndCondition(uil,b1);
    
}
void tnsui_MaterialListItem(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil); laUiItem* b,*b1;
    b=laBeginRow(uil,c,0,0);
    laShowItemFull(uil,c,This,"color",LA_WIDGET_FLOAT_COLOR,0,0,0)->Flags|=LA_UI_FLAGS_NO_EVENT|LA_UI_FLAGS_ICON;
    laUiItem* ui=laShowItem(uil,c,This,"name"); ui->Flags|=LA_UI_FLAGS_PLAIN; ui->Expand=1;
    b1=laOnConditionThat(uil,c,laOr(laPropExpression(This,"shader_page.racks"),
                                    laPropExpression(This,"shader_page.use_script")));{
        laShowLabel(uil,c,"🎨",0,0);
    }laEndCondition(uil,b1);
    laEndRow(uil,b);
}
void tnsui_MaterialSlot(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil); laUiItem* b,*b1,*b2,*b3;
    b1=laOnConditionThat(uil,c,laPropExpression(This,"material"));{
        b=laBeginRow(uil,c,0,0);
        b3=laOnConditionThat(uil,c,laPropExpression(This,"material.as_library"));{
            laShowLabel(uil,c,"LIB ",0,0)->Flags|=LA_UI_DISABLED;
            laShowItem(uil,c,This,"material.name")->Flags|=LA_UI_FLAGS_PLAIN|LA_UI_FLAGS_DISABLED;
        }laElse(uil,b3);{
            laShowItemFull(uil,c,This,"material.color",LA_WIDGET_FLOAT_COLOR,0,0,0)
                ->Flags|=LA_UI_FLAGS_NO_EVENT|LA_UI_FLAGS_ICON|LA_UI_FLAGS_COLORFUL;
            laUiItem* ui=laShowItem(uil,c,This,"material.name"); ui->Flags|=LA_UI_FLAGS_PLAIN; ui->Expand=1;
            b2=laOnConditionThat(uil,c,laOr(laPropExpression(This,"material.shader_page.racks"),
                                        laPropExpression(This,"material.shader_page.use_script")));{
                laShowLabel(uil,c,"🎨",0,0);
            }laEndCondition(uil,b2);
        }laEndCondition(uil,b3);
        laEndRow(uil,b);
    }laElse(uil,b1);{
        laShowItem(uil,c,This,"name")->Flags|=LA_UI_FLAGS_DISABLED;
    }laEndCondition(uil,b1);
}
void tnsui_MeshObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil); laUiItem* g,*tg; laUiList* gu,*tu; laColumn* gc,*gcl,*gcr,*tc; laUiItem* b1,*b2,*b3,*b4,*b5;
    laShowLabel(uil,c,"Mesh Object",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED; 
    g=laMakeGroup(uil,c,"Materials",0); gu=g->Page; gc=laFirstColumn(gu); laSplitColumn(gu,gc,0.7);
    gcl=laLeftColumn(gc,0); gcr=laRightColumn(gc,1);
    tg=laMakeEmptyGroup(gu,gcl,"Slots",0); tu=tg->Page; tc=laFirstColumn(tu); tu->HeightCoeff=4;
    laShowItemFull(tu,tc,This,"materials",0,0,tnsui_MaterialSlot,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
    laShowItem(gu,gcr,This,"add_material_slot");
    b2=laOnConditionThat(gu,gc,laPropExpression(This,"current_material"));{
        laUiItem* mat=laShowInvisibleItem(gu,gc,This,"current_material");
        laShowItem(gu,gcr,&mat->PP,"remove");
        b3=laBeginRow(gu,gc,0,0); laUiItem* sel;
        laShowItem(gu,gc,This,"add_material")->Flags|=LA_UI_FLAGS_ICON;
        b4=laOnConditionThat(gu,gc,laPropExpression(&mat->PP,"material"));{
            sel=laShowItemFull(gu,gc,&mat->PP,"material",LA_WIDGET_COLLECTION_SELECTOR,0,tnsui_MaterialListItem,0);
            sel->Flags|=LA_UI_COLLECTION_SIMPLE_SELECTOR;
            laShowItem(gu,gc,&mat->PP,"material.name")->Expand=1;
            laShowItem(gu,gc,&sel->PP,"clear_selection")->Flags|=LA_UI_FLAGS_ICON;
        }laElse(gu,b4);{
            sel=laShowItemFull(gu,gc,&mat->PP,"material",LA_WIDGET_COLLECTION_SELECTOR,0,tnsui_MaterialListItem,0);
            sel->Flags|=LA_UI_COLLECTION_SIMPLE_SELECTOR; sel->Expand=1;
        }laEndCondition(gu,b4);
        laEndRow(gu,b3);
        b4=laOnConditionThat(gu,gc,laEqual(laPropExpression(This,"mode"),laIntExpression(TNS_MESH_EDIT_MODE)));{
            b5=laBeginRow(gu,gc,0,0);
            laShowLabel(gu,gc,"Edit:",0,0);
            laShowItemFull(gu,gc,This,"assign_material_slot",0,"text=Assign",0,0);
            laEndRow(gu,b5);
        }laEndCondition(gu,b4);
        laShowSeparator(gu,gc);
        b4=laOnConditionThat(gu,gc,laPropExpression(&mat->PP,"material"));{
            laShowItemFull(gu,gc,&mat->PP,"material",LA_WIDGET_COLLECTION_SINGLE,0,tnsui_Material,1)->Flags|=LA_UI_FLAGS_NO_DECAL;
        }laEndCondition(gu,b4);
    }laElse(gu,b2);{
        laShowLabel(gu,gc,"No active material slot.",0,0);
    }laEndCondition(gu,b2);
}
void tnsui_ShapeObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil); laUiItem* g,*tg; laUiList* gu,*tu; laColumn* gc,*gcl,*gcr,*tc; laUiItem* b1,*b2,*b3,*b4,*b5;
    laShowLabel(uil,c,"Shape Object",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED; 
    g=laMakeGroup(uil,c,"Materials",0); gu=g->Page; gc=laFirstColumn(gu); laSplitColumn(gu,gc,0.4);
    gcl=laLeftColumn(gc,10); gcr=laRightColumn(gc,0);
    laShowLabel(gu,gcl,"Backdrop",0,0);
    laShowItemFull(gu,gcr,This,"backdrop",0,"text=Backdrop",0,0)->Flags|=LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT;

    g=laMakeGroup(uil,c,"Materials",0); gu=g->Page; gc=laFirstColumn(gu); laSplitColumn(gu,gc,0.7);
    gcl=laLeftColumn(gc,0); gcr=laRightColumn(gc,1);
    tg=laMakeEmptyGroup(gu,gcl,"Slots",0); tu=tg->Page; tc=laFirstColumn(tu); tu->HeightCoeff=4;
    laShowItemFull(tu,tc,This,"materials",0,0,tnsui_MaterialSlot,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
    laShowItem(gu,gcr,This,"add_material_slot");
    b2=laOnConditionThat(gu,gc,laPropExpression(This,"current_material"));{
        laUiItem* mat=laShowInvisibleItem(gu,gc,This,"current_material");
        laShowItem(gu,gcr,&mat->PP,"remove");
        b3=laBeginRow(gu,gc,0,0); laUiItem* sel;
        laShowItem(gu,gc,This,"add_material")->Flags|=LA_UI_FLAGS_ICON;
        b4=laOnConditionThat(gu,gc,laPropExpression(&mat->PP,"material"));{
            sel=laShowItemFull(gu,gc,&mat->PP,"material",LA_WIDGET_COLLECTION_SELECTOR,0,tnsui_MaterialListItem,0);
            sel->Flags|=LA_UI_COLLECTION_SIMPLE_SELECTOR;
            laShowItem(gu,gc,&mat->PP,"material.name")->Expand=1;
            laShowItem(gu,gc,&sel->PP,"clear_selection")->Flags|=LA_UI_FLAGS_ICON;
        }laElse(gu,b4);{
            sel=laShowItemFull(gu,gc,&mat->PP,"material",LA_WIDGET_COLLECTION_SELECTOR,0,tnsui_MaterialListItem,0);
            sel->Flags|=LA_UI_COLLECTION_SIMPLE_SELECTOR; sel->Expand=1;
        }laEndCondition(gu,b4);
        laEndRow(gu,b3);
        b4=laOnConditionThat(gu,gc,laEqual(laPropExpression(This,"mode"),laIntExpression(TNS_MESH_EDIT_MODE)));{
            b5=laBeginRow(gu,gc,0,0);
            laShowLabel(gu,gc,"Edit:",0,0);
            laShowItemFull(gu,gc,This,"assign_material_slot",0,"text=Assign",0,0);
            laEndRow(gu,b5);
        }laEndCondition(gu,b4);
        laShowSeparator(gu,gc);
        b4=laOnConditionThat(gu,gc,laPropExpression(&mat->PP,"material"));{
            laShowItemFull(gu,gc,&mat->PP,"material",LA_WIDGET_COLLECTION_SINGLE,0,tnsui_Material,1)->Flags|=LA_UI_FLAGS_NO_DECAL;
        }laEndCondition(gu,b4);
    }laElse(gu,b2);{
        laShowLabel(gu,gc,"No active material slot.",0,0);
    }laEndCondition(gu,b2);
}
void tnsui_InstancerObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil), *cl,*cr;
    laSplitColumn(uil,c,0.5); cl=laLeftColumn(c,5); cr=laRightColumn(c,0);
    laShowLabel(uil,cl,"Instance",0,0); laShowItemFull(uil,cr,This,"instance",LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0);
    laUiItem* b=laOnConditionThat(uil,c,laAnd(laPropExpression(This,"instance"),laPropExpression(This,"instance.__self.as_root_object.is_2d")));
    laShowSeparator(uil,c);
    laShowLabel(uil,cl,"Hook",0,0); laShowItemFull(uil,cr,This,"hook",0,0,0,0);
    laShowLabel(uil,cl,"Offsets",0,0); laShowItemFull(uil,cr,This,"hook_offset",0,0,0,0)->Flags|=LA_UI_FLAGS_TRANSPOSE;
    laEndCondition(uil,b);
}
void tnsui_RootObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil), *cl,*cr;
    laSplitColumn(uil,c,0.5); cl=laLeftColumn(c,5); cr=laRightColumn(c,0);
    laShowLabel(uil,cl,"Mode",0,0); laShowItemFull(uil,cr,This,"is_2d",0,0,0,0)->Flags|=LA_UI_FLAGS_EXPAND;
    laShowLabel(uil,cl,"Active Camera",0,0); laShowItemFull(uil,cr,This,"active_camera",LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0);
}
void tnsui_BaseObjectProperties(laUiList *uil, laPropPack *This, laPropPack *UNUSED_Extra, laColumn *UNUSED_Colums, int context){
    laColumn* c=laFirstColumn(uil); laUiItem* g,*tg; laUiList* gu,*tu; laColumn* gc,*gcl,*gcr,*tc; laUiItem* b1,*b2,*b3,*b4,*b5;
    laShowLabel(uil,c,"Generic",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED; 
    g=laMakeGroup(uil,c,"Object",0); gu=g->Page; gc=laFirstColumn(gu);
    b1=laOnConditionToggle(gu,gc,0,0,0,0,0);{ strSafeSet(&b1->ExtraInstructions,"text=Transformations");
        //b1->State=LA_UI_ACTIVE;
        laSplitColumn(gu,gc,0.5); gcl=laLeftColumn(gc,0); gcr=laRightColumn(gc,0);
        laShowLabel(gu,gcl,"Location",0,0); laShowItem(gu,gcl,This,"location")->Flags|=LA_UI_FLAGS_TRANSPOSE;
        laShowLabel(gu,gcr,"Rotation",0,0); laShowItem(gu,gcr,This,"rotation")->Flags|=LA_UI_FLAGS_TRANSPOSE;
        laShowLabel(gu,gcr,"Scale",0,0);    laShowItem(gu,gcr,This,"scale")->Flags|=LA_UI_FLAGS_TRANSPOSE;
        laUiItem* b=laOnConditionToggle(gu,gc,0,0,0,0,0);{ strSafeSet(&b->ExtraInstructions,"text=Delta");
            laShowLabel(gu,gcl,"Delta Loc",0,0); laShowItem(gu,gcl,This,"dlocation")->Flags|=LA_UI_FLAGS_TRANSPOSE;
            laShowLabel(gu,gcr,"Delta Rot",0,0); laShowItem(gu,gcr,This,"drotation")->Flags|=LA_UI_FLAGS_TRANSPOSE;
            laShowLabel(gu,gcr,"Delta Scale",0,0);laShowItem(gu,gcr,This,"dscale")->Flags|=LA_UI_FLAGS_TRANSPOSE;
        }laEndCondition(gu,b);
        laShowSeparator(gu,gc);
    }laEndCondition(gu,b1);
}
void tnsuidetached_ObjectProperties(laPanel* p){
    la_MakeDetachedProp(p, "la.detached_view_switch", "detached");
    la_MakeDetachedProp(p, "tns.world.root_objects", "root_object");
    la_MakeDetachedProp(p, "tns.world.property_page", "page");
}
void tnsui_ObjectProperties(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil);

    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowItemFull(uil,c,Extra,"detached",0,0,0,0)->Flags|=LA_UI_FLAGS_HIGHLIGHT|LA_UI_FLAGS_ICON;

#define ADD_PAGE1(base,path) \
    laUiItem* rb=laShowItemFull(uil,c,base,path,LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0); \
        rb->Flags|=LA_UI_COLLECTION_SIMPLE_SELECTOR; \
    laUiItem* b2=laOnConditionThat(uil,c,laPropExpression(&rb->PP,""));{ \
        laShowItem(uil,c,&rb->PP,"name")->Flags|=LA_UI_FLAGS_NO_DECAL; \
        laUiItem* b3=laOnConditionThat(uil,c,laPropExpression(&rb->PP,"active"));{ \
            laShowLabel(uil,c,"⯈",0,0); laShowItem(uil,c,&rb->PP,"active.name")->Flags|=LA_UI_FLAGS_NO_DECAL; \
        }laEndCondition(uil,b3); \
    }laEndCondition(uil,b2); \
    laEndRow(uil,b); \
    b=laBeginRow(uil,c,0,0); \
    laShowItemFull(uil,c,Extra,"page",0,0,0,0)->Flags|=LA_UI_FLAGS_EXPAND; \
    laEndRow(uil,b); \
    b=laOnConditionThat(uil,c,laNot(laPropExpression(Extra,"page")));{ \
        laShowItemFull(uil,c,base,path ".as_root_object",LA_WIDGET_COLLECTION_SINGLE,0,0,0)->Flags|=LA_UI_FLAGS_NO_DECAL; \
    }laElse(uil,b);

#define ADD_PAGE2 \
    laUiItem* b3=laOnConditionThat(uil,c,laPropExpression(&actui->PP,""));{ \
        tnsui_BaseObjectProperties(uil,&actui->PP,0,0,0); \
    }laElse(uil,b3);{ \
        laShowLabel(uil,c,"No active object.",0,0)->Flags|=LA_TEXT_ALIGN_CENTER|LA_UI_FLAGS_DISABLED;; \
    }laEndCondition(uil,b3); \
    laEndCondition(uil,b);

    laUiItem* b0=laOnConditionThat(uil,c,laPropExpression(Extra,"detached"));{
        ADD_PAGE1(Extra,"root_object")
        laUiItem* actui=laShowItemFull(uil,c,&rb->PP,"active",LA_WIDGET_COLLECTION_SINGLE,0,0,0);
            actui->Flags|=LA_UI_FLAGS_NO_DECAL;
        ADD_PAGE2
    }laElse(uil,b0);{
        ADD_PAGE1(0,"tns.world.active_root")
        laUiItem* actui=laShowItemFull(uil,c,0,"tns.world.active_root.active",LA_WIDGET_COLLECTION_SINGLE,0,0,0);
            actui->Flags|=LA_UI_FLAGS_NO_DECAL;
        ADD_PAGE2
    }laEndCondition(uil,b0);

#undef ADD_PAGE1
#undef ADD_PAGE2
}
void tnsui_DetachedScenePanel(laPanel* p){
    la_MakeDetachedProp(p, "la.detached_view_switch", "detached");
    la_MakeDetachedProp(p, "tns.world.root_objects", "root_object");
}
void tnsui_ScenePanel(laUiList *uil, laPropPack *This, laPropPack *DetachedProps, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil); 
    laShow3DCanvasCombo(uil,c,DetachedProps,"root_object",-1,DetachedProps);
}


void tnsui_ObjectHierachy(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context);
void tnsui_ChildObjectHierachy(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil); laShowItemFull(uil,c,This,"object",0,0,tnsui_ObjectHierachy,1)
        ->Flags|=LA_UI_FLAGS_NO_DECAL|LA_UI_FLAGS_NO_GAP|LA_UI_COLLECTION_NO_HIGHLIGHT;
}
void tnsui_ObjectHierachy(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil),*cl,*cr; laSplitColumn(uil,c,0.5); cl=laLeftColumn(c,1); cr=laRightColumn(c,0);
    laUiItem* r=laBeginRow(uil,cr,0,0);
    laShowItemFull(uil,cr,This,"name",0,0,0,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
    laEndRow(uil,r);
    char* prop="base.children";
    laUiItem* b2=laOnConditionThat(uil,cl,laPropExpression(This,prop));{
        laUiItem* b=laOnConditionToggle(uil,cl,0,0,0,0,0);{ b->Flags|=LA_UI_FLAGS_NO_DECAL;
            b->State=LA_UI_ACTIVE;
            laShowItemFull(uil,cr,This,prop,0,0,tnsui_ChildObjectHierachy,0)
                ->Flags|=LA_UI_FLAGS_NO_DECAL|LA_UI_FLAGS_NO_GAP|LA_UI_COLLECTION_NO_HIGHLIGHT;
        }laEndCondition(uil,b);
    }laEndCondition(uil,b2);
}
void tnsui_WorldHierachy(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil),*gc; laUiList* gu;
    laUiItem* r=laBeginRow(uil,c,0,0);
    gu=laMakeMenuPage(uil,c,"Menu");{  gc=laFirstColumn(gu);
        laShowItem(gu,gc,0,"M_new_root");
    }
    laEndRow(uil,r);
    laUiItem* g=laMakeEmptyGroup(uil,c,"Hierachy",0);{ gu=g->Page; gc=laFirstColumn(gu);
        gu->HeightCoeff=-1; //g->Flags|=LA_UI_FLAGS_NO_DECAL;
        laShowItemFull(gu,gc,0,"tns.world.root_objects",0,0,tnsui_ObjectHierachy,0)->Flags|=LA_UI_FLAGS_NO_DECAL;
    }
}
void tnsui_RootObjectMenuUi(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil);
    laShowItemFull(uil, c, This, "name",LA_WIDGET_STRING_PLAIN,0,0,0);
    laShowItemFull(uil, c, This, "remove_root",0,0,0,0);
    laShowItemFull(uil, c, 0, "M_new_root",0,0,0,0);
}



void la_RegisterBuiltinTemplates(){
    int obj=MAIN.InitArgs.HasWorldObjects;
    int tex=MAIN.InitArgs.HasTextureInspector;
    int his=MAIN.InitArgs.HasHistories;
    int ter=MAIN.InitArgs.HasTerminal;
    int act=MAIN.InitArgs.HasAction&&obj;
            laRegisterUiTemplate("LAUI_toolbox","Toolbox",laui_Toolbox,lauidetached_Toolbox,0,"Toolboxes",0,0,0);
    if(obj) laRegisterUiTemplate("LAUI_scene", "Scene", tnsui_ScenePanel, tnsui_DetachedScenePanel, 0, 0, 0,25,25);
    if(obj) laRegisterUiTemplate("LAUI_world_hierachy","World",tnsui_WorldHierachy,0,0,0,0,0,0);
    if(obj) laRegisterUiTemplate("LAUI_object_properties", "Properties", tnsui_ObjectProperties, tnsuidetached_ObjectProperties, 0, 0, 0,15,25);
    if(act) laRegisterUiTemplate("LAUI_animation_actions", "Actions", laui_AnimationActions, 0, 0, 0, 0,15,25);
    if(act) laRegisterUiTemplate("LAUI_animation_action_channels", "Action Channels", laui_AnimationActionChannels, 0, 0, 0, 0,20,15); 
    if(obj) laRegisterUiTemplate("LAUI_drivers","Drivers",laui_Drivers,lauidetached_Drivers,0,0,0,0,0);
    if(obj) laRegisterUiTemplate("LAUI_materials","Materials",laui_Materials,lauidetached_Materials,0,0,0,0,0);
            
            laRegisterUiTemplate("LAUI_user_preferences", "User Preferences", laui_UserPreference, 0, 0, "System",0,25,25);
            laRegisterUiTemplate("LAUI_toolbox_editor","Toolbox Editor",laui_ToolboxEditor,0,0,0,0,0,0);
            laRegisterUiTemplate("LAUI_input_mapping","Input Mapping",laui_InputMappingBundle,0,0,0,0,0,0);
            laRegisterUiTemplate("LAUI_controllers", "Controllers", laui_GameController, lauidetached_GameController, 0,0,0,0,0);\
    if(tex) laRegisterUiTemplate("LAUI_texture_inspector", "Texture Inspector", laui_TextureInspector, lauidetached_TextureInspector, 0, 0, 0,0,0);
            laRegisterUiTemplate("LAUI_data_manager", "Data Manager", laui_IdleDataManager, lauidetached_IdleDataManager, 0, 0, 0,25,25);
    if(his) laRegisterUiTemplate("LAUI_histories", "Histories", laui_UndoHistories, 0, 0, 0, 0,10,25);
            laRegisterUiTemplate("LAUI_about", "About", laui_About, 0, 0, 0, 0,15,25);
    if(ter) laRegisterUiTemplate("LAUI_terminal", "Terminal", laui_Terminal, 0, 0, 0, 0,20,25);
  
}
