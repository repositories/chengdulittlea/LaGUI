/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../la_5.h"

extern LA MAIN;
extern tnsMain* T;

laWidget _LA_WIDGET_FIXED_GROUP={0};
laWidget _LA_WIDGET_TAB={0};
laWidget _LA_WIDGET_COLLECTION={0};
laWidget _LA_WIDGET_COLLECTION_ITEM={0};
laWidget _LA_WIDGET_COLLECTION_SELECTOR={0};
laWidget _LA_WIDGET_COLLECTION_SINGLE={0};
laWidget _LA_WIDGET_CONDITION_TOGGLE={0};
laWidget _LA_WIDGET_COLUMN_ADJUSTER={0,LA_UI_FLAGS_NO_HEIGHT};
laWidget _LA_WIDGET_COLUMN_VIEWER={0};
laWidget _LA_WIDGET_BUTTON={0};
laWidget _LA_WIDGET_BUTTON_NO_CONFIRM={0};
laWidget _LA_WIDGET_LABEL={0};
laWidget _LA_WIDGET_INT={0};
laWidget _LA_WIDGET_INT_PLAIN={0, LA_UI_FLAGS_PLAIN};
laWidget _LA_WIDGET_INT_PLAIN_ICON={0, LA_UI_FLAGS_INT_ICON};
laWidget _LA_WIDGET_METER_TYPE1={0,LA_TEXT_ALIGN_CENTER};
laWidget _LA_WIDGET_METER_TYPE2={0,LA_TEXT_ALIGN_CENTER};
laWidget _LA_WIDGET_VALUE_METER_2D={0,LA_TEXT_ALIGN_CENTER};
laWidget _LA_WIDGET_FLOAT={0};
laWidget _LA_WIDGET_FLOAT_PLAIN={0, LA_UI_FLAGS_PLAIN};
laWidget _LA_WIDGET_FLOAT_COLOR={0};
laWidget _LA_WIDGET_FLOAT_COLOR_HCY={0};
laWidget _LA_WIDGET_ENUM_SELECTOR={0};
laWidget _LA_WIDGET_ENUM_SELECTOR_ICON={0, LA_UI_FLAGS_ICON};
laWidget _LA_WIDGET_ENUM_CYCLE={0, LA_UI_FLAGS_CYCLE};
laWidget _LA_WIDGET_ENUM_CYCLE_ICON={0, LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_ICON};
laWidget _LA_WIDGET_ENUM_ICON_PLAIN={0, LA_UI_FLAGS_PLAIN|LA_UI_FLAGS_ICON};
laWidget _LA_WIDGET_ENUM_HIGHLIGHT={0, LA_UI_FLAGS_HIGHLIGHT|LA_UI_FLAGS_CYCLE|LA_TEXT_ALIGN_CENTER};
laWidget _LA_WIDGET_STRING={0};
laWidget _LA_WIDGET_STRING_PLAIN={0, LA_UI_FLAGS_PLAIN};
laWidget _LA_WIDGET_STRING_MONO_PLAIN={0, LA_TEXT_MONO|LA_UI_FLAGS_PLAIN};
laWidget _LA_WIDGET_STRING_MULTI={0, LA_TEXT_MONO};
laWidget _LA_WIDGET_MENU_ROOT={0};
laWidget _LA_WIDGET_ALIGN={0};
laWidget _LA_WIDGET_3D_VIEW={0};
laWidget _LA_WIDGET_2D_VIEW={0};
laWidget _LA_WIDGET_NODE_SOCKET={0};
laWidget _LA_WIDGET_HEIGHT_ADJUSTER={0};
laWidget _LA_WIDGET_RAW={0};
laWidget _LA_WIDGET_MAPPER={0};
laWidget _LA_WIDGET_IMAGE={0};
laWidget _LA_WIDGET_SCOPE={0};
laWidget _LA_WIDGET_KNOB={0,LA_UI_FLAGS_KNOB};
laWidget _LA_WIDGET_MOUSE_ACTION_REPORTER={0,LA_TEXT_ALIGN_CENTER};

laWidget *LA_WIDGET_FIXED_GROUP=&_LA_WIDGET_FIXED_GROUP;
laWidget *LA_WIDGET_TAB=&_LA_WIDGET_TAB;
laWidget *LA_WIDGET_COLLECTION=&_LA_WIDGET_COLLECTION;
laWidget *LA_WIDGET_COLLECTION_ITEM=&_LA_WIDGET_COLLECTION_ITEM;
laWidget *LA_WIDGET_COLLECTION_SELECTOR=&_LA_WIDGET_COLLECTION_SELECTOR;
laWidget *LA_WIDGET_COLLECTION_SINGLE=&_LA_WIDGET_COLLECTION_SINGLE;
laWidget *LA_WIDGET_CONDITION_TOGGLE=&_LA_WIDGET_CONDITION_TOGGLE;
laWidget *LA_WIDGET_COLUMN_ADJUSTER=&_LA_WIDGET_COLUMN_ADJUSTER;
laWidget *LA_WIDGET_COLUMN_VIEWER=&_LA_WIDGET_COLUMN_VIEWER;
laWidget *LA_WIDGET_BUTTON=&_LA_WIDGET_BUTTON;
laWidget *LA_WIDGET_BUTTON_NO_CONFIRM=&_LA_WIDGET_BUTTON_NO_CONFIRM;
laWidget *LA_WIDGET_LABEL=&_LA_WIDGET_LABEL;
laWidget *LA_WIDGET_INT=&_LA_WIDGET_INT;
laWidget *LA_WIDGET_INT_PLAIN=&_LA_WIDGET_INT_PLAIN;
laWidget *LA_WIDGET_INT_PLAIN_ICON=&_LA_WIDGET_INT_PLAIN_ICON;
laWidget *LA_WIDGET_METER_TYPE1=&_LA_WIDGET_METER_TYPE1;
laWidget *LA_WIDGET_METER_TYPE2=&_LA_WIDGET_METER_TYPE2;
laWidget *LA_WIDGET_VALUE_METER_2D=&_LA_WIDGET_VALUE_METER_2D;
laWidget *LA_WIDGET_FLOAT=&_LA_WIDGET_FLOAT;
laWidget *LA_WIDGET_FLOAT_PLAIN=&_LA_WIDGET_FLOAT_PLAIN;
laWidget *LA_WIDGET_FLOAT_COLOR=&_LA_WIDGET_FLOAT_COLOR;
laWidget *LA_WIDGET_FLOAT_COLOR_HCY=&_LA_WIDGET_FLOAT_COLOR_HCY;
laWidget *LA_WIDGET_ENUM_SELECTOR=&_LA_WIDGET_ENUM_SELECTOR;
laWidget *LA_WIDGET_ENUM_SELECTOR_ICON=&_LA_WIDGET_ENUM_SELECTOR_ICON;
laWidget *LA_WIDGET_ENUM_CYCLE=&_LA_WIDGET_ENUM_CYCLE;
laWidget *LA_WIDGET_ENUM_CYCLE_ICON=&_LA_WIDGET_ENUM_CYCLE_ICON;
laWidget *LA_WIDGET_ENUM_ICON_PLAIN=&_LA_WIDGET_ENUM_ICON_PLAIN;
laWidget *LA_WIDGET_ENUM_HIGHLIGHT=&_LA_WIDGET_ENUM_HIGHLIGHT;
laWidget *LA_WIDGET_STRING=&_LA_WIDGET_STRING;
laWidget *LA_WIDGET_STRING_PLAIN=&_LA_WIDGET_STRING_PLAIN;
laWidget *LA_WIDGET_STRING_MULTI=&_LA_WIDGET_STRING_MULTI;
laWidget *LA_WIDGET_STRING_MONO_PLAIN=&_LA_WIDGET_STRING_MONO_PLAIN;
laWidget *LA_WIDGET_MENU_ROOT=&_LA_WIDGET_MENU_ROOT;
laWidget *LA_WIDGET_ALIGN=&_LA_WIDGET_ALIGN;
laWidget *LA_WIDGET_3D_VIEW=&_LA_WIDGET_3D_VIEW;
laWidget *LA_WIDGET_2D_VIEW=&_LA_WIDGET_2D_VIEW;
laWidget *LA_WIDGET_NODE_SOCKET=&_LA_WIDGET_NODE_SOCKET;
laWidget *LA_WIDGET_HEIGHT_ADJUSTER=&_LA_WIDGET_HEIGHT_ADJUSTER;
laWidget *LA_WIDGET_RAW=&_LA_WIDGET_RAW;
laWidget *LA_WIDGET_MAPPER=&_LA_WIDGET_MAPPER;
laWidget *LA_WIDGET_IMAGE=&_LA_WIDGET_IMAGE;
laWidget *LA_WIDGET_SCOPE=&_LA_WIDGET_SCOPE;
laWidget *LA_WIDGET_KNOB=&_LA_WIDGET_KNOB;
laWidget *LA_WIDGET_MOUSE_ACTION_REPORTER=&_LA_WIDGET_MOUSE_ACTION_REPORTER;

//============================================== [Dimension]

int la_ArrayGetHeight(laUiItem *ui){
    return laGetArrayLength(&ui->PP);
}
int la_ValueGetHeight(laUiItem*ui){
    int rows = ui->Extent; if(!rows){ rows=1; }
    if(ui->Flags&LA_UI_FLAGS_TRANSPOSE){ return la_ArrayGetHeight(ui)*rows; }
    return rows;
}
int la_ValueMeterGetHeight(laUiItem*ui){
    if(ui->Flags&LA_UI_FLAGS_TRANSPOSE){ return ui->Extent?ui->Extent:6; }
    return laGetArrayLength(&ui->PP);
}
int la_ValueMeter2DGetHeight(laUiItem*ui){
    return ui->Extent?ui->Extent:(ui->Extent=(ui->TR-ui->TL)/LA_RH);
}
int la_CanvasGetHeight(laUiItem *ui){
    return ui->Expand;
}
int la_EnumGetHeight(laUiItem *ui){
    // norm      e1
    // expand    e1 e2 e3
    // expand vertical  (vertical)
    // arr expand    e1 e2 e3
    //               e1 e2 e3
    // arr exp+vert  e1 e1
    //               e2 e2
    //               e3 e3
    int IsVertical = ui->Flags&LA_UI_FLAGS_TRANSPOSE;
    int IsExpand = ui->Flags&LA_UI_FLAGS_EXPAND;
    int IsCycle = ui->Flags&LA_UI_FLAGS_CYCLE;
    int ArrLen = laGetArrayLength(&ui->PP);
    if(ArrLen==1){ if(!IsExpand) IsVertical=0; }else{ IsExpand=1; }
    int EnumLen = (IsExpand&&(!IsCycle)) ? laGetEnumEntryLen(&ui->PP) : 1;
    if(ArrLen==1){
        if(IsVertical&&IsExpand) return laGetEnumEntryLen(&ui->PP);
        return 1;
    }else{
        if(IsVertical) return EnumLen;
        else return ArrLen;
    }
}
int la_ColorPickerGetHeight(laUiItem *ui){
    if(!ui->Extent) ui->Extent=7;
    return ui->Extent;
}
int la_GroupGetHeight(laUiItem *ui){
    if (ui->State == LA_UI_ACTIVE) return 0;
    return LA_RH;
}
int la_StringPropGetHeight(laUiItem *ui){
    laBoxedTheme *bt = *ui->Type->Theme;
    if(!(ui->Flags&LA_TEXT_LINE_WRAP)) return 1;
    char _buf[LA_RAW_CSTR_MAX_LEN]={0}; char* buf=_buf;
    laGetString(&ui->PP, _buf, &buf); int rows=0;
    int strw=tnsStringGetDimension(buf, 0, 0, ui->TR-ui->TL, &rows, ui->Flags&LA_TEXT_MONO);
    return rows;
}
int la_LabelHeight(laUiItem *ui){
    int Wrap=ui->Flags&(LA_TEXT_USE_NEWLINE|LA_TEXT_LINE_WRAP);
    if(!Wrap) return 1;
    int rows;
    tnsStringGetDimension(transLate(ui->Display->Ptr), 0, 0, ui->TR-ui->TL, &rows, ui->Flags&LA_TEXT_MONO);
    return rows;
}
int la_SocketGetHeight(laUiItem *ui){
    if(ui->Flags&(LA_UI_SOCKET_LABEL_N|LA_UI_SOCKET_LABEL_S))return 2; return 1;
}
int la_ImageGetHeight(laUiItem *ui){
    tnsImage* im=ui->Extra;
    if(ui->Flags&LA_UI_IMAGE_FULL_W){
        if(!im->Texture){ return 1; }
        int W=im->Texture->Width, H=im->Texture->Height;
        real CW=MAIN.CurrentWindow->CW-LA_RH*4, CH=MAIN.CurrentWindow->CH-LA_RH*4;
        real ra=1;
        if(W>CW){ real r=W/CW; ra=TNS_MAX2(r,ra); }
        if(H>CH){ real r=H/CH; ra=TNS_MAX2(r,ra); }
        W/=ra; H/=ra;
        return H/LA_RH+1;
    }
    if(ui->Extent) return ui->Extent;
    return 1;
}
int la_VariedGetHeight(laUiItem *ui){
    return ui->Extent;
}

int la_ColorSelectorGetMinWidth(laUiItem *ui){
    if(ui->Flags&LA_UI_FLAGS_ICON) return LA_RH;
    return 5*LA_RH;
}
int la_ValueGetMinWidth(laUiItem *ui){
    int ExtraW=0;
    if(ui->Flags&LA_UI_FLAGS_NO_LABEL){ return LA_RH; }
    if(ui->Flags&LA_UI_FLAGS_KNOB){ return LA_RH*TNS_MAX2(1,ui->Extent); }
    if((ui->Flags&LA_UI_FLAGS_EXPAND)&&(!(ui->Flags&LA_UI_FLAGS_NO_LABEL))){
        ExtraW+=tnsStringGetWidth(transLate(ui->PP.LastPs->p->Name),0,0);
    }
    return LA_RH*4+ExtraW;
}
int la_LabelGetMinWidth(laUiItem *ui){
    laBoxedTheme *bt = *ui->Type->Theme;
    if(ui->Flags&LA_UI_MIN_WIDTH){ return LA_RH; }
    int strw=tnsStringGetWidth(transLate(SSTR(ui->Display)), 0, ui->Flags&LA_TEXT_MONO);
    if(ui->Type==_LA_UI_MENU_ROOT && strw<LA_RH)strw=LA_RH;
    return (strw + LA_M + LA_M);
}
int la_StringPropGetMinWidth(laUiItem *ui){
    laBoxedTheme *bt = *ui->Type->Theme;
    if(ui->Flags&LA_UI_MIN_WIDTH){ return LA_RH; }
    char _buf[LA_RAW_CSTR_MAX_LEN]={0}; int ExtraW=0; char* buf=_buf;
    if(ui->Type == _LA_UI_STRING_MULTI){ExtraW=2*LA_RH+LA_M;}
    laGetString(&ui->PP, _buf, &buf); int rows=0;
    int strw=tnsStringGetDimension(buf, 0, 0, 0, &rows, ui->Flags&LA_TEXT_MONO);
    int Plain = ui->Flags&LA_UI_FLAGS_PLAIN;
    if((!Plain)&&strw<LA_RH*4){strw=LA_RH*4;}
    return (strw + LA_M + LA_M+ExtraW);
}
int la_EnumGetMinWidth(laUiItem *ui){
    laBoxedTheme *bt = *ui->Type->Theme;
    laEnumProp *ep = ui->PP.LastPs->p;
    laEnumItem *i;
    int W = 0; int tW=0;
    int IsVertical = ui->Flags&LA_UI_FLAGS_TRANSPOSE;
    int IsExpand = ui->Flags&LA_UI_FLAGS_EXPAND;
    int IsCycle = ui->Flags&LA_UI_FLAGS_CYCLE;
    int IsIcon = ui->Flags&LA_UI_FLAGS_ICON;
    int Highlight=ui->Flags&LA_UI_FLAGS_HIGHLIGHT;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int SharedWidth;
    if(!IsIcon){
        SharedWidth = LA_M + LA_M + ((IsCycle||IsExpand||NoDecal)?0:LA_RH); int HasIcon=0;
        if(Highlight){ int ico=0;
            if (ui->ExtraInstructions){
                if (ui->Type->OperatorType->ParseArgs){
                    char buf[256]; buf[0]=0;
                    ui->Type->OperatorType->ParseArgs(0,ui->Instructions, &ico, buf);
                    tW = tnsStringGetWidth(transLate(buf), 0, ui->Flags&LA_TEXT_MONO) + SharedWidth;
                }
            }else{
                for (i = ep->Items.pFirst; i; i = i->Item.pNext){ if (i->IconID){ HasIcon=1; } }
                tW = tnsStringGetWidth(transLate(ep->Base.Name), 0, ui->Flags&LA_TEXT_MONO) + SharedWidth;
            }
            if (tW > W) W = tW;
            if(HasIcon||ico) W+=LA_M+LA_RH;
        }else{
            for (i = ep->Items.pFirst; i; i = i->Item.pNext){
                tW = tnsStringGetWidth(transLate(i->Name), 0, ui->Flags&LA_TEXT_MONO) + SharedWidth;
                if (i->IconID){ HasIcon=1; }
                if (tW > W) W = tW;
            }
            if(HasIcon) W+=LA_M+LA_RH;
        }
    }else{
        W = LA_RH;
    }
    int ArrLen = laGetArrayLength(&ui->PP);
    if(ArrLen==1){ if(!IsExpand) IsVertical=0; }else{ IsExpand=1; }
    int EnumLen = (IsExpand&&(!IsCycle)) ? laGetEnumEntryLen(&ui->PP) : 1;
    if(ArrLen==1){
        if(!IsVertical) return W*EnumLen;
        return W;
    }else{
        if(IsVertical) return ArrLen*W;
        return EnumLen*W;
    }
    return W;
}
int la_ButtonGetMinWidth(laUiItem *ui){
    laBoxedTheme *bt = *ui->Type->Theme;
    uint32_t IconID;
    char buf[LA_RAW_CSTR_MAX_LEN]={0};
    char *label;
    laOperatorProp *ap;
    int IconOnly=ui->Flags&LA_UI_FLAGS_ICON;

    if(IconOnly) return LA_RH;

    if (ui->PP.LastPs && ui->PP.LastPs->p){
        ap = ui->PP.LastPs->p;
        label = transLate(ap->Base.Name);
        if (!ap->OperatorType) ap->OperatorType = laGetOperatorType(ap->OperatorID);
        IconID = ap->Base.IconID ? ap->Base.IconID : ap->OperatorType->IconID;
    }else{
        label = transLate(ui->AT->Name);
        IconID = ui->AT->IconID;
    }

    if (ui->ExtraInstructions &&ui->AT && ui->AT->ParseArgs){
        ui->AT->ParseArgs(0,ui->Instructions, &IconID, buf);
        if (buf[0]) label = transLate(buf);
    }elif (ui->PP.LastPs && ui->PP.LastPs->p->PropertyType == LA_PROP_OPERATOR){
        laOperatorProp *ap = ui->PP.LastPs->p;
        if (ap && ap->OperatorType && ap->OperatorType->ParseArgs){
            ap->OperatorType->ParseArgs(ui->PP.RawThis, ui->Instructions, &IconID, buf);
            if (buf[0]) label = transLate(buf);
        }
    }

    int strw=tnsStringGetWidth(label, 0, ui->Flags&LA_TEXT_MONO);
    if(strw){ strw+=LA_M+LA_M; }

    return ((IconID ? (LA_RH) : 0) + strw);
}
int la_CollectionSimpleGetMinWidth(laUiItem *ui){
    laBoxedTheme *bt = *ui->Type->Theme;
    int Simple=ui->Flags&LA_UI_COLLECTION_SIMPLE_SELECTOR;
    int Expand=ui->Expand||(ui->Flags&LA_UI_FLAGS_EXPAND);
    int w=LA_RH+((Simple&&Expand)?(tnsStringGetWidth("Select",0,0)+LA_M+LA_M):0);
    return w;
}
int la_ColumnViewerGetMinWidth(laUiItem *ui){
    return 350;
}
int la_ColorPickerGetMinWidth(laUiItem *ui){
    return LA_RH * 9;
}
int la_SocketGetMinWidth(laUiItem *ui){
    if(ui->Flags&(LA_UI_SOCKET_LABEL_W|LA_UI_SOCKET_LABEL_E))return LA_RH*3;
    if(ui->Flags&(LA_UI_SOCKET_LABEL_S|LA_UI_SOCKET_LABEL_N))return LA_RH*2;
    return LA_RH;
}
int la_ImageGetMinWidth(laUiItem *ui){
    tnsImage* im=ui->Extra;
    if(!im->Texture) return LA_RH;
    int W=im->Texture->Width, H=im->Texture->Height;
    int UseW=LA_RH;
    if(ui->Flags&LA_UI_IMAGE_FULL_W) UseW=W;
    else UseW=(int)((real)LA_RH*ui->Extent/H*W);
    real CW=MAIN.CurrentWindow->CW-LA_RH*4, CH=MAIN.CurrentWindow->CH-LA_RH*4;
    real ra=1;
    if(UseW>CW){ real r=UseW/CW; ra=TNS_MAX2(r,ra); }
    if(H>CH){ real r=H/CH; ra=TNS_MAX2(r,ra); }
    UseW/=ra; H/=ra;
    return UseW;
}
int la_MouseActionReporterMinWidth(laUiItem *ui){
    laBoxedTheme *bt = *ui->Type->Theme;
    if(ui->Flags&LA_UI_MIN_WIDTH){ return LA_RH; }
    if(!(SSTR(ui->Display)[0])) return 3*LA_RH;
    int strw=tnsStringGetWidth(transLate(SSTR(ui->Display)), 0, ui->Flags&LA_TEXT_MONO);
    if(ui->Type==_LA_UI_MENU_ROOT && strw<LA_RH)strw=LA_RH;
    return (strw + LA_M + LA_M);
}


//============================================== [Draw]

void la_DrawBorder(int L, int R, int U, int B){
    tnsVertex2d(L+0.5f,U+0.5f); tnsVertex2d(R-0.5f,U+0.5f); tnsVertex2d(R-0.5f,B-0.5f); tnsVertex2d(L+0.5f,B-0.5f); tnsPackAs(GL_LINE_LOOP); 
}
void la_DrawBox(int L, int R, int U, int B){
    tnsVertex2d(L,U); tnsVertex2d(R,U); tnsVertex2d(R,B); tnsVertex2d(L,B); tnsPackAs(GL_TRIANGLE_FAN); 
}
void la_DrawOuterShadowLength(int L, int R, int U, int B, int sw){
    tnsVertex2d(L+sw,B); tnsVertex2d(L+sw,B+sw); tnsVertex2d(R,B); tnsVertex2d(R+sw,B+sw);
    tnsVertex2d(R,U+sw); tnsVertex2d(R+sw,U+sw); tnsPackAs(GL_TRIANGLE_STRIP);
}
void la_DrawOuterShadow(int L, int R, int U, int B){
    la_DrawOuterShadowLength(L,R,U,B,LA_SHADOW_W);
}
void la_DrawInnerShadowLength(int L, int R, int U, int B, int sw){
    tnsVertex2d(L,B); tnsVertex2d(L+sw,B); tnsVertex2d(L,U), tnsVertex2d(L+sw,U+sw);
    tnsVertex2d(R,U); tnsVertex2d(R,U+sw); tnsPackAs(GL_TRIANGLE_STRIP);
}
void la_DrawInnerShadow(int L, int R, int U, int B){
    la_DrawInnerShadowLength(L,R,U,B,LA_SHADOW_W);
}
void la_DrawBoxAutoArrayStyle(int L, int R, int U, int B, laBoxedTheme* bt, int State, real* ReplaceColor, int IsVertical, int IsFirst, int BoxStyle){
    int sw= LA_SHADOW_W; if(State == LA_UI_EDITING){ State=LA_BT_ACTIVE; } real* rc=ReplaceColor;
    int sL=((!IsVertical) && (!IsFirst))?-1:0, sU=(IsVertical && (!IsFirst))?-1:0;
    int sR=R, sB=B; if((!IsFirst) && (bt->BoxStyle)){ if(IsVertical){ sR=L+sw; }else{ sB=U+sw; } }
    switch(BoxStyle){
    default: case 0:{
        if(State==LA_BT_ACTIVE){
            tnsColor4dv(laThemeColor(bt,LA_BT_ACTIVE)); la_DrawBox(L,R,U,B);
            tnsColor4dv(laAccentColor(LA_BT_SHADOW));   la_DrawInnerShadow(L,sR,U,sB);
        }else{
            tnsColor4dv(rc?rc:laThemeColor(bt,State));  la_DrawBox(L,R,U,B);
        }
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER)); la_DrawBorder(L+sL,R,U+sU,B);
    }break;
    case 1:{
        if(State==LA_BT_ACTIVE){
            tnsColor4dv(laThemeColor(bt,LA_BT_ACTIVE)); la_DrawBox(L,R,U,B);
            tnsColor4dv(laThemeColor(bt,LA_BT_BORDER)); la_DrawBorder(L+sL,R,U+sU,B);
        }else{
            tnsColor4dv(laAccentColor(LA_BT_SHADOW));   la_DrawOuterShadow(L-sw,R-sw,U-sw,B-sw);
            tnsColor4dv(rc?rc:laThemeColor(bt,State));  la_DrawBox(L-sw,R-sw,U-sw,B-sw);
            tnsColor4dv(laThemeColor(bt,LA_BT_BORDER)); la_DrawBorder(L-sw+sL,R-sw,U-sw+sU,B-sw);
        }
    }break;
    case -1:{
        tnsColor4dv(rc?rc:laThemeColor(bt,State));  la_DrawBox(L,R,U,B);
        tnsColor4dv(laAccentColor(LA_BT_SHADOW));   la_DrawInnerShadow(L,sR,U,sB);
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER)); la_DrawBorder(L+sL,R,U+sU,B);
    }break;
    }
}
void la_DrawBoxAutoArray(int L, int R, int U, int B, laBoxedTheme* bt, int State, real* ReplaceColor, int IsVertical, int IsFirst){
    la_DrawBoxAutoArrayStyle(L,R,U,B,bt,State,ReplaceColor,IsVertical,IsFirst,bt->BoxStyle);
}
void la_DrawBoxAuto(int L, int R, int U, int B, laBoxedTheme* bt, int State, real* ReplaceColor){
    la_DrawBoxAutoArray(L,R,U,B,bt,State,ReplaceColor,0,1);
}
void la_DrawBoxAutoFillArray(int L, int R, int U, int B, laBoxedTheme* bt, int State, real* ReplaceColor, int IsVertical, int IsFirst){
    int sw= LA_SHADOW_W; if(State == LA_UI_EDITING){ State=LA_BT_ACTIVE; } real* rc=ReplaceColor;
    int sR=R, sB=B; if((!IsFirst) && (bt->BoxStyle)){ if(IsVertical){ sR=L+sw; }else{ sB=U+sw; } }
    switch(bt->BoxStyle){
    default: case 0:{
        if(State==LA_BT_ACTIVE){
            tnsColor4dv(laThemeColor(bt,LA_BT_ACTIVE)); la_DrawBox(L,R,U,B);
            tnsColor4dv(laAccentColor(LA_BT_SHADOW));   la_DrawInnerShadow(L,sR,U,sB);
        }else{
            tnsColor4dv(rc?rc:laThemeColor(bt,State));  la_DrawBox(L,R,U,B);
        }
    }break;
    case 1:{
        if(State==LA_BT_ACTIVE){
            tnsColor4dv(laThemeColor(bt,LA_BT_ACTIVE)); la_DrawBox(L,R,U,B);
        }else{
            tnsColor4dv(laAccentColor(LA_BT_SHADOW));   la_DrawOuterShadow(L-sw,R-sw,U-sw,B-sw);
            tnsColor4dv(rc?rc:laThemeColor(bt,State));  la_DrawBox(L-sw,R-sw,U-sw,B-sw);
        }
    }break;
    case -1:{
        tnsColor4dv(rc?rc:laThemeColor(bt,State));  la_DrawBox(L,R,U,B);
        tnsColor4dv(laAccentColor(LA_BT_SHADOW));   la_DrawInnerShadow(L,sR,U,sB);
    }break;
    }
}
void la_DrawBoxAutoFill(int L, int R, int U, int B, laBoxedTheme* bt, int State, real* ReplaceColor){
    la_DrawBoxAutoFillArray(L,R,U,B,bt,State,ReplaceColor,0,1);
}
void la_DrawBoxAutoBorderArray(int L, int R, int U, int B, laBoxedTheme* bt, int State, int IsVertical, int IsFirst){
    int sw= LA_SHADOW_W; if(State == LA_UI_EDITING){ State=LA_BT_ACTIVE; }
    int sL=((!IsVertical) && (!IsFirst))?-1:0, sU=(IsVertical && (!IsFirst))?-1:0;
    switch(bt->BoxStyle){
    default: case 0:{
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER)); la_DrawBorder(L+sL,R,U+sU,B);
    }break;
    case 1:{
        if(State==LA_BT_ACTIVE){
            tnsColor4dv(laThemeColor(bt,LA_BT_BORDER)); la_DrawBorder(L+sL,R,U+sU,B);
        }else{
            tnsColor4dv(laThemeColor(bt,LA_BT_BORDER)); la_DrawBorder(L-sw+sL,R-sw,U-sw+sU,B-sw);
        }
    }break;
    case -1:{
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER)); la_DrawBorder(L+sL,R,U+sU,B);
    }break;
    }
}
void la_DrawBoxAutoBorder(int L, int R, int U, int B, laBoxedTheme* bt, int State){
    la_DrawBoxAutoBorderArray(L,R,U,B,bt,State,0,1);
}
int la_GetBoxOffset(laBoxedTheme* bt, int State){
    int sw= LA_SHADOW_W;
    switch(bt->BoxStyle){
    default: case 0:
        if(State==LA_BT_ACTIVE || State==LA_UI_EDITING) return -sw;
        return 0;
    case 1:
        if(State==LA_BT_ACTIVE || State==LA_UI_EDITING) return 0;
        return sw;
    case -1:
        return 0;
    }
}

real* la_UiStateColorState(laBoxedTheme* bt, laUiItem* ui, int State){
    int flags = ui->Flags;
    if(flags&LA_UI_FLAGS_DISABLED){ return laThemeColor(bt, LA_BT_DISABLED); }
    if(flags&LA_UI_WARNING){ return laAccentColor(LA_BT_WARNING); }
    if(flags&LA_UI_FLAGS_HIGHLIGHT){ return laAccentColor(LA_BT_ACTIVE); }
    return laThemeColor(bt, State);
}
real* la_UiStateColor(laBoxedTheme* bt, laUiItem* ui){
    return la_UiStateColorState(bt,ui,ui->State);
}
real* la_UiTextColorPlain(laBoxedTheme* bt, laUiItem* ui){
    int flags = ui->Flags;
    if(flags&LA_UI_FLAGS_DISABLED){ return laThemeColor(bt, LA_BT_DISABLED|LA_BT_TEXT); }
    if(flags&LA_UI_FLAGS_WARNING){ return laAccentColor(LA_BT_WARNING); }
    if(flags&LA_UI_FLAGS_HIGHLIGHT){ return laAccentColor(LA_BT_ACTIVE|LA_BT_TEXT); }
    if(flags&LA_UI_FLAGS_NO_DECAL){ return laThemeColor(bt, LA_BT_NO_DECAL|LA_BT_TEXT); }
    return laThemeColor(bt,LA_BT_TEXT);
}
real* la_UiTextColorState(laBoxedTheme* bt, laUiItem* ui, int State){
    int flags = ui->Flags;
    if(flags&LA_UI_FLAGS_DISABLED){ return laThemeColor(bt, LA_BT_DISABLED|LA_BT_TEXT); }
    if(flags&LA_UI_FLAGS_NO_DECAL){ return laThemeColor(bt, LA_BT_NO_DECAL|LA_BT_TEXT); }
    return laThemeColor(bt,LA_BT_TEXT|State);
}
real* la_UiTextColor(laBoxedTheme* bt, laUiItem* ui){
    return la_UiTextColorState(bt,ui,ui->State);
}

void la_SingleLineStringDrawSelection(laUiItem *ui, int Begin, int U, laBoxedTheme *bt, uint32_t *str, laStringEdit *se);

void la_CollectionSelectorDraw(laUiItem *ui, int h){
    char temp[128] = {0};
    laBoxedTheme *bt = (*ui->Type->Theme);
    real Color[16] = {0};
    tnsVector4d Transp = {0};
    int CrossDraw = 0;
    int Simple=ui->Flags&LA_UI_COLLECTION_SIMPLE_SELECTOR;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    if(Simple){ CrossDraw=0; }

    if (!ui->State) ui->State = LA_UI_NORMAL;

    int sw=0;
    if(!NoDecal){
        sw=la_GetBoxOffset(bt,ui->State);
        tnsUseNoTexture();
        la_DrawBoxAutoFill(ui->L,ui->R,ui->U,ui->B,bt,ui->State,0);
    }

    if(!Simple){
        CrossDraw = laGetActiveInstanceStrict(ui->PP.LastPs->p, ui->PP.LastPs->UseInstance) ? 1 : 0;
    }

    if (CrossDraw){
        tnsVectorCopy4d(laThemeColor(bt,LA_BT_BORDER), Transp);
        Transp[3] = 0;

        tnsMakeLinerGradient4dv(Color, 2, Transp, laThemeColor(bt,LA_BT_BORDER));
        tnsMakeLinerGradient4dv(&Color[8], 2, Transp, laThemeColor(bt,LA_BT_BORDER));
        //tnsMakeLinerGradient4dv(&Color[8], 2, laThemeColor(bt, LA_BT_BORDER), Transp);
        int usw=sw; if(usw<0){ usw=0; }
        tnsVertex2d(ui->R - LA_RH-usw, ui->U-usw); tnsVertex2d(ui->R-usw, ui->U-usw);
        tnsVertex2d(ui->R - LA_RH-usw, ui->B-usw); tnsVertex2d(ui->R-usw, ui->B-usw);
        tnsColorArray4d(Color, 4);
        tnsPackAs(GL_TRIANGLE_STRIP);
    }

    la_DrawBoxAutoBorder(ui->L,ui->R,ui->U,ui->B,bt,ui->State);

    if(Simple){
        int R=ui->R; if(ui->Expand || (ui->Flags&LA_UI_FLAGS_EXPAND)){
            R=ui->L+LA_RH; tnsDrawStringAuto(transLate("Select"), laThemeColor(bt,ui->State|LA_BT_TEXT), R+LA_M-sw, ui->R-sw, ui->U-sw, 0);
        }
        tnsDrawStringAuto("⯆", laThemeColor(bt,ui->State|LA_BT_TEXT), ui->L-sw, R-sw, ui->U-sw, LA_TEXT_ALIGN_CENTER);
    }else{
        if (CrossDraw){
            tnsDrawStringAuto("🧹", laThemeColor(bt,ui->State|LA_BT_TEXT), ui->R - LA_RH-sw, ui->R-sw, ui->U-sw, 0);
        }else{
            tnsDrawStringAuto("?", laThemeColor(bt, LA_BT_BORDER), ui->L-sw, ui->L+LA_RH-sw, ui->U-sw, LA_TEXT_ALIGN_CENTER);
        }
    }
}
void la_EmptyDraw(laUiItem *ui, int h){
    char temp[128] = {0};
    laBoxedTheme *bt = (*ui->Type->Theme);
    if (!ui->State) ui->State = LA_UI_NORMAL;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;

    if(!NoDecal){
        tnsUseNoTexture();
        la_DrawBoxAuto(ui->L,ui->R,ui->U,ui->B,bt,ui->State,0);
    }
}
void la_GroupDraw(laUiItem *ui, int h){
    char temp[128] = {0};
    laBoxedTheme *bt = (*ui->Type->Theme);
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;

    if(!NoDecal){
        tnsUseNoTexture();
        la_DrawBoxAutoArrayStyle(ui->L,ui->R,ui->U,ui->B,bt,LA_BT_NORMAL,0,0,1,-1);
    }

    if (ui->State != LA_UI_ACTIVE){
        tnsDrawStringAuto(ui->Page ? transLate(ui->Page->TabName->Ptr) : transLate("No Page"),
            laThemeColor(bt,ui->State|LA_BT_TEXT), ui->L+LA_M, ui->R+LA_M, ui->U, LA_TEXT_ALIGN_CENTER);
    }
}
void la_TabDraw(laUiItem *ui, int h){
    char temp[128] = {0};
    laBoxedTheme *bt = (*ui->Type->Theme);
    laUiList *uil; int count = 0, page = 0;real w, l = ui->L;

    for (uil = ui->Subs.pFirst; uil; uil = uil->Item.pNext){ count++; }
    w = (real)(ui->R - ui->L) / (count == 0 ? 1 : count);

    real* bkg=laThemeColor(bt,LA_BT_NORMAL);
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    if(!NoDecal){
        tnsUseNoTexture();
        tnsColor4d(LA_COLOR3(bkg),bkg[3]*0.7);
        tnsVertex2d(ui->L, ui->U); tnsVertex2d(ui->R, ui->U);
        tnsVertex2d(ui->R, ui->B); tnsVertex2d(ui->L, ui->B);
        tnsPackAs(GL_TRIANGLE_FAN);
        real* bc=laThemeColor(bt,LA_BT_BORDER); real bcolor[24];
        tnsVectorCopy4d(bc,bcolor); tnsVectorSet4(&bcolor[4],bc[0],bc[1],bc[2],bc[3]*0.6);
        tnsVectorCopy4d(&bcolor[4],&bcolor[8]);
        tnsVectorCopy4d(&bcolor[4],&bcolor[12]);
        tnsVectorCopy4d(&bcolor[4],&bcolor[16]);
        tnsVectorCopy4d(bcolor,&bcolor[20]);
        tnsVertex2d(ui->L+0.5, ui->U+h-0.5); tnsVertex2d(ui->L+0.5, TNS_MIN2(ui->U+h*3,ui->B)-0.5); tnsVertex2d(ui->L+0.5, ui->B-0.5);
        tnsVertex2d(ui->R-0.5, ui->B-0.5); tnsVertex2d(ui->R-0.5, TNS_MIN2(ui->U+h*3,ui->B)-0.5); tnsVertex2d(ui->R-0.5, ui->U+h-0.5);
        tnsColorArray4d(bcolor,6);
        tnsPackAs(GL_LINE_STRIP);
    }

    for (uil = ui->Subs.pFirst; uil; uil = uil->Item.pNext){
        l = page*w; int IsThisPage=(uil==ui->Page);
        tnsUseNoTexture();
        if(!IsThisPage){
            tnsColor4dv(laThemeColor(bt,LA_BT_ACTIVE));
            tnsVertex2d(ui->L+l, ui->U); tnsVertex2d(ui->L+l+w, ui->U);
            tnsVertex2d(ui->L+l+w, ui->U+h); tnsVertex2d(ui->L+l, ui->U+h);
            tnsPackAs(GL_TRIANGLE_FAN);
        }
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        int ss=1; if(!uil->Item.pNext){ss=-1;}
        tnsVertex2d(ui->L+l+0.5, ui->U+h+0.5); tnsVertex2d(ui->L+l+0.5, ui->U+0.5);
        tnsVertex2d(ui->L+l+w+0.5*ss, ui->U+0.5); tnsVertex2d(ui->L+l+w+0.5*ss, ui->U+h+0.5);
        tnsPackAs(IsThisPage?GL_LINE_STRIP:GL_LINE_LOOP);
        tnsDrawStringAuto(transLate(uil->TabName->Ptr), laThemeColor(bt,IsThisPage?LA_BT_BORDER:LA_BT_TEXT), ui->L+l+LA_M, ui->L+l+w-LA_M, ui->U, LA_TEXT_ALIGN_CENTER);
        page++;
    }
    tnsFlush();
}

void la_IntDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    int Data[32];
    int Len, i, Seg, W;
    char buf[48] = {0};
    char buf2[48] = {0};
    char prefix[8][64] = {0};
    int Original=0;
    int Ranged;
    int min, max;
    int s, State;
    int IsVertical=ui->Flags&LA_UI_FLAGS_TRANSPOSE;
    int IsIcon=ui->Flags&LA_UI_FLAGS_ICON;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int NoLabel=ui->Flags&LA_UI_FLAGS_NO_LABEL;
    int IsKnob = ui->Flags&LA_UI_FLAGS_NODE_CONTAINER;
    int SliderVertical = ui->Flags&LA_UI_FLAGS_VERTICAL_SLIDER;
    uint64_t sf=(SliderVertical?LA_TEXT_ROTATE_N:0) | ( bt->TextShadow?LA_TEXT_SHADOW:0);

    if (laIsPropertyReadOnly(&ui->PP) && !NoDecal) ui->State = LA_BT_DISABLED;

    laGetIntArray(&ui->PP, Data);
    Len = laGetArrayLength(&ui->PP);

    laGetPrefixP(&ui->PP, prefix);
    Ranged = laGetIntRange(&ui->PP, &min, &max);

    for (i = 0; i < Len; i++){
        int _L=ui->L, _R=ui->R, _U=ui->U, _B=ui->B; int sw;
        if(IsVertical){
            Seg = ui->R - ui->L;
            _U = ui->U + i * LA_RH; _B = _U+LA_RH;
        }else{
            Seg = (ui->R - ui->L) / Len;
            _L = ui->L + i * Seg; _R=_L+Seg;
        }

        if(IsIcon){
            sprintf(buf, "%lc", laGetInt(&ui->PP));
        }else{
            buf[0] = '\0'; buf2[0] = '\0';
            if(Len==1){ strcat(buf2, transLate(ui->PP.LastPs->p->Name)); }
            if(i<8)strcat(Len==1?buf:buf2,&prefix[i]);
            strPrintIntAfter(buf, 48, Data[i]);
            strAppend(buf, ui->PP.LastPs->p->Unit ? transLate(ui->PP.LastPs->p->Unit) : "");
        }

        if (ui->Extra && ui->Extra->On == i + 1){
            Original = ui->State;
            ui->State = LA_UI_ACTIVE;
        }
        
        sw=0;

        if(IsKnob && !ui->Extra->Edit){
            real ctrx = (real)(_L+_R)/2.0f, ctry=(real)(_U+_B)/2.0f, radius=TNS_MIN2((_B-_U)/2.0f,(_R-_L)/2.0f);
            tnsUseNoTexture();
            real dgx=0,dgy=0;
            if(radius*2>(_B-_U)-LA_RH/5){ dgx = radius/1.5; }
            if(radius*2>(_R-_L)-LA_RH/5){ dgy = radius/1.5; }

            tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
            tnsVertex2d(_L+0.5f, (int)ctry-dgy+0.5f); tnsVertex2d(_L+0.5f, _U+0.5f); tnsVertex2d(ctrx-(int)dgx+0.5f, _U+0.5f); tnsPackAs(GL_LINE_STRIP);
            tnsVertex2d(_R-0.5f, (int)ctry-dgy+0.5f); tnsVertex2d(_R-0.5f, _U+0.5f); tnsVertex2d(ctrx+(int)dgx-0.5f, _U+0.5f); tnsPackAs(GL_LINE_STRIP);
            tnsVertex2d(_R-0.5f, (int)ctry+dgy-0.5f); tnsVertex2d(_R-0.5f, _B-0.5f); tnsVertex2d(ctrx+(int)dgx-0.5f, _B-0.5f); tnsPackAs(GL_LINE_STRIP);
            tnsVertex2d(_L+0.5f, (int)ctry+dgy-0.5f); tnsVertex2d(_L+0.5f, _B-0.5f); tnsVertex2d(ctrx-(int)dgx+0.5f, _B-0.5f); tnsPackAs(GL_LINE_STRIP);
                
            real verts[52];//24*2+ctr+overlap
            tnsMakeCircle2d(&verts[2],24,ctrx,ctry,radius,0);
            tnsVectorCopy2d(&verts[2],&verts[50]);
            verts[0]=ctrx; verts[1]=ctry;
            tnsColor4dv(laThemeColor(bt,ui->State));
            tnsVertexArray2d(verts,26); tnsPackAs(GL_TRIANGLE_FAN);
            tnsMakeCircle2d(verts,24,ctrx,ctry,radius,0);
            tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
            tnsVertexArray2d(verts,24); tnsPackAs(GL_LINE_LOOP);
            if(Ranged){
                real arc[52];  int arcindex[26]; // 12 slices 13 ends
                real range = (real)(Data[i] - min) / (real)(max - min) * TNS_PI * 1.5;
                real radius2=radius-0.2*LA_RH; if(radius2<LA_RH*0.1){ radius2=LA_RH*0.1; }
                tnsMakeArc2d(arc,12,ctrx,ctry,radius,TNS_PI*2.75, TNS_PI*2.75 + range);
                tnsMakeArc2d(&arc[26],12,ctrx,ctry,radius2,TNS_PI*2.75, TNS_PI*2.75 + range);
                tnsMakeBridgedIndex(arcindex, 13, 0, 0);
                real *color = laThemeColor(bt,LA_BT_TEXT|ui->State); //tnsColor4d(LA_COLOR3(color),0.3);
                tnsColor4dv(color);
                tnsVertexArray2d(arc,26); tnsIndexArray(arcindex,26); tnsPackAs(GL_TRIANGLE_STRIP);
                tnsMakeArc2d(&arc[2],12,ctrx,ctry,radius2,TNS_PI*2.75, TNS_PI*4.25);
                arc[0]=ctrx+cos(TNS_PI*2.75)*radius; arc[1]=ctry+sin(TNS_PI*2.75)*radius;
                arc[28]=ctrx+cos(TNS_PI*0.25)*radius; arc[29]=ctry+sin(TNS_PI*0.25)*radius;
                tnsVertexArray2d(arc,15);
                tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
                tnsPackAs(GL_LINE_STRIP);
            }
        }else{
            if(!NoDecal){
                sw=la_GetBoxOffset(bt,ui->State);
                tnsUseNoTexture();
                la_DrawBoxAutoFillArray(_L,_R,_U,_B,bt,ui->State,la_UiStateColor(bt,ui),IsVertical,i==0);

                if (Ranged){
                    int L1 = _L;
                    int R1 = SliderVertical?_R:((real)(Data[i] - min) / (real)(max - min) * (real)(Seg) + L1);
                    int U1 = SliderVertical?(_B - (real)(Data[i] - min) / (real)(max - min) * (real)(_B-_U)):_U;
                    tnsUseNoTexture(); real* color=laThemeColor(bt,LA_BT_TEXT);
                    tnsColor4d(LA_COLOR3(color),0.3);
                    la_DrawBox(L1-sw,R1-sw,U1-sw,_B-sw);
                    tnsPackAs(GL_TRIANGLE_FAN);
                }

                la_DrawBoxAutoBorderArray(_L,_R,_U,_B,bt,ui->State,IsVertical,i==0);
            }
            int TL, TR, TU;
            if(!SliderVertical){ TL=_L+LA_M; TR=_R-LA_M; TU=_U; }
            else{ TL=_L; TR=TL+(_B-_U)-LA_M*2; TU=_B-LA_M-LA_RH; }
            TL-=sw; TR-=sw; TU-=sw;
            if (ui->Extra->On == i + 1 && ui->Extra->Edit){
                uint32_t *buf = strGetCursorLine(ui->Extra->Edit, 0)->Buf;
                int LL = _L + (Seg - tnsStringGetWidthU(buf, 0, ui->Flags&LA_TEXT_MONO)) / 2;
                tnsColor4dv(laThemeColor(bt, LA_BT_TEXT));
                la_SingleLineStringDrawSelection(ui, TL,TU, bt, buf, ui->Extra->Edit);

                tnsDrawStringM(0, buf, laThemeColor(bt, LA_BT_TEXT_ACTIVE), TL,TR,TU, ui->Flags|sf);
            }else{
                tnsDrawStringWithPriority(NoLabel?0:buf2, buf, la_UiTextColor(bt,ui), 0, TL,TR,TU, ui->Flags|sf);//, ui->ExtraInstructions);
            }
        }

        if (ui->Extra && ui->Extra->On == i + 1) ui->State = Original;
    }

    tnsFlush();
}
void la_FloatDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    real Data[32];
    int Len, i, W, Seg;
    char buf[48] = {0};
    char buf2[48] = {0};
    char prefix[8][64] = {0};
    int Original;
    int Ranged;
    real min, max;
    int s, State;
    int IsVertical=ui->Flags&LA_UI_FLAGS_TRANSPOSE;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int IsRad=ui->PP.LastPs->p->IsRadAngle;
    int NoLabel=ui->Flags&LA_UI_FLAGS_NO_LABEL;
    int IsKnob = ui->Flags&LA_UI_FLAGS_NODE_CONTAINER;
    int SliderVertical = ui->Flags&LA_UI_FLAGS_VERTICAL_SLIDER;
    uint64_t sf=(SliderVertical?LA_TEXT_ROTATE_N:0) | ( bt->TextShadow?LA_TEXT_SHADOW:0);

    if (laIsPropertyReadOnly(&ui->PP) && !NoDecal) ui->State = LA_BT_DISABLED;

    laGetFloatArray(&ui->PP, Data);
    Len = laGetArrayLength(&ui->PP);

    laGetPrefixP(&ui->PP, prefix);
    Ranged = laGetFloatRange(&ui->PP, &min, &max);

    for (i = 0; i < Len; i++){
        int _L=ui->L, _R=ui->R, _U=ui->U, _B=ui->B; int sw;
        if(IsVertical){
            Seg = ui->R - ui->L;
            _U = ui->U + i * LA_RH; _B = _U+LA_RH;
        }else{
            Seg = (ui->R - ui->L) / Len;
            _L = ui->L + i * Seg; _R=_L+Seg;
        }

        buf[0] = '\0'; buf2[0] = '\0';
        if(Len==1){ strcat(buf2, transLate(ui->PP.LastPs->p->Name)); }
        if(i<8)strcat(Len==1?buf:buf2, &prefix[i]);
        strPrintFloatAfter(buf, 48, 3, IsRad?deg(Data[i]):Data[i]);
        strAppend(buf, ui->PP.LastPs->p->Unit ? transLate(ui->PP.LastPs->p->Unit) : "");

        if (ui->Extra && ui->Extra->On == i + 1){
            Original = ui->State;
            ui->State = LA_UI_ACTIVE;
        }

        sw=0;

        if(IsKnob && !ui->Extra->Edit){
            real ctrx = (real)(_L+_R)/2.0f, ctry=(real)(_U+_B)/2.0f, radius=TNS_MIN2((_B-_U)/2.0f,(_R-_L)/2.0f);
            tnsUseNoTexture();
            real dgx=0,dgy=0;
            if(radius*2>(_B-_U)-LA_RH/5){ dgx = radius/1.5; }
            if(radius*2>(_R-_L)-LA_RH/5){ dgy = radius/1.5; }

            tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
            tnsVertex2d(_L+0.5f, (int)ctry-dgy+0.5f); tnsVertex2d(_L+0.5f, _U+0.5f); tnsVertex2d(ctrx-(int)dgx+0.5f, _U+0.5f); tnsPackAs(GL_LINE_STRIP);
            tnsVertex2d(_R-0.5f, (int)ctry-dgy+0.5f); tnsVertex2d(_R-0.5f, _U+0.5f); tnsVertex2d(ctrx+(int)dgx-0.5f, _U+0.5f); tnsPackAs(GL_LINE_STRIP);
            tnsVertex2d(_R-0.5f, (int)ctry+dgy-0.5f); tnsVertex2d(_R-0.5f, _B-0.5f); tnsVertex2d(ctrx+(int)dgx-0.5f, _B-0.5f); tnsPackAs(GL_LINE_STRIP);
            tnsVertex2d(_L+0.5f, (int)ctry+dgy-0.5f); tnsVertex2d(_L+0.5f, _B-0.5f); tnsVertex2d(ctrx-(int)dgx+0.5f, _B-0.5f); tnsPackAs(GL_LINE_STRIP);
            
            real verts[52];//24*2+ctr+overlap
            tnsMakeCircle2d(&verts[2],24,ctrx,ctry,radius,0);
            tnsVectorCopy2d(&verts[2],&verts[50]);
            verts[0]=ctrx; verts[1]=ctry;
            tnsColor4dv(laThemeColor(bt,ui->State));
            tnsVertexArray2d(verts,26); tnsPackAs(GL_TRIANGLE_FAN);
            tnsMakeCircle2d(verts,24,ctrx,ctry,radius,0);
            tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
            tnsVertexArray2d(verts,24); tnsPackAs(GL_LINE_LOOP);
            if(Ranged){
                real arc[52];  int arcindex[26]; // 12 slices 13 ends
                real range,rstart;
                real radius2=radius-0.2*LA_RH; if(radius2<LA_RH*0.1){ radius2=LA_RH*0.1; }
                if(min<0&&max>0){ rstart=TNS_PI*3.5; range = (real)(Data[i]) / (real)(max) * TNS_PI * 1.5 * max/(max-min);}
                else{ rstart=TNS_PI*2.75; range = (real)(Data[i] - min) / (real)(max - min) * TNS_PI * 1.5; }
                tnsMakeArc2d(arc,12,ctrx,ctry,radius,rstart, rstart + range);
                tnsMakeArc2d(&arc[26],12,ctrx,ctry,radius2,rstart, rstart + range);
                tnsMakeBridgedIndex(arcindex, 13, 0, 0);
                real *color = laThemeColor(bt,LA_BT_TEXT|ui->State); //tnsColor4d(LA_COLOR3(color),0.3);
                tnsColor4dv(color);
                tnsVertexArray2d(arc,26); tnsIndexArray(arcindex,26); tnsPackAs(GL_TRIANGLE_STRIP);
                tnsMakeArc2d(&arc[2],12,ctrx,ctry,radius2,TNS_PI*2.75, TNS_PI*4.25);
                arc[0]=ctrx+cos(TNS_PI*2.75)*radius; arc[1]=ctry+sin(TNS_PI*2.75)*radius;
                arc[28]=ctrx+cos(TNS_PI*0.25)*radius; arc[29]=ctry+sin(TNS_PI*0.25)*radius;
                tnsVertexArray2d(arc,15);
                tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
                tnsPackAs(GL_LINE_STRIP);
            }
        }else{
            if(!NoDecal){
                sw=la_GetBoxOffset(bt,ui->State);
                tnsUseNoTexture();
                la_DrawBoxAutoFillArray(_L,_R,_U,_B,bt,ui->State,0,IsVertical,i==0);

                if (Ranged){
                    int L1 = _L;
                    int R1 = SliderVertical?_R:((real)(Data[i] - min) / (real)(max - min) * (real)(Seg) + L1);
                    int U1 = SliderVertical?(_B - (real)(Data[i] - min) / (real)(max - min) * (real)(_B-_U)):_U;
                    tnsUseNoTexture(); real* color=laThemeColor(bt,LA_BT_TEXT);
                    tnsColor4d(LA_COLOR3(color),0.3);
                    tnsVertex2d(L1-sw, U1-sw); tnsVertex2d(R1-sw, U1-sw);
                    tnsVertex2d(R1-sw, _B-sw); tnsVertex2d(L1-sw, _B-sw);
                    tnsPackAs(GL_TRIANGLE_FAN);
                }

                la_DrawBoxAutoBorderArray(_L,_R,_U,_B,bt,ui->State,IsVertical,i==0);
            }
            int TL, TR, TU;
            if(!SliderVertical){ TL=_L+LA_M; TR=_R-LA_M; TU=_U; }
            else{ TL=_L; TR=TL+(_B-_U)-LA_M*2; TU=_B-LA_M-LA_RH; }
            TL-=sw; TR-=sw; TU-=sw;
            if (ui->Extra->On == i + 1 && ui->Extra->Edit){
                uint32_t *buf = strGetCursorLine(ui->Extra->Edit,0)->Buf;
                int LL = _L + (Seg - tnsStringGetWidthU(buf, 0, ui->Flags&LA_TEXT_MONO)) / 2;
                tnsColor4dv(laThemeColor(bt, LA_BT_TEXT));
                la_SingleLineStringDrawSelection(ui, TL,TU, bt, buf, ui->Extra->Edit);

                tnsDrawStringM(0, buf, laThemeColor(bt, LA_BT_TEXT_ACTIVE), TL,TR,TU, ui->Flags|sf);
            }else{
                tnsDrawStringWithPriority(NoLabel?0:buf2, buf,la_UiTextColor(bt,ui), 0, TL,TR,TU, ui->Flags|sf);
            }
        }
        if (ui->Extra && ui->Extra->On == i + 1) ui->State = Original;
    }

    tnsFlush();
}
void la_FloatArrayColorDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    real Data[32];
    int Len, i;
    int IsColorful=ui->Flags&LA_UI_FLAGS_COLORFUL;

    laGetFloatArray(&ui->PP, Data);
    Len = laGetArrayLength(&ui->PP);

    if(ui->PP.LastPs->p->Tag&LA_PROP_IS_LINEAR_SRGB){ tns2LogsRGB(Data); }

    tnsUseNoTexture();
    if(IsColorful){
        int sw=(ui->R-ui->L)/2;
        tnsColor4d(Data[0], Data[1], Data[2], 1);
        tnsVertex2d(ui->L, ui->U); tnsVertex2d(ui->L+sw, ui->U);
        tnsVertex2d(ui->L+sw, ui->B); tnsVertex2d(ui->L, ui->B);
        tnsPackAs(GL_TRIANGLE_FAN);
        tnsColor4d(Data[0], Data[1], Data[2], Len>3?Data[3]:1);
        tnsVertex2d(ui->L+sw, ui->U); tnsVertex2d(ui->R, ui->U);
        tnsVertex2d(ui->R, ui->B); tnsVertex2d(ui->L+sw, ui->B);
        tnsUseHalftone(1.0); tnsPackAs(GL_TRIANGLE_FAN); tnsUseHalftone(0.0);
    }else{
        tnsColor4d(Data[0], Data[1], Data[2], 1);
        tnsVertex2d(ui->L, ui->U); tnsVertex2d(ui->R, ui->U);
        tnsVertex2d(ui->R, ui->B); tnsVertex2d(ui->L, ui->B);
        tnsPackAs(GL_TRIANGLE_FAN);
    }

    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    if(!NoDecal){
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        tnsVertex2d(ui->L, ui->U); tnsVertex2d(ui->R, ui->U);
        tnsVertex2d(ui->R, ui->B); tnsVertex2d(ui->L, ui->B);
        tnsPackAs(GL_LINE_LOOP);
    }

    tnsFlush();
}
void la_EnumSelectorDraw(laUiItem *ui, int h){
    laEnumItem *Data[32], Len;
    laBoxedTheme *bt = (*ui->Type->Theme);
    char buf[48] = {0};
    char buf2[48] = {0};
    char prefix[8][64]= {0};
    laEnumProp *ep = ui->PP.LastPs->p;
    int HasIcon = laEnumHasIcon(&ui->PP);
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int IconOnly=ui->Flags&LA_UI_FLAGS_ICON;
    int NoEvent=ui->Flags&LA_UI_FLAGS_NO_EVENT;
    int Highlight=ui->Flags&LA_UI_FLAGS_HIGHLIGHT;
    int IsDisabled=ui->Flags&LA_UI_FLAGS_DISABLED;
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;
    // norm      e1
    // expand    e1 e2 e3
    // expand vertical  (vertical)
    // arr expand    e1 e1 e3
    //               e2 e2 e3
    // arr exp+vert  e1 e1
    //               e2 e2
    //               e3 e3

    tnsUseNoTexture();

    laGetEnumArray(&ui->PP,Data);
    laEnumItem* ei, *use_ei;

    int IsVertical = ui->Flags&LA_UI_FLAGS_TRANSPOSE;
    int IsExpand = ui->Flags&LA_UI_FLAGS_EXPAND;
    int IsCycle = ui->Flags&LA_UI_FLAGS_CYCLE;
    int ArrLen = laGetArrayLength(&ui->PP);
    if(ArrLen==1){ if(!IsExpand) IsVertical=0; }else{ IsExpand=1; }
    int EnumLen = (IsExpand&&(!IsCycle)) ? laGetEnumEntryLen(&ui->PP) : 1;

    if(Highlight&&ArrLen!=1){laGetPrefixP(&ui->PP, prefix);}

    if (laIsPropertyReadOnly(&ui->PP) && !NoDecal) ui->State = LA_BT_DISABLED;

    int _L,_R,_U,_B, _W=ui->R - ui->L;
    if(IsVertical){
        if(IsExpand){ _W/=ArrLen; }
    }else{
        if(IsExpand){ _W/=EnumLen; }
        else{ _W/=ArrLen; }
    }
    
    int UseIco=0,ico=0; char buft[48]={0}; int HasText=0;
    if (ui->ExtraInstructions){
        if (ui->Type->OperatorType->ParseArgs){
            ui->Type->OperatorType->ParseArgs(0, ui->Instructions, &UseIco, buft);
        }
        sprintf(buf,"%s",transLate(buft)); HasText=1;
    }
    for(int i=0;i<ArrLen;i++){
        if(!Data[i]){ Data[i]=((laEnumProp*)ui->PP.LastPs->p)->Items.pFirst; }
        _L=ui->L; _R=ui->R; _U=ui->U; _B=ui->B; ei = ep->Items.pFirst;
        for(int j=0;j<EnumLen;j++){ if(!HasText)buf[0]=0;
            ico=UseIco;
            if(IsVertical){
                if(IsExpand){ _U=ui->U+j*LA_RH; _L=ui->L+i*_W; }
                else{ _U=ui->U+i*LA_RH;  _L=ui->L; }
            }else{
                if(IsExpand){ _U=ui->U+i*LA_RH;  _L=ui->L+j*_W; }
                else{ _U=ui->U; _L=ui->L+i*_W; }
            }
            _B=_U+LA_RH; _R=_L+_W;

            int ExtraState=IsDisabled?LA_BT_DISABLED:ui->State;
            if(IsExpand&&!IsCycle){ use_ei = ei; ei=ei->Item.pNext; ExtraState=(use_ei==Data[i])?LA_BT_ACTIVE:ExtraState; }
            else{ use_ei = Data[i]; if(Highlight && Data[i]->Index!=0){ ExtraState=LA_BT_ACTIVE; } }

            int sw=0;
            if(!NoDecal){
                sw=la_GetBoxOffset(bt, ExtraState);
                tnsUseNoTexture();
                la_DrawBoxAutoArray(_L,_R,_U,_B,bt, ExtraState, laThemeColor(bt,ExtraState),IsVertical,j==0);
            }

            if(Highlight){ if(!HasText && ArrLen==1) strcpy(buf, transLate(ui->PP.LastPs->p->Name)); else{ if(i<8)strcat(buf, &prefix[i]); } }
            if((!HasText) && (!buf[0])) strcpy(buf, transLate(use_ei->Name));
            int iconR; if(IconOnly)iconR=TNS_MAX2(_L+LA_RH,_R);else{ iconR=TNS_MIN2(_L+LA_RH,_R); }
            if (use_ei->IconID && !ico){ ico=use_ei->IconID; }
            if (ico) tnsDrawIcon(ico, la_UiTextColorState(bt,ui,ExtraState), _L-sw,iconR-sw, _U-sw, LA_TEXT_ALIGN_CENTER|ui->Flags|sf);
            if(!IconOnly){
                int UseFlags=ui->Flags; if(!HasIcon && IsExpand){ if(!(UseFlags&LA_TEXT_ALIGN)); UseFlags|=LA_TEXT_ALIGN_CENTER; }
                tnsDrawStringAuto(buf, la_UiTextColorState(bt,ui,ExtraState), _L+LA_M+(HasIcon?LA_RH:0)-sw, _R-LA_M-sw, _U-sw, UseFlags|sf);
            }
            if (!IsExpand && !IsVertical && !IconOnly && !IsCycle && !NoEvent){
                tnsDrawIcon(U'🔻', la_UiTextColorState(bt,ui,ExtraState), _R-LA_RH-sw, _R-sw, _U-sw, ui->Flags|sf);
            }
        }
    }
    tnsFlush();
}
void la_ButtonDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    char buf[LA_RAW_CSTR_MAX_LEN] = {0};
    char *label = 0;
    uint32_t IconID=0;
    int L;
    char *Identifier;
    laOperatorProp *ap = 0;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int IconOnly=ui->Flags&LA_UI_FLAGS_ICON;

    if (ui->PP.LastPs && ui->PP.LastPs->p){
        ap = ui->PP.LastPs->p;
        label = transLate(ap->Base.Name);
        if (!ap->OperatorType) ap->OperatorType = laGetOperatorType(ap->OperatorID);
        IconID = ap->Base.IconID ? ap->Base.IconID : (ap->OperatorType?ap->OperatorType->IconID:0);
    }else{
        label = transLate(ui->AT->Name);
        IconID = ui->AT->IconID;
    }

    if (ui->AT && (!laOperatorAvailableP(ui->AT, 0, ui->Instructions)))
        ui->State = LA_UI_DISABLED;
    elif (ap && ap->OperatorType && (!laOperatorAvailablePSafe(ap->OperatorType, ui->PP.RawThis, ui->PP.Go->UseInstance, ui->Instructions))) ui->State = LA_UI_DISABLED;
    else if (ui->State == LA_UI_DISABLED) ui->State = LA_UI_NORMAL;

    int sw=0;
    if(!NoDecal){
        sw=la_GetBoxOffset(bt,ui->State);
        tnsUseNoTexture();
        la_DrawBoxAuto(ui->L,ui->R,ui->U,ui->B,bt,ui->State,la_UiStateColor(bt,ui));
    }

    uint32_t tIconID=0;
    if (ui->ExtraInstructions && ui->AT && ui->AT->ParseArgs){
        ui->AT->ParseArgs(0, ui->Instructions, &tIconID, buf);
        if (buf[0]) label = transLate(buf);
        if (tIconID) IconID=tIconID;
    }elif (ui->PP.LastPs && ui->PP.LastPs->p->PropertyType == LA_PROP_OPERATOR){
        laOperatorProp *ap = ui->PP.LastPs->p;
        if (ap->OperatorType && ap->OperatorType->ParseArgs){
            ap->OperatorType->ParseArgs(ui->PP.RawThis, ui->Instructions, &tIconID, buf);
            if (buf[0]) label = transLate(buf);
            if (tIconID) IconID=tIconID;
        }
    }

    if (IconID) L = ui->L + LA_M  + h;
    else L = ui->L + LA_M;

    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;
    if (IconID) tnsDrawIcon(IconID, la_UiTextColor(bt,ui), ui->L-sw, (IconOnly?(ui->R):(ui->L+LA_RH))-sw, ui->U-sw, LA_TEXT_ALIGN_CENTER|sf);
    if (!IconOnly) tnsDrawStringAuto(label, la_UiTextColor(bt,ui), L-sw, ui->R-LA_M-sw, ui->U-sw, ui->Flags|sf);
}
void la_LabelDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;

    if (!ui->State) ui->State = LA_UI_NORMAL;
    if (ui->ExtraInstructions && !ui->Instructions){
        strMakeInstructions(&ui->Instructions, ui->ExtraInstructions->Ptr);
    }

    //int State = ui->Instructions ? (strArgumentMatch(ui->Instructions, "disabled", "true") ? LA_THEME_DISABLED : LA_UI_NORMAL) : LA_UI_NORMAL;

    tnsDrawStringAuto(transLate(SSTR(ui->Display)),la_UiTextColorPlain(bt, ui), ui->L, ui->R, ui->U, ui->Flags|sf);
}
void la_SingleLineStringDrawSelection(laUiItem *ui, int Begin, int U, laBoxedTheme *bt, uint32_t *str, laStringEdit *se){
    tnsUseNoTexture();
    if (!strHasSelection(se)){
        tnsColor4dv(laAccentColor(LA_BT_TEXT));
        int L = (se->CursorBefore ? tnsStringGetWidthU(str, se->CursorBefore, ui->Flags&LA_TEXT_MONO) : 0) + Begin;
        int CW=LA_M;
        tnsVertex2d(L-CW, U + LA_RH - LA_M);
        tnsVertex2d(L-CW, U + LA_M);
        tnsVertex2d(L+CW, U + LA_M);
        tnsVertex2d(L+CW, U + LA_RH - LA_M);
        tnsPackAs(GL_TRIANGLE_FAN);
    }else{
        int L = (se->BeginBefore ? tnsStringGetWidthU(str, se->BeginBefore, ui->Flags&LA_TEXT_MONO) : 0) + Begin;
        int R = (se->EndBefore ? tnsStringGetWidthU(str, se->EndBefore, ui->Flags&LA_TEXT_MONO) : 0) + Begin;
        tnsColor4dv(laAccentColor(LA_BT_NORMAL));
        tnsVertex2d(L, U + LA_M);
        tnsVertex2d(L, U + LA_RH - LA_M);
        tnsVertex2d(R, U + LA_RH - LA_M);
        tnsVertex2d(R, U + LA_M);
        tnsPackAs(GL_TRIANGLE_FAN);
    }
}
void la_SingleLineStringDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    char* _temp[LA_RAW_CSTR_MAX_LEN] = {0}; char* temp=_temp;
    uint32_t* line=0;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;

    if (!ui->State) ui->State = LA_UI_NORMAL;

    int sw=0;
    if(!NoDecal){
        sw=la_GetBoxOffset(bt,ui->State);
        tnsUseNoTexture();
        la_DrawBoxAuto(ui->L,ui->R,ui->U,ui->B,bt,ui->State,la_UiStateColor(bt,ui));
    }

    if (ui->Extra && ui->Extra->Edit){
        line=strGetCursorLine(ui->Extra->Edit,0)->Buf;
    }else{
        laGetString(&ui->PP, _temp, &temp);
    }

    if (line) la_SingleLineStringDrawSelection(ui, ui->L+(NoDecal?0:LA_M)-sw, ui->U-sw, bt, line, ui->Extra->Edit);

    if(ui->PP.LastPs->p->CanTranslate){ temp=transLate(temp); }
    tnsDrawStringAutoM(line?0:temp, line, la_UiTextColor(bt,ui), ui->L+(NoDecal?0:LA_M)-sw, ui->R-(NoDecal?0:LA_M)-sw, ui->U-sw, ui->Flags|sf);
}
void la_MultiStringDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    char _temp[LA_RAW_CSTR_MAX_LEN] = {0}; char* temp=_temp;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;

    if (!ui->State) ui->State = LA_UI_NORMAL;

    int NumberWidth=LA_M;

    if((!ui->Extra->Edit) || (ui->Extra->Ptr1!=ui->PP.EndInstance)){
        laGetString(&ui->PP, _temp, &temp);
        strBeginEdit(&ui->Extra->Edit, temp);
        ui->Extra->Ptr1=ui->PP.EndInstance;
    }

    laStringEdit* se=ui->Extra->Edit;
    real MonoWidth=tnsGetMonoFontAdvance(); int Count=se->TotalLines;
    while(Count){ Count/=10; NumberWidth+=MonoWidth; }

    if (ui->State!=LA_UI_ACTIVE){
        strSetEditViewRange(ui->Extra->Edit,
            ui->Extent>0?ui->Extent:((ui->B-ui->U)/LA_RH),(ui->R-ui->L-NumberWidth-LA_M*2-LA_M)/MonoWidth);
    }

    int sw=0;
    if(!NoDecal){
        sw=la_GetBoxOffset(bt,ui->State);
        tnsUseNoTexture();
        la_DrawBoxAutoFill(ui->L,ui->R,ui->U,ui->B,bt,ui->State,la_UiStateColor(bt,ui));

        real SideColor[4]; tnsVectorSet3v(SideColor,laThemeColor(bt,LA_BT_BORDER));SideColor[3]=0.8;
        la_DrawBoxAutoFill(ui->L,ui->L+NumberWidth,ui->U,ui->B,bt,ui->State,SideColor);

        if(!(se->CursorLine<se->ViewStartLine||se->CursorLine>se->ViewStartLine+se->ViewHeight)){
            int LineU=(se->CursorLine-se->ViewStartLine)*LA_RH+ui->U;
            tnsColor4dv(ui->State==LA_UI_ACTIVE?laAccentColor(LA_BT_NORMAL):laThemeColor(bt,LA_BT_BORDER));
            int _R=ui->R-sw; if(_R>ui->R){ _R=ui->R; }
            tnsVertex2d(ui->L-sw, LineU+0.5f-sw); tnsVertex2d(_R, LineU+0.5f-sw);
            tnsVertex2d(ui->L-sw, LineU+LA_RH-0.5f-sw); tnsVertex2d(_R, LineU+LA_RH-0.5f-sw);
            tnsPackAs(GL_LINES);

            tnsColor4dv(ui->State==LA_UI_ACTIVE?laAccentColor(LA_BT_TEXT):laThemeColor(bt,LA_BT_TEXT_NORMAL));
            la_DrawBox(ui->L-sw,ui->L+NumberWidth-sw,LineU-sw,LineU+LA_RH-sw);
        }

        la_DrawBoxAutoBorder(ui->L,ui->R,ui->U,ui->B,bt,ui->State);
    }else{
        NumberWidth=LA_M;
    }

    tnsColor4dv(laThemeColor(bt, LA_BT_TEXT));

    int Line=0, Skip=0, _L=ui->L+NumberWidth+LA_M; char num[64]={0};
    for(laStringLine* sl=se->Lines.pFirst;sl;sl=sl->Item.pNext){
        int UseLine=Line+Skip;
        if(UseLine<se->ViewStartLine){ Skip++; continue; }
        if(UseLine>=se->ViewHeight+se->ViewStartLine){ break; }
        int slen=strlenU(sl->Buf);
        sprintf(num,"%d", UseLine+1); int LineU=ui->U+Line*LA_RH;
        tnsDrawStringM(num, 0, laThemeColor(bt, ((UseLine==se->CursorLine)?LA_BT_NORMAL:LA_BT_TEXT)),
             _L-LA_M -(tnsStringGetWidth(num, 0, ui->Flags&LA_TEXT_MONO))-sw, ui->R-sw, LineU-sw, ui->Flags|sf);
        if(slen>=se->ViewStartCol){
            tnsDrawStringM(0, &sl->Buf[se->ViewStartCol], laThemeColor(bt, ui->State|LA_BT_TEXT), _L-sw, ui->R-sw, LineU-sw, ui->Flags|LA_TEXT_OVERFLOW_ARROW|sf);
        }
        if(UseLine>=se->BeginLine&&UseLine<=se->EndLine){
            int BeginC=UseLine==se->BeginLine?se->BeginBefore:0; int EndC=UseLine==se->EndLine?se->EndBefore:slen; 
            BeginC-=se->ViewStartCol; EndC-=se->ViewStartCol; if(EndC>se->ViewWidth)EndC=se->ViewWidth;
            tnsUseNoTexture();
            int UseL=BeginC*MonoWidth+_L, UseR=EndC*MonoWidth+_L;
            TNS_CLAMP(UseL, _L, ui->R); TNS_CLAMP(UseR, _L, ui->R);
            tnsColor4dv(laAccentColor(LA_BT_NORMAL));
            la_DrawBox(UseL-sw,UseR-sw,LineU-sw,LineU+LA_RH-sw);
        }
        int len=strlenU(sl->Buf); 
        if(UseLine==se->CursorLine){
            int Dist=se->CursorBefore==0?0:tnsStringGetWidthU(sl->Buf,se->CursorBefore,ui->Flags&LA_TEXT_MONO);
            tnsUseNoTexture(); int d=se->ViewStartCol*MonoWidth; int w2=LA_M;
            tnsColor4dv(ui->State==LA_UI_ACTIVE?laAccentColor(LA_BT_TEXT):laThemeColor(bt,LA_BT_TEXT_NORMAL));
            tnsVertex2d(_L+Dist-w2-d-sw, LineU-sw); tnsVertex2d(_L+Dist+w2-d, LineU-sw);
            tnsVertex2d(_L+Dist-w2-d-sw, LineU+LA_RH-sw); tnsVertex2d(_L+Dist+w2-d, LineU+LA_RH-sw);
            tnsPackAs(GL_TRIANGLE_STRIP);
        }
        Line++;
        tnsFlush();
    }

    if(ui->Extent>=0)
        tnsDrawStringAuto("◿",laThemeColor(bt,LA_BT_BORDER),ui->R-LA_RH-sw, ui->R-sw, ui->B-LA_M-LA_RH-sw, LA_TEXT_ALIGN_CENTER|sf);

    if(!NoDecal){
        char buf[100]={0}; sprintf(buf, "%d,%d | %d Lines", se->CursorLine+1, se->CursorBefore+1, se->TotalLines);
        tnsDrawStringAuto(buf,laThemeColor(bt,LA_BT_BORDER),ui->L-sw, ui->R-LA_RH-sw, ui->B-LA_M-LA_RH-sw, LA_TEXT_MONO|LA_TEXT_ALIGN_RIGHT|sf);
    }

    tnsFlush();
}
void la_UiSeperatorDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    if (!bt) return;

    if(ui->Flags&LA_UI_FLAGS_TRANSPOSE){
        char str[]="🞄";
        int strw=tnsStringGetWidth(str,0, 0);
        tnsDrawStringM(str, 0, laThemeColor(bt,LA_BT_BORDER), ui->L+(ui->R-ui->L-strw)/2, ui->R, ui->U, 0);
    }else{
        tnsUseNoTexture();
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        real u=(ui->U+ui->B)/2; tnsVertex2d(ui->L+0.5, u+0.5); tnsVertex2d(ui->R+0.5, (ui->U+ui->B)/2+0.5);
        tnsPackAs(GL_LINES);
    }
}
void la_ColorCircleDrawHCY(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    real c = (real)((ui->R + ui->L) / 2) - h / 2; /*- LA_M*/
    real r = (ui->B - ui->U - LA_M - LA_M) / 2;
    int i = 0;
    real colors[584]={0}; //4*72*2+4*2
    real verts[292]={0}; //72*2*2+2*2
    real hcy[3] = {0, 1, 1};
    real step = 1.0 / 72.0f;
    real RealColor[4] = {0,0,0,1};
    real PickerPos[2];
    real pi_4;
    GLuint index[26];
    real v, cv, sv;
    real lw = (float)(ui->B-ui->U)/LA_RH/5; if(lw<1.0f) lw=1.0f;

    laGetFloatArray(&ui->PP, RealColor);

    if(ui->PP.LastPs->p->Tag&LA_PROP_IS_LINEAR_SRGB){ tns2LogsRGB(RealColor); }

    tnsRGB2HCY(RealColor, hcy);

    PickerPos[0] = c + cos(hcy[0] * TNS_PI * 2) * pow(hcy[1],1.0f/MAIN.ColorPickerGamma) * r;
    PickerPos[1] = ui->U + r + sin(hcy[0] * TNS_PI * 2) * pow(hcy[1],1.0f/MAIN.ColorPickerGamma) * r;

    hcy[0] = 0;
    hcy[1] = 1;

    for (i; i < 72; i += 1){
        int beg = i * 8;
        hcy[0]+=step; tnsVectorSet3v(&colors[beg],hcy); colors[beg+3]=1;
        tnsVectorSet4v(&colors[beg+4],&colors[beg]); colors[beg+5]=0;
    }

    tnsFlush();

    tnsUniformColorMode(T->immShader,1); tnsUniformHCYGamma(T->immShader,MAIN.ColorPickerGamma);

    int IsClay=ui->Flags&LA_UI_FLAGS_COLOR_SPACE_CLAY;
    int IsP3=ui->Flags&LA_UI_FLAGS_COLOR_SPACE_D65_P3;
    int ShowInfo=0,ShowProof=0;

    if(MAIN.CurrentWindow->OutputColorSpace!=TNS_COLOR_SPACE_SRGB && (!IsClay) && (!IsP3)){
        tnsUniformShowColorOverflowStripes(T->immShader,1);
    }

    laWindow* win=MAIN.CurrentWindow;

    if(IsClay){
        tnsUniformInputColorSpace(T->immShader,TNS_COLOR_SPACE_CLAY);
        if(win->OutputColorSpace!=TNS_COLOR_SPACE_CLAY) ShowInfo=1;
    }elif(IsP3){
        tnsUniformInputColorSpace(T->immShader,TNS_COLOR_SPACE_D65_P3);
        if(win->OutputColorSpace!=TNS_COLOR_SPACE_D65_P3) ShowInfo=1;
    }else{
        tnsUniformInputColorSpace(T->immShader,TNS_COLOR_SPACE_SRGB);
    }
    if(win->OutputProofing){ ShowProof=1; }

    tnsUseNoTexture();
    tnsVectorSet4(&colors[576],1,1,hcy[2],1);
    tnsVectorSet4(&colors[580],1,0,hcy[2],1);
    tnsColorArray4d(colors, 146);
    tnsMakeCircle2d(verts, 72, c, ui->U + r, r, 1);
    for(int i=0;i<73;i++){ tnsVectorSet2(&verts[2+i*4],c,ui->U + r); }
    tnsVectorSet2v(&verts[288],&verts[0]);
    tnsVectorSet2v(&verts[290],&verts[2]);
    tnsVertexArray2d(verts, 146);
    tnsPackAs(GL_TRIANGLE_STRIP);

    tnsFlush();
    
    tnsUniformColorMode(T->immShader,0);

    tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
    tnsMakeCircle2d(verts, 72, c, ui->U + r, r, 0);
    tnsVertexArray2d(verts, 72);
    tnsPackAs(GL_LINE_LOOP);

    pi_4 = TNS_PI / 4;

    tnsMakeArc2d(&verts[26], 12, c, ui->U + r, r + LA_M + h, pi_4, -pi_4);
    tnsMakeArc2d(verts, 12, c, ui->U + r, r + LA_M, pi_4, -pi_4);
    tnsMakeLinerGradient4d(&colors[52], 13, 0, 0, 0, 1, 1, 1, 1, 1);
    tnsMakeLinerGradient4d(colors, 13, 0, 0, 0, 1, 1, 1, 1, 1);
    tnsMakeBridgedIndex(index, 13, 0, 0);
    tnsColorArray4d(colors, 26);
    tnsVertexArray2d(verts, 26);
    tnsIndexArray(index, 26);
    tnsPackAs(GL_TRIANGLE_STRIP);

    tnsMakeArc2d(&verts[26], 12, c, ui->U + r, r + LA_M + h, -pi_4, pi_4);
    tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
    tnsVertexArray2d(verts, 26);
    tnsPackAs(GL_LINE_LOOP);

    v = -hcy[2] + 0.5;
    cv = cos(v / 2 * TNS_PI);
    sv = sin(v / 2 * TNS_PI);

    tnsLineWidth(lw);
    verts[0] = cv * r + c; verts[1] = sv * r + ui->U + r;
    verts[2] = cv * (r + h) + c; verts[3] = sv * (r + h) + ui->U + r;
    tnsColor4d(1, 1, 1, 1);
    tnsVertexArray2d(verts, 2);
    tnsPackAs(GL_LINES);
    verts[1]+=lw; verts[3]+=lw;
    tnsColor4d(0, 0, 0, 1);
    tnsVertexArray2d(verts, 2);
    tnsPackAs(GL_LINES);

    real circleR=lw*8;

    tnsColor4dv(RealColor);
    tnsMakeCircle2d(verts, 16, PickerPos[0], PickerPos[1], circleR,0);
    tnsVertexArray2d(verts, 16);
    tnsPackAs(GL_TRIANGLE_FAN);
    tnsColor4d(1, 1, 1, 1);
    tnsVertexArray2d(verts, 16);
    tnsPackAs(GL_LINE_LOOP);
    tnsMakeCircle2d(verts, 16, PickerPos[0], PickerPos[1], circleR-lw,0);
    tnsColor4d(0.1, 0.1, 0.1, 1);
    tnsVertexArray2d(verts, 16);
    tnsPackAs(GL_LINE_LOOP);
    tnsLineWidth(1);

    if(ui->Extent>=0) tnsDrawStringAuto("◿",laThemeColor(bt,LA_BT_BORDER),ui->R-LA_RH, ui->R, ui->B-LA_M-LA_RH, LA_TEXT_ALIGN_CENTER);

    if(IsClay) tnsDrawStringAuto("Clay",laThemeColor(bt,LA_BT_TEXT),ui->L, c-r+LA_RH, ui->U+LA_M, LA_TEXT_ALIGN_RIGHT|LA_TEXT_MONO);
    if(IsP3) tnsDrawStringAuto("D65 P3",laThemeColor(bt,LA_BT_TEXT),ui->L, c-r+LA_RH, ui->U+LA_M, LA_TEXT_ALIGN_RIGHT|LA_TEXT_MONO);
    char bufinfo[64];
    sprintf(bufinfo,"%s%s",ShowInfo?"🛈":"",ShowProof?"🖶":"");
    if(ShowInfo||ShowProof) tnsDrawStringAuto(bufinfo,laThemeColor(bt,LA_BT_TEXT),c-r, ui->R, ui->B-LA_M-LA_RH, LA_TEXT_ALIGN_LEFT);

    tnsFlush();

    tnsUniformShowColorOverflowStripes(T->immShader,0);
    tnsUniformInputColorSpace(T->immShader,0);
}
void la_MenuItemDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;

    int sw=0;
    if(!NoDecal){
        sw=la_GetBoxOffset(bt,ui->State);
        tnsUseNoTexture();
        la_DrawBoxAuto(ui->L,ui->R,ui->U,ui->B,bt,ui->State,la_UiStateColor(bt,ui));
    }
    tnsDrawStringAuto(transLate(ui->Display->Ptr), la_UiTextColor(bt,ui), ui->L+LA_M-sw, ui->R-sw, ui->U-sw, ui->Flags|sf);
}
void la_ConditionToggleDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    char *buf[LA_RAW_CSTR_MAX_LEN];
    char *buf2[LA_RAW_CSTR_MAX_LEN]; buf2[0]=0;
    uint32_t IconID;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;

    if (!ui->State) ui->State = LA_UI_NORMAL;
    int UseState=ui->State;
    if (!(ui->Flags&LA_TEXT_ALIGN)) ui->Flags|=LA_TEXT_ALIGN_CENTER;

    int sw=0;
    if(!NoDecal){
        sw=la_GetBoxOffset(bt,ui->State);
        tnsUseNoTexture();
        la_DrawBoxAuto(ui->L,ui->R,ui->U,ui->B,bt,ui->State,la_UiStateColor(bt,ui));
    }else{
        UseState=LA_UI_NORMAL;
    }

    buf[0] = 0;
    if (ui->ExtraInstructions){
        if (ui->Type->OperatorType->ParseArgs){
            ui->Type->OperatorType->ParseArgs(0, ui->Instructions, &IconID, buf);
        }
    }
    if (buf[0]){ sprintf(buf2,"%s%s",transLate(buf),(NoDecal&&(ui->State==LA_UI_NORMAL))?" [...]":""); }
    if (buf2[0]) tnsDrawStringAuto(buf2, la_UiTextColorState(bt,ui,UseState), ui->L+LA_M-sw, ui->R-LA_M-sw, ui->U-sw, ui->Flags|sf);
    else tnsDrawStringAuto(ui->State == LA_UI_ACTIVE ? "⯆" : "⯈", la_UiTextColorState(bt,ui,UseState), ui->L+LA_M-sw, ui->R-LA_M-sw, ui->U-sw, ui->Flags|sf);
}
void la_DrawColumnAdjusterRecursive(int U, int B, laColumn *c, int W, tnsVector4d color, int LeftMost){
    if (c->LS){
        la_DrawColumnAdjusterRecursive(U, B, c->LS, W, color, LeftMost);
        la_DrawColumnAdjusterRecursive(U, B, c->RS, W, color, LeftMost);
    }else{
        if(c->IL==LeftMost) return;
        tnsDrawStringAuto("⌉⌈", color, c->IL-LA_RH, c->IL+LA_RH, U, LA_TEXT_ALIGN_CENTER);
    }
}
void la_ColumnAdjusterDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);

    tnsUseNoTexture();
    tnsColor4dv(laThemeColor(bt,LA_BT_TEXT));
    la_DrawColumnAdjusterRecursive(ui->U, ui->U + LA_RH, ui->C, (LA_M+LA_M)/2, la_UiTextColor(bt,ui), ui->C->IL);
}
void la_ColumnViewerDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    laColumn *c = ui->PP.EndInstance;
    int W = ui->R - ui->L - LA_M - LA_M;
    //if (c->IR == c->IL)
    //    return;
    int T = c->Top->IR - c->Top->IL;
    int L = (int)((real)(c->IL - c->Top->IL) * W / T) + ui->L + LA_M,
        R = (int)((real)(c->IR - c->Top->IL) * W / T) + ui->L + LA_M;

    tnsUseNoTexture();
    tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));

    tnsVertex2d(L, ui->U + LA_M);
    tnsVertex2d(R, ui->U + LA_M);
    tnsVertex2d(R, ui->B - LA_M);
    tnsVertex2d(L, ui->B - LA_M);
    tnsPackAs(GL_TRIANGLE_FAN);
}
void la_NodeSocketDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    char* label=0; char* icon;
    laProp* p=ui->PP.LastPs->p; laPropContainer* pc=la_EnsureSubTarget(p,0);
    real* ColorBkg=0,*ColorSocket;
    if(pc==LA_PC_SOCKET_IN)   { laNodeInSocket*s=ui->PP.EndInstance; if(!s) return; ColorSocket=laThemeColor(bt,LA_BT_TEXT); ColorBkg=laThemeColor(bt, LA_BT_NORMAL); label=s->Label?s->Label->Ptr:""; }
    else{ laNodeOutSocket*s=ui->PP.EndInstance; if(!s) return; ColorSocket=laThemeColor(bt,LA_BT_TEXT_ACTIVE); ColorBkg=laThemeColor(bt, LA_BT_ACTIVE); label=s->Label?s->Label->Ptr:""; }

    int IsDisabled=ui->Flags&LA_UI_FLAGS_DISABLED;
    int NoDecal = ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;
    int sw=0;

    if(!NoDecal){
        sw=la_GetBoxOffset(bt,LA_BT_NORMAL);
        tnsUseNoTexture();
        la_DrawBoxAuto(ui->L,ui->R,ui->U,ui->B,bt,LA_BT_NORMAL,ColorBkg);
    }

    int tl,tr,tu,sl,sr,su;
    if(ui->Flags&LA_UI_SOCKET_LABEL_N){ tl=ui->L; tu=ui->U; tr=ui->R; sl=ui->L; sr=ui->R; su=ui->U+LA_RH; }
    elif(ui->Flags&LA_UI_SOCKET_LABEL_S){ tl=ui->L; tu=ui->U+LA_RH; tr=ui->R; sl=ui->L; sr=ui->R; su=ui->U; }
    elif(ui->Flags&LA_UI_SOCKET_LABEL_W){ tl=ui->L; tu=ui->U; tr=tl+LA_2RH; sl=ui->L+LA_2RH; sr=ui->R; su=ui->U; }
    elif(ui->Flags&LA_UI_SOCKET_LABEL_E){ tu=ui->U; tr=ui->R; tl=tr-LA_2RH; sl=ui->L; sr=tl; su=ui->U; }
    else{ sl=ui->L; su=ui->U; sr=ui->R; label=0; }

    tnsDrawStringAuto("⭗", IsDisabled?laThemeColor(bt,LA_BT_TEXT|LA_UI_DISABLED):ColorSocket, sl, sr, su, LA_TEXT_ALIGN_CENTER|sf);
    if(label) tnsDrawStringAuto(label, IsDisabled?laThemeColor(bt,LA_BT_TEXT|LA_UI_DISABLED):ColorSocket, tl-sw, tr-sw, tu-sw, LA_TEXT_ALIGN_CENTER|LA_TEXT_MONO|sf);
}
void la_HeightAdjusterDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;
    char* str="☰";
    if(ui->State==LA_BT_ACTIVE){ str="🡙"; }
    tnsDrawStringAuto(str, laThemeColor(bt,LA_BT_TEXT), ui->L-1000, ui->R+1000, ui->U, LA_TEXT_ALIGN_CENTER|sf);
}
void la_RawPropDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    int s=0;
    //void* data=laGetRaw(&ui->PP, &s);
    //char* buf[128]; sprintf(buf,"%d bytes of raw data at 0x%0x", s, data);
    tnsDrawStringAuto("RAW DATA", laThemeColor(bt,LA_BT_TEXT), ui->L, ui->R, ui->U, ui->Flags|LA_TEXT_MONO);
}
void la_MapperDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;

    if(!NoDecal){
        tnsUseNoTexture();
        la_DrawBoxAuto(ui->L,ui->R,ui->U,ui->B,bt,LA_BT_NORMAL,0);
    }

    laPropContainer* pc=la_EnsureSubTarget(ui->PP.LastPs->p,ui->PP.EndInstance);
    if(!pc || pc!=LA_PC_MAPPER || !ui->PP.EndInstance){
        tnsDrawStringAuto("Property is not a la_value_mapper.",0,ui->L+LA_M,ui->R-LA_M,LA_M,LA_TEXT_LINE_WRAP); return;
    }

    laValueMapper* vm=ui->PP.EndInstance;

    tnsColor4dv(laAccentColor(LA_BT_TEXT)); tnsLineWidth(2);tnsPointSize(5);
    for(laValueMapperPoint* vmp=vm->Points.pFirst;vmp;vmp=vmp->Item.pNext){
        tnsVertex2d(tnsInterpolate(ui->L,ui->R,vmp->x),tnsInterpolate(ui->B,ui->U,vmp->y));
    } tnsPackAs(GL_LINE_STRIP);

    for(laValueMapperPoint* vmp=vm->Points.pFirst;vmp;vmp=vmp->Item.pNext){
        tnsVertex2d(tnsInterpolate(ui->L,ui->R,vmp->x),tnsInterpolate(ui->B,ui->U,vmp->y));
    } tnsPackAs(GL_POINTS);

    tnsLineWidth(1);tnsPointSize(1);
}
void la_ImageDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    tnsImage* im=ui->Extra;
    if(!im->Texture) { tns_ImageToTexture(im); }
    if(!im->Texture) {
        tnsUseNoTexture();
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        tnsVertex2d(ui->L, ui->U); tnsVertex2d(ui->R, ui->U);
        tnsVertex2d(ui->R, ui->B); tnsVertex2d(ui->L, ui->B);
        tnsPackAs(GL_LINE_LOOP);
        tnsVertex2d(ui->L, ui->U); tnsVertex2d(ui->R, ui->B);
        tnsVertex2d(ui->R, ui->U); tnsVertex2d(ui->L, ui->B);
        tnsPackAs(GL_LINES);
        return;
    }
    int W=im->Texture->Width, H=im->Texture->Height;
    int Full=ui->Flags&LA_UI_IMAGE_FULL_W;
    real r=(real)(ui->R-ui->L)/W; if(r<1){W*=r;H*=r;}
    real UseW=Full?(W):((real)LA_RH*ui->Extent/H*W);
    real L=(ui->R-ui->L-UseW)/2,U=0,UseH=Full?H:(ui->B-ui->U);
    if(Full){ U=(ui->B-ui->U-H)/2; }
    if(ui->Flags&LA_TEXT_ALIGN_CENTER){ L=0; }
    elif(ui->Flags&LA_TEXT_ALIGN_RIGHT){ L=ui->R-ui->L-UseW; }
    tnsDraw2DTextureDirectly(im->Texture,ui->L+L,ui->U+U,UseW,UseH);
    tnsFlush();
}

void la_ValueMeterType1Draw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    int DataI[8]; real Data[8];
    int Len, i, W; real Seg;
    char buf[64] = {0};
    char buf2[64] = {0};
    char prefix[8][64] = {0};
    int Original;
    int Imin=-100, Imax=100;
    real min=-100, max=100;
    int s, State; int IsVertical=ui->Flags&LA_UI_FLAGS_TRANSPOSE;
    int NoLabel=ui->Flags&LA_UI_FLAGS_NO_LABEL;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int sw=NoDecal?0:la_GetBoxOffset(bt,LA_BT_NORMAL);
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;

    int IsInt=(ui->PP.LastPs->p->PropertyType&LA_PROP_INT)?1:0;
    Len = laGetArrayLength(&ui->PP);
    laGetPrefixP(&ui->PP, prefix);
    if(IsInt){
        laGetIntArray(&ui->PP, DataI); for(int i=0;i<Len;i++){ Data[i]=DataI[i]; }
        laGetIntRange(&ui->PP, &Imin, &Imax); min=Imin; max=Imax;
    }else{
        laGetFloatArray(&ui->PP, Data);
        laGetFloatRange(&ui->PP,&min,&max);
    }

    for (i = 0; i < Len; i++){
        int _L=ui->L, _R=ui->R, _U=ui->U, _B=ui->B;
        if(IsVertical){
            Seg = (real)(ui->R-ui->L)/Len;
            _L = ui->L + i * Seg; _R=ui->L + (i+1) * Seg;
        }else{
            Seg = (real)(ui->B-ui->U)/Len;
            _U = ui->U + i * Seg; _B=_U+Seg;
        }

        if(!NoDecal){
            tnsUseNoTexture();
            la_DrawBoxAutoFillArray(_L,_R,_U,_B,bt,ui->State,la_UiStateColor(bt,ui),IsVertical,i==0);

            int R1=_R,U1=_U;
            if(IsVertical){ U1 = (real)(max - Data[i]) / (real)(max - min) * (real)(_B-_U) + _U; }
            else{ R1 = (real)(Data[i] - min) / (real)(max - min) * (real)(_R-_L) + _L; }
            tnsUseNoTexture(); real* color=laThemeColor(bt,LA_BT_TEXT);
            tnsColor4d(LA_COLOR3(color),0.4);
            la_DrawBox(_L-sw,R1-sw,U1-sw,_B-sw);
            tnsPackAs(GL_TRIANGLE_FAN);

            la_DrawBoxAutoBorderArray(_L,_R,_U,_B,bt,ui->State,IsVertical,i==0);
        }

        if(!NoLabel){
            if(Len==1){ sprintf(buf,"%s",ui->PP.LastPs->p->Name); } else{ if(i<8)sprintf(buf,"%s",prefix[i]); }
            if(buf[0]) tnsDrawStringAuto(buf, laThemeColor(bt, LA_BT_TEXT), _L+LA_M-sw, _R-LA_M-sw, _U-sw, ui->Flags|sf);//, ui->ExtraInstructions);
        }

        int oflow=0,ofhigh=0;
        if(Data[i]<min){ oflow=1; Data[i]=min; }
        if(Data[i]>max){ ofhigh=1; Data[i]=max; }

        if(IsVertical){
            if(oflow){ tnsDrawStringAuto("⯆",laThemeColor(bt, LA_BT_TEXT), _L+LA_M-sw,_R-LA_M-sw, _U-sw, LA_TEXT_ALIGN_CENTER|sf); }
            elif(ofhigh){ tnsDrawStringAuto("⯅",laThemeColor(bt, LA_BT_TEXT), _L+LA_M-sw,_R-LA_M-sw, _B-LA_RH-sw, LA_TEXT_ALIGN_CENTER|sf); }
        }else{
            if(oflow){ tnsDrawStringAuto("⯇",laThemeColor(bt, LA_BT_TEXT), _L+LA_M-sw,_R-LA_M-sw, _U-sw, LA_TEXT_ALIGN_LEFT|sf); }
            elif(ofhigh){ tnsDrawStringAuto("⯈",laThemeColor(bt, LA_BT_TEXT), _L+LA_M-sw,_R-LA_M-sw, _U-sw, LA_TEXT_ALIGN_RIGHT|sf); }
        }
    }

    int trisize=LA_RH/3.0f;
    for(laCompoundPP* CPP=ui->CompoundPPs.pFirst;CPP;CPP=CPP->Item.pNext){
        IsInt=(CPP->PP.LastPs->p->PropertyType&LA_PROP_INT)?1:0;
        Len = laGetArrayLength(&CPP->PP);
        laGetPrefixP(&CPP->PP, prefix);
        if(IsInt){
            laGetIntArray(&CPP->PP, DataI); for(int i=0;i<Len;i++){ Data[i]=DataI[i]; }
            laGetIntRange(&CPP->PP, &Imin, &Imax); min=Imin; max=Imax;
        }else{
            laGetFloatArray(&CPP->PP, Data);
            laGetFloatRange(&CPP->PP,&min,&max);
        }
        if(CPP->Slot <= LA_SLOT_MARKER_4){
            for (i = 0; i < Len; i++){
                int _L=ui->L, _R=ui->R, _U=ui->U, _B=ui->B;
                if(IsVertical){
                    Seg = (real)(ui->R-ui->L)/Len;
                    _L = ui->L + i * Seg; _R=ui->L + (i+1) * Seg;
                }else{
                    Seg = (real)(ui->B-ui->U)/Len;
                    _U = ui->U + i * Seg; _B=_U+Seg;
                }
                _L-=sw; _R-=sw; _U-=sw; _B-=sw;
                int R1=_R,U1=_U;
                if(IsVertical){ U1 = (real)(max - Data[i]) / (real)(max - min) * (real)(_B-_U) + _U; }
                else{ R1 = (real)(Data[i] - min) / (real)(max - min) * (real)(_R-_L) + _L; }
                
                tnsUseNoTexture();
                tnsColor4dv(laAccentColor(LA_BT_TEXT));
                if(IsVertical){ tnsVertex2d(_L, U1); tnsVertex2d(R1, U1); }
                else{ tnsVertex2d(R1, U1); tnsVertex2d(R1, _B); }
                tnsPackAs(GL_LINES);
                if(IsVertical){
                    if(! CPP->Slot % 2){ tnsVertex2d(_L, U1+trisize); tnsVertex2d(_L+trisize, U1); tnsVertex2d(_L, U1-trisize); }
                    else{ tnsVertex2d(_R, U1+trisize); tnsVertex2d(_R-trisize, U1); tnsVertex2d(_R, U1-trisize); }
                }
                else{
                    if(! CPP->Slot % 2){ tnsVertex2d(R1-trisize, _B); tnsVertex2d(R1, _B-trisize); tnsVertex2d(R1+trisize, _B); }
                    else{ tnsVertex2d(R1-trisize, U1); tnsVertex2d(R1, U1+trisize); tnsVertex2d(R1+trisize, U1); }
                }
                tnsPackAs(GL_TRIANGLES);
            }
        }
    }

    tnsFlush();
}
void la_ValueMeterType2Draw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    int DataI[8]; real Data[8]; int SDataI[8]; real SData[8]; int Len, i, W; real Seg;
    char buf[48] = {0}; char buf2[48] = {0};char prefix[16][64] = {0};char limits[2][64]={0};
    int Original;
    int Imin=-100, Imax=100; real min=-100, max=100;
    int s, State; int IsVertical=ui->Flags&LA_UI_FLAGS_TRANSPOSE;
    int NoLabel=ui->Flags&LA_UI_FLAGS_NO_LABEL;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int sw=NoDecal?0:la_GetBoxOffset(bt,LA_BT_NORMAL);
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;

    int IsInt=(ui->PP.LastPs->p->PropertyType&LA_PROP_INT)?1:0;
    Len = laGetArrayLength(&ui->PP);
    laGetPrefixP(&ui->PP, prefix);
    if(IsInt){
        laGetIntArray(&ui->PP, DataI); for(int i=0;i<Len;i++){ Data[i]=DataI[i]; }
        laGetIntRange(&ui->PP, &Imin, &Imax); min=Imin; max=Imax;
        sprintf(limits[0],"%d",Imin); sprintf(limits[1],"%d",Imax);
    }else{
        laGetFloatArray(&ui->PP, Data);
        laGetFloatRange(&ui->PP,&min,&max);
        sprintf(limits[0],"%lf",min); sprintf(limits[1],"%lf",max);
    }

    int sl = TNS_MAX2(abs(ui->Extent),5);
    int ScaleLen = sl * LA_RH;
    real seg = 1;
    int times=1,times10=0;
    while((max-min)/seg > ScaleLen/10){
        seg*=10;
    }
    if(seg>100){ int sseg=seg; while(sseg>=100){ sseg/=10; times*=10; times10++; } }

    for (i = 0; i < Len; i++){
        int _L=ui->L, _R=ui->R, _U=ui->U, _B=ui->B;
        if(IsVertical){
            Seg = (real)(ui->R-ui->L)/Len;
            _L = ui->L + i * Seg; _R=ui->L + (i+1) * Seg;
        }else{
            Seg = (real)(ui->B-ui->U)/Len;
            _U = ui->U + i * Seg; _B=_U+Seg;
        }

        real cx=(_L+_R)/2,cy=(_U+_B)/2,cw=(_R-_L),ch=(_B-_U);

        if(!NoDecal){
            tnsUseNoTexture();
            la_DrawBoxAutoFillArray(_L,_R,_U,_B,bt,ui->State,la_UiStateColor(bt,ui),IsVertical,i==0);

            tnsPushStringClip(_L-sw,_R-sw,_U-sw,_B-sw);
        }

        int oflow=0,ofhigh=0;
        if(Data[i]<min){ oflow=1; Data[i]=min; }
        if(Data[i]>max){ ofhigh=1; Data[i]=max; }
        
        real fac = (real)(Data[i]-min) / (real)(max - min);
        int SL,SR,SU,SB,_SL,_SR,_SU,_SB,Move = fac*ScaleLen;
        if(IsVertical){ SL=_L; SR=_R; SB=_SB=cy+Move; SU=_SU=SB-ScaleLen; TNS_CLAMP(SB,_U,_B); TNS_CLAMP(SU,_U,_B); }
        else{ SL=_SL=cx-Move; SR=_SR=SL+ScaleLen; SB=_B; SU=_U; TNS_CLAMP(SL,_L,_R); TNS_CLAMP(SR,_L,_R); }
        tnsUseNoTexture(); real* color=laThemeColor(bt,LA_BT_TEXT);
        tnsColor4d(LA_COLOR3(color),0.3);
        la_DrawBox(SL-sw,SR-sw,SU-sw,SB-sw);

        tnsColor4d(LA_COLOR3(color),0.7);
        int scalelow=min/seg-1, scalehigh=max/seg+1;
        for(int si=scalelow;si<=scalehigh;si++){
            for(int ssi=0;ssi<10;ssi++){
                real val=si*seg + ssi*0.1*seg; if(val<min) continue; if(val>max) break;
                real sfac = (real)(val-min) / (max - min); int smove=Move-sfac*ScaleLen;
                real lfac = ssi==0?(si==ssi?0.5:0.3):(ssi==5?0.2:0.1);
                if(IsVertical){ int scaleb=cy+smove; if(scaleb>_B || scaleb<_U) continue;
                    tnsVertex2d(cx-cw*lfac-sw,scaleb-sw); tnsVertex2d(cx+cw*lfac-sw,scaleb-sw);
                }else{ int scalel=cx-smove; if(scalel<_L || scalel>_R) continue;
                    tnsVertex2d(scalel,cy-sw+ch*lfac-sw); tnsVertex2d(scalel-sw,cy-ch*lfac-sw);
                }
            }
        }
        tnsPackAs(GL_LINES);

        if(!NoLabel){
            for(int si=scalelow;si<=scalehigh;si++){
                real val=si*seg; if(val<min) continue; if(val>max) break;
                real sfac = (real)(val-min) / (max - min); int smove=Move-sfac*ScaleLen;
                    if(IsInt) sprintf(buf,"%.0lf",val/times); else sprintf(buf,"%.2lf",val/times);
                if(IsVertical){ int scaleb=cy+smove; if(scaleb>_B+LA_RH2 || scaleb<_U-LA_RH2) continue;
                    tnsDrawStringAuto(buf,laThemeColor(bt,LA_BT_TEXT),_L+LA_M,_R-LA_M,scaleb-LA_RH2,ui->Flags);
                }else{ int scalel=cx-smove; if(scalel<_L-100 || scalel>_R+100) continue;
                    tnsDrawStringAuto(buf,laThemeColor(bt,LA_BT_TEXT),scalel-100,scalel+100,_U,ui->Flags);
                }
            }
            if(IsVertical){
                tnsDrawStringAuto(limits[0],laThemeColor(bt,LA_BT_TEXT|ui->State),_L+LA_M-sw,_R-LA_M-sw,_SB-sw,ui->Flags|sf);
                tnsDrawStringAuto(limits[1],laThemeColor(bt,LA_BT_TEXT|ui->State),_L+LA_M-sw,_R-LA_M-sw,_SU-LA_RH-sw,ui->Flags|sf);
            }else{
                tnsDrawStringAuto(limits[0],laThemeColor(bt,LA_BT_TEXT|ui->State),_SL-100-sw,_SL-LA_M-sw,_B-LA_RH-sw,LA_TEXT_ALIGN_RIGHT|sf);
                tnsDrawStringAuto(limits[1],laThemeColor(bt,LA_BT_TEXT|ui->State),_SR+LA_M-sw,_SR+100-sw,_B-LA_RH-sw,LA_TEXT_ALIGN_LEFT|sf);
            }
        }

        if(times!=1){ sprintf(buf,"%d",times10);
            if(IsVertical && cw>LA_RH) tnsDrawStringAuto(buf,laAccentColor(LA_BT_BORDER),_L+LA_M-sw,_R-LA_M-sw,cy-sw,LA_TEXT_MONO|LA_TEXT_ALIGN_RIGHT|sf);
            elif(ch>LA_RH) tnsDrawStringAuto(buf,laAccentColor(LA_BT_BORDER),cx+LA_M-sw,cx+100-sw,_U-sw,LA_TEXT_MONO|LA_TEXT_ALIGN_LEFT|sf);
        }

        tnsUseNoTexture();
        tnsColor4dv(laAccentColor(LA_BT_BORDER));
        if(IsVertical){tnsVertex2d(_L-sw,cy-sw); tnsVertex2d(_R-sw,cy-sw);}
        else{tnsVertex2d(cx-sw,_U-sw); tnsVertex2d(cx-sw,_B-sw);}
        tnsPackAs(GL_LINES);

        if(!NoDecal){
            la_DrawBoxAutoBorderArray(_L,_R,_U,_B,bt,ui->State,IsVertical,i==0);
        }

        //if(IsVertical){
        //    if(oflow){ tnsDrawStringAuto("⯆",laThemeColor(bt, LA_BT_TEXT), _L+LA_M,_R-LA_M, _U, LA_TEXT_ALIGN_CENTER); }
        //    elif(ofhigh){ tnsDrawStringAuto("⯅",laThemeColor(bt, LA_BT_TEXT), _L+LA_M,_R-LA_M, _B-LA_RH, LA_TEXT_ALIGN_CENTER); }
        //}else{
        //    if(oflow){ tnsDrawStringAuto("⯇",laThemeColor(bt, LA_BT_TEXT), _L+LA_M,_R-LA_M, _U, LA_TEXT_ALIGN_LEFT); }
        //    elif(ofhigh){ tnsDrawStringAuto("⯈",laThemeColor(bt, LA_BT_TEXT), _L+LA_M,_R-LA_M, _U, LA_TEXT_ALIGN_RIGHT); }
        //}

        tnsPopStringClip();
    }

    int trisize=LA_RH/3.0f;
    for(laCompoundPP* CPP=ui->CompoundPPs.pFirst;CPP;CPP=CPP->Item.pNext){
        IsInt=(CPP->PP.LastPs->p->PropertyType&LA_PROP_INT)?1:0;
        Len = laGetArrayLength(&CPP->PP);
        laGetPrefixP(&CPP->PP, prefix);
        if(IsInt){
            laGetIntArray(&CPP->PP, SDataI); for(int i=0;i<Len;i++){ SData[i]=SDataI[i]; }
        }else{
            laGetFloatArray(&CPP->PP, SData);
        }
        if(CPP->Slot <= LA_SLOT_MARKER_4){
            for (i = 0; i < Len; i++){
                int _L=ui->L, _R=ui->R, _U=ui->U, _B=ui->B;
                if(IsVertical){
                    Seg = (real)(ui->R-ui->L)/Len;
                    _L = ui->L + i * Seg; _R=ui->L + (i+1) * Seg;
                }else{
                    Seg = (real)(ui->B-ui->U)/Len;
                    _U = ui->U + i * Seg; _B=_U+Seg;
                }
                real cx=(_L+_R)/2,cy=(_U+_B)/2,cw=(_R-_L),ch=(_B-_U);
                real sfac = (real)(SData[i]-min) / (real)(max - min);
                real fac = (real)(Data[i]-min) / (real)(max - min);
                int SL,SR,SU,SB,_SL,_SR,_SU,_SB, Move = fac*ScaleLen,SMove=sfac*ScaleLen;
                if(IsVertical){ _SB=SB=cy+Move-SMove; TNS_CLAMP(SB,_U,_B); }
                else{ SL=_SL=cx-Move+SMove; TNS_CLAMP(SL,_L,_R); }
                
                tnsUseNoTexture();
                tnsColor4dv(laThemeColor(bt,LA_BT_TEXT|LA_BT_ACTIVE));
                if(IsVertical){ tnsVertex2d(_L-sw, SB-sw); tnsVertex2d(_R-sw, SB-sw); }
                else{ tnsVertex2d(SL-sw, _U-sw); tnsVertex2d(SL-sw, _B-sw); }
                tnsPackAs(GL_LINES);
                if(IsVertical){
                    if(! CPP->Slot % 2){ tnsVertex2d(_L-sw, SB-sw); tnsVertex2d(_L-sw+trisize, SB-sw); tnsVertex2d(_L-sw, SB-trisize-sw); }
                    else{ tnsVertex2d(_R-sw, SB+trisize-sw); tnsVertex2d(_R-sw-trisize, SB-sw); tnsVertex2d(_R-sw, SB-sw); }
                    if(SB==_SB){ tnsPackAs(GL_TRIANGLES); }else{ tnsPackAs(GL_LINE_LOOP); }
                }
                else{
                    if(! CPP->Slot % 2){ tnsVertex2d(SL-sw, _B-sw); tnsVertex2d(SL-sw, _B-trisize-sw); tnsVertex2d(SL+trisize-sw, _B-sw); }
                    else{ tnsVertex2d(SL-trisize-sw, _U-sw); tnsVertex2d(SL-sw, _U+trisize-sw); tnsVertex2d(SL-sw, _U-sw); }
                    if(SL==_SL){ tnsPackAs(GL_TRIANGLES); }else{ tnsPackAs(GL_LINE_LOOP); }
                }
            }
        }
    }

    tnsFlush();
}
void la_ValueMeter2DDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    int DataI[8]; real Data[8] = {0};
    int Len, i, W; real Seg;
    char buf[48] = {0};
    char buf2[48] = {0};
    char prefix[8][64] = {0};
    int Original;
    int Imin=-100, Imax=100;
    real min=-100, max=100;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int sw=NoDecal?0:la_GetBoxOffset(bt,LA_BT_NORMAL);
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;

    int IsInt=(ui->PP.LastPs->p->PropertyType&LA_PROP_INT)?1:0;
    Len = laGetArrayLength(&ui->PP);
    laGetPrefixP(&ui->PP, prefix);
    if(IsInt){
        laGetIntArray(&ui->PP, DataI); for(int i=0;i<Len;i++){ Data[i]=DataI[i]; }
        laGetIntRange(&ui->PP, &Imin, &Imax); min=Imin; max=Imax;
    }else{
        laGetFloatArray(&ui->PP, Data);
        laGetFloatRange(&ui->PP,&min,&max);
    }

    int _L=ui->L, _R=ui->R, _U=ui->U, _B=ui->B;

    tnsUseNoTexture();

    if(!NoDecal){
        la_DrawBoxAutoFill(ui->L,ui->R,ui->U,ui->B,bt,LA_BT_NORMAL,0);
    }

    real CL=(real)(Data[0] - min) / (real)(max - min) * (real)(_R-_L) + _L +sw;
    real CU=(real)(max - Data[1]) / (real)(max - min) * (real)(_B-_U) + _U +sw;
    real HW=((real)_R-(real)_L)/4; real HH=((real)_B-(real)_U)/4;
    real ML=((real)_R+(real)_L)/2; real MU=((real)_B+(real)_U)/2;

    tnsUseNoTexture(); real* color=laThemeColor(bt,LA_BT_TEXT);
    tnsColor4d(LA_COLOR3(color),0.4);
    tnsVertex2d(ML+HW, MU+HH); tnsVertex2d(ML-HW, MU+HH);
    tnsVertex2d(ML-HW, MU-HH); tnsVertex2d(ML+HW, MU-HH);
    tnsPackAs(GL_LINE_LOOP);
    tnsVertex2d(_L,_U); tnsVertex2d(_R,_B);
    tnsVertex2d(_R,_U); tnsVertex2d(_L,_B);
    tnsVertex2d(ML,_U); tnsVertex2d(ML,_B);
    tnsVertex2d(_R,MU); tnsVertex2d(_L,MU);
    tnsPackAs(GL_LINES);

    if(!NoDecal){
        la_DrawBoxAutoBorder(ui->L,ui->R,ui->U,ui->B,bt,LA_BT_NORMAL);
    }

    tnsLineWidth(2);

    tnsColor4dv(laThemeColor(bt,LA_BT_TEXT));
    tnsVertex2d(_L-sw, CU); tnsVertex2d(_R-sw, CU);
    tnsVertex2d(CL-sw, _U); tnsVertex2d(CL, _B-sw);
    tnsPackAs(GL_LINES);

    real circle[48];
    tnsMakeCircle2d(circle,24,CL,CU,LA_RH2,0);
    tnsVertexArray2d(circle,24);
    tnsPackAs(GL_LINE_LOOP);
    
    tnsLineWidth(1);

    tnsFlush();
}

void la_ScopeDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme); laSynthNodeScope* s=ui->PP.EndInstance;
    if(!s) return;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int sw=NoDecal?0:la_GetBoxOffset(bt,LA_BT_NORMAL);
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;

    real* lcolor=laAccentColor(LA_BT_TEXT); int plen=LA_SYNTH_PLEN;
    int cl=ui->L+LA_M-sw, cr=ui->R-LA_M-sw, cu=ui->U+LA_M-sw, cb=ui->B-LA_M-sw; real cc=(real)(cu+cb)/2;
    real yfac1=(real)(1<<(s->Gain1+1))/4.0f, yfac2=(real)(1<<(s->Gain2+1))/4.0f; yfac1/=10; yfac2/=10;
    real off1=s->Offset1/20*(cb-cu), off2=s->Offset2/20*(cb-cu);
    real arrx[LA_SYNTH_PLEN];
    real arru1[LA_SYNTH_PLEN],arrb1[LA_SYNTH_PLEN];
    real arru2[LA_SYNTH_PLEN],arrb2[LA_SYNTH_PLEN];

    tnsFlush(); tnsUseNoTexture();
    
    if(!NoDecal){
        real bkg[4]={0,0,0,0.8};
        la_DrawBoxAutoFill(ui->L,ui->R,ui->U,ui->B,bt,LA_BT_NORMAL,bkg);
    }

    tnsLineWidth(3.0);

    laSpinLock(&s->Lock);

    real b1=s->Brightness1, b2=s->Brightness2; real hcy[3]; tnsRGB2HCY(lcolor,hcy); hcy[2]=0.75;
    real c1[3]; tnsHCY2RGB(hcy,c1);

    for(int i=0;i<plen;i++){
        arrx[i]=tnsInterpolate(cl,cr,((real)i)/plen);
        arru1[i]=tnsInterpolate(cc,cu,yfac1*s->Display1[i])-off1;
        arrb1[i]=tnsInterpolate(cc,cu,yfac1*s->Display1[i+plen])-off1;
        TNS_CLAMP(arru1[i],cu,cb); TNS_CLAMP(arrb1[i],cu,cb);
        tnsVertex2d(arrx[i],arru1[i]); tnsVertex2d(arrx[i],arrb1[i]);
    }
    tnsColor4d(LA_COLOR3(c1),b1*0.4);
    tnsPackAs(GL_TRIANGLE_STRIP); tnsFlush();

    for(int i=0;i<plen;i++){
        arru2[i]=tnsInterpolate(cc,cu,yfac2*s->Display2[i])-off2;
        arrb2[i]=tnsInterpolate(cc,cu,yfac2*s->Display2[i+plen])-off2;
        TNS_CLAMP(arru2[i],cu,cb); TNS_CLAMP(arrb2[i],cu,cb);
        tnsVertex2d(arrx[i],arru2[i]); tnsVertex2d(arrx[i],arrb2[i]);
    }
    tnsColor4d(LA_COLOR3(c1),b2*0.6);
    tnsPackAs(GL_TRIANGLE_STRIP); tnsFlush();

    for(int i=0;i<plen;i++){ tnsVertex2d(arrx[i],arru1[i]); }
    tnsColor4d(LA_COLOR3(c1),b1);
    tnsPackAs(GL_LINE_STRIP);
    for(int i=0;i<plen;i++){ tnsVertex2d(arrx[i],arrb1[i]); }
    tnsColor4d(LA_COLOR3(c1),b1);
    tnsPackAs(GL_LINE_STRIP);

    for(int i=0;i<plen;i++){ tnsVertex2d(arrx[i],arru2[i]); }
    tnsColor4d(LA_COLOR3(c1),b2);
    tnsPackAs(GL_LINE_STRIP);
    for(int i=0;i<plen;i++){ tnsVertex2d(arrx[i],arrb2[i]); }
    tnsColor4d(LA_COLOR3(c1),b2);
    tnsPackAs(GL_LINE_STRIP);

    laSpinUnlock(&s->Lock);

    tnsFlush(); tnsLineWidth(1.0);

    if(!NoDecal){
        la_DrawBoxAutoBorder(ui->L,ui->R,ui->U,ui->B,bt,LA_BT_NORMAL);
    }

    if(s->FromSynth && laget_SynthPlaying(s->FromSynth)){ laDeferredRedraw(MAIN.CurrentPanel); }
}

void la_DefaultOperatorParser(laStringSplitor *ss, uint32_t *IconID, char *DisplayString);
void la_MouseActionReporterDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    char buf[LA_RAW_CSTR_MAX_LEN] = {0};
    char *label = 0;
    uint32_t IconID=0;
    int L;
    char *Identifier;
    laOperatorProp *ap = 0;
    int NoDecal=ui->Flags&LA_UI_FLAGS_NO_DECAL;
    int IconOnly=ui->Flags&LA_UI_FLAGS_ICON;
    int sw=NoDecal?0:la_GetBoxOffset(bt,LA_BT_NORMAL);
    int sf=bt->TextShadow?LA_TEXT_SHADOW:0;

    if(!NoDecal){
        tnsUseNoTexture();
        la_DrawBoxAuto(ui->L,ui->R,ui->U,ui->B,bt,LA_BT_NORMAL,la_UiStateColorState(bt,ui,LA_BT_NORMAL));
    }

    if (ui->ExtraInstructions){
        uint32_t tIconID=0;
        la_DefaultOperatorParser(ui->Instructions, &tIconID, buf);
        if (buf[0]) label = transLate(buf);
        else label = transLate(SSTR(ui->Display));
    }else{
        label = transLate(SSTR(ui->Display));
    }
    if(label[0]=='\0'){ label="🖱"; }

    int UseU = (ui->B-ui->U-LA_RH)/2+ui->U;
    tnsDrawStringAuto(label,la_UiTextColorState(bt,ui,LA_BT_NORMAL), ui->L-sw, ui->R-sw, UseU-sw, ui->Flags|sf);
}

void la_ConditionNodeFreeRecursive(laUiConditionNode *ucn);
void la_GeneralUiInit(laUiItem *ui){
    ui->Extra = memAcquireSimple(sizeof(laGeneralUiExtraData));
    ui->Extra->ui=ui;
}
void la_GeneralUiDestroy(laUiItem *ui){
    if(ui->Extra && ui->Extra->Edit){ strEndEdit(&ui->Extra->Edit, 1); }
    memFree(ui->Extra);
}
void la_ConditionUiDestroy(laUiItem *ui){
    laConditionUiExtraData *cued = ui->Extra;
    la_ConditionNodeFreeRecursive(cued->Expression);
    FreeMem(ui->Extra);
}
void la_MultiStringInit(laUiItem *ui){
    laGeneralUiExtraData *e = ui->Extra;
    if (!e) e = memAcquireSimple(sizeof(laGeneralUiExtraData));
    ui->Extra = e;
    ui->Extent = 10;
}
void la_ImageUiInit(laUiItem *ui){
    tnsImage* im=ui->Extra;
#ifdef LA_WITH_PNG
    tnsUseImage(im);
#endif
}
void la_ImageUiDestroy(laUiItem *ui){
    tnsImage* im=ui->Extra;
#ifdef LA_WITH_PNG
    tnsStopUsingImage(im);
#endif
    //memAssignRef(ui, &ui->Extra, 0);
}
void la_MeterUiInit(laUiItem *ui){
    la_GeneralUiInit(ui); ui->Extent = 20;
}

void la_RegisterUiTypesBasic(){
    laKeyMapper* km;
    laPropContainer* pc;

    LA_WIDGET_INT->Type=LA_WIDGET_INT_PLAIN->Type=
    _LA_UI_INT = la_RegisterUiType("LA_int_array_horizon", LA_PROP_INT | LA_PROP_ARRAY, "LA_int_array_h_operator",
                        &_LA_THEME_VALUATOR, la_IntDraw, la_ValueGetHeight, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_INT->GetMinWidth=la_ValueGetMinWidth;

    LA_WIDGET_METER_TYPE1->Type=
    _LA_UI_METER_TYPE1 = la_RegisterUiType("LA_meter_type1", 0, 0, &_LA_THEME_VALUATOR, la_ValueMeterType1Draw, la_ValueMeterGetHeight, la_MeterUiInit, la_GeneralUiDestroy);
    _LA_UI_METER_TYPE1->GetMinWidth=la_ValueGetMinWidth;

    LA_WIDGET_METER_TYPE2->Type=
    _LA_UI_METER_TYPE2 = la_RegisterUiType("LA_meter_type2", 0, 0, &_LA_THEME_VALUATOR, la_ValueMeterType2Draw, la_ValueMeterGetHeight, la_MeterUiInit, la_GeneralUiDestroy);
    _LA_UI_METER_TYPE2->GetMinWidth=la_ValueGetMinWidth;
    
    LA_WIDGET_VALUE_METER_2D->Type=
    _LA_UI_VALUE_METER_2D = la_RegisterUiType("LA_value_meter_2d", 0, 0, &_LA_THEME_VALUATOR, la_ValueMeter2DDraw, la_ValueMeter2DGetHeight, la_MeterUiInit, la_GeneralUiDestroy);

    LA_WIDGET_FLOAT->Type=LA_WIDGET_FLOAT_PLAIN->Type=
    _LA_UI_FLOAT = la_RegisterUiType("LA_real_array_horizon", LA_PROP_FLOAT | LA_PROP_ARRAY, "LA_real_array_h_operator",
                        &_LA_THEME_VALUATOR, la_FloatDraw, la_ValueGetHeight, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_FLOAT->GetMinWidth=la_ValueGetMinWidth;

    LA_WIDGET_FLOAT_COLOR->Type=
    _LA_UI_FLOAT_COLOR = la_RegisterUiType("LA_real_color", LA_PROP_FLOAT | LA_PROP_ARRAY, "LA_real_color_operator",
                        &_LA_THEME_VALUATOR, la_FloatArrayColorDraw, 0, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_FLOAT_COLOR->GetMinWidth=la_ColorSelectorGetMinWidth;

    LA_WIDGET_FLOAT_COLOR_HCY->Type=
    _LA_UI_FLOAT_COLOR_HCY = la_RegisterUiType("LA_real_hcy", LA_PROP_FLOAT | LA_PROP_ARRAY, "LA_real_hcy_operator",
                        &_LA_THEME_VALUATOR, la_ColorCircleDrawHCY, la_ColorPickerGetHeight, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_FLOAT_COLOR_HCY->GetMinWidth = la_ColorPickerGetMinWidth;

    LA_WIDGET_ENUM_SELECTOR->Type=LA_WIDGET_ENUM_SELECTOR_ICON->Type=LA_WIDGET_ENUM_CYCLE->Type=LA_WIDGET_ENUM_HIGHLIGHT->Type=
    _LA_UI_ENUM_SELECTOR = la_RegisterUiType("LA_enum_selector", LA_PROP_ENUM, "LA_enum_selector_operator",
                        &_LA_THEME_SELECTOR, la_EnumSelectorDraw, la_EnumGetHeight, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_ENUM_SELECTOR->GetMinWidth = la_EnumGetMinWidth;

    LA_WIDGET_FIXED_GROUP->Type=
    _LA_UI_FIXED_GROUP = la_RegisterUiType("LA_group_default", 0, 0,
                        &_LA_THEME_COLLECTION_GROUP, la_GroupDraw, la_GroupGetHeight, la_GeneralUiInit, la_GeneralUiDestroy);
                        _LA_UI_FIXED_GROUP->Tag|=LA_UI_TAG_SCISSOR;

    LA_WIDGET_COLLECTION->Type=
    _LA_UI_COLLECTION = la_RegisterUiType("LA_collection_default", LA_PROP_SUB, "LA_collection_operator",
                        &_LA_THEME_COLLECTION_GROUP, la_EmptyDraw, 0, la_GeneralUiInit, la_GeneralUiDestroy);

    LA_WIDGET_COLLECTION_SELECTOR->Type=
    _LA_UI_COLLECTION_SELECTOR = la_RegisterUiType("LA_collection_selector_default", LA_PROP_SUB, "LA_collection_selector_operator",
                        &_LA_THEME_COLLECTION_SELECTOR, la_CollectionSelectorDraw, 0, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_COLLECTION_SELECTOR->GetMinWidth = la_CollectionSimpleGetMinWidth;

    LA_WIDGET_COLLECTION_SINGLE->Type=
    _LA_UI_COLLECTION_SINGLE = la_RegisterUiType("LA_collection_single", LA_PROP_SUB, "LA_collection_operator",
                        &_LA_THEME_COLLECTION_GROUP, la_EmptyDraw, 0, la_GeneralUiInit, la_GeneralUiDestroy);

    LA_WIDGET_TAB->Type=
    _LA_UI_TAB = la_RegisterUiType("LA_tab_default", 0, "LA_tab_operator",
                        &_LA_THEME_TAB, la_TabDraw, 0, la_GeneralUiInit, la_GeneralUiDestroy);

    LA_WIDGET_BUTTON->Type=LA_WIDGET_BUTTON_NO_CONFIRM->Type=
    _LA_UI_BUTTON = la_RegisterUiType("LA_button_default", LA_PROP_OPERATOR, "LA_button_operator",
                        &_LA_THEME_BUTTON, la_ButtonDraw, 0, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_BUTTON->GetMinWidth = la_ButtonGetMinWidth;
    LA_WIDGET_BUTTON_NO_CONFIRM->Flags|=LA_UI_FLAGS_NO_CONFIRM;
    pc=laUiHasExtraProps(_LA_UI_BUTTON, sizeof(laGeneralUiExtraData),0);

    LA_WIDGET_LABEL->Type=
    _LA_UI_LABEL = la_RegisterUiType("LA_label_default", LA_PROP_OPERATOR, 0,
                        &_LA_THEME_LABEL, la_LabelDraw, la_LabelHeight, 0, 0);
    _LA_UI_LABEL->GetMinWidth = la_LabelGetMinWidth;

    LA_WIDGET_MENU_ROOT->Type=
    _LA_UI_MENU_ROOT = la_RegisterUiType("LA_menu_default", LA_PROP_OPERATOR, "LA_menu_root_operator",
                        &_LA_THEME_BUTTON, la_MenuItemDraw, 0, 0, 0);
    _LA_UI_MENU_ROOT->GetMinWidth=la_LabelGetMinWidth;

    LA_WIDGET_CONDITION_TOGGLE->Type=
    _LA_UI_CONDITION_TOGGLE = la_RegisterUiType("LA_condition_toggle_default", LA_PROP_SELF_CONDITION, "LA_condition_toggle_operator",
                        &_LA_THEME_BUTTON, la_ConditionToggleDraw, 0, 0, la_ConditionUiDestroy);

    LA_WIDGET_STRING->Type=LA_WIDGET_STRING_PLAIN->Type=LA_WIDGET_STRING_MONO_PLAIN->Type=
    _LA_UI_STRING = la_RegisterUiType("LA_string_default", LA_PROP_STRING, "LA_string_operator",
                        &_LA_THEME_STRING, la_SingleLineStringDraw, la_StringPropGetHeight, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_STRING->GetMinWidth = la_StringPropGetMinWidth;
    km=&_LA_UI_STRING->KeyMapper;
    laAssignNewKey(km, 0, "LA_edit_string_copy", LA_KM_SEL_UI_EXTRA, LA_KEY_CTRL, LA_KEY_DOWN, 'c', 0);
    laAssignNewKey(km, 0, "LA_edit_string_copy", LA_KM_SEL_UI_EXTRA, LA_KEY_CTRL, LA_KEY_DOWN, 'x', "mode=CUT");
    laAssignNewKey(km, 0, "LA_system_paste", 0, LA_KEY_CTRL, LA_KEY_DOWN, 'v', 0);
    pc=laUiHasExtraProps(_LA_UI_STRING, sizeof(laGeneralUiExtraData), 0);{
        laAddOperatorProperty(pc, "copy_selection", "Copy Selection", "Copy selected string into clipboard", "LA_edit_string_copy",0,0);
    }

    LA_WIDGET_STRING_MULTI->Type=
    _LA_UI_STRING_MULTI = la_RegisterUiType("LA_string_multiline", LA_PROP_STRING, "LA_string_multi",
                        &_LA_THEME_STRING, la_MultiStringDraw, la_VariedGetHeight, la_MultiStringInit, la_GeneralUiDestroy);
    _LA_UI_STRING_MULTI->GetMinWidth = la_StringPropGetMinWidth;
    km=&_LA_UI_STRING_MULTI->KeyMapper;
    laAssignNewKey(km, 0, "LA_edit_string_copy", LA_KM_SEL_UI_EXTRA, LA_KEY_CTRL, LA_KEY_DOWN, 'c', 0);
    laAssignNewKey(km, 0, "LA_edit_string_copy", LA_KM_SEL_UI_EXTRA, LA_KEY_CTRL, LA_KEY_DOWN, 'x', "mode=CUT");
    laAssignNewKey(km, 0, "LA_system_paste", 0, LA_KEY_CTRL, LA_KEY_DOWN, 'v', 0);
    pc=laUiHasExtraProps(_LA_UI_STRING_MULTI, sizeof(laGeneralUiExtraData), 0);{
        laAddOperatorProperty(pc, "copy_selection", "Copy Selection", "Copy selected string into clipboard", "LA_edit_string_copy",0,0);
    }

    LA_WIDGET_ALIGN->Type=
    _LA_UI_ALIGN = la_RegisterUiType("LA_colum_align_default", 0, 0,
                        &_LA_THEME_BUTTON, la_UiSeperatorDraw, 0, 0, 0);

    LA_WIDGET_COLUMN_ADJUSTER->Type=
    _LA_UI_COLUMN_ADJUSTER = la_RegisterUiType("LA_column_adjuster_default", 0, "LA_column_adjuster",
                        &_LA_THEME_BUTTON, la_ColumnAdjusterDraw, 0, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_COLUMN_ADJUSTER->TargetSub = "ui_column";

    LA_WIDGET_COLUMN_VIEWER->Type=
    _LA_UI_COLUMN_VIEWER = la_RegisterUiType("LA_column_viewer_default", 0, 0,
                        &_LA_THEME_BUTTON, la_ColumnViewerDraw, 0, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_COLUMN_VIEWER->TargetSub = "ui_column";
    _LA_UI_COLUMN_VIEWER->GetMinWidth = la_ColumnViewerGetMinWidth;

    _LA_UI_CONDITION_ELSE.Theme = &_LA_THEME_BUTTON;
    la_UDFAppendSharedTypePointer("LA_condition_else", &_LA_UI_CONDITION_ELSE);
    _LA_UI_CONDITION.Destroy = _LA_UI_CONDITION_ELSE.Destroy = _LA_UI_CONDITION_END.Destroy =la_ConditionUiDestroy; 
    _LA_UI_CONDITION.Theme = &_LA_THEME_BUTTON;
    la_UDFAppendSharedTypePointer("LA_condition", &_LA_UI_CONDITION);
    _LA_UI_CONDITION_END.Theme = &_LA_THEME_BUTTON;
    la_UDFAppendSharedTypePointer("LA_condition_end", &_LA_UI_CONDITION_END);

    LA_WIDGET_NODE_SOCKET->Type=
    _LA_UI_NODE_SOCKET = la_RegisterUiType("LA_node_socket_default", 0, "LA_node_socket", &_LA_THEME_SOCKET, la_NodeSocketDraw, la_SocketGetHeight, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_NODE_SOCKET->GetMinWidth=la_SocketGetMinWidth;
    
    LA_WIDGET_HEIGHT_ADJUSTER->Type=
    _LA_UI_HEIGHT_ADJUSTER = la_RegisterUiType("LA_height_adjuster_default", 0, "LA_height_adjuster", &_LA_THEME_COLLECTION_GROUP, la_HeightAdjusterDraw, 0, la_GeneralUiInit, la_GeneralUiDestroy);
    _LA_UI_HEIGHT_ADJUSTER->ForType = LA_PROP_INT;

    LA_WIDGET_RAW->Type=
    _LA_UI_RAW = la_RegisterUiType("LA_raw_prop_default", 0, 0, &_LA_THEME_COLLECTION_GROUP, la_RawPropDraw, 0, 0, 0);
    _LA_UI_RAW->ForType = LA_PROP_RAW;

    LA_WIDGET_MAPPER->Type=
    _LA_UI_MAPPER = la_RegisterUiType("LA_mapper_default", 0, "LA_value_mapper", &_LA_THEME_COLLECTION_GROUP, la_MapperDraw, la_ColorPickerGetHeight, la_GeneralUiInit, la_GeneralUiDestroy);

    LA_WIDGET_IMAGE->Type=
    _LA_UI_IMAGE = la_RegisterUiType("LA_image_default", 0, 0, &_LA_THEME_COLLECTION_GROUP, la_ImageDraw, la_ImageGetHeight, la_ImageUiInit, la_ImageUiDestroy);
    _LA_UI_IMAGE->GetMinWidth=la_ImageGetMinWidth;

    LA_WIDGET_SCOPE->Type=
    _LA_UI_SCOPE = la_RegisterUiType("LA_scope_default", 0, "LA_synth_scope", &_LA_THEME_BUTTON, la_ScopeDraw, la_ColorPickerGetHeight, la_GeneralUiInit, la_GeneralUiDestroy);

    LA_WIDGET_MOUSE_ACTION_REPORTER->Type=
    _LA_UI_MOUSE_ACTION_REPORTER = la_RegisterUiType("LA_mouse_action_reporter_default",0,"LA_mouse_action_reporter", &_LA_THEME_STRING,la_MouseActionReporterDraw,la_VariedGetHeight,0,0);
    _LA_UI_MOUSE_ACTION_REPORTER->GetMinWidth = la_MouseActionReporterMinWidth;

    _LA_UI_INVISIBLE.Theme=&_LA_THEME_BUTTON;
    _LA_UI_ROW_BEGIN.Theme=&_LA_THEME_BUTTON;
    _LA_UI_ROW_END.Theme=&_LA_THEME_BUTTON;
}

// ============================================================ [Operators]

void la_DefaultEnumPanel(laUiList *uil, laPropPack *This, laPropPack *OperatorProps, laColumn *UNUSED){
    laColumn *c = laFirstColumn(uil);
    laShowItem(uil, c, This, 0)->Flags=(LA_UI_FLAGS_TRANSPOSE|LA_UI_FLAGS_EXPAND);
}

int la_DetectRow(laUiItem *ui, int LocalY){
    return (LocalY - ui->U) / LA_RH;
}
int la_DetectColumn(laUiItem *ui, int LocalX, int Total){
    return (int)((real)(LocalX - ui->L) / (real)(ui->R - ui->L) * (real)Total);
}

laColumn *la_DetectSplit(laColumn *Root, int LocalX){
    laColumn *result = 0;
    if (!Root->LS) return 0;
    if (LocalX - Root->LS->IR < LA_SCROLL_W && Root->LS->IR - LocalX < LA_SCROLL_W &&
        !Root->LS->MaxW && !Root->RS->MaxW){
        return Root;
    }
    result = la_DetectSplit(Root->LS, LocalX);
    if (result) return result;
    else
        return la_DetectSplit(Root->RS, LocalX);
}
void la_AdjustColumnSplit(laColumn *c, int PxLeftPositive){
    if (!c->LS || c->LS->MaxW || c->RS->MaxW) return;
    if (PxLeftPositive < 0 && c->LS->IR - c->LS->IL <= LA_SCROLL_W*2) return;
    if (PxLeftPositive > 0 && c->RS->IR - c->RS->IL <= LA_SCROLL_W*2) return;
    c->LS->IR += PxLeftPositive;
    c->RS->IL += PxLeftPositive;
    c->LS->SP = (real)(c->LS->IR - c->LS->IL) / (real)(c->IR - c->IL);
    c->RS->SP = c->LS->SP;
}

int OPCHK_EditStringCopySelection(laPropPack *This, laStringSplitor *Instructions){
    laGeneralUiExtraData *uit = This->EndInstance; if(!uit) return 0;
    if(!uit->Edit || !strHasSelection(uit->Edit)) return 0;
    return 1;
}
int OPINV_EditStringCopySelection(laOperator *a, laEvent *e){
    laGeneralUiExtraData *uit = a->This->EndInstance; if(!uit) return LA_FINISHED;
    if(!uit->Edit || !strHasSelection(uit->Edit)) return LA_FINISHED;

    char* mode=strGetArgumentString(a->ExtraInstructionsP,"mode");

    char* str=strGetEditString(uit->Edit, 1);
    laCopyToClipboard(str); free(str);

    if(strSame(mode,"CUT")){ strClearSelection(uit->Edit); laConfirmInt(a,0,LA_CONFIRM_TEXT_EDIT);
        laRedrawCurrentPanel(); return LA_FINISHED_PASS;
    }

    return LA_FINISHED;
}

int la_AccpetedUnicodeInput(uint32_t ch);
int la_ProcessTextEdit(laEvent *e, laStringEdit *se, laUiItem* ui){
    int Select=e->SpecialKeyBit&LA_KEY_SHIFT;
    real _L, MonoWidth;
    unsigned char* pasted;
    switch (e->type){
    case LA_INPUT:
        switch (e->Input){
        case 0x08: if(strHasSelection(se)) strClearSelection(se); else strBackspace(se); return 1;
        case 0x7f: if(strHasSelection(se)) strClearSelection(se); else { strMoveCursor(se,0,0); strBackspace(se); } return 1;
        case 0x09: strInsertChar(se, ' ');strInsertChar(se, ' ');strInsertChar(se, ' ');strInsertChar(se, ' '); return 1;
        case 0x1B: case 0x0D: return 0;
        default: if(e->Input>=32) strInsertChar(se, e->Input); return 1;
        }
        break;
    case LA_PASTE:
        pasted=MAIN.PasteString?MAIN.PasteString->Ptr:0; if(!pasted) break;
        uint32_t uni = 0, adv = 0;
        while(uni=laToUnicode(pasted,&adv)){ pasted+=adv; if(la_AccpetedUnicodeInput(uni)){ strInsertChar(se, uni); } }
        return 1;
    case LA_KEY_DOWN:
        switch (e->key){
        case LA_KEY_ENTER: strInsertChar(se, '\n'); return 1;
        case LA_KEY_ARRLEFT: strMoveCursor(se, 1, Select); return 1;
        case LA_KEY_ARRRIGHT: strMoveCursor(se, 0, Select); return 1;
        case LA_KEY_ARRUP: strMoveCursorLine(se, 1, Select); return 1;
        case LA_KEY_ARRDOWN: strMoveCursorLine(se, 0, Select); return 1;
#ifdef LAGUI_ANDROID
        case LA_KEY_BACKSPACE: if(strHasSelection(se)) strClearSelection(se); else strBackspace(se); return 1;
#endif
        }
        break;
    case LA_L_MOUSE_DOWN:
    case LA_MOUSEMOVE:
        if(!ui) return 0;
        MonoWidth=tnsGetMonoFontAdvance(); _L=LA_M*2+ui->L;
        int Count=se->TotalLines; while(Count){ Count/=10; _L+=MonoWidth; }
        if(e->type==LA_L_MOUSE_DOWN){
            strSetCursor(se, (e->y-ui->U)/LA_RH+se->ViewStartLine, (real)se->ViewStartCol+((real)e->x-_L)/MonoWidth+0.5); strCancelSelect(se); strLazySelect(se);
            se->MouseSelecting=1; se->CursorPreferBefore=se->CursorBefore; return 1;
        }else{
            if(!se->MouseSelecting) return 0;
            strSetCursor(se, (e->y-ui->U)/LA_RH+se->ViewStartLine, (real)se->ViewStartCol+((real)e->x-_L)/MonoWidth+0.5); strEndSelect(se);
            se->CursorPreferBefore=se->CursorBefore; return 1;
        }return 0;
    case LA_L_MOUSE_UP:
        se->MouseSelecting=0;
    }
    return 0;
}

int OPINV_UiItem(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    if (!laIsInUiItem(ui, a, e->x, e->y)){
        return LA_FINISHED;
    }
    a->CustomData = ui->Extra;
    return LA_RUNNING;
}
int OPINV_Tab(laOperator *a, laEvent *e){
    laUiList *uil;
    laUiItem *ui = a->Instance;
    laGeneralUiExtraData *uit;
    int count = 0;

    //if (e->Type == LA_L_MOUSE_DOWN) {
    //}

    a->CustomData = ui->Extra;

    uit = a->CustomData;
    uit->TargetIndexVali = count;

    return LA_RUNNING;
}

int OPEXT_UiItem(laOperator *a, int ExitCode){
    laUiItem *ui = a->Instance;
    return 0;
}

int OPMOD_IntArrayHorizon(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    laIntProp *ip = ui->PP.LastPs->p;
    int Away = 0;
    int TmpArr[32];
    int Len = laGetArrayLength(&ui->PP);
    int IsVertical=(ui->Flags&LA_UI_FLAGS_TRANSPOSE)!=0;
    int NoEvent = ui->Flags&LA_UI_FLAGS_NO_EVENT;
    int NoTooltip=ui->Flags&LA_UI_FLAGS_NO_TOOLTIP;
    int IsKnob = ui->Flags&LA_UI_FLAGS_NODE_CONTAINER || ui->Flags&LA_UI_FLAGS_VERTICAL_SLIDER;

    if (!laIsInUiItem(ui, a, e->x, e->y) && !ui->Extra->On){
        ui->State = LA_UI_NORMAL;
        if (ui->Extra) ui->Extra->On = 0;
        laRedrawCurrentPanel();
        return LA_FINISHED_PASS;
    }

    if (e->type == LA_TIME_IDLE && !uit->On && !uit->Edit && !NoTooltip){
        int GX = e->x, GY = e->y; laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnableIdlePanel(a->ToPanel, a, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }

    if(NoEvent){ return LA_RUNNING_PASS; }

    if (e->type == LA_L_MOUSE_DOWN){
        if (laIsPropertyReadOnly(&ui->PP)) return LA_RUNNING_PASS;
        if (uit->Edit){
            if (!laIsInUiItem(ui, a, e->x, e->y)){
                strEndEdit(&uit->Edit, 1);
                uit->On = 0;
                laRedrawCurrentPanel(); la_DisplayKeyboard(0);
                return LA_FINISHED;
            }
            if (strHasSelection(uit->Edit)) strDeselectAll(uit->Edit);
            else strSelectLineAll(uit->Edit); la_DisplayKeyboard(1);
            return LA_RUNNING;
        }
        uit->LastX = e->x;
        uit->LastY = e->y;
        ui->Extra->On = (IsVertical ? la_DetectRow(ui, e->y) : la_DetectColumn(ui, e->x, Len)) + 1;
        if (ui->Extra->On > Len) return LA_RUNNING;
        laGetIntArray(&ui->PP, &TmpArr);
        uit->TargetIndexValf = TmpArr[ui->Extra->On - 1];
        laRedrawCurrentPanel();
        return LA_RUNNING;
    }
    if (e->type == LA_MOUSEMOVE && ui->Extra->On){
        if(ui->Extra->Edit){ return LA_RUNNING; }
        int dist=abs(e->x - uit->LastX + uit->LastY - e->y);
        if (dist > MAIN.ValuatorThreshold || uit->Dragging){
            int min,max,Ranged = laGetIntRange(&ui->PP, &min, &max); 
            int UseRange = (Ranged&&!IsKnob); real step=ip->Step; if(UseRange){ step=(real)(max-min)/(ui->R-ui->L); }
            if(Ranged){ TNS_CLAMP(step,(max-min)/500,INT_MAX); }
            int delta=e->x-uit->LastX; if(IsKnob){ int dy=uit->LastY-e->y; delta=delta+dy; }
            if(UseRange || (abs(delta) > MAIN.ValuatorThreshold)){
                if((!uit->Dragging ) || !(UseRange)) delta=delta>0?1:-1;
                uit->TargetIndexValf += delta*step;
                laSetIntArraySingle(&ui->PP, ui->Extra->On - 1, uit->TargetIndexValf);
                uit->LastX = e->x;
                uit->LastY = e->y;
            }
            uit->Dragging = 1;
            laRedrawCurrentPanel();
        }
        return LA_RUNNING;
    }
    if (e->type == LA_L_MOUSE_UP){
        if (laIsPropertyReadOnly(&ui->PP)) return LA_RUNNING_PASS;
        if (uit->Dragging){
            if (ui->Extra->On){
                ui->Extra->On = 0;
                laConfirmInt(a, uit->TargetIndexVali, LA_CONFIRM_DATA);
                laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
            }
        }else if(ui->Extra->On){
            int SelectOn=(IsVertical ? la_DetectRow(ui, e->y) : la_DetectColumn(ui, e->x, Len)) + 1; 
            if ((!uit->Edit) || (uit->Edit && (SelectOn != ui->Extra->On))){
                char buf[32] = {0};
                uit->On=SelectOn;
                laGetIntArray(&ui->PP, &TmpArr);
                uit->TargetIndexVali = TmpArr[ui->Extra->On - 1];
                strPrintIntAfter(buf, 32, uit->TargetIndexVali);
                strBeginEdit(&uit->Edit,buf);
                strSelectLineAll(uit->Edit); la_DisplayKeyboard(1);
            }
        }
        uit->Dragging = 0;
        laRedrawCurrentPanel();
        return LA_RUNNING;
    }

    if(e->type ==LA_KEY_DOWN){
        if (e->key == LA_KEY_ENTER && uit->Edit){
            char* buf=strEndEdit(&uit->Edit, 0);
            int Result; sscanf(buf, "%d", &Result); free(buf);
            laSetIntArraySingle(&ui->PP, ui->Extra->On - 1, Result);
            laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
            ui->State = LA_UI_NORMAL;
            uit->Dragging = 0;
            uit->On = 0;
            laRedrawCurrentPanel(); la_DisplayKeyboard(0);
            return LA_FINISHED_PASS;
        }
    }
    
    if (e->type == LA_R_MOUSE_DOWN){
        int GX = e->x, GY = e->y;
        ui->PP.LastIndex = la_DetectColumn(ui, e->x, Len);
        laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnablePropertyPanel(a->ToPanel, a, 0, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }

    if (uit->Edit){
        if (la_ProcessTextEdit(e, uit->Edit, ui)) laRedrawCurrentPanel();
        if (a->ConfirmData){
            if(a->ConfirmData->Mode == LA_CONFIRM_TEXT_EDIT){ laRedrawCurrentPanel(); }
        }
        return LA_RUNNING;
    }

    return LA_RUNNING_PASS;
}
int OPMOD_FloatArrayHorizon(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    laFloatProp *fp = ui->PP.LastPs->p;
    int Away = 0;
    real TmpArr[32];
    int Len = laGetArrayLength(&ui->PP);
    int IsVertical=(ui->Flags&LA_UI_FLAGS_TRANSPOSE)!=0;
    int NoEvent = ui->Flags&LA_UI_FLAGS_NO_EVENT;
    int IsRad=ui->PP.LastPs->p->IsRadAngle;
    int NoTooltip=ui->Flags&LA_UI_FLAGS_NO_TOOLTIP;
    int IsKnob = ui->Flags&LA_UI_FLAGS_NODE_CONTAINER || ui->Flags&LA_UI_FLAGS_VERTICAL_SLIDER;

    if (!laIsInUiItem(ui, a, e->x, e->y) && !ui->Extra->On && !NoTooltip){
        ui->State = LA_UI_NORMAL;
        ui->Extra->On = 0;
        laRedrawCurrentPanel();
        return LA_FINISHED_PASS;
    }

    if (e->type == LA_TIME_IDLE && !uit->On && !uit->Edit){
        int GX = e->x, GY = e->y; laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnableIdlePanel(a->ToPanel, a, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }

    if(NoEvent){ return LA_RUNNING_PASS; }

    if (e->type == LA_L_MOUSE_DOWN){
        if (uit->Edit){
            if (!laIsInUiItem(ui, a, e->x, e->y)){
                strEndEdit(&uit->Edit, 1);
                uit->On = 0;
                laRedrawCurrentPanel(); la_DisplayKeyboard(0);
                return LA_FINISHED;
            }
            if (strHasSelection(uit->Edit)) strDeselectAll(uit->Edit);
            else strSelectLineAll(uit->Edit); la_DisplayKeyboard(1);
            return LA_RUNNING;
        }
        if (laIsPropertyReadOnly(&ui->PP)) return LA_RUNNING_PASS;
        uit->LastX = e->x;
        uit->LastY = e->y;
        ui->Extra->On = (IsVertical ? la_DetectRow(ui, e->y) : la_DetectColumn(ui, e->x, Len)) + 1;
        if (ui->Extra->On > Len) return LA_RUNNING;
        laGetFloatArray(&ui->PP, &TmpArr);
        uit->TargetIndexValf = TmpArr[ui->Extra->On - 1];
        laRedrawCurrentPanel();

        return LA_RUNNING;
    }
    if (e->type == LA_MOUSEMOVE && ui->Extra->On){
        if(uit->Edit){ return LA_RUNNING; }
        int dist=abs(e->x - uit->LastX - e->y + uit->LastY);
        if (dist > MAIN.ValuatorThreshold || uit->Dragging){
            real min,max; int Ranged = laGetFloatRange(&ui->PP, &min, &max); 
            int UseRange = (Ranged&&!IsKnob); real step=fp->Step; if(UseRange){ step=(max-min)/(ui->R-ui->L); }
            if(Ranged){ TNS_CLAMP(step,(max-min)/500,INT_MAX); }
            laGetFloatArray(&ui->PP, &TmpArr);
            uit->TargetIndexValf = TmpArr[ui->Extra->On - 1];
            int delta=e->x-uit->LastX; if(IsKnob){ int dy=uit->LastY-e->y; delta=delta+dy; }
            if(UseRange || (abs(delta) > MAIN.ValuatorThreshold)){
                if((!uit->Dragging ) || !(UseRange)) delta=delta>0?1:-1;
                uit->TargetIndexValf +=  delta*step;
                laSetFloatArraySingle(&ui->PP, ui->Extra->On - 1, uit->TargetIndexValf);
                uit->LastX = e->x;
                uit->LastY = e->y;
            }
            uit->Dragging = 1;
            laRedrawCurrentPanel();
        }
        return LA_RUNNING;
    }
    if (e->type == LA_L_MOUSE_UP){
        if (laIsPropertyReadOnly(&ui->PP)) return LA_RUNNING_PASS;
        if (uit->Dragging){
            if (ui->Extra->On){
                ui->Extra->On = 0;
                laConfirmFloat(a, uit->TargetIndexValf, LA_CONFIRM_DATA);
                laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
            }
        }else if(ui->Extra->On){
            int SelectOn=(IsVertical ? la_DetectRow(ui, e->y) : la_DetectColumn(ui, e->x, Len)) + 1; 
            if ((!uit->Edit) || (uit->Edit && (SelectOn != ui->Extra->On))){
                char buf[32] = {0};
                uit->On=SelectOn;
                laGetFloatArray(&ui->PP, &TmpArr);
                uit->TargetIndexValf = TmpArr[ui->Extra->On - 1];
                strPrintFloatAfter(buf, 32, 5, IsRad?deg(uit->TargetIndexValf):uit->TargetIndexValf);
                strBeginEdit(&uit->Edit,buf);
                strSelectLineAll(uit->Edit); la_DisplayKeyboard(1);
            }
        }
        uit->Dragging = 0;
        laRedrawCurrentPanel();
        return LA_RUNNING;
    }

    if(e->type ==LA_KEY_DOWN){
        if (e->key == LA_KEY_ENTER && uit->Edit){
            char* buf=strEndEdit(&uit->Edit, 0);
            real Result; sscanf(buf, "%lf", &Result); free(buf);
            if(IsRad) Result=rad(Result);
            laSetFloatArraySingle(&ui->PP, ui->Extra->On - 1, Result);
            laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
            uit->Dragging = 0;
            uit->On = 0;
            laRedrawCurrentPanel(); la_DisplayKeyboard(0);
            return LA_FINISHED_PASS;
        }
    }

    if (e->type == LA_R_MOUSE_DOWN){
        int GX = e->x, GY = e->y;
        ui->PP.LastIndex = la_DetectColumn(ui, e->x, laGetArrayLength(&ui->PP));
        laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnablePropertyPanel(a->ToPanel, a, 0, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }

    if (uit->Edit){
        if (la_ProcessTextEdit(e, uit->Edit, ui)) laRedrawCurrentPanel();
        if (a->ConfirmData){
            if(a->ConfirmData->Mode == LA_CONFIRM_TEXT_EDIT){ laRedrawCurrentPanel(); }
        }
        return LA_RUNNING;
    }

    return LA_RUNNING_PASS;
}
int OPMOD_FloatColor(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    int Away = 0;
    int NoEvent = ui->Flags&LA_UI_FLAGS_NO_EVENT;
    int NoTooltip=ui->Flags&LA_UI_FLAGS_NO_TOOLTIP;

    if (!laIsInUiItem(ui, a, e->x, e->y)){
        return LA_FINISHED_PASS;
    }

    if (e->type == LA_TIME_IDLE && !NoTooltip){
        int GX = e->x, GY = e->y; laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnableIdlePanel(a->ToPanel, a, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }
    
    if(NoEvent){ return LA_RUNNING_PASS; }

    if (e->type == LA_L_MOUSE_DOWN){
        laUiList *uil;
        laColumn *col;
        laPanel *p;
        int GX = ui->L, GY = ui->B;
        ui->State = LA_UI_ACTIVE;
        laLocalToWindow(a, a->ToPanel, &GX, &GY);
        p = laEnableEmptyPropertyPanel(a->ToPanel, a, GX, GX + 150, GY, 500, e);{
            uil = &p->UI;
            col = laFirstColumn(uil);
            laShowItemFull(uil, col, &ui->PP, 0, LA_WIDGET_FLOAT_COLOR_HCY, 0, 0, 0);
            laShowItemFull(uil, col, &ui->PP, 0, LA_WIDGET_FLOAT, 0, 0, 0)->Flags=LA_UI_FLAGS_TRANSPOSE;
            laEnclosePanelContent(p, uil);
        }
        return LA_RUNNING;
    }
    if (e->type == LA_R_MOUSE_DOWN){
        int GX = e->x, GY = e->y;
        laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnablePropertyPanel(a->ToPanel, a, 0, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }

    return LA_RUNNING_PASS;
}
int OPMOD_ColorHCY(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *es = a->CustomData;
    int Away = 0;
    int i = 0;
    real hcy[3] = {0};
    real Color[4] = {0};
    real PickerPos[2];
    real abso;

    if (!laIsInUiItem(ui, a, e->x, e->y) && !es->Dragging){
        return LA_FINISHED_PASS;
    }

    if(e->type == LA_L_MOUSE_DOWN && ui->Extent>=0 && e->x>ui->R-LA_M-LA_RH && e->y>ui->B-LA_M-LA_RH){
        es->Dragging=3; es->LastX=e->x; es->LastY=e->y; es->TargetIndexVali=ui->Extent; return LA_RUNNING;
    }
    if(es->Dragging==3){
        if(e->type==LA_MOUSEMOVE){ ui->Extent=es->TargetIndexVali+((real)e->y-es->LastY+0.5)/LA_RH;
            if(ui->Extent<1)ui->Extent=1; laRecalcCurrentPanel();}
        elif(e->type==LA_L_MOUSE_UP){ es->Dragging=0; }
        elif(e->type==LA_R_MOUSE_DOWN){
            es->Dragging=0; ui->Extent=es->TargetIndexVali; laRecalcCurrentPanel(); }
        return LA_RUNNING;
    }

    real c = (real)((ui->R + ui->L) / 2) - LA_RH / 2; /*- LA_M;*/
    real r = (ui->B - ui->U - LA_M - LA_M) / 2;
    real dx = e->x - c, dy = e->y - (ui->U + r + LA_M);
    real dist = sqrt(dx * dx + dy * dy);

    if (e->type == LA_L_MOUSE_DOWN){ ui->State = LA_UI_ACTIVE; laRedrawCurrentPanel(); es->Dragging=(dist<=r)?1:2; }
    if (e->type == LA_L_MOUSE_UP){
        ui->State = LA_UI_NORMAL; es->Dragging=0; laRedrawCurrentPanel();
        laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
        return LA_RUNNING_PASS;
    }

    if (e->type & LA_MOUSE_EVENT && es->Dragging){
        laGetFloatArray(&ui->PP, Color);
        if(ui->PP.LastPs->p->Tag&LA_PROP_IS_LINEAR_SRGB){ tns2LogsRGB(Color); }
        tnsRGB2HCY(Color, hcy);
        if (es->Dragging==1){
            abso = fabs(atan((real)dy / (real)dx));
            hcy[0] = abso / TNS_PI / 2;
            if (dx > 0){
                if (dy < 0) hcy[0] = 1 - hcy[0];
            }else{
                if (dy > 0){
                    hcy[0] = 0.5 - hcy[0];
                }else{
                    hcy[0] += 0.5;
                }
            }
            hcy[1] = dist / r; TNS_CLAMP(hcy[1],0,1);
            hcy[1]=pow(hcy[1],MAIN.ColorPickerGamma);
        }else if (dist < r + LA_RH * 2 && dx > 0){
            abso = -atan((real)dy / (real)dx) / TNS_PI * 2 + 0.5;
            hcy[2] = abso; TNS_CLAMP(hcy[2],0,1);
        }
        tnsHCY2RGB(hcy, Color);
        if(ui->PP.LastPs->p->Tag&LA_PROP_IS_LINEAR_SRGB){ tns2LinearsRGB(Color); }
        laSetFloatArrayAllArray(&ui->PP, Color);
        laRedrawCurrentPanel();
        return LA_RUNNING;
    }

    return LA_RUNNING_PASS;
}
int OPMOD_EnumSelector(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    laEnumItem *ei;
    laEnumProp *ep = ui->PP.LastPs->p;
    int at = 0;
    laEnumItem *Data[32];
    int NoEvent = ui->Flags&LA_UI_FLAGS_NO_EVENT;
    int NoTooltip=ui->Flags&LA_UI_FLAGS_NO_TOOLTIP;
    int IsCycle = ui->Flags&LA_UI_FLAGS_CYCLE;
    int IsMomentary = IsCycle && ui->Flags&LA_UI_FLAGS_NODE_CONTAINER;
    int NoConfirm = ui->Flags&LA_UI_FLAGS_NO_CONFIRM;
    // norm      e1
    // expand    e1 e2 e3
    // expand vertical  (vertical)
    // arr expand    e1 e1 e3
    //               e2 e2 e3
    // arr exp+vert  e1 e1
    //               e2 e2
    //               e3 e3

    if (a->ConfirmData /* && a->ConfirmData->Mode == LA_CONFIRM_DATA*/){
        ui->State = LA_UI_NORMAL;
        laRedrawCurrentPanel();
        return LA_RUNNING;
    }

    if (!laIsInUiItem(ui, a, e->x, e->y) && !uit->On){ return LA_FINISHED_PASS; }
    
    if (e->type == LA_TIME_IDLE && !NoTooltip && !uit->On){
        int GX = e->x, GY = e->y; laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnableIdlePanel(a->ToPanel, a, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }

    if(NoEvent){ return LA_RUNNING_PASS; }

    if (e->type == LA_L_MOUSE_DOWN || (IsMomentary && e->type == LA_L_MOUSE_UP)){
        if (laIsPropertyReadOnly(&ui->PP)) return LA_RUNNING_PASS;

        int IsVertical = ui->Flags&LA_UI_FLAGS_TRANSPOSE;
        int IsExpand = ui->Flags&LA_UI_FLAGS_EXPAND;
        int ArrLen = laGetArrayLength(&ui->PP);
        if(ArrLen==1){ if(!IsExpand) IsVertical=0; }else{ IsExpand=1; }
        int EnumLen = (IsExpand&&(!IsCycle)) ? laGetEnumEntryLen(&ui->PP) : 1;

        int _L,_R,_U,_B, _W=ui->R - ui->L, _Col=1;
        if(IsVertical){
            if(IsExpand){ _Col=ArrLen; }
        }else{
            if(IsExpand){ _Col=EnumLen; } else { _Col=ArrLen; }
        }

        int row = la_DetectRow(ui, e->y);
        int col = la_DetectColumn(ui, e->x, _Col);

        int ArrTarget=0, EnumTarget=0, ShowMenu=0;

        if(IsVertical){
            if(IsExpand){ EnumTarget=row; ArrTarget=col; } else { EnumTarget=col; }
        }else{
            if(IsExpand){ EnumTarget=col; ArrTarget=row; } else { if(!IsCycle) ShowMenu=1; }
        }

        if(ShowMenu){
            int GX = ui->L, GY = ui->B, GR = ui->R, t = 0;
            ui->State = LA_UI_ACTIVE;
            laLocalToWindow(a, a->ToPanel, &GX, &GY);
            laLocalToWindow(a, a->ToPanel, &GR, &t);
            laEnablePropertyPanel(a->ToPanel, a, 0, la_DefaultEnumPanel, 0, &ui->PP, GX, GR, GY, MAIN.CurrentWindow->CH-LA_2RH, 0, e);
            laRedrawCurrentPanel();
        }elif(IsCycle){
            laEnumItem* ei=laGetEnumArrayIndexed(&ui->PP, ArrTarget);
            if(!ei){ ei=((laEnumProp*)ui->PP.LastPs->p)->Items.pFirst; }
            ei = ei->Item.pNext?ei->Item.pNext:((laEnumProp*)ui->PP.LastPs->p)->Items.pFirst;
            if(IsMomentary){  uit->On = 1; if(e->type==LA_L_MOUSE_UP){ ei = ei=((laEnumProp*)ui->PP.LastPs->p)->Items.pFirst; uit->On = 0;} }
            laSetEnumArrayIndexed(&ui->PP, ArrTarget, ei->Index);
            if(!NoConfirm){ laConfirmInt(a,EnumTarget,LA_CONFIRM_DATA); }
            laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
            if(ui->Flags&LA_UI_FLAGS_EXIT_WHEN_TRIGGERED) return LA_FINISHED;
        }else{
            if(ArrTarget>=ArrLen){ArrTarget=ArrLen-1;} 
            if(EnumTarget>=EnumLen){EnumTarget=EnumLen-1;}
            laSetEnumArrayIndexedExternal(&ui->PP, ArrTarget, EnumTarget);
            if(!NoConfirm){ laConfirmInt(a,EnumTarget,LA_CONFIRM_DATA); }
            laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
            return LA_RUNNING_PASS;
        }
        return LA_RUNNING;
    }

    if(e->type==LA_R_MOUSE_DOWN){
        int GX = e->x, GY = e->y;
        laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnablePropertyPanel(a->ToPanel, a, 0, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }

    return LA_RUNNING_PASS;
}
int OPMOD_Button(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    int lx, ly;
    int Away = 0;
    int NoEvent = ui->Flags&LA_UI_FLAGS_NO_EVENT;

    if (e->type == LA_TIME_IDLE && (!ui->State) && (!NoEvent)){
        int GX = e->x, GY = e->y; laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnableIdlePanel(a->ToPanel, a,  &ui->ExtraPP, 0,  &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }
    
    if (laConfirmSameDataIfAny(a)) return LA_RUNNING_PASS;

    if ((e->type == LA_KEY_DOWN && e->key==LA_KEY_ESCAPE) || e->type == LA_INPUT) return LA_FINISHED; //debug

    if (!laIsInUiItem(ui, a, e->x, e->y) /*&& ui->State!= LA_UI_ACTIVE*/){
        ui->State = LA_UI_NORMAL;
        laRedrawCurrentPanel();
        return LA_FINISHED_PASS;
    }

    if(NoEvent){ return LA_RUNNING_PASS; }

    if (e->type == LA_L_MOUSE_DOWN){
        ui->State = LA_UI_ACTIVE;
        laRedrawCurrentPanel();
        return LA_RUNNING;
    }

    char *str;

    int call_ret=0;

    if (e->type == LA_L_MOUSE_UP && ui->State!=LA_UI_NORMAL){
        ui->State = LA_UI_NORMAL;
        if (ui->AT){
            if (!ui->Instructions){
                if (ui->ExtraInstructions) strMakeInstructions(&ui->Instructions, ui->ExtraInstructions->Ptr);
                if (ui->AT->ExtraInstructions) strMakeInstructions(&ui->Instructions, ui->AT->ExtraInstructions);
            }

            if (!laOperatorAvailableP(ui->AT, 0, ui->Instructions)) return LA_RUNNING;

            lx = e->x;
            ly = e->y;
            laLocalToWindow(a, a->ToPanel, &e->x, &e->y);
            e->Localized = 0;
            call_ret=laInvokeP(0, ui->AT, e, 0, ui->ExtraInstructions ? ui->ExtraInstructions->Ptr : 0, 0);
            e->x = lx;
            e->y = ly;
            e->Localized = 1;
        }else{
            laOperatorProp *ap = ui->PP.LastPs->p;
            if (ap->OperatorID){

                if (!ap->OperatorType) ap->OperatorType = laGetOperatorType(ap->OperatorID);

                if (!ui->Instructions){
                    if (ui->ExtraInstructions) strMakeInstructions(&ui->Instructions, ui->ExtraInstructions->Ptr);
                    if (ap->OperatorType) strMakeInstructions(&ui->Instructions, ap->OperatorType->ExtraInstructions);
                }

                if (!laOperatorAvailablePSafe(ap->OperatorType, ui->PP.RawThis, ui->PP.Go->UseInstance, ui->Instructions)) return LA_RUNNING;

                lx = e->x;
                ly = e->y;
                laLocalToWindow(a, a->ToPanel, &e->x, &e->y);
                e->Localized = 0;
                call_ret=laInvokePCreateThis(0, ap->OperatorType, e, ui->PP.RawThis, ui->PP.Go->UseInstance, ui->ExtraInstructions ? ui->ExtraInstructions->Ptr : 0, ap->Base.ExtraInstructions);
                if(a->StopNow) return LA_FINISHED;
                laRetriggerOperators();
                e->x = lx;
                e->y = ly;
                e->Localized = 1;
            }
        }

        if(MAIN.InvokeConfirmData){
            a->ConfirmData=MAIN.InvokeConfirmData; laConfirmSameDataIfAny(a); a->ConfirmData=0;
        }else{
            if (str = strGetArgumentString(ui->Instructions, "feedback")){
                if (strSame(str, "CONFIRM")) laConfirmInt(a, 0, LA_CONFIRM_OK);
                elif (strSame(str, "CANCEL")) laConfirmInt(a, 0, LA_CONFIRM_CANCEL);
                else { laConfirmString(a, str, LA_CONFIRM_CUSTOM_STRING); }
            }elif(!(ui->Flags&LA_UI_FLAGS_NO_CONFIRM)){
                laConfirmInt(a, 0, LA_CONFIRM_OK);
            }
        }

        if(call_ret==LA_OPERATOR_CALLS_SHUTOFF) return LA_OPERATOR_CALLS_SHUTOFF;

        laRedrawCurrentPanel();
        if(ui->Flags&LA_UI_FLAGS_EXIT_WHEN_TRIGGERED) return LA_FINISHED_PASS;
        return LA_RUNNING_PASS;
    }

    return LA_RUNNING_PASS;
}
int OPMOD_MenuItem(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laPanel *p;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    int Away = 0;

    if (a->ConfirmData){
        ui->State = LA_UI_NORMAL;
        laConfirmInt(a, a->ConfirmData->IData, a->ConfirmData->Mode);
        laRedrawCurrentPanel();
        return LA_RUNNING_PASS;
    }

    if (!laIsInUiItem(ui, a, e->x, e->y) && ui->State != LA_UI_ACTIVE){
        ui->State = LA_UI_NORMAL;
        laRedrawCurrentPanel();
        return LA_FINISHED_PASS;
    }

    if (e->type == LA_L_MOUSE_DOWN){
        int GX = ui->L, GY = ui->B;
        laLocalToWindow(a, a->ToPanel, &GX, &GY);
        ui->State = LA_UI_ACTIVE;
        p = laEnableMenuPanel(a->ToPanel, a, ui->Subs.pFirst, &ui->PP, GX, GX + 300, GY, MAIN.CurrentWindow->CH-LA_2RH, 0, e);
        p->NoConfirm = (ui->Flags&LA_UI_FLAGS_NO_CONFIRM)!=0;
        laShowPanelWithDropDownEffect(p);
        laRedrawCurrentPanel();
        return LA_RUNNING;
    }

    return LA_RUNNING_PASS;
}
int OPMOD_ConditionToggle(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    int Away = 0;

    if (!laIsInUiItem(ui, a, e->x, e->y) /*&& ui->State!= LA_UI_ACTIVE*/){
        return LA_FINISHED_PASS;
    }

    if (e->type == LA_L_MOUSE_DOWN){
        ui->State = ui->State == LA_UI_ACTIVE ? LA_UI_NORMAL : LA_UI_ACTIVE;
        laRecalcCurrentPanel();
        return LA_RUNNING;
    }

    return LA_RUNNING_PASS;
}
int OPMOD_SingleLineString(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    laStringEdit *se = uit->Edit;
    int Away = 0;
    char _buf[LA_RAW_CSTR_MAX_LEN] = {0}; char* buf=_buf;
    int NoEvent = ui->Flags&LA_UI_FLAGS_NO_EVENT;
    int NoTooltip=ui->Flags&LA_UI_FLAGS_NO_TOOLTIP;
    int IsTerminal=ui->Flags&LA_UI_FLAGS_TERMINAL_INPUT;
    int ImmediateInput=ui->Flags&LA_UI_FLAGS_IMMEDIATE_INPUT;

    if (!laIsInUiItem(ui, a, e->x, e->y)){
        if (ui->State != LA_UI_EDITING){
            ui->State = LA_UI_NORMAL;
            laRedrawCurrentPanel();
            return LA_FINISHED_PASS;
        }else{
            if(IsTerminal && (e->type&LA_KEY_MOUSE_SCROLL)){ return LA_RUNNING_PASS; }
            if ((e->type & LA_MOUSEDOWN) == LA_MOUSEDOWN){
                buf=strEndEdit(&uit->Edit, 0);
                laSetString(&ui->PP, buf); free(buf);
                laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
                ui->State = LA_UI_NORMAL; la_DisplayKeyboard(0);
                laRedrawCurrentPanel();
                return LA_FINISHED_PASS;
            }
        }
    }

    if (e->type == LA_TIME_IDLE && !ui->State && !NoTooltip){
        int GX = e->x, GY = e->y; laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnableIdlePanel(a->ToPanel, a, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }
    
    if(NoEvent){ return LA_RUNNING_PASS; }

    if(e->type ==LA_KEY_DOWN){
        if (e->key == LA_KEY_ENTER){
            buf=IsTerminal?strGetEditString(uit->Edit,0):strEndEdit(&uit->Edit, 0);
            if(IsTerminal){ MAIN.IsTerminalEnter=1; }
            laSetString(&ui->PP, buf); free(buf);
            laRedrawCurrentPanel();
            if(!IsTerminal){
                laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
                ui->State = LA_UI_NORMAL; la_DisplayKeyboard(0);
                return LA_FINISHED_PASS;
            }else{
                laGetString(&ui->PP, _buf, &buf);
                strBeginEdit(&uit->Edit,buf); la_DisplayKeyboard(1);
                MAIN.IsTerminalEnter=0;
                return LA_RUNNING;
            }
        }else if (e->key == LA_KEY_ESCAPE && ui->State != LA_UI_NORMAL){
            ui->State = LA_UI_NORMAL; la_DisplayKeyboard(0); strEndEdit(&uit->Edit,1);
            laRedrawCurrentPanel();
            return LA_FINISHED;
        }
    }

    if(laKeyMapExecuteEventEx(a, &ui->ExtraPP, &ui->Type->KeyMapper, e)) return LA_RUNNING;

    if (e->type == LA_L_MOUSE_DOWN){
        if (ui->State == LA_UI_NORMAL){
            if(laIsPropertyReadOnly(&ui->PP)) return LA_RUNNING;
            ui->State = LA_UI_EDITING;
            laGetString(&ui->PP, _buf, &buf);
            strBeginEdit(&uit->Edit,buf); la_DisplayKeyboard(1);
            strSelectLineAll(uit->Edit);
        }elif (ui->State == LA_UI_EDITING){
            if (strHasSelection(uit->Edit)) strDeselectAll(uit->Edit);
            else strSelectLineAll(uit->Edit);
        }
        laRedrawCurrentPanel();
        return LA_RUNNING;
    }

    if (e->type == LA_R_MOUSE_DOWN){
        int GX = e->x, GY = e->y;
        ui->PP.LastIndex = la_DetectColumn(ui, e->x, laGetArrayLength(&ui->PP));
        laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnablePropertyPanel(a->ToPanel, a, ui->State==LA_UI_EDITING?&ui->ExtraPP:0, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }

    if (ui->State == LA_UI_EDITING){
        if (la_ProcessTextEdit(e, se, ui)){
            laRedrawCurrentPanel();
            if(ImmediateInput){
                buf=strGetEditString(uit->Edit,0); laSetString(&ui->PP, buf); free(buf);
            }
        }
        if (a->ConfirmData){
            if(a->ConfirmData->Mode == LA_CONFIRM_TEXT_EDIT){ laRedrawCurrentPanel(); }
        }
        return LA_RUNNING;
    }

    if (ui->State != LA_UI_EDITING) return LA_RUNNING_PASS;
    else return LA_RUNNING;
}
void la_SetMultistringViewRange(laUiItem* ui, laStringEdit* se, laBoxedTheme* bt){
    int NumberWidth=0; int MonoWidth=tnsGetMonoFontAdvance(); int Count=se->TotalLines;
    while(Count){ Count/=10; NumberWidth+=MonoWidth; }
    strSetEditViewRange(ui->Extra->Edit,
        ui->Extent>0?ui->Extent:((ui->B-ui->U)/LA_RH),(ui->R-ui->L-NumberWidth-LA_M*2-LA_M)/MonoWidth);
}
int OPMOD_MultiString(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *es = a->CustomData;
    laStringEdit *se = es->Edit;
    int Away = 0;
    char _buf[LA_RAW_CSTR_MAX_LEN] = {0}; char* buf=&_buf;
    int NoEvent = ui->Flags&LA_UI_FLAGS_NO_EVENT;
    int NoTooltip=ui->Flags&LA_UI_FLAGS_NO_TOOLTIP;

    if (!laIsInUiItem(ui, a, e->x, e->y) && !es->Dragging){
        if ((e->type & LA_MOUSEDOWN) == LA_MOUSEDOWN){
            buf=strGetEditString(es->Edit, 0); laSetString(&ui->PP, buf); free(buf);
            laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
            ui->State = LA_UI_NORMAL; la_DisplayKeyboard(0);
            laRedrawCurrentPanel();
            return LA_FINISHED_PASS;
        }
        if(ui->State!=LA_UI_ACTIVE) return LA_FINISHED_PASS;
    }

    if (e->type == LA_TIME_IDLE && ui->State!=LA_UI_ACTIVE && !NoTooltip){
        int GX = e->x, GY = e->y; laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnableIdlePanel(a->ToPanel, a, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }

    if(NoEvent){ return LA_RUNNING_PASS; }

    if(e->type ==LA_KEY_DOWN){
        if (e->key == LA_KEY_ESCAPE){
            if(ui->State != LA_UI_NORMAL){
                buf=strGetEditString(es->Edit, 0); laSetString(&ui->PP, buf); free(buf);
                laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
                ui->State = LA_UI_NORMAL; laRedrawCurrentPanel(); la_SetMultistringViewRange(ui,es->Edit,bt); la_DisplayKeyboard(0);
            }
            if(es->Dragging){ es->Dragging=0; ui->Extent=es->TargetIndexVali; laRecalcCurrentPanel();la_SetMultistringViewRange(ui,es->Edit,bt); }
            return LA_FINISHED;
        }
    }

    if(laKeyMapExecuteEventEx(a, &ui->ExtraPP, &ui->Type->KeyMapper, e)) return LA_RUNNING;
    
    if(e->type == LA_L_MOUSE_DOWN && ui->Extent>=0 && e->x>ui->R-LA_M-LA_RH && e->y>ui->B-LA_M-LA_RH){
        es->Dragging=1; es->LastX=e->x; es->LastY=e->y; es->TargetIndexVali=ui->Extent;
        return LA_RUNNING;
    }
    if(es->Dragging){
        if(e->type==LA_MOUSEMOVE){ ui->Extent=es->TargetIndexVali+((real)e->y-es->LastY+0.5)/LA_RH;
            if(ui->Extent<1)ui->Extent=1; laRecalcCurrentPanel(); la_SetMultistringViewRange(ui,es->Edit,bt);}
        elif(e->type==LA_L_MOUSE_UP){ es->Dragging=0; }
        elif(e->type==LA_R_MOUSE_DOWN){
            es->Dragging=0; ui->Extent=es->TargetIndexVali; laRecalcCurrentPanel();la_SetMultistringViewRange(ui,se,bt); }
        return LA_RUNNING;
    }
    
    int NoInactiveScroll=ui->Flags&LA_UI_FLAGS_NO_SCROLL_INACTIVE;
    if((!NoInactiveScroll) || (ui->State==LA_UI_ACTIVE)){
        if(e->type==LA_MOUSE_WHEEL_DOWN){ strMoveView(se, MAIN.ScrollingSpeed, 0); laRedrawCurrentPanel(); return LA_RUNNING; }
        elif(e->type==LA_MOUSE_WHEEL_UP){ strMoveView(se, -MAIN.ScrollingSpeed, 0); laRedrawCurrentPanel(); return LA_RUNNING; }
    }

    if (e->type == LA_L_MOUSE_DOWN){
        if (ui->State == LA_UI_NORMAL){
            ui->State = LA_UI_ACTIVE;
            laGetString(&ui->PP, _buf, &buf);
            strBeginEdit(&es->Edit,buf);la_SetMultistringViewRange(ui,es->Edit,bt);
            laRedrawCurrentPanel(); la_DisplayKeyboard(1);
            return LA_RUNNING;
        }
    }

    if (e->type == LA_R_MOUSE_DOWN){
        int GX = e->x, GY = e->y;
        ui->PP.LastIndex = la_DetectColumn(ui, e->x, laGetArrayLength(&ui->PP));
        laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnablePropertyPanel(a->ToPanel, a, ui->State==LA_UI_ACTIVE?&ui->ExtraPP:0, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }

    if (ui->State == LA_UI_ACTIVE){
        if (la_ProcessTextEdit(e, se, ui)) laRedrawCurrentPanel();
        if (a->ConfirmData){
            if(a->ConfirmData->Mode == LA_CONFIRM_TEXT_EDIT){ laRedrawCurrentPanel(); }
        }
        return LA_RUNNING;
    }

    if (ui->State != LA_UI_ACTIVE) return LA_RUNNING_PASS;
    else return LA_RUNNING;
}
int OPMOD_TabSelector(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    laUiList *uil;
    int Away = 0;
    int count = 0;
    int it = 0;
    int page;

    if (!laIsInBound(e->x, e->y, ui->L, ui->R, ui->U, ui->U+LA_RH)){ return LA_FINISHED; }

    if (e->type == LA_L_MOUSE_DOWN){
        if (ui->Page->HeightCoeff && ui->R - e->x < LA_RH + LA_M) return LA_RUNNING_PASS;
        for (uil = ui->Subs.pFirst; uil; uil = uil->Item.pNext){ count++; }
        page = la_DetectColumn(ui, e->x, count);
        for (uil = ui->Subs.pFirst; uil; uil = uil->Item.pNext){
            if (it == page){
                laRecalcCurrentPanel();
                ui->Page = uil;
                return LA_RUNNING;
            }else
                it++;
        }
    }
    
    return LA_RUNNING_PASS;
}
int OPMOD_Collection(laOperator *a, laEvent *e) {
  laUiItem *ui = a->Instance;
  laGeneralUiExtraData *uit = a->CustomData;
  laListHandle Locals = {0};
  laUiList *uil;
  int lx = e->x, ly = e->y;
  int at = 0;
  laUiList *ToUil = a->ToPanel->MenuRefer ? a->ToPanel->MenuRefer : &a->ToPanel->UI;
  int NoEvent=ui->Flags&LA_UI_FLAGS_NO_EVENT;

  if (!laIsInUiItem(ui, a, e->x, e->y) /* || e->y>(ui->Subs.pFirst?((laUiList*)ui->Subs.pFirst)->HeightCoeff:10000)*/){
    return LA_FINISHED_PASS;
  }

  laLocalToPanel(a, &lx, &ly);
  if (a->ToPanel && ui->U>=a->ToPanel->TitleBar.B && ly < a->ToPanel->TitleBar.B) {
    return LA_FINISHED_PASS;
  }
  uil = la_DetectUiListRecursive(ToUil, lx, ly, 10000, 0, 0, 0, 0, 0);
  //if (uil != ((laUiListDrawItem *)a->LocalUiLists.pFirst)->Target) {
  //  printf("%d %d EXITC\n",((laUiListDrawItem *)a->LocalUiLists.pFirst)->Target->U, uil->U);
  //  uil = la_DetectUiListRecursive(ToUil, lx, ly, 10000, 0, 0, 0, 0, 0);
  //  return LA_FINISHED;
  //}
//
  if (e->type & LA_MOUSE_EVENT) {
    laUiList *sub;
    laUiItem *tui;
    int LimHeight = 10000;
    int UIB = ui->B, UIU = ui->U;
    int t = 0;
    void *Active = ui->PP.EndInstance;
    if (ui->Subs.pFirst)
      LimHeight = (int)((laUiList *)ui->Subs.pFirst)->HeightCoeff;
    if (LimHeight > 0)
      UIB = LimHeight;
    laPanelToLocal(a, &t, &UIB);
    for (sub = ui->Subs.pFirst; sub; sub = sub->Item.pNext) {
      ui->PP.EndInstance = sub->Instance;
      if ((tui = la_DetectUiItemRecursive(sub, e->x, e->y, UIB, &Locals, 0))) {
        if (tui && !a->Child && tui->Type->OperatorType) {
          // printf("invui %d\n", tui);
          laSetOperatorLocalizer(a->ToPanel);
          laInvokeUiP(a, tui->Type->OperatorType, e, tui, &Locals, 0);
        }
      }
      lstClearPointer(&Locals);
    }
    ui->PP.EndInstance = Active;
  }

  if (e->type == LA_L_MOUSE_DOWN && (!laIsPropertyReadOnly(&ui->PP)) &&
      ui->Type != _LA_UI_COLLECTION_SINGLE) {
    laUiList *uil;
    if (a->ConfirmData) return LA_RUNNING_PASS;
    for (uil = ui->Subs.pFirst; uil; uil = uil->Item.pNext) {
      if (laIsInBound(e->x, e->y, uil->L, uil->R, uil->U, uil->B)) {
        laRecalcCurrentPanel();
        laPropPack PP = {0};
        la_CopyPropPack(
            &ui->PP,
            &PP); // needed because layout can be switched after set active.
        char *cmd;
        if ((!(ui->Flags&LA_UI_FLAGS_NO_CONFIRM)) &&
            ((!(cmd = strGetArgumentString(ui->Instructions, "feedback"))) ||
             (!strSame(cmd, "NONE")))) {
            laConfirmPointer(a, uil->Instance, la_EnsureSubTarget(ui->PP.LastPs->p, uil->Instance), LA_CONFIRM_DATA);
        }
        laSetActiveInstance(ui->PP.LastPs->p, ui->PP.LastPs->UseInstance,
                            uil->Instance);
        laRecordAndPushProp(&PP, 0);
        laMarkPropChanged(&PP);
        return LA_RUNNING_PASS;
      }
    }
    return LA_RUNNING_PASS;
  }
  elif (e->type == LA_R_MOUSE_DOWN && (!NoEvent)) {
    laUiList *uil;
    for (uil = ui->Subs.pFirst; uil; uil = uil->Item.pNext) {
      if (laIsInBound(e->x, e->y, uil->L, uil->R, uil->U, uil->B)) {
        ui->PP.EndInstance = uil->Instance;
        lx = e->x, ly = e->y;
        laLocalToWindow(a, a->ToPanel, &lx, &ly);
        laEnablePropertyPanel(a->ToPanel, a, 0, 0, laui_PropOperatorUiDefine, &ui->PP,
                              lx, lx, ly, 0, 0, e);
      }
    }
    return LA_RUNNING;
  }

  return LA_RUNNING_PASS;
}
int OPMOD_CollectionSelector(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    int NoTooltip=ui->Flags&LA_UI_FLAGS_NO_TOOLTIP;
    int at = 0;
    int ReadOnly=laIsPropertyReadOnly(&ui->PP);
    int Simple=ui->Flags&LA_UI_COLLECTION_SIMPLE_SELECTOR;

    if(ui->Flags & LA_UI_FLAGS_NO_CONFIRM){
        if (a->ConfirmData) {
            ui->State = LA_UI_NORMAL;
            laRecalcCurrentPanel();
            return LA_RUNNING_PASS;
        }
    }elif (laConfirmSameDataIfAny(a)){
        ui->State = LA_UI_NORMAL;
        laRecalcCurrentPanel();
        return LA_RUNNING_PASS;
    }

    if (!laIsInUiItem(ui, a, e->x, e->y)){
        return LA_FINISHED;
    }

    //int lx = e->x, ly = e->y;
    //laUiList *ToUil =
    //    a->ToPanel->MenuRefer ? a->ToPanel->MenuRefer : &a->ToPanel->UI;
    //laUiList* uil = la_DetectUiListRecursive(ToUil, lx, ly, 10000, 0, 0, 0, 0, 0);
    //if (uil != ((laUiListDrawItem *)a->LocalUiLists.pFirst)->Target) {
    //    printf("%d %d EXIT\n",((laUiListDrawItem *)a->LocalUiLists.pFirst)->Target, uil);
    //    return LA_FINISHED;
    //}

    if (e->type == LA_TIME_IDLE && !ui->State && !NoTooltip){
        int GX = e->x, GY = e->y; laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laPanel *p = laEnableIdlePanel(a->ToPanel, a, 0, 0, &ui->PP, GX, GX + 150, GY, 600, 200, e);
        return LA_RUNNING;
    }

    if (e->type == LA_L_MOUSE_DOWN){
        laUiList *uil;
        laColumn *col;
        laPanel *p;
        int GX = ui->L, GY = ui->B, GR = ui->R, t = 0;
        if(ReadOnly){ return LA_FINISHED; }

        if(!Simple){
            if (e->x >= TNS_MAX2(ui->R - LA_RH,(ui->R+ui->L)/2) && e->x < ui->R){
                laSetActiveInstance(ui->PP.LastPs->p, ui->PP.LastPs->UseInstance, 0);
                laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
                return LA_FINISHED;
            }
        }

        laLocalToWindow(a, a->ToPanel, &GX, &GY);
        laLocalToWindow(a, a->ToPanel, &GR, &t);
        laRedrawCurrentPanel();
        ui->State = LA_UI_ACTIVE;
        p = laEnableEmptyPropertyPanel(a->ToPanel, a, GX, GR, GY, 500, e);{
            int MinWidth;
            uil = &p->UI;
            col = laFirstColumn(uil);
            laShowItemFull(uil, col, &ui->PP, 0, LA_WIDGET_COLLECTION, 0, ui->Template, 0)->Extent = ui->Extent;
            laEnclosePanelContent(p, &p->UI);
        }
        return LA_RUNNING;
    }
    return LA_RUNNING_PASS;
}
int OPMOD_ColumnAdjuster(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    laColumn *c = uit->Ptr1;

    c = uit->Ptr1 ? uit->Ptr1 : la_DetectSplit(ui->C, e->x);
    if(c){ laSetWindowCursor(LA_LEFT_AND_RIGHT); }else{ laSetWindowCursor(LA_ARROW); }
    if ((!uit->Dragging) && (!laIsInUiItem(ui, a, e->x, e->y)||!c)){
        //printf("done\n");
        laSetWindowCursor(LA_ARROW);
        return LA_FINISHED_PASS;
    }

    if (e->type == LA_L_MOUSE_DOWN){
        uit->Ptr1 = c;
        uit->LastX = e->x;
        uit->Dragging=1;
        return LA_RUNNING;
    }
    if (uit->Dragging && uit->Ptr1 && e->type == LA_MOUSEMOVE){
        la_AdjustColumnSplit(c, e->x - uit->LastX);
        uit->LastX = e->x;
        laRecalcCurrentPanel();
        return LA_RUNNING;
    }
    if (uit->Dragging && (e->type == LA_L_MOUSE_UP || e->type == LA_R_MOUSE_DOWN || e->key == LA_KEY_ESCAPE)){
        uit->Ptr1 = 0; uit->Dragging=0;
        return LA_RUNNING;
    }

    return LA_RUNNING;
}
void la_NodeSocketGetPositionDelta(laUiItem* ui, int ex,int ey,int* dx,int* dy){
    int cx,cy;
    if(ui->Flags&LA_UI_SOCKET_LABEL_E){ cy=(ui->U+ui->B)/2; cx=ui->L+(ui->B-ui->U)/2; }
    elif(ui->Flags&LA_UI_SOCKET_LABEL_W){ cy=(ui->U+ui->B)/2; cx=ui->R-(ui->B-ui->U)/2; }
    elif(ui->Flags&LA_UI_SOCKET_LABEL_N){ cx=(ui->L+ui->R)/2; cy=ui->U+(ui->R-ui->L)*0.75; }
    elif(ui->Flags&LA_UI_SOCKET_LABEL_S){ cx=(ui->L+ui->R)/2; cy=ui->B-(ui->R-ui->L)*0.75; }
    else{ cx=(ui->L+ui->R)/2; cy=(ui->U+ui->B)/2; }
    *dx=cx-ex; *dy=cy-ey;
}
int OPMOD_NodeSocket(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    int lx=e->x,ly=e->y;

    laProp* p=ui->PP.LastPs->p; laPropContainer* pc=la_EnsureSubTarget(p,0);
    laPropContainer* PCFind=0;
    if(pc==LA_PC_SOCKET_IN)   { PCFind=LA_PC_SOCKET_OUT; }
    elif(pc==LA_PC_SOCKET_OUT){ PCFind=LA_PC_SOCKET_IN; }

    if ((!uit->Dragging) && (!laIsInUiItem(ui, a, e->x, e->y))){ return LA_FINISHED_PASS; }

    if (e->type==LA_TIME_IDLE){ return LA_RUNNING; }
    if (e->type == LA_L_MOUSE_DOWN){
        uit->Dragging=1; MAIN.NextWireColor+=3;
        if(pc==LA_PC_SOCKET_IN)   {
            laNodeInSocket* s=ui->PP.EndInstance;
            uit->Ptr2 = s->Source;
            if(!s->Source){
                s->ColorId=MAIN.NextWireColor; s->Source=MAIN.tNodeOut; latouched_NodeInSocket(0,0);
                MAIN.tNodeOut->RuntimeX=MAIN.tNodeOut->RuntimeY=MAIN.tNodeOut->RuntimePX=MAIN.tNodeOut->RuntimePY=0;
            }else{
                MAIN.tNodeIn->Source = s->Source;
                MAIN.tNodeIn->RuntimeX=MAIN.tNodeIn->RuntimeY=MAIN.tNodeIn->RuntimePX=MAIN.tNodeIn->RuntimePY=0;
                MAIN.tNodeIn->RuntimePX=-(s->Source->RuntimeX+s->Source->RuntimePX)+(s->RuntimeX+s->RuntimePX);
                MAIN.tNodeIn->RuntimePY=-(s->Source->RuntimeY+s->Source->RuntimePY)+(s->RuntimeY+s->RuntimePY);
                s->Source=0; MAIN.tNodeIn->ColorId=s->ColorId; latouched_NodeInSocket(0,0);
            }
        }
        elif(pc==LA_PC_SOCKET_OUT){ MAIN.tNodeIn->Source=ui->PP.EndInstance; latouched_NodeInSocket(0,0);
            MAIN.tNodeIn->RuntimeX=MAIN.tNodeIn->RuntimeY=MAIN.tNodeIn->RuntimePX=MAIN.tNodeIn->RuntimePY=0;
            MAIN.tNodeIn->ColorId=MAIN.NextWireColor;  }
        int dx,dy; la_NodeSocketGetPositionDelta(ui,e->x,e->y,&dx,&dy); uit->LastX = e->x+dx; uit->LastY = e->y+dy;
        laRedrawCurrentPanel();
        return LA_RUNNING;
    }
    if (uit->Dragging && e->type == LA_MOUSEMOVE){
        laPanel* p=MAIN.CurrentPanel;
        laLocalToPanel(a, &lx, &ly);
        laUiItem* tui=la_DetectSocketRecursive(&p->UI,lx,ly,100000,uit->Ptr2?LA_PC_SOCKET_IN:PCFind); uit->Ptr1=tui;
        if(pc==LA_PC_SOCKET_IN){
            if(!uit->Ptr2){MAIN.tNodeOut->RuntimeX=e->x-uit->LastX;MAIN.tNodeOut->RuntimeY=e->y-uit->LastY;}
            else { MAIN.tNodeIn->RuntimeX=e->x-uit->LastX;MAIN.tNodeIn->RuntimeY=e->y-uit->LastY; } }
        elif(pc==LA_PC_SOCKET_OUT){ MAIN.tNodeIn->RuntimeX=e->x-uit->LastX; MAIN.tNodeIn->RuntimeY=e->y-uit->LastY; }
        laRedrawCurrentPanel();
        return LA_RUNNING;
    }
    if (uit->Dragging && (e->type == LA_L_MOUSE_UP || e->type == LA_R_MOUSE_DOWN || e->key == LA_KEY_ESCAPE)){
        uit->Dragging=0; MAIN.tNodeIn->Source=0;
        laRecalcCurrentPanel();
        laUiItem* tui=uit->Ptr1; void* soc=tui?tui->PP.EndInstance:0;
        if(pc == LA_PC_SOCKET_IN){
            if(!uit->Ptr2){ if(!tui){ laNodeInSocket* s=ui->PP.EndInstance; s->Source=0; return LA_RUNNING; }
                laNodeInSocket* s=ui->PP.EndInstance; s->Source=0; memAssignRef(s,&s->Source,soc); latouched_NodeInSocket(0,0);
                s->ColorId=MAIN.NextWireColor; laRecordAndPushProp(&ui->PP, 0); laMarkPropChanged(&ui->PP);
            }else{
                if(!tui){
                    laNodeInSocket* s=ui->PP.EndInstance; s->Source=uit->Ptr2; memAssignRef(s,&s->Source,0); latouched_NodeInSocket(0,0);
                    laRecordAndPushProp(&ui->PP, 0); laMarkPropChanged(&ui->PP);
                }else{
                    laNodeInSocket* s=soc; if(s->Source){ uit->Ptr2=0; return LA_RUNNING; }  latouched_NodeInSocket(0,0);
                    memAssignRef(s,&s->Source,uit->Ptr2); s->ColorId=((laNodeInSocket*)ui->PP.EndInstance)->ColorId;
                    laRecordDifferences(&ui->PP,0); laRecordDifferences(&tui->PP,0); laPushDifferences("Socket status changed", 0);
                    laMarkPropChanged(&tui->PP); laMarkPropChanged(&ui->PP);
                }
            }
        }
        else{
            if(!tui){ return LA_RUNNING; }
            laNodeInSocket* s=soc; s->Source=0; memAssignRef(s,&s->Source,ui->PP.EndInstance); latouched_NodeInSocket(0,0);
            s->ColorId=MAIN.NextWireColor; laRecordAndPushProp(&tui->PP, 0); laMarkPropChanged(&tui->PP);
        }
        
        uit->Ptr2=0;
        return LA_RUNNING;
    }

    return LA_RUNNING_PASS;
}
int OPMOD_HeightAdjuster(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    int lx=e->x,ly=e->y;
    int ClickedVal=0;

    if ((!uit->Dragging) && (!laIsInUiItem(ui, a, e->x, e->y))){ return LA_FINISHED_PASS; }

    if (e->type == LA_L_MOUSE_DOWN || e->type==LA_R_MOUSE_DOWN){
        uit->Dragging=1; uit->LastY=e->y; uit->On=0; uit->TargetIndexVali=e->type==LA_L_MOUSE_DOWN?(ui->State==LA_BT_ACTIVE?2:1):2; ui->Extent=1;
        return LA_RUNNING;
    }
    if(e->type==LA_L_MOUSE_UP && ui->Extent){ ui->State=(ui->State==LA_BT_ACTIVE)?LA_BT_NORMAL:LA_BT_ACTIVE; laRedrawCurrentPanel(); }
    if (uit->Dragging && e->type == LA_MOUSEMOVE){
        int diff=e->y-uit->LastY; diff=((real)diff+0.5)/(ui->B-ui->U); int delta=uit->TargetIndexVali;
        if(diff!=uit->On){ ui->Extent=0; int d=diff-uit->On; uit->On=diff; while(d){ laSetInt(&ui->PP, d>0?delta:-delta); d-=(d>0?1:-1); }; laRecalcCurrentPanel(); }
        return LA_RUNNING;
    }
    if (uit->Dragging && (e->type == LA_L_MOUSE_UP || e->type == LA_R_MOUSE_UP || e->key == LA_KEY_ESCAPE)){
        uit->Dragging=0; return LA_RUNNING_PASS;
    }

    return LA_RUNNING_PASS;
}
int OPMOD_MouseActionReporter(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);

    if (!laIsInUiItem(ui, a, e->x, e->y)){ return LA_FINISHED_PASS; }

    if (!(e->type&LA_MOUSE_EVENT)){ return LA_RUNNING_PASS; }
    if (e->type==LA_MOUSEMOVE){ return LA_RUNNING_PASS; }

    if((e->type&(LA_KEY_MOUSE_LEFT|LA_KEY_MOUSE_RIGHT|LA_KEY_MOUSE_MIDDLE)) && (e->type&LA_STATE_DOWN)){
        int MouseKey = (e->type &(~(LA_MOUSE_EVENT|LA_STATE_DOWN)));
        char buf[64];
        la_InputMappingGetKeyName(MouseKey, e->SpecialKeyBit, buf);
        laConfirmString(a,buf,LA_CONFIRM_CUSTOM_STRING);
    }
    if(e->type&(LA_KEY_MOUSE_SCROLL)){
        int MouseKey = (e->type &(~LA_MOUSE_EVENT));
        char buf[64];
        la_InputMappingGetKeyName(MouseKey, e->SpecialKeyBit, buf);
        laConfirmString(a,buf,LA_CONFIRM_CUSTOM_STRING);
    }

    return LA_RUNNING_PASS;
}

void la_ValueMapperSortPoint(laValueMapper* vm){
    laValueMapperPoint* vmp,*ivmp; laListHandle temp={0}; int Inserted=0;
    while(vmp=lstPopItem(&vm->Points)){
        Inserted=0;
        for(ivmp=temp.pLast;ivmp;ivmp=ivmp->Item.pPrev){
            if(ivmp->x<vmp->x){ lstInsertItemAfter(&temp,vmp,ivmp); Inserted=1; break; }
        }
        if(!Inserted){ lstPushItem(&temp,vmp); }
    }
    vm->Points.pFirst=temp.pFirst; vm->Points.pLast=temp.pLast;
}
laValueMapperPoint* la_ValueMapperCreatePoint(laValueMapper* vm, real x, real y){
    laValueMapperPoint* vmp=memAcquire(sizeof(laValueMapperPoint));
    vmp->x=x;vmp->y=y; lstAppendItem(&vm->Points,vmp);
    la_ValueMapperSortPoint(vm);
    return vmp;
}
laValueMapperPoint* la_ValueMapperGetPoint(laValueMapper* vm, real x, real y){
    laValueMapperPoint* vmp=0,*ClosestVMP=0; real MinD=FLT_MAX;
    if(x<0||x>1||y>1||y<0) return 0;
    const real thres=0.1;
    for(vmp=vm->Points.pFirst;vmp;vmp=vmp->Item.pNext){
        if(vmp->x>x+thres || vmp->x<x-thres || vmp->y>y+thres || vmp->y<y-thres) continue;
        real d=tnsDistIdv2(x,y,vmp->x,vmp->y);
        if(d<MinD){ ClosestVMP=vmp; MinD=d; }
    }
    vmp=ClosestVMP;
    if(!vmp){ vmp=la_ValueMapperCreatePoint(vm,x,y); }
    return vmp;
}
void la_ValueMapperEnsureValidPoints(laValueMapper* vm){
    if(!vm->Points.pFirst){ la_ValueMapperCreatePoint(vm,0,0); }
    if(vm->Points.pFirst==vm->Points.pLast){ la_ValueMapperCreatePoint(vm,1,1); }
}
laValueMapper* laValueMapperInit(){
    laValueMapper* vm=memAcquire(sizeof(laValueMapper));
    vm->InRange[0]=0; vm->InRange[1]=1; vm->OutRange[0]=0; vm->OutRange[1]=1;
    la_ValueMapperEnsureValidPoints(vm);
    return vm;
}
void laValueMapperDestroy(laValueMapper* vm){
    laValueMapperPoint* vmp;
    while(vmp=lstPopItem(&vm->Points)){ memLeave(vmp); }
    memLeave(vm);
}
laValueMapper* laValueMapperCopy(laValueMapper* new_optional, laValueMapper* vm){
    laValueMapper* into=new_optional?new_optional:memAcquire(sizeof(laValueMapper)); laValueMapperPoint* vmp;
    while(vmp=lstPopItem(&into->Points)){ memLeave(vmp); }
    for(vmp=vm->Points.pFirst;vmp;vmp=vmp->Item.pNext){ la_ValueMapperCreatePoint(into, vmp->x, vmp->y); }
    tnsVectorSet2v(into->InRange,vm->InRange); tnsVectorSet2v(into->OutRange,vm->OutRange);
    return into;
}
real laValueMapperEvaluate(laValueMapper* vm, real x, real* force_InMin, real* force_InMax, real* force_OutMin, real* force_OutMax, int ClampIn, int ClampOut){
    real InMin=vm->InRange[0], InMax=vm->InRange[1], OutMin=vm->OutRange[0], OutMax=vm->OutRange[1];
    if(force_InMin) InMin=*force_InMin; if(force_InMax) InMax=*force_InMax; if(force_OutMin) OutMin=*force_OutMin; if(force_OutMax) OutMax=*force_OutMax;
    if(fabs(InMax-InMin)<1e-7) return 0;
    x=tnsGetRatiod(InMin,InMax,x); if(ClampIn) TNS_CLAMP(x,0,1);
    laValueMapperPoint* vmp,*NextVMP;
    real ey=0;
    for(vmp=vm->Points.pFirst;vmp;vmp=vmp->Item.pNext){
        NextVMP=vmp->Item.pNext;
        if(NextVMP && (!(vmp->x<=x && NextVMP->x>x))) continue;
        if(!NextVMP){ NextVMP=vmp; vmp=vmp->Item.pPrev; if(!vmp) return 0; }
        if(fabs(vmp->x-NextVMP->x)<1e-7){ ey=vmp->y; break; }
        real ratio=tnsGetRatiod(vmp->x,NextVMP->x,x);
        ey=tnsInterpolate(vmp->y,NextVMP->y,ratio); break;
    }
    real Out=tnsInterpolate(OutMin,OutMax,ey);
    if(ClampOut) TNS_CLAMP(Out,OutMin,OutMax);
    return Out;
}
int OPMOD_ValueMapper(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laGeneralUiExtraData *uit = a->CustomData;
    int w=ui->R-ui->L, h=ui->B-ui->U;
    real x=(real)(e->x-ui->L)/w,y=(real)(ui->B-e->y)/h;
    int ClickedVal=0;

    if ((!uit->Dragging) && (!laIsInUiItem(ui, a, e->x, e->y))){ return LA_FINISHED_PASS; }

    laValueMapper* vm=ui->PP.EndInstance;

    if (e->type == LA_L_MOUSE_DOWN){
        uit->Dragging=1; uit->LastX=e->x; uit->LastY=e->y;
        return LA_RUNNING;
    }
    if (uit->Dragging && e->type == LA_MOUSEMOVE){
        laValueMapperPoint* vmp=uit->Ptr1;
        if(uit->Dragging==1 && tnsDistIdv2(e->x,e->y,uit->LastX,uit->LastY)>LA_RH/2){ 
            uit->Ptr1=la_ValueMapperGetPoint(vm,x,y); if(!uit->Ptr1){ uit->Dragging=0; return LA_RUNNING; }
            uit->Dragging=2; 
        }
        elif(uit->Dragging==2){
            if(x<0&&x>-0.05){ x=0; } if(x>1&&x<1.05){ x=1; } TNS_CLAMP(y,0,1);
            vmp->x=x; vmp->y=y; la_ValueMapperSortPoint(vm);
            laNotifyUsersPP(&ui->PP); laRedrawCurrentPanel(); laMarkPropChanged(&ui->PP);
        }
        return LA_RUNNING;
    }
    if (uit->Dragging && (e->type == LA_L_MOUSE_UP || e->type == LA_R_MOUSE_UP || e->key == LA_KEY_ESCAPE)){
        uit->Dragging=0; laValueMapperPoint* vmp=uit->Ptr1; if(!vmp) return LA_RUNNING_PASS;
        if(vmp->x>1||vmp->x<0) {
            lstRemoveItem(&vm->Points,vmp); la_ValueMapperEnsureValidPoints(vm);
            laNotifyUsersPP(&ui->PP); laRedrawCurrentPanel(); laRecordAndPushProp(&ui->PP,0); laMarkPropChanged(&ui->PP);
        }
        return LA_RUNNING_PASS;
    }

    return LA_RUNNING_PASS;
}

void la_RegisterUiOperatorsBasic(){
    laPropContainer *pc, *p;
    laOperatorType *at;
    laEnumProp *ep;

    laCreateOperatorType("LA_edit_string_copy", "Copy Selection", "Copy selected string to clip board",
                          OPCHK_EditStringCopySelection, 0, 0, OPINV_EditStringCopySelection, 0, U'🗍', LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);

    laCreateOperatorType("LA_int_array_h_operator", "Int(Horizon) UiItem Operator", "Do Array Valuator Stuff",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_IntArrayHorizon, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_real_array_h_operator", "Float(Horizon) UiItem Operator", "Do Array Valuator Stuff",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_FloatArrayHorizon, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_real_color_operator", "Color(Float) Display UiItem Operator", "Color Display,Click To Set Color",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_FloatColor, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_real_hcy_operator", "HCY Color Picker UiItem Operator", "HCY Color Picker,Click To Set Color",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_ColorHCY, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_enum_selector_operator", "Enum Selector UiItem Operator", "Do Selector Stuff",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_EnumSelector, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_button_operator", "Button UiItem Operator", "Execute an Operator",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_Button, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_menu_root_operator", "Menu Root UiItem Operator", "Drop A Menu",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_MenuItem, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_condition_toggle_operator", "Condition +/- Button Operator", "Do Self Condition Function",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_ConditionToggle, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_string_operator", "Single Line String Operator", "Do String Modifying Function",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_SingleLineString, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_string_multi", "Multi Line String Operator", "Do String Modifying Function",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_MultiString, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_tab_operator", "Tab Switcher Operator", "Change The Page In The Tab Group",
                          0, 0, OPEXT_UiItem, OPINV_Tab, OPMOD_TabSelector, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_collection_operator", "Collection UiItem Operator", "Activate/Deactivate Collection Item(s)",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_Collection, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_collection_selector_operator", "Collection Dropdown Selector Operator", "Do Selector Stuff",
                          0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_CollectionSelector, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_column_adjuster", "Column Width Adjuster", "Drag To Set Width of Columns",
                        0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_ColumnAdjuster, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_node_socket", "Node Socket Operator", "Drag to connect node sockets",
                        0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_NodeSocket, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_height_adjuster", "Height Adjuster Operator", "Drag to adjust heights of an instance",
                        0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_HeightAdjuster, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_value_mapper", "Value mapper Operator", "Drag to set value mapping from input to output",
                        0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_ValueMapper, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
    laCreateOperatorType("LA_mouse_action_reporter", "Mouse action reporter", "Report mouse actions to caller",
                        0, 0, OPEXT_UiItem, OPINV_UiItem, OPMOD_MouseActionReporter, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);
}

