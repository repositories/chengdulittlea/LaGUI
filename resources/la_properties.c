/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../la_5.h"

extern LA MAIN;
extern struct _tnsMain *T;

void laset_TerminalInput(void* unused, char* content){
    if((!content)||(!content[0])){ MAIN.TerminalInput[0]=0; return; }
    int len=strlen(content);
    if(MAIN.IsTerminalEnter){
        if(strSame(content,"clear")){ logClear(); MAIN.TerminalInput[0]=0; return; }
        logPrint("%s %s\n",MAIN.TerminalIncomplete?"...":">>>",content);
        terLoadLine(content,1);
        MAIN.TerminalInput[0]=0; return;
    }
    strcpy(MAIN.TerminalInput,content);
}

void lareset_Theme(laTheme *th){
    laBoxedTheme*bt; while(bt=lstPopItem(&th->BoxedThemes)){ strSafeDestroy(&bt->Name); *bt->BackRef=0; memFree(bt); }
    strSafeDestroy(&th->Name);
}
void lapostim_Theme(laTheme *th){
    for(laBoxedTheme* bt=th->BoxedThemes.pFirst;bt;bt=bt->Item.pNext){
        memAssignRef(bt, &bt->Parent,th);
    }
    MAIN.CurrentTheme = th;
    la_RefreshThemeColor(th);
    la_RegenerateWireColors();
    laRedrawAllWindows();
}
laTheme* laget_ThemePreviewTheme(void* unused, laTheme* theme){
    return theme;
}
void *laget_ActiveTheme(void *unused){ return MAIN.CurrentTheme; }
void laset_ActiveTheme(void* unused, laTheme* t){
    if(!t) MAIN.CurrentTheme = MAIN.Themes.pFirst;
    else MAIN.CurrentTheme = t;
    la_RefreshThemeColor(MAIN.CurrentTheme);
    la_RegenerateWireColors();
    laRedrawAllWindows();
}
void laset_ThemeName(laTheme *t, char *content){
    strSafeSet(&t->Name, content);
    char buf[32]; sprintf(buf,"LATHEME_%.22s",content);
    laset_InstanceUID(t, buf);
}
void laset_ThemeColor(laTheme* t, real* array){
    tnsVectorCopy4d(array,t->Color); la_RefreshThemeColor(t); laRedrawAllWindows();
}
void laset_ThemeColorB(laTheme* t, real* array){
    tnsVectorCopy4d(array,t->ColorB); la_RefreshThemeColor(t); laRedrawAllWindows();
}
void laset_ThemeColorC(laTheme* t, real* array){
    tnsVectorCopy4d(array,t->ColorC); la_RefreshThemeColor(t); laRedrawAllWindows();
}
void laset_ThemeAccentColor(laTheme* t, real* array){
    tnsVectorCopy4d(array,t->AccentColor); la_RefreshThemeColor(t); laRedrawAllWindows();
}
void laset_ThemeWarningColor(laTheme* t, real* array){
    tnsVectorCopy4d(array,t->WarningColor); la_RefreshThemeColor(t); laRedrawAllWindows();
}
void laset_ThemeInactiveSaturation(laTheme* t, real a){
    t->InactiveSaturation=a; la_RefreshThemeColor(t); laRedrawAllWindows();
}
void laset_ThemeInactiveMix(laTheme* t, real a){
    t->InactiveMix=a; la_RefreshThemeColor(t); laRedrawAllWindows();
}
void laset_ThemeCursorAlpha(laTheme* t, real a){
    t->CursorAlpha=a; la_RefreshThemeColorSelf(t); laRedrawAllWindows();
}
void laset_ThemeSelectionAlpha(laTheme* t, real a){
    t->SelectionAlpha=a; la_RefreshThemeColorSelf(t); laRedrawAllWindows();
}
void laset_ThemeShadowAlpha(laTheme* t, real a){
    t->ShadowAlpha=a; la_RefreshThemeColorSelf(t); laRedrawAllWindows();
}
void laset_ThemeTextShadowAlpha(laTheme* t, real a){
    t->TextShadowAlpha=a; la_RefreshThemeColorSelf(t); laRedrawAllWindows();
}
void laset_ThemeNormal(laBoxedTheme* bt, real val){
    bt->NormalY=val; la_RefreshBoxedThemeColor(bt); laRedrawAllWindows();
}
void laset_ThemeActive(laBoxedTheme* bt, real val){
    bt->ActiveY=val; la_RefreshBoxedThemeColor(bt); laRedrawAllWindows();
}
void laset_ThemeBorder(laBoxedTheme* bt, real val){
    bt->BorderY=val; la_RefreshBoxedThemeColor(bt); laRedrawAllWindows();
}
void laset_ThemeText(laBoxedTheme* bt, real val){
    bt->TextY=val; la_RefreshBoxedThemeColor(bt); laRedrawAllWindows();
}
void laset_ThemeTextActive(laBoxedTheme* bt, real val){
    bt->TextActiveY=val; la_RefreshBoxedThemeColor(bt); laRedrawAllWindows();
}
void laset_ThemeAlpha(laBoxedTheme* bt, real val){
    bt->Alpha=val; la_RefreshBoxedThemeColor(bt); laRedrawAllWindows();
}
void laset_ThemeBoxStyle(laBoxedTheme* bt, int val){
    bt->BoxStyle=val; la_RefreshBoxedThemeColor(bt); laRedrawAllWindows();
}
void laset_ThemeTextShadow(laBoxedTheme* bt, int val){
    bt->TextShadow=val; la_RefreshBoxedThemeColor(bt); laRedrawAllWindows();
}
void laset_ThemeColorSelection(laBoxedTheme* bt, int val){
    bt->ColorSelection=val; la_RefreshBoxedThemeColor(bt); laRedrawAllWindows();
}
void laset_ThemeNoDecalInvert(laBoxedTheme* bt, int val){
    bt->NoDecalInvert=val; la_RefreshBoxedThemeColor(bt); laRedrawAllWindows();
}
void laset_ThemeWireTransparency(laTheme* t, real val){ t->WireTransparency = val; la_RegenerateWireColors(); laRedrawAllWindows(); }
void laset_ThemeWireBrightness(laTheme* t, real val){ t->WireBrightness = val; la_RegenerateWireColors(); laRedrawAllWindows(); }
void laset_ThemeWireSaturation(laTheme* t, real val){ t->WireSaturation = val; la_RegenerateWireColors(); laRedrawAllWindows(); }
void laset_ThemeEdgeTransparency(laTheme* t, real val){ t->EdgeTransparency = val; la_RefreshThemeColorSelf(t); laRedrawAllWindows(); }
void laset_ThemeEdgeBrightness(laTheme* t, real val){ t->EdgeBrightness = val; la_RefreshThemeColorSelf(t); laRedrawAllWindows(); }
void laset_ThemeVertexTransparency(laTheme* t, real val){ t->VertexTransparency = val; la_RefreshThemeColorSelf(t); laRedrawAllWindows(); }
void laset_ThemeVertexBrightness(laTheme* t, real val){ t->VertexBrightness = val; la_RefreshThemeColorSelf(t); laRedrawAllWindows(); }
void laset_ThemeSVertexTransparency(laTheme* t, real val){ t->SelectedVertexTransparency = val; la_RefreshThemeColorSelf(t); laRedrawAllWindows(); }
void laset_ThemeSEdgeTransparency(laTheme* t, real val){ t->SelectedEdgeTransparency = val; la_RefreshThemeColorSelf(t); laRedrawAllWindows(); }
void laset_ThemeSFaceTransparency(laTheme* t, real val){ t->SelectedFaceTransparency = val; la_RefreshThemeColorSelf(t); laRedrawAllWindows(); }

void laread_UsingTheme(void* unused, char* name){ laTheme* t=laGetTheme(name); if(t){ laset_ActiveTheme(0,t); } }
void laget_UsingTheme(void* unused, char* result, char**result_direct){ laTheme* t=laget_ActiveTheme(0);
    if(t&&t->Name&&t->Name->Ptr){ *result_direct=t->Name->Ptr; return; } *result_direct="NOT FOUND";
}

void laset_WireColorSlices(void* unused, int val){ MAIN.WireColorSlices = val; la_RegenerateWireColors(); laRedrawAllWindows(); }
void laset_WireThickness(void* unused, real val){ MAIN.WireThickness = val;laRedrawAllWindows(); }
void laset_WireSaggyness(void* unused, real val){ MAIN.WireSaggyness = val; laRedrawAllWindows(); }

void* laget_DetachedControllerFirst(void* unused,void* unused2){
    return MAIN.Controllers.pFirst;
}

extern tnsFontManager* FM;

void laset_UiRowHeight(void* unused, int val){
    MAIN.UiRowHeight = val;
    MAIN.ScaledUiRowHeight = MAIN.UiRowHeight;
    MAIN.ScaledMargin = MAIN.ScaledUiRowHeight*MAIN.MarginSize*0.1f;
    la_RefreshThemeColor(MAIN.CurrentTheme);
    tfntResizeFontTexture(FM->UsingFont, TNS_FONT_BUFFER_W_DEFAULT);
    tnsInvalidateFontCache();
    laRedrawAllWindows();
}
void laset_FontSize(void* unused, real val){
    MAIN.FontSize = val;
    MAIN.ScaledUiRowHeight=MAIN.UiRowHeight;
    MAIN.ScaledMargin = MAIN.ScaledUiRowHeight*MAIN.MarginSize*0.1f;
    tnsInvalidateFontCache();
    tfntResizeFontTexture(FM->UsingFont, TNS_FONT_BUFFER_W_DEFAULT);
    laRedrawAllWindows();
}
void laset_MarginSize(void* unused, real val){
    MAIN.MarginSize = val;
    MAIN.ScaledMargin = MAIN.ScaledUiRowHeight*MAIN.MarginSize*0.1f;
    la_RefreshThemeColor(MAIN.CurrentTheme);
    laRedrawAllWindows();
}

void laset_ResourcePath(laResourceFolder *rf, char *content){
    strSafeSet(&rf->Path, content);
    laRefreshUDFRegistries();
}

void *laget_Self(void *p, void *UNUSED){
    return p;
}

void *laget_ListNext(laListItem *Item, void *UNUSED){
    return Item->pNext;
}
void *laget_ListPrev(laListItem *Item, void *UNUSED){
    return Item->pPrev;
}
void *laget_List2Next(laListItem2 *Item, void *UNUSED){
    return Item->pNext;
}
void *laget_List2Prev(laListItem2 *Item, void *UNUSED){
    return Item->pPrev;
}
void *laget_PointerListNext(void *UNUSED, laPropIterator *pi){
    pi->Linker = ((laListItemPointer *)pi->Linker)->pNext;
    return pi->Linker->p;
}

void *laget_FirstUiItem(laUiList *uil, void *UNUSED){
    return uil->UiItems.pFirst;
}
void *laget_FirstColumnItem(laUiList *uil, void *UNUSED){
    if (!uil) return 0;
    return uil->Columns.pFirst;
}
int la_ResetUiColum(laColumn *c, int U, int L, int R, int LR, int repos);
void laset_ColumnSP(laColumn *c, real val){
    if (c->LS) c->LS->SP = val;
    if (c->RS) c->RS->SP = val;
}
real laget_ColumnSP(laColumn *c){
    if (c->LS) return c->LS->SP;
    else
        return 0.5;
}
void laset_ColumnSnap(laColumn *c, int val){
    if (c->LS && c->LS->MaxW) c->LS->MaxW = val;
    if (c->RS && c->RS->MaxW) c->RS->MaxW = val;
}
int laget_ColumnSnap(laColumn *c){
    if (c->LS && c->LS->MaxW) return c->LS->MaxW;
    if (c->RS && c->RS->MaxW) return c->RS->MaxW;
    return 20;
}
void laset_SnapState(laColumn *c, int val){
    if (val == 1){
        if (c->LS) c->LS->MaxW = 20;
        if (c->RS) c->RS->MaxW = 0;
    }elif (val == 2){
        if (c->LS) c->LS->MaxW = 0;
        if (c->RS) c->RS->MaxW = 20;
    }else{
        if (c->LS) c->LS->MaxW = 0;
        if (c->RS) c->RS->MaxW = 0;
    }
}
int laget_SnapState(laColumn *c){
    if (c->LS && c->LS->MaxW) return 1;
    if (c->RS && c->RS->MaxW) return 2;
    return 0;
}
void *laget_BoxedTheme(laTheme *theme){
    return theme->BoxedThemes.pFirst;
}
void *laget_BoxedThemeUi(void *UNUSED){
    return ((laTheme *)MAIN.Themes.pFirst)->BoxedThemes.pFirst;
}

void* laget_Main(void *UNUSED, void* UNUSED2){
    return &MAIN;
}

laPropContainer* laget_PropertyNodeType(laProp* p){
    static laPropContainer* pc=0;
    static laPropContainer* ipc=0;
    static laPropContainer* fpc=0;
    switch(p->PropertyType&(~LA_PROP_ARRAY)){
        case LA_PROP_INT: { if(!ipc) ipc=la_ContainerLookup("int_property"); return ipc; }
        case LA_PROP_FLOAT: { if(!fpc) fpc=la_ContainerLookup("float_property"); return fpc; }
        default: { if(!pc) pc=la_ContainerLookup("property_item"); return pc; }
    }
}

void *laget_PropertyItemFirst(laPropContainer *p){
    if (!p) return 0;
    return p->Props.pFirst;
}
void *laget_PropertyItemNext(laProp *p, void *UNUSED){
    return p->Item.pNext;
}
void laget_PropertyName(laProp *p, char *result,char** result_direct){
    strCopyFull(result, p ? p->Name : "");
}
void laget_PropertyIdentifier(laProp *p, char *result,char** result_direct){
    strCopyFull(result, p ? p->Identifier : "");
}
void laget_PropertyDescription(laProp *p, char *result,char** result_direct){
    strCopyFull(result, p ? p->Description : "");
}
int laget_PropertySubContainerIconID(laProp *p){
    if (p->PropertyType != LA_PROP_SUB) return 0;
    if (!p->SubProp){
        p->SubProp = la_ContainerLookup(((laSubProp *)p)->TargetID);
        if (!p->SubProp) return 0;
    }
    return p->SubProp->IconID;
}

void* laget_TrashItemInstance(void *a){
    return a;
}

void *laget_ActiveWindow(void *unused){
    return MAIN.CurrentWindow;
}
void *laget_FirstWindow(void *unused, void* unused_iter){
    return MAIN.Windows.pFirst;
}
void *laget_FirstContainer(void *unused, void* unused_iter){
    return MAIN.PropContainers.pFirst;
}
void *laget_WindowFirstLayout(laWindow *window, void* unused_iter){
    return window->Layouts.pFirst;
}
void *laget_FirstHiddenPanel(laWindow *window, void* unused_iter){
    for(laPanel* p=window->Panels.pFirst;p;p=p->Item.pNext){
        if(!p->Show&&!p->LaterDestroy){return p;}
    }
    return 0;
}
void *laget_NextHiddenPanel(laPanel* p, void* unused_iter){
    for(p=p->Item.pNext;p;p=p->Item.pNext){
        if(!p->Show&&!p->LaterDestroy){return p;}
    }
    return 0;
}
void laset_WindowHiddenPanel(laWindow *window, laPanel* p){
    laShowPanelWithExpandEffect(p);
    laPopPanel(p);
}
void laget_PanelTitle(laPanel *p, char *result,char** result_direct){
    strCopyFull(result, p->Title->Ptr);
}
void laset_PanelTitle(laPanel *p, char *content){
    strSafeSet(&p->Title, content);
    p->TitleWidth = tnsStringGetWidth(transLate(p->Title->Ptr), 0,0);
}
void laread_PanelTitle(laPanel *p, char *content){
    strSafeSet(&p->Title, content);
}
void laset_PanelSnapDistance(laPanel *p, int index, int n){
    int a;
    if (!n) return;
    switch (index){ //lrtb
    case 0:
        p->SL = p->SL ? n : 0;
        break;
    case 1:
        p->SR = p->SR ? n : 0;
        break;
    case 2:
        p->ST = p->ST ? n : 0;
        break;
    case 3:
        p->SB = p->SB ? n : 0;
        break;
    }
}
void laread_PanelSnapDistance(laPanel* p, int index, int n) {
    int a;
    if (!n) return;
    switch (index) { //lrtb
    case 0: p->SL = n; break;
    case 1: p->SR = n; break;
    case 2: p->ST = n; break;
    case 3: p->SB = n; break;
    }
}
void laset_PanelSnapEnable(laPanel *p, int index, int n){
    laWindow *w = MAIN.CurrentWindow;
    int a;
    switch (index){ //lrtb
    case 0:
        p->SL = (n && p->X) ? p->X : 0;
        break;
    case 1:
        p->SR = (n && w ? ((a = MAIN.CurrentWindow->CW - p->X - p->W) ? a : 0) : n);
        break;
    case 2:
        p->ST = (n && p->Y) ? p->Y : 0;
        break;
    case 3:
        p->SB = (n && w ? ((a = MAIN.CurrentWindow->CH - p->Y - p->H) ? a : 0) : n);
        break;
    }
}
void laget_PanelSnapEnable(laPanel *p, int *result){
    result[0] = p->SL ? 1 : 0;
    result[1] = p->SR ? 1 : 0;
    result[2] = p->ST ? 1 : 0;
    result[3] = p->SB ? 1 : 0;
}
void laget_LayoutTitle(laLayout *l, char *result, char** result_direct){
    strCopyFull(result, l->ID->Ptr);
}
void laset_LayoutTitle(laLayout *l, char *content){
    strSafeSet(&l->ID, content);
    laRenameWindow(MAIN.CurrentWindow, SSTR(l->ID));
}
//void* laget_LayoutPanelFirst(laLayout* l) {
//	return l->Panels.pFirst;
//}
void laset_WindowActiveLayout(laWindow *w, laLayout *l, int UNUSED_State){
    la_StopAllOperators();
    memAssignRef(w, &w->CurrentLayout, l);
    if(!w->win){return;}
    laRenameWindow(w, l->ID->Ptr);
    laRedrawCurrentWindow();
}
void* laget_WindowActiveLayout(laWindow *w, void* unused){
    return w->CurrentLayout;
}
//void laget_PanelSnappingTrueFalse(laPanel* p, int* result) {
//	result[0] = p->SL > 0 ? 1 : 0;
//	result[1] = p->SR > 0 ? 1 : 0;
//	result[2] = p->ST > 0 ? 1 : 0;
//	result[3] = p->SB > 0 ? 1 : 0;
//}
//void laset_PanelSnappingTrueFalse(laPanel* p,int Index, int n) {
//	int* v = &p->SL;
//	int val = abs(v[Index]);
//	v[Index] = val*(n ? 1 : -1);
//}

void laset_PanelMultisample(void* unused, int value){
    MAIN.PanelMultisample = value;
    tnsReconfigureTextureParameters(value);
    laRedrawAllWindows();
}

void *laget_UiInternalType(void *UNUSED){
    return MAIN.UiTypes.pFirst;
}

void laget_TimeString(laTimeInfo *ti, char *result,char** result_direct){
    sprintf(result, "%d-%.02d-%.02d %.02d:%.02d:%.02d", ti->Year, ti->Month, ti->Day, ti->Hour, ti->Minute, ti->Second);
}
int laget_TimeYear(laTimeInfo *ti){
    return (int)ti->Year;
}
int laget_TimeMonth(laTimeInfo *ti){
    return (int)ti->Month;
}
int laget_TimeDay(laTimeInfo *ti){
    return (int)ti->Day;
}
int laget_TimeHour(laTimeInfo *ti){
    return (int)ti->Hour;
}
int laget_TimeMinute(laTimeInfo *ti){
    return (int)ti->Minute;
}
int laget_TimeSecond(laTimeInfo *ti){
    return (int)ti->Second;
}

void *laget_ConditionerExtra(laUiItem *ui){
    if (ui->Type == &_LA_UI_CONDITION ||
        ui->Type == &_LA_UI_CONDITION_END ||
        ui->Type == &_LA_UI_CONDITION_ELSE ||
        ui->Type == _LA_UI_CONDITION_TOGGLE){
        return ui->Extra;
    }
    return 0;
}
void *laget_CanvasExtra(laUiItem *ui){
    if (ui->Type == _LA_UI_CANVAS){
        return ui->Extra;
    }
    return 0;
}

void laget_UnknownPropertyString(laUDF *udf, char *result, char** here){
    *here="Unknown Property";
}
void laget_MainIdentifier(laUDF *udf, char *result, char** here){
    strcpy(result, "MAIN");
}
void laget_UDFFilePath(laUDF *udf, char *result, char** here){
    *here=udf->FileName->Ptr;
}
void laset_ManagerFilterInstances(void* unused, int val){
    MAIN.ManagerFilterInstances = val;
    laRedrawAllWindows();
}

void lapost_PropPack(laPropPack *pp);
void laget_ConditionNodePropPath(laUiConditionNode *ucn, char *result, char** here){
    if (!ucn->PP.Go) return;
    la_GetPropPackPath(&ucn->PP, result);
}
void laset_ConditionNodePropPath(laUiConditionNode *ucn, char *content, char** here){
    if (ucn->PP.Go){
        la_FreePropStepCache(ucn->PP.Go);
        ucn->PP.LastPs = ucn->PP.Go = 0;
    }
    la_GetPropFromPath(&ucn->PP, ucn->PP.RawThis, content, 0);
}
void laread_ConditionNodePropPath(laUiConditionNode *ucn, char *Content, char** here){
    if (!Content || !Content[0]) return;
    ucn->PP.Go = CreateNewBuffer(char, strlen(Content) + 1);
    strcpy(ucn->PP.Go, Content);
}
void lapost_ConditionNode(laUiConditionNode *ucn){
    lapost_PropPack(&ucn->PP);
}
void la_ConditionNodeFreeRecursive(laUiConditionNode *ucn);
void laset_ConditionNodeType(laUiConditionNode *ucn, int Type){
    ucn->Type = Type;
    switch (Type){
    case LA_CONDITION_PROP:
    case LA_CONDITION_INT:
    case LA_CONDITION_FLOAT:
    case LA_CONDITION_STRING:
    case LA_CONDITION_FALSE:
    case LA_CONDITION_TRUE:
        la_ConditionNodeFreeRecursive(ucn->Expression1);
        la_ConditionNodeFreeRecursive(ucn->Expression2);
        ucn->Expression1 = 0;
        ucn->Expression2 = 0;
        if (ucn->PP.LastPs) la_FreePropStepCache(ucn->PP.Go);
        ucn->PP.LastPs = ucn->PP.Go = ucn->PP.RawThis = 0;
        break;
    case LA_CONDITION_NOT:
        la_ConditionNodeFreeRecursive(ucn->Expression2);
        ucn->Expression2 = 0;
        if (ucn->PP.LastPs) la_FreePropStepCache(ucn->PP.Go);
        ucn->PP.LastPs = ucn->PP.Go = ucn->PP.RawThis = 0;
        if (!ucn->Expression1) ucn->Expression1 = laIntExpression(0);
        break;
    default:
        if (!ucn->Expression1) ucn->Expression1 = laIntExpression(0);
        if (!ucn->Expression2) ucn->Expression2 = laIntExpression(1);
        break;
    }
    laRecalcCurrentPanel();
}
void laread_ConditionNodeType(laUiConditionNode *ucn, int Type){
    ucn->Type = Type;
}

void* laget_FirstDriverPage(laRackPageCollection* rpc, void* unused2){
    return rpc->Pages.pFirst;
}
void* laget_CurrentRackPage(laRackPageCollection* c){
    return c->CurrentPage;
}
void laset_CurrentRackPage(laRackPageCollection* c,laRackPage* rp){
    memAssignRef(c,&c->CurrentPage,rp);
}
void* laget_CurrentAnimationAction(void* unused_a){
    return MAIN.Animation->CurrentAction;
}
void laset_CurrentAnimationAction(void* unused_a, laAction* c){
    memAssignRef(MAIN.Animation,&MAIN.Animation->CurrentAction,c);
    laNotifyUsers("la.animation.current_action");
}

void laset_AutoSwitchColorSpace(void* unused, int enabled){
    MAIN.AutoSwitchColorSpace = enabled;
    if(enabled){
        for(laWindow* w=MAIN.Windows.pFirst;w;w=w->Item.pNext){
            laScreen* s = laGetWindowScreen(w);
            if(s){ w->OutputColorSpace = s->ColorSpace; laNotifyInstanceUsers(w); }
        }
    }
    laNotifyUsers("la.user_preferences.auto_switch_color_space");
}
void laset_ScreenColorSpace(laScreen* s, int space){
    s->ColorSpace=space;
    if(MAIN.AutoSwitchColorSpace){
        for(laWindow* w=MAIN.Windows.pFirst;w;w=w->Item.pNext){
            laScreen* s = laGetWindowScreen(w);
            if(s){ w->OutputColorSpace = s->ColorSpace; laNotifyInstanceUsers(w); }
        }
    }
}

void laset_WindowColorSpace(laWindow* w, int space){ w->OutputColorSpace=space; MAIN.LutNeedsRefreshing=1; if(w->win) laRedrawCurrentWindow(); }
void laset_WindowOutputProofing(laWindow* w, int v){ w->OutputProofing=v; MAIN.LutNeedsRefreshing=1; if(w->win) laRedrawCurrentWindow(); }
void laset_WindowShowStripes(laWindow* w, int stripes){ w->OutputShowStripes=stripes; if(w->win) laRedrawCurrentWindow(); }
void laset_WindowUseComposing(laWindow* w, int comp){ w->UseComposing=comp; if(w->win) laRedrawCurrentWindow(); }
void laset_WindowComposingGamma(laWindow* w, real v){ w->ComposingGamma=v; if(w->win) laRedrawCurrentWindow(); }
void laset_WindowComposingBlackpoint(laWindow* w, real v){ w->ComposingBlackpoint=v; if(w->win) laRedrawCurrentWindow(); }

void *tnsget_TnsMain(void *unused,void *unused2){
    return T;
}
void *tnsget_World(void *unused,void *unused2){
    return T->World;
}
void tnsread_World(void *unused, void *inst){
}
void tnsset_InstancerInstance(tnsInstancer *o, tnsObject* value){
    o->Instance=value; tnsInvalidateEvaluation(o); laNotifyUsers("tns.world");
}
void tnsset_InstancerHook(tnsInstancer *o, int Hook){ laNotifyUsers("tns.world");
    o->Hook=Hook; laNotifyInstanceUsers(o); tnsInvalidateEvaluation(o); tnsInvalidateEvaluation(o->Instance);
}
void tnssetarr_InstancerHookOffset(tnsInstancer *o, real* arr){ laNotifyUsers("tns.world");
    tnsVectorSet2v(o->HookOffset,arr); laNotifyInstanceUsers(o); tnsInvalidateEvaluation(o); tnsInvalidateEvaluation(o->Instance);
}
void *tnsget_detached_FirstRootObject(void *UNUSED1, void *UNUSED2){
    return T->World->RootObjects.pFirst;
}
void *tnsget_detached_FirstMaterial(void *UNUSED1, void *UNUSED2){
    return T->World->Materials.pFirst;
}
void *tnsget_detached_FirstTexture(void *UNUSED1, void *UNUSED2){
    return T->Textures.pFirst;
}
void *tnsget_PreviewTexture(void *UNUSED){
    return T->PreviewTexture;
}
void tnsset_PreviewTexture(void *UNUSED_PARENT, void *tex, int UNUSEDSTATE){
    T->PreviewTexture = tex;
}
void *tnsget_NextLinkedObject(tnsObject *o, laPropIterator *pi){
    pi->Linker = ((laListItemPointer *)pi->Linker->pNext);
    return pi->Linker->p;
}
void *tnsget_FirstChildObject(tnsObject *ob){
    return ob->ChildObjects.pFirst;
}
void tnsset_RootObjectIs2D(tnsRootObject *root, int Is2D){
    root->Is2D=Is2D; laNotifyInstanceUsers(root);
}

void tnsset_ObjectLocation(tnsObject* o, int n, real val){ o->Location[n]=val;tnsSelfTransformValueChanged(o); laNotifyUsers("tns.world"); }
void tnsset_ObjectRotation(tnsObject* o, int n, real val){ o->Rotation[n]=val; tnsSelfTransformValueChanged(o); laNotifyUsers("tns.world"); }
void tnsset_ObjectScale(tnsObject* o, int n, real val){ o->Scale[n]=val; tnsSelfTransformValueChanged(o); laNotifyUsers("tns.world"); }
void tnsset_ObjectDLocationARR(tnsObject* o, real* arr){ tnsVectorCopy3d(arr,o->DLocation); tnsDeltaTransformValueChanged(o); laNotifyUsers("tns.world"); }
void tnsset_ObjectDRotationARR(tnsObject* o, real* arr){ tnsVectorCopy3d(arr,o->DRotation); tnsDeltaTransformValueChanged(o); laNotifyUsers("tns.world"); }
void tnsset_ObjectDScaleARR(tnsObject* o, real* arr){ tnsVectorCopy3d(arr,o->DScale); tnsDeltaTransformValueChanged(o); laNotifyUsers("tns.world"); }

tnsMeshObject* tnsget_ObjectAsMesh(tnsObject* o){ if(!o || o->Type!=TNS_OBJECT_MESH) return 0; return o; }
tnsRootObject* tnsget_ObjectAsRoot(tnsObject* o){ if(!o || o->Type!=TNS_OBJECT_ROOT) return 0; return o; }

void laget_UiTemplateIdentifier(laUiTemplate *uit, char *result, char** here){
    *here=uit->Identifier->Ptr;
}
void laset_UiTemplateIdentifier(laUiTemplate *uit, char *content){
    strSafeSet(&uit->Identifier, content);
}
void laget_UiDataPath(laUiItem *ui, char *result){
    laPropStep *ps = ui->PP.Go;
    if (ui->PP.LastPs || ui->PP.Go){
        for (ps; ps; ps = ps->pNext){
            if (ps->Type == U'@' || ps->Type == U'#' || ps->Type == U'='){
                strcat(result, ps->p);
            }else{
                strcat(result, ps->p->Identifier);
            }
            if (ps->pNext){
                if (ps->pNext->Type == U'.') strcat(result, ".");
                elif (ps->pNext->Type == U'#') strcat(result, "#");
                elif (ps->pNext->Type == U'@') strcat(result, "@");
                elif (ps->pNext->Type == U'=') strcat(result, "= ");
                elif (ps->pNext->Type == U'$') strcat(result, "$");
            }
        }
    }elif (ui->AT){
        strcpy(result, "");
    }
}
void laget_UiOperatorID(laUiItem *ui, char *result, char** here){
    if (ui->AT){
        *here=ui->AT->Identifier;
    }else *result=0;
}
void laread_UiDataPath(laUiItem *ui, char *Content){
    if (!Content || !Content[0]) return;
    ui->PP.Go = CreateNewBuffer(char, strlen(Content) + 1);
    strcpy(ui->PP.Go, Content);
}
void laread_UiOperatorID(laUiItem *ui, char *Content){
    ui->AT = laGetOperatorType(Content);
}

void *laget_PanelTemplate(void *UNUSED, void *UNUSED2){
    return MAIN.PanelTemplates.pFirst;
}
void laget_PanelTemplateCategory(void* rack_unused, laUiTemplate* uit, char* copy, char** ptr){
    if(uit->CategoryName&&uit->CategoryName->Ptr) *ptr=uit->CategoryName->Ptr;
}

void lapost_Window(laWindow *w){
    laStartWindow(w);
    laDisableIME(w);
}
int la_CreateSystemWindow(laWindow *window, int synctovbank);
void la_AssignWindowPP(laWindow* w);
void lapostim_Window(laWindow *w){
    w->IsFullScreen=0;
    la_CreateSystemWindow(w,MAIN.Windows.pFirst==MAIN.Windows.pLast);
    la_AssignWindowPP(w);
    MAIN.CurrentWindow = w;
}
void lapost_PropPack(laPropPack *pp){
    char *path = pp->Go;
    if (pp->LastPs /* || !path*/) return;
    if ((!path || (path && !path[0])) && pp->RawThis) pp->LastPs = pp->RawThis->LastPs;
    if (pp->RawThis) lapost_PropPack(pp->RawThis);
    pp->Go = 0;
    la_GetPropFromPath(pp, pp->RawThis, path, 0);
    FreeMem(path);
}
void lapost_UiItem(laUiItem *ui){
    lapost_PropPack(&ui->PP);
    //if(ui->Type == _LA_UI_CANVAS) ui->Type->Init(ui);
}
void la_AssignPropExtras(laUiItem* ui);
void lapostim_UiItem(laUiItem *ui){
    if (ui->Type && ui->Type->Init && ui->Type != _LA_UI_CANVAS && ui->Type != &_LA_UI_CONDITION) ui->Type->Init(ui);
    ui->State = ui->State ? ui->State : (ui->Type ? LA_UI_NORMAL : 0);

    la_AssignPropExtras(ui);
}
void* laget_SavePanel(laWindow* w, laPropIterator* pi){
    for(laPanel* p=w->Panels.pFirst;p;p=p->Item.pNext){ if(!p->Mode || p->IsMenuPanel || p->PanelTemplate) return p; }
    return 0;
}
void* lagetnext_SavePanel(laPanel* p, laPropIterator* pi){
    for(p=p->Item.pNext;p;p=p->Item.pNext){ if(!p->Mode || p->IsMenuPanel || p->PanelTemplate) return p; }
    return 0;
}
void lapost_Panel(laPanel *p){
    p->PP.EndInstance = p;
    p->PP.LastPs = &p->FakePS;
    p->PP.LastPs->p = _LA_PROP_PANEL;
    p->PP.LastPs->UseInstance = p;
    p->PP.LastPs->Type = U'.';

    la_EnsurePanelExtras(p);

    laRecalcPanel(p);
    p->FrameDistinguish = 100;
    if(p->IsMenuPanel || p->Block) p->Show=1;

    laUiTemplate* uit=p->PanelTemplate;
    if(uit&&uit->PropFunc){ uit->PropFunc(p); }
    if(uit){
        uit->Define(&p->UI, &p->PP, &p->PropLinkPP, 0, 0);
        if(p->Mode) laui_DefaultPanelTitleBar(&p->TitleBar, &p->PP, &p->PropLinkPP, uit->Header);
    }
    if(p->IsMenuPanel){ laui_DefaultMenuBarActual(&p->TitleBar, &p->PP, &p->PropLinkPP, 0, 0); }
    //if(p->Title) p->TitleWidth = tnsStringGetWidth(transLate(p->Title->Ptr), 0, 0);
#ifdef LA_LINUX
    XSync(MAIN.dpy,0);
#endif
}
void la_AssignBlockPP(laBlock* b);
void lapost_Block(laBlock *b){
    la_AssignBlockPP(b);
}
void lapost_UserPreferences(void* unused){
    //MAIN.ScaledUiRowHeight=MAIN.UiRowHeight;
    //tnsInvalidateFontCache();
}
void laset_ColorPickerGamma(void* unused, real gamma){
    MAIN.ColorPickerGamma=gamma; laRedrawCurrentWindow();
}
void laset_ViewportHalftoneFactor(void* unused, real v){
    MAIN.ViewportHalftoneFactor=v; laNotifyUsers("tns.world");
}
void laset_ViewportHalftoneSize(void* unused, real v){
    MAIN.ViewportHalftoneSize=v; laNotifyUsers("tns.world");
}

void laset_EnableGLDebug(void* unused, int e){ MAIN.EnableGLDebug=e; la_NotifyGLDebugChanges(); }
void laset_GLDebugSync(void* unused, int e){ MAIN.GLDebugSync=e; la_NotifyGLDebugChanges(); }
void laset_GLDebugLevel(void* unused, int e){  MAIN.GLDebugLevel=e; la_NotifyGLDebugChanges(); }

void laset_Language(void* unused, laTranslationNode* lang){
    MAIN.Translation.CurrentLanguage = lang;
    laRedrawAllWindows();
}

//void laget_DetachedPropContainerID(laProp* p, char * result) {
//	strcpy(result, p->Detached->Container->Identifier);
//}
//void laget_DetachedPropPropID(laProp* p, char * result) {
//	strcpy(result, p->Detached->Identifier);
//}
//void laset_DetachedPropContainerID(laProp* p, char * content) {
//	p->Description = CreateNewBuffer(char, 128);
//	strcpy(p->Description, content);
//}
//void laset_DetachedPropPropID(laProp* p, char * content) {
//	p->Name = CreateNewBuffer(char, 128);
//	strcpy(p->Name, content);
//}
void laread_DetachedPropRename(laProp *p, char *content){
    p->Identifier = CreateNewBuffer(char, 128);
    strcpy(p->Identifier, content);
}
void laget_DetachedPropPath(laProp *p, char *result, char** here){
    la_GetPropPackPath(&p->DetachedPP, result);
}
void laread_DetachedPropPath(laProp *p, char *content){
    p->DetachedPP.Go = CreateNewBuffer(char, 128);
    strcpy(p->DetachedPP.Go, content);
}
int la_GetPropertySize(int Type);
void lapost_DetachedProp(laProp *Prop){
    laPropContainer *pc = 0;
    laSubProp *np = 0;
    laIntProp *ip;
    laFloatProp *fp;
    laEnumProp *ep;
    laSubProp *sp;
    char *path = Prop->DetachedPP.Go;
    char *id = Prop->Identifier;
    laPropPack TempPP = {0};
    laListItem Item;

    Prop->DetachedPP.Go = 0;
    memcpy(&Item, &Prop->Item, sizeof(laListItem));

    la_GetPropFromPath(&TempPP, Prop->DetachedPP.RawThis, path, 0);
    //FreeMem(path);

    memcpy(Prop, TempPP.LastPs->p, la_GetPropertySize(TempPP.LastPs->p->PropertyType));

    memcpy(&Prop->DetachedPP, &TempPP, sizeof(laPropPack));
    memcpy(&Prop->Item, &Item, sizeof(laListItem));

    switch (Prop->PropertyType){
    case LA_PROP_INT:
    case LA_PROP_INT | LA_PROP_ARRAY:
        ip = Prop;
        ip->Detached = CreateNewBuffer(int, Prop->Len ? Prop->Len : 1);
        break;
    case LA_PROP_FLOAT:
    case LA_PROP_FLOAT | LA_PROP_ARRAY:
        fp = Prop;
        fp->Detached = CreateNewBuffer(real, Prop->Len ? Prop->Len : 1);
        break;
    case LA_PROP_ENUM:
    case LA_PROP_ENUM | LA_PROP_ARRAY:
        ep = Prop;
        ep->Detached = CreateNewBuffer(int, 1);
        ep->Detached[0] = ((laEnumItem *)ep->Items.pFirst)->Index;
    }

    Prop->Identifier = id;
}

void latouched_NodeInSocket(void* unused, int hint){
    laGraphRequestRebuild();
}

int lafilter_NodeCategory(void* unused, laNodeCategory* c){
    if(c->For&MAIN.FilterNodeCategory) return 1; return 0;
}

void* laget_FirstInputMapping(void* unused1,void* unused2){
    return MAIN.InputMapping->InputMappings.pFirst;
}
void* laget_CurrentInputMapping(laInputMappingBundle* imb){
    return imb->CurrentInputMapping;
}
void* laget_FirstToolbox(void* unused1,void* unused2){
    return MAIN.InputMapping->Toolboxes.pFirst;
}
void* laget_CurrentToolbox(laInputMappingBundle* imb){
    return imb->CurrentToolbox;
}
void *laget_detached_FirstToolbox(void *UNUSED1, void *UNUSED2){
    return MAIN.InputMapping->Toolboxes.pFirst;
}
void* laget_FirstCustomSignal(void* unused1,void* unused2){
    return MAIN.CustomSignals.pFirst;
}
void laset_InputMappingEntrySignal(laInputMappingEntry* ime, laCustomSignal* cs){
    if(cs){ strSafeSet(&ime->Signal,SSTR(cs->Name)); ime->SignalValue=cs->Signal; }
    else{ strSafeSet(&ime->Signal,"none"); ime->SignalValue=0; }
}
void laset_InputMappingEntryKeyString(laInputMappingEntry* ime, char* key){
    strSafeSet(&ime->Key,key); laInputMappingUpdateSignal(ime); 
}
void laset_InputMappingEntrySignalString(laInputMappingEntry* ime, char* sig){
    strSafeSet(&ime->Signal,sig); laInputMappingUpdateSignal(ime); 
}
void laset_InputMappingEntryMove(laInputMappingEntry* ime, int move){
    laInputMapping* im = ime->Parent;
    if(move<0 && ime->Item.pPrev){ lstMoveUp(&im->Entries, ime); laNotifyUsers("la.input_mapping"); }
    elif(move>0 && ime->Item.pNext){ lstMoveDown(&im->Entries, ime); laNotifyUsers("la.input_mapping"); }
}
void laset_InputMappingEntryOperatorString(laInputMappingEntry* ime, char* string){
    strSafeSet(&ime->Operator,string);
    laOperatorType* at=laGetOperatorType(string);
    if(at){
        strSafeSet(&ime->OperatorName, at->Name);
        ime->OperatorType = at;
    }else{
        strSafeSet(&ime->OperatorName, string);
    }
}
void laset_SignalFilter(void* unused, char* sig){
    strSafeSet(&MAIN.SignalFilter,sig);
    laNotifyUsers("la.filtered_signals");
}
void* laget_FirstFilteredCustomSignal(void* unused1,laPropIterator* pi){
    laCustomSignal* p = MAIN.CustomSignals.pFirst;
    while(!strstr(SSTR(p->Name),SSTR(MAIN.SignalFilter))){ p=p->Item.pNext; if(!p){ return 0; } }
    return p;
}
void* lagetnext_FilteredSignal(laCustomSignal* p, laPropIterator* pi){
    p=p->Item.pNext; if(!p){ return 0; }
    while(!strstr(SSTR(p->Name),SSTR(MAIN.SignalFilter))){ p=p->Item.pNext; if(!p){ return 0; } }
    return p;
}
void laset_OperatorFilter(void* unused, char* sig){
    strSafeSet(&MAIN.OperatorFilter,sig);
    laNotifyUsers("la.filtered_operators");
}
void* laget_FirstFilteredOperator(void* unused1,laPropIterator* pi){
    laOperatorType* at = MAIN.OperatorList.pFirst;
    while(at->ExtraMark & (LA_ACTUATOR_HIDDEN|LA_ACTUATOR_SYSTEM)){
        at=at->ListItem.pNext; if(!at){ return 0; }
    }
    while((!strstr(at->Name,SSTR(MAIN.OperatorFilter))) && (!strstr(at->Identifier,SSTR(MAIN.OperatorFilter)))){
        at=at->ListItem.pNext; if(!at){ return 0; }
    }
    return at;
}
void* lagetnext_FilteredOperator(laOperatorType* at, laPropIterator* pi){
    at=at->ListItem.pNext; if(!at){ return 0; }
    while(at->ExtraMark & (LA_ACTUATOR_HIDDEN|LA_ACTUATOR_SYSTEM)){
        at=at->ListItem.pNext; if(!at){ return 0; }
    }
    while((!strstr(at->Name,SSTR(MAIN.OperatorFilter))) && (!strstr(at->Identifier,SSTR(MAIN.OperatorFilter)))){
        at=at->ListItem.pNext; if(!at){ return 0; }
    }
    return at;
}

void tnspost_World(tnsWorld *w){
    tnsRefreshMaterialLibraries();
    laAnimationUpdateHolderList();
}
void tnspost_Material(tnsMaterial *m){
    tns_GetMaterial2D(m);
    //if(m->AsLibrary){ tnsRefreshMaterialLibraries(); return; }
    //tnsEnsureMaterialShader(m,1);
}
void tnspost_Object(tnsObject *o){ //XXX: No instance clear operation for object?
    laListItemPointer* NextLip,*NextLip2;
    if(o->Type!=TNS_OBJECT_ROOT){
        tnsDeltaTransformValueChanged(o); tnsGlobalMatrixChanged(o,1);
    }else{ tnsRootObject* ro=o;
        for(laAction* aa=ro->Actions.pFirst;aa;aa=aa->Item.pNext){
            if(!aa->HolderContainer){
                aa->HolderContainer=la_ContainerLookup("tns_root_object");
                aa->HolderInstance=ro;
            }
        }
    }
    for(laListItemPointer* lip=o->ChildObjects.pFirst;lip;lip=NextLip){
        NextLip=lip->pNext; if(!lip->p){ lstRemoveItem(&o->ChildObjects,lip); memFree(lip); continue; }
        for(laListItemPointer* lip2=NextLip;lip2;lip2=NextLip2){ NextLip2=lip2->pNext;
            if(lip2->p==lip->p){ NextLip=lip2->pNext; lstRemoveItem(&o->ChildObjects,lip2); memFree(lip2);  continue; }
        }
    }
}
void tnspropagate_Object(tnsObject* o, laUDF* udf, int force){
    for(laListItemPointer* lip=o->ChildObjects.pFirst;lip;lip=lip->pNext){ if(!lip->p) continue;
        if(force || !laget_InstanceActiveUDF(lip->p)){ laset_InstanceUDF(lip->p, udf); tnspropagate_Object(lip->p,udf,force); }
    }
}
void tnstouched_Object(tnsObject *o, int hint){
    printf("touched\n");
    if(o->Type==TNS_OBJECT_MESH && (hint&TNS_HINT_GEOMETRY)){
        tnsInvalidateMeshBatch(o);
    }
    tnsInvalidateEvaluation(o);
    laNotifyUsers("tns.world");
}
int tnsfilter_SavableObject(void* unused, tnsObject* o){
    if(o->InRoot) return 1; return 0;
}

int tnsget_MeshObjectVertSize(tnsMeshObject* o){ return o->totv*sizeof(tnsVert);/*can't use maxv*/ }
int tnsget_MeshObjectEdgeSize(tnsMeshObject* o){ return o->tote*sizeof(tnsEdge);/*can't use maxv*/ }
void* tnsget_MeshObjectFaceRaw(tnsMeshObject* o, int* r_size, int* r_is_copy){
    if(o->Mode==TNS_MESH_EDIT_MODE) return 0;
    int* arr=0; int next=0,max=0; int i=0;
    arrEnsureLength(&arr, i, &max, sizeof(int));
    arr[i]=o->totf; i++;
    for(int f=0;f<o->totf;f++){
        arrEnsureLength(&arr, i+o->f[f].looplen+6, &max, sizeof(int));
        arr[i]=o->f[f].looplen; i++;
        arr[i]=o->f[f].flags; i++;
        arr[i]=o->f[f].mat; i++;
        float* nm=(void*)&arr[i]; tnsVectorSet3v(nm,o->f[f].n); i+=3;
        for(int l=0;l<o->f[f].looplen;l++){
            arr[i]=o->f[f].loop[l]; i++;
        }
    }
    *r_size = i*sizeof(int);
    *r_is_copy = 1;
    return arr;
}
void tnsset_MeshObjectFaceRaw(tnsMeshObject* o, int* data, int s){
    if(o->f){
        for(int fi=0;fi<o->maxf;fi++){ // ??? due to undo property order, maxf can be != totf, will it have problems?
            if(o->f[fi].loop){ free(o->f[fi].loop); o->f[fi].loop=0; }
        }
        arrFree(&o->f, &o->maxf); o->totf=0; o->maxf=0;
    }
    if(!data) return;
    int totf=data[0]; int i=1;
    o->f=calloc(1,sizeof(tnsFace)*totf); o->maxf=o->totf=totf;
    for(int fi=0;fi<totf;fi++){
        tnsFace*f=&o->f[fi];
        f->looplen=data[i]; i++;
        f->flags=data[i]; i++;
        f->mat=data[i]; i++;
        float* nm=(void*)&data[i]; tnsVectorSet3v(f->n,nm); i+=3;
        f->loop=calloc(1,sizeof(int)*f->looplen);
        for(int li=0;li<f->looplen;li++){
            f->loop[li]=data[i]; i++;
        }
    }
}

void tns_InvalidateMeshWithMaterial(tnsMaterial* m){
    for(tnsMeshObject* o=T->World->AllObjects.pFirst;o;o=o->Base.Item.pNext){
        if(o->Base.Type!=TNS_OBJECT_MESH) continue;
        for(tnsMaterialSlot* ms=o->Materials.pFirst;ms;ms=ms->Item.pNext){
            if(ms->Material==m){ tnsInvalidateMeshBatch(o); break; }
        }
    }
}
void tnsset_MaterialColor(tnsMaterial* m, real* c){
    tnsVectorCopy4d(c,m->Color); tns_InvalidateMeshWithMaterial(m); laNotifyUsers("tns.world");
}
void tnsset_MaterialColor2(tnsMaterial* m, real* c){ tnsVectorCopy4d(c,m->Color2);  laNotifyUsers("tns.world"); }
void tnsset_MaterialGradientMode(tnsMaterial* m, int Mode){ m->GradientMode=Mode; laNotifyUsers("tns.world"); }
void tnsset_MaterialGradientCenter(tnsMaterial* m, real* c){ tnsVectorCopy2d(c,m->GradientCenter); laNotifyUsers("tns.world"); }
void tnsset_MaterialGradientSize(tnsMaterial* m, real* c){ tnsVectorCopy2d(c,m->GradientSize); laNotifyUsers("tns.world"); }
void tnsset_MaterialGradientF(tnsMaterial* m, real c){ m->GradientBoxF=c; laNotifyUsers("tns.world"); }
void tnsset_MaterialGradientR(tnsMaterial* m, real c){ m->GradientBoxR=c; laNotifyUsers("tns.world"); }
void tnsget_MaterialSlotname(tnsMaterialSlot* ms, char *result, char** here){
    if(!ms){ strcpy(result,"?"); return; }
    if(ms->Material&&ms->Material->Name&&ms->Material->Name->Ptr){
        sprintf(result,"%d: %s",ms->Index,ms->Material->Name->Ptr); return;
    }
    sprintf(result,"%d: %s",ms->Index,transLate("<Default Material>"));
}
tnsMaterialSlot* tnsget_FirstMaterialSlot(tnsObject* o, void* unused){
    if(o->Type==TNS_OBJECT_MESH) return ((tnsMeshObject*)o)->Materials.pFirst;
    if(o->Type==TNS_OBJECT_SHAPE) return ((tnsShapeObject*)o)->Materials.pFirst;
    return 0;
}
tnsMaterialSlot* tnsget_ActiveMaterialSlot(tnsObject* o){
    if(o->Type==TNS_OBJECT_MESH) return ((tnsMeshObject*)o)->CurrentMaterial;
    if(o->Type==TNS_OBJECT_SHAPE) return ((tnsShapeObject*)o)->CurrentMaterial;
    return 0;
}
void tnsset_ActiveMaterialSlot(tnsObject* o, tnsMaterialSlot* ms){
    if(o->Type==TNS_OBJECT_MESH){ memAssignRef(o,&((tnsMeshObject*)o)->CurrentMaterial,ms); return; }
    if(o->Type==TNS_OBJECT_SHAPE){ memAssignRef(o,&((tnsShapeObject*)o)->CurrentMaterial,ms); return; }
}
void tnsset_ShapeObjectBackdrop(tnsShapeObject* o, int v){
    o->Backdrop=v; tnsInvalidateEvaluation(o); laNotifyUsers("tns.world");
}
tnsMaterial* tnsget_FirstMaterial(void* unused1, void* unused2){
    return T->World->Materials.pFirst;
}
tnsMaterial* tnsgetactive_SlotMaterial(tnsMaterialSlot* ms, void* unused){
    return ms->Material;
}
tnsCamera* tnsget_CameraInRoot(tnsRootObject* ro, void* unused){
    for(tnsObject* o=T->World->AllObjects.pFirst;o;o=o->Item.pNext){ if(o->InRoot!=ro) continue;
        if(o->Type==TNS_OBJECT_CAMERA) return o;
    }
    return 0;
}
tnsCamera* tnsgetnext_CameraInRoot(tnsCamera* c, void* unused){
    for(tnsObject* o=c->Base.Item.pNext;o;o=o->Item.pNext){ if(o->InRoot!=c->Base.InRoot) continue;
        if(o->Type==TNS_OBJECT_CAMERA) return o;
    }
    return 0;
}

laPropContainer* tnsget_ObjectType(tnsObject* o){
    switch(o->Type){
    case TNS_OBJECT_INSTANCER: return TNS_PC_OBJECT_INSTANCER;
    case TNS_OBJECT_CAMERA:    return TNS_PC_OBJECT_CAMERA;
    case TNS_OBJECT_MESH:      return TNS_PC_OBJECT_MESH;
    case TNS_OBJECT_LIGHT:     return TNS_PC_OBJECT_LIGHT;
    case TNS_OBJECT_ROOT:      return TNS_PC_OBJECT_ROOT;
    case TNS_OBJECT_SHAPE:     return TNS_PC_OBJECT_SHAPE;
    default: return TNS_PC_OBJECT_GENERIC;
    }
}

tnsObject* tnsget_ObjectRetarget(tnsObject* o, void* unused){
    if(o && o->PlayDuplicate) return o->PlayDuplicate;
    return o;
}

void laget_AnimationActionHolderCategory(void* a_unused, laActionHolder* ah, char* copy, char** ptr){
    if(ah->CategoryTitle&&ah->CategoryTitle->Ptr) *ptr=ah->CategoryTitle->Ptr;
}
int laget_AnimationActionCurrentFrame(laAction* aa){
    return LA_ACTION_FRAME(aa,-1);
}
void laget_AnimationPropStr(laActionProp* ap, char* str, char** here){
    sprintf(str,"%s.%s",ap->Prop->Container->Identifier,ap->Prop->Identifier);
}
void laread_AnimationPropStr(laActionProp* ap, char* str){
    if(!str || !str[0]) return;
    laStringSplitor* ss=strSplitPath(str,0); if(ss->NumberParts!=2) goto anim_set_prop_str_cleanup;
    laStringPart* sp1=ss->parts.pFirst,*sp2=ss->parts.pLast;
    laPropContainer* pc=la_ContainerLookup(sp1->Content); if(!pc) goto anim_set_prop_str_cleanup;
    laProp* p=la_PropLookup(&pc->Props,sp2->Content); if(!p) goto anim_set_prop_str_cleanup;
    ap->Prop=p;
anim_set_prop_str_cleanup:
    strDestroyStringSplitor(&ss);
}
void* lagetraw_ActionKeyData(laActionKey* ak, int* r_size, int* ret_is_copy){
    *r_size=ak->DataSize; *ret_is_copy=1; if(!ak->DataSize){ return 0; }
    void* data=malloc(ak->DataSize); memcpy(data,ak->Data,ak->DataSize);
    return data;
}
void lasetraw_ActionKeyData(laActionKey* ak, void* data, int DataSize){
    ak->DataSize=DataSize; ak->Data=memAcquireSimple(DataSize); memcpy(ak->Data,data,DataSize);
}
void laset_AnimationPlayHead(void* unused,real data){
    if(data<0){ data=0; }
    laAnimationSetPlayHead(data); la_AnimationEvaluateActions(1); laNotifyUsers("la.animation");
}
void laget_ActionContainer(laAction* aa, char* str, char** here){ *here=aa->HolderContainer->Identifier; }
void laread_ActionContainer(laAction* aa, char* str){ aa->HolderContainer=la_ContainerLookup(str); }

int laaction_VerifyRootObject(void* Parent, laPropContainer* ParentType, void* Child, laPropContainer* ChildType){
    if(ParentType!=TNS_PC_OBJECT_ROOT) return 0;
    if (ChildType==TNS_PC_OBJECT_GENERIC||ChildType==TNS_PC_OBJECT_CAMERA||ChildType==TNS_OBJECT_SHAPE||
        ChildType==TNS_PC_OBJECT_INSTANCER||ChildType==TNS_PC_OBJECT_LIGHT||ChildType==TNS_OBJECT_MESH){
        tnsObject*o=Child; if(o==Parent || o->InRoot==Parent) return 1;
    }
    if (ChildType==TNS_PC_MATERIAL){ return 1; }
    return 0;
}

void lareset_Main(void* Unused){
    return;
}

laPropContainer* LA_PC_SOCKET_IN;
laPropContainer* LA_PC_SOCKET_OUT;
laPropContainer* LA_PROP_SOCKET_SOURCE;
laPropContainer* LA_PROP_SOCKET_OUT;
laPropContainer* LA_PC_MAPPER;
laPropContainer* LA_PC_RACK_PAGE;
laPropContainer* TNS_PC_OBJECT_GENERIC;
laPropContainer* TNS_PC_OBJECT_INSTANCER;
laPropContainer* TNS_PC_OBJECT_CAMERA;
laPropContainer* TNS_PC_OBJECT_LIGHT;
laPropContainer* TNS_PC_OBJECT_MESH;
laPropContainer* TNS_PC_OBJECT_SHAPE;
laPropContainer* TNS_PC_OBJECT_ROOT;
laPropContainer* TNS_PC_MATERIAL;
laPropContainer* TNS_PC_MATERIAL_SLOT;

laProp* LA_PROP_CONTROLLER;

void la_RegisterGeneralProps(){
    laPropContainer *p, *ip, *p2, *ip2, *sp1, *sp2;
    laProp *ep;
    laPropPack pp;
    laSubProp *sp;
    laKeyMapper *km;

    p = la_SetGeneralRoot(&MAIN.GeneralIntSub, "__general_int__", "Genral Int Operations", "Genral Int Operations");
    laAddOperatorProperty(p, "restore", "Restore Default", "Restore the property to the original value", "LA_prop_restore_default", U'⭯', 0);
    laAddOperatorProperty(p, "set_max", "Set Max", "Set property to its maximum value", "LA_prop_set_max", 0,0);
    laAddOperatorProperty(p, "set_min", "Set Min", "Set property to its minimum value", "LA_prop_set_min", 0,0);
    //laAddOperatorProperty(p, "hyper_data", "View Hyper Data", "Show Properties Of Specific Data Block", "LA_view_hyper_data", U'🛈', 0);
    laAddOperatorProperty(p, "insert_key", "Insert Key Frame", "Insert key frame in the active action", "LA_prop_insert_key", 0,0);
    
    p = la_SetGeneralRoot(&MAIN.GeneralIntArraySub, "__general_int_arr__", "Genral Int Array Operations", "Genral Int Array Operations");
    laAddOperatorProperty(p, "restore", "Restore Default", "Restore the property to the original value", "LA_prop_restore_default", U'⭯', 0);
    laAddOperatorProperty(p, "set_max", "Set Max", "Set property to its maximum value", "LA_prop_set_max", 0,0);
    laAddOperatorProperty(p, "set_min", "Set Min", "Set property to its minimum value", "LA_prop_set_min", 0,0);
    //laAddOperatorProperty(p, "hyper_data", "View Hyper Data", "Show Properties Of Specific Data Block", "LA_view_hyper_data", U'🛈', 0);
    laAddOperatorProperty(p, "insert_key", "Insert Key Frame", "Insert key frame in the active action", "LA_prop_insert_key", 0,0);
    
    p = la_SetGeneralRoot(&MAIN.GeneralFloatSub, "__general_real__", "Genral Float Operations", "Genral Float Operations");
    laAddOperatorProperty(p, "restore", "Restore Default", "Restore the property to the original value", "LA_prop_restore_default", U'⭯', 0);
    laAddOperatorProperty(p, "set_max", "Set Max", "Set property to its maximum value", "LA_prop_set_max", 0,0);
    laAddOperatorProperty(p, "set_min", "Set Min", "Set property to its minimum value", "LA_prop_set_min", 0,0);
    //laAddOperatorProperty(p, "hyper_data", "View Hyper Data", "Show Properties Of Specific Data Block", "LA_view_hyper_data", U'🛈', 0);
    laAddOperatorProperty(p, "insert_key", "Insert Key Frame", "Insert key frame in the active action", "LA_prop_insert_key", 0,0);
    
    p = la_SetGeneralRoot(&MAIN.GeneralFloatArraySub, "__general_real_arr__", "Genral Float Array Operations", "Genral Float Array Operations");
    laAddOperatorProperty(p, "restore", "Restore Default", "Restore the property to the original value", "LA_prop_restore_default", U'⭯', 0);
    laAddOperatorProperty(p, "set_max", "Set Max", "Set property to its maximum value", "LA_prop_set_max", 0,0);
    laAddOperatorProperty(p, "set_min", "Set Min", "Set property to its minimum value", "LA_prop_set_min", 0,0);
    //laAddOperatorProperty(p, "hyper_data", "View Hyper Data", "Show Properties Of Specific Data Block", "LA_view_hyper_data", U'🛈', 0);
    laAddOperatorProperty(p, "insert_key", "Insert Key Frame", "Insert key frame in the active action", "LA_prop_insert_key", 0,0);
    
    p = la_SetGeneralRoot(&MAIN.GeneralEnumSub, "__general_enum__", "Genral Enum Operations", "Genral Enum Operations");
    laAddOperatorProperty(p, "restore", "Restore Default", "Restore the property to the original value", "LA_prop_restore_default", U'⭯', 0);
    laAddOperatorProperty(p, "insert_key", "Insert Key Frame", "Insert key frame in the active action", "LA_prop_insert_key", 0,0);
    
    p = la_SetGeneralRoot(&MAIN.GeneralEnumArraySub, "__general_enum_arr__", "Genral Enum Array Operations", "Genral Enum Array Operations");
    laAddOperatorProperty(p, "restore", "Restore Default", "Restore the property to the original value", "LA_prop_restore_default", U'⭯', 0);
    laAddOperatorProperty(p, "insert_key", "Insert Key Frame", "Insert key frame in the active action", "LA_prop_insert_key", 0,0);
    
    p = la_SetGeneralRoot(&MAIN.GeneralStringSub, "__general_string__", "Genral String Operations", "Genral String Operations");
    laAddOperatorProperty(p, "copy", "Copy", "Copy to clipboard", "LA_string_copy", 0,0);
    laAddOperatorProperty(p, "paste", "Paste", "Paste from clipboard", "LA_system_paste", 0,0);
    laAddOperatorProperty(p, "restore", "Restore Default", "Restore default value", "LA_string_set_default", U'⭯', 0);
    laAddOperatorProperty(p, "get_folder_path", "Get Folder Path", "Get a folder path", "LA_string_get_folder_path", U'📁', 0);
    laAddOperatorProperty(p, "get_file_path", "Get File Path", "Get a file path", "LA_string_get_file_path", U'🖹', 0);
    p->MenuUiDefine=laui_StringPropUiDefine;

    p = la_SetGeneralRoot(&MAIN.GeneralOperatorSub, "__general_operator__", "Genral Operator Operations", "Genral Operator Operations");

    p = la_SetGeneralRoot(&MAIN.GeneralCollectionSub, "__general_collection__", "Genral Collection Operations", "Genral Collection Operations");
    laAddOperatorProperty(p, "put_data_block", "Put", "Append Pending Data Block Here", "LA_sub_put_data_block", U'🔗', 0);
    laAddOperatorProperty(p, "save_instance", "Save Instance", "Save instance as a UDF block", "LA_udf_save_instance", 0,0);
    laAddOperatorProperty(p, "clear_selection", "Clear Selection", "Clear selected instance", "LA_collection_clear_selection", U'🧹',0);
}

void lareset_RackPage(laRackPage* rp){
    laNodeRack* r; while(r=rp->Racks.pFirst){
        laBaseNode* bn; while(bn=r->Nodes.pFirst){
            bn->Type->Destroy(bn); lstRemoveItem(&r->Nodes,bn);
        }
        lstRemoveItem(&r->ParentPage->Racks, r); memLeave(r);
    }
}

laRackPage* DEBUG_READ_RACK_PAGE=0; // some files have rack parent page missing, this to fix.
void lapost_RackPage(laRackPage* rp){ DEBUG_READ_RACK_PAGE=rp; }

void la_RegisterTNSProps(){
    laPropContainer *p, *ip, *p2, *ip2, *sp1, *sp2;
    laProp *ep; laPropPack pp; laSubProp *sp; laKeyMapper *km;
    laCanvasTemplate *v2dt;
    
    p = laAddPropertyContainer("tns_main", "TNS Main", "Render kernel root structure", 0,0,sizeof(tnsMain), 0,0,1);{
        if(MAIN.InitArgs.HasWorldObjects){
            laAddSubGroup(p, "world", "World", "World Descriptor", "tns_world",0,0,0,offsetof(tnsMain, World), 0,0,0,0,0,0,0,LA_UDF_SINGLE);
        }
        sp = laAddSubGroup(p, "texture_list", "Texture List", "List Of All Textures Under TNS Management", "tns_texture",0,0,0,-1, 0,tnsget_PreviewTexture, 0,0,0,tnsset_PreviewTexture, offsetof(tnsMain, Textures), LA_UDF_IGNORE);
        laSubGroupDetachable(sp, tnsget_detached_FirstTexture, laget_ListNext);
        //laAddSubGroup(p, "Render Buffers", "Storing All Render Buffers In Current Program Instance", "tns_render_buffer",0,0,0,offsetof(tnsMain, ActiveRenderBuffer), 0,0,0,0,0,0,0,0,offsetof(tnsMain, RenderBuffers), 0);
    }

    p = laAddPropertyContainer("tns_texture", "TNS Texture Item", "A texture descriptor with gl handle", 0,0,sizeof(tnsTexture), 0,0,0);{
        laAddIntProperty(p, "gl_handle", "OpenGL Handle", "Opengl handle of this texture", LA_WIDGET_INT_PLAIN, 0,0,0,0,0,0,0,offsetof(tnsTexture, GLTexHandle), 0,0,0,0,0,0,0,0,0,0,LA_AS_IDENTIFIER|LA_READ_ONLY);
        laAddIntProperty(p, "size", "Size", "Width And Height", 0,"Width,Height", "px", 0,0,0,0,0,offsetof(tnsTexture, Width), 0,0,2, 0,0,0,0,0,0,0,LA_READ_ONLY);
        ep = laAddEnumProperty(p, "internal_type", "Internal Type", "Internal bits type", LA_WIDGET_ENUM_CYCLE, 0,0,0,0,offsetof(tnsTexture, GLTexBitsType), 0,0,0,0,0,0,0,0,0,LA_READ_ONLY);{
            laAddEnumItemAs(ep, "rgba", "GL_RGBA", "GL_RGBA", GL_RGBA8,0);
            laAddEnumItemAs(ep, "rgb", "GL_RGB", "GL_RGB", GL_RGB8,0);
            laAddEnumItemAs(ep, "rg", "GL_RG", "GL_RG", GL_RG8,0);
            laAddEnumItemAs(ep, "r", "GL_RED", "GL_RED", GL_R8,0);
            laAddEnumItemAs(ep, "rgba32f", "GL_RGBA32F", "GL_RGBA32F", GL_RGBA32F,0);
            laAddEnumItemAs(ep, "rgb32f", "GL_RGB32F", "GL_RGB32F", GL_RGB32F,0);
            laAddEnumItemAs(ep, "rg32f", "GL_RG32F", "GL_RG32F", GL_RG32F,0);
            laAddEnumItemAs(ep, "r32f", "GL_R32F", "GL_R32F", GL_R32F,0);
            laAddEnumItemAs(ep, "rgba32i", "GL_RGBA32I", "GL_R32I", GL_RGBA32I,0);
            laAddEnumItemAs(ep, "rgb32i", "GL_RGB32I", "GL_R32I", GL_RGB32I,0);
            laAddEnumItemAs(ep, "rg32i", "GL_RG32I", "GL_R32I", GL_RG32I,0);
            laAddEnumItemAs(ep, "r32i", "GL_R32I", "GL_R32I", GL_R32I,0);
            laAddEnumItemAs(ep, "depth", "GL_DEPTH_COMPONENT32F", "GL_DEPTH_COMPONENT32F", GL_DEPTH_COMPONENT32F,0);
        }
        p->Template2D = la_GetCanvasTemplate(0,"la_CanvasDrawTexture");
    }

    if(!MAIN.InitArgs.HasWorldObjects) return;

    p = laAddPropertyContainer("tns_world", "World", "3d world structure", 0,0,sizeof(tnsWorld),tnspost_World,0,1);{
        sp = laAddSubGroup(p, "root_objects", "Root Objects", "List of all root objects", "tns_object",tnsget_ObjectType,0,0,-1,0,0,0,0,0,0,offsetof(tnsWorld, RootObjects), 0);
        laSubGroupDetachable(sp, tnsget_detached_FirstRootObject, laget_ListNext);
        sp = laAddSubGroup(p, "root_objects_as_root", "Root Objects", "List of all root objects (in type tns_root_object)", "tns_root_object",0,0,0,-1,0,0,0,0,0,0,offsetof(tnsWorld, RootObjects), LA_UDF_IGNORE);
        laSubGroupDetachable(sp, tnsget_detached_FirstRootObject, laget_ListNext);
        laAddSubGroup(p, "active_root", "Active Root Object", "Global active root object", "tns_object",0,0,0,offsetof(tnsWorld,ActiveRoot),tnsget_detached_FirstRootObject,0,laget_ListNext,0,0,0,0,LA_UDF_REFER);
        sp = laAddSubGroup(p, "objects", "Objects", "List of all objects", "tns_object",tnsget_ObjectType, 0,0,-1,0,0,0,0,0,0,offsetof(tnsWorld, AllObjects), 0);
        laSubGroupExtraFunctions(sp,tnsfilter_SavableObject,tnsfilter_SavableObject,0,0,0);
        sp = laAddSubGroup(p, "materials", "Materials", "List of all materials", "tns_material",0,0,0,-1,0,0,0,0,0,0,offsetof(tnsWorld, Materials), 0);
        laSubGroupDetachable(sp, tnsget_detached_FirstMaterial, laget_ListNext);
        ep = laAddEnumProperty(p, "property_page", "Property Page", "Show which page in the properties panel", 0,0,0,1,0,offsetof(tnsWorld, PropertyPage), 0,0,0,0,0,0,0,0,0,LA_UDF_IGNORE);{
            laAddEnumItemAs(ep, "ROOT", "Root", "Display root object properties",0,0);
            laAddEnumItemAs(ep, "OBJECT", "Object", "Display object properties",1,0);
        }
    }

    p = laAddPropertyContainer("tns_child_object", "Child Object", "Child object linker", 0,0,sizeof(laListItemPointer), 0,0,0);{
        laAddSubGroup(p, "object", "Object", "Object link", "tns_object",tnsget_ObjectType, 0,0,offsetof(laListItemPointer, p), 0,0,0,0,0,0,0,LA_UDF_REFER);
    }

    p = laAddPropertyContainer("tns_material", "Material" "Object material", 0,0,0,sizeof(tnsMaterial),tnspost_Material,0,2);{
        TNS_PC_MATERIAL=p;
        laAddStringProperty(p, "name", "Material Name", "The name ff the material", 0,0,0,0,1, offsetof(tnsMaterial, Name), 0,0,0,0,LA_AS_IDENTIFIER);
        laAddFloatProperty(p, "color", "Color", "Base color of the material", LA_WIDGET_FLOAT_COLOR, "R,G,B,A", 0,1,0,0.025, 1, 0,offsetof(tnsMaterial, Color), 0,0,4, 0,0,0,0,tnsset_MaterialColor,0,0,LA_PROP_KEYABLE);
        laAddFloatProperty(p, "color2", "Color 2", "Gradient end color of the material", LA_WIDGET_FLOAT_COLOR, "R,G,B,A", 0,1,0,0.025, 1, 0,offsetof(tnsMaterial, Color2), 0,0,4, 0,0,0,0,tnsset_MaterialColor2,0,0,LA_PROP_KEYABLE);
        ep = laAddEnumProperty(p, "colorful", "Colorful", "Use colorful display", 0,0,0,0,0,offsetof(tnsMaterial, Colorful), 0,0,0,0,0,0,0,0,0,0);{
            laAddEnumItemAs(ep, "NONE", "None", "Display materials normally",0,0);
            laAddEnumItemAs(ep, "COLORFUL", "Colorful", "Display material with colorful halftone",1,0);
        }
        ep = laAddEnumProperty(p, "as_library", "As Library", "As commom library", 0,0,0,0,0,offsetof(tnsMaterial, AsLibrary), 0,0,0,0,0,0,0,0,0,0);{
            laAddEnumItemAs(ep, "NONE", "None", "Use as normal material",0,0);
            laAddEnumItemAs(ep, "LIBRARY", "LIBRARY", "As commom library",1,0);
        }
        ep=laAddEnumProperty(p,"gradient_mode","Gradient Mode","2d gradient mode of shapes",0,0,0,0,0,offsetof(tnsMaterial,GradientMode),0,tnsset_MaterialGradientMode,0,0,0,0,0,0,0,0);
        laAddEnumItemAs(ep,"NONE","None","Don't do any gradient",0,0);
        laAddEnumItemAs(ep,"LINEAR","Linear","Do linear gradient",TNS_GRADIENT_MODE_LINEAR,0);
        laAddEnumItemAs(ep,"BOX","Box","Do box gradient",TNS_GRADIENT_MODE_BOX,0);
        laAddEnumItemAs(ep,"RADIAL","Radial","Do radial gradient",TNS_GRADIENT_MODE_RADIAL,0);
        laAddFloatProperty(p,"gradient_center","Gradient Center","Starting point of the gradient",0,0,0,0,0,0,0,0,offsetof(tnsMaterial,GradientCenter),0,0,2,0,0,0,0,tnsset_MaterialGradientCenter,0,0,LA_PROP_KEYABLE);
        laAddFloatProperty(p,"gradient_size","Gradient Size","Size of the gradient",0,0,0,0,0,0,0,0,offsetof(tnsMaterial,GradientSize),0,0,2,0,0,0,0,tnsset_MaterialGradientSize,0,0,LA_PROP_KEYABLE);
        laAddFloatProperty(p,"gradient_box_r","Corner","Corner radius of the box gradient",0,0,0,0,0,0,0,0,offsetof(tnsMaterial,GradientBoxR),0,tnsset_MaterialGradientR,0,0,0,0,0,0,0,0,LA_PROP_KEYABLE);
        laAddFloatProperty(p,"gradient_box_f","Feather","Feather distance of the box gradient",0,0,0,0,0,0,0,0,offsetof(tnsMaterial,GradientBoxF),0,tnsset_MaterialGradientF,0,0,0,0,0,0,0,0,LA_PROP_KEYABLE);
        laAddSubGroup(p, "shader_page", "Shader Page", "Shader page of this material","la_rack_page",0,0,0,offsetof(tnsMaterial,Page),0,0,0,0,0,0,0,LA_UDF_SINGLE|LA_HIDE_IN_SAVE);
        laAddOperatorProperty(p,"refresh","Refresh","Refresh material shader","M_refresh_material_shader",L'🗘',0);
        laAddOperatorProperty(p,"remove","Remove","Remove this material","M_remove_material",L'🗴',0);
    }

    p = laAddPropertyContainer("tns_material_slot", "Material Slot" "Material slot", 0,0,0,sizeof(tnsMaterialSlot),0,0,1);{
        TNS_PC_MATERIAL_SLOT=p;
        laAddStringProperty(p,"name","Name","Name of the material slot",LA_WIDGET_STRING_PLAIN,0,0,0,0,0,0,tnsget_MaterialSlotname,0,0,LA_READ_ONLY|LA_UDF_IGNORE);
        laAddSubGroup(p, "material", "Material", "Material reference of this slot","tns_material",0,0,0,offsetof(tnsMaterialSlot,Material),tnsget_FirstMaterial,tnsgetactive_SlotMaterial,laget_ListNext,0,0,0,0,LA_UDF_REFER);
        laAddSubGroup(p, "parent", "Parent", "Parent object","tns_object",0,0,0,offsetof(tnsMaterialSlot,Parent),0,0,0,0,0,0,0,LA_UDF_REFER|LA_READ_ONLY);
        laAddIntProperty(p,"index","Index","Index of the material slot",LA_WIDGET_INT_PLAIN,0,0,0,0,0,0,0,offsetof(tnsMaterialSlot,Index),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddOperatorProperty(p,"remove","Remove","Remove this material slot","M_remove_material_slot",L'🗴',0);
    }

    p = laAddPropertyContainer("tns_object", "Object", "3d object item", 0,tnsui_BaseObjectProperties,sizeof(tnsObject), tnspost_Object, 0,2);{
        TNS_PC_OBJECT_GENERIC=p;
        laAddSubGroup(p, "base", "Base", "Object base", "tns_object",0,0,0,-1,laget_Self,0,0,0,0,0,0,LA_UDF_REFER|LA_UDF_IGNORE);
        laAddSubGroup(p, "__action_retarget__", "Action Retarget", "Get action retargeted instance (play duplicate)", "tns_object",0,0,0,-1,tnsget_ObjectRetarget,0,0,0,0,0,0,LA_UDF_IGNORE);
        laAddStringProperty(p, "name", "Object Name", "The name of the object", 0,0,0,0,1, offsetof(tnsObject, Name), 0,0,0,0,LA_AS_IDENTIFIER);
        laAddIntProperty(p,"flags","Flags","Flags",0,0,0,0,0,0,0,0,offsetof(tnsObject,Flags),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        ep = laAddEnumProperty(p, "show", "Show", "Show object in the viewport", 0,0,0,0,0,offsetof(tnsObject, Show), 0,0,0,0,0,0,0,0,0,0);{
            laAddEnumItem(ep, "false", "False", "False", U'🌔');
            laAddEnumItem(ep, "true", "IsTrue", "IsTrue", U'🌑');
        }
        ep = laAddEnumProperty(p, "show_on_render", "Show On Render", "Show on render", 0,0,0,0,0,offsetof(tnsObject, ShowOnRender), 0,0,0,0,0,0,0,0,0,0);{
            laAddEnumItem(ep, "false", "False", "False", U'🚫');
            laAddEnumItem(ep, "true", "IsTrue", "IsTrue", U'📷');
        }
        ep = laAddEnumProperty(p, "type", "Type", "Object type like mesh,camera and lamp", 0,0,0,0,0,offsetof(tnsObject, Type), 0,0,0,0,0,0,0,0,0,LA_READ_ONLY);{
            laAddEnumItemAs(ep, "ROOT", "Root", "Root object", TNS_OBJECT_ROOT, 0);
            laAddEnumItemAs(ep, "INSTANCER", "Instancer", "Instancer object", TNS_OBJECT_INSTANCER, 0);
            laAddEnumItemAs(ep, "CAMERA", "Camera", "Camera object, to render a scene", TNS_OBJECT_CAMERA, U'📷');
            laAddEnumItemAs(ep, "LIGHT", "Lamp", "Lamp object, to illuminate the scene", TNS_OBJECT_LIGHT, 0);
            laAddEnumItemAs(ep, "MESH", "Mesh", "Mesh object, made of verts/edges/faces", TNS_OBJECT_MESH, 0);
            laAddEnumItemAs(ep, "SHAPE", "Shape", "Shape object, 2d boundary", TNS_OBJECT_SHAPE, 0);
        }
        laAddFloatProperty(p, "location", "Location", "XYZ Location In Local Coordinates", 0,"X,Y,Z", 0,0,0,0.1, 0,0,offsetof(tnsObject, Location), 0,0,3, 0,tnsset_ObjectLocation, 0,0,0,0,0,0);
        laAddFloatProperty(p, "rotation", "Rotation", "Rotation In Local Coordinates", 0,"X,Y,Z", 0,0,0,0,0,0,offsetof(tnsObject, Rotation), 0,0,3, 0,tnsset_ObjectRotation, 0,0,0,0,0,0);
        laAddFloatProperty(p, "scale", "Scale", "Local Scale", 0,"X,Y,Z",0,0,0,0,1,0,offsetof(tnsObject, Scale), 0,0,3,0,tnsset_ObjectScale,0,0,0,0,0,0);
        laAddFloatProperty(p, "glocation", "Global Location", "Location in global coordinates", 0,"X,Y,Z", 0,0,0,0.1, 0,0,offsetof(tnsObject, GLocation), 0,0,3, 0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddFloatProperty(p, "grotation", "Global Rotation", "Rotation in global coordinates", 0,"X,Y,Z", 0,0,0,0,0,0,offsetof(tnsObject, GRotation), 0,0,3, 0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddFloatProperty(p, "gscale", "Global Scale", "Global scale", 0,"X,Y,Z",0,0,0,0,1,0,offsetof(tnsObject, GScale), 0,0,3,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddFloatProperty(p, "dlocation", "Delta Location", "XYZ delta transforms", 0,"X,Y,Z", 0,0,0,0.1, 0,0,offsetof(tnsObject, DLocation), 0,0,3,0,0,0,0,tnsset_ObjectDLocationARR,0,0,LA_PROP_KEYABLE);
        laAddFloatProperty(p, "drotation", "Delta Rotation", "Delta rotation", 0,"X,Y,Z", 0,0,0,0,0,0,offsetof(tnsObject, DRotation), 0,0,3,0,0,0,0,tnsset_ObjectDRotationARR,0,0,LA_PROP_KEYABLE|LA_PROP_ROTATION);
        laAddFloatProperty(p, "dscale", "Delta Scale", "Delta scale", 0,"X,Y,Z",0,0,0,0,1,0,offsetof(tnsObject, DScale), 0,0,3,0,0,0,0,tnsset_ObjectDScaleARR,0,0,LA_PROP_KEYABLE);
        laAddFloatProperty(p, "global_mat", "Global Matrix", "Global transformation matrix", 0,0,0,0,0,0,0,0,offsetof(tnsObject, GlobalTransform), 0,0,16, 0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddFloatProperty(p, "local_mat", "Local Matrix", "Local transformation matrix", 0,0,0,0,0,0,0,0,offsetof(tnsObject, SelfTransform), 0,0,16, 0,0,0,0,0,0,0,LA_READ_ONLY);
        ep = laAddEnumProperty(p, "rotation_mode", "Rotation Mode", "Rotation mode of this object(e.g. xyz/xzy/quaternion...)", 0,0,0,0,0,offsetof(tnsObject, RotationMode), 0,0,0,0,0,0,0,0,0,0);{
            laAddEnumItem(ep, "xyz", "XYZ", "XYZ Euler Mode", 0); laAddEnumItem(ep, "xzy", "XZY", "XZY Euler Mode", 0);
            laAddEnumItem(ep, "yxz", "YXZ", "YXZ Euler Mode", 0); laAddEnumItem(ep, "yzx", "YZX", "YZX Euler Mode", 0);
            laAddEnumItem(ep, "zxy", "ZXY", "ZXY Euler Mode", 0); laAddEnumItem(ep, "zyx", "ZYX", "ZYX Euler Mode", 0);
            laAddEnumItem(ep, "quaternion", "Quaternion", "Quaternion Mode", 0);
        }
        laAddSubGroup(p, "active", "Active", "Active reference", "tns_object",tnsget_ObjectType,0,0,offsetof(tnsObject, Active), 0,0,0,0,0,0,0,LA_UDF_REFER|LA_READ_ONLY);
        laAddSubGroup(p, "in_root", "In Root", "Root object of this object", "tns_object",0,0,0,offsetof(tnsObject, InRoot), 0,0,0,0,0,0,0,LA_UDF_REFER);
        laAddSubGroup(p, "parent", "Parent", "Object parent", "tns_object",0,0,0,offsetof(tnsObject, ParentObject), 0,0,0,0,0,0,0,LA_UDF_REFER);
        laAddSubGroup(p, "children", "Children", "The Children Of This Object", "tns_child_object",0,0,0,-1, 0,0,0,0,0,0,offsetof(tnsObject, ChildObjects), 0);
        laAddSubGroup(p, "drivers", "Drivers", "Driver page collection","la_driver_collection",0,0,0,offsetof(tnsObject,Drivers),0,0,0,0,0,0,0,LA_UDF_SINGLE|LA_HIDE_IN_SAVE);
        laAddOperatorProperty(p, "add_driver_page", "Add Page", "Add a driver page","LA_add_driver_page",'+',0);
        laAddSubGroup(p, "as_mesh", "As Mesh", "As mesh object", "tns_mesh_object",0,0,0,-1,0,tnsget_ObjectAsMesh,0,0,0,0,0,LA_UDF_REFER|LA_READ_ONLY|LA_UDF_IGNORE);
        laAddSubGroup(p, "as_root_object", "As Root Object", "As root object", "tns_root_object",0,0,0,-1,0,tnsget_ObjectAsRoot,0,0,0,0,0,LA_UDF_REFER|LA_READ_ONLY|LA_UDF_IGNORE);
    }
    p = laAddPropertyContainer("tns_root_object", "Root Object", "Root object", 0, tnsui_RootObjectProperties,sizeof(tnsRootObject), 0,0,2);{
        laPropContainerExtraFunctions(p,0,0,tnstouched_Object,tnspropagate_Object,tnsui_RootObjectMenuUi);
        laContainerAnimationFunctions(p,laaction_VerifyRootObject); 
        TNS_PC_OBJECT_ROOT=p;
        laAddStringProperty(p, "name", "Object Name", "The name of the object", 0,0,0,0,1, offsetof(tnsObject, Name), 0,0,0,0,LA_AS_IDENTIFIER);
        laAddSubGroup(p, "base", "Base", "Object base", "tns_object",0,0,0,0,0,0,0,0,0,0,0,LA_UDF_LOCAL);
        laAddSubGroup(p, "__actions__", "Actions", "Animation actions", "la_animation_action",0,0,0,-1,0,laget_CurrentAnimationAction,0,laset_CurrentAnimationAction,0,0,offsetof(tnsRootObject, Actions), 0);
        laAddSubGroup(p, "__action_props__", "Action Props", "Action properties", "la_animation_prop",0,0,0,-1,0,0,0,0,0,0,offsetof(tnsRootObject, ActionProps), 0);
        laAddSubGroup(p, "active_camera", "Active Camera", "Active camera of this root object", "tns_object",0,0,0,offsetof(tnsRootObject, ActiveCamera),tnsget_CameraInRoot,0,tnsgetnext_CameraInRoot,0,0,0,0,LA_UDF_REFER);
        ep = laAddEnumProperty(p, "is_2d", "Is 2D", "Is 2d root object", 0,0,0,0,0,offsetof(tnsRootObject, Is2D), 0,tnsset_RootObjectIs2D,0,0,0,0,0,0,0,0);{
            laAddEnumItemAs(ep, "3D", "3D", "Root object is in 3D", 0, 0);
            laAddEnumItemAs(ep, "2D", "2D", "Root object is in 2D", 1, 0);
        }
        laAddOperatorProperty(p, "remove_root", "Remove root", "Remove the root node","M_remove_root",L'🗴',0);
        //laAddSubGroup(p, "as_root_object", "As Root Object", "As root object", "tns_root_object",0,0,0,-1,0,tnsget_ObjectAsRoot,0,0,0,0,0,LA_UDF_REFER|LA_READ_ONLY|LA_UDF_IGNORE);
    }
    p = laAddPropertyContainer("tns_instancer", "Instancer", "Instance placeholder object", U'📎', tnsui_InstancerObjectProperties,sizeof(tnsInstancer), 0,0,2);{
        laPropContainerExtraFunctions(p,0,0,tnstouched_Object,0/*tnspropagate_Object*/,0);
        TNS_PC_OBJECT_INSTANCER=p;
        laAddStringProperty(p, "name", "Object Name", "The name of the object", 0,0,0,0,1, offsetof(tnsObject, Name), 0,0,0,0,LA_AS_IDENTIFIER);
        laAddSubGroup(p, "base", "Base", "Object base", "tns_object",0,0,0,0,0,0,0,0,0,0,0,LA_UDF_LOCAL);
        laAddSubGroup(p, "instance", "Instance", "Root object to be referenced", "tns_object",0,0,0,offsetof(tnsInstancer, Instance),tnsget_detached_FirstRootObject,0,laget_ListNext,tnsset_InstancerInstance,0,0,0,LA_UDF_REFER);
        ep = laAddEnumProperty(p, "hook", "Hook", "Hook position of the 2d instance", 0,0,0,0,0,offsetof(tnsInstancer, Hook), 0,tnsset_InstancerHook,0,0,0,0,0,0,0,0);{
            laAddEnumItemAs(ep, "NONE", "None", "Doesn't hook to anything, use instancer x,y coordinates", 0, 0);
            laAddEnumItemAs(ep, "TL", "Top Left", "Hook to top left", TNS_INSTANCER_HOOK_TL, L'🡤');
            laAddEnumItemAs(ep, "TC", "Top Center", "Hook to top center", TNS_INSTANCER_HOOK_TC, L'🡡');
            laAddEnumItemAs(ep, "TR", "Top Right", "Hook to top right", TNS_INSTANCER_HOOK_TR, L'🡥');
            laAddEnumItemAs(ep, "CL", "Center Left", "Hook to center left", TNS_INSTANCER_HOOK_CL, L'🡠');
            laAddEnumItemAs(ep, "CC", "Center", "Hook to center", TNS_INSTANCER_HOOK_CC, L'🞆');
            laAddEnumItemAs(ep, "CR", "Center Right", "Hook to center right", TNS_INSTANCER_HOOK_CR, L'🡢');
            laAddEnumItemAs(ep, "BL", "Bottom Left", "Hook to bottom left", TNS_INSTANCER_HOOK_BL, L'🡧');
            laAddEnumItemAs(ep, "BC", "Bottom Center", "Hook to bottom center", TNS_INSTANCER_HOOK_BC, L'🡣');
            laAddEnumItemAs(ep, "BR", "Bottom Right", "Hook to bottom right", TNS_INSTANCER_HOOK_BR, L'🡦');
        }
        laAddFloatProperty(p, "hook_offset", "Hook Offset", "Offset of the 2d hook point", 0,0,"X,Y",0,0,0,0,0,offsetof(tnsInstancer, HookOffset),0,0,2,0,0,0,0,tnssetarr_InstancerHookOffset,0,0,LA_PROP_KEYABLE);
    }
    p = laAddPropertyContainer("tns_mesh_object", "Mesh Object", "Mesh object", 0,tnsui_MeshObjectProperties,sizeof(tnsMeshObject), tnspost_Object, 0,2);{
        laPropContainerExtraFunctions(p,0,0,tnstouched_Object,0/*tnspropagate_Object*/,0);
        TNS_PC_OBJECT_MESH=p;
        laAddStringProperty(p, "name", "Object Name", "The name of the object", 0,0,0,0,1, offsetof(tnsObject, Name), 0,0,0,0,LA_AS_IDENTIFIER);
        laAddSubGroup(p, "base", "Base", "Object base", "tns_object",0,0,0,0,0,0,0,0,0,0,0,LA_UDF_LOCAL);
        ep = laAddEnumProperty(p, "mode", "Mode", "Mesh object mode", 0,0,0,0,0,offsetof(tnsMeshObject, Mode), 0,0,0,0,0,0,0,0,0,0);{
            laAddEnumItemAs(ep, "OBJECT", "Object", "Object mode", TNS_MESH_OBJECT_MODE, 0);
            laAddEnumItemAs(ep, "EDIT", "Edit", "Edit mode", TNS_MESH_EDIT_MODE, 0);
        }
        laAddSubGroup(p, "mv", "MMesh Verts", "Vertices of editing mesh", "tns_mvert",0,0,0,-1, 0,0,0,0,0,0,offsetof(tnsMeshObject, mv), 0);
        laAddSubGroup(p, "me", "MMesh Edges", "Edges of editing mesh", "tns_medge",0,0,0,-1, 0,0,0,0,0,0,offsetof(tnsMeshObject, me), 0);
        laAddSubGroup(p, "mf", "MMesh Faces", "Faces of editing mesh", "tns_mface",0,0,0,-1, 0,0,0,0,0,0,offsetof(tnsMeshObject, mf), 0);
        laAddIntProperty(p, "totmv", "MVert Count", "Total mvert", 0,0,0,0,0,0,0,0,offsetof(tnsMeshObject, totmv),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddIntProperty(p, "totme", "MEdge Count", "Total medge", 0,0,0,0,0,0,0,0,offsetof(tnsMeshObject, totme),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddIntProperty(p, "totmf", "MFace Count", "Total mface", 0,0,0,0,0,0,0,0,offsetof(tnsMeshObject, totmf),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddIntProperty(p, "totv", "Vert Count", "Total vert", 0,0,0,0,0,0,0,0,offsetof(tnsMeshObject, totv),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddIntProperty(p, "tote", "Edge Count", "Total edge", 0,0,0,0,0,0,0,0,offsetof(tnsMeshObject, tote),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddIntProperty(p, "totf", "Face Count", "Total face", 0,0,0,0,0,0,0,0,offsetof(tnsMeshObject, totf),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddRawProperty(p, "v", "Verts", "Verts", offsetof(tnsMeshObject, v), tnsget_MeshObjectVertSize, 0,0,LA_READ_ONLY);
        laAddRawProperty(p, "e", "Edges", "Edges", offsetof(tnsMeshObject, e), tnsget_MeshObjectEdgeSize, 0,0,LA_READ_ONLY);
        laAddRawProperty(p, "f", "Faces", "Faces", offsetof(tnsMeshObject, f), 0,tnsget_MeshObjectFaceRaw, tnsset_MeshObjectFaceRaw, LA_READ_ONLY);
        //laAddIntProperty(p, "maxv", "Max Vert", "Max vert count", 0,0,0,0,0,0,0,0,offsetof(tnsMeshObject, maxv),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        //laAddIntProperty(p, "maxe", "Max Edge", "Max edge count", 0,0,0,0,0,0,0,0,offsetof(tnsMeshObject, maxe),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        //laAddIntProperty(p, "maxf", "Max Face", "Max face count", 0,0,0,0,0,0,0,0,offsetof(tnsMeshObject, maxf),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddSubGroup(p, "current_material", "Current Material", "Current material slot in this object", "tns_material_slot",0,0,0,offsetof(tnsMeshObject, CurrentMaterial),tnsget_FirstMaterialSlot,0,laget_ListNext,0,0,0,0,LA_UDF_REFER);
        laAddSubGroup(p, "materials", "Materials", "Material slots in this mesh object", "tns_material_slot",0,0,0,-1,0,tnsget_ActiveMaterialSlot,0,tnsset_ActiveMaterialSlot,0,0,offsetof(tnsMeshObject, Materials),0);
        laAddOperatorProperty(p,"add_material_slot","Add Material Slot","Add a material slot into this object","M_new_material_slot",L'+',0);
        laAddOperatorProperty(p,"add_material","Add Material","Add a new material to this material slot","M_new_material",'+',0);
        laAddOperatorProperty(p,"assign_material_slot","Assign Material Slot","Assign faces to a current slot","M_assign_material_slot",L'🖌',0);
    }
    p = laAddPropertyContainer("tns_camera", "Camera", "Camera object", U'📷', tnsui_CameraObjectProperties,sizeof(tnsCamera), 0,0,2);{
        //laPropContainerExtraFunctions(p,0,0,0,tnspropagate_Object,0);
        TNS_PC_OBJECT_CAMERA=p;
        laAddStringProperty(p, "name", "Object Name", "The name of the object", 0,0,0,0,1, offsetof(tnsObject, Name), 0,0,0,0,LA_AS_IDENTIFIER);
        laAddSubGroup(p, "base", "Base", "Object base", "tns_object",0,0,0,0,0,0,0,0,0,0,0,LA_UDF_LOCAL);
        ep = laAddEnumProperty(p, "camera_type", "Camera Type", "Type of a camera, like perspective or fisheye", 0,0,0,0,0,offsetof(tnsCamera, CameraType), 0,0,0,0,0,0,0,0,0,0);{
            laAddEnumItem(ep, "PERSP", "Perspective", "Camera in linear perspective", 0);
            laAddEnumItem(ep, "ORTHO", "Orthographic", "Camera in orthographic view", 0);
        }
        laAddFloatProperty(p, "fov", "FOV", "Field of view", 0,0,"^", rad(160), rad(1), rad(0.1), rad(60), 0,offsetof(tnsCamera, FOV), 0,0,0,0,0,0,0,0,0,0,LA_RAD_ANGLE);
        laAddFloatProperty(p, "depth_range", "Depth Range", "Depth Range To Map From 0 To 1", 0,"Near,far", 0,0,0,0.1, 0,0,offsetof(tnsCamera, ZMin), 0,0,2, 0,0,0,0,0,0,0,0);
        laAddFloatProperty(p, "focus_distance", "Focus Distance", "For viewing camera to determin zooming center", 0,0,0,0,0,0.1, 100,0,offsetof(tnsCamera, FocusDistance), 0,0,0,0,0,0,0,0,0,0,0);
        laAddFloatProperty(p, "orth_scale", "Scale", "Orthographical Camera Scale", 0,0,"^^", 1000,0.001, 0.1, 1, 0,offsetof(tnsCamera, OrthScale), 0,0,0,0,0,0,0,0,0,0,0);
        //laAddOperatorProperty(p, "set_active", "Set Active", "Set this camera as the active one", "Tns_set_active_camera", 0,0);
    }
    p = laAddPropertyContainer("tns_light", "Light", "Light object", U'🔅', tnsui_LightObjectProperties,sizeof(tnsLight), 0,0,2);{
        //laPropContainerExtraFunctions(p,0,0,0,tnspropagate_Object,0);
        TNS_PC_OBJECT_LIGHT=p;
        laAddStringProperty(p, "name", "Object Name", "The name of the object", 0,0,0,0,1, offsetof(tnsObject, Name), 0,0,0,0,LA_AS_IDENTIFIER);
        laAddSubGroup(p, "base", "Base", "Object base", "tns_object",0,0,0,0,0,0,0,0,0,0,0,LA_UDF_LOCAL);
        ep = laAddEnumProperty(p, "unidirectional", "UniDirectional", "Unidirectional lighting", 0,0,0,0,0,offsetof(tnsLight, UniDirectional), 0,0,0,0,0,0,0,0,0,0);{
            laAddEnumItem(ep, "NONE", "Perspective", "Camera in linear perspective", 0);
            laAddEnumItem(ep, "orthographic", "Orthographic", "Camera in orthographic view", 0);
        }
    }
    p = laAddPropertyContainer("tns_shape_object", "Shape Object", "Shape object", 0,tnsui_ShapeObjectProperties,sizeof(tnsShapeObject), tnspost_Object, 0,2);{
        laPropContainerExtraFunctions(p,0,0,tnstouched_Object,0/*tnspropagate_Object*/,0);
        TNS_PC_OBJECT_SHAPE=p;
        laAddStringProperty(p, "name", "Object Name", "The name of the object", 0,0,0,0,1, offsetof(tnsObject, Name), 0,0,0,0,LA_AS_IDENTIFIER);
        laAddSubGroup(p, "base", "Base", "Object base", "tns_object",0,0,0,0,0,0,0,0,0,0,0,LA_UDF_LOCAL);
        ep = laAddEnumProperty(p, "mode", "Mode", "Shape object mode", 0,0,0,0,0,offsetof(tnsShapeObject, Mode),0,0,0,0,0,0,0,0,0,0);{
            laAddEnumItemAs(ep, "OBJECT", "Object", "Object mode", TNS_MESH_OBJECT_MODE, 0);
            laAddEnumItemAs(ep, "EDIT", "Edit", "Edit mode", TNS_MESH_EDIT_MODE, 0);
        }
        ep = laAddEnumProperty(p, "backdrop", "Backdrop", "Draw shape as backdrop", 0,0,0,0,0,offsetof(tnsShapeObject, Backdrop),0,tnsset_ShapeObjectBackdrop,0,0,0,0,0,0,0,0);{
            laAddEnumItemAs(ep, "NONE", "None", "Draw shape on top of stuff", 0, 0);
            laAddEnumItemAs(ep, "BACKDROP", "Backdrop", "Draw shape as backdrop", 1, 0);
        }
        laAddSubGroup(p, "shapes", "Shapes", "Shapes inside of the object", "tns_shape",0,0,0,-1,0,0,0,0,0,0,offsetof(tnsShapeObject, Shapes), 0);
        laAddSubGroup(p, "current_material", "Current Material", "Current material slot in this object", "tns_material_slot",0,0,0,offsetof(tnsShapeObject, CurrentMaterial),tnsget_FirstMaterialSlot,0,laget_ListNext,0,0,0,0,LA_UDF_REFER);
        laAddSubGroup(p, "materials", "Materials", "Material slots in this mesh object", "tns_material_slot",0,0,0,-1,0,tnsget_ActiveMaterialSlot,0,tnsset_ActiveMaterialSlot,0,0,offsetof(tnsShapeObject, Materials),0);
        laAddOperatorProperty(p,"add_material_slot","Add Material Slot","Add a material slot into this object","M_new_material_slot",L'+',0);
        laAddOperatorProperty(p,"add_material","Add Material","Add a new material to this material slot","M_new_material",'+',0);
        laAddOperatorProperty(p,"assign_material_slot","Assign Material Slot","Assign shapes to a current slot","M_assign_material_slot",L'🖌',0);
    }
    
    p = laAddPropertyContainer("tns_mvert", "MVert", "MMesh vert", 0,0,sizeof(tnsMVert), 0,0,0);{
        laAddFloatProperty(p, "p", "Position", "Position", 0,"X,Y,Z", 0,0,0,0,0,0,offsetof(tnsMVert, p),0,0,3,0,0,0,0,0,0,0,0);
        laAddFloatProperty(p, "n", "Normal", "Normal", 0,"X,Y,Z", 0,0,0,0,0,0,offsetof(tnsMVert, n),0,0,3,0,0,0,0,0,0,0,0);
        laAddIntProperty(p,"i","Index","Index",0,0,0,0,0,0,0,0,offsetof(tnsMVert, i),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddIntProperty(p,"flags","Flags","Flags",0,0,0,0,0,0,0,0,offsetof(tnsMVert, flags),0,0,0,0,0,0,0,0,0,0,0);
        laAddSubGroup(p, "elink", "Edge Link", "Edge link", "tns_medge_link",0,0,0,-1,0,0,0,0,0,0,offsetof(tnsMVert, elink),0);
    }
    p = laAddPropertyContainer("tns_medge", "MEdge", "MMesh edge", 0,0,sizeof(tnsMEdge), 0,0,0);{
        laAddIntProperty(p,"i","Index","Index",0,0,0,0,0,0,0,0,offsetof(tnsMEdge, i),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddIntProperty(p,"flags","Flags","Flags",0,0,0,0,0,0,0,0,offsetof(tnsMEdge, flags),0,0,0,0,0,0,0,0,0,0,0);
        laAddSubGroup(p, "vl", "vl", "Left vert", "tns_mvert",0,0,0,offsetof(tnsMEdge, vl),0,0,0,0,0,0,0,LA_UDF_REFER);
        laAddSubGroup(p, "vr", "vr", "Right vert", "tns_mvert",0,0,0,offsetof(tnsMEdge, vr),0,0,0,0,0,0,0,LA_UDF_REFER);
        laAddSubGroup(p, "fl", "fl", "Left face", "tns_mface",0,0,0,offsetof(tnsMEdge, fl),0,0,0,0,0,0,0,LA_UDF_REFER);
        laAddSubGroup(p, "fr", "fr", "Right face", "tns_mface",0,0,0,offsetof(tnsMEdge, fr),0,0,0,0,0,0,0,LA_UDF_REFER);
    }
    p = laAddPropertyContainer("tns_mface", "MFace", "MMesh face", 0,0,sizeof(tnsMFace), 0,0,0);{
        laAddIntProperty(p,"i","Index","Index",0,0,0,0,0,0,0,0,offsetof(tnsMFace, i),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        laAddIntProperty(p,"flags","Flags","Flags",0,0,0,0,0,0,0,0,offsetof(tnsMFace, flags),0,0,0,0,0,0,0,0,0,0,0);
        laAddSubGroup(p, "l", "Loop", "Loop", "tns_loop",0,0,0,-1,0,0,0,0,0,0,offsetof(tnsMFace, l),0);
        laAddIntProperty(p,"looplen","Loop Length","Loop length",0,0,0,0,0,0,0,0,offsetof(tnsMFace, looplen),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY)->ElementBytes=2;
        laAddIntProperty(p,"mat","Material","Material of this face",0,0,0,0,0,0,0,0,offsetof(tnsMFace, mat),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY)->ElementBytes=2;
        laAddFloatProperty(p, "n", "Normal", "Normal", 0,"X,Y,Z", 0,0,0,0,0,0,offsetof(tnsMFace, n),0,0,3,0,0,0,0,0,0,0,0);
        //laAddFloatProperty(p, "gn", "Global Normal", "Global normal", 0,"X,Y,Z", 0,0,0,0,0,0,offsetof(tnsMFace, gn),0,0,3,0,0,0,0,0,0,0,0);
        laAddFloatProperty(p, "c", "Center", "Center", 0,"X,Y,Z", 0,0,0,0,0,0,offsetof(tnsMFace, c),0,0,3,0,0,0,0,0,0,0,0);
    }
    p = laAddPropertyContainer("tns_medge_link", "MEdge Link", "MEdge link", 0,0,sizeof(laListItemPointer), 0,0,0);{
        laAddSubGroup(p, "e", "Edge", "Edge", "tns_medge",0,0,0,offsetof(laListItemPointer, p),0,0,0,0,0,0,0,LA_UDF_REFER);
    }
    p = laAddPropertyContainer("tns_loop", "MFace Loop", "MFace loop", 0,0,sizeof(laListItemPointer), 0,0,0);{
        laAddSubGroup(p, "e", "Edge", "Edge", "tns_medge",0,0,0,offsetof(laListItemPointer, p),0,0,0,0,0,0,0,LA_UDF_REFER);
    }

    p = laAddPropertyContainer("tns_shape", "Shape", "Shape data structure", 0,0,sizeof(tnsShape), 0,0,0);{
        laAddIntProperty(p,"mat","Material","Material of this face",0,0,0,0,0,0,0,0,offsetof(tnsShape, mat),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY)->ElementBytes=2;
        laAddIntProperty(p,"flags","Flags","Flags",0,0,0,0,0,0,0,0,offsetof(tnsShape, flags),0,0,0,0,0,0,0,0,0,0,0);
        laAddSubGroup(p, "points", "Points", "Points in this shape", "tns_spoint",0,0,0,-1,0,0,0,0,0,0,offsetof(tnsShape, Points),0);
    }
    p = laAddPropertyContainer("tns_spoint", "SPoint", "Shape point", 0,0,sizeof(tnsSPoint), 0,0,0);{
        laAddFloatProperty(p, "p", "Position", "Position", 0,"X,Y", 0,0,0,0,0,0,offsetof(tnsSPoint, p),0,0,2,0,0,0,0,0,0,0,0);
        laAddFloatProperty(p, "dl", "Delta Left", "Delta of left control point", 0,"X,Y", 0,0,0,0,0,0,offsetof(tnsSPoint, dl),0,0,2,0,0,0,0,0,0,0,0);
        laAddFloatProperty(p, "dr", "Delta Right", "Delta of right control point", 0,"X,Y", 0,0,0,0,0,0,offsetof(tnsSPoint, dr),0,0,2,0,0,0,0,0,0,0,0);
        laAddIntProperty(p,"flags","Flags","Flags",0,0,0,0,0,0,0,0,offsetof(tnsSPoint, flags),0,0,0,0,0,0,0,0,0,0,0);
        }
}

void la_RegisterInternalProps(){
    laPropContainer *p, *ip, *p2, *ip2, *sp1, *sp2;
    laProp *ep; laPropPack pp; laSubProp *sp; laKeyMapper *km;
    laCanvasTemplate *v2dt;

    {

        la_UDFAppendSharedTypePointer("_LA_ROOT_NODE_", &MAIN);
        la_UDFAppendSharedTypePointer("_TNS_ROOT_NODE_", T);

        // THEME ==================================================================================================
        {
            p = laDefineRoot();

            laAddPropertyContainer("any_pointer", "Any Pointer", "A pointer that is not exposed to access", 0,0,sizeof(void*), 0,0,LA_PROP_OTHER_ALLOC);
            laAddPropertyContainer("any_pointer_h2", "Any Pointer (h2)", "A pointer that is not exposed to access", 0,0,sizeof(void*), 0,0,2);
            
            laAddSubGroup(p, "tns","TNS", "TNS Kernel Main Structure", "tns_main",0,0,0,-1, tnsget_TnsMain, 0,0,0,0,0,0,LA_UDF_SINGLE | LA_UDF_LOCAL);

            laAddSubGroup(p, "la","LA", "LA Main Structure", "la_main",0,0,laui_SubPropInfoDefault, -1, laget_Main, 0,0,0,0,0,0,LA_UDF_SINGLE | LA_UDF_LOCAL);

            p = laAddPropertyContainer("boxed_theme", "Boxed Theme", "A single theme item for one or multiple kinds of uiitems", 0,laui_BoxedThemeItem, sizeof(laBoxedTheme), 0,0,1);{
                laAddStringProperty(p, "name", "Name", "Boxed theme name", 0,0,0,0,1, offsetof(laBoxedTheme, Name), 0,0,0,0,LA_AS_IDENTIFIER|LA_TRANSLATE);
                laAddFloatProperty(p, "normal", "Normal", "Background brightness", 0,0,0,1, 0,0.025, 1, 0,offsetof(laBoxedTheme, NormalY), 0,laset_ThemeNormal, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "active", "Active", "Background brightness in active state", 0,0,0,1, 0,0.025, 1, 0,offsetof(laBoxedTheme, ActiveY), 0,laset_ThemeActive, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "border", "Border", "Border brightness", 0,0,0,1, 0,0.025, 1, 0,offsetof(laBoxedTheme, BorderY), 0,laset_ThemeBorder, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "text", "Text", "Text brightness", 0,0,0,1, 0,0.025, 1, 0,offsetof(laBoxedTheme, TextY), 0,laset_ThemeText, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "text_active", "Text Active", "Text brightness in active state", 0,0,0,1, 0,0.025, 1, 0,offsetof(laBoxedTheme, TextActiveY), 0,laset_ThemeTextActive, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "alpha", "Alpha", "Background alpha", 0,0,0,1, 0,0.025, 1, 0,offsetof(laBoxedTheme, Alpha), 0,laset_ThemeAlpha, 0,0,0,0,0,0,0,0,0);
                laAddSubGroup(p, "back_ref", "Back Ref", "Boxed theme internal back-ref", "any_pointer",0,0,0,offsetof(laBoxedTheme, BackRef), 0,0,0,0,0,0,0,LA_UDF_REFER);
                laAddSubGroup(p, "parent", "Parent", "Parent Theme", "theme",0,0,0,offsetof(laBoxedTheme, Parent), 0,0,0,0,0,0,0,LA_UDF_REFER);
                ep=laAddEnumProperty(p, "box_style","Box Style","Draw box pop up or lowered down",0,0,0,0,0,offsetof(laBoxedTheme, BoxStyle),0,laset_ThemeBoxStyle,0,0,0,0,0,0,0,0);
                laAddEnumItemAs(ep,"NORMAL","Normal","Normal box",0,0);
                laAddEnumItemAs(ep,"POP","Pop-up","Pop-up box",1,0);
                laAddEnumItemAs(ep,"LOWERED","Lowered","Lowered box",-1,0);
                ep=laAddEnumProperty(p, "text_shadow","Text Shadow","Whether to show text shadow",0,0,0,0,0,offsetof(laBoxedTheme, TextShadow),0,laset_ThemeTextShadow,0,0,0,0,0,0,0,0);
                laAddEnumItemAs(ep,"NONE","None","Text doesn't have any shadow",0,0);
                laAddEnumItemAs(ep,"SHADOW","Shadow","Text has shadow",1,0);
                ep=laAddEnumProperty(p, "color_selection","Color Selection","Which color to use for this widget",0,0,0,0,0,offsetof(laBoxedTheme, ColorSelection),0,laset_ThemeColorSelection,0,0,0,0,0,0,0,0);
                laAddEnumItemAs(ep,"SELA","A","Select Color A",0,0);
                laAddEnumItemAs(ep,"SELB","B","Select Color B",1,0);
                laAddEnumItemAs(ep,"SELC","C","Select Color C",2,0);
                ep=laAddEnumProperty(p, "no_decal_invert","No Decal Invert","Invert text color when widget has no decal",0,0,0,0,0,offsetof(laBoxedTheme, NoDecalInvert),0,laset_ThemeNoDecalInvert,0,0,0,0,0,0,0,0);
                laAddEnumItemAs(ep,"NONE","None","Don't invert text brightness when widget has no decal",0,0);
                laAddEnumItemAs(ep,"INVERT","Invert","Invert text brightness when widget has no decal",1,0);
            }

            p = laAddPropertyContainer("theme", "Theme Package", "A package with all types of theme for ui items", 0,laui_Theme, sizeof(laTheme),0,lapostim_Theme,2);{
                laPropContainerExtraFunctions(p,0,lareset_Theme,0,0,0);
                laAddStringProperty(p, "name", "Name", "Theme name", 0,0,0,0,1, offsetof(laTheme, Name), 0,0,laset_ThemeName,0,LA_AS_IDENTIFIER);
                laAddSubGroup(p, "boxed_themes", "Boxed Themes", "The Boxed Theme For Single UiItem Or Panel", "boxed_theme",0,0,0,-1, 0,0,0,0,0,0,offsetof(laTheme, BoxedThemes), 0);
                laAddFloatProperty(p, "color", "Color A", "Base color of the theme", LA_WIDGET_FLOAT_COLOR, "R,G,B,A", 0,1, 0,0.025, 1, 0,offsetof(laTheme, Color), 0,0,4, 0,0,0,0,laset_ThemeColor, 0,0,0);
                laAddFloatProperty(p, "color_b", "Color B", "Alternative (B) base color of the theme", LA_WIDGET_FLOAT_COLOR, "R,G,B,A", 0,1, 0,0.025, 1, 0,offsetof(laTheme, ColorB), 0,0,4, 0,0,0,0,laset_ThemeColorB, 0,0,0);
                laAddFloatProperty(p, "color_c", "Color C", "Alternative (C) base color of the theme", LA_WIDGET_FLOAT_COLOR, "R,G,B,A", 0,1, 0,0.025, 1, 0,offsetof(laTheme, ColorC), 0,0,4, 0,0,0,0,laset_ThemeColorC, 0,0,0);
                laAddFloatProperty(p, "accent_color", "Accent Color", "Theme accent color for hightlight etc", LA_WIDGET_FLOAT_COLOR, "R,G,B,A", 0,1, 0,0.025, 1, 0,offsetof(laTheme, AccentColor), 0,0,4, 0,0,0,0,laset_ThemeAccentColor, 0,0,0);
                laAddFloatProperty(p, "warning_color", "Warning Color", "Theme accent color for warning etc", LA_WIDGET_FLOAT_COLOR, "R,G,B,A", 0,1, 0,0.025, 1, 0,offsetof(laTheme, WarningColor), 0,0,4, 0,0,0,0,laset_ThemeWarningColor, 0,0,0);
                laAddFloatProperty(p, "inactive_saturation", "Inactive Saturation", "Reduced saturation on inactive widgets", 0,0,0,1, 0,0.025, 1, 0,offsetof(laTheme, InactiveSaturation), 0,laset_ThemeInactiveSaturation,0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "inactive_mix", "Inactive Mix", "Reduced alpha on inactive widgets", 0,0,0,1, 0,0.025, 1, 0,offsetof(laTheme, InactiveMix),0,laset_ThemeInactiveMix,0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "cursor_alpha", "Cursor Alpha", "Transparency of the cursor", 0,0,0,1, 0,0.025, 1, 0,offsetof(laTheme, CursorAlpha), 0,laset_ThemeCursorAlpha,0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "selection_alpha", "Selection Alpha", "Transparency of selection backgrounds", 0,0,0,1, 0,0.025, 1, 0,offsetof(laTheme, SelectionAlpha),0,laset_ThemeSelectionAlpha,0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "shadow_alpha", "Shadow Alpha", "Transparency of the shadow", 0,0,0,1, 0,0.025, 1, 0,offsetof(laTheme, ShadowAlpha), 0,laset_ThemeShadowAlpha,0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "text_shadow_alpha", "Text Shadow Alpha", "Transparency of the text shadow", 0,0,0,1, 0,0.025, 1, 0,offsetof(laTheme, TextShadowAlpha), 0,laset_ThemeTextShadowAlpha,0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "wire_transparency", "Wire Transparency", "Alpha of the wire color", 0,0,0,1, 0,0.05, 0.7, 0,offsetof(laTheme, WireTransparency), 0,laset_ThemeWireTransparency, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "wire_saturation", "Wire Saturation", "Saturation of the wires", 0,0,0,1, 0,0.05, 0.8, 0,offsetof(laTheme, WireSaturation), 0,laset_ThemeWireSaturation, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "wire_brightness", "Wire Brightness", "Brightness of the wires", 0,0,0,1, 0,0.05, 0.8, 0,offsetof(laTheme, WireBrightness), 0,laset_ThemeWireBrightness, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "edge_transparency", "Edge Transparency", "Transparency of the edge color", 0,0,0,1, 0,0.05, 0.7, 0,offsetof(laTheme, EdgeTransparency), 0,laset_ThemeEdgeTransparency, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "edge_brightness", "Edge Brightness", "Brightness of the edge color", 0,0,0,1, 0,0.05, 0.7, 0,offsetof(laTheme, EdgeBrightness), 0,laset_ThemeEdgeBrightness, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "vertex_transparency", "Edge Transparency", "Transparency of the vertex color", 0,0,0,1, 0,0.05, 0.7, 0,offsetof(laTheme, VertexTransparency), 0,laset_ThemeVertexTransparency, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "vertex_brightness", "Edge Brightness", "Brightness of the vertex color", 0,0,0,1, 0,0.05, 0.7, 0,offsetof(laTheme, VertexBrightness), 0,laset_ThemeVertexBrightness, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "svertex_transparency", "Selected Vertex Transparency", "Transparency of selected vertices", 0,0,0,1, 0,0.05, 0.7, 0,offsetof(laTheme, SelectedVertexTransparency), 0,laset_ThemeSVertexTransparency, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "sedge_transparency", "Selected Edge Transparency", "Transparency of selected edges", 0,0,0,1, 0,0.05, 0.7, 0,offsetof(laTheme, SelectedEdgeTransparency), 0,laset_ThemeSEdgeTransparency, 0,0,0,0,0,0,0,0,0);
                laAddFloatProperty(p, "sface_transparency", "Selected Face Transparency", "Transparency of selected faces", 0,0,0,1, 0,0.05, 0.7, 0,offsetof(laTheme, SelectedFaceTransparency), 0,laset_ThemeSFaceTransparency, 0,0,0,0,0,0,0,0,0);
                sp=laAddSubGroup(p,"preview","Preview","Theme preview","theme",0,0,0,-1,laget_Self,0,0,0,0,0,0,LA_READ_ONLY|LA_UDF_IGNORE|LA_UDF_REFER);
                laSubGroupExtraFunctions(sp,0,0,laget_ThemePreviewTheme,0,0);
                laAddOperatorProperty(p, "delete", "Delete", "Delete this theme", "LA_delete_theme", 0,0);
                laAddOperatorProperty(p, "save_as", "Save as", "Save theme as file", "LA_udf_save_instance", U'🖫',0);
                laAddOperatorProperty(p, "duplicate", "Duplicate", "Duplicate this theme", "LA_new_theme", U'⎘',0)
                    ->ExtraInstructions="duplicate=true;";
            }
        }

        // TIME INFO =========================================================================================

        p = laAddPropertyContainer("time_info", "Time Info", "Time information y/m/d/h/m/s", U'🕒', 0,sizeof(laTimeInfo), 0,0,LA_PROP_OTHER_ALLOC);{
            laAddIntProperty(p, "year", "Year", "Year value", 0,0,0,0,0,0,0,0,0,laget_TimeYear, 0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "month", "Month", "Month value", 0,0,0,0,0,0,0,0,0,laget_TimeMonth, 0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "day", "Day", "Day value", 0,0,0,0,0,0,0,0,0,laget_TimeDay, 0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "hour", "Hour", "Hour value", 0,0,0,0,0,0,0,0,0,laget_TimeHour, 0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "minute", "Minute", "Minute value", 0,0,0,0,0,0,0,0,0,laget_TimeMinute, 0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "second", "Second", "Second value", 0,0,0,0,0,0,0,0,0,laget_TimeSecond, 0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddStringProperty(p, "time_string", "Time String", "Full String In \"Y-M-D H:M:S\" format", 0,0,0,0,0,0,0,laget_TimeString, 0,0,LA_READ_ONLY);
        }

        // LA MAIN =========================================================================================

        p = laAddPropertyContainer("la_main", "LA Root", "La root structure", U'🖴', 0,sizeof(LA), 0,0,2|LA_PROP_OTHER_ALLOC);{
            laAddSubGroup(p, "logs", "Logs", "Application logs", "la_log",0,0,laui_LogItem, -1, 0,0,0,0,0,0,offsetof(LA, Logs), LA_UDF_IGNORE|LA_READ_ONLY);
            laAddStringProperty(p, "terminal_input", "Terminal Input", "Terminal input string", 0,0,0,0,0, offsetof(LA,TerminalInput),0,0,laset_TerminalInput,0,LA_UDF_LOCAL|LA_UDF_IGNORE);
            ep=laAddEnumProperty(p, "terminal_incomplete", "Terminal Incomplete", "Incomplete input pending in terminal", 0,0,0,0,0,offsetof(LA, TerminalIncomplete),0,0,0,0,0,0,0,0,0,LA_READ_ONLY|LA_UDF_IGNORE);
            laAddEnumItemAs(ep, "NONE", "None", "Terminal status is normal", 0, 0);
            laAddEnumItemAs(ep, "INCOMPLETE", "Incomplete", "Terminal has incomplete input pending", 1, 0);

            laAddSubGroup(p, "differences", "Differences", "Difference stack (for undo/redo)", "la_difference",0,0,0,offsetof(LA, HeadDifference), 0,0,0,0,0,0,offsetof(LA, Differences), LA_UDF_IGNORE|LA_READ_ONLY);
            sp=laAddSubGroup(p, "panel_templates", "Panel Templates", "Panel templates used to create new panel", "panel_template",0,0,0,-1, 0,0,0,0,0,0,offsetof(LA, PanelTemplates), 0);
            
            _LA_PROP_WINDOW=laAddSubGroup(p, "windows", "Windows", "All windows inside this application", "ui_window",0,0,0,offsetof(LA, CurrentWindow), laget_FirstWindow, 0,laget_ListNext, 0,0,0,offsetof(LA, Windows), 0);
            la_UDFAppendSharedTypePointer("_LA_PROP_WINDOW", _LA_PROP_WINDOW);
            laAddSubGroup(p, "data", "Data", "Data Root Control", "property_container",0,0,0,offsetof(LA, DataRoot.Root), 0,0,0,0,0,0,0,0);
            laAddSubGroup(p, "prop_containers", "Sub Type", "Sub Type Property Container", "property_container",0,0,0,-1, laget_FirstContainer, 0,laget_ListNext, 0,0,0,0,0);
            laAddSubGroup(p, "managed_props", "Managed Props", "Managed properties for saving", "managed_prop",0,0,0,-1, 0,0,0,0,0,0,offsetof(LA, ManagedSaveProps), LA_UDF_IGNORE|LA_READ_ONLY);
            laAddSubGroup(p, "managed_udfs", "Managed UDFs", "Managed UDFs for saving", "managed_udf", 0,0,laui_ManagedUDFOps, -1, 0,0,0,0,0,0,offsetof(LA, ManagedUDFs), LA_UDF_IGNORE|LA_READ_ONLY);
            laAddSubGroup(p, "user_preferences", "User Preference", "Kernel Settings For LA Main Structure", "la_user_preference",0,0,0,-1, laget_Main, 0,0,0,0,0,0,LA_UDF_LOCAL);
            laSubGroupExtraFunctions(sp,0,0,0,0,laget_PanelTemplateCategory);

            sp=laAddSubGroup(p, "themes", "Themes", "Themes Loded In The Program", "theme",0,0,laui_Theme, -1, 0,laget_ActiveTheme, 0,laset_ActiveTheme, 0,0,offsetof(LA,Themes), 0);

            sp=laAddSubGroup(p, "controllers", "Controllers", "Detected game controllers","la_controller",laget_ControllerType,0,0,-1,0,0,0,0,0,0,offsetof(LA,Controllers),0);
            laSubGroupDetachable(sp, laget_DetachedControllerFirst, laget_ListNext);
            LA_PROP_CONTROLLER=sp;

            sp=laAddSubGroup(p, "node_categories", "Node Categories", "Node categories","la_node_category",0,0,0,offsetof(LA,CurrentNodeCategory),0,0,0,0,0,0,offsetof(LA,NodeCategories),0);
            laSubGroupExtraFunctions(sp,lafilter_NodeCategory,0,0,0,0);

            laAddSubGroup(p, "animation", "Animation", "Animation data","la_animation",0,0,0,offsetof(LA,Animation),0,0,0,0,0,0,0,LA_UDF_SINGLE);
            if(MAIN.InitArgs.HasAudio) laAddSubGroup(p, "audio", "Audio", "Audio data","la_audio",0,0,0,offsetof(LA,Audio),0,0,0,0,0,0,0,LA_UDF_SINGLE);

            laAddSubGroup(p,"input_mapping","Input Mapping","Input mapping bundle","la_input_mapping_bundle",0,0,0,offsetof(LA,InputMapping),0,0,0,0,0,0,0,LA_UDF_SINGLE);
            laAddStringProperty(p,"signal_filter","Signal Filter","Filter displayed signals",0,0,0,0,1,offsetof(LA,SignalFilter),0,0,laset_SignalFilter,0,0);
            laAddStringProperty(p,"operator_filter","Operator Filter","Filter displayed operators",0,0,0,0,1,offsetof(LA,OperatorFilter),0,0,laset_OperatorFilter,0,0);
            laAddSubGroup(p,"custom_signals","Signals","Registered custom signals","la_custom_signal",0,0,laui_IdentifierOnly,-1,laget_FirstCustomSignal,0,laget_ListNext,0,0,0,0,LA_UDF_IGNORE);
            laAddSubGroup(p,"filtered_signals","Filtered Signals","Filtered custom signals","la_custom_signal",0,0,laui_IdentifierOnly,-1,laget_FirstFilteredCustomSignal,0,lagetnext_FilteredSignal,0,0,0,0,LA_UDF_IGNORE);
            laAddSubGroup(p,"filtered_operators","Filtered Operators","Filtered operators","la_operator_type",0,0,laui_IdentifierOnly,-1,laget_FirstFilteredOperator,0,lagetnext_FilteredOperator,0,0,0,0,LA_UDF_IGNORE);
            
            laAddStringProperty(p, "identifier", "Identifier", "Identifier", 0,0,0,0,0,0,0,laget_MainIdentifier, 0,0,LA_AS_IDENTIFIER|LA_READ_ONLY);
  
            laAddIntProperty(p, "example_int", "Example Integer", "Example integer", 0,0,0,100,0,1, 50,0,offsetof(LA, example_int), 0,0,0,0,0,0,0,0,0,0,0);
            laAddStringProperty(p, "example_string", "Example String", "Example string", 0,0,0,0,1, offsetof(LA,example_string), 0,0,0,0,0);
            laAddStringProperty(p, "unknown_prop", "Unknown Property", "Placeholder for unknown property, probably due to a wrong property path, or not setting LA_AS_IDENTIFIER in any of the properties.",
                LA_WIDGET_STRING_PLAIN, 0,0,0,0,0,0,laget_UnknownPropertyString, 0,0,LA_READ_ONLY);
            ep=laAddEnumProperty(p, "detached_view_switch", "View Detached", "Use detached view on panels", LA_WIDGET_ENUM_HIGHLIGHT,0,0,0,0,offsetof(LA, DetachedViewSwitch),0,0,0,0,0,0,0,0,0,LA_UDF_IGNORE);
            laAddEnumItemAs(ep, "LINKED", "Linked", "View is linked to global current states", 0, U'🔗');
            laAddEnumItemAs(ep, "DETACHED", "Detached", "View is detached from global states and is pinned to panel", 1, U'📌');
        }
        
        if(MAIN.InitArgs.HasAudio){
            p = laAddPropertyContainer("la_audio", "Audio", "Audio management", 0,0, sizeof(laAudio), 0,0,1);{
                laAddSubGroup(p,"synths","Synthesizers","Synthesizers","la_synth",0,0,0,-1,0,0,0,0,0,0,offsetof(laAudio,Synths),0);
                sp=laAddSubGroup(p, "current_synth", "Current Synthesizer", "Current synthesizer","la_synth",0,0,0,offsetof(laAudio,CurrentSynth),laget_FirstSynth,0,laget_ListNext,laset_CurrentSynth,0,0,0,LA_UDF_REFER);
                laSubGroupDetachable(sp,laget_FirstSynth,laget_ListNext);
                laAddSubGroup(p,"audio_devices","Audio Devices","Enumerated audio devices","la_audio_device",0,0,0,-1,0,0,0,0,0,0,offsetof(laAudio,AudioDevices),LA_UDF_IGNORE);
                laAddSubGroup(p, "current_audio_device", "Current Audio Device", "Current audio device","la_audio_device",0,0,0,offsetof(laAudio,UsingDevice),laget_FirstAudioDevice,0,laget_ListNext,laset_CurrentAudioDevice,0,0,0,LA_UDF_REFER|LA_UDF_IGNORE);
                laAddSubGroup(p,"channels","Channels","Audio channels","la_audio_channel",0,0,0,-1,0,0,0,0,0,0,offsetof(laAudio,Channels),0);
                ep=laAddEnumProperty(p,"backend","Backend","Audio backend",0,0,0,ma_backend_null,0,offsetof(laAudio,MiniAudioBackend),0,laset_MiniAudioBackend,0,0,0,0,0,0,0,0);
                laAddEnumItemAs(ep,"AUTO","Auto","Automatic backend selection",ma_backend_null,0);
                laAddEnumItemAs(ep,"JACK","Jack","Jack backend",ma_backend_jack,0);
#ifdef LA_LINUX
                laAddEnumItemAs(ep,"PULSEAUDIO","Pulseaudio","Pulseaudio backend",ma_backend_pulseaudio,0);
                laAddEnumItemAs(ep,"ALSA","Alsa","Alsa backend",ma_backend_alsa,0);
#endif
#ifdef _WIN32
                laAddEnumItemAs(ep,"WASAPI","WASAPI"," WASAPI backend",ma_backend_wasapi,0);
                laAddEnumItemAs(ep,"DIRECTSOUND","DirectSound","DirectSound backend",ma_backend_dsound, 0);
                laAddEnumItemAs(ep,"WINMM","WinMM","WinMM backend",ma_backend_winmm,0);
#endif
#ifdef LAGUI_ANDROID
                laAddEnumItemAs(ep,"AAUDIO","AAudio"," AAudio backend",ma_backend_aaudio,0);
                laAddEnumItemAs(ep,"OpenSL ES","OpenSL ES","OpenSL ES backend",ma_backend_opensl,0);
#endif
            }
        }

        p = laAddPropertyContainer("la_node_category", "Node Category", "Node category", 0,laui_IdentifierOnly, sizeof(laNodeCategory), 0,0,1);{
            laAddStringProperty(p, "name", "Name", "Name of the page", 0,0,0,0,1, offsetof(laNodeCategory, Name), 0,0,0,0,LA_AS_IDENTIFIER|LA_TRANSLATE);
            laAddIntProperty(p, "for_racks", "For Rack Types", "For rack types", 0,0,0,0,0,0,0,0,offsetof(laNodeCategory, For), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        }

        p = laAddPropertyContainer("la_driver_collection", "Driver Collection", "Driver collection", 0,0,sizeof(laRackPageCollection), 0,0,1);{
            laAddSubGroup(p, "pages", "Pages", "Rack pages","la_rack_page",0,0,0,-1,0,laget_CurrentRackPage,0,0,0,0,offsetof(laRackPageCollection,Pages),0);
            sp=laAddSubGroup(p, "current_page", "Current Page", "Current page","la_rack_page",0,0,0,offsetof(laRackPageCollection,CurrentPage),laget_FirstDriverPage,0,laget_ListNext,laset_CurrentRackPage,0,0,0,LA_UDF_REFER);
            //laSubGroupDetachable(sp,laget_FirstDriverPage,laget_ListNext);
        }

        p = laAddPropertyContainer("la_rack_page", "Rack Page", "A page of nodes", 0,laui_IdentifierOnly, sizeof(laRackPage),lapost_RackPage,0,1);{
            LA_PC_RACK_PAGE = p;
            laPropContainerExtraFunctions(p,0,lareset_RackPage,0,0,0);
            laAddStringProperty(p, "name", "Name", "Name of the page", 0,0,0,0,1, offsetof(laRackPage, Name), 0,0,0,0,LA_AS_IDENTIFIER);
            laAddIntProperty(p,"type", "Type", "Type of the rack", 0,0,0,0,0,0,0,0,offsetof(laRackPage,RackType),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p,"has_rack", "Has Rack", "Has rack", 0,0,0,0,0,0,0,0,offsetof(laRackPage,Racks.pFirst),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY|LA_UDF_IGNORE);
            laAddSubGroup(p, "racks", "Racks", "Racks for nodes","la_node_rack",0,0,0,-1,0,0,0,0,0,0,offsetof(laRackPage,Racks),0);
            if(MAIN.InitArgs.HasWorldObjects){
                laAddSubGroup(p, "parent_object", "Parent Object", "Parent object of this page","tns_object",0,0,0,offsetof(laRackPage,ParentObject),0,0,0,0,0,0,0,LA_READ_ONLY|LA_UDF_REFER);
            }
            ep=laAddEnumProperty(p, "trigger", "Trigger", "Trigger evaluation on event",0,0,0,0,0,offsetof(laRackPage,TriggerMode),0,0,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "PROCESS", "onProcess", "Run this page during idle frame processing of the node",TNS_PAGE_TRIGGER_PROCESS,0);
            laAddEnumItemAs(ep, "KEYBOARD", "onKeyboard", "This page is run when keyboard event is recieved",TNS_PAGE_TRIGGER_KEYBOARD,0);
            laAddEnumItemAs(ep, "MOUSE", "onMouse", "This page is run when mouse event is recieved",TNS_PAGE_TRIGGER_KEYBOARD,0);
            laAddEnumItemAs(ep, "CONTROLLER", "onController", "This page is run when controller event is recieved",TNS_PAGE_TRIGGER_KEYBOARD,0);
            ep=laAddEnumProperty(p, "use_script", "Use Script", "Use script instead of nodes",0,0,0,0,0,offsetof(laRackPage,UseScript),0,0,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "NODES", "Nodes", "Use nodes to express logic",0,0);
            laAddEnumItemAs(ep, "SCRIPT", "Script", "Use scripts to express logic",1,L'📃');
            laAddStringProperty(p, "script", "Script", "Script content",LA_WIDGET_STRING_MULTI,0,0,0,1,offsetof(laRackPage, Script), 0,0,0,0,0);
            laAddOperatorProperty(p,"add_rack","Add Rack", "Add a rack into the page", "LA_add_rack", '+', 0);
            laAddOperatorProperty(p,"remove_driver_page","Remove Page", "Remove this page", "LA_remove_driver_page", L'🗴', 0);
        }

        //p = laAddPropertyContainer("udf_fail_node", "UDF Failed Node", "Single wild data block reference", laui_UDFFailNodeItem, sizeof(laUDFFailNode), 0,0,0,0,0); {
        //	laAddSubGroup(p, "instance", "Instance", "Actual Data Block Instance", "property_trash_item",0,0,0 Item.p), 0,0,0,0,0,0,0,0,
        //		0,0,
        //		0);
        //	laAddSubGroup(p, "target_container", "Target Container", "The Container The Data Block Belongs To", "property_container",0,0,0 pc), 0,0,0,0,0,0,0,0,
        //		0,0,
        //		0);
        //}
        
        p = laAddPropertyContainer("la_difference", "Difference", "A difference stack item (undo/redo).", 0,laui_IdentifierOnly, sizeof(laDiff), 0,0,1);{
            laAddStringProperty(p, "description", "Description", "Description of this recorded change", LA_WIDGET_STRING_PLAIN, 0,0,0,1, offsetof(laDiff, Description), 0,0,0,0,LA_READ_ONLY|LA_AS_IDENTIFIER);
        }

        p = laAddPropertyContainer("la_log", "Resource Folder", "A resource folder to search for UDF references.", 0,laui_ResourceFolderItem, sizeof(laLogEntry), 0,0,1);{
            laAddStringProperty(p, "content", "Content", "Content of the log", LA_WIDGET_STRING_PLAIN, 0,0,0,1, offsetof(laLogEntry, Content), 0,0,0,0,LA_READ_ONLY);
        }

        p = laAddPropertyContainer("la_resource_folder", "Resource Folder", "A resource folder to search for UDF references.", 0,laui_ResourceFolderItem, sizeof(laResourceFolder), 0,0,1);{
            laAddStringProperty(p, "path", "Path", "Path", 0,0,0,0,1, offsetof(laResourceFolder, Path), 0,0,laset_ResourcePath, 0,0);
            laAddOperatorProperty(p, "remove", "Remove", "Remove this resource folder entry", "LA_remove_resource_folder", U'🞫', 0);
        }

        p = laAddPropertyContainer("managed_udf", "Managed UDF", "Managed UDF files", U'🖹', laui_ManagedUDFItem, sizeof(laManagedUDF), 0,0,0);{
            laAddStringProperty(p, "basename", "Base Name", "Base name of the file (withiout directory)", 0,0,0,0,1, offsetof(laManagedUDF, BaseName), 0,0,0,0,LA_AS_IDENTIFIER|LA_READ_ONLY);
            laAddSubGroup(p, "udf", "UDF", "Reference to target UDF file", "udf",0,0,0,offsetof(laManagedUDF, udf), 0,0,0,0,0,0,0,LA_UDF_REFER);
        }
        p = laAddPropertyContainer("udf", "UDF File", "UDF file block", U'🖹', 0,sizeof(laUDF), 0,0,0);{
            laAddStringProperty(p, "path", "Path", "File path", 0,0,0,0,1, offsetof(laUDF, FileName), 0,0,0,0,LA_READ_ONLY);
            ep=laAddEnumProperty(p, "modified", "Modified", "File modified", LA_WIDGET_ENUM_ICON_PLAIN,0,0,0,0,offsetof(laUDF, Modified),0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddEnumItemAs(ep, "MODIFIED", "Modified", "There are unsaved changes bound to this file", 1, U'🌑');
            laAddEnumItemAs(ep, "CLEAN", "Clean", "File data is untouched", 0,0);
            ep->ElementBytes=2;
        }
        
        p = laAddPropertyContainer("managed_prop", "Managed Property", "Managed property for detecting changes for saving files", 0,0,sizeof(laManagedSaveProp), 0,0,0);{
            laAddStringProperty(p, "path", "Path", "Property path", 0,0,0,0,1, offsetof(laManagedSaveProp, Path), 0,0,0,0,LA_READ_ONLY);
        }

        p = laAddPropertyContainer("int_property", "Int Property", "Int property specific info", U'i', laui_IntPropInfo, sizeof(laIntProp), 0,0,1);{
            laAddIntProperty(p, "range", "Range", "Range of the property", 0,"Min,Max", 0,0,0,0,0,0,offsetof(laIntProp, Min), 0,0,2, 0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "default", "Default", "Default value", 0,0,0,0,0,0,0,0,offsetof(laIntProp, DefVal), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "step", "Step", "Ui step of the value", 0,0,0,0,0,0,0,0,offsetof(laIntProp, Step), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddSubGroup(p, "base", "Base", "Property Base", "property_item", 0,0,0,0,0,0,0,0,0,0,0,LA_UDF_SINGLE|LA_UDF_LOCAL);
        } MAIN.ContainerInt = p;

        p = laAddPropertyContainer("float_property", "Float Property", "Float property specific info", U'i', laui_FloatPropInfo, sizeof(laFloatProp), 0,0,1);{
            laAddFloatProperty(p, "range", "Range", "Range of the property", 0,"Min,Max", 0,0,0,0,0,0,offsetof(laFloatProp, Min), 0,0,2, 0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddFloatProperty(p, "default", "Default", "Default value", 0,0,0,0,0,0,0,0,offsetof(laFloatProp, DefVal), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddFloatProperty(p, "step", "Step", "Ui step of the value", 0,0,0,0,0,0,0,0,offsetof(laFloatProp, Step), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddSubGroup(p, "base","Base", "Property Base", "property_item", 0,0,0,0,0,0,0,0,0,0,0,LA_UDF_SINGLE|LA_UDF_LOCAL);
        } MAIN.ContainerFloat = p;

        // USER PREF ========================================================

        p = laAddPropertyContainer("la_user_preference", "User Preference", "Kernel settings for la main structure", U'⚙', 0,sizeof(LA), lapost_UserPreferences, 0,2|LA_PROP_OTHER_ALLOC);{
            laPropContainerExtraFunctions(p,0,lareset_Main,0,0,0);
            laAddFloatProperty(p, "idle_time", "Idle time", "Time out on no input to show tooltips", 0,0,0,2.0,0.3, 0.05, 0.75, 0,offsetof(LA, IdleTime), 0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "top_framerate", "Top Framerate", "Framerate limiter for drawing the user interface", 0,0,0,60,25, 1, 60,0,offsetof(LA, TopFramerate), 0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "valuator_threshold", "Valuator Threshold", "Drag how many pixels trigger a change in valuator", 0,0,0,10,1, 1, 3, 0,offsetof(LA, ValuatorThreshold), 0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "scroll_speed", "Scrolling Speed", "How many rows to move when scrolling using mouse wheel", 0,0,0,10,1, 1, 3, 0,offsetof(LA, ScrollingSpeed), 0,0,0,0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "animation_speed", "Animation Speed", "Ui animation speed", 0,0,0,0.6, 0.1, 0.05, 0.2, 0,offsetof(LA, AnimationSpeed), 0,0,0,0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "panel_animation_speed", "Panel Animation Speed", "Panel animation speed", 0,0,0,0.6, 0.1, 0.05, 0.2, 0,offsetof(LA, PanelAnimationSpeed), 0,0,0,0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "zoom_speed_2d", "Zoom Speed 2D", "2d canvas zooming speed", 0,0,0,0.5, 0.01, 0.01, 0.01, 0,offsetof(LA, ZoomSpeed2D), 0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "tooltip_close_distance", "Tooltip Close Distance", "The tooltip will hide if mouse moved away further", 0,0,0,100,0,1, 40,0,offsetof(LA, TooltipCloseDistance), 0,0,0,0,0,0,0,0,0,0,0);
            ep = laAddEnumProperty(p, "panel_multisample", "Panel Multisample", "Multisample mode for drawing panels", 0,0,0,0,0,offsetof(LA, PanelMultisample), 0,laset_PanelMultisample, 0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "none", "None", "Don't use multisample", 0,0);
                laAddEnumItemAs(ep, "2", "2X", "2X multisample", 2, 0);
                laAddEnumItemAs(ep, "4", "4X", "4X multisample", 4, 0);
                laAddEnumItemAs(ep, "8", "8X", "8X multisample", 8, 0);
                laAddEnumItemAs(ep, "16", "16X", "16X multisample", 16, 0);
            }
            
            ep=laAddEnumProperty(p, "enable_gl_debug", "Enable OpenGL Debug", "Enable OpenGL debug output in the system terminal", LA_WIDGET_ENUM_HIGHLIGHT,0,0,0,0,offsetof(LA, EnableGLDebug),0,laset_EnableGLDebug,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "DISABLED", "Disabled", "OpenGL debug output is disabled", 0, 0);
            laAddEnumItemAs(ep, "ENABLED", "Enabled", "Will print OpenGL debug information to the system terminal", 1, 0);
            ep=laAddEnumProperty(p, "gl_debug_sync", "Debug Sync", "Whether OpenGL debug output should be in sync with the caller", LA_WIDGET_ENUM_HIGHLIGHT,0,0,0,0,offsetof(LA, GLDebugSync),0,laset_GLDebugSync,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "DISABLED", "Disabled", "OpenGL debug output will be printing freely", 0, 0);
            laAddEnumItemAs(ep, "ENABLED", "Enabled", "OpenGL debug output will hold the gl-call until return", 1, 0);
            ep=laAddEnumProperty(p, "gl_debug_level", "Debug Level", "OpenGL debug message level", 0,"Note,Low,Med,High",0,0,0,offsetof(LA, GLDebugLevel),0,laset_GLDebugLevel,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "ALL", "ALL", "Show all debug messages", 0, 0);
            laAddEnumItemAs(ep, "NOTE", "Note", "Show notifications", 1, 0);
            laAddEnumItemAs(ep, "LOW", "Low", "Show low severity messages", 2, 0);
            laAddEnumItemAs(ep, "MEDIUM", "Medium", "Show low severity messages", 3, 0);
            laAddEnumItemAs(ep, "HIGH", "High", "Show high severity messages", 4, 0);

            ep=laAddEnumProperty(p, "enable_log_stdout", "Enable Log StdOut", "Enable log printing to stdout", LA_WIDGET_ENUM_HIGHLIGHT,0,0,0,0,offsetof(LA, EnableLogStdOut),0,0,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "DISABLED", "Disabled", "StdOut log only outputs necessary stuff", 0, 0);
            laAddEnumItemAs(ep, "ENABLED", "Enabled", "Will print all logs to stdout", 1, 0);

            ep = laAddEnumProperty(p, "enable_performance_overlay", "Performance Overlay", "Show performance overlay for development purposes", LA_WIDGET_ENUM_HIGHLIGHT,0,0,0,0,offsetof(LA, ShowPerf),0,0,0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "NONE", "None", "Don't show overlay", 0,0);
                laAddEnumItemAs(ep, "PERF", "Perf", "Show performance overlay", 1,0);
            }
            
            ep=laAddEnumProperty(p, "enable_color_management", "Enable Color Management", "Whether to enable color management or not", LA_WIDGET_ENUM_HIGHLIGHT,0,0,0,0,offsetof(LA, EnableColorManagement),0,0,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "NONE", "None", "Do not do color management on the window", 0, 0);
            laAddEnumItemAs(ep, "ENABLED", "Enabled", "Enable color management on the window", 1, 0);

            laAddFloatProperty(p, "margin_size", "Margin Size", "The global margin factor", 0,0,0,2.0f, 0.1f, 0.02, 1.0f, 0,offsetof(LA, MarginSize), 0,laset_MarginSize, 0,0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "font_size", "Font Size", "The height of the font related to the row height", 0,0,0,1.0f, 0.1f, 0.02, 0.75, 0,offsetof(LA, FontSize), 0,laset_FontSize, 0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "interface_size", "Interface Size", "The height of one row of ui item", 0,0,0,64, 16, 1, 40,0,offsetof(LA, UiRowHeight), 0,laset_UiRowHeight, 0,0,0,0,0,0,0,0,0);

            laAddIntProperty(p, "wire_color_slices", "Wire color slices", "How many slices of color to give to node sockets", 0,0,0,32, 1, 1, 16, 0,offsetof(LA, WireColorSlices), 0,laset_WireColorSlices, 0,0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "wire_thickness", "Wire thickness", "How thick is a wire", 0,0,0,10,0.1, 0.5, 5, 0,offsetof(LA, WireThickness), 0,laset_WireThickness, 0,0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "wire_saggyness", "Wire saggyness", "How saggy is a wire", 0,0,0,1,0,0.01, 0.5, 0,offsetof(LA, WireSaggyness), 0,laset_WireSaggyness, 0,0,0,0,0,0,0,0,0);

            ep = laAddEnumProperty(p, "enable_translation", "Enable Translation", "Translate user interface into another language", 0,0,0,1, 0,offsetof(LA, Translation.EnableTranslation), 0,transState, 0,0,0,0,0,0,0,0);{
                laAddEnumItem(ep, "no", "No", "Use original english string", 0);
                laAddEnumItem(ep, "yes", "Yes", "Use translated string", 0);
            }
            laAddSubGroup(p, "languages", "Language", "The language list in the software", "la_translation_language",0,0,laui_IdentifierOnly, offsetof(LA, Translation.CurrentLanguage), 0,0,0,laset_Language,0,0,offsetof(LA, Translation.Languages),LA_UDF_IGNORE);
            
            laAddSubGroup(p, "resource_folders", "Resource Folders", "Folders to search for resources", "la_resource_folder",0,0,0,-1, 0,0,0,0,0,0,offsetof(LA, ResourceFolders), 0);
            ep = laAddEnumProperty(p, "manager_default_view", "UDF Manager Default View", "Prefer to show data blocks or files when saving", 0,0,0,0,0,offsetof(LA, ManagerDefaultView), 0,0,0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "DATA_BLOCKS", "Data Blocks", "All data blocks", 0,0);
                laAddEnumItemAs(ep, "FILES", "Files", "All Files", 1, 0);
            }
            ep = laAddEnumProperty(p, "manager_filter_instances", "Filter Instances", "Whether to show only unsaved stuff or everything", 0,0,0,0,0,offsetof(LA, ManagerFilterInstances), 0,laset_ManagerFilterInstances,0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "ALL", "All", "All Instances", 0,0);
                laAddEnumItemAs(ep, "Modified", "Modified", "Modified and unsigned instances", 1, 0);
            }
            ep = laAddEnumProperty(p, "save_preferences_on_exit", "Save On Exit", "Save user preferences before exiting the program", 0,0,0,0,0,offsetof(LA, SavePreferenceOnExit), 0,0,0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "NONE", "Don't save", "Not saving user preferences on exit", 0,0);
                laAddEnumItemAs(ep, "SAVE", "Save", "Saving user preferences on exit", 1, U'✓');
            }
        
            laAddIntProperty(p, "wacom_device_stylus", "Stylus Device", "Wacom stylus device ID", LA_WIDGET_INT_PLAIN, 0,0,0,0,0,0,0,offsetof(LA, WacomDeviceStylus), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY|LA_UDF_IGNORE);
            laAddIntProperty(p, "wacom_device_eraser", "Eraser Device", "Wacom eraser device ID", LA_WIDGET_INT_PLAIN, 0,0,0,0,0,0,0,offsetof(LA, WacomDeviceEraser), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY|LA_UDF_IGNORE);
            
            ep = laAddEnumProperty(p, "wacom_driver", "Wacom driver", "Wacom driver to read pressure info from on Windows", 0, 0, 0, 0, 0, offsetof(LA, InkOrWinTab), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0); {
                laAddEnumItemAs(ep, "WINDOWS_INK", "Windows Ink", "Use Windows Ink pressure events (WM_POINTERUPDATE).", 0, 0);
                laAddEnumItemAs(ep, "WINTAB", "WinTab", "Use wacom WinTab driver to read pressure events (WT_PACKET).", 1, 0);
            }

            laAddFloatProperty(p, "color_picker_gamma", "Color Picker Gamma", "Allows less saturated colors to have more areas", 0,0,0,3.0,1.0, 0.05,1.5, 0,offsetof(LA, ColorPickerGamma), 0,laset_ColorPickerGamma,0,0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "viewport_halftone_factor", "Halftone Factor", "Viewport halftone mixing factor", 0,0,0,1.0,0.0,0.05,0.5, 0,offsetof(LA, ViewportHalftoneFactor), 0,laset_ViewportHalftoneFactor,0,0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "viewport_halftone_size", "Halftone Size", "Viewport halftone size", 0,0,0,8.0,1.9, 0.05,3.7, 0,offsetof(LA, ViewportHalftoneSize), 0,laset_ViewportHalftoneSize,0,0,0,0,0,0,0,0,0);

            laAddStringProperty(p,"theme_name","Theme Name","Using theme name",0,0,0,0,0,0,0,laget_UsingTheme,0,laread_UsingTheme,0);

            laAddSubGroup(p,"screens","Screens","Screens connected to this computer","la_screen",0,0,0,-1,0,0,0,0,0,0,offsetof(LA,Screens),0);
            ep = laAddEnumProperty(p, "auto_switch_color_space", "Auto Switch Color Space", "Automatically switch color space for windows on different screens",LA_WIDGET_ENUM_HIGHLIGHT, 0, 0, 0, 0, offsetof(LA, AutoSwitchColorSpace), 0, laset_AutoSwitchColorSpace, 0, 0, 0, 0, 0, 0, 0, 0); {
                laAddEnumItemAs(ep, "NONE", "None", "Windows keep their own color space", 0, 0);
                laAddEnumItemAs(ep, "ENABLED", "Enabled", "Automatically switch window's color space based on settings here", 1, 0);
            }
        }

        p = laAddPropertyContainer("la_input_mapping_bundle", "Input Mapping Bundle", "Bundle of input mapping data", 0,0,sizeof(laInputMappingBundle), 0,0,1);{
            laAddSubGroup(p, "mappings","Mappings","Input mappings","la_input_mapping",0,0,0,-1,0,laget_CurrentInputMapping,0,0,0,0,offsetof(laInputMappingBundle,InputMappings),0);
            laAddSubGroup(p, "current","Current Mapping","Current input mapping","la_input_mapping",0,0,0,offsetof(laInputMappingBundle,CurrentInputMapping),laget_FirstInputMapping,0,laget_ListNext,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "toolboxes","Toolboxes","Custom toolboxes","la_toolbox",0,0,0,-1,0,laget_CurrentInputMapping,0,0,0,0,offsetof(laInputMappingBundle,Toolboxes),0);
            sp=laAddSubGroup(p, "current_toolbox","Current Toolbox","Current tool box","la_toolbox",0,0,0,offsetof(laInputMappingBundle,CurrentToolbox),laget_FirstToolbox,0,laget_ListNext,0,0,0,0,LA_UDF_REFER);
            laSubGroupDetachable(sp, laget_detached_FirstToolbox, laget_ListNext);
            ep=laAddEnumProperty(p,"toolbox_layout","Toolbox Layout","Toolbox layout",0,0,0,0,0,offsetof(laInputMappingBundle,ToolboxLayout),0,0,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "LIST", "#", "Display as list", 0, 0);
            laAddEnumItemAs(ep, "W3", "W3", "Width of 3", 3, 0);
            laAddEnumItemAs(ep, "W5", "W5", "Width of 5", 5, 0);
            laAddEnumItemAs(ep, "W8", "W8", "Width of 8", 8, 0);
            laAddEnumItemAs(ep, "W13", "W13", "Width of 13", 13, 0);
        }
        p = laAddPropertyContainer("la_input_mapping", "Input Mapping", "Input mapping data", 0,0,sizeof(laInputMapping), 0,0,2);{
            laAddStringProperty(p, "name", "Name", "The name of this mapping", 0,0,0,"Mapping", 1, offsetof(laInputMapping, Name), 0,0,0,0,LA_AS_IDENTIFIER);
            laAddSubGroup(p, "entries","Entries","Input mapping entries","la_input_mapping_entry",0,0,0,-1,0,0,0,0,0,0,offsetof(laInputMapping,Entries),0);
            laAddOperatorProperty(p,"remove","Remove","Remove this mapping","LA_remove_input_mapping",U'🞫',0);
            laAddOperatorProperty(p,"new_entry","New Entry","New mapping entry","LA_new_input_mapping_entry",'+',0);
        }
        p = laAddPropertyContainer("la_toolbox", "Toolbox", "Toolbox data", 0,0,sizeof(laInputMapping), 0,0,2);{
            laAddStringProperty(p, "name", "Name", "The name of this toolbox", 0,0,0,"Toolbox", 1, offsetof(laInputMapping, Name), 0,0,0,0,LA_AS_IDENTIFIER);
            laAddSubGroup(p, "entries","Entries","Toolbox entries","la_input_mapping_entry",0,0,0,-1,0,0,0,0,0,0,offsetof(laInputMapping,Entries),0);
            laAddOperatorProperty(p,"remove","Remove","Remove this mapping","LA_remove_toolbox",U'🞫',0);
            laAddOperatorProperty(p,"new_entry","New Entry","New toolbox entry","LA_new_input_mapping_entry",'+',0);
        }
        p = laAddPropertyContainer("la_input_mapping_entry", "Input Mapping Entry", "Input mapping entry", 0,0,sizeof(laInputMappingEntry), 0,0,1);{
            laAddIntProperty(p,"__move","Move Slider","Move Slider",LA_WIDGET_HEIGHT_ADJUSTER,0,0,0,0,0,0,0,0,0,laset_InputMappingEntryMove,0,0,0,0,0,0,0,0,LA_UDF_IGNORE);
            ep=laAddEnumProperty(p,"disabled","Enabled","Enable or disable this input mapping entry",0,0,0,0,0,offsetof(laInputMappingEntry,Disabled),0,0,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "ENABLED", "Enabled", "Entry enabled", 0, U'✓');
            laAddEnumItemAs(ep, "DISABLED", "Disabled", "Entry disabled", 1, U'✗');
            ep=laAddEnumProperty(p,"device_type","Device Type","Device type of the input event",0,0,0,0,0,offsetof(laInputMappingEntry,DeviceType),0,0,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "KEYBOARD", "Keyboard", "Keyboard input", LA_INPUT_DEVICE_KEYBOARD, U'🖮');
            laAddEnumItemAs(ep, "MOUSE", "Mouse", "Mouse input", LA_INPUT_DEVICE_MOUSE, U'🖱');
            laAddEnumItemAs(ep, "JOYSTICK", "Joystick", "Joystick input", LA_INPUT_DEVICE_JOYSTICK, U'🕹');
            laAddIntProperty(p,"joystick_device","Joystick Device","Joystick device number",0,0,0,0,0,0,0,0,offsetof(laInputMappingEntry,JoystickDevice),0,0,0,0,0,0,0,0,0,0,0);
            laAddStringProperty(p,"key","Key","Event key",0,0,0,0,1,offsetof(laInputMappingEntry,Key),0,0,laset_InputMappingEntryKeyString,0,0);
            laAddStringProperty(p,"signal","Signal","Target signal",0,0,0,0,1,offsetof(laInputMappingEntry,Signal),0,0,laset_InputMappingEntrySignalString,0,0);
            laAddSubGroup(p,"signal_selection","Signal","Signal selection","la_custom_signal",0,0,laui_IdentifierOnly,-1,laget_FirstCustomSignal,0,laget_ListNext,laset_InputMappingEntrySignal,0,0,0,LA_UDF_IGNORE);
            laAddOperatorProperty(p,"remove","Remove","Remove this entry","LA_remove_input_mapping_entry",U'🞫',0);
            laAddOperatorProperty(p,"select_signal","Select","Select signal","LA_input_mapping_entry_select_signal",0,0);
            laAddOperatorProperty(p,"select_operator","Select","Select operator","LA_input_mapping_entry_select_operator",0,0);
            laAddOperatorProperty(p,"select_key","Select Key","Select key","LA_input_mapping_entry_select_key",0,0);
            laAddOperatorProperty(p,"reset","Reset","Reset entry","LA_reset_input_mapping_fields",0,0);
            laAddSubGroup(p,"parent","Parent","Parent group","la_input_mapping",0,0,0,offsetof(laInputMappingEntry,Parent),0,0,0,0,0,0,0,LA_UDF_REFER);
            ep=laAddEnumProperty(p,"use_operator","Device Type","Device type of the input event",0,0,0,0,0,offsetof(laInputMappingEntry,UseOperator),0,0,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "SIGNAL", "Signal", "Send signal upon input", 0, U'⚟');
            laAddEnumItemAs(ep, "OPERATOR", "Operator", "Run operator upon input", 1, U'🏃');
            laAddStringProperty(p,"operator","Operator","Operator to call",0,0,0,0,1,offsetof(laInputMappingEntry,Operator),0,0,laset_InputMappingEntryOperatorString,0,0);
            laAddStringProperty(p,"operator_name","Operator Name","Target operator name",0,0,0,0,1,offsetof(laInputMappingEntry,OperatorName),0,0,0,0,LA_READ_ONLY);
            laAddStringProperty(p,"operator_arguments","Arguments","Extra arguments for the operator",0,0,0,0,1,offsetof(laInputMappingEntry,OperatorArguments),0,0,0,0,0);
            ep=laAddEnumProperty(p,"operator_base","Operator Base","Operator parent context base",0,0,0,0,0,offsetof(laInputMappingEntry,OperatorBase),0,0,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "NONE", "None", "Run operator directly", 0, 0);
            laAddEnumItemAs(ep, "WIDGET", "Widget", "Run operator with extra reference of current UI", LA_KM_SEL_UI_EXTRA, 0);
            laAddEnumItemAs(ep, "PANEL", "Panel", "Run operator with extra reference of current panel", LA_KM_SEL_PANEL, 0);
            laAddOperatorProperty(p,"run_entry","Run Entry","Run this entry","LA_run_toolbox_entry",0,0);
        }
        p = laAddPropertyContainer("la_custom_signal", "Custom Signal", "Custom signal", 0,0,sizeof(laCustomSignal), 0,0,1);{
            laAddStringProperty(p, "name", "Name", "The name of this mapping", 0,0,0,"Mapping", 1, offsetof(laCustomSignal, Name), 0,0,0,0,LA_AS_IDENTIFIER);
        }
        p = laAddPropertyContainer("la_operator_type", "Operator Type", "Operator type", 0,laui_OperatorTypeEntry,sizeof(laOperatorType), 0,0,1);{
            laAddStringProperty(p, "identifier", "Identifier", "The identifier of the operator", 0,0,0, "none", 0, offsetof(laOperatorType, Identifier), 0,0,0,0,LA_AS_IDENTIFIER|LA_READ_ONLY);
            laAddStringProperty(p, "name", "Name", "The name of operator", 0,0,0, "none", 0, offsetof(laOperatorType, Name), 0,0,0,0,LA_READ_ONLY);
            laAddStringProperty(p, "description", "Description", "Description of the operator", 0,0,0, "none", 0, offsetof(laOperatorType, Description), 0,0,0,0,LA_READ_ONLY);
        }

        p = laAddPropertyContainer("la_translation_language", "Language", "Translation language pack", 0,0,sizeof(laTranslationNode), 0,0,1);{
            laAddStringProperty(p, "name", "Name", "The name of this language", 0,0,0,"Unknown", 1, offsetof(laTranslationNode, LanguageName), 0,0,0,0,LA_AS_IDENTIFIER);
        }

        p = laAddPropertyContainer("la_extension_type", "Extension Type", "File extension and its matching type for file filtering", 0,0,sizeof(laExtensionType), 0,0,0);{
            laAddStringProperty(p, "extension", "Extension", "File extension string", LA_WIDGET_STRING_PLAIN, 0,0,0,0,offsetof(laExtensionType, Extension), 0,0,0,0,LA_AS_IDENTIFIER|LA_READ_ONLY);
        }

        // UI WINDOW ========================================================================================

        p = laAddPropertyContainer("ui_window", "SYSWINDOW Node", "Property container for a system syswindow", 0,0, sizeof(laWindow), lapost_Window, lapostim_Window,1);{
            laAddStringProperty(p, "title", "Title", "The title/name of a panel", 0,0,0,0,1, offsetof(laWindow, Title), 0,0,0,0,LA_AS_IDENTIFIER);
            laAddSubGroup(p, "layouts", "Layouts", "Layout List Of The Whole SYSWINDOW", "ui_layout",0,0,0,offsetof(laWindow, CurrentLayout), laget_WindowFirstLayout, laget_WindowActiveLayout,laget_ListNext, 0,0,laset_WindowActiveLayout, offsetof(laWindow, Layouts), 0);
            _LA_PROP_PANEL = laAddSubGroup(p, "panels", "Panels", "Panel list of this window", "ui_panel",0,0,0,-1,laget_SavePanel,0,lagetnext_SavePanel,0,0,0,offsetof(laWindow, Panels), 0);
            la_UDFAppendSharedTypePointer("_LA_PROP_PANEL", _LA_PROP_PANEL);
            laAddSubGroup(p, "maximized_block", "Maximized Block", "Maximized block in this window", "ui_block",0,0,0,offsetof(laWindow, MaximizedBlock), 0,0,0,0,0,0,0,LA_UDF_REFER|LA_UDF_IGNORE);
            laAddSubGroup(p, "maximized_ui", "Maximized Ui", "Maximized ui in this window", "ui_item",0,0,0,offsetof(laWindow, MaximizedUi), 0,0,0,0,0,0,0,LA_UDF_REFER|LA_UDF_IGNORE);
            laAddSubGroup(p, "panels_hidden", "Hidden Panels", "Hidden panels of this window", "ui_panel",0,0,0,-1, laget_FirstHiddenPanel, 0,laget_NextHiddenPanel, laset_WindowHiddenPanel, 0,0,0,LA_UDF_IGNORE);
            laAddIntProperty(p, "position", "Position", "The Position Of A SYSWINDOW", 0,"X,Y", "px", 0,0,1, 0,0,offsetof(laWindow, X), 0,0,2, 0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "size", "Size", "The Size Of A SYSWINDOW", 0,"W,H", "px", 0,0,0,0,0,offsetof(laWindow, W), 0,0,2, 0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "client_size", "Client Area Size", "Display Canvans Size Of A SYSWINDOW", 0,"W,H", "px", 0,0,1, 0,0,offsetof(laWindow, CW), 0,0,2, 0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddStringProperty(p, "operator_hints", "Operator Hints", "Operator hints if there's any", LA_WIDGET_STRING_PLAIN, 0,0,0,1, offsetof(laWindow, OperatorHints), 0,0,0,0,LA_READ_ONLY|LA_UDF_IGNORE);
            ep = laAddEnumProperty(p, "output_color_space", "Output Color Space", "Output color space of this window, set this to the monitor's color space to get accurate result", 0,0,0,0,0,offsetof(laWindow, OutputColorSpace), 0,laset_WindowColorSpace, 0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "SRGB", "sRGB", "Standard sRGB diplay", TNS_COLOR_SPACE_SRGB, 0);
                laAddEnumItemAs(ep, "CLAY", "Clay", "Clay color space (AdobeRGB 1998 compatible)", TNS_COLOR_SPACE_CLAY, 0);
                laAddEnumItemAs(ep, "D65_P3", "D65 P3", "D65 P3 color space", TNS_COLOR_SPACE_D65_P3, 0);
            }
            ep = laAddEnumProperty(p, "output_proofing", "Proofing", "Show soft proofing in this window", 0,0,0,0,0,offsetof(laWindow, OutputProofing), 0,laset_WindowOutputProofing, 0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "NONE", "None", "Not doing soft proofing", 0, U'🖵');
                laAddEnumItemAs(ep, "PROOFING", "Proofing", "Show soft proofing", 1, U'🖶');
            }
            ep = laAddEnumProperty(p, "output_show_overflow", "Show Overflow", "Show stripes on overflowing colors", LA_WIDGET_ENUM_HIGHLIGHT, 0,0,0,0,offsetof(laWindow, OutputShowStripes), 0,laset_WindowShowStripes, 0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "NONE", "None", "Don't show overflow", 0,0);
                laAddEnumItemAs(ep, "STRIPES", "Stripes", "Show overflowing colors as stripes", 1, 0);
            }
            ep = laAddEnumProperty(p, "use_composing", "Use Composing", "Compose and adjust window colors, best for outdoor lighting", LA_WIDGET_ENUM_HIGHLIGHT,0,0,0,0,offsetof(laWindow, UseComposing), 0,laset_WindowUseComposing, 0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "NONE", "None", "Do not use composing", 0, 0);
                laAddEnumItemAs(ep, "SUN", "Sun", "Compose with outdoor sunlight options", 1, 0);
            }
            laAddFloatProperty(p, "composing_gamma", "Composing Gamma", "Composing gamma", 0,0,0,2,0.25,0.05,1.0,0,offsetof(laWindow, ComposingGamma), 0,laset_WindowComposingGamma,0,0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "composing_blackpoint", "Composing Blackpoint", "Composing blackpoint", 0,0,0,0.5,0,0.05,0.0,0,offsetof(laWindow, ComposingBlackpoint), 0,laset_WindowComposingBlackpoint,0,0,0,0,0,0,0,0,0);

            laAddIntProperty(p, "is_fullscreen", "Is Fullscreen", "Is the window fullscreen", 0,0,0,0,0,0,0,0,offsetof(laWindow, IsFullScreen),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        }

        p = laAddPropertyContainer("la_screen","Screen","Screen detected from system",0,0,sizeof(laScreen),0,0,1);{
            laAddStringProperty(p,"name","Name","Name of the screen",LA_WIDGET_STRING_PLAIN,0,0,0,1,offsetof(laScreen,Name),0,0,0,0,LA_READ_ONLY|LA_AS_IDENTIFIER);
            laAddStringProperty(p,"description","Description","Desctiption of this screen",LA_WIDGET_STRING_MONO_PLAIN,0,0,0,1,offsetof(laScreen,Description),0,0,0,0,LA_READ_ONLY);
            ep = laAddEnumProperty(p, "color_space", "Output Color Space", "Hardware color space of this screen ", 0,0,0,0,0,offsetof(laScreen,ColorSpace), 0,laset_ScreenColorSpace, 0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "SRGB", "sRGB", "Standard sRGB diplay", TNS_COLOR_SPACE_SRGB, 0);
                laAddEnumItemAs(ep, "CLAY", "Clay", "Clay color space (AdobeRGB 1998 compatible)", TNS_COLOR_SPACE_CLAY, 0);
                laAddEnumItemAs(ep, "D65_P3", "D65 P3", "D65 P3 color space", TNS_COLOR_SPACE_D65_P3, 0);
            }
            laAddOperatorProperty(p,"remove","Remove","Remove this screen config","LA_remove_screen_config",0,0);
        }

        // UI LAYOUT ========================================================================================

        p = laAddPropertyContainer("ui_block", "Ui Block", "Property container for single ui block", 0,0, sizeof(laBlock), lapost_Block,0,1);{
            laAddIntProperty(p, "location", "Location", "Block Location", 0,"X,Y", "px", 0,0,1, 0,0,offsetof(laBlock, X), 0,0,2, 0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "size", "Size", "Block Size", 0,"W,H", "px", 0,0,1, 0,0,offsetof(laBlock, W), 0,0,2, 0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddFloatProperty(p, "split_ratio", "Split Ratio", "Split ratio on two subs", 0,0,0,1, 0,0.05, 0.5, 0,offsetof(laBlock, SplitRatio), 0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "vertical", "Vertical", "Is vertival or not", 0,0,0,1, 0,1, 0,0,offsetof(laBlock, Vertical), 0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "folded", "Folded", "Block is folded",0,0,0,1,0,0,0,0,offsetof(laBlock, Folded),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddSubGroup(p, "panel_list", "Panel List", "Panels Under This Block", "ui_panel",0,0,0,-1, 0,0,0,0,0,0,offsetof(laBlock, Panels), 0);
            laAddSubGroup(p, "current_panel", "Current Panel", "Current Selected Tab Panel", "ui_panel",0,0,0,offsetof(laBlock, CurrentPanel), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "sub1", "Sub 1", "Sub Block 1", "ui_block",0,0,0,offsetof(laBlock, B1), 0,0,0,0,0,0,0,LA_UDF_SINGLE);
            laAddSubGroup(p, "sub2", "Sub 2", "Sub Block 2", "ui_block",0,0,0,offsetof(laBlock, B2), 0,0,0,0,0,0,0,LA_UDF_SINGLE);
            laAddSubGroup(p, "parent", "Parent", "Parent block reference", "ui_block",0,0,0,offsetof(laBlock, parent), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddOperatorProperty(p, "fold", "Fold", "Fold the title bar", "LA_block_fold_title", 0,0);
            laAddOperatorProperty(p, "maximize", "Maximize", "Maximize the block", "LA_block_maximize", 0,0);
        }

        p = laAddPropertyContainer("ui_layout", "Layout Node", "Property container for single layout", 0,laui_LayoutListItem, sizeof(laLayout), 0,0,1);{
            laAddStringProperty(p, "title", "Title", "The title/name of a panel", 0,0,0,0,1, offsetof(laLayout, ID), 0,0,laset_LayoutTitle,0,LA_AS_IDENTIFIER);
            _LA_PROP_BLOCK = laAddSubGroup(p, "root_block", "Root Block", "Root Block For Panel Docking", "ui_block",0,0,0,offsetof(laLayout, FirstBlock), 0,0,0,0,0,0,0,LA_UDF_SINGLE);
            la_UDFAppendSharedTypePointer("_LA_PROP_BLOCK", _LA_PROP_BLOCK);
        }

        // UI PANEL =========================================================================================

        p = laAddPropertyContainer("ui_panel", "Panel Node", "Property container for general panels", 0,laui_PanelListItem, sizeof(laPanel), lapost_Panel, 0,1);{
            laAddStringProperty(p, "title", "Title", "The title/name of a panel", 0,0,0,0,1, offsetof(laPanel, Title), 0,0,laset_PanelTitle, laread_PanelTitle,LA_AS_IDENTIFIER|LA_TRANSLATE);
            laAddIntProperty(p, "position", "Position", "The Position Of A Panel", 0,"X,Y", "px", 0,0,1, 0,0,offsetof(laPanel, X), 0,0,2, 0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "size", "Size", "The Size Of A Panel", 0,"Width,Height", "px", 0,0,1, 0,0,offsetof(laPanel, W), 0,0,2, 0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "real_xywh", "Real Placemnt", "Placement data in structure", 0,"X,Y,W,H", "px", 0,0,1, 0,0,offsetof(laPanel, TX), 0,0,4, 0,0,0,0,0,0,0,LA_READ_ONLY);
            ep = laAddEnumProperty(p, "snap_enable", "Snap Enable", "Enable Snapping To Edges", 0,"Left,Right,Top,Bottom", 0,0,0,0,0,0,4, 0,laset_PanelSnapEnable, laget_PanelSnapEnable, 0,0,0,0);{
                laAddEnumItem(ep, "no", "No Snap", "Not Snapped to edge", 0);
                laAddEnumItem(ep, "yes", "Snap", "Snapped to edge", U'🞉');
            } //don't swap order with the one below
            laAddIntProperty(p, "snap", "Snap Distance", "Snapping distance to edges", 0,"Left,Right,Top,Bottom", "px", 0,0,1, 0,0,offsetof(laPanel, SL), 0,0,4, 0,laset_PanelSnapDistance, 0,0,0, laread_PanelSnapDistance,0,0);
            laAddIntProperty(p, "show", "Show", "The panel is shown or not", 0,0,0,0,0,1, 0,0,offsetof(laPanel, Show), 0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "mode", "Mode", "Normal/floating/static/modal etc.", 0,0,0,0,0,1, 0,0,offsetof(laPanel, Mode), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            ep = laAddEnumProperty(p, "is_menu_panel", "Is Menu Panel", "Is menu panel", 0,0,0,0,0,offsetof(laPanel, IsMenuPanel), 0,0,0,0,0,0,0,0,0,0);{
                ep->ElementBytes = 1;
                laAddEnumItem(ep, "false", "False", "Not A Menu Panel", U'🞫');
                laAddEnumItem(ep, "true", "IsTrue", "Is A Menu Panel", U'🗩');
            }
            laAddOperatorProperty(p, "hide", "Hide", "Hide this panel", "LA_hide_panel", U'🗕', 0);
            laAddOperatorProperty(p, "dock", "Dock", "Dock this panel", "LA_dock_panel", U'🗖', 0);
            laAddOperatorProperty(p, "close", "Close", "Close this panel", "LA_block_close_panel", U'🞫', 0);
            //laAddSubGroup(p, "Detached Props", "Detached Props", "detached_props",0,0,0,0,0,0,0,0,0,0,0,offsetof(laPanel, PropLinkContainer->Props), 0);
            laAddSubGroup(p, "uil","Ui List", "Panel Main Ui List", "ui_list",0,0,0,offsetof(laPanel, UI), 0,0,0,0,0,0,0,LA_UDF_IGNORE);
            laAddSubGroup(p, "title_uil","Title Ui List", "Panel Title Ui List", "ui_list",0,0,0, offsetof(laPanel, TitleBar), 0,0,0,0,0,0,0,LA_UDF_IGNORE);
            laAddSubGroup(p, "pp","Prop Pack", "Panel Base With Own Instance", "property_package",0,0,0,offsetof(laPanel, PP), 0,0,0,0,0,0,0,LA_UDF_REFER|LA_UDF_IGNORE);
            laAddSubGroup(p, "fake_pp","Fake Prop Pack", "Fake Prop Pack", "property_package",0,0,0,offsetof(laPanel, PropLinkPP), 0,0,0,0,0,0,0,LA_UDF_REFER|LA_UDF_IGNORE);
            laAddSubGroup(p, "parent_block","Parent Block", "The Block That Directly Links This Panel In", "ui_block",0,0,0,offsetof(laPanel, Block), 0,0,0,0,0,0,0,LA_UDF_REFER|LA_UDF_IGNORE);
            laAddSubGroup(p, "template","Template", "Panel template", "panel_template",0,0,0,offsetof(laPanel, PanelTemplate), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "block", "Block", "Block reference of this panel", "ui_block",0,0,0,offsetof(laPanel, Block), 0,0,0,0,0,0,0,LA_UDF_REFER);
            //laAddStringProperty(p, "_toolbox_name", "Toolbox Name", "Toolbox name",0,0,0,0,1,offsetof(laPanel,_ToolboxName),0,laget_PanelToolboxName,0,0,LA_READ_ONLY);
            //laAddIntProperty(p, "_toolbox_layout", "Toolbox Layout", "Toolbox layout", 0,0,0,0,0,0,0,0,offsetof(laPanel,_ToolboxLayout),0,0,0,0,laget_PanelToolboxLayout, 0,0,0,0,0,LA_READ_ONLY);
        }

        // UI ITEM ==========================================================================================

        p = laAddPropertyContainer("ui_list", "Ui List", "Property container for ui list sub type", U'⮑', 0,sizeof(laUiList), 0,0,0);{
            laAddStringProperty(p, "tab_name", "Tab Name", "The name of a tab", 0,0,0,0,0,0,0,0,0,0,LA_AS_IDENTIFIER);
            _LA_PROP_UI_ITEM = laAddSubGroup(p, "ui_items", "Ui Items", "Ui Items Listed In Ui List", "ui_item",0,0,0,-1, laget_FirstUiItem, 0,laget_ListNext, 0,0,0,offsetof(laUiList, UiItems), 0);
            la_UDFAppendSharedTypePointer("_LA_PROP_UI_ITEM", _LA_PROP_UI_ITEM);
            laAddSubGroup(p, "column_items", "Column Items", "Ui Items Listed In Ui List", "ui_column",0,0,0,-1, laget_FirstColumnItem, 0,0,0,0,0,offsetof(laUiList, Columns), 0);
            laAddIntProperty(p, "pan", "Pan", "Cavans Panning Pixels", 0,"Pan X,Pan Y", "px", 10000,0,1, 0,0,offsetof(laUiList, PanX), 0,0,2, 0,0,0,0,0,0,0,0);
            laAddSubGroup(p, "instance", "Instance", "Collection Instance For Every Item", "ui_instance",0,0,0,offsetof(laUiList, Instance), 0,0,0,0,0,0,0,LA_UDF_REFER);

            laAddIntProperty(p, "height_coeff", "Height Coefficiency", "How many rows a ui should take or reserve", 0,0,"Rows", 0,0,1, 0,0,offsetof(laUiList, HeightCoeff), 0,0,0,0,0,0,0,0,0,0,0)
                ->ElementBytes = 2;
        }
        p = laAddPropertyContainer("ui_instance", "Instance", "Uilist instance entry", U'🗇', 0,sizeof(laColumn), 0,0,0);{
            //nothing needed
        }
        p = laAddPropertyContainer("ui_column", "Ui Column", "A column handles the aligning of ui items", U'◫', laui_IdentifierOnly, sizeof(laColumn), 0,0,0);{
            laAddFloatProperty(p, "split_at", "Split At", "Split width percentage from left", 0,0,0,1, 0,0.01, 0.5, 0,0,laget_ColumnSP, laset_ColumnSP, 0,0,0,0,0,0,0,0,LA_UDF_IGNORE);
            laAddFloatProperty(p, "real_split", "Real Split", "Float value split pos in real strucutre", 0,0,0,1, 0,0.01, 0.5, 0,offsetof(laColumn, SP), 0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "snap_width", "Snap Width", "Snap width of a column", 0,0,0,200,20,1, 30,0,0,laget_ColumnSnap, laset_ColumnSnap, 0,0,0,0,0,0,0,0,LA_UDF_IGNORE);
            laAddIntProperty(p, "real_snap", "Real Snap", "Int snap value in real structure", 0,0,0,0,0,0,0,0,offsetof(laColumn, MaxW), 0,0,0,0,0,0,0,0,0,0,0);
            ep = laAddEnumProperty(p, "snap_state", "Sub Snap State", "How to snap sub columns", 0,0,0,0,0,0,laget_SnapState, laset_SnapState, 0,0,0,0,0,0,0,LA_UDF_IGNORE);{
                laAddEnumItem(ep, "none", "None", "No Snapping", 0);
                laAddEnumItem(ep, "left", "Left", "Snap At Left", U'⮄');
                laAddEnumItem(ep, "right", "Right", "Snap At Right", U'⮆');
            }
            laAddSubGroup(p, "left", "Left Sub Column", "Left Sub Column", "ui_column",0,0,0,offsetof(laColumn, LS), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "right", "Right Sub Column", "Right Sub Column", "ui_column",0,0,0,offsetof(laColumn, RS), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "self", "Self", "Self Display", "ui_column",0,LA_WIDGET_COLUMN_VIEWER, 0,-1, laget_Self, 0,0,0,0,0,0,LA_UDF_REFER | LA_AS_IDENTIFIER);
        }
        p = laAddPropertyContainer("panel_template", "Panel Template", "Panel template for creating new panels", 0,0,sizeof(laUiTemplate), 0,0,0);{
            laAddStringProperty(p, "identifier", "Identifier", "Identifier of this template", 0,0,0,0,1, offsetof(laUiTemplate, Identifier), 0,0,0,0,LA_AS_IDENTIFIER);
            laAddStringProperty(p, "title", "Title", "Panel title", 0,0,0,0,1, offsetof(laUiTemplate, Title), 0,0,0,0,LA_TRANSLATE);
            laAddIntProperty(p, "define_func", "Define Func", "Define function distinguish(internal only)", 0,0,0,0,0,0,0,0,offsetof(laUiTemplate, Define), 0,0,0,0,0,0,0,0,0,0,LA_UDF_IGNORE|LA_READ_ONLY);
        }

        p = laAddPropertyContainer("ui_item", "Ui Item", "Property container for ui items", 0,0,sizeof(laUiItem), lapost_UiItem, lapostim_UiItem, 0);{
            laAddIntProperty(p, "location", "Location", "The Ui's Location In A UiList(Prop For Live Edit Only)", 0,"Up", "Down", 0,0,1, 0,0,0,0,0,0,0,0,0,0,0,0,0,LA_AS_IDENTIFIER);
            laAddIntProperty(p, "state", "State", "The ui's internal state", 0,0,0,0,0,1, 0,0,offsetof(laUiItem, State), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "column_layout", "Column Layout", "The ui's column layout", 0,0,0,10,-10,1, 0,0,offsetof(laUiItem, Extent), 0,0,0,0,0,0,0,0,0,0,0)
                ->ElementBytes = sizeof(short);
            laAddStringProperty(p, "path", "Path", "Data path", 0,0,0,0,0,0,0,laget_UiDataPath, 0,laread_UiDataPath,LA_READ_ONLY);
            laAddStringProperty(p, "actuator_id", "Operator ID", "Pure operator with no 'this' pointer", 0,0,0,0,0,0,0,laget_UiOperatorID, 0,laread_UiOperatorID,LA_READ_ONLY);
            laAddSubGroup(p, "pp", "Prop Pack", "Property Package In ui->PP Entry", "property_package",0,0,0,offsetof(laUiItem, PP), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "extra_pp", "Extra Prop Pack", "Property Package In ui->ExtraPP Entry", "property_package",0,0,0,offsetof(laUiItem, ExtraPP), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "go", "Prop Step Go", "Go Entry (For Determin If This Is A Prop Or Not)", "property_step",0,0,0,offsetof(laUiItem, PP.Go), 0,0,0,0,0,0,0,LA_UDF_REFER | LA_UDF_IGNORE);
            laAddSubGroup(p, "raw_this", "Prop Raw This", "ui->PP.RawThis Entry", "property_package",0,0,0,offsetof(laUiItem, PP.RawThis), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "current_page", "Current Page", "Current Page In Sub Ui List", "ui_list",0,0,0,offsetof(laUiItem, Page), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "internal_type", "Ui Internal Type", "Ui Internal Type", "ui_type",0,0,0,offsetof(laUiItem, Type), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "on_column", "On Column", "Ui On Which Column", "ui_column",0,0,0,offsetof(laUiItem, C), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "sub", "Sub UI", "Sub Ui Lists For Tabs And Collection", "ui_list",0,0,0,-1, 0,0,0,0,0,0,offsetof(laUiItem, Subs), 0);
            laAddStringProperty(p, "extra_args", "Extra Arguments", "Extra arguments for this ui item", 0,0,0,0,0,0,0,0,0,0,0);
            laAddStringProperty(p, "display", "Display", "Display string for label", 0,0,0,0,0,0,0,0,0,0,0);
            laAddOperatorProperty(p, "maximize", "Maximize", "Maximize this UI item", "LA_canvas_ui_maximize", 0,0);
        }

        p = laAddPropertyContainer("ui_type", "Ui Type", "Ui type descriptor", 0,laui_IdentifierOnly, sizeof(laUiType), 0,0,0);{
            laAddStringProperty(p, "identifier", "Identifier", "Identifier of this ui type", 0,0,0,0,0,offsetof(laUiType, Identifier), 0,0,0,0,LA_AS_IDENTIFIER|LA_READ_ONLY);
        }

        // NODE ================================================================================================

        p = laAddPropertyContainer("la_out_socket", "Output Socket", "Output socket for nodes", 0,0,sizeof(laNodeOutSocket), 0,0,1);{
            laAddStringProperty(p, "label", "Label", "Socket's label", 0,0,0,0,1, offsetof(laNodeOutSocket, Label), 0,0,0,0,LA_AS_IDENTIFIER);
            laAddIntProperty(p, "data_type", "Data type", "User defined data type", 0,0,0,0,0,0,0,0,offsetof(laNodeOutSocket, DataType), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "array_length", "Array Length", "Array length of data", 0,0,0,0,0,0,0,0,offsetof(laNodeOutSocket, ArrLen), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddSubGroup(p, "parent", "Parent", "Parent node", "la_base_node",0,0,0,offsetof(laNodeOutSocket, Parent), 0,0,0,0,0,0,0,LA_UDF_REFER);
        } LA_PC_SOCKET_OUT = p;
        p = laAddPropertyContainer("la_in_socket", "Input Socket", "Input socket for nodest", 0,0,sizeof(laNodeInSocket), 0,0,1);{
            laAddStringProperty(p, "label", "Label", "Socket's label", 0,0,0,0,1, offsetof(laNodeInSocket, Label), 0,0,0,0,LA_AS_IDENTIFIER);
            sp=laAddSubGroup(p, "source", "Source", "Source socket", "la_out_socket",0,0,offsetof(laNodeInSocket, Source), 0,0,0,0,0,0,0,0,LA_UDF_REFER);
            LA_PROP_SOCKET_SOURCE=sp;
            laAddIntProperty(p, "data_type", "Data type", "User defined data type", 0,0,0,0,0,0,0,0,offsetof(laNodeInSocket, DataType), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "color_id", "Color ID", "Color id of the source wire", 0,0,0,0,0,0,0,0,offsetof(laNodeInSocket, ColorId), 0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "array_length", "Array Length", "Array length of data", 0,0,0,0,0,0,0,0,offsetof(laNodeInSocket, ArrLen), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        } LA_PC_SOCKET_IN = p;
        laPropContainerExtraFunctions(p,0,0,latouched_NodeInSocket,0,0);

        p = laAddPropertyContainer("la_value_mapper", "Value Mapper", "Value mapper", 0,0,sizeof(laValueMapper), 0,0,1);{
            laAddSubGroup(p, "points", "Points", "Points inside the mapper", "la_value_mapper_point",0,0,-1, 0,0,0,0,0,0,0,offsetof(laValueMapper, Points), 0);
            laAddFloatProperty(p, "in_range", "Input Range", "Input range from 0 to 1", 0,"Min,Max", 0,0,0,0,0,0,offsetof(laValueMapper, InRange), 0,0,2, 0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "out_range", "Output Range", "Output range from 0 to 1", 0,"Min,Max", 0,0,0,0,0,0,offsetof(laValueMapper, OutRange), 0,0,2, 0,0,0,0,0,0,0,0);
        } LA_PC_MAPPER=p;
        p = laAddPropertyContainer("la_value_mapper_point", "Value Mapper", "Value mapper", 0,0,sizeof(laValueMapperPoint), 0,0,1);{
            laAddFloatProperty(p, "position", "Position", "XY Position ranging from 0 to 1", 0,"X,Y", 0,0,0,0,0,0,offsetof(laValueMapperPoint, x), 0,0,2, 0,0,0,0,0,0,0,0);
        }
        

        // PROPERTIES ==========================================================================================

        p = laAddPropertyContainer("property_item", "Property Item", "Property item for data types like int/float/enum/string/subtype", U'🔌', 0,sizeof(laProp), 0,0,1);{
            laAddStringProperty(p, "identifier", "Identifier", "Property unique identifier", LA_WIDGET_STRING_PLAIN, 0,0,0,0,0,0,laget_PropertyIdentifier, 0,0,LA_AS_IDENTIFIER|LA_READ_ONLY);
            laAddStringProperty(p, "name", "Name", "Property display name", 0,0,0,0,0,offsetof(laProp, Name), 0,0,0,0,LA_READ_ONLY);
            laAddStringProperty(p, "description", "Description", "Property description", 0,0,0,0,0,offsetof(laProp, Description), 0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "prop_type", "Property Type", "Property type(like int/float/enunm/sub)", 0,0,0,0,0,0,0,0,offsetof(laProp, PropertyType), 0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p, "icon_id", "Icon Id", "Icon id for current container", 0,0,0,0,0,0,0,0,offsetof(laProp, IconID), 0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "sub_icon_id", "Icon Id", "Icon id for current container", 0,0,0,0,0,0,0,0,0,laget_PropertySubContainerIconID, 0,0,0,0,0,0,0,0,0,0);
            laAddSubGroup(p, "sub", "Sub Type", "Sub Type Property Container", "property_container",0,0,0,offsetof(laProp, SubProp), 0,0,0,0,0,0,0,LA_UDF_REFER);
        }

        p = laAddPropertyContainer("property_container", "Property Container", "Property container for some property items", U'🔌', 0,sizeof(laPropContainer), 0,0,1);{
            laAddStringProperty(p, "identifier", "Identifier", "Property unique identifier", LA_WIDGET_STRING_PLAIN, 0,0,0,0,offsetof(laPropContainer, Identifier), 0,0,0,0,LA_AS_IDENTIFIER | LA_UDF_IGNORE|LA_READ_ONLY);
            laAddStringProperty(p, "name", "Name", "Property display name", 0,0,0,0,0,offsetof(laPropContainer, Name), 0,0,0,0,LA_UDF_IGNORE|LA_READ_ONLY);
            laAddStringProperty(p, "description", "Description", "Property description", 0,0,0,0,0,offsetof(laPropContainer, Name), 0,0,0,0,LA_UDF_IGNORE|LA_READ_ONLY);
            laAddIntProperty(p, "icon_id", "Icon Id", "Icon id for current container", 0,0,0,0,0,0,0,0,offsetof(laPropContainer, IconID), 0,0,0,0,0,0,0,0,0,0,0);
            laAddSubGroup(p, "properties", "Properties", "Single Property", "property_item", laget_PropertyNodeType, 0,0,-1, laget_PropertyItemFirst, laget_PropertyItemFirst, laget_PropertyItemNext, 0,0,0,0,LA_UDF_IGNORE);
            _LA_PROP_FAILED_ITEM = laAddSubGroup(p, "failed_nodes", "Failed Nodes", "Used To Store UDF Failed Nodes", "property_trash_item",0,0,0,-1, 0,0,0,0,0,0,offsetof(laPropContainer, FailedNodes), LA_UDF_IGNORE);
            _LA_PROP_TRASH_ITEM = laAddSubGroup(p, "trash_bin", "Trash Bin", "Used To Store Unlinked Items", "property_trash_item",0,0,0,-1, 0,0,0,0,0,0,offsetof(laPropContainer, TrashBin), LA_UDF_IGNORE);
            laAddOperatorProperty(p, "restore_all", "Restore All", "Restore All Trash Items Or Failed Nodes To A User Selected Linkage", "LA_sub_restore_data_block", U'⭯', 0);
        }

        p = laAddPropertyContainer("property_trash_item", "Trash Item", "Single trash item", 0,0,0,0,0,0);{
            laAddIntProperty(p, "instance_int", "Instance", "Memory address of this data block (int represent)", 0,0,0,0,0,0,0,0,0,laget_TrashItemInstance, 0,0,0,0,0,0,0,0,0,LA_AS_IDENTIFIER|LA_READ_ONLY);
            laAddSubGroup(p, "instance", "Instance", "Single Memory Address Of This Data Block", "property_trash_item",0,0,0,-1, 0,laget_TrashItemInstance, 0,0,0,0,0,LA_UDF_REFER | LA_UDF_IGNORE);
            laAddOperatorProperty(p, "restore", "Restore", "Restore Data Block To A User Selected Linkage", "LA_sub_restore_data_block", U'⭯', 0);
        }

        p = laAddPropertyContainer("property_package", "Property Package", "Property package for data access (mainly ui)", U'🔌', 0,sizeof(laPropPack), 0,0,0);{
            laAddSubGroup(p, "last_step", "Last Step", "Last Prop Step(Segment)", "property_step",0,0,0,offsetof(laPropPack, LastPs), 0,0,0,0,0,0,0,LA_UDF_REFER | LA_UDF_IGNORE);
        }
        p = laAddPropertyContainer("property_step", "Property Step", "Property segment item", 0,0,sizeof(laPropStep), 0,0,0);{
            laAddSubGroup(p, "property", "Property", "Property Reference", "property_item",0,0,0,offsetof(laPropStep, p), 0,0,0,0,0,0,0,LA_UDF_REFER);
        }
        p = laAddPropertyContainer("detached_prop", "Detached Prop", "Detached prop", U'🔌', 0,sizeof(laSubProp) + 48, lapost_DetachedProp, 0,0);{
            laAddSubGroup(p, "raw_this", "Raw This", "Raw This Pointer", "property_package",0,0,0,offsetof(laProp, DetachedPP.RawThis), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddStringProperty(p, "path", "Path", "Data path", 0,0,0,0,0,0,0,laget_DetachedPropPath, 0,laread_DetachedPropPath, 0);
            laAddStringProperty(p, "rename", "Rename", "Rename", 0,0,0,0,0,offsetof(laProp, Identifier), 0,0,0,laread_DetachedPropRename, 0);
        }


        p = laAddPropertyContainer("la_animation", "Animation", "Animation data",0,0,sizeof(laAnimation),0,0,1);{
            sp = laAddSubGroup(p, "action_holders", "Action Holders", "All action holders","la_animation_action_holder",0,0,0,-1,0,0,0,0,0,0,offsetof(laAnimation,ActionHolders),LA_UDF_IGNORE);
            laSubGroupExtraFunctions(sp,0,0,0,0,laget_AnimationActionHolderCategory);

            laAddSubGroup(p, "current_action", "Current Action", "Current action","la_animation_action",0,0,0,offsetof(laAnimation,CurrentAction),0,0,0,0,0,0,0,LA_UDF_REFER|LA_READ_ONLY);
            
            laAddFloatProperty(p, "play_head","Play Head","Animation viewer global playhead",0,0,"s",0,0,0.01,0,0,offsetof(laAnimation,PlayHead),0,laset_AnimationPlayHead,0,0,0,0,0,0,0,0,0);
            ep=laAddEnumProperty(p, "play_status", "Play Status", "Animation viewer global play status", 0,0,0,0,0,offsetof(laAnimation, PlayStatus),0,0,0,0,0,0,0,0,0,0);
            laAddEnumItemAs(ep, "PAUSED", "Paused", "Animation is playing", LA_ANIMATION_STATUS_PAUSED, U'⏸');
            laAddEnumItemAs(ep, "PLAY_FWD", "Playing", "File data is untouched", LA_ANIMATION_STATUS_PLAY_FWD,U'▶');
            laAddEnumItemAs(ep, "PLAY_REV", "Reversing", "File data is untouched", LA_ANIMATION_STATUS_PLAY_REV,U'◀');
        }
        p = laAddPropertyContainer("la_animation_action", "Action", "Animation action",0,0,sizeof(laAction),0,0,1);{
            laAddStringProperty(p,"name","Name","Name of the action",0,0,0,0,1,offsetof(laAction,Name),0,0,0,0,LA_AS_IDENTIFIER);
            laAddStringProperty(p,"container","Container","Container of the action parent",0,0,0,0,0,0,0,laget_ActionContainer,0,laread_ActionContainer,LA_READ_ONLY);
            laAddSubGroup(p, "holder", "Holder", "Holder data block", "any_pointer",0,0,0,offsetof(laAction, HolderInstance), 0,0,0,0,0,0,0,LA_UDF_REFER|LA_READ_ONLY);
            laAddSubGroup(p, "holder_h2", "Holder H2", "Holder data block (hyper 2)", "any_pointer_h2",0,0,0,offsetof(laAction, HolderInstance), 0,0,0,0,0,0,0,LA_UDF_REFER|LA_READ_ONLY);
            laAddSubGroup(p, "channels", "Channels", "Action channels", "la_animation_channel",0,0,0,-1,0,0,0,0,0,0,offsetof(laAction, Channels),0);
            laAddFloatProperty(p, "length","Length","Length of the action in seconds",0,0,"s",30,0,0.1,2,0,offsetof(laAction,Length),0,0,0,0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "play_head","Play Head","Play head position",0,0,0,1.0,0,0.01,0,0,offsetof(laAction,PlayHead),0,0,0,0,0,0,0,0,0,0,0);
            laAddFloatProperty(p, "offset","Offset","Play head offset from global timing",0,0,0,1.0,0,0.01,0,0,offsetof(laAction,Offset),0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "frame_count","Frame Count","Total frame count in the whole length of the action",0,0,0,960,1,0,24,0,offsetof(laAction,FrameCount),0,0,0,0,0,0,0,0,0,0,0);
            laAddIntProperty(p, "current_frame","Current Frame","Current frame in this action",0,0,0,0,0,0,0,0,0,laget_AnimationActionCurrentFrame,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            ep = laAddEnumProperty(p, "mute", "Mute", "Mute this action", 0,0,0,0,0,offsetof(laAction, Mute),0,0,0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "NONE", "None", "Not muted", 0,0);
                laAddEnumItemAs(ep, "MUTE", "Mute", "Muted", 1,0);
            }
            ep = laAddEnumProperty(p, "solo", "Solo", "Solo play this action", 0,0,0,0,0,offsetof(laAction, Solo),0,0,0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "NONE", "None", "Play this action with", 0,0);
                laAddEnumItemAs(ep, "SOLO", "Solo", "Solo play", 1,0);
            }
            ep = laAddEnumProperty(p, "play_by_default", "Auto Play", "Automatically play this animation when player starts", 0,0,0,0,0,offsetof(laAction, PlayByDefault),0,0,0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "NONE", "Stop", "Player initialize the animation in stopped state", 0,0);
                laAddEnumItemAs(ep, "PLAY", "Play", "Animation automatically plays when player starts", 1,0);
            }
            ep = laAddEnumProperty(p, "play_mode", "Play Mode", "How to play this action", 0,0,0,0,0,offsetof(laAction, PlayMode),0,0,0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "REPEAT", "Repeat", "Play action in repeat", LA_ANIMATION_PLAY_MODE_REPEAT, U'⮆');
                laAddEnumItemAs(ep, "HOLD", "Hold", "Hold end values when time is outside time range", LA_ANIMATION_PLAY_MODE_HOLD,U'⭲');
                laAddEnumItemAs(ep, "BOUNCE", "Bounce", "Play action back and forth", LA_ANIMATION_PLAY_MODE_BOUNCE,U'⮀');
            }
            ep = laAddEnumProperty(p, "mix_mode", "Mix", "How this action is mixed with existing ones", 0,0,0,0,0,offsetof(laAction, MixMode),0,0,0,0,0,0,0,0,0,0);{
                laAddEnumItemAs(ep, "REPLACE", "Replace", "Replace previously set values", LA_ANIMATION_MIX_REPLACE,0);
                laAddEnumItemAs(ep, "ADD", "Add", "Add on top of previously set values", LA_ANIMATION_MIX_ADD,0);
            }
            laAddOperatorProperty(p,"remove","Remove","Remove this action","LA_animation_remove_action",L'🞫',0);
        }
        p = laAddPropertyContainer("la_animation_channel", "Channel", "Action channel",0,0,sizeof(laActionChannel),0,0,1);{
            laAddSubGroup(p, "keys", "Keys", "Key Frames", "la_animation_key",0,0,0,-1,0,0,0,0,0,0,offsetof(laActionChannel, Keys),0);
            laAddSubGroup(p, "ap", "Animation Property", "Animation property reference", "la_animation_prop",0,0,0,offsetof(laActionChannel, AP), 0,0,0,0,0,0,0,LA_UDF_REFER);
        }
        p = laAddPropertyContainer("la_animation_prop", "Property", "Animation property",0,0,sizeof(laActionProp),0,0,1);{
            laAddIntProperty(p, "data_size","Data Size","Data size of the channel",0,0,0,0,0,0,0,0,offsetof(laActionProp,DataSize),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddStringProperty(p,"prop_str","Property String","Property of this channel",0,0,0,0,0,0,0,laget_AnimationPropStr,0,laread_AnimationPropStr,LA_READ_ONLY);
            laAddStringProperty(p,"cached_str","Cached String","Property name of this channel",0,0,0,0,1,offsetof(laActionProp,CachedName),0,0,0,0,LA_READ_ONLY|LA_AS_IDENTIFIER);
            laAddSubGroup(p, "for", "For", "Target data block", "any_pointer",0,0,0,offsetof(laActionProp, For), 0,0,0,0,0,0,0,LA_UDF_REFER);
            laAddSubGroup(p, "for_h2", "For H2", "Target data block (hyper 2)", "any_pointer_h2",0,0,0,offsetof(laActionProp, For), 0,0,0,0,0,0,0,LA_UDF_REFER);
        }
        p = laAddPropertyContainer("la_animation_key", "key", "Action channel",0,0,sizeof(laActionKey),0,0,1);{
            laAddIntProperty(p, "at","At","Frame number of this key frame",0,0,0,0,0,0,0,0,offsetof(laActionKey,At),0,0,0,0,0,0,0,0,0,0,0);
            laAddRawProperty(p,"data","Data","Data of this key frame",0,0,lagetraw_ActionKeyData,lasetraw_ActionKeyData,0);
        }
        p = laAddPropertyContainer("la_animation_action_holder", "Action Holder", "Action holder",0,0,sizeof(laActionHolder),0,0,1);{
            laAddStringProperty(p,"name","Name","Name of the action",0,0,0,0,1,offsetof(laActionHolder,Name),0,0,0,0,LA_AS_IDENTIFIER);
            laAddIntProperty(p,"action_offset","Action Offset","List handle offset of the action list in the instance",0,0,0,0,0,0,0,0,offsetof(laActionHolder,ActionOffset),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
            laAddIntProperty(p,"prop_offset","Prop Offset","List handle offset of the property list in the instance",0,0,0,0,0,0,0,0,offsetof(laActionHolder,PropOffset),0,0,0,0,0,0,0,0,0,0,LA_READ_ONLY);
        }
    }

    la_RegisterTNSProps();
}

void la_RegisterDefaultSignals(){
    laNewCustomSignal("la.new",LA_SIGNAL_NEW);
    laNewCustomSignal("la.delete",LA_SIGNAL_DELETE);
    laNewCustomSignal("la.confirm",LA_SIGNAL_CONFIRM);
    laNewCustomSignal("la.undo",LA_SIGNAL_UNDO);
    laNewCustomSignal("la.redo",LA_SIGNAL_REDO);
    laNewCustomSignal("la.open",LA_SIGNAL_OPEN);
    laNewCustomSignal("la.save",LA_SIGNAL_SAVE);
    laNewCustomSignal("la.save_as",LA_SIGNAL_SAVE_AS);
    laNewCustomSignal("la.layout_next",LA_SIGNAL_LAYOUT_NEXT);
    laNewCustomSignal("la.layout_prev",LA_SIGNAL_LAYOUT_PREV);
    laNewCustomSignal("la.fullscreen",LA_SIGNAL_FULLSCREEN);

    laInputMapping* im=laNewInputMapping("Default");
    laNewInputMappingEntryP(im,LA_INPUT_DEVICE_KEYBOARD,0,"z",LA_KEY_CTRL,LA_SIGNAL_UNDO);
    laNewInputMappingEntryP(im,LA_INPUT_DEVICE_KEYBOARD,0,"z",LA_KEY_CTRL|LA_KEY_SHIFT,LA_SIGNAL_REDO);
    laNewInputMappingEntryP(im,LA_INPUT_DEVICE_KEYBOARD,0,"o",LA_KEY_CTRL,LA_SIGNAL_OPEN);
    laNewInputMappingEntryP(im,LA_INPUT_DEVICE_KEYBOARD,0,"s",LA_KEY_CTRL,LA_SIGNAL_SAVE);
    laNewInputMappingEntryP(im,LA_INPUT_DEVICE_KEYBOARD,0,"s",LA_KEY_CTRL|LA_KEY_SHIFT,LA_SIGNAL_SAVE_AS);
    laNewInputMappingEntryP(im,LA_INPUT_DEVICE_KEYBOARD,0,"Right",LA_KEY_CTRL,LA_SIGNAL_LAYOUT_NEXT);
    laNewInputMappingEntryP(im,LA_INPUT_DEVICE_KEYBOARD,0,"Left",LA_KEY_CTRL,LA_SIGNAL_LAYOUT_PREV);
    laNewInputMappingEntryP(im,LA_INPUT_DEVICE_KEYBOARD,0,"F11",0,LA_SIGNAL_FULLSCREEN);
    
    laNewInputMappingEntryP(im,LA_INPUT_DEVICE_KEYBOARD,0,"a",LA_KEY_SHIFT,"la.new");
    laNewInputMappingEntryP(im,LA_INPUT_DEVICE_KEYBOARD,0,"x",0,"la.delete");
}
