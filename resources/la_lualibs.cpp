/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "la_5.h"

extern "C" const char* LA_LUA_LIB_COMMON=R"(
function ddump(tbl, ...)
    log("[dump]")
    if(type(tbl)~='table') then log("Not a table."); return end
    local levels=100
    local dtableonly=0
    local l,d=...
    if(l) then levels=l end
    if(d) then dtableonly=d end
    local checklist = {}
    local function print_kv(indent,k,v,t)
        log(indent..t.." "..k,v,""..(t==' ' and type(v) or ""))
    end
    local function innerdump(tbl,indent,lv)
        local tablelist={}; local funclist={}; local otherlist={}
        checklist[ tostring(tbl) ] = true
        for k,v in pairs(tbl) do
            if type(v)=="function" then funclist[k]={v,'f'}
            elseif type(v)=="table" then tablelist[k]={v,'t'}
            else otherlist[k]={v,' '} end
        end
        for k,v in pairs(tablelist) do
            print_kv(indent,k,v[1],v[2])
            if (lv<levels and not checklist[ tostring(v[1]) ]) then innerdump(v[1],indent.."    ",lv+1) end
        end
        if(dtableonly==0) then for k,v in pairs(funclist) do print_kv(indent,k,v[1],v[2]) end end
        if(dtableonly==0) then for k,v in pairs(otherlist) do print_kv(indent,k,v[1],v[2]) end end
    end
    checklist[ tostring(tbl) ] = true
    innerdump( tbl, "", 0)
end

function tdump(t)
    ddump(t,100,1)
end

ffi = require("ffi")
)";


extern "C" const char* LA_LUA_LIB_AUDIO=R"(
ffi.cdef[[

typedef struct laSynth laSynth;

laSynth* laFindSynth(const char* Name);
void laSynthTriggerNew(laSynth* s);
]]
)";