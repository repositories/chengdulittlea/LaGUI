/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "la_5.h"

extern "C" const char* LA_TNS_LIB_COMMON=R"(
ffi.cdef[[

typedef double real;

typedef struct _laListItem laListItem;
struct _laListItem {
	void* pPrev;
	void* pNext;
};
typedef struct _laEvent laEvent;
struct _laEvent{
    laListItem Item;
    void* window;
    int type;
    int state;
    int x, y;
    int key;
    int SpecialKeyBit;
    int p1, p2;
    uint32_t Input;
    int GoodPressure,IsEraser,HasTwist;
    real Pressure,Orientation,Deviation,Twist;
    void *Localized;
};

typedef struct tnsObject tnsObject;
int tnsSizeOfObject(tnsObject* o);

void tnsSetObjectPositionLocal(tnsObject*o, double x, double y, double z);
void tnsResetObjectTransformations(tnsObject* o, int reset_loc,int reset_rot,int reset_sca);
void tnsMoveObjectLocal(tnsObject *o, double x, double y, double z);

tnsObject *tnsFindObject(const char *Name, tnsObject *FromObj);
]]
)";