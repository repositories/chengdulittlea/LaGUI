/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../la_5.h"

extern LA MAIN;
extern struct _tnsMain *T;

laBaseNodeType TNS_IDN_TRANSFORM;
laBaseNodeType TNS_IDN_MAKE_TRANSFORM;
laBaseNodeType TNS_IDN_ACTION_PLAYER;

laPropContainer* TNS_PC_IDN_TRANSFORM;
laPropContainer* TNS_PC_IDN_MAKE_TRANSFORM;
laPropContainer* TNS_PC_IDN_ACTION_PLAYER;

void IDN_TransformInit(tnsTransformNode* n, int NoCreate){
    if(NoCreate){return;}
    n->Mat=laCreateInSocket("MAT",0); strSafeSet(&n->Base.Name,"Transform");
}
void IDN_TransformDestroy(tnsTransformNode* n){
    laDestroyInSocket(n->Mat); strSafeDestroy(&n->Base.Name);
}
int IDN_TransformVisit(tnsTransformNode* n, laNodeVisitInfo* vi){
    if(LA_SRC_AND_PARENT(n->Mat)){ laBaseNode*bn=n->Mat->Source->Parent; LA_VISIT_NODE(bn,vi); }
    LA_ADD_THIS_NODE(n,vi);
    return LA_DAG_FLAG_PERM;
}
int IDN_TransformEval(tnsTransformNode* n){
    if(!n->Target) return 0;
    tnsObject* ob=n->Target; if(ob->PlayDuplicate){ ob=ob->PlayDuplicate; }
    if((!n->Mat->Source) || (n->Mat->Source->DataType!=(LA_PROP_FLOAT|LA_PROP_ARRAY)) || (n->Mat->Source->ArrLen!=16)){
        tnsLoadIdentity44d(ob->DeltaTransform); tnsSelfMatrixChanged(ob, 1);
    }else{
        memcpy(ob->DeltaTransform, n->Mat->Source->Data, sizeof(tnsMatrix44d)); tnsSelfMatrixChanged(ob, 1);
    }
    laNotifyInstanceUsers(n->Target);
    return 1;
}
void IDN_TransformCopy(tnsTransformNode* new, tnsTransformNode* old, int DoRematch){
    if(DoRematch){ LA_IDN_NEW_LINK(Mat) return; }
    memAssignRef(new,&new->Target,old->Target);
}
void tnsui_TransformNode(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil); tnsTransformNode*n=This->EndInstance;
    LA_BASE_NODE_HEADER(uil,c,This);
    laColumn* cl,*cr; laSplitColumn(uil,c,0.3); cl=laLeftColumn(c,0); cr=laRightColumn(c,0);

    laUiItem* b=laBeginRow(uil,cl,0,0);
    laShowNodeSocket(uil,cl,This,"mat",0)->Flags|=LA_UI_SOCKET_LABEL_E;
    laEndRow(uil,b);

    laShowItem(uil,cr,This,"target");
}

void IDN_MakeTransformInit(tnsMakeTransformNode* n, int NoCreate){
    if(NoCreate){return;}
    n->Out=laCreateOutSocket(n,"MAT",0); strSafeSet(&n->Base.Name,"Make Transform");
    n->Loc=laCreateInSocket("LOC",0); 
    n->Rot=laCreateInSocket("ROT",0);  n->Angle=laCreateInSocket("ANGLE",0); n->UseRot[2]=1;
    n->Sca=laCreateInSocket("SCALE",0);  n->UseSca=1;
}
void IDN_MakeTransformDestroy(tnsMakeTransformNode* n){
    laDestroyOutSocket(n->Out); strSafeDestroy(&n->Base.Name);
    laDestroyInSocket(n->Loc);
    laDestroyInSocket(n->Rot); laDestroyInSocket(n->Angle);
    laDestroyInSocket(n->Sca);
}
int IDN_MakeTransformVisit(tnsMakeTransformNode* n, laNodeVisitInfo* vi){
    LA_GUARD_THIS_NODE(n,vi);
    if(LA_SRC_AND_PARENT(n->Loc)){ laBaseNode*bn=n->Loc->Source->Parent; LA_VISIT_NODE(bn,vi); }
    if(LA_SRC_AND_PARENT(n->Rot)){ laBaseNode*bn=n->Rot->Source->Parent; LA_VISIT_NODE(bn,vi); }
    if(LA_SRC_AND_PARENT(n->Sca)){ laBaseNode*bn=n->Sca->Source->Parent; LA_VISIT_NODE(bn,vi); }
    if(LA_SRC_AND_PARENT(n->Angle)){ laBaseNode*bn=n->Angle->Source->Parent;  LA_VISIT_NODE(bn,vi); }
    LA_ADD_THIS_NODE(n,vi);
    return LA_DAG_FLAG_PERM;
}
int IDN_MakeTransformEval(tnsMakeTransformNode* n){
    tnsMatrix44d mm,mt,mr,ms; real* UseT,*UseR,*UseA,*UseS; tnsVector3d nr;
    if(LA_SRC_AND_PARENT(n->Loc) && n->Loc->Source->ArrLen>=3 && n->Loc->Source->DataType==LA_PROP_FLOAT|LA_PROP_ARRAY){ UseT=n->Loc->Source->Data; }else{ UseT=n->UseLoc; }
    if(LA_SRC_AND_PARENT(n->Rot) && n->Rot->Source->ArrLen>=3 && n->Rot->Source->DataType==LA_PROP_FLOAT|LA_PROP_ARRAY){ UseR=n->Rot->Source->Data; }else{ UseR=n->UseRot; }
    if(LA_SRC_AND_PARENT(n->Angle) && n->Angle->Source->DataType&LA_PROP_FLOAT){ UseA=n->Angle->Source->Data; }else{ UseA=&n->UseAngle; }
    if(LA_SRC_AND_PARENT(n->Sca) && n->Sca->Source->DataType&LA_PROP_FLOAT){ UseS=n->Sca->Source->Data; }else{ UseS=&n->UseSca; }
    tnsMakeTranslationMatrix44d(mt,LA_COLOR3(UseT));
    tnsNormalize3d(nr, UseR); tnsMakeRotationMatrix44d(mr,*UseA, LA_COLOR3(nr));
    tnsMakeScaleMatrix44d(ms,*UseS,*UseS,*UseS);
    tnsMultiply44d(mm,mt,mr); tnsMultiply44d(n->Mat,mm,ms);
    n->Out->ArrLen=16; n->Out->Data=n->Mat; n->Out->DataType=LA_PROP_FLOAT|LA_PROP_ARRAY;
    return 1;
}
void IDN_MakeTransformCopy(tnsMakeTransformNode* new, tnsMakeTransformNode* old, int DoRematch){
    if(DoRematch){ LA_IDN_NEW_LINK(Loc) LA_IDN_NEW_LINK(Rot) LA_IDN_NEW_LINK(Sca) LA_IDN_NEW_LINK(Angle) return; }
    LA_IDN_OLD_DUPL(Out); new->UseSca=old->UseSca; new->UseAngle=old->UseAngle;
    tnsVectorSet3v(new->UseLoc,old->UseLoc); tnsVectorSet3v(new->UseRot,old->UseRot);
}
void tnsui_MakeTransformNode(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil); tnsMakeTransformNode*n=This->EndInstance;
    laUiItem* b2, *rui;
    LA_BASE_NODE_HEADER(uil,c,This);

    laUiItem* b=laBeginRow(uil,c,0,0);
    laShowNodeSocket(uil,c,This,"loc",0)->Flags|=LA_UI_SOCKET_LABEL_E; b2=laOnConditionThat(uil,c,laNot(laPropExpression(This,"loc.source")));{
        laShowItem(uil,c,This,"use_loc")->Expand=1;
    }laEndCondition(uil,b2);
    laEndRow(uil,b);

    b=laBeginRow(uil,c,0,0);
    laShowNodeSocket(uil,c,This,"rot",0)->Flags|=LA_UI_SOCKET_LABEL_E; b2=laOnConditionThat(uil,c,laNot(laPropExpression(This,"rot.source")));{
        laShowItem(uil,c,This,"use_rot")->Expand=1;
    }laEndCondition(uil,b2);
    laEndRow(uil,b);

    b=laBeginRow(uil,c,0,0);
    laShowNodeSocket(uil,c,This,"angle",0)->Flags|=LA_UI_SOCKET_LABEL_E; b2=laOnConditionThat(uil,c,laNot(laPropExpression(This,"angle.source")));{
        laShowItem(uil,c,This,"use_angle")->Expand=1;
    }laEndCondition(uil,b2);
    laEndRow(uil,b);

    b=laBeginRow(uil,c,0,0);
    laShowNodeSocket(uil,c,This,"sca",0)->Flags|=LA_UI_SOCKET_LABEL_E; b2=laOnConditionThat(uil,c,laNot(laPropExpression(This,"sca.source")));{
        laShowItem(uil,c,This,"use_sca")->Expand=1;
    }laEndCondition(uil,b2);
    laEndRow(uil,b);

    b=laBeginRow(uil,c,0,0);
    laShowSeparator(uil,c)->Expand=1;;
    laShowNodeSocket(uil,c,This,"out",0)->Flags|=LA_UI_SOCKET_LABEL_W;
    laEndRow(uil,b);
}

void IDN_ActionPlayerInit(tnsActionPlayerNode* n, int NoCreate){
    if(NoCreate){ return; }
    strSafeSet(&n->Base.Name,"Make Transform");
    n->Next=laCreateOutSocket(n,"NEXT",0); n->Prev=laCreateInSocket("PREV",0);
    n->Transport=laCreateInSocket("TRANSPORT",0); n->Detached=laCreateInSocket("DETACHED",0); n->Time=laCreateInSocket("TIME",0);
}
void IDN_ActionPlayerDestroy(tnsActionPlayerNode* n){
    laDestroyInSocket(n->Prev); laDestroyOutSocket(n->Next); strSafeDestroy(&n->Base.Name);
    laDestroyInSocket(n->Transport); laDestroyInSocket(n->Time); laDestroyInSocket(n->Detached);
}
int IDN_ActionPlayerVisit(tnsActionPlayerNode* n, laNodeVisitInfo* vi){
    LA_GUARD_THIS_NODE(n,vi);
    if(LA_SRC_AND_PARENT(n->Prev)){ laBaseNode*bn=n->Prev->Source->Parent; LA_VISIT_NODE(bn,vi); }
    LA_ADD_THIS_NODE(n,vi);
    return LA_DAG_FLAG_PERM;
}
int IDN_ActionPlayerEval(tnsActionPlayerNode* n){
    if(!T->Runtime.CurrentEN || !n->Action){ return 1; } tnsEvaluatedNode* en=T->Runtime.CurrentEN;
    int transport = n->iTransport; LA_GET_SRC_AS_VALUE(transport,n->Transport);
    int detached = n->iDetached; LA_GET_SRC_AS_VALUE(detached,n->Detached);
    real time = 0; int has_time=n->Time->Source?1:0; LA_GET_SRC_AS_VALUE(time,n->Time);
    if(en->ActionRetarget){
        laActionRetarget* ar=en->ActionRetarget;
        en->ActionRetarget->DetachedInNode = detached;
        if(!en->ActionRetarget->DetachedInNode){ ar=((tnsRootObject*)en->Target)->ActionRetarget; }
        laRetargetedAction* ra=la_AnimationGetRetargetedAction(ar,n->Action); if(!ra){ return 1; }
        ra->PlayStatus = transport; if(has_time){ ra->PlayHead = time; }
    }else{
        tnsRootObject* o=en->Target; if(o->Base.Type!=TNS_OBJECT_ROOT){ return 1; }
        if(has_time){ n->Action->Offset=0; n->Action->PlayHead=time; }
    }
    return 1;
}
void IDN_ActionPlayerCopy(tnsActionPlayerNode* new, tnsActionPlayerNode* old, int DoRematch){
    if(DoRematch){ LA_IDN_NEW_LINK(Prev) return; }
    LA_IDN_OLD_DUPL(Next); new->Time=old->Time; new->Transport=old->Transport; new->Detached=old->Detached;
    new->iDetached = old->iDetached; new->iTransport = old->iTransport;
    memAssignRef(new,&new->Action,old->Action);
}
void tnsui_ActionPlayerNode(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil),*cl,*cr,*cll,*clr;
    LA_BASE_NODE_HEADER(uil,c,This);
    laSplitColumn(uil,c,0.6); cl=laLeftColumn(c,0); cr=laRightColumn(c,3);
    laSplitColumn(uil,cl,0.4); cll=laLeftColumn(cl,3); clr=laRightColumn(cl,0);
    tnsActionPlayerNode*n=This->EndInstance;
    laUiItem* b2, *rui;

    laShowNodeSocket(uil,cr,This,"prev",0)->Flags|=LA_UI_SOCKET_LABEL_W;
    laShowNodeSocket(uil,cr,This,"next",0)->Flags|=LA_UI_SOCKET_LABEL_W;

    laUiItem* b=laBeginRow(uil,cl,0,0);
    laShowNodeSocket(uil,cl,This,"in_transport",0); b2=laOnConditionThat(uil,c,laNot(laPropExpression(This,"in_transport.source")));{
        laShowItemFull(uil,cl,This,"transport",0,"text=Transport",0,0)->Expand=1;
    }laElse(uil,b2);{
        laShowLabel(uil,cl,"Transport",0,0)->Expand=1;
    }laEndCondition(uil,b2);
    laShowNodeSocket(uil,cl,This,"in_detached",0); b2=laOnConditionThat(uil,c,laNot(laPropExpression(This,"in_detached.source")));{
        laShowItemFull(uil,cl,This,"detached",0,"text=Detached",0,0)->Expand=1;
    }laElse(uil,b2);{
        laShowLabel(uil,cl,"Detached",0,0)->Expand=1;
    }laEndCondition(uil,b2);
    laEndRow(uil,b);

    laShowNodeSocket(uil,cll,This,"in_time",0)->Flags|=LA_UI_SOCKET_LABEL_E;
    laShowItemFull(uil,clr,This,"action",LA_WIDGET_COLLECTION_SELECTOR,0,laui_IdentifierOnly,0);
}


int OPCHK_AddDriverPage(laPropPack *This, laStringSplitor *ss){
    if (This && la_EnsureSubTarget(This->LastPs->p,0) == TNS_PC_OBJECT_GENERIC) return 1;
    return 0;
}
int OPINV_AddDriverPage(laOperator* a, laEvent *e){
    tnsObject* ob=a->This?a->This->EndInstance:0; if(!ob || !ob->Drivers) return LA_CANCELED;
    laRackPage* dp=memAcquire(sizeof(laRackPage));
    strSafeSet(&dp->Name,"New Page");
    lstAppendItem(&ob->Drivers->Pages, dp);
    memAssignRef(dp,&dp->ParentObject,ob); memAssignRef(ob->Drivers,&ob->Drivers->CurrentPage,dp);
    dp->RackType=LA_RACK_TYPE_DRIVER;
    laNotifyInstanceUsers(ob->Drivers); laRecordAndPush(a->This,"","Add driver page", 0);
    return LA_FINISHED;
}
int OPCHK_RemoveDriverPage(laPropPack *This, laStringSplitor *ss){
    if ((!This) || (la_EnsureSubTarget(This->LastPs->p,0) != LA_PC_RACK_PAGE)) return 0;
    laRackPage* dp=This->EndInstance; if(!dp->ParentObject) return 0;
    return 1;
}
int OPINV_RemoveDriverPage(laOperator* a, laEvent *e){
    laRackPage* dp=a->This?a->This->EndInstance:0; if(!dp || !dp->ParentObject) return LA_CANCELED;
    tnsObject* ob=dp->ParentObject;

    if(strSame(strGetArgumentString(a->ExtraInstructionsP,"confirm"),"true")){
        strSafeDestroy(&dp->Name);
        while(dp->Racks.pFirst){ laDestroyRack(dp->Racks.pFirst); }
        while(lstPopPointer(&dp->Eval)); while(lstPopPointer(&dp->AlwaysBranchers));
        if(dp->ParentObject->Drivers->CurrentPage==dp){
            laRackPage* adp=dp->Item.pNext?dp->Item.pNext:dp->Item.pPrev;
            memAssignRef(dp->ParentObject->Drivers,&dp->ParentObject->Drivers->CurrentPage,adp);
        }laNotifyInstanceUsers(dp);
        lstRemoveItem(&dp->ParentObject->Drivers->Pages, dp); memLeave(dp);
        laNotifyInstanceUsers(ob->Drivers); laRecordAndPush(a->This,"","Remove driver page", 0);
        return LA_FINISHED;
    }
    laEnableOperatorPanel(a,a->This,e->x,e->y,200,200,0,0,0,0,0,0,0,0,e);
    return LA_RUNNING;
}
void laui_RemoveDriverPage(laUiList *uil, laPropPack *This, laPropPack *Extra, laColumn *UNUSED, int context){
    laColumn* c=laFirstColumn(uil);
    laShowItemFull(uil,c,This,"remove_driver_page",0,"confirm=true;text=Confirm",0,0);
}

int OPINV_RebuildDrivers(laOperator* a, laEvent *e){
    laGraphRequestRebuild();
    return LA_FINISHED;
}

tnsObject* tnsget_FirstObject(void* unused, void* unused2){
    return T->World->AllObjects.pFirst;
}
laAction* tnsget_FirstActionFromActionPlayer(tnsActionPlayerNode* apn, void* unused2){
    if(!apn->Base.InRack || !apn->Base.InRack->ParentPage || !apn->Base.InRack->ParentPage->ParentObject) return 0;
    tnsRootObject* ro = apn->Base.InRack->ParentPage->ParentObject; return ro->Actions.pFirst;
}

void tns_RegisterNodes(){
    laPropContainer *pc; laProp *p;
    laOperatorType *at;
    laEnumProp *ep;

    laCreateOperatorType("LA_add_driver_page", "New Page", "Add a driver page",OPCHK_AddDriverPage,0,0,OPINV_AddDriverPage,0,'+',0);
    laCreateOperatorType("LA_remove_driver_page", "Remove Page", "Remove a driver page",OPCHK_RemoveDriverPage,0,0,OPINV_RemoveDriverPage,OPMOD_FinishOnData,L'🗴',0)
        ->UiDefine=laui_RemoveDriverPage;
    laCreateOperatorType("LA_driver_rebuild", "Rebuild Drivers", "Rebuild drivers for evaluation",0,0,0,OPINV_RebuildDrivers,0,U'⭮',0);

    LA_NODE_CATEGORY_DRIVER=laEnsureNodeCategory("Driver",0,LA_RACK_TYPE_DRIVER);

    if(!MAIN.InitArgs.HasWorldObjects) return;

    pc=laAddPropertyContainer("tns_transform_node", "Transform", "Transform objects",0,tnsui_TransformNode,sizeof(tnsTransformNode),lapost_Node,0,1);
    TNS_PC_IDN_TRANSFORM=pc; laPropContainerExtraFunctions(pc,0,0,0,0,laui_DefaultNodeOperationsPropUiDefine);
    laAddSubGroup(pc,"base","Base","Base node","la_base_node",0,0,0,0,0,0,0,0,0,0,0,LA_UDF_LOCAL);
    laAddSubGroup(pc,"mat", "Mat","Input matrix","la_in_socket",0,0,0,offsetof(tnsTransformNode,Mat),0,0,0,0,0,0,0,LA_UDF_SINGLE);
    laAddSubGroup(pc,"target", "Target","Target object","tns_object",0,LA_WIDGET_COLLECTION_SELECTOR,laui_IdentifierOnly,offsetof(tnsTransformNode,Target),tnsget_FirstObject,0,laget_ListNext,0,0,0,0,LA_UDF_REFER);
    
    pc=laAddPropertyContainer("tns_make_transform_node", "Make Transform", "Make transform matrix",0,tnsui_MakeTransformNode,sizeof(tnsMakeTransformNode),lapost_Node,0,1);
    TNS_PC_IDN_MAKE_TRANSFORM=pc; laPropContainerExtraFunctions(pc,0,0,0,0,laui_DefaultNodeOperationsPropUiDefine);
    laAddSubGroup(pc,"base","Base","Base node","la_base_node",0,0,0,0,0,0,0,0,0,0,0,LA_UDF_LOCAL);
    laAddSubGroup(pc,"out", "Out","Output matrix","la_out_socket",0,0,0,offsetof(tnsMakeTransformNode,Out),0,0,0,0,0,0,0,LA_UDF_SINGLE);
    laAddSubGroup(pc,"loc", "Location","Location","la_in_socket",0,0,0,offsetof(tnsMakeTransformNode,Loc),0,0,0,0,0,0,0,LA_UDF_SINGLE);
    laAddSubGroup(pc,"rot", "Rotation","Rotation","la_in_socket",0,0,0,offsetof(tnsMakeTransformNode,Rot),0,0,0,0,0,0,0,LA_UDF_SINGLE);
    laAddSubGroup(pc,"sca", "Scale","Scale","la_in_socket",0,0,0,offsetof(tnsMakeTransformNode,Sca),0,0,0,0,0,0,0,LA_UDF_SINGLE);
    laAddSubGroup(pc,"angle", "Angle","Rotation Angle","la_in_socket",0,0,0,offsetof(tnsMakeTransformNode,Angle),0,0,0,0,0,0,0,LA_UDF_SINGLE);
    laAddFloatProperty(pc,"use_loc", "Location", "Use Location",0,"X,Y,Z",0,0,0,0.05,0,0,offsetof(tnsMakeTransformNode, UseLoc),0,0,3,0,0,0,0,0,0,0,0);
    laAddFloatProperty(pc,"use_rot", "Rotation", "Use Rotation",0,"X,Y,Z",0,0,0,0.05,0,0,offsetof(tnsMakeTransformNode, UseRot),0,0,3,0,0,0,0,0,0,0,0);
    laAddFloatProperty(pc,"use_sca", "Scale", "Use scale",0,0,0,0,0,0.05,0,0,offsetof(tnsMakeTransformNode, UseSca),0,0,0,0,0,0,0,0,0,0,0);
    laAddFloatProperty(pc,"use_angle", "Angle", "Use angle",0,0,0,0,0,0.05,0,0,offsetof(tnsMakeTransformNode, UseAngle),0,0,0,0,0,0,0,0,0,0,0);

    pc=laAddPropertyContainer("tns_action_player_node", "Action Player", "Action player node",0,tnsui_ActionPlayerNode,sizeof(tnsActionPlayerNode),lapost_Node,0,1);
    TNS_PC_IDN_ACTION_PLAYER=pc; laPropContainerExtraFunctions(pc,0,0,0,0,laui_DefaultNodeOperationsPropUiDefine);
    laAddSubGroup(pc,"base","Base","Base node","la_base_node",0,0,0,0,0,0,0,0,0,0,0,LA_UDF_LOCAL);
    laAddSubGroup(pc,"prev", "Previous","Previous node","la_in_socket",0,0,0,offsetof(tnsActionPlayerNode,Prev),0,0,0,0,0,0,0,LA_UDF_SINGLE);
    laAddSubGroup(pc,"next", "Next","Next node","la_out_socket",0,0,0,offsetof(tnsActionPlayerNode,Next),0,0,0,0,0,0,0,LA_UDF_SINGLE);
    laAddSubGroup(pc,"in_transport", "In Transport","Transport input","la_in_socket",0,0,0,offsetof(tnsActionPlayerNode,Transport),0,0,0,0,0,0,0,LA_UDF_SINGLE);
    laAddSubGroup(pc,"in_time", "In Time","Time Input","la_in_socket",0,0,0,offsetof(tnsActionPlayerNode,Time),0,0,0,0,0,0,0,LA_UDF_SINGLE);
    laAddSubGroup(pc,"in_detached", "In Detached","Detached input","la_in_socket",0,0,0,offsetof(tnsActionPlayerNode,Detached),0,0,0,0,0,0,0,LA_UDF_SINGLE);
    ep=laAddEnumProperty(pc,"transport","Transport","Action in transport",LA_WIDGET_ENUM_HIGHLIGHT,0,0,0,0,offsetof(tnsActionPlayerNode,iTransport),0,0,0,0,0,0,0,0,0,0);
    laAddEnumItemAs(ep,"STOPPED","Stopped","Player is stopped",0,0);
    laAddEnumItemAs(ep,"PLAYING","Playing","Player is playing",1,0);
    ep=laAddEnumProperty(pc,"detached","Detached","Action is detached on the instance",LA_WIDGET_ENUM_HIGHLIGHT,0,0,0,0,offsetof(tnsActionPlayerNode,iDetached),0,0,0,0,0,0,0,0,0,0);
    laAddEnumItemAs(ep,"NONE","None","Action is reused from instancer",0,0);
    laAddEnumItemAs(ep,"DETACHED","Detached","Action state is on instance itself",1,0);
    laAddSubGroup(pc,"action","Action","Target action","la_animation_action",0,0,0,offsetof(tnsActionPlayerNode,Action),tnsget_FirstActionFromActionPlayer,0,laget_ListNext,0,0,0,0,LA_UDF_REFER);
    
    LA_IDN_REGISTER("Transform",0,TNS_IDN_TRANSFORM,TNS_PC_IDN_TRANSFORM, IDN_Transform, tnsTransformNode);
    LA_IDN_REGISTER("Make Transform",0,TNS_IDN_MAKE_TRANSFORM,TNS_PC_IDN_MAKE_TRANSFORM, IDN_MakeTransform, tnsMakeTransformNode);
    LA_IDN_REGISTER("Action Player",0,TNS_IDN_ACTION_PLAYER,TNS_PC_IDN_ACTION_PLAYER, IDN_ActionPlayer, tnsActionPlayerNode);

    laNodeCategoryAddNodeTypes(LA_NODE_CATEGORY_MATH, &TNS_IDN_MAKE_TRANSFORM,0LL);
    laNodeCategoryAddNodeTypes(LA_NODE_CATEGORY_DRIVER, &TNS_IDN_TRANSFORM, &TNS_IDN_ACTION_PLAYER,0LL);
}

