/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "la_5.h"

extern "C" const char* TNS_SHADER_COLOR_COMMON=R"(
#define M_PI 3.1415926535897932384626433832795
float rad(float d){ return d/180.0*M_PI; }
float deg(float r){ return r*180.0/M_PI; }
float cbrt( float x ){
    return sign(x)*pow(abs(x),1.0f/3.0f);
}
float srgb_transfer_function(float a){
	return .0031308f >= a ? 12.92f * a : 1.055f * pow(a, .4166666666666667f) - .055f;
}
float srgb_transfer_function_inv(float a){
	return .04045f < a ? pow((a + .055f) / 1.055f, 2.4f) : a / 12.92f;
}
vec3 to_log_srgb(vec3 color){
	return vec3(srgb_transfer_function(color.r),srgb_transfer_function(color.g),srgb_transfer_function(color.b));
}
vec3 to_linear_srgb(vec3 color){
	return vec3(srgb_transfer_function_inv(color.r),srgb_transfer_function_inv(color.g),srgb_transfer_function_inv(color.b));
}
vec3 to_linear_clay(vec3 color){
	return vec3(pow(color.r,2.19921875),pow(color.g,2.19921875),pow(color.b,2.19921875));
}
vec3 to_log_clay(vec3 color){
	return vec3(pow(color.r,1.0/2.19921875),pow(color.g,1.0/2.19921875),pow(color.b,1.0/2.19921875));
}
vec3 linear_srgb_to_oklab(vec3 c){
	float l = 0.4122214708f * c.r + 0.5363325363f * c.g + 0.0514459929f * c.b;
	float m = 0.2119034982f * c.r + 0.6806995451f * c.g + 0.1073969566f * c.b;
	float s = 0.0883024619f * c.r + 0.2817188376f * c.g + 0.6299787005f * c.b;
	float l_ = cbrt(l);
	float m_ = cbrt(m);
	float s_ = cbrt(s);
	return vec3(
		0.2104542553f * l_ + 0.7936177850f * m_ - 0.0040720468f * s_,
		1.9779984951f * l_ - 2.4285922050f * m_ + 0.4505937099f * s_,
		0.0259040371f * l_ + 0.7827717662f * m_ - 0.8086757660f * s_
	);
}
vec3 oklab_to_linear_srgb(vec3 c){
	float l_ = c.x + 0.3963377774f * c.y + 0.2158037573f * c.z;
	float m_ = c.x - 0.1055613458f * c.y - 0.0638541728f * c.z;
	float s_ = c.x - 0.0894841775f * c.y - 1.2914855480f * c.z;
	float l = l_ * l_ * l_;
	float m = m_ * m_ * m_;
	float s = s_ * s_ * s_;
	return vec3(
		+4.0767416621f * l - 3.3077115913f * m + 0.2309699292f * s,
		-1.2684380046f * l + 2.6097574011f * m - 0.3413193965f * s,
		-0.0041960863f * l - 0.7034186147f * m + 1.7076147010f * s
	);
}
vec3 oklab_to_xyz(vec3 c){
	float l_ = c.x + 0.3963377774f * c.y + 0.2158037573f * c.z;
	float m_ = c.x - 0.1055613458f * c.y - 0.0638541728f * c.z;
	float s_ = c.x - 0.0894841775f * c.y - 1.2914855480f * c.z;
	float l = l_ * l_ * l_;
	float m = m_ * m_ * m_;
	float s = s_ * s_ * s_;
	mat3 mat=inverse(mat3(vec3(+0.8189330101,+0.0329845436,+0.0482003018),
			              vec3(+0.3618667424,+0.9293118715,+0.2643662691),
				          vec3(-0.1288597137,+0.0361456387,+0.6338517070)));
	return mat*vec3(l,m,s);
}
float compute_max_saturation(float a, float b){	float k0, k1, k2, k3, k4, wl, wm, ws;
	if (-1.88170328f * a - 0.80936493f * b > 1.f){		k0 = +1.19086277f; k1 = +1.76576728f; k2 = +0.59662641f; k3 = +0.75515197f; k4 = +0.56771245f;
		wl = +4.0767416621f; wm = -3.3077115913f; ws = +0.2309699292f;
	}
	else if (1.81444104f * a - 1.19445276f * b > 1.f){		k0 = +0.73956515f; k1 = -0.45954404f; k2 = +0.08285427f; k3 = +0.12541070f; k4 = +0.14503204f;
		wl = -1.2684380046f; wm = +2.6097574011f; ws = -0.3413193965f;
	}
	else{		k0 = +1.35733652f; k1 = -0.00915799f; k2 = -1.15130210f; k3 = -0.50559606f; k4 = +0.00692167f;
		wl = -0.0041960863f; wm = -0.7034186147f; ws = +1.7076147010f;
	}	float S = k0 + k1 * a + k2 * b + k3 * a * a + k4 * a * b;
	float k_l = +0.3963377774f * a + 0.2158037573f * b;
	float k_m = -0.1055613458f * a - 0.0638541728f * b;
	float k_s = -0.0894841775f * a - 1.2914855480f * b;{
		float l_ = 1.f + S * k_l;
		float m_ = 1.f + S * k_m;
		float s_ = 1.f + S * k_s;
		float l = l_ * l_ * l_;
		float m = m_ * m_ * m_;
		float s = s_ * s_ * s_;
		float l_dS = 3.f * k_l * l_ * l_;
		float m_dS = 3.f * k_m * m_ * m_;
		float s_dS = 3.f * k_s * s_ * s_;
		float l_dS2 = 6.f * k_l * k_l * l_;
		float m_dS2 = 6.f * k_m * k_m * m_;
		float s_dS2 = 6.f * k_s * k_s * s_;
		float f = wl * l + wm * m + ws * s;
		float f1 = wl * l_dS + wm * m_dS + ws * s_dS;
		float f2 = wl * l_dS2 + wm * m_dS2 + ws * s_dS2;
		S = S - f * f1 / (f1 * f1 - 0.5f * f * f2);
	}
	return S;
}
vec2 find_cusp(float a, float b){	float S_cusp = compute_max_saturation(a, b);	vec3 rgb_at_max = oklab_to_linear_srgb(vec3( 1, S_cusp * a, S_cusp * b ));
	float L_cusp = cbrt(1.f / max(max(rgb_at_max.r, rgb_at_max.g), rgb_at_max.b));
	float C_cusp = L_cusp * S_cusp;
	return vec2( L_cusp , C_cusp );
}
float find_gamut_intersection(float a, float b, float L1, float C1, float L0, vec2 cusp){	float t;
	if (((L1 - L0) * cusp.y - (cusp.x - L0) * C1) <= 0.f){
		t = cusp.y * L0 / (C1 * cusp.x + cusp.y * (L0 - L1));
	}
	else{		t = cusp.y * (L0 - 1.f) / (C1 * (cusp.x - 1.f) + cusp.y * (L0 - L1));		{
			float dL = L1 - L0;
			float dC = C1;
			float k_l = +0.3963377774f * a + 0.2158037573f * b;
			float k_m = -0.1055613458f * a - 0.0638541728f * b;
			float k_s = -0.0894841775f * a - 1.2914855480f * b;
			float l_dt = dL + dC * k_l;
			float m_dt = dL + dC * k_m;
			float s_dt = dL + dC * k_s;			{
				float L = L0 * (1.f - t) + t * L1;
				float C = t * C1;
				float l_ = L + C * k_l;
				float m_ = L + C * k_m;
				float s_ = L + C * k_s;
				float l = l_ * l_ * l_;
				float m = m_ * m_ * m_;
				float s = s_ * s_ * s_;
				float ldt = 3.f * l_dt * l_ * l_;
				float mdt = 3.f * m_dt * m_ * m_;
				float sdt = 3.f * s_dt * s_ * s_;
				float ldt2 = 6.f * l_dt * l_dt * l_;
				float mdt2 = 6.f * m_dt * m_dt * m_;
				float sdt2 = 6.f * s_dt * s_dt * s_;
				float r = 4.0767416621f * l - 3.3077115913f * m + 0.2309699292f * s - 1.f;
				float r1 = 4.0767416621f * ldt - 3.3077115913f * mdt + 0.2309699292f * sdt;
				float r2 = 4.0767416621f * ldt2 - 3.3077115913f * mdt2 + 0.2309699292f * sdt2;
				float u_r = r1 / (r1 * r1 - 0.5f * r * r2);
				float t_r = -r * u_r;
				float g = -1.2684380046f * l + 2.6097574011f * m - 0.3413193965f * s - 1.f;
				float g1 = -1.2684380046f * ldt + 2.6097574011f * mdt - 0.3413193965f * sdt;
				float g2 = -1.2684380046f * ldt2 + 2.6097574011f * mdt2 - 0.3413193965f * sdt2;
				float u_g = g1 / (g1 * g1 - 0.5f * g * g2);
				float t_g = -g * u_g;
				float b = -0.0041960863f * l - 0.7034186147f * m + 1.7076147010f * s - 1.f;
				float b1 = -0.0041960863f * ldt - 0.7034186147f * mdt + 1.7076147010f * sdt;
				float b2 = -0.0041960863f * ldt2 - 0.7034186147f * mdt2 + 1.7076147010f * sdt2;
				float u_b = b1 / (b1 * b1 - 0.5f * b * b2);
				float t_b = -b * u_b;
				t_r = u_r >= 0.f ? t_r : 10000.f;
				t_g = u_g >= 0.f ? t_g : 10000.f;
				t_b = u_b >= 0.f ? t_b : 10000.f;
				t += min(t_r, min(t_g, t_b));
			}
		}
	}
	return t;
}
float find_gamut_intersection(float a, float b, float L1, float C1, float L0){	vec2 cusp = find_cusp(a, b);
	return find_gamut_intersection(a, b, L1, C1, L0, cusp);
}
vec3 gamut_clip_preserve_chroma(vec3 rgb){
	if (rgb.r < 1.f && rgb.g < 1.f && rgb.b < 1.f && rgb.r > 0.f && rgb.g > 0.f && rgb.b > 0.f)
		return rgb;
	vec3 lab = linear_srgb_to_oklab(rgb);
	float L = lab.x;
	float eps = 0.00001f;
	float C = max(eps, sqrt(lab.y * lab.y + lab.z * lab.z));
	float a_ = lab.y / C;
	float b_ = lab.z / C;
	float L0 = clamp(L, 0.f, 1.f);
	float t = find_gamut_intersection(a_, b_, L, C, L0);
	float L_clipped = L0 * (1.f - t) + t * L;
	float C_clipped = t * C;
	return oklab_to_linear_srgb(vec3( L_clipped, C_clipped * a_, C_clipped * b_ ));
}
vec3 gamut_clip_project_to_0_5(vec3 rgb){
	if (rgb.r < 1.f && rgb.g < 1.f && rgb.b < 1.f && rgb.r > 0.f && rgb.g > 0.f && rgb.b > 0.f)
		return rgb;
	vec3 lab = linear_srgb_to_oklab(rgb);
	float L = lab.x;
	float eps = 0.00001f;
	float C = max(eps, sqrt(lab.y * lab.y + lab.z * lab.z));
	float a_ = lab.y / C;
	float b_ = lab.z / C;
	float L0 = 0.5;
	float t = find_gamut_intersection(a_, b_, L, C, L0);
	float L_clipped = L0 * (1.f - t) + t * L;
	float C_clipped = t * C;
	return oklab_to_linear_srgb(vec3( L_clipped, C_clipped * a_, C_clipped * b_ ));
}
vec3 gamut_clip_project_to_L_cusp(vec3 rgb){
	if (rgb.r < 1.f && rgb.g < 1.f && rgb.b < 1.f && rgb.r > 0.f && rgb.g > 0.f && rgb.b > 0.f)
		return rgb;
	vec3 lab = linear_srgb_to_oklab(rgb);
	float L = lab.x;
	float eps = 0.00001f;
	float C = max(eps, sqrt(lab.y * lab.y + lab.z * lab.z));
	float a_ = lab.y / C;
	float b_ = lab.z / C;	vec2 cusp = find_cusp(a_, b_);
	float L0 = cusp.x;
	float t = find_gamut_intersection(a_, b_, L, C, L0);
	float L_clipped = L0 * (1.f - t) + t * L;
	float C_clipped = t * C;
	return oklab_to_linear_srgb(vec3( L_clipped, C_clipped * a_, C_clipped * b_ ));
}
vec3 gamut_clip_adaptive_L0_0_5(vec3 rgb, float alpha){
	if (rgb.r < 1.f && rgb.g < 1.f && rgb.b < 1.f && rgb.r > 0.f && rgb.g > 0.f && rgb.b > 0.f)
		return rgb;
	vec3 lab = linear_srgb_to_oklab(rgb);
	float L = lab.x;
	float eps = 0.00001f;
	float C = max(eps, sqrt(lab.y * lab.y + lab.z * lab.z));
	float a_ = lab.y / C;
	float b_ = lab.z / C;
	float Ld = L - 0.5f;
	float e1 = 0.5f + abs(Ld) + alpha * C;
	float L0 = 0.5f * (1.f + sign(Ld) * (e1 - sqrt(e1 * e1 - 2.f * abs(Ld))));
	float t = find_gamut_intersection(a_, b_, L, C, L0);
	float L_clipped = L0 * (1.f - t) + t * L;
	float C_clipped = t * C;
	return oklab_to_linear_srgb(vec3( L_clipped, C_clipped * a_, C_clipped * b_ ));
}
vec3 gamut_clip_adaptive_L0_L_cusp(vec3 rgb, float alpha){
	if (rgb.r < 1.f && rgb.g < 1.f && rgb.b < 1.f && rgb.r > 0.f && rgb.g > 0.f && rgb.b > 0.f)
		return rgb;
	vec3 lab = linear_srgb_to_oklab(rgb);
	float L = lab.x;
	float eps = 0.00001f;
	float C = max(eps, sqrt(lab.y * lab.y + lab.z * lab.z));
	float a_ = lab.y / C;
	float b_ = lab.z / C;	vec2 cusp = find_cusp(a_, b_);
	float Ld = L - cusp.x;
	float k = 2.f * (Ld > 0.f ? 1.f - cusp.x : cusp.x);
	float e1 = 0.5f * k + abs(Ld) + alpha * C / k;
	float L0 = cusp.x + 0.5f * (sign(Ld) * (e1 - sqrt(e1 * e1 - 2.f * k * abs(Ld))));
	float t = find_gamut_intersection(a_, b_, L, C, L0);
	float L_clipped = L0 * (1.f - t) + t * L;
	float C_clipped = t * C;
	return oklab_to_linear_srgb(vec3( L_clipped, C_clipped * a_, C_clipped * b_ ));
}
float toe(float x){
	float k_1 = 0.206f;
	float k_2 = 0.03f;
	float k_3 = (1.f + k_1) / (1.f + k_2);
	return 0.5f * (k_3 * x - k_1 + sqrt((k_3 * x - k_1) * (k_3 * x - k_1) + 4.f * k_2 * k_3 * x));
}
float toe_inv(float x){
	float k_1 = 0.206f;
	float k_2 = 0.03f;
	float k_3 = (1.f + k_1) / (1.f + k_2);
	return (x * x + k_1 * x) / (k_3 * (x + k_2));
}
vec2 to_ST(vec2 cusp){
	float L = cusp.x;
	float C = cusp.y;
	return vec2( C / L, C / (1.f - L) );
}
vec2 get_ST_mid(float a_, float b_){
	float S = 0.11516993f + 1.f / (
		+7.44778970f + 4.15901240f * b_
		+ a_ * (-2.19557347f + 1.75198401f * b_
			+ a_ * (-2.13704948f - 10.02301043f * b_
				+ a_ * (-4.24894561f + 5.38770819f * b_ + 4.69891013f * a_
					)))
		);
	float T = 0.11239642f + 1.f / (
		+1.61320320f - 0.68124379f * b_
		+ a_ * (+0.40370612f + 0.90148123f * b_
			+ a_ * (-0.27087943f + 0.61223990f * b_
				+ a_ * (+0.00299215f - 0.45399568f * b_ - 0.14661872f * a_
					)))
		);
	return vec2( S, T );
}
vec3 get_Cs(float L, float a_, float b_){
	vec2 cusp = find_cusp(a_, b_);
	float C_max = find_gamut_intersection(a_, b_, L, 1.f, L, cusp);
	vec2 ST_max = to_ST(cusp);	float k = C_max / min((L * ST_max.x), (1.f - L) * ST_max.y);
	float C_mid;{
		vec2 ST_mid = get_ST_mid(a_, b_);		float C_a = L * ST_mid.x;
		float C_b = (1.f - L) * ST_mid.y;
		C_mid = 0.9f * k * sqrt(sqrt(1.f / (1.f / (C_a * C_a * C_a * C_a) + 1.f / (C_b * C_b * C_b * C_b))));
	}
	float C_0;{		float C_a = L * 0.4f;
		float C_b = (1.f - L) * 0.8f;		C_0 = sqrt(1.f / (1.f / (C_a * C_a) + 1.f / (C_b * C_b)));
	}
	return vec3( C_0, C_mid, C_max );
}
vec3 okhsl_to_srgb(vec3 hsl){
	float h = hsl.x;
	float s = hsl.y;
	float l = hsl.z;
	if (l == 1.0f){
		return vec3( 1.f, 1.f, 1.f );
	}
	else if (l == 0.f){
		return vec3( 0.f, 0.f, 0.f );
	}
	float a_ = cos(2.f * M_PI * h);
	float b_ = sin(2.f * M_PI * h);
	float L = toe_inv(l);
	vec3 cs = get_Cs(L, a_, b_);
	float C_0 = cs.x;
	float C_mid = cs.y;
	float C_max = cs.z;
	float mid = 0.8f;
	float mid_inv = 1.25f;
	float C, t, k_0, k_1, k_2;
	if (s < mid){
		t = mid_inv * s;
		k_1 = mid * C_0;
		k_2 = (1.f - k_1 / C_mid);
		C = t * k_1 / (1.f - k_2 * t);
	}
	else{
		t = (s - mid)/ (1.f - mid);
		k_0 = C_mid;
		k_1 = (1.f - mid) * C_mid * C_mid * mid_inv * mid_inv / C_0;
		k_2 = (1.f - (k_1) / (C_max - C_mid));
		C = k_0 + t * k_1 / (1.f - k_2 * t);
	}
	vec3 rgb = oklab_to_linear_srgb(vec3( L, C * a_, C * b_ ));
	return vec3(
		srgb_transfer_function(rgb.r),
		srgb_transfer_function(rgb.g),
		srgb_transfer_function(rgb.b)
	);
}
vec3 okhsl_to_linear_srgb(vec3 hsl){
	float h = hsl.x;
	float s = hsl.y;
	float l = hsl.z;
	if (l == 1.0f){
		return vec3( 1.f, 1.f, 1.f );
	}
	else if (l == 0.f){
		return vec3( 0.f, 0.f, 0.f );
	}
	float a_ = cos(2.f * M_PI * h);
	float b_ = sin(2.f * M_PI * h);
	float L = toe_inv(l);
	vec3 cs = get_Cs(L, a_, b_);
	float C_0 = cs.x;
	float C_mid = cs.y;
	float C_max = cs.z;
	float mid = 0.8f;
	float mid_inv = 1.25f;
	float C, t, k_0, k_1, k_2;
	if (s < mid){
		t = mid_inv * s;
		k_1 = mid * C_0;
		k_2 = (1.f - k_1 / C_mid);
		C = t * k_1 / (1.f - k_2 * t);
	}
	else{
		t = (s - mid)/ (1.f - mid);
		k_0 = C_mid;
		k_1 = (1.f - mid) * C_mid * C_mid * mid_inv * mid_inv / C_0;
		k_2 = (1.f - (k_1) / (C_max - C_mid));
		C = k_0 + t * k_1 / (1.f - k_2 * t);
	}
	return oklab_to_linear_srgb(vec3( L, C * a_, C * b_ ));
}
vec3 okhsl_to_xyz(vec3 hsl){
	float h = hsl.x;
	float s = hsl.y;
	float l = hsl.z;
	if (l == 1.0f){
		return vec3( 1.f, 1.f, 1.f );
	}
	else if (l == 0.f){
		return vec3( 0.f, 0.f, 0.f );
	}
	float a_ = cos(2.f * M_PI * h);
	float b_ = sin(2.f * M_PI * h);
	float L = toe_inv(l);
	vec3 cs = get_Cs(L, a_, b_);
	float C_0 = cs.x;
	float C_mid = cs.y;
	float C_max = cs.z;
	float mid = 0.8f;
	float mid_inv = 1.25f;
	float C, t, k_0, k_1, k_2;
	if (s < mid){
		t = mid_inv * s;
		k_1 = mid * C_0;
		k_2 = (1.f - k_1 / C_mid);
		C = t * k_1 / (1.f - k_2 * t);
	}
	else{
		t = (s - mid)/ (1.f - mid);
		k_0 = C_mid;
		k_1 = (1.f - mid) * C_mid * C_mid * mid_inv * mid_inv / C_0;
		k_2 = (1.f - (k_1) / (C_max - C_mid));
		C = k_0 + t * k_1 / (1.f - k_2 * t);
	}
	return oklab_to_xyz(vec3( L, C * a_, C * b_ ));
}
vec3 srgb_to_okhsl(vec3 rgb){
	vec3 lab = linear_srgb_to_oklab(vec3(
		srgb_transfer_function_inv(rgb.r),
		srgb_transfer_function_inv(rgb.g),
		srgb_transfer_function_inv(rgb.b)
		));
	float C = sqrt(lab.y * lab.y + lab.z * lab.z);
	float a_ = lab.y / C;
	float b_ = lab.z / C;
	float L = lab.x;
	float h = 0.5f + 0.5f * atan(-lab.z, -lab.y) / M_PI;
	vec3 cs = get_Cs(L, a_, b_);
	float C_0 = cs.x;
	float C_mid = cs.y;
	float C_max = cs.z;
	float mid = 0.8f;
	float mid_inv = 1.25f;
	float s;
	if (C < C_mid){
		float k_1 = mid * C_0;
		float k_2 = (1.f - k_1 / C_mid);
		float t = C / (k_1 + k_2 * C);
		s = t * mid;
	}
	else{
		float k_0 = C_mid;
		float k_1 = (1.f - mid) * C_mid * C_mid * mid_inv * mid_inv / C_0;
		float k_2 = (1.f - (k_1) / (C_max - C_mid));
		float t = (C - k_0) / (k_1 + k_2 * (C - k_0));
		s = mid + (1.f - mid) * t;
	}
	float l = toe(L);
	return vec3( h, s, l );
}
vec3 okhsv_to_srgb(vec3 hsv){
	float h = hsv.x;
	float s = hsv.y;
	float v = hsv.z;
	float a_ = cos(2.f * M_PI * h);
	float b_ = sin(2.f * M_PI * h);
	
	vec2 cusp = find_cusp(a_, b_);
	vec2 ST_max = to_ST(cusp);
	float S_max = ST_max.x;
	float T_max = ST_max.y;
	float S_0 = 0.5f;
	float k = 1.f- S_0 / S_max;	float L_v = 1.f   - s * S_0 / (S_0 + T_max - T_max * k * s);
	float C_v = s * T_max * S_0 / (S_0 + T_max - T_max * k * s);
	float L = v * L_v;
	float C = v * C_v;	float L_vt = toe_inv(L_v);
	float C_vt = C_v * L_vt / L_v;
	float L_new = toe_inv(L);
	C = C * L_new / L;
	L = L_new;
	vec3 rgb_scale = oklab_to_linear_srgb(vec3( L_vt, a_ * C_vt, b_ * C_vt ));
	float scale_L = cbrt(1.f / max(max(rgb_scale.r, rgb_scale.g), max(rgb_scale.b, 0.f)));
	L = L * scale_L;
	C = C * scale_L;
	vec3 rgb = oklab_to_linear_srgb(vec3( L, C * a_, C * b_ ));
	return vec3(
		srgb_transfer_function(rgb.r),
		srgb_transfer_function(rgb.g),
		srgb_transfer_function(rgb.b)
	);
})" R"(
vec3 srgb_to_okhsv(vec3 rgb){
	vec3 lab = linear_srgb_to_oklab(vec3(
		srgb_transfer_function_inv(rgb.r),
		srgb_transfer_function_inv(rgb.g),
		srgb_transfer_function_inv(rgb.b)
		));
	float C = sqrt(lab.y * lab.y + lab.z * lab.z);
	float a_ = lab.y / C;
	float b_ = lab.z / C;
	float L = lab.x;
	float h = 0.5f + 0.5f * atan(-lab.z, -lab.y) / M_PI;
	vec2 cusp = find_cusp(a_, b_);
	vec2 ST_max = to_ST(cusp);
	float S_max = ST_max.x;
	float T_max = ST_max.y;
	float S_0 = 0.5f;
	float k = 1.f - S_0 / S_max;
	float t = T_max / (C + L * T_max);
	float L_v = t * L;
	float C_v = t * C;
	float L_vt = toe_inv(L_v);
	float C_vt = C_v * L_vt / L_v;	vec3 rgb_scale = oklab_to_linear_srgb(vec3( L_vt, a_ * C_vt, b_ * C_vt ));
	float scale_L = cbrt(1.f / max(max(rgb_scale.r, rgb_scale.g), max(rgb_scale.b, 0.f)));
	L = L / scale_L;
	C = C / scale_L;
	C = C * toe(L) / L;
	L = toe(L);
	float v = L / L_v;
	float s = (S_0 + T_max) * C_v / ((T_max * S_0) + T_max * k * C_v);
	return vec3 (h, s, v );
}
vec3 sRGB2XYZ(vec3 color){
	mat3 mat=mat3(vec3(0.4124564,0.3575761,0.1804375),
				  vec3(0.2126729,0.7151522,0.0721750),
				  vec3(0.0193339,0.1191920,0.9503041));
	return color*mat;
}
vec3 Clay2XYZ(vec3 color){
	mat3 mat=mat3(vec3(0.5767309,0.1855540,0.1881852),
				  vec3(0.2973769,0.6273491,0.0752741),
				  vec3(0.0270343,0.0706872,0.9911085));
	return color*mat;
}
vec3 D65P32XYZ(vec3 color){
	mat3 mat=mat3(vec3(0.4865709,0.2656677,0.1982173),
				  vec3(0.2289746,0.6917385,0.0792869),
				  vec3(0.0000000,0.0451134,1.0439444));
	return color*mat;
}
vec3 XYZ2sRGB(vec3 xyz){
	mat3 mat=mat3(vec3(3.2404542,-1.5371385,-0.4985314),
				  vec3(-0.9692660,1.8760108,0.0415560),
				  vec3(0.0556434,-0.2040259,1.0572252));
	return xyz*mat;
}
vec3 XYZ2Clay(vec3 xyz){
	mat3 mat=mat3(vec3(2.0413690,-0.5649464,-0.3446944),
				  vec3(-0.9692660,1.8760108,0.0415560),
				  vec3(0.0134474,-0.1183897,1.0154096));
	return xyz*mat;
}
vec3 XYZ2D65P3(vec3 xyz){
	mat3 mat=mat3(vec3(2.4934969,-0.9313836,-0.4027108),
				  vec3(-0.8294890,1.7626641,0.0236247),
				  vec3(0.0358458,-0.0761724,0.9568845));
	return xyz*mat;
}
vec3 YUV2sRGB(vec3 yuv){
    yuv[0]=1.1643*(yuv[0]-0.0625);
    yuv[1]-=0.5; yuv[2]-=0.5;
	vec3 rgb;
    rgb[0] = (yuv[0] + 1.793 * yuv[2]);
    rgb[1] = (yuv[0] - 0.213 * yuv[1] - 0.533 * yuv[2]);
    rgb[2] = (yuv[0] + 2.112 * yuv[1]);
	return rgb;
}

#define htsize HalftoneSize
vec4 rgb2cmyk(vec3 rgb){
	vec4 cmyk; cmyk.w=1.-max(max(rgb.r,rgb.g),rgb.b);
	float k1=1.-cmyk.w;
	cmyk.r=(k1-rgb.r)/k1; cmyk.g=(k1-rgb.g)/k1; cmyk.b=(k1-rgb.b)/k1;
	return cmyk;
}
vec3 cmyk2rgb(vec4 cmyk){
	vec3 rgb; float k1=1.-cmyk.w;
	rgb.r=(1.-cmyk.r)*k1; rgb.g=(1.-cmyk.g)*k1; rgb.b=(1.-cmyk.b)*k1;
	return rgb;
}
float rand(vec2 co){
    return fract(sin(dot(co, vec2(12.9898, 78.233))) * 43758.5453);
}
float HalftoneSingle(float a,float ps,float theta,float sm){
	float psize=ps; vec2 ctr=vec2(psize/2.,psize/2.); vec2 pt=vec2(psize,psize);
    vec2 xy=gl_FragCoord.xy; xy=vec2(sin(theta)*xy.x-cos(theta)*xy.y,cos(theta)*xy.x+sin(theta)*xy.y);
	xy.x=xy.x+rand(xy)/1.; xy.y=xy.y+rand(xy)/1.; ivec2 xyi=ivec2(int(xy.x/psize),int(xy.y/psize));
	vec2 xyf=mod(xy,pt);
	float px1=(sm==1.)?(3.0f/psize):(0.000001);
	float cmp=(pow(a,1.)*psize/2.*(1.414+px1)); float fac=distance(xyf,ctr)/cmp;
	return smoothstep(1.+px1,1.-px1,fac);
}
vec4 halftone(vec4 color){
	vec4 cmyk=rgb2cmyk(color.rgb); float a=color.a*(gl_FragCoord.x/400.0f);
	cmyk.r=HalftoneSingle(cmyk.r,htsize,rad(15.0),1.);
	cmyk.g=HalftoneSingle(cmyk.g,htsize,rad(75.0),1.);
	cmyk.b=HalftoneSingle(cmyk.b,htsize,rad(0.),1.);
	cmyk.a=HalftoneSingle(cmyk.a,htsize,rad(45.0),1.);
	color.rgb=cmyk2rgb(cmyk);
	//color.a=HalftoneSingle(a,htsize,rad(30),0);
	return color;
}
)";


extern "C" const char* TNS_VERTEX_SIMPLE_MATCAP = R"(#version 310 es
precision highp float;
uniform mat4 mProjection;
uniform mat4 mModel;
uniform mat4 mView;

in vec4 vVertex;
in vec3 vNormal;
smooth out vec3 fNormal;

void main(){
    gl_Position = mProjection * mView  * mModel * vVertex;
    vec3 N = ( mView * mModel * vec4(vNormal,0)).xyz;
    fNormal = normalize(N);
})";

extern "C" const char* TNS_FRAGMENT_SIMPLE_MATCAP = R"(#version 310 es
precision highp float;
smooth in vec3 fNormal;
out vec4 outColor;
float Interpolate(float between1,float between2,float value1,float value2,float key){
    float i = (key-between1)/(between2-between1);
    return value1*(1.-i)+value2*i;
}
void main(){
    float value = dot(vec3(0,0,1),fNormal);
    if(value<0.65) value=0.15;
    else if(value>=0.65 && value<0.85) value=Interpolate(0.65,0.85,0.15,0.75,value);
    else if(value>=0.85 && value<0.95) value=0.75;
    else if(value>=0.95) value=0.9;
    outColor = vec4(vec3(0.84, 0.41, 0.16)*value,1);
})";

extern "C" const char* TNS_VERTEX_GRID = R"(#version 310 es
precision highp float;

uniform mat4 mProjection;
uniform mat4 mModel;
uniform mat4 mView;

in vec4 vVertex;
in vec4 vColor;
in vec2 vUV;
out vec4 fColor;
out vec2 uv;

void main(){
    vec4 pos = mProjection * mView * mModel * vVertex;
    gl_Position = pos;
    fColor = vColor;
    uv = vUV;
})";

extern "C" const char* TNS_FRAGMENT_TRANSPARNT_GRID = R"(#version 310 es
precision highp float;
in vec4 fColor;
in vec2 uv;
out vec4 outColor;
void main(){
    vec4 c = fColor;
    c.a = sin(uv.x)*sin(uv.y)>0.?c.a:0.;
    outColor = c;
})";

extern "C" const char* LA_IMM_VERTEX_SHADER = R"(#version 310 es
precision highp float;
uniform mat4 mProjection;
uniform mat4 mModel;
uniform mat4 mView;
uniform int DoOffset;
in vec4 vVertex;
in vec4 vColor;
in vec3 vNormal;
in vec2 vUV;
out vec4 fColor;
out vec2 fUV;
flat out vec3 fNormal;
out vec3 fGPos;
void main(){
	vec4 pos=mProjection * mView * mModel * vVertex;
	if(DoOffset!=0){ pos.xyw*=1.0+0.00005*float(DoOffset); }
    gl_Position = pos;
    fColor = vColor;
    fUV=vUV;
    fGPos=vec3((mModel * vVertex).xyz);
    fNormal= normalize((mModel * vec4(vNormal,0.)).xyz);
})";

extern "C" const char* LA_IMM_FRAGMENT_SHADER = R"(#version 310 es
#extension GL_ARB_shading_language_420pack : enable // uniform sampler binding
precision highp float;
precision highp int;
layout (binding=0) uniform highp sampler2D TexColor;
layout (binding=1) uniform highp sampler2DMS TexColorMS;
layout (binding=2) uniform highp usampler2D TexColorUI;
layout (binding=3) uniform highp sampler3D TexLut;
layout (binding=4) uniform highp sampler2D TexColor2;
layout (binding=5) uniform highp sampler2D TexColorUI2;
uniform int TexIsUI;
uniform int Tex2IsUI;
uniform int UseLut;
uniform int TextureMode;
uniform int ColorMode;
uniform int MultiplyColor;
uniform int SampleAmount;
uniform int UseNormal;
uniform float UseHalftone;
uniform float HalftoneSize;
uniform int InputColorSpace;
uniform int OutputColorSpace;
uniform int Composing;
uniform float ComposingGamma;
uniform float ComposingBlackpoint;
uniform int ShowStripes;
uniform float HCYGamma;

uniform vec3 uObjectPos;
uniform vec3 uViewPos;
uniform vec3 uViewDir;
in vec4 fColor;
in vec2 fUV;
flat in vec3 fNormal;
in vec3 fGPos;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec3 outGPos;

#with TNS_SHADER_COLOR_COMMON
#with TNS_SHADER_LIBRARY
#with TNS_GLES_UINT_TEXTURE

vec4 ConvertColorSpace(vec4 _rgba){
	vec3 color=_rgba.rgb;
    if(InputColorSpace!=OutputColorSpace){
        if(ColorMode==0){
                 if(InputColorSpace==0) color=to_linear_srgb(color);
            else if(InputColorSpace==1) color=to_linear_clay(color);
            else if(InputColorSpace==2) color=to_linear_srgb(color);
        } 
        vec3 xyz; if(ColorMode==1){ color.y=pow(color.y,max(HCYGamma,1.)); color=okhsl_to_linear_srgb(color); }
             if(InputColorSpace==1){ xyz=Clay2XYZ(color); }
        else if(InputColorSpace==0){ xyz=sRGB2XYZ(color); }
        else if(InputColorSpace==2){ xyz=D65P32XYZ(color); }
             if(OutputColorSpace==0){ color=to_log_srgb(XYZ2sRGB(xyz)); /*_rgba.a=srgb_transfer_function(_rgba.a);*/ }
        else if(OutputColorSpace==1){ color=to_log_clay(XYZ2Clay(xyz)); /*_rgba.a=pow(_rgba.a,1.0/2.19921875);*/ }
    	else if(OutputColorSpace==2){ color=to_log_srgb(XYZ2D65P3(xyz)); /*_rgba.a=pow(_rgba.a,1.0/2.19921875);*/ }
    }else{
        if(ColorMode==1){ color.y=pow(color.y,max(HCYGamma,1.)); color=okhsl_to_srgb(color); }
        else if(ColorMode==0){
			color = color;
		}else{
                 if(OutputColorSpace==0){ color=to_log_srgb(color); /*_rgba.a=srgb_transfer_function(_rgba.a);*/ }
            else if(OutputColorSpace==1){ color=to_log_clay(color); /*_rgba.a=pow(_rgba.a,1.0/2.19921875);*/ }
            else if(OutputColorSpace==2){ color=to_log_srgb(color); /*_rgba.a=pow(_rgba.a,1.0/2.19921875);*/ }
        }
    }
    if(ShowStripes!=0){
        if(color.r>1.00001||color.g>1.00001||color.b>1.00001||color.r<0.||color.g<0.||color.b<0.){ color=mix(color,vec3(0.5,0.5,0.5),(sin((gl_FragCoord.x+gl_FragCoord.y)/2.)>0.)?1.:0.5); }
    }
    return vec4(color,_rgba.a);
}

#ifdef GLES_UINT_TEXTURE
vec4 cunpack(uint d){
    return vec4(float(d&0xFFu)/255.,float((d>>8u)&0xFFu)/255.,float((d>>16u)&0xFFu)/255.,float((d>>24u)&0xFFu)/255.);
}
#endif

#ifdef GLES_UINT_TEXTURE
#define textureUI(sampler,uv) \
	cunpack(texture(sampler,uv).r)
#else
#define textureUI(sampler,uv) \
	vec4(texture(sampler,uv))/vec4(65535.)
#endif

vec4 texture1(vec2 uv){
	if(TexIsUI!=0){ vec4 c=textureUI(TexColorUI,uv); return c; }
	return texture(TexColor,uv);
}
vec4 texture2(){
	if(Tex2IsUI!=0){ vec4 c=texelFetch(TexColorUI2,ivec2(gl_FragCoord.xy),0); return c; }
	return texelFetch(TexColor2,ivec2(gl_FragCoord.xy),0);
}

void main(){
    vec4 color=vec4(1,0,1,1);
    if(TextureMode==0){ color = fColor; if(UseHalftone>0.) color.a=HalftoneSingle(color.a,htsize,rad(7.),0.); }
    else if(TextureMode==1){color = vec4(fColor.rgb,fColor.a*texture1(fUV.st).r);}
    else if(TextureMode==2){
        color=texture1(fUV.st);
        if(MultiplyColor!=0){color*=fColor;}
    }else if(TextureMode==3){
        color=vec4(0,0,0,0);
        ivec2 texSize = textureSize(TexColorMS);
        for(int i=0;i<SampleAmount;i++){ vec4 res=texelFetch(TexColorMS, ivec2(fUV * vec2(texSize)),i); if(res[0]>-1e19) color+=res; };
        color/=float(SampleAmount);
        if(MultiplyColor!=0){color*=fColor;}
    }else if(TextureMode==4){
        color=vec4(1,0,1,1); //deprecated
	}else if(TextureMode==5){
        vec4 color1=texture1(fUV.st); if(MultiplyColor!=0){color1*=fColor;}
		vec4 color2=texture2();
		color = color1+color2*(1.0-color1.a);
	}else if(TextureMode==6){
        vec4 color1=texture1(fUV.st); if(MultiplyColor!=0){color1*=fColor;}
		vec4 color2=texture2();
		color = color1+color2;
	}else if(TextureMode==101){ // YUYV
		ivec2 tsize = textureSize(TexColor,0);
		vec2 pixel = fUV.st * vec2(tsize);
		ivec2 tpixel = ivec2(pixel);
        vec4 yuyv = texture(TexColor,fUV.st); //texelFetch(TexColor,tpixel,0);
		vec3 yuv; yuv.yz=yuyv.yw;
		if(fract(pixel.x)<=1.){ yuv.x=yuyv.x; }else{ yuv.x=yuyv.z; }
		color=vec4(YUV2sRGB(yuv),1.0f);
        if(MultiplyColor!=0){color*=fColor;}
    }else if(TextureMode==102){ // UYVY
		ivec2 tsize = textureSize(TexColor,0);
		vec2 pixel = fUV.st * vec2(tsize);
		ivec2 tpixel = ivec2(pixel);
        vec4 uyvy = texture(TexColor,fUV.st); //texelFetch(TexColor,tpixel,0);
		vec3 yuv; yuv.yz=uyvy.xz;
		if(fract(pixel.x)<=1.){ yuv.x=uyvy.y; }else{ yuv.x=uyvy.w; }
		color=vec4(YUV2sRGB(yuv),1.0f);
        if(MultiplyColor!=0){color*=fColor;}
    }
    if(UseNormal!=0){
		color.a=HalftoneSingle(color.a,htsize,rad(7.),0.);
		if(color.a==0.) discard;
        float light_factor=dot(fNormal,vec3(0,0,1));
		vec3 vd=uViewDir;
		if(length(uViewDir)<0.5){ vd=fGPos-uViewPos; }
        float view=dot(fNormal,vd);
        float factor=abs(light_factor);
        if(light_factor*view>0.){ factor=0.; }
        color=vec4(color.rgb*mix(0.2,1.,factor),color.a);
        vec3 oNormal=fNormal; if(view<0.){ oNormal=-fNormal; }
        outNormal = oNormal;
    }
    color=ConvertColorSpace(color); color.a=clamp(color.a,0.,1.);
	if(Composing!=0){
		if(color.rgb!=vec3(0,0,0)){
			//vec3 cl=srgb_to_okhsl(color.rgb);
			//cl.z=cl.z/1.0f*(1.0f-ComposingBlackpoint)+ComposingBlackpoint;
			//cl.z=pow(cl.z,ComposingGamma);
			//color.rgb=okhsl_to_srgb(cl);
			color.rgb=pow(color.rgb,vec3(ComposingGamma,ComposingGamma,ComposingGamma));
			color.r=color.r/1.0f*(1.0f-ComposingBlackpoint)+ComposingBlackpoint;
			color.g=color.g/1.0f*(1.0f-ComposingBlackpoint)+ComposingBlackpoint;
			color.b=color.b/1.0f*(1.0f-ComposingBlackpoint)+ComposingBlackpoint;
		}else{
			color.rgb=vec3(ComposingBlackpoint,ComposingBlackpoint,ComposingBlackpoint);
		}
	}
	if(UseLut!=0){
		color.rgb = texture(TexLut,color.bgr*vec3(32.0/33.0)+vec3(1.0/64.0)).rgb;
	}
	if(UseHalftone>1e-6){ color=mix(color,halftone(color),UseHalftone); }
    outColor = color;
    outGPos = fGPos;
})";

extern "C" const char* LA_OBJECT_FRAGMENT_SHADER = R"(#version 310 es
precision highp float;
uniform int UseNormal;
uniform float UseHalftone;
uniform float HalftoneSize;

uniform vec3 uObjectPos;
uniform vec3 uViewPos;
uniform vec3 uViewDir;
in vec4 fColor;
in vec2 fUV;
flat in vec3 fNormal;
in vec3 fGPos;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec3 outGPos;

#with TNS_SHADER_COLOR_COMMON
#with TNS_SHADER_LIBRARY

void main(){
    vec4 color=fColor;
		{
#with TNS_SHADER_MATERIAL
		}
    if(UseNormal!=0){
		color.a=HalftoneSingle(color.a,htsize,rad(7),0);
		if(color.a==0) discard;
        float light_factor=dot(fNormal,vec3(0,0,1));
		vec3 vd=uViewDir;
		if(len(uViewDir)<0.5){ vd=fGPos-uViewPos; }
        float view=dot(fNormal,vd);
        float factor=abs(light_factor);
        if(light_factor*view>0){ factor=0; }
        color=vec4(color.rgb*mix(0.2,1.,factor),color.a);
        vec3 oNormal=fNormal; if(view<0){ oNormal=-fNormal; }
        outNormal = oNormal;
    }
	if(UseHalftone>1e-6){ color=mix(color,halftone(color),UseHalftone); }
    outColor = color; outGPos = fGPos;
})";

extern "C" const char* LA_FLOOR_VERTEX_SHADER = R"(#version 310 es
precision highp float;
uniform mat4 mProjection;
uniform mat4 mModel;
uniform mat4 mView;
in vec4 vVertex;
in vec4 vColor;
out vec3 fGPos;
out vec4 fColor;
void main(){
    gl_Position=mProjection * mView * mModel * vVertex;
    fGPos=vec3((mModel * vVertex).xyz);
	fColor=vColor;
})";

extern "C" const char* LA_FLOOR_FRAGMENT_SHADER = R"(#version 310 es
precision highp float;
uniform vec3 uViewPos;
uniform float uFar;
in vec4 fColor;
in vec3 fGPos;
layout(location = 0) out vec4 outColor;
void main(){
	float fac=1.-pow(clamp(length(uViewPos-fGPos)/uFar,0.,1.),0.4);
	outColor=vec4(fColor.rgb,fColor.a*fac);
})";

extern "C" const char* LA_RAY_VERTEX_SHADER = R"(#version 310 es
precision highp float;
in vec3 vUV;
in vec4 vVertex;
out vec3 fViewDir;
void main(){
    gl_Position=vVertex;
    fViewDir = vUV;
})";

extern "C" const char* LA_SHADER_LIB_FXAA = R"(
#define DIFF_LUM_ABS_HOLD 0.0833 
#define DIFF_LUM_RES_HOLD 0.166
float luminance(vec3 col) {
    return dot(col, vec3(0.2126729f,  0.7151522f, 0.0721750f));
}
vec4 fxaa(in sampler2D tex, vec2 uv, vec2 texsize) {
    vec3 e = vec3(-1., 1., 0.);
    vec2 offuv = uv;
	vec3 colnw = texture(tex, uv + e.xy / texsize).rgb;
    vec3 coln  = texture(tex, uv + e.zy / texsize).rgb;
    vec3 colne = texture(tex, uv + e.yy / texsize).rgb;
    vec3 colw  = texture(tex, uv + e.xz / texsize).rgb;
    vec4 colm4  = texture(tex, uv + e.zz / texsize);
    vec3 colm  = colm4.rgb;
    vec3 cole  = texture(tex, uv + e.yz / texsize).rgb;
    vec3 colsw = texture(tex, uv + e.xx / texsize).rgb;
    vec3 cols  = texture(tex, uv + e.zx / texsize).rgb;
    vec3 colse = texture(tex, uv + e.yx / texsize).rgb;
    float lnw = luminance(colnw), ln = luminance(coln), lne = luminance(colne),
          lw  = luminance(colw),  lm = luminance(colm), le  = luminance(cole),
          lsw = luminance(colsw), ls = luminance(cols), lse = luminance(colse);
    float maxl = max(ln, max(ls, max(lw, max(le, lm))));
    float minl = min(ln, min(ls, min(lw, min(le, lm))));
    float diff = maxl - minl;
    if (diff < max(DIFF_LUM_ABS_HOLD, DIFF_LUM_RES_HOLD * maxl)) return colm4;
    float filterfactor = 0.;
    filterfactor += 2. * (ln + lw + ls + le) + lnw + lne + lsw + lse;
    filterfactor /= 12.;
    filterfactor = abs(filterfactor - lm);
    filterfactor = clamp(filterfactor / diff, 0., 1.);
    float blend = smoothstep(0., 1., filterfactor);
    blend *= blend;
    float hedge = 2.*(ln + ls - 2.*lm) + (lne + lse - 2.*le) + (lnw + lsw - 2.*lw);
    float vedge = 2.*(le + lw - 2.*lm) + (lne + lnw - 2.*ln) + (lse + lsw - 2.*ls);
    float ish = step(vedge, hedge);
    float psoff = ish >= 1.0 ? 1./texsize.y : 1./texsize.x;
    float pleft = ish >= 1.0 ? ln : le;
    float pright = ish >= 1.0 ? ls : lw;
    if (abs(pleft - lm) < abs(pright - lm)) psoff = -psoff;
    if (ish >= 1.0) { offuv.y += psoff * blend; }else{ offuv.x += psoff * blend; }
    return vec4(texture(tex, offuv).rgb,colm4.a);  
})";

extern "C" const char* LA_RAY_FRAGMENT_SHADER = R"(#version 310 es
precision highp float;
uniform vec3 uViewDir;
uniform vec3 uViewPos;
uniform float uFOV;
in vec3 fViewDir;
uniform highp sampler2D TexColor;
uniform highp sampler2D TexNormal;
uniform highp sampler2D TexGPos;
#with LA_SHADER_LIB_FXAA
out vec4 outColor;
void main(){
    float d=dot(uViewDir,normalize(fViewDir));
    float target=cos(uFOV/2.);
    vec4 color=vec4(1.,1.,1.,1.); float mul=0.;
    //if(d<(target+0.005)&&d>target) mul=1.0;
    vec2 uv=gl_FragCoord.xy/vec2(textureSize(TexColor,0));
    vec4 buffer_color=fxaa(TexColor,uv,vec2(textureSize(TexColor,0)));
    //vec4 buffer_color=texture(TexColor,uv);
    outColor = mul*color+buffer_color;
})";

extern "C" const char* LA_SCENE_VERTEX_SHADER = R"(#version 310 es
precision highp float;
uniform mat4 mProjection;
uniform mat4 mModel;
uniform mat4 mView;
uniform mat4 mShadow;
in vec4 vVertex;
in vec4 vColor;
in vec4 vNormal;
in vec2 vUV;
out vec4 fColor;
//out vec4 fNormal;
out vec2 fUV;
out vec4 fGPos;
void main(){
    gl_Position= mProjection * mView * mModel * vVertex;
    fUV=vUV;
    //fNormal=vNormal;
    fColor=vColor;
    fGPos= mShadow * mModel * vVertex;\
})";

extern "C" const char* LA_SCENE_FRAGMENT_SHADER = R"(#version 310 es
precision highp float;
uniform highp sampler2D TexColor;
uniform highp sampler2DMS TexColorMS;\
uniform int TextureMode;
uniform int SampleAmount;
uniform int MultiplyColor;
in vec4 fColor;
//in vec4 fNormal;
in vec2 fUV;
in vec4 fGPos;
out vec4 outColor;
vec4 GetTexture(vec2 uv){
    vec4 color=vec4(1,0,1,1);
    if(TextureMode==1 || TextureMode==2){ return texture(TexColor,uv); }
    else if(TextureMode==3){
        ivec2 texSize = textureSize(TexColorMS);
        for(int i=0;i<SampleAmount;i++) color+=texelFetch(TexColorMS, ivec2(fUV * vec2(texSize)),i);
        color/=float(SampleAmount);
        if(MultiplyColor!=0){color*=fColor;}
        return color;
    }
    else return vec4(1,0,1,1);
}
float GetShadow(vec4 GPos){
    vec3 projCoords = GPos.xyz / GPos.w;
    projCoords = projCoords * 0.5 + 0.5;
    float closestDepth = GetTexture(projCoords.xy).r;
    float currentDepth = projCoords.z;
    float shadow = currentDepth > (closestDepth+0.001)  ? 0.5 : 1.0;
    return shadow;
}
void main(){
    outColor=GetShadow(fGPos)*fColor;
})";

extern "C" const char* LA_CASCADE_SHADOW_VERTEX_SHADER = R"(#version 310 es
precision highp float;
in vec4 vVertex;
uniform mat4 mModel;
uniform mat4 mShadow;
void main(){
    gl_Position=mShadow*mModel*vVertex;
})";

extern "C" const char* LA_CASCADE_SHADOW_FRAGMENT_SHADER = "#version 310 es\n \
precision highp float;\nvoid main(){gl_FragDepth = gl_FragCoord.z;}";

extern "C" const char* LA_SELECTION_VERTEX_SHADER = R"(#version 310 es
precision highp float;
in vec4 vVertex;
in vec3 vColor;
uniform mat4 mProjection;
uniform mat4 mModel;
uniform mat4 mView;
uniform int DoOffset;
flat out vec3 fIdColor;
void main(){
	vec4 pos = mProjection * mView * mModel * vVertex;
	if(DoOffset!=0){ pos.xyw*=1.00005; }
    gl_Position = pos;
    fIdColor = vColor;
})";

extern "C" const char* LA_SELECTION_FRAGMENT_SHADER = R"(#version 310 es
precision highp float;
flat in vec3 fIdColor;
out vec4 outColor;
void main(){
    outColor=vec4(fIdColor,1.);
})";

