/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../la_5.h"

extern LA MAIN;
extern struct _tnsMain *T;

//============================================================= [Draw]

extern tnsFontManager *FM;

void la_SetCanvasOrtho(laCanvasExtra* e,int W, int H){
    real x2=(real)W*e->ZoomX/2,y2=(real)H*e->ZoomY/2;
    tnsOrtho(e->PanX-x2,e->PanX+x2,e->PanY-y2,e->PanY+y2,-100,100);
    tnsVector3d dir={0,0,-1}; tnsSetCameraViewDir(dir);
}
void la_SetCanvasOrthoFixed(laCanvasExtra* e,int W, int H){
    real x2=(real)W/LA_RH/2,y2=(real)H/LA_RH/2;
    tnsOrtho(-x2,+x2,-y2,+y2,-100,100);
    tnsVector3d dir={0,0,-1}; tnsSetCameraViewDir(dir);
}
void la_BeginNVG(NVGcontext* vg,laCanvasExtra* e,int W,int H, int Is2D){
    nvgBeginFrame(vg,W,H,1);
    if(Is2D){
        nvgTranslate(vg,-e->PanX/e->ZoomX+W/2,e->PanY/e->ZoomY+H/2);
        nvgScale(vg,1.0f/e->ZoomX,1.0f/e->ZoomY);
    }else{
        nvgTranslate(vg,W/2,H/2);
        nvgScale(vg,1,-1);
    }
}
void la_BeginNVGFixed(NVGcontext* vg,laCanvasExtra* e,int W,int H){
    nvgBeginFrame(vg,W,H,1); nvgTranslate(vg,W/2,H/2); nvgScale(vg,LA_RH,LA_RH);
}
void la_BeginNVGDynamic(NVGcontext* vg,laCanvasExtra* e,int W,int H, int Is2D, int OverrideFixed){
    if(OverrideFixed)la_BeginNVGFixed(vg,e,W,H);
    else la_BeginNVG(vg,e,W,H,Is2D);
}

void la_RootObjectDrawFullscreenQuad(tnsOffscreen* DeferredOffScr,tnsCamera* c, real Aspect){
    real FOV = c->FOV;
    tnsMatrix44d mat;
    tnsGetMVMatrix(mat);
    real vv[3]={0,0,-1};
    real ViewDir[3]={0},vdTR[3]={0},vdBR[3]={0},vdTL[3]={0},vdBL[3]={0};
    tnsApplyRotation43d(ViewDir,mat,vv);
    real fovv=FOV/2, fovh=fovh=atan(tan(FOV/2)*Aspect);
    vv[0]=tan(fovh); vv[1]=tan(fovv); tnsApplyRotation43d(vdTR,mat,vv); // note don't normalize those.
    vv[0]=tan(fovh); vv[1]=tan(-fovv);tnsApplyRotation43d(vdBR,mat,vv);
    vv[0]=tan(-fovh);vv[1]=tan(-fovv);tnsApplyRotation43d(vdBL,mat,vv);
    vv[0]=tan(-fovh);vv[1]=tan(fovv); tnsApplyRotation43d(vdTL,mat,vv);
    tnsUseRayShader();
    tnsEnableShaderv(T->RayShader);
    tnsSetRayShaderUniformTextures(DeferredOffScr);
    real uv[12]; tnsMakeQuad3d(uv, LA_COLOR3(vdTL), LA_COLOR3(vdTR), LA_COLOR3(vdBR), LA_COLOR3(vdBL));
    real vt[8]; tnsMakeQuad2d(vt, -1,1, 1,1, 1,-1, -1,-1);
    tnsTexCoordArray3d(uv,4);
    tnsVertexArray2d(vt,4);
    tnsPackAs(GL_TRIANGLE_FAN);
    glUniform3f(T->RayShader->uViewDir,LA_COLOR3(ViewDir));
    glUniform3f(T->RayShader->uViewPos,LA_COLOR3(c->Base.Location));
    glUniform1f(T->RayShader->uFOV,FOV);
    tnsFlush();
    tnsUseImmShader();
}

#define LA_DEPTH_RESOLUTION 1024

void la_3DViewEnsureCamera(laCanvasExtra* e);

void la_RootObjectDraw(laBoxedTheme *bt, tnsObject *root, laUiItem* ui){
    laCanvasExtra *e = ui->Extra;
    la_3DViewEnsureCamera(e);
    tnsCamera *c = e->UsingCamera ? e->UsingCamera : e->ViewingCamera;
    int W, H; W = ui->R - ui->L; H = ui->B - ui->U; if (W<=0 || H<=0) return;

    tnsFlush();

    if (!e->OffScr){
        //e->OffScr = tnsCreateDeferredOffscreen(W,H,MAIN.PanelMultisample);
        e->OffScr = tnsCreate2DOffscreen(GL_RGBA, W, H, MAIN.PanelMultisample ,1, 0);
    }else{ tnsEnsureOffscreenStatus(e->OffScr, ui->R - ui->L, ui->B - ui->U); }

    if (0){//(e->CurrentScene && e->CurrentScene->ActiveSun){
        //if(!e->OffScrShadow) e->OffScrShadow = tnsCreate2DOffscreen(0, LA_DEPTH_RESOLUTION, LA_DEPTH_RESOLUTION, MAIN.PanelMultisample, 1, 0);

        //tnsUseNoTexture();
        //tnsDrawToOffscreen(e->OffScrShadow, 1, TNS_ATTACHMENT_ARRAY_NONE);
        //tnsViewportWithScissor(0,0,LA_DEPTH_RESOLUTION,LA_DEPTH_RESOLUTION);
        //glEnable(GL_DEPTH_TEST);
        //glClearDepth(1.0); glClear(GL_DEPTH_BUFFER_BIT);
        //tnsResetViewMatrix();tnsResetModelMatrix();tnsResetProjectionMatrix();
        //tnsEnableShaderv(T->ShadowShader);
        //tnsUseShadowShader();
        //tnsApplyShadowCameraView(e->CurrentScene->ActiveSun);
        //
        //real p[12];
        //tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        //tnsMakeQuad3d(p, -100,-100, 25,-100,100, 25,100,100, 25,100,-100, 25);
        //tnsVertexArray3d(p, 4); tnsPackAs(GL_TRIANGLE_FAN);
        //tnsMakeQuad3d(p, -100,-100, 74,-100,100, 74,100,100, 74,100,-100, 74);
        //tnsVertexArray3d(p, 4); tnsPackAs(GL_TRIANGLE_FAN);
        ////tnsDrawFloor(e->ViewingCamera->Base.GLocation, e->ViewingCamera->ZMax, e->ShowAxis);
        //tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        //tnsDrawFloor(e->ViewingCamera->Base.GLocation, e->ViewingCamera->ZMax, e->ShowAxis);
        //tnsEnableShaderv(T->immShader);
        //tnsFlush();

        tnsDrawToOffscreen(e->OffScr,1,0);

        tnsViewportWithScissor(0, 0, W, H);
        tnsClearColorv(laThemeColor(bt,LA_BT_NORMAL));
        tnsClearAll();

        //tnsUseSceneShader();
        //tnsEnableShaderv(T->SceneShader);
        //tnsApplyShadowCameraView(e->CurrentScene->ActiveSun);
        tnsApplyCameraView(W, H, c,0,0);
//
        //tnsUseTexture(e->OffScrShadow->pDepth);
//
        //tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        //tnsMakeQuad3d(p, -100,-100, 25,-100,100, 25,100,100, 25,100,-100, 25);
        //tnsVertexArray3d(p, 4); tnsPackAs(GL_TRIANGLE_FAN);
        //tnsMakeQuad3d(p, -100,-100, 74,-100,100, 74,100,100, 74,100,-100, 74);
        //tnsVertexArray3d(p, 4); tnsPackAs(GL_TRIANGLE_FAN);

        //tnsUseNoTexture();
        //if (e->ShowFloorGrid){
        //    tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        //    tnsDrawFloor(e->ViewingCamera->Base.GLocation, e->ViewingCamera->ZMax, e->ShowAxis);
        //    tnsFlush();
        //}
//
        //tnsFlush();

    }else if(e->OffScrShadow){ tnsDelete2DOffscreen(e->OffScrShadow); }

    //{
    //    tnsDrawToOffscreen(e->OffScr, 1, 0);
    //    //tnsUseShader(T->TEST_MatcapShader);
    //    tnsEnableShaderv(T->TEST_MatcapShader);
    //    tnsViewportWithScissor(0, 0, W, H);
    //    tnsClearColorv(laThemeColor(bt,LA_BT_NORMAL));
    //    tnsClearAll();
    //}

    tnsDrawToOffscreen(e->OffScr,1,TNS_ATTACHMENT_ARRAY);
    //tnsDrawToOffscreen(e->DeferredOffScr,3,TNS_ATTACHMENT_ARRAY_0_1_2);
    tnsViewportWithScissor(0, 0, W, H); glDepthMask(GL_TRUE);
    tnsClearColorv(laThemeColor(bt,LA_BT_NORMAL)); tnsClearAll(); float gpos_clear[]={-1e20, -1e20, -1e20, 0};
    glClearBufferfv(GL_COLOR, 2, gpos_clear);

    la3DObjectDrawExtra de={0};
    de.MeshEditType=e->SelectMode; de.DisplayMode=e->AsPlayer?LA_CANVAS_DISPLAY_MATERIAL:e->DisplayMode;
    de.W=W; de.H=H; de.PointScale=1.0f/LA_RH;

    tnsRootObject* ro=root;
    NVGcontext* vg=MAIN.CurrentWindow->nvg;
    tnsMatrix44d mview,mproj;
    if(ro && ro->Is2D){ la_SetCanvasOrtho(e,W,H); de.PointScale=e->ZoomX; }
    else{
        if(ro && ro->ActiveCamera && e->AsPlayer){ c=ro->ActiveCamera; }
        tnsApplyCameraView(W, H, c, mview,mproj); tnsMultiply44d(de.mViewProj,mproj,mview); de.Is3D=1;
    }

    tnsPushMatrix(); tnsPopMatrix(); //those are necessary when ui is the first in list;

    if(root){
        tnsPushMatrix();
        if(!e->AsPlayer){
            tnsSetObjectTreeEvaluationArgs(root,root->Active,1,1,W,H);
            tnsEvaluateObjectTree(root,0,0);
        }

        if(ro && e->AsPlayer && ro->ActiveCamera && !ro->Is2D){ c=ro->ActiveCamera; c=c->Base.PlayDuplicate?c->Base.PlayDuplicate:c;
            tnsApplyCameraView(W, H, c,0,0);
        }

        for(int i=0;i<2;i++){
            int Do2DInstance=(i==1)?TNS_EVAL_LAYER_SHOW_2D:0;
            int Is2D=(i==1)?1:ro->Is2D;
            if (Do2DInstance){
                la_SetCanvasOrthoFixed(e,W,H); de.PointScale=1.0f/LA_RH; de.Is3D=0;
                tnsResetViewMatrix(); tnsShaderApplyView(T->BindedShader,tnsGetViewMatrix());
            }
            la_BeginNVGDynamic(vg,e,W,H,Is2D,Do2DInstance);
            tnsDrawObjectTree(root,TNS_EVAL_LAYER_BACKDROP|Do2DInstance,&de,e->AsPlayer);
            nvgEndFrame(vg); tnsRestoreFromNanoVG();
            la_BeginNVGDynamic(vg,e,W,H,Is2D,Do2DInstance);

            glEnable(GL_DEPTH_TEST); glDisable(GL_BLEND);

            tnsUseShader(T->immShader); tnsEnableShaderv(T->immShader); tnsUseNoTexture();
            tnsUnbindTexture(); tnsUniformUseTexture(T->immShader,0,0,0,0); tnsUseMultiplyColor(0);
            tnsDrawObjectTree(root,TNS_EVAL_LAYER_SOLID|Do2DInstance,&de,e->AsPlayer);
            
            glLineWidth(7);  tnsUniformUseOffset(T->immShader,-100);
            glDepthMask(GL_FALSE);
#ifndef LAGUI_ANDROID
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
#endif
            tnsDrawObjectTree(root,TNS_EVAL_LAYER_OUTLINE|Do2DInstance,0,e->AsPlayer);
            glDepthMask(GL_TRUE);
#ifndef LAGUI_ANDROID
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
#endif
            glLineWidth(1); tnsUniformUseOffset(T->immShader,0);

            tnsEvaluateData* ed=e->AsPlayer?(&root->EvaluatedPlay):(&root->Evaluated);
            if(!ed->Has2D){ break; }
            nvgEndFrame(vg); tnsRestoreFromNanoVG();
        }
        tnsPopMatrix();
    }

    if(!(ro && ro->Is2D)){ de.Is3D=1; tnsVector3d dir={0,0,0}; tnsSetCameraViewDir(dir); }

    if (e->ShowFloorGrid && (!e->AsPlayer)){
        tnsUseNoTexture();
        real* color=laThemeColor(bt,LA_BT_BORDER); tnsColor4d(LA_COLOR3(color),0.4);
        tnsDrawFloor(e->ViewingCamera->Base.GLocation, e->ViewingCamera->ZMax, e->ShowAxis);
        tnsFlush();
    }

    if(e->SelectThrough){ glClear(GL_DEPTH_BUFFER_BIT); }
    tnsDrawObjectTree(root,TNS_EVAL_LAYER_OVERLAY,&de,e->AsPlayer);

    if(root && (!e->AsPlayer)){
        glDisable(GL_DEPTH_TEST);
        glPointSize(8); tnsDrawCursor(root);
        tnsDrawObjectOrigins(root,root->Active,0); tnsFlush();
        glPointSize(1); glEnable(GL_DEPTH_TEST);
    }

    //laInvoke(0,"M_select",e,&ui->ExtraPP,0,0);

    //glColorMask(GL_FALSE, GL_FALSE,GL_FALSE,GL_FALSE);
    //tnsEvaluateObjectTree(e->CurrentScene, 0,0);

    tnsFlush();
    //glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

    glDisable(GL_DEPTH_TEST); glEnable(GL_BLEND);

    nvgEndFrame(vg); tnsRestoreFromNanoVG();

    //tnsDrawToOffscreen(e->OffScr, 1,0);
    //tnsClearColorv(laThemeColor(bt,LA_BT_NORMAL)); tnsClearAll();
    //la_RootObjectDrawFullscreenQuad(e->DeferredOffScr, c, (real)W/(real)H);
}
void la_RootObjectDrawOverlay(laUiItem *ui, int h){
    laCanvasExtra *e = ui->Extra;
    laBoxedTheme *bt = (*ui->Type->Theme);

    tnsUseHalftone(MAIN.ViewportHalftoneFactor);
    tnsUniformHalftoneSize(T->immShader,MAIN.ViewportHalftoneSize);
    tnsDraw2DTextureDirectly(e->OffScr->pColor[0], ui->L, ui->U, ui->R - ui->L, ui->B - ui->U);
    tnsUseHalftone(0);

    //if(e->OffScrShadow){
        //tnsDraw2DTextureDirectly(e->OffScrShadow->pDepth, ui->L, ui->U, ui->R - ui->L, ui->B - ui->U);
    //}

    tnsUseNoTexture();

    if (e->DrawCursor){
        if(e->DrawCursor==LA_CANVAS_CURSOR_ARROW){
            tnsColor4dv(laThemeColor(bt,LA_BT_TEXT));
            tnsVertex2d(e->OnX, e->OnY); tnsVertex2d(e->TargetX, e->TargetY);
            tnsPackAs(GL_LINES);
        }else{
            if(e->DrawCursor==LA_CANVAS_CURSOR_BOX){
                tnsColor4dv(laAccentColor(LA_BT_NORMAL));
                tnsVertex2d(e->ClickedX, e->ClickedY); tnsVertex2d(e->ClickedX, e->OnY);
                tnsVertex2d(e->OnX, e->OnY); tnsVertex2d(e->OnX, e->ClickedY);
                tnsPackAs(GL_TRIANGLE_FAN);
                tnsColor4dv(laAccentColor(LA_BT_TEXT));
                tnsVertex2d(e->ClickedX, e->ClickedY); tnsVertex2d(e->ClickedX, e->OnY);
                tnsVertex2d(e->OnX, e->OnY); tnsVertex2d(e->OnX, e->ClickedY);
                tnsPackAs(GL_LINE_LOOP);
            }
            tnsColor4dv(laThemeColor(bt,LA_BT_TEXT));
            int drawx=(e->DrawCursor==LA_CANVAS_CURSOR_CROSS)||(e->DrawCursor==LA_CANVAS_CURSOR_X);
            int drawy=(e->DrawCursor==LA_CANVAS_CURSOR_CROSS)||(e->DrawCursor==LA_CANVAS_CURSOR_Y);
            if(drawx){ tnsVertex2d(e->OnX, ui->U); tnsVertex2d(e->OnX, ui->B); }
            if(drawy){ tnsVertex2d(ui->L, e->OnY); tnsVertex2d(ui->R, e->OnY); }
            tnsPackAs(GL_LINES);
        }
    }

    if(MAIN.CurrentWindow->MaximizedUi!=ui){
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        tnsVertex2d(ui->L, ui->U);
        tnsVertex2d(ui->R, ui->U);
        tnsVertex2d(ui->R, ui->B);
        tnsVertex2d(ui->L, ui->B);
        tnsPackAs(GL_LINE_LOOP);
    }

    tnsFlush();

    char* MaxIco=(MAIN.CurrentWindow->MaximizedUi!=ui)?"🡹":"🡻";
    tnsVector4d color; tnsVectorCopy4d(laThemeColor(bt,LA_BT_TEXT),color);
    tnsVector4d colork; tnsVectorCopy4d(laThemeColor(bt,LA_BT_NORMAL),colork);
    color[3]=ui->State==LA_UI_NORMAL?0.0:(ui->State==LA_UI_ACTIVE?0.5:1.0);
    colork[3]=ui->State==LA_UI_NORMAL?0.0:(ui->State==LA_UI_ACTIVE?0.5:1.0);
    int startx=(ui->R+ui->L)/2-LA_RH2*2;
    int offs=LA_RH/12;
    
    tnsDrawStringAuto("☰",colork,startx+offs, startx+LA_RH+offs, ui->B-LA_M-LA_RH+offs, LA_TEXT_ALIGN_CENTER);
    tnsDrawStringAuto("☰",color,startx, startx+LA_RH, ui->B-LA_M-LA_RH, LA_TEXT_ALIGN_CENTER);
    tnsDrawStringAuto(MaxIco,colork,startx+LA_RH+offs, startx+LA_2RH+offs, ui->B-LA_M-LA_RH+offs, LA_TEXT_ALIGN_CENTER);
    tnsDrawStringAuto(MaxIco,color,startx+LA_RH, startx+LA_2RH, ui->B-LA_M-LA_RH, LA_TEXT_ALIGN_CENTER);

    if(ui->Expand>=0 && ui!=MAIN.CurrentWindow->MaximizedUi){
        tnsDrawStringAuto("◿",colork,ui->R-LA_RH+offs, ui->R+offs, ui->B-LA_M-LA_RH+offs, LA_TEXT_ALIGN_CENTER);
        tnsDrawStringAuto("◿",laThemeColor(bt,LA_BT_BORDER),ui->R-LA_RH, ui->R, ui->B-LA_M-LA_RH, LA_TEXT_ALIGN_CENTER);
    }
    tnsFlush();
}

void la_CanvasDrawTexture(laBoxedTheme *bt, tnsTexture *t, laUiItem* ui){
    laCanvasExtra* e=ui->Extra;
    int W, H; W = ui->R - ui->L; H = ui->B - ui->U;
    if (W<=0 || H<=0) return;

    tnsFlush();

    if (!e->OffScr || e->OffScr->pColor[0]->Height != ui->B - ui->U || e->OffScr->pColor[0]->Width != ui->R - ui->L){
        if (e->OffScr) tnsDelete2DOffscreen(e->OffScr);
        e->OffScr = tnsCreate2DOffscreen(GL_RGBA, W, H, MAIN.PanelMultisample, 1, 0);
    }

    tnsDrawToOffscreen(e->OffScr, 1, 0);
    tnsViewportWithScissor(0, 0, W, H);
    tnsResetViewMatrix();tnsResetModelMatrix();tnsResetProjectionMatrix();
    la_SetCanvasOrtho(e,W,H);

    //glClearColor(0.3,0.3,0.3, 1);
    if (e->ClearBackground){ tnsClearColorv(laThemeColor(bt,LA_BT_NORMAL)); glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); }

    // above is basic routine setup

    real V[8] = {0};
    real UV[8] = {0};

    if (t){
        real w2=t->Width/2, h2=t->Height/2;
        tnsDraw2DTextureDirectly(t, -w2, h2, t->Width, -t->Height);
        tnsFlush();

        if (e->ImageDrawBorder){
            tnsUseNoTexture();
            tnsMakeQuad2d(V, -w2, -h2, -w2, h2, w2, h2, w2, -h2);
            tnsVertexArray2d(V, 4);
            tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
            tnsVertexArray2d(V, 4);
            tnsPackAs(GL_LINE_LOOP);
            tnsFlush();
        }
    }
}
real la_CanvasDrawGetDisplayStep(real Max, real Min, int AreaSize){
    real r;
    real Step = 1.0;
    real DispW;
    real Div[3] = {2.0, 2.5, 2.0};
    int Times = 302;

    while (1){
        Times++;
        r = tnsGetRatiod(0, Max - Min, Step);
        DispW = tnsLinearItp(0, AreaSize, r);
        if (DispW > 100){
            Step /= Div[Times % 3];
        }elif (DispW < 40){
            Step *= Div[Times % 3];
        }else break;
    }
    return Step;
}
void la_CanvasDrawGridW(real Ratio, int RealW, real Min, real Max, real U, real B){
    real t;
    real Step = la_CanvasDrawGetDisplayStep(Max, Min, RealW);

    t = (real)((int)(Min / Step)) * Step;
    for (t; t <= Max; t += Step){
        tnsVertex2d(t, U);
        tnsVertex2d(t, B);
        tnsPackAs(GL_LINES);
    }
}
void la_CanvasDrawGridH(real Ratio, int RealH, real Min, real Max, real L, real R){
    real t;
    real Step = la_CanvasDrawGetDisplayStep(Max, Min, RealH);

    t = (real)((int)(Min / Step)) * Step;
    for (t; t <= Max; t += Step){
        tnsVertex2d(L, t);
        tnsVertex2d(R, t);
        tnsPackAs(GL_LINES);
    }
}
void la_CanvasDrawRulerW(real Ratio, laBoxedTheme *bt, int RealW, real Min, real Max, int B){
    char buf[20] = {0};
    real t, IL;
    real Step = la_CanvasDrawGetDisplayStep(Max, Min, RealW);
    int U = B - LA_RH;

    tnsUseNoTexture();
    tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
    tnsVertex2d(0, U);
    tnsVertex2d(RealW, U);
    tnsVertex2d(RealW, B);
    tnsVertex2d(0, B);
    tnsPackAs(GL_TRIANGLE_FAN);
    tnsColor4dv(laThemeColor(bt,LA_BT_TEXT_ACTIVE));
    tnsVertex2d(0, U + 0.5);
    tnsVertex2d(RealW, U + 0.5);
    tnsPackAs(GL_LINES);

    t = (real)((int)(Min / Step)) * Step;
    for (t; t <= Max; t += Step){
        IL = tnsGetRatiod(Min, Max, t) * (RealW);
        sprintf(buf, "%.0f", t);
        tnsDrawStringAuto(buf, bt->Active, IL, IL + 100, U, 0);
    }
}
void la_CanvasDrawRulerRWithBkg(real Ratio, laBoxedTheme *bt, int RealH, real Min, real Max, int R){
    real t, OrigT;
    char buf[50][20] = {0};
    real IL[50];
    int i = 0;
    int W = 0, MaxW = 0;
    int RH = LA_RH, hH = RH / 2;
    int L;

    real Step = la_CanvasDrawGetDisplayStep(Max, Min, RealH);
    int IS = Step > 1 ? 0 : (Step > 0.09 ? 1 : 2);
    t = OrigT = (real)((int)(Min / Step)) * Step;
    for (t; t <= Max; t += Step, i++){
        sprintf(buf[i], "%.*f", IS, t);
        W = tnsStringGetWidth(buf[i], 0, 0);
        if (W > MaxW) MaxW = W;
    }

    L = R - MaxW - 4;

    tnsUseNoTexture();
    for (t = OrigT, i = 0; t <= Max; t += Step, i++){
        IL[i] = tnsGetRatiod(Max, Min, t) * (RealH);
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        tnsVertex2d(R, IL[i] - hH);
        tnsVertex2d(R, IL[i] + hH);
        tnsVertex2d(L, IL[i] + hH);
        tnsVertex2d(L, IL[i] - hH);
        tnsPackAs(GL_TRIANGLE_FAN);

        tnsColor4dv(laThemeColor(bt,LA_BT_TEXT_ACTIVE));
        tnsVertex2d(R, IL[i] - hH);
        tnsVertex2d(R, IL[i] + hH);
        tnsVertex2d(L, IL[i] + hH);
        tnsVertex2d(L, IL[i] - hH);
        tnsPackAs(GL_LINE_LOOP);
    }

    for (t = OrigT, i = 0; t <= Max; t += Step, i++){
        tnsDrawStringAuto(buf[i], bt->Active, L + 2, R, IL[i] - hH, 0);
    }
}

void la_CanvasDraw(laUiItem *ui, int h){
    laBoxedTheme *bt = (*ui->Type->Theme);
    if (ui->CanvasTemplate && ui->CanvasTemplate->Draw) ui->CanvasTemplate->Draw(bt, ui->PP.EndInstance, ui);
}

void la_CanvasDefaultOverlay(laUiItem* ui, int h){
    laCanvasExtra *e = ui->Extra;
    laBoxedTheme *bt = (*ui->Type->Theme);

    tnsUseNoTexture();

    if(MAIN.CurrentWindow->MaximizedUi!=ui){
        tnsColor4dv(laThemeColor(bt,LA_BT_BORDER));
        tnsVertex2d(ui->L, ui->U); tnsVertex2d(ui->R, ui->U);
        tnsVertex2d(ui->R, ui->B); tnsVertex2d(ui->L, ui->B);
        tnsPackAs(GL_LINE_LOOP);
    }

    if (e->DrawCursor){
        tnsColor4dv(laThemeColor(bt,LA_BT_TEXT));
        int drawx=(e->DrawCursor==LA_CANVAS_CURSOR_CROSS)||(e->DrawCursor==LA_CANVAS_CURSOR_X);
        int drawy=(e->DrawCursor==LA_CANVAS_CURSOR_CROSS)||(e->DrawCursor==LA_CANVAS_CURSOR_Y);
        if(drawx) tnsVertex2d(e->OnX, ui->U); tnsVertex2d(e->OnX, ui->B);
        if(drawy) tnsVertex2d(ui->L, e->OnY); tnsVertex2d(ui->R, e->OnY);
        tnsPackAs(GL_LINES);
        tnsFlush();
    }

    char* MaxIco=(MAIN.CurrentWindow->MaximizedUi!=ui)?"🡹":"🡻";
    tnsVector4d color; tnsVectorCopy4d(laThemeColor(bt,LA_BT_TEXT),color);
    tnsVector4d colork; tnsVectorCopy4d(laThemeColor(bt,LA_BT_NORMAL),colork);
    color[3]=ui->State==LA_UI_NORMAL?0.0:(ui->State==LA_UI_ACTIVE?0.5:1.0);
    colork[3]=ui->State==LA_UI_NORMAL?0.0:(ui->State==LA_UI_ACTIVE?0.5:1.0);
    int startx=(ui->R+ui->L)/2-LA_RH2*2;
    int offs=LA_RH/12;
    
    tnsDrawStringAuto("☰",colork,startx+offs, startx+LA_RH+offs, ui->B-LA_M-LA_RH+offs, LA_TEXT_ALIGN_CENTER);
    tnsDrawStringAuto("☰",color,startx, startx+LA_RH, ui->B-LA_M-LA_RH, LA_TEXT_ALIGN_CENTER);
    tnsDrawStringAuto(MaxIco,colork,startx+LA_RH+offs, startx+LA_2RH+offs, ui->B-LA_M-LA_RH+offs, LA_TEXT_ALIGN_CENTER);
    tnsDrawStringAuto(MaxIco,color,startx+LA_RH, startx+LA_2RH, ui->B-LA_M-LA_RH, LA_TEXT_ALIGN_CENTER);

    if(ui->Expand>=0 && ui!=MAIN.CurrentWindow->MaximizedUi){
        tnsDrawStringAuto("◿",colork,ui->R-LA_RH+offs, ui->R+offs, ui->B-LA_M-LA_RH+offs, LA_TEXT_ALIGN_CENTER);
        tnsDrawStringAuto("◿",laThemeColor(bt,LA_BT_BORDER),ui->R-LA_RH, ui->R, ui->B-LA_M-LA_RH, LA_TEXT_ALIGN_CENTER);
    }

    tnsFlush();
}
void la_CanvasDrawOverlay(laUiItem *ui, int h){
    laCanvasExtra *e = ui->Extra;
    laBoxedTheme *bt = (*ui->Type->Theme);

    tnsDraw2DTextureDirectly(e->OffScr->pColor[0], ui->L, ui->U, ui->R - ui->L, ui->B - ui->U);

    la_CanvasDefaultOverlay(ui, h);
}

int la_AnimateUiListRecursive(laUiList *uil);

void laDefault3DViewOverlay(laUiItem *ui){
    laUiList *uil, *gu,*muil,*gu2;
    laColumn *c, *cl, *cr,  *gc, *gcl, *gcr, *mc,*gc2;
    laPropPack *e = &ui->ExtraPP;
    laUiItem *b, *g, *b1, *b2,*g2;

    if (!(uil = ui->Subs.pFirst)) uil = laAddTabPage(ui, "New Group");

    c = laFirstColumn(uil);
    laSplitColumn(uil, c, 0.8); cl = laLeftColumn(c, 0); cr = laRightColumn(c, 0);

    b=laBeginRow(uil,c,0,0);
    laUiItem* activeui=laShowInvisibleItem(uil,c,&ui->PP,"active");

    muil=laMakeMenuPage(uil,c,"👁"); mc=laFirstColumn(muil);{
        laSplitColumn(muil, mc, 0.3); gcl = laLeftColumn(mc, 0); gcr = laRightColumn(mc, 0);
        laShowLabel(muil,gcl,"Floor:",0,0); laShowLabel(muil,gcr,"Axis:",0,0);
        laShowItem(muil,gcl,&ui->ExtraPP,"show_floor_grid")->Flags|=LA_UI_FLAGS_ICON|LA_UI_FLAGS_CYCLE;
        laShowItem(muil,gcr,&ui->ExtraPP,"show_axis")->Flags|=LA_UI_FLAGS_TRANSPOSE|LA_UI_FLAGS_ICON;
    }

    muil=laMakeMenuPage(uil,c,"Transform"); mc=laFirstColumn(muil);{
        laShowItem(muil,mc,&ui->ExtraPP,"_this_M_grab");
        laShowItem(muil,mc,&ui->ExtraPP,"_this_M_scale");
        laShowItem(muil,mc,&ui->ExtraPP,"_this_M_rotate");
        laShowSeparator(muil,mc);
        laShowItem(muil,mc,&ui->ExtraPP,"_this_M_clear_transformations");
    }

    muil=laMakeMenuPage(uil,c,"Object"); mc=laFirstColumn(muil);{
        laShowItem(muil,mc,&ui->ExtraPP,"_this_M_add");
        laShowSeparator(muil,mc);
        laShowItem(muil,mc,&ui->ExtraPP,"_this_M_make_parent");
        laShowItem(muil,mc,&ui->ExtraPP,"_this_M_unparent");
        laShowSeparator(muil,mc);
        laShowItem(muil,mc,&ui->ExtraPP,"_this_M_duplicate");
        laShowItem(muil,mc,&ui->ExtraPP,"_this_M_combine");
        laShowSeparator(muil,mc);
        laShowItem(muil,mc,&ui->ExtraPP,"_this_M_delete");
    }

    b1=laOnConditionThat(uil,c,laPropExpression(&activeui->PP,0));{
        b2=laOnConditionThat(uil,c,laEqual(laPropExpression(&activeui->PP,"type"),laIntExpression(TNS_OBJECT_MESH)));{
            muil=laMakeMenuPage(uil,c,"Mesh"); mc=laFirstColumn(muil);{
                laShowItem(muil,mc,&ui->ExtraPP,"_this_M_extrude");
                laShowItem(muil,mc,&ui->ExtraPP,"_this_M_make");
                laShowItem(muil,mc,&ui->ExtraPP,"_this_M_knife");
                laShowItemFull(muil,mc,&ui->ExtraPP,"_this_M_knife",0,"mode=loop_cut;text=Loop Cut",0,0);
                laShowSeparator(muil,mc);
                laShowItem(muil,mc,&ui->ExtraPP,"_this_M_duplicate");
                laShowItem(muil,mc,&ui->ExtraPP,"_this_M_merge");
                laShowItem(muil,mc,&ui->ExtraPP,"_this_M_separate");
                laShowSeparator(muil,mc);
                laShowItem(muil,mc,&ui->ExtraPP,"_this_M_subdiv");
                laShowItem(muil,mc,&ui->ExtraPP,"_this_M_recalculate_normals");
                laShowSeparator(muil,mc);
                laShowItem(muil,mc,&ui->ExtraPP,"_this_M_delete");
            }
            laShowSeparator(uil,c);
            laShowItem(uil,c,&ui->ExtraPP,"select_mode")->Flags|=LA_UI_FLAGS_EXPAND;
            laShowItem(uil,c,&ui->ExtraPP,"select_through");
        }laEndCondition(uil,b2);

        b2=laOnConditionThat(uil,c,laEqual(laPropExpression(&activeui->PP,"type"),laIntExpression(TNS_OBJECT_SHAPE)));{
            muil=laMakeMenuPage(uil,c,"Shape"); mc=laFirstColumn(muil);{
                laShowItem(muil,mc,&ui->ExtraPP,"_this_M_set_point_handle");
                laUiItem* b3=laBeginRow(muil,mc,0,0);
                laShowItemFull(muil,mc,&ui->ExtraPP,"_this_M_set_shape_closed",0,"text=Open;mode=OPEN",0,0);
                laShowItemFull(muil,mc,&ui->ExtraPP,"_this_M_set_shape_closed",0,"text=Close Shape;mode=CLOSE",0,0);
                laEndRow(muil,b3);
            }
        }laEndCondition(uil,b2);
    }laEndCondition(uil,b1);

    laShowSeparator(uil,c)->Expand=1;

    laShowItem(uil,c,&ui->ExtraPP,"display_mode")->Flags|=LA_UI_FLAGS_EXPAND;
    laShowItem(uil,c,&ui->ExtraPP,"delta_mode")->Flags|=LA_UI_FLAGS_CYCLE|LA_UI_FLAGS_HIGHLIGHT;
    laShowItem(uil,c,&ui->ExtraPP,"show_details")->Flags|=LA_UI_FLAGS_ICON;

    laEndRow(uil,b);

    b=laOnConditionThat(uil,c,laPropExpression(&ui->ExtraPP,"show_details"));{
        laShowColumnAdjuster(uil,c);

        g2 = laMakeEmptyGroup(uil, cr, "Info", 0);{
            gu2 = g2->Page; gc2 = laFirstColumn(gu2); gu2->HeightCoeff=-1; g2->Flags|=LA_UI_FLAGS_NO_DECAL;
            g = laMakeGroup(gu2, gc2, "Scene Info", 0);{ gu = g->Page; gc = laFirstColumn(gu);
            laShowLabel(gu, gcl, "Name:", 0, 0);
                laShowItem(gu, gc, &ui->PP, "name");
            }
            g = laMakeGroup(gu2, gc2, "Object", 0);{ gu = g->Page; gc = laFirstColumn(gu);
                b1=laOnConditionThat(gu,gc,laPropExpression(&ui->PP,"active"));{
                    laShowLabel(gu, gc, "Name:", 0, 0);
                    laShowItem(gu, gc, &ui->PP, "active.name");
                    b=laOnConditionThat(gu,gc,laPropExpression(&ui->ExtraPP, "delta_mode"));{
                        laShowLabel(gu, gc, "DLoc:", 0, 0);laShowItem(gu, gc, &ui->PP, "active.dlocation")->Flags|=LA_UI_FLAGS_TRANSPOSE;
                        laShowSeparator(gu,gc);
                        laShowLabel(gu, gc, "DRot:", 0, 0);laShowItem(gu, gc, &ui->PP, "active.drotation")->Flags|=LA_UI_FLAGS_TRANSPOSE;
                        laShowSeparator(gu,gc);
                        laShowLabel(gu, gc, "DSca:", 0, 0);laShowItem(gu, gc, &ui->PP, "active.dscale")->Flags|=LA_UI_FLAGS_TRANSPOSE;
                    }laElse(gu,b);{
                        laShowLabel(gu, gc, "Loc:", 0, 0);laShowItem(gu, gc, &ui->PP, "active.location")->Flags|=LA_UI_FLAGS_TRANSPOSE;
                        laShowSeparator(gu,gc);
                        laShowLabel(gu, gc, "Rot:", 0, 0);laShowItem(gu, gc, &ui->PP, "active.rotation")->Flags|=LA_UI_FLAGS_TRANSPOSE;
                        laShowSeparator(gu,gc);
                        laShowLabel(gu, gc, "Sca:", 0, 0);laShowItem(gu, gc, &ui->PP, "active.scale")->Flags|=LA_UI_FLAGS_TRANSPOSE;
                    }laEndCondition(gu,b);
                }laElse(gu,b1);{
                    laShowLabel(gu, gc, "No active object.", 0, 0);
                }laEndCondition(gu,b1);
            }
        }
    }laEndCondition(uil,b);
}
void la_3DViewEnsureCamera(laCanvasExtra* e){
    if(e->ViewingCamera) return;
    tnsVector3d pos={-3, -4, 1.5};
    tnsCamera* c= tnsCreateCamera(0, "VIEWING_CAMERA", rad(50), LA_COLOR3(pos), rad(70), 0, rad(-40), 25);
    memAssignRef(e,&e->ViewingCamera,c);
    tnsVector3d target={0,0,0}; tnsVector3d up={0,0,1};
    tnsLookAt(e->ViewingCamera, target, up);
    e->ViewingCamera->FocusDistance=tnsDist3dv(pos,target);
}
void la_3DViewInit(laUiItem *ui){
    laCanvasExtra *e = ui->Extra;

    if (e){ return; }
    else{ e = memAcquireHyper(sizeof(laCanvasExtra)); ui->Extra = e; }

    la_CanvasInit(ui);

    e->ParentUi = ui;

    la_3DViewEnsureCamera(e);

    e->ShowAxis[0] = 1; e->ShowAxis[1] = 1;
    e->ShowFloorGrid = 1;
    e->HeightCoeff = 10;
    e->ZoomX=1.0f/LA_RH; e->ZoomY=1.0f/LA_RH;

    laRecordDifferences(0,"tns.world");
    laPushDifferences("Created camera for 3D view widget,",0);
}
void la_3DViewDestroy(laUiItem *ui){
    laCanvasExtra *e = ui->Extra;

    tnsDelete2DOffscreen(e->OffScr);
    tnsDelete2DOffscreen(e->OffScrShadow);
    
    if(e->ViewingCamera) tnsDestroyObject(e->ViewingCamera);

    memFree(e);
}

void laDefault2DViewOverlayRight(laUiItem *ui){
    laUiList *uil, *gu;
    laColumn *c, *cr, *crl, *crr, *gc, *gcl, *gcr, *cl;
    laPropPack *e = &ui->ExtraPP;
    laUiItem *b, *b1, *b2, *g;

    if (!(uil = ui->Subs.pFirst)) uil = laAddTabPage(ui, "New Group");
    uil->HeightCoeff=-1;

    c = laFirstColumn(uil);
    laSplitColumn(uil, c, 0.7);
    cl = laLeftColumn(c, 0);
    cr = laRightColumn(c, 0);

    laShowColumnAdjuster(uil, c);

    b=laBeginRow(uil,cl,0,0);
    laShowLabel(uil, cl, "Line Width:", 0, 0);
    laShowItem(uil, cl, e, "adaptive_line_width")->Flags|=LA_UI_FLAGS_PLAIN;
    laEndRow(uil,b);

    b=laBeginRow(uil,cl,0,0);
    laShowItem(uil, cl, &ui->PP, "size")->Flags|=LA_UI_FLAGS_PLAIN|LA_UI_FLAGS_TRANSPOSE|LA_TEXT_ALIGN_LEFT;
    laEndRow(uil,b);
    laShowItem(uil, cl, &ui->PP, "internal_type")->Flags|=LA_UI_FLAGS_PLAIN;

    g = laMakeFoldableGroup(uil, cr, "Display", 0, 1, 0);{
        gu = g->Page;
        gc = laFirstColumn(gu);
        laSplitColumn(gu, gc, 0.35);
        gcl = laLeftColumn(gc, 0);
        gcr = laRightColumn(gc, 0);

        // needs another mode for access the ExtraPP.
        // laShowItem(gu, gc, &ui->ExtraPP, "maximize");

        laShowLabel(gu, gcl, "Transparency:", 0, 0);
        laShowItem(gu, gcr, e, "draw_image_alpha");
        laShowLabel(gu, gcl, "Border:", 0, 0);
        laShowItemFull(gu, gcr, e, "draw_image_border", LA_WIDGET_ENUM_CYCLE, 0, 0, 0);
        laShowLabel(gu, gcl, "Line Width:", 0, 0);
        laShowItemFull(gu, gcr, e, "adaptive_line_width", LA_WIDGET_ENUM_CYCLE, 0, 0, 0);
        laShowLabel(gu, gcl, "Cursor:", 0, 0);
        laShowItemFull(gu, gcr, e, "draw_cursor", LA_WIDGET_ENUM_CYCLE, 0, 0, 0);
    }laEndFoldableGroup(uil,g);
}

void la_CanvasUiInit(laUiItem* ui){ laCanvasTemplate* ct=ui->CanvasTemplate; if(ct->Init) ct->Init(ui); }
void la_CanvasUiDestroy(laUiItem *ui){ laCanvasTemplate* ct=ui->CanvasTemplate; if(ct->Destroy) ct->Destroy(ui); }

void la_CanvasInit(laUiItem *ui){
    laCanvasExtra *e = ui->Extra;

    if (!e){ e = memAcquireHyper(sizeof(laCanvasExtra)); ui->Extra = e; }

    e->ParentUi = ui;

    e->HeightCoeff = 10;
    e->ZoomX = 1; e->ZoomY = 1;
    e->ImageDrawAlpha = 1;
    e->ImageDrawBorder = 1;
    e->AdaptiveLineWidth = 1;
    e->ClearBackground = 1;

    laFirstColumn(laAddTabPage(ui, "New Group"));
}
void la_CanvasDestroy(laUiItem *ui){
    laCanvasExtra *e = ui->Extra;

    tnsDelete2DOffscreen(e->OffScr);

    memFree(e);
}


//============================================================= [Operators]

int OPINV_3DOr2DViewUiItem(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    a->CustomData = ui->Extra;
    return LA_RUNNING;
}
int OPEXT_3DOr2DViewUiItem(laOperator *a, int ExitCode){
    return 0;
}

int la_CanvasDetectButtons(laUiItem* ui, laBoxedTheme* bt, int x,int y, int total_buttons, int* very_close){
    int midx=(ui->R+ui->L)/2-LA_RH2*(total_buttons-1), midy=ui->B-LA_RH2-LA_M;
    if(y>ui->B-LA_2RH-LA_M || y<ui->U+LA_2RH+LA_M || x<ui->L+LA_2RH+LA_M || x>ui->R-LA_2RH-LA_M)
        { if(very_close)*very_close=1; }else return 0;
    if(y<ui->B-LA_RH-LA_M)return 0;
    for(int i=0;i<total_buttons;i++){ if(x>=midx-LA_RH2 && x<midx+LA_RH2) return i+1; midx+=LA_RH;}
    return 0;
}
int OPMOD_Canvas(laOperator *a, laEvent *e){
    laUiItem *ui = a->Instance;
    laBoxedTheme *bt = (*ui->Type->Theme);
    laCanvasExtra *ex = a->CustomData;
    laUiList *subu;
    laUiItem *subui = 0;
    laListHandle Locals = {0};
    int px = e->x, py = e->y;

    if (!laIsInUiItem(ui, a, e->x, e->y) && !ex->Dragging){
        ex->OnX = INT_MAX; ex->OnY = INT_MAX; if(ui->State!=LA_UI_NORMAL){ ui->State=LA_UI_NORMAL; laRedrawCurrentPanel(); }
        return LA_FINISHED_PASS;
    }

    //int x=e->x,y=e->y; laPanel*p=MAIN.CurrentPanel,*dp;
    //laLocalToWindow(0, p, &x, &y); if(!p) p=MAIN.CurrentWindow->MaximizedUiPanel;
    //if(dp = laDetectPanel(x,y) && dp!=p){ return LA_RUNNING_PASS; }

    if(e->type&LA_MOUSE_EVENT){
        if(e->type == LA_L_MOUSE_DOWN && ui->Expand>=0 && e->x>ui->R-LA_M-LA_RH && e->y>ui->B-LA_M-LA_RH){
            ex->Dragging=1; ex->ClickedX=e->x; ex->ClickedY=e->y; ex->TargetIndexVali=ui->Expand;
            return LA_RUNNING;
        }
        if(ex->Dragging==1){
            if(e->type==LA_MOUSEMOVE){ ui->Expand=ex->TargetIndexVali+((real)e->y-ex->ClickedY+0.5)/LA_RH;
                if(ex->HeightCoeff<1) ex->HeightCoeff=1; laRecalcCurrentPanel(); }
            elif(e->type==LA_L_MOUSE_UP){ ex->Dragging=0; }
            elif(e->type==LA_R_MOUSE_DOWN){ ex->Dragging=0; ui->Expand=ex->TargetIndexVali; laRecalcCurrentPanel(); }
            return LA_RUNNING;
        }

        int VeryClose=0; int btn=la_CanvasDetectButtons(ui, bt, e->x, e->y, 2, &VeryClose);
        if(e->type==LA_L_MOUSE_DOWN){
            if(btn==2){ if(MAIN.CurrentWindow->MaximizedUi==ui) laRestoreCanvasUI(); else laMaximizeCanvasUI(ui,MAIN.CurrentPanel); return LA_RUNNING; }
            if(btn==1){
                if(ui->Flags&LA_UI_FLAGS_NO_OVERLAY)ui->Flags&=~LA_UI_FLAGS_NO_OVERLAY; else ui->Flags|=LA_UI_FLAGS_NO_OVERLAY;
                laRedrawCurrentPanel(); return LA_RUNNING;
            }
        }
        int state=VeryClose?(btn?LA_UI_EDITING:LA_UI_ACTIVE):LA_UI_NORMAL;
        if(state!=ui->State){ ui->State=state; laRedrawCurrentPanel(); }
    }

    if(!(ui->Flags&LA_UI_FLAGS_NO_OVERLAY)){
        for (subu = ui->Subs.pFirst; subu; subu = subu->Item.pNext){
            if (subui = la_DetectUiItemRecursive(subu, px, py, ui->B, &Locals, 0)){
                if (subui && !a->Child && subui->Type->OperatorType){
                    laUiList *luil = ((laUiListDrawItem *)Locals.pFirst)->Target;
                    laSetOperatorLocalizer(a->ToPanel);
                    if(subui && !a->Child && subui->Type->OperatorType && !la_UiOperatorExists(subui)){
                        if (laInvokeUiP(a, subui->Type->OperatorType, e, subui, &Locals, 0) >= 0); laRetriggerOperators();
                    }
                    lstClearPointer(&Locals);
                    return LA_RUNNING_PASS;
                }
            }
            lstClearPointer(&Locals);

            if(e->type&LA_KEY_MOUSE_SCROLL){
                laUiItem *pui = 0; laListHandle levels={0}; int dir=(e->type&LA_STATE_DOWN)?1:-1;
                laUiList *duil = la_DetectUiListRecursiveDeep(subu, e->x, e->y, 10000, &pui, 0, 0, 0, 0, &levels);
                laUiListRecord* lip=levels.pLast; laUiList* uuil=lip->uil; laUiItem* upui=lip->Item.pPrev?((laUiListRecord*)lip->Item.pPrev)->pui:0; int ran=0;
                while (lip && upui){
                    if((ran=laPanUiListAuto(uuil, 0, dir*MAIN.ScrollingSpeed*LA_RH,
                        uuil->L, upui->R-(uuil->ScrollerShownV?(LA_SCROLL_W+LA_M):0),
                        uuil->U, upui->B-LA_M-(uuil->ScrollerShownH?(LA_SCROLL_W+LA_M):0)))) break;
                    lip=lip->Item.pPrev; uuil=lip->uil;  upui=lip->Item.pPrev?((laUiListRecord*)lip->Item.pPrev)->pui:0; 
                }
                if(ran)laRedrawCurrentPanel();
                if(ran){return LA_RUNNING;}
            }
        }
    }

    if (ex->DrawCursor){
        ex->OnX = e->x;
        ex->OnY = e->y;
        laRedrawCurrentPanel();
    }

    if(ui->CanvasTemplate->ExtraModal){ if(!(ui->CanvasTemplate->ExtraModal(a,e)&LA_PASS_ON)) return LA_RUNNING; }

    if(!ex->AsPlayer){
        if (laKeyMapExecuteEventEx(a, &ui->ExtraPP, &ui->CanvasTemplate->KeyMapper, e)) return LA_RUNNING;
        if (laKeyMapExecuteEventEx(a, &ui->ExtraPP, &ui->Type->KeyMapper, e)) return LA_RUNNING;
    }

    return LA_RUNNING_PASS;
}

int OPCHK_Is2DViewExtra(laPropPack *This, laStringSplitor *ss){
    return 1;
    //if (This && (This->LastPs->p->SubProp == _LA_PROP_2D_EXTRA)) return 1;
    return 0;
}
int OPINV_CanvasZoom(laOperator *a, laEvent *e){
    laCanvasExtra *ex = a->This->EndInstance;

    if (strArgumentMatch(a->ExtraInstructionsP, "mode", "mouse")){
        laGeneralUiExtraData *ex = memAcquireSimple(sizeof(laGeneralUiExtraData));
        laSetWindowCursor(LA_HAND);
        a->CustomData = ex;
        ex->LastX = e->x;
        ex->LastY = e->y;
        return LA_RUNNING;
    }

    if (strArgumentMatch(a->ExtraInstructionsP, "direction", "in")){
        if (strArgumentMatch(a->ExtraInstructionsP, "axis", "x")){
            ex->ZoomX *= 0.9;
        }else{
            ex->ZoomX *= 0.9;
            ex->ZoomY *= 0.9;
        }
        laRedrawCurrentPanel();
    }elif (strArgumentMatch(a->ExtraInstructionsP, "direction", "out")){
        if (strArgumentMatch(a->ExtraInstructionsP, "axis", "x")){
            ex->ZoomX *= 1.1;
        }else{
            ex->ZoomX *= 1.1;
            ex->ZoomY *= 1.1;
        }
        laRedrawCurrentPanel();
    }

    return LA_FINISHED;
}
int OPMOD_CanvasZoom(laOperator *a, laEvent *e){
    laCanvasExtra *ex = a->This->EndInstance;
    laGeneralUiExtraData *uex = a->CustomData;

    if (e->type == LA_L_MOUSE_UP || e->type == LA_R_MOUSE_DOWN || e->type==LA_M_MOUSE_UP){
        laSetWindowCursor(LA_ARROW);
        return LA_FINISHED;
    }

    if (e->type == LA_MOUSEMOVE){
        if (strArgumentMatch(a->ExtraInstructionsP, "axis", "x")){
            ex->ZoomX *= (1.0 - (e->x - uex->LastX) * MAIN.ZoomSpeed2D);
        }elif(strArgumentMatch(a->ExtraInstructionsP, "lock", "true")){
            ex->ZoomX *= (1.0 - (e->y - uex->LastY) * MAIN.ZoomSpeed2D); ex->ZoomY = ex->ZoomX;
        }else{
            ex->ZoomX *= (1.0 - (e->x - uex->LastX) * MAIN.ZoomSpeed2D);
            ex->ZoomY *= (1.0 + (e->y - uex->LastY) * MAIN.ZoomSpeed2D);
        }
        if(ex->ZoomX<0||ex->ZoomX!=ex->ZoomX){
            printf("Error zoom!\n"); ex->ZoomX=1; ex->ZoomY=1;
        }
        uex->LastX = e->x;
        uex->LastY = e->y;
        laRedrawCurrentPanel();
    }

    return LA_RUNNING;
}
int OPMOD_CanvasMove(laOperator *a, laEvent *e){
    laCanvasExtra *ex = a->This->EndInstance;
    laGeneralUiExtraData *uex = a->CustomData;

    if (e->type == LA_L_MOUSE_UP || e->type == LA_R_MOUSE_DOWN || e->type==LA_M_MOUSE_UP ||
       (e->type==LA_KEY_UP && e->key==' ')) {
        laSetWindowCursor(LA_ARROW);
        return LA_FINISHED;
    }

    if (e->type == LA_MOUSEMOVE){
        if (strArgumentMatch(a->ExtraInstructionsP, "axis", "x")){
            ex->PanX -= (e->x - uex->LastX) * ex->ZoomX;
        }else{
            ex->PanX -= (e->x - uex->LastX) * ex->ZoomX;
            ex->PanY += (e->y - uex->LastY) * ex->ZoomY;
        }
        uex->LastX = e->x;
        uex->LastY = e->y;
        laRedrawCurrentPanel();
    }

    return LA_RUNNING;
}
int OPINV_CanvasClick(laOperator *a, laEvent *e){
    laCanvasExtra *ex = a->This->EndInstance;
    laGeneralUiExtraData *uex = a->CustomData;

    if (e->type == LA_L_MOUSE_DOWN){
        ex->ClickedX = (e->x - (ex->ParentUi->R - ex->ParentUi->L) / 2 - ex->ParentUi->L) * ex->ZoomX + ex->PanX;
        ex->ClickedY = ((ex->ParentUi->B - ex->ParentUi->U) / 2 - e->y + ex->ParentUi->U) * ex->ZoomY + ex->PanY;
        laRedrawCurrentPanel();
    }

    return LA_FINISHED_PASS;
}


int OPCHK_Is3DViewExtra(laPropPack *This, laStringSplitor *ss){
    if (This && This->LastPs->p->SubProp == _LA_PROP_3D_EXTRA) return 1;
    return 0;
}
int OPINV_3DViewCameraZoom(laOperator *a, laEvent *e){
    laCanvasExtra *ex = a->This->EndInstance;
    tnsCamera *c = ex->ViewingCamera;
    laUiItem* ui=ex->ParentUi; tnsRootObject* root=ui->PP.EndInstance;
    if(root && root->Base.Type==TNS_OBJECT_ROOT && root->Is2D){
        return OPINV_CanvasZoom(a,e);
    }

    if (strArgumentMatch(a->ExtraInstructionsP, "direction", "in")){
        tnsZoomViewingCamera(c, 0.1);
        laRedrawCurrentPanel();
    }elif (strArgumentMatch(a->ExtraInstructionsP, "direction", "out")){
        tnsZoomViewingCamera(c, -0.1);
        laRedrawCurrentPanel();
    }

    return LA_FINISHED;
}
int OPINV_3DOr2DViewAdjust(laOperator *a, laEvent *e){
    laGeneralUiExtraData *ex = memAcquireSimple(sizeof(laGeneralUiExtraData));
    laSetWindowCursor(LA_HAND);
    a->CustomData = ex;
    ex->LastX = e->x; ex->LastY = e->y;

    if(a->Type->Modal == OPMOD_CanvasMove && a->This->EndInstance){
        laCanvasExtra* cex=a->This->EndInstance;
        char* str=strGetArgumentString(a->ExtraInstructionsP,"pan");
        if(str){
            if(strSame(str,"left")){ cex->PanX -= (LA_RH) * cex->ZoomX; }
            elif(strSame(str,"right")){ cex->PanX += (LA_RH) * cex->ZoomX; }
            elif(strSame(str,"up")){ cex->PanY += (LA_RH) * cex->ZoomY; }
            elif(strSame(str,"down")){ cex->PanY -= (LA_RH) * cex->ZoomY; }
            return LA_FINISHED;
        }
    }

    return LA_RUNNING;
}
void OPEXT_3DOr2DViewAdjust(laOperator *a, int Mark){
    if (a->CustomData) memFree(a->CustomData);
    laSetWindowCursor(LA_ARROW);
}
int OPMOD_3DViewCameraRotate(laOperator *a, laEvent *e){
    laCanvasExtra *ex = a->This->EndInstance;
    laGeneralUiExtraData *uex = a->CustomData;
    laUiItem* ui=ex->ParentUi; tnsRootObject* root=ui->PP.EndInstance;
    if(root && root->Base.Type==TNS_OBJECT_ROOT && root->Is2D){
        return OPMOD_CanvasMove(a,e);
    }

    if (e->type == LA_L_MOUSE_UP || e->type == LA_M_MOUSE_UP || e->type == LA_R_MOUSE_DOWN) return LA_FINISHED;

    if (e->type == LA_MOUSEMOVE){
        tnsRotateViewingCamera(ex->ViewingCamera, (real)(uex->LastY - e->y) / 150.0, (real)(uex->LastX - e->x) / 150.0);
        uex->LastX = e->x;
        uex->LastY = e->y;
        laRedrawCurrentPanel();
    }

    return LA_RUNNING;
}
int OPMOD_3DViewCameraMove(laOperator *a, laEvent *e){
    laCanvasExtra *ex = a->This->EndInstance;
    laGeneralUiExtraData *uex = a->CustomData;
    laUiItem* ui=ex->ParentUi; tnsRootObject* root=ui->PP.EndInstance;
    if(root && root->Base.Type==TNS_OBJECT_ROOT && root->Is2D){
        return OPMOD_CanvasMove(a,e);
    }

    if (e->type == LA_L_MOUSE_UP || e->type == LA_M_MOUSE_UP || e->type == LA_R_MOUSE_DOWN){
        laSetWindowCursor(LA_ARROW);
        return LA_FINISHED;
    }
    if (e->type == LA_MOUSEMOVE){
        tnsTranslateViewingCamera(ex->ViewingCamera, ex->ParentUi->R - ex->ParentUi->L, ex->ParentUi->B - ex->ParentUi->U, -e->x + uex->LastX, e->y - uex->LastY);
        uex->LastX = e->x; uex->LastY = e->y;
        laRedrawCurrentPanel();
    }

    return LA_RUNNING;
}

void la_RegisterViewerOperators(){
    laCreateOperatorType("LA_canvas_operator", "2D View UiItem Operator", "All Visual 2D View Operations Are Called From This Ui",
                          0, 0, OPEXT_3DOr2DViewUiItem, OPINV_3DOr2DViewUiItem, OPMOD_Canvas, U'🖦', LA_EXTRA_TO_PANEL | LA_ACTUATOR_SYSTEM | LA_ACTUATOR_HIDDEN);

    laCreateOperatorType("LA_3d_view_camera_zoom", "Camera Zoom", "Let View Camera Zoom In Or Out",
                          OPCHK_Is3DViewExtra, 0, 0, OPINV_3DViewCameraZoom, 0, U'🔎', 0);
    laCreateOperatorType("LA_3d_view_camera_rotate", "Camera Rotate", "Let View Camera Rotate Along Local X/Y Axis",
                          OPCHK_Is3DViewExtra, 0, OPEXT_3DOr2DViewAdjust, OPINV_3DOr2DViewAdjust, OPMOD_3DViewCameraRotate, U'🗘', LA_EXTRA_TO_PANEL);
    laCreateOperatorType("LA_3d_view_camera_move", "Camera Move", "Let View Camera Move Along Local X/Y Axis",
                          OPCHK_Is3DViewExtra, 0, OPEXT_3DOr2DViewAdjust, OPINV_3DOr2DViewAdjust, OPMOD_3DViewCameraMove, U'🤚', LA_EXTRA_TO_PANEL);

    laCreateOperatorType("LA_2d_view_zoom", "2D Zoom", "Let View 2D Cavans Zoom In Or Out",
                          OPCHK_Is2DViewExtra, 0, OPEXT_3DOr2DViewAdjust, OPINV_CanvasZoom, OPMOD_CanvasZoom, U'🔎', 0);
    laCreateOperatorType("LA_2d_view_move", "2D Move", "Let View 2D Cavans Move Along Local X/Y Axis",
                          OPCHK_Is2DViewExtra, 0, OPEXT_3DOr2DViewAdjust, OPINV_3DOr2DViewAdjust, OPMOD_CanvasMove, U'🤚', LA_EXTRA_TO_PANEL);
    laCreateOperatorType("LA_2d_view_click", "2D Click", "2D Click",
                          OPCHK_Is2DViewExtra, 0, OPEXT_3DOr2DViewAdjust, OPINV_CanvasClick, 0, U'🖦', LA_EXTRA_TO_PANEL);
}


void *tnsget_detached_FirstScene(void *UNUSED1, void *UNUSED2);

void laset_CanvasSelectMode(laCanvasExtra* ex, int mode){
    ex->SelectMode=mode; laUiItem* ui=ex->ParentUi;
    tnsObject*root=ui?ui->PP.EndInstance:0; if(!root) return;
    if(root->Active){ if(root->Active->Type==TNS_OBJECT_MESH && ((tnsMeshObject*)root->Active)->Mode==TNS_MESH_EDIT_MODE){ tnsMMeshEnsureSelection(root->Active, mode); } }
}

void la_RegisterUiTypesViewerWidgets(){
    laPropContainer *pc = 0;
    laProp *p = 0;
    laKeyMapper *km;

    _LA_UI_CANVAS = la_RegisterUiType("LA_canvas_default", 0, "LA_canvas_operator", &_LA_THEME_2D_VIEW, la_CanvasDraw, la_CanvasGetHeight, la_CanvasUiInit, la_CanvasUiDestroy);
    _LA_UI_CANVAS->Tag = LA_UI_TAG_NEED_REBUILD;

    laCanvasTemplate* ct=laRegisterCanvasTemplate("la_CanvasDrawTexture", "tns_texture", 0, la_CanvasDrawTexture, la_CanvasDrawOverlay, la_CanvasInit, la_CanvasDestroy);
    pc = laCanvasHasExtraProps(ct,sizeof(laCanvasExtra),2);{
        _LA_PROP_2D_EXTRA = pc;
        laAddIntProperty(pc, "height_coeff", "Ui Height", "Ui Height Coefficiency Entry", 0, 0, "Rows", 100, -100, 1, 0, 0, offsetof(laCanvasExtra, HeightCoeff), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        p = laAddEnumProperty(pc, "draw_image_alpha", "Draw Image Alpha", "Draw grid background on alpha<1", 0, 0, 0, 0, 0,
                               offsetof(laCanvasExtra, ImageDrawAlpha), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItem(p, "null", "null", "Don't Draw Grid", 0);
            laAddEnumItem(p, "normal", "normal", "Use Normal Color", 0);
            laAddEnumItem(p, "bright", "bright", "Draw Alpha Grid Using Very Bright White Color", 0);
        }
        p = laAddEnumProperty(pc, "draw_image_border", "Draw Image Border", "Draw image border using same color as ui item", 0, 0, 0, 0, 0,
                               offsetof(laCanvasExtra, ImageDrawBorder), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItem(p, "no", "No", "Don't Draw Border", U'☐');
            laAddEnumItem(p, "yes", "Yes", "Draw Border", U'☑');
        }
        p = laAddEnumProperty(pc, "adaptive_line_width", "Adaptive Line Width", "glLineWidth() will follow 2dview zooming", 0, 0, 0, 0, 0,
                               offsetof(laCanvasExtra, AdaptiveLineWidth), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItem(p, "constant", "Constant", "Don't change line width", U'☐');
            laAddEnumItem(p, "adaptive", "Adaptive", "Adaptive Line Width", U'☑');
        }
        p = laAddEnumProperty(pc, "line_width_warning", "Line Width Warnning", "Show whether line width is acceptable by hardware", 0, 0, 0, 0, 0,
                               offsetof(laCanvasExtra, LineWidthWarning), 0, 0, 0, 0, 0, 0, 0, 0, 0, LA_READ_ONLY);{
            laAddEnumItem(p, "acceptable", "Acceptable", "Line width is acceptable by graphic hadware", U'✔');
            laAddEnumItem(p, "too_wide", "Too Wide", "Line width is too wide for graphic hadware", U'🞫');
            laAddEnumItem(p, "too_thin", "Too Thin", "Line width is too thin for graphic hadware", U'🞫');
        }
        p = laAddEnumProperty(pc, "clear_background", "Clear Background", "Clear background", 0, 0, 0, 0, 0, offsetof(laCanvasExtra, ClearBackground), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItem(p, "no", "No", "Don't Clear Background", U'🌔');
            laAddEnumItem(p, "yes", "Yes", "Clear Background", U'🌑');
        }
        p = laAddEnumProperty(pc, "draw_cursor", "Show Cursor", "Show cursor", 0, 0, 0, 0, 0, offsetof(laCanvasExtra, DrawCursor), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItem(p, "no", "No", "Don't draw cursor", U'🖦');
            laAddEnumItem(p, "yes", "Yes", "Draw cursor", U'➕');
        }
        laAddFloatProperty(pc, "pan", "Pan", "Pan On X,Y Axis", 0, "X,Y", "px", 0, 0, 1, 0, 0, offsetof(laCanvasExtra, PanX), 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0);
        laAddFloatProperty(pc, "zoom", "Zoom", "Zoom Factor On X,Y Axis", 0, "X,Y", 0, 0, 0, 0.01, 1, 0, offsetof(laCanvasExtra, ZoomX), 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0);
        //laAddSubGroup(pc, "Template", "Template Used To Draw 2D Stuff", "la_2d_view_template",0, 0, 0, -offsetof(laCanvasExtra, Template), 0, 0, 0, 0, 0, 0, 0, 0, LA_UDF_REFER);
        laAddOperatorProperty(pc, "zoom", "Zoom", "Zoom 2D Canvans", "LA_2d_view_zoom", U'🔎', 0);
        laAddOperatorProperty(pc, "move", "Move", "Move 2D Canvans", "LA_2d_view_move", U'🤚', 0);
        laAddOperatorProperty(pc, "click", "Click", "Click On 2D Canvans", "LA_2d_view_click", U'🤚', 0);
        laAddSubGroup(pc, "parent_ui", "Parent UI", "The Ui That Holds The Viewer", "ui_item",0, 0, 0, offsetof(laCanvasExtra, ParentUi), 0, 0, 0, 0, 0, 0, 0, LA_UDF_REFER);
    }
    km = &ct->KeyMapper;
    laAssignNewKey(km, 0, "LA_2d_view_zoom", LA_KM_SEL_UI_EXTRA, 0, LA_MOUSE_WHEEL_DOWN, 0, "direction=out");
    laAssignNewKey(km, 0, "LA_2d_view_zoom", LA_KM_SEL_UI_EXTRA, 0, LA_MOUSE_WHEEL_UP, 0, "direction=in");
    laAssignNewKey(km, 0, "LA_2d_view_move", LA_KM_SEL_UI_EXTRA, LA_KEY_ALT, LA_L_MOUSE_DOWN, 0, 0);
    laAssignNewKey(km, 0, "LA_2d_view_move", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, LA_PANNING_LEFT, "pan=left");
    laAssignNewKey(km, 0, "LA_2d_view_move", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, LA_PANNING_RIGHT, "pan=right");
    laAssignNewKey(km, 0, "LA_2d_view_move", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, LA_PANNING_UP, "pan=up");
    laAssignNewKey(km, 0, "LA_2d_view_move", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, LA_PANNING_DOWN, "pan=down");
    laAssignNewKey(km, 0, "LA_2d_view_click", LA_KM_SEL_UI_EXTRA, 0, LA_L_MOUSE_DOWN, 0, 0);


    ct=laRegisterCanvasTemplate("la_3DView", "tns_object", 0, la_RootObjectDraw, la_RootObjectDrawOverlay, la_3DViewInit, la_3DViewDestroy);
    pc = laCanvasHasExtraProps(ct, sizeof(laCanvasExtra), 2);{
        _LA_PROP_3D_EXTRA = pc;
        p = laAddEnumProperty(pc, "show_axis", "Show Axis", "Show Global X,Y,Z Axis", LA_WIDGET_ENUM_CYCLE, "X,Y,z", 0, 0, 0, offsetof(laCanvasExtra, ShowAxis), 0, 0, 3, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItem(p, "HIDDEN", "Hidden", "Current Axis Is Hidden", U'🌔');
            laAddEnumItem(p, "SHOWN", "Shown", "Current Axis Is Shown", U'🌑');
        }
        p = laAddEnumProperty(pc, "show_floor_grid", "Show Floor Grid", "Show floor grid", 0,  0, 0, 0, 0, offsetof(laCanvasExtra, ShowFloorGrid), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItem(p, "HIDDEN", "Hidden", "Current Axis Is Hidden", U'🌔');
            laAddEnumItem(p, "SHOWN", "Shown", "Current Axis Is Shown", U'🌑');
        }
        p = laAddEnumProperty(pc, "select_mode", "Select Mode", "Select by vertices or edges", 0, 0, 0, 0, 0, offsetof(laCanvasExtra, SelectMode), 0, laset_CanvasSelectMode, 0, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItemAs(p, "VERTS", "Verts", "Select by vertices", LA_CANVAS_SELECT_MODE_VERTS,0);
            laAddEnumItemAs(p, "EDGES", "Edges", "Select by edges", LA_CANVAS_SELECT_MODE_EDGES,0);
        }
        p = laAddEnumProperty(pc, "select_through", "Select Through", "Select through stuff", LA_WIDGET_ENUM_HIGHLIGHT, 0, 0, 0, 0, offsetof(laCanvasExtra, SelectThrough), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItemAs(p, "OFF", "Off", "Don't select through stuff", LA_CANVAS_SELECT_THROUGH_OFF,0);
            laAddEnumItemAs(p, "ON", "On", "Select through stuff", LA_CANVAS_SELECT_THROUGH_ON,0);
        }
        p = laAddEnumProperty(pc, "delta_mode", "Delta Mode", "Toggle delta transformation mode for animation editing", 0, 0, 0, 0, 0, offsetof(laCanvasExtra, DeltaMode), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItemAs(p, "NONE", "None", "Regular mode",0,0);
            laAddEnumItemAs(p, "DELTA", "Delta", "Delta mode",1,0);
        }
        p = laAddEnumProperty(pc, "show_details", "Show Details", "Show detailed information", LA_WIDGET_ENUM_HIGHLIGHT, 0, 0, 0, 0, offsetof(laCanvasExtra, ShowDetails), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItem(p, "HIDDEN", "Hidden", "Current Axis Is Hidden", U'🛈');
            laAddEnumItem(p, "SHOWN", "Shown", "Current Axis Is Shown", U'i');
        }
        p = laAddEnumProperty(pc, "display_mode", "Display Mode", "Display mode of the viewport", 0, 0, 0, 0, 0, offsetof(laCanvasExtra, DisplayMode), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);{
            laAddEnumItemAs(p, "SOLID", "Solid", "Solid object view",LA_CANVAS_DISPLAY_SOLID, 0);
            laAddEnumItemAs(p, "MATERIAL", "Material", "Material view",LA_CANVAS_DISPLAY_MATERIAL, 0);
        }
        laAddIntProperty(pc, "height_coeff", "Ui Height", "Ui Height Coefficiency Entry", 0, 0, "Rows", 100, -100, 1, 0, 0, offsetof(laCanvasExtra, HeightCoeff), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        laAddOperatorProperty(pc, "zoom", "Zoom", "Zoom Viewing Camera", "LA_3d_view_camera_zoom", U'🔎', 0);
        laAddOperatorProperty(pc, "rotate", "Rotate", "Rotate Viewing Camera", "LA_3d_view_camera_rotate", U'🗘', 0);
        laAddOperatorProperty(pc, "move", "Move", "Move Viewing Camera", "LA_3d_view_camera_move", U'🤚', 0);
        //laAddSubGroup(pc, "Viewing Camera", "Unique Camera That Is Used To Draw 3D Viewport", "tns_object",0, 0, 0, -offsetof(laCanvasExtra, ViewingCamera), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        laAddSubGroup(pc, "parent_ui", "Parent Ui", "Parent Ui (Mostly Used To Determin Viewport Size)", "ui_item",0, 0, 0, offsetof(laCanvasExtra, ParentUi), 0, 0, 0, 0, 0, 0, 0, LA_UDF_REFER);
        laAddOperatorProperty(pc, "align_camera_to_view", "Align Active Camera To View", "Align Active Camera To View", "TNS_align_camera_to_view", U'🎥', 0);
        laAddOperatorProperty(pc, "align_view_to_camera", "Align View To Active Camera", "Align View To Active Camera", "TNS_align_view_to_camera", U'🎥', 0);
        laAddOperatorProperty(pc, "_this_M_delete", "Delete", "Delete parts of mesh", "M_delete", 0, 0);
        laAddOperatorProperty(pc, "_this_M_make_parent", "Make parent", "Parent objects to active objects or unparent selected ones", "M_make_parent", 0, 0);
        laAddOperatorProperty(pc, "_this_M_unparent", "Unparent", "Unparent selected objects", "M_unparent", 0, 0);
        laAddOperatorProperty(pc, "_this_M_add", "Add", "Add objects/primitives", "M_add", 0, 0);
        laAddOperatorProperty(pc, "_this_M_grab", "Grab", "Grab things and move around", "M_grab", 0, 0);
        laAddOperatorProperty(pc, "_this_M_scale", "Scale", "Scale selected things", "M_scale", 0, 0);
        laAddOperatorProperty(pc, "_this_M_rotate", "Rotate", "Rotation selected things", "M_rotate", 0, 0);
        laAddOperatorProperty(pc, "_this_M_extrude", "Extrude", "Extrude parts of the mesh", "M_extrude", 0, 0);
        laAddOperatorProperty(pc, "_this_M_delete", "Delete", "Delete parts of the mesh", "M_delete", 0, 0);
        laAddOperatorProperty(pc, "_this_M_make", "Make", "Make mesh primitive from selected ones", "M_make", 0, 0);
        laAddOperatorProperty(pc, "_this_M_subdiv", "Subdiv", "Subdivide edges", "M_subdiv", 0, 0);
        laAddOperatorProperty(pc, "_this_M_add", "Add", "Add objects/primitives", "M_add", 0, 0);
        laAddOperatorProperty(pc, "_this_M_separate", "Separate", "Separate mesh parts", "M_separate", 0, 0);
        laAddOperatorProperty(pc, "_this_M_combine", "Combine", "Combine mesh objects", "M_combine", 0, 0);
        laAddOperatorProperty(pc, "_this_M_duplicate", "Duplicate", "Duplicate objects", "M_duplicate", 0, 0);
        laAddOperatorProperty(pc, "_this_M_recalculate_normals", "Recalculate Normals", "Recalculate normals", "M_recalculate_normals", 0, 0);
        laAddOperatorProperty(pc, "_this_M_knife", "Knife", "Cut though edges", "M_knife", 0, 0);
        laAddOperatorProperty(pc, "_this_M_merge", "Merge", "Merge vertices", "M_merge", 0, 0);
        laAddOperatorProperty(pc, "_this_M_clear_transformations", "Clear Transformations", "Clear object transformations", "M_clear_transformations", 0, 0);

        laAddOperatorProperty(pc, "_this_M_set_point_handle", "Set Point Handle", "Set handle type of selected points", "M_set_point_handle", 0, 0);
        laAddOperatorProperty(pc, "_this_M_set_shape_closed", "Set Shape Closed", "Open or close the shape", "M_set_shape_closed", 0, 0);
    }

    km = &ct->KeyMapper;
    laAssignNewKey(km, 0, "LA_3d_view_camera_zoom", LA_KM_SEL_UI_EXTRA, 0, LA_MOUSE_WHEEL_DOWN, 0, "direction=out");
    laAssignNewKey(km, 0, "LA_3d_view_camera_zoom", LA_KM_SEL_UI_EXTRA, 0, LA_MOUSE_WHEEL_UP, 0, "direction=in");
    laAssignNewKey(km, 0, "LA_3d_view_camera_rotate", LA_KM_SEL_UI_EXTRA, LA_KEY_ALT, LA_L_MOUSE_DOWN, 0, 0);
    laAssignNewKey(km, 0, "LA_3d_view_camera_rotate", LA_KM_SEL_UI_EXTRA, 0, LA_M_MOUSE_DOWN, 0, 0);
    laAssignNewKey(km, 0, "LA_3d_view_camera_move", LA_KM_SEL_UI_EXTRA, LA_KEY_SHIFT, LA_M_MOUSE_DOWN, 0, 0);
    laAssignNewKey(km, 0, "LA_3d_view_camera_move", LA_KM_SEL_UI_EXTRA, LA_KEY_SHIFT | LA_KEY_ALT, LA_L_MOUSE_DOWN, 0, 0);
    //laAssignNewKey(km, 0, "TNS_align_camera_to_view", LA_KM_SEL_UI_EXTRA, LA_KEY_ALT | LA_KEY_SHIFT, LA_R_MOUSE_DOWN, 0, 0);
    //laAssignNewKey(km, 0, "TNS_align_view_to_camera", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, '0', 0);

    laAssignNewKey(km, 0, "M_set_cursor", LA_KM_SEL_UI_EXTRA, 0, LA_L_MOUSE_DOWN, 0, 0);
    laAssignNewKey(km, 0, "M_toggle_edit_mode", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, LA_KEY_TAB, 0);
    laAssignNewKey(km, 0, "M_select", LA_KM_SEL_UI_EXTRA, LA_KEY_ALT, LA_R_MOUSE_DOWN, 0, 0);
    laAssignNewKey(km, 0, "M_select", LA_KM_SEL_UI_EXTRA, LA_KEY_ALT|LA_KEY_CTRL, LA_R_MOUSE_DOWN, 0, 0);
    laAssignNewKey(km, 0, "M_select", LA_KM_SEL_UI_EXTRA, LA_KEY_SHIFT, LA_R_MOUSE_DOWN, 0, 0);
    laAssignNewKey(km, 0, "M_select", LA_KM_SEL_UI_EXTRA, LA_KEY_SHIFT|LA_KEY_ALT, LA_R_MOUSE_DOWN, 0, 0);
    laAssignNewKey(km, 0, "M_select", LA_KM_SEL_UI_EXTRA, LA_KEY_SHIFT|LA_KEY_ALT|LA_KEY_CTRL, LA_R_MOUSE_DOWN, 0, 0);
    laAssignNewKey(km, 0, "M_select", LA_KM_SEL_UI_EXTRA, 0, LA_R_MOUSE_DOWN, 0, 0);
    laAssignNewKey(km, 0, "M_select", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'b', "mode=box;");
    laAssignNewKey(km, 0, "M_select", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'a', "mode=toggle;");
    laAssignNewKey(km, 0, "M_grab", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'g', 0);
    laAssignNewKey(km, 0, "M_scale", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 's', 0);
    laAssignNewKey(km, 0, "M_rotate", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'r', 0);
    laAssignNewKey(km, 0, "M_clear_transformations", LA_KM_SEL_UI_EXTRA, LA_KEY_ALT, LA_KEY_DOWN, 'g', "channel=loc");
    laAssignNewKey(km, 0, "M_clear_transformations", LA_KM_SEL_UI_EXTRA, LA_KEY_ALT, LA_KEY_DOWN, 's', "channel=sca");
    laAssignNewKey(km, 0, "M_clear_transformations", LA_KM_SEL_UI_EXTRA, LA_KEY_ALT, LA_KEY_DOWN, 'r', "channel=rot");
    laAssignNewKey(km, 0, "M_clear_transformations", LA_KM_SEL_UI_EXTRA, LA_KEY_ALT, LA_KEY_DOWN, 't', 0);
    laAssignNewKey(km, 0, "M_make_parent", LA_KM_SEL_UI_EXTRA, LA_KEY_CTRL, LA_KEY_DOWN, 'p', 0);
    laAssignNewKey(km, 0, "M_unparent", LA_KM_SEL_UI_EXTRA, LA_KEY_ALT, LA_KEY_DOWN, 'p', 0);
    laAssignNewKey(km, 0, "M_extrude", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'e', 0);
    laAssignNewKey(km, 0, "M_extrude", LA_KM_SEL_UI_EXTRA, LA_KEY_SHIFT, LA_KEY_DOWN, 'd', "duplicate_only=true;text=Duplicate;");
    laAssignNewKey(km, 0, "M_delete", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'x', 0);
    laAssignNewKey(km, 0, "M_make", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'f', 0);
    laAssignNewKey(km, 0, "M_subdiv", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'w', 0);
    laAssignNewKey(km, 0, "M_add", LA_KM_SEL_UI_EXTRA, LA_KEY_SHIFT, LA_KEY_DOWN, 'a', 0);
    laAssignNewKey(km, 0, "M_separate", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'p', 0);
    laAssignNewKey(km, 0, "M_combine", LA_KM_SEL_UI_EXTRA, LA_KEY_CTRL, LA_KEY_DOWN, 'j', 0);
    laAssignNewKey(km, 0, "M_duplicate", LA_KM_SEL_UI_EXTRA, LA_KEY_SHIFT, LA_KEY_DOWN, 'd', 0);
    laAssignNewKey(km, 0, "M_recalculate_normals", LA_KM_SEL_UI_EXTRA, LA_KEY_CTRL, LA_KEY_DOWN, 'n', 0);
    laAssignNewKey(km, 0, "M_knife", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'k', 0);
    laAssignNewKey(km, 0, "M_knife", LA_KM_SEL_UI_EXTRA, LA_KEY_CTRL, LA_KEY_DOWN, 'r', "mode=loop_cut");
    laAssignNewKey(km, 0, "M_merge", LA_KM_SEL_UI_EXTRA, LA_KEY_ALT, LA_KEY_DOWN, 'm', 0);
    laAssignNewKey(km, 0, "M_select_linked", LA_KM_SEL_UI_EXTRA, LA_KEY_CTRL, LA_KEY_DOWN, 'l', 0);

    laAssignNewKey(km, 0, "M_set_point_handle", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'v', 0);
    laAssignNewKey(km, 0, "M_set_shape_closed", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'q', 0);
    laAssignNewKey(km, 0, "M_set_shape_closed", LA_KM_SEL_UI_EXTRA, LA_KEY_SHIFT, LA_KEY_DOWN, 'q', "reset=TRUE");
    laAssignNewKey(km, 0, "M_set_shape_hole", LA_KM_SEL_UI_EXTRA, 0, LA_KEY_DOWN, 'n', 0);
    laAssignNewKey(km, 0, "M_reorder_shape", LA_KM_SEL_UI_EXTRA, LA_KEY_CTRL, LA_KEY_DOWN, LA_KEY_ARRDOWN, "direction=DOWN");
    laAssignNewKey(km, 0, "M_reorder_shape", LA_KM_SEL_UI_EXTRA, LA_KEY_CTRL, LA_KEY_DOWN, LA_KEY_ARRUP, "direction=UP");
}
