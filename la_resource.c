﻿/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "la_5.h"

extern LA MAIN;
extern struct _tnsMain *T;


laUiType *_LA_UI_FIXED_GROUP;
laUiType *_LA_UI_TAB;
laUiType _LA_UI_CONDITION;      //NO PTR
laUiType _LA_UI_CONDITION_END;  //NO PTR
laUiType _LA_UI_CONDITION_ELSE; //NO PTR
laUiType _LA_UI_ROW_BEGIN;      //NO PTR
laUiType _LA_UI_ROW_END;        //NO PTR
laUiType _LA_UI_INVISIBLE;      //NO PTR
laUiType *_LA_UI_COLLECTION;
laUiType *_LA_UI_COLLECTION_ITEM;
laUiType *_LA_UI_COLLECTION_SELECTOR;
laUiType *_LA_UI_COLLECTION_SINGLE;
laUiType *_LA_UI_BUTTON;
laUiType *_LA_UI_LABEL;
laUiType *_LA_UI_INT;
laUiType *_LA_UI_METER_TYPE1;
laUiType *_LA_UI_METER_TYPE2;
laUiType *_LA_UI_VALUE_METER_2D;
laUiType *_LA_UI_FLOAT;
laUiType *_LA_UI_FLOAT_COLOR;
laUiType *_LA_UI_FLOAT_COLOR_HCY;
laUiType *_LA_UI_ENUM_SELECTOR;
laUiType *_LA_UI_MENU_ROOT;
laUiType *_LA_UI_CONDITION_TOGGLE;
laUiType *_LA_UI_STRING;
laUiType *_LA_UI_STRING_MULTI;
laUiType *_LA_UI_ALIGN;
laUiType *_LA_UI_CANVAS;
laUiType *_LA_UI_COLUMN_ADJUSTER;
laUiType *_LA_UI_COLUMN_VIEWER;
laUiType *_LA_UI_NODE_SOCKET;
laUiType *_LA_UI_HEIGHT_ADJUSTER;
laUiType *_LA_UI_RAW;
laUiType *_LA_UI_MAPPER;
laUiType *_LA_UI_IMAGE;
laUiType *_LA_UI_SCOPE;
laUiType *_LA_UI_MOUSE_ACTION_REPORTER;

laUiDefineFunc _LA_SUBPROP_DONT_CARE;

laBoxedTheme *_LA_THEME_PANEL;
laBoxedTheme *_LA_THEME_VALUATOR;
laBoxedTheme *_LA_THEME_BUTTON;
laBoxedTheme *_LA_THEME_STRING;
laBoxedTheme *_LA_THEME_SELECTOR;
laBoxedTheme *_LA_THEME_COLLECTION_SELECTOR;
laBoxedTheme *_LA_THEME_LABEL;
laBoxedTheme *_LA_THEME_TAB;
laBoxedTheme *_LA_THEME_COLLECTION_GROUP;
laBoxedTheme *_LA_THEME_COLLECTION_ITEM;
laBoxedTheme *_LA_THEME_FLOATING_PANEL;
laBoxedTheme *_LA_THEME_3D_VIEW;
laBoxedTheme *_LA_THEME_2D_VIEW;
laBoxedTheme *_LA_THEME_SOCKET;

laProp *_LA_PROP_PANEL;
laProp *_LA_PROP_BLOCK;
laProp *_LA_PROP_WINDOW;
laProp *_LA_PROP_UI_ITEM;
laProp *_LA_PROP_FAILED_ITEM;
laProp *_LA_PROP_TRASH_ITEM;
laProp *_LA_PROP_NODE_GRAPH;
laPropContainer *_LA_PROP_3D_EXTRA;
laPropContainer *_LA_PROP_2D_EXTRA;
laPropContainer *_LA_PROP_FILE_BROWSER;

laProp _P_LA_USE_INSTANCE_ONLY;

void la_RegisterMainOperators(){
    la_RegisterUiOperatorsBasic();
    la_RegisterViewerOperators();
    la_RegisterBuiltinOperators();
}

void la_RegisterMainUiTypes(){
    la_RegisterUiTypesBasic();
    la_RegisterUiTypesViewerWidgets();
}

void la_RegisterWindowKeys(){
    laKeyMapper* km=&MAIN.KeyMap;
    laAssignNewKey(km, 0, "LA_udf_read", 0, 0, LA_SIGNAL_EVENT, LA_SIGNAL_OPEN, 0);
    laAssignNewKey(km, 0, "LA_managed_save", 0, 0, LA_SIGNAL_EVENT, LA_SIGNAL_SAVE, "quiet=true;");
    laAssignNewKey(km, 0, "LA_managed_save", 0, 0, LA_SIGNAL_EVENT, LA_SIGNAL_SAVE_AS, 0);
    laAssignNewKey(km, 0, "LA_switch_layout", 0, 0, LA_SIGNAL_EVENT, LA_SIGNAL_LAYOUT_PREV, "reverse=true;");
    laAssignNewKey(km, 0, "LA_switch_layout", 0, 0, LA_SIGNAL_EVENT, LA_SIGNAL_LAYOUT_NEXT, 0);
    laAssignNewKey(km, 0, "LA_undo", 0, 0, LA_SIGNAL_EVENT, LA_SIGNAL_UNDO, 0);
    laAssignNewKey(km, 0, "LA_redo", 0, 0, LA_SIGNAL_EVENT, LA_SIGNAL_REDO, 0);
    laAssignNewKey(km, 0, "LA_fullscreen", 0, 0, LA_SIGNAL_EVENT, LA_SIGNAL_FULLSCREEN, "toggle=true;");

    laAssignNewKey(km, 0, "LA_system_paste", 0, LA_KEY_CTRL, LA_KEY_DOWN, 'v', 0);
}

laTheme* la_CreateClassicLightTheme(){
    laTheme *t; laBoxedTheme *bt;
    t = laDesignTheme("Classic Light");{
        LA_SET3(t->Color, 0.58,0.58,0.55);
        LA_SET3(t->ColorB, 0.58,0.58,0.55);
        LA_SET3(t->ColorC, 0.58,0.58,0.55);
        LA_SET3(t->AccentColor, 0.27,0.47,0.79);
        LA_SET3(t->WarningColor, 0.9,0.45,0.2);
        t->InactiveMix=0.7; t->InactiveSaturation=0.2;
        t->CursorAlpha=0.9; t->SelectionAlpha=0.5;
        t->ShadowAlpha=0.3; t->TextShadowAlpha=0.3;
        t->WireBrightness=0.5; t->WireSaturation=0.6; t->WireTransparency=0.65;
        t->EdgeBrightness=0.65; t->EdgeTransparency=0.8; t->VertexBrightness=0.65, t->VertexTransparency=0.9;
        t->SelectedFaceTransparency=0.6,t->SelectedEdgeTransparency=0.9, t->SelectedVertexTransparency=1.0;
        laDesignBoxedTheme(t, "Panel",&_LA_THEME_PANEL, 0,
            0.85, 0.5, 0.9, 0.2, 0.1, 0.9, 0, 0);
        laDesignBoxedTheme(t, "Floating Panel",&_LA_THEME_FLOATING_PANEL, 0,
            0.95, 0.8, 0.15, 0.15, 0.1, 0.9, 0, 0);
        laDesignBoxedTheme(t, "Valuator",&_LA_THEME_VALUATOR, 0,
            0.8, 0.5, 0.3, 0.2, 0.1, 0.9, 0, 0);
        laDesignBoxedTheme(t, "Button",&_LA_THEME_BUTTON, 0,
            0.97, 0.2, 0.3, 0.2, 0.9, 0.95, 0, 0);
        laDesignBoxedTheme(t, "String",&_LA_THEME_STRING, 0,
            0.9, 0.95, 0.3, 0.2, 0.1, 0.95, 0, 0);
        laDesignBoxedTheme(t, "Selector",&_LA_THEME_SELECTOR, 0,
            0.97, 0.25, 0.3, 0.2, 0.9, 0.95, 0, 0);
        laDesignBoxedTheme(t, "Collection Selector",&_LA_THEME_COLLECTION_SELECTOR, 0,
            0.85, 0.7, 0.3, 0.2, 0.1, 0.95, 0, 0);
        laDesignBoxedTheme(t, "Label",&_LA_THEME_LABEL, 0,
            0.7, 0.8, 0.3, 0.1, 0.05, 0.95, 0, 0);
        laDesignBoxedTheme(t, "Tab",&_LA_THEME_TAB, 0,
            0.9, 0.8, 0.2, 0.1, 0.7, 0.9, 0, 0);
        laDesignBoxedTheme(t, "Socket",&_LA_THEME_SOCKET, 0,
            0.97, 0.2, 0.3, 0.2, 0.9, 0.95, 0, 0);
        laDesignBoxedTheme(t, "Collection Group",&_LA_THEME_COLLECTION_GROUP, 0,
            0.75, 0.65, 0.2, 0.1, 0.7, 0.3, 0, 0);
        laDesignBoxedTheme(t, "Collection Item",&_LA_THEME_COLLECTION_ITEM, 0,
            0.75, 0.65, 0.2, 0.1, 0.7, 0.3, 0, 0);
        laDesignBoxedTheme(t, "3D Viewer",&_LA_THEME_3D_VIEW, 0,
            0.75, 0.6, 0.50, 0.1, 0.92, 0.6, 0, 0);
        laDesignBoxedTheme(t, "2D Viewer",&_LA_THEME_2D_VIEW, 0,
            0.75, 0.6, 0.50, 0.1, 0.92, 0.6, 0, 0);

        la_RefreshThemeColor(t);
        la_RegenerateWireColors();
    }
    return t;
}

laTheme* la_CreateClassicDarkTheme(){
    laTheme *t; laBoxedTheme *bt;
    t = laDesignTheme("Classic Dark");{
        LA_SET3(t->Color, 0.5,0.4,0.3);
        LA_SET3(t->ColorB, 0.5,0.4,0.3);
        LA_SET3(t->ColorC, 0.5,0.4,0.3);
        LA_SET3(t->AccentColor, 0.17,0.74,0.49);
        LA_SET3(t->WarningColor, 0.9,0.45,0.2);
        t->InactiveMix=0.7; t->InactiveSaturation=0.2;
        t->CursorAlpha=0.9; t->SelectionAlpha=0.4;
        t->ShadowAlpha=0.3; t->TextShadowAlpha=0.3;
        t->WireBrightness=0.6; t->WireSaturation=0.7; t->WireTransparency=0.65;
        t->EdgeBrightness=0.05; t->EdgeTransparency=0.8; t->VertexBrightness=0.05, t->VertexTransparency=0.9;
        t->SelectedFaceTransparency=0.6,t->SelectedEdgeTransparency=0.9, t->SelectedVertexTransparency=1.0;
        bt = laDesignBoxedTheme(t, "Panel",&_LA_THEME_PANEL, 0,
            0.2, 0.2, 0.1, 0.7, 0.9, 0.8, 0, 0);
        bt = laDesignBoxedTheme(t, "Floating Panel",&_LA_THEME_FLOATING_PANEL, 0,
            0.05, 0.05, 0.4, 0.8, 0.9, 0.8, 0, 0);
        bt = laDesignBoxedTheme(t, "Valuator",&_LA_THEME_VALUATOR, 0,
            0.3, 0.1, 0.4, 0.8, 0.9, 0.9, 0, 0);
        bt = laDesignBoxedTheme(t, "Button",&_LA_THEME_BUTTON, 0,
            0.1, 0.9, 0.3, 0.8, 0.1, 0.95, 0, 0);
        bt = laDesignBoxedTheme(t, "String",&_LA_THEME_STRING, 0,
            0.1, 0.03, 0.3, 0.8, 0.9, 0.95, 0, 0);
        bt = laDesignBoxedTheme(t, "Selector",&_LA_THEME_SELECTOR, 0,
            0.15, 0.7, 0.3, 0.8, 0.2, 0.95, 0, 0);
        bt = laDesignBoxedTheme(t, "Collection Selector",&_LA_THEME_COLLECTION_SELECTOR, 0,
            0.15, 0.1, 0.3, 0.8, 0.9, 0.95, 0, 0);
        bt = laDesignBoxedTheme(t, "Label",&_LA_THEME_LABEL, 0,
            0.15, 0.1, 0.3, 0.8, 0.9, 0.95, 0, 0);
        bt = laDesignBoxedTheme(t, "Tab",&_LA_THEME_TAB, 0,
            0.2, 0.3, 0.8, 0.7, 0.1, 0.9, 0, 0);
        bt = laDesignBoxedTheme(t, "Socket",&_LA_THEME_SOCKET, 0,
            0.1, 0.65, 0.7, 0.8, 0.1, 0.95, 0, 0);
        bt = laDesignBoxedTheme(t, "Collection Group",&_LA_THEME_COLLECTION_GROUP, 0,
            0.25, 0.35, 0.4, 0.7, 0.1, 0.45, 0, 0);
        bt = laDesignBoxedTheme(t, "Collection Item",&_LA_THEME_COLLECTION_ITEM, 0,
            0.25, 0.35, 0.8, 0.7, 0.1, 0.3, 0, 0);
        bt = laDesignBoxedTheme(t, "3D Viewer",&_LA_THEME_3D_VIEW, 0,
            0.25, 0.5, 0.5, 0.7, 0.1, 0.6, 0, 0);
        bt = laDesignBoxedTheme(t, "2D Viewer",&_LA_THEME_2D_VIEW, 0,
            0.25, 0.5, 0.55, 0.7, 0.1, 0.6, 0, 0);

        la_RefreshThemeColor(t);
        la_RegenerateWireColors();
    }
    return t;
}

void la_RegisterMainThemes(){
    strSafeSet(&MAIN.example_string,
"hello(){\n"
"    world!\n"
"    This is a LaGUI application 🤔\n"
"}");

    la_UDFAppendSharedTypePointer("BT Panel", &_LA_THEME_PANEL);
    la_UDFAppendSharedTypePointer("BT Floating Panel", &_LA_THEME_FLOATING_PANEL);
    la_UDFAppendSharedTypePointer("BT Valuator", &_LA_THEME_VALUATOR);
    la_UDFAppendSharedTypePointer("BT Button", &_LA_THEME_BUTTON);
    la_UDFAppendSharedTypePointer("BT String", &_LA_THEME_STRING);
    la_UDFAppendSharedTypePointer("BT Selector", &_LA_THEME_SELECTOR);
    la_UDFAppendSharedTypePointer("BT Collection Selector", &_LA_THEME_COLLECTION_SELECTOR);
    la_UDFAppendSharedTypePointer("BT Label", &_LA_THEME_LABEL);
    la_UDFAppendSharedTypePointer("BT Tab", &_LA_THEME_TAB);
    la_UDFAppendSharedTypePointer("BT Collection Group", &_LA_THEME_COLLECTION_GROUP);
    la_UDFAppendSharedTypePointer("BT Collection Item", &_LA_THEME_COLLECTION_ITEM);
    la_UDFAppendSharedTypePointer("BT 3D Viewer", &_LA_THEME_3D_VIEW);
    la_UDFAppendSharedTypePointer("BT 2D Viewer", &_LA_THEME_2D_VIEW);
    la_UDFAppendSharedTypePointer("BT Socket", &_LA_THEME_SOCKET);
}
