/*
* LaGUI: A graphical application framework.
* Copyright (C) 2022-2023 Wu Yiming
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "la_5.h"
#include <nanovg.h>
//#include "tinycthread.h"
#include <float.h>

#ifdef __cplusplus
extern "C" {
#endif
extern const char* TNS_SHADER_COLOR_COMMON;
extern const char* TNS_VERTEX_SIMPLE_MATCAP;
extern const char* TNS_FRAGMENT_SIMPLE_MATCAP;
extern const char* TNS_VERTEX_GRID;
extern const char* TNS_FRAGMENT_TRANSPARNT_GRID;
extern const char* LA_FLOOR_VERTEX_SHADER;
extern const char* LA_FLOOR_FRAGMENT_SHADER;
extern const char* LA_IMM_VERTEX_SHADER;
extern const char* LA_IMM_FRAGMENT_SHADER;
extern const char* LA_OBJECT_FRAGMENT_SHADER;
extern const char* LA_RAY_VERTEX_SHADER;
extern const char* LA_SHADER_LIB_FXAA;
extern const char* LA_RAY_FRAGMENT_SHADER;
extern const char* LA_SCENE_VERTEX_SHADER;
extern const char* LA_SCENE_FRAGMENT_SHADER;
extern const char* LA_CASCADE_SHADOW_VERTEX_SHADER;
extern const char* LA_CASCADE_SHADOW_FRAGMENT_SHADER;
extern const char* LA_SELECTION_VERTEX_SHADER;
extern const char* LA_SELECTION_FRAGMENT_SHADER;
#ifdef __cplusplus
}
#endif

#define TNS_PI 3.1415926535897932384626433832795
#define deg(r) ((r) / TNS_PI * 180.0)
#define rad(d) ((d) *TNS_PI / 180.0)

#define TNS_COLOR_SPACE_SRGB 0
#define TNS_COLOR_SPACE_CLAY 1
#define TNS_COLOR_SPACE_D65_P3 2

#define TNS_MIX_NORMAL 0
#define TNS_MIX_ADD    1

//typedef real tnsMatrix33d[9];

typedef float tnsMatrix44f[16];

typedef real tnsMatrix44d[16];
typedef real tnsVector2d[2];
typedef real tnsVector3d[3];
typedef real tnsVector4d[4];

typedef float tnsVector3f[3];

typedef int tnsVector2i[2];

typedef struct _tnsMatrixStackItem tnsMatrixStackItem;
struct _tnsMatrixStackItem
{
    tnsMatrix44d view;
    tnsMatrix44d model;
    tnsMatrix44d projection;
};
typedef struct _tnsMatrixStack tnsMatrixStack;
struct _tnsMatrixStack
{
    tnsMatrixStackItem *level;
    int current_level;
    int max_level;
};

typedef struct _tnsShader tnsShader;
struct _tnsShader{
    laListItem Item;
    int vtShaderID;
    int fgShaderID;
    int gsShaderID;
    int glProgramID;

    int CustomID;

    int iModel, iProjection, iView, iProjectionInverse;
    int iShadow;

    int iVertex, iNormal, iColor, iUV;
    int iUseNormal;

    int iTexColor,iTexColor2,iTexNormal,iTexGPos;
    int iTexIsUI,iTex2IsUI;
    int iTexColorMS,iTexColorU,iTexColor2U;
    int iMultiplyColor;
    int iTextureMode,iColorMode,iHCYGamma;
    int iSampleAmount;
    int iInputColorSpace, iOutputColorSpace, iShowStripes, iTexLut, iUseLut;
    int iComposing, iComposingGamma, iComposingBlackpoint;
    int iDoOffset;
    int iUseHalftone,iHalftoneSize;
    int StateTextureMode,StateMultiplyColor,StateSampleAmount,StateTexIsUI,StateTex2IsUI;

    int uViewDir,uViewPos,uFOV,uNear,uFar;
};
typedef struct _tnsTexture tnsTexture;

STRUCTURE(tnsCommand){
    GLenum Mode;
    short Dimensions; // 2 or 3
    short UVDimensions; // 2 or 3
    char UseVert;
    char UseColor;
    char UseNormal;
    char UseTexCoord;
    char UseIndex;
    GLenum PolyMode; //0-solid 1-wire
    GLenum Shade;    //0-falt 1-smooth
    GLfloat UniformColor[4];
    real UseHalftone;

    int NumVert;
    int NumIndex;
    int VertBegin;
    int VertEnd; //'END'is the next one after the last one.
    int ColorBegin;
    int ColorEnd;
    int TexCoordBegin;
    int TexCoordEnd;
    int NormalBegin;
    int NormalEnd;
    GLushort IndexBegin;
    GLushort IndexEnd;

    tnsShader *ReplaceShader;
    tnsTexture *ColorTexture;
    tnsTexture *ColorTexture2;
    int TextureMode;
    int MultiplyColor;

    GLfloat LineWidth,PointSize;
};
typedef struct _tnsMain tnsMain;
typedef struct _tnsWorld tnsWorld;
typedef struct _tnsObject tnsObject;
struct _tnsWorld
{
    laListItem Item;
    laListHandle RootObjects;
    tnsObject *ActiveRoot;

    laListHandle AllObjects;
    laListHandle Materials;
    laSafeString* MaterialLibraries;

    int PropertyPage;

    u16bit TimeYear;
    u8bit TimeMonth;
    u8bit TimeDay;

    laListHandle BezierCurves;
    laListHandle Meshes;
};

NEED_STRUCTURE(tnsLoopItem);
NEED_STRUCTURE(tnsRenderLine);
NEED_STRUCTURE(tnsRenderBuffer);
NEED_STRUCTURE(tnsEvaluatedNode);

STRUCTURE(tnsImage){
    laListItem Item;
    void* MemPNG;
    tnsTexture* Texture;
    int UserCount;
};

struct _tnsMain {
    laListItem Item;

    tnsMatrixStack stack;

    int vsx,vsy,vsw,vsh;//for saving scissor etc.
    int vol,vor,vou,vob;//for saving ortho etc.

    laListHandle Shaders;
    int NextShaderIndex;
    tnsShader *CurrentShader;
    tnsShader *BindedShader;

    GLuint CurrentVAO;
    SYSGLCONTEXT CurrentContext;
#ifdef LA_USE_GLES
    EGLSurface CurrentSurface;
#endif
#ifdef _WIN32
    SYSTEMDC CurrentDC;
#endif
#ifdef __linux__
    SYSWINDOW CurrentWindow;
#endif

    //int            MatrixMode;
    int IsOffscreen;
    laListHandle Offscreens;

    const char *GLVersionStr;
    const char *GLVendorStr;
    const char *GLRendererStr;
    const char *GLSLVersionStr;

    tnsShader* immShader;
    tnsShader* RayShader;
    tnsShader* ShadowShader;
    tnsShader* SceneShader;
    tnsShader* SelectionShader;
    tnsShader* FloorShader;

    tnsShader *uiShader;
    tnsShader *stringShader;
    tnsShader *TextureShader;
    tnsShader *RectangleTextureShader;
    tnsShader *TextureMultiplyShader;
    tnsShader *MSTextureShader;
    tnsShader *MSATextureShader;
    tnsShader *TEST_MatcapShader;
    tnsShader *TransparentGridShader;
    tnsShader *SobelColorShader;
    tnsShader *SobelShader;
    tnsShader *ExtraBuffersShader;

    laListHandle Textures;
    tnsTexture *PreviewTexture;

    GLenum GlTextureSets;

    // Corresponds to current GL_TEXTURE0/1...
    tnsTexture *TexColor;
    tnsTexture *TexColor2; //==TEXTURE4
    tnsTexture *TexRenderbuffer;

    // For commands
    tnsTexture *StateTexColor;
    tnsTexture *StateTexColor2;
    int StateTextureMode;
    int StateMultiplyColor;
    int StateUseNormal;
    int SetUseNormal;
    real StateUseHalftone;
    real SetUseHalftone;
    real SetHalftoneSize;
    float SetViewPos[3];
    float SetViewDir[3];
    
    tnsShader* StateShader;

    GLfloat StateColor[4];
    GLfloat StateLineWidth,SetLineWidth;
    GLfloat StatePointSize,SetPointSize;

    GLfloat* Vert;
    GLuint VertBufObject;
    int NextVert, MaxVert;

    GLfloat* Color;
    GLuint ColorBufObject;
    int NextColor, MaxColor;

    GLfloat* Normal;
    GLuint NormalBufObject;
    int NextNormal, MaxNormal;

    GLfloat* TexCoord;
    GLuint TexCoordBufObject;
    int NextTexCoord, MaxTexCoord;

    GLuint* Index;
    GLuint IndexBufObject;
    int NextIndex, MaxIndex;

    tnsCommand* DrawingCommand;
    int NextCommand, MaxCommand;

    tnsWorld* World;

    laListHandle RenderBuffers;
    tnsRenderBuffer *ActiveRenderBuffer;

    laListHandle Images;

    struct Runtime{
        tnsEvaluatedNode* CurrentEN;
    }Runtime;
};

#define TNS_CHROMA_COMPRESSION_YUYV 1
#define TNS_CHROMA_COMPRESSION_UYVY 2

typedef struct _tnsTexture tnsTexture;
struct _tnsTexture
{
    laListItem Item;

    int IsRenderBuffer;

    GLuint GLTexHandle;
    GLint GLTexBitsType; //like GL_RGBA
    GLuint GLTexType; //like GL_TEXTURE_2D
    int IsUIntTexture;
    int Multisample;
    int ChromaCompression;
    int Width;
    int Height;
    int Slices;

    void *DrawData;

    int RBWidth, RBHeight;
    int ElemSize;
    void *TextureReadBack;
    void **SamplePtr;
    laListHandle PendingSamples;
    laListHandle ErasedSamples;

    laListHandle LineStrips;
};

typedef struct _tnsOffscreen tnsOffscreen;
struct _tnsOffscreen
{
    laListItem Item;

    tnsTexture *pColor[16];
    tnsTexture *pDepth;

    GLuint FboHandle;
    SYSGLCONTEXT FboContext;
#ifdef LA_USE_GLES
    EGLSurface FboSurface;
#endif
#ifdef _WIN32
    SYSTEMDC FboDC;
#endif
#ifdef __linux__
    SYSWINDOW FboWindow;
#endif

    int UseSecondary;
};

extern const GLuint TNS_ATTACHMENT_ARRAY_NONE[];
extern const GLuint TNS_ATTACHMENT_ARRAY[];
extern const GLuint TNS_ATTACHMENT_ARRAY_1[];
extern const GLuint TNS_ATTACHMENT_ARRAY_2[];
extern const GLuint TNS_ATTACHMENT_ARRAY_1_2[];
extern const GLuint TNS_ATTACHMENT_ARRAY_0_1_2[];
extern const GLenum TNS_WINDOW_DRAWBUFFER_ARRAY[];

#define TNS_PROJECTION_MATRIX 1
#define TNS_MODEL_MATRIX 2
#define TNS_VIEW_MATRIX 3
#define TNS_TEXTURE_MATRIX 4

//==========================================================================[FT]
#include "freetype/ftglyph.h"
#include "freetype/ftoutln.h"
#include "freetype/fttrigon.h"
#include "ft2build.h"
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

NEED_STRUCTURE(tnsTexture);

typedef struct _tnsFontSingleCharacter tnsFontSingleCharacter;
struct _tnsFontSingleCharacter
{
    tnsTexture *Tex;
    int Generated;
    short width;
    short height;
    real  advx;
    short advy;
    short deltax;
    short deltay;

    short bufferx;
    short buffery;
};


#define TNS_UNICODE_COUNT ((unsigned int)(1<<17))
#define TNS_MONO_COUNT (0x045f)

typedef struct _tnsFont tnsFont;
struct _tnsFont
{
    laListItem Item;

    char *fontName;
    char *IconFontName;

    tnsFontSingleCharacter** characters; /* [(1<<17)] TNS_UNICODE_COUNT Unicode plane 0-1 */
    tnsFontSingleCharacter** monocharacters; /* [0x045f] TNS_MONO_COUNT Up to cryllic. */

    FT_Library ftlib;
    FT_Face ftface[16];
    FT_Face ftfacemono;
    int NumFaces;/* excludes mono. */
    real MonoScale;
    real MonoAdvance;

    real size;
    unsigned int height;

    tnsTexture TexBuffer;
    int CurrentX;
    int CurrentY;
};

typedef struct _tnsStringClip tnsStringClip;
struct _tnsStringClip{
    laListItem Item;
    int L,R,U,B;
};

typedef struct _tnsFontManager tnsFontManager;
struct _tnsFontManager
{
    laListHandle Fonts;
    tnsFont *UsingFont;

    tnsFont *VectorsGrapghs;

    int BufferWidth;

    unsigned LastDlst;

    laListHandle StringClips;
    int L,R,U,B;
};

typedef struct _tnsFontBoundBox tnsFontBoundBox;
struct _tnsFontBoundBox
{
    int x, y, w, h; /* Upper Left and width,height */
};

#define TNS_FONT_BUFFER_W_DEFAULT 512

//=====================================================[3d comp]

typedef struct _tnsWorld tnsWorld;
typedef struct _tnsObject tnsObject;

#define TNS_ROTATION_XYZ_EULER 0
#define TNS_ROTATION_XZY_EULER 1
#define TNS_ROTATION_YXZ_EULER 2
#define TNS_ROTATION_YZX_EULER 3
#define TNS_ROTATION_ZXY_EULER 4
#define TNS_ROTATION_ZYX_EULER 5
#define TNS_ROTATION_QUATERNION 6

#define TNS_OBJECT_ROOT   0
#define TNS_OBJECT_INSTANCER  (1<<0)
#define TNS_OBJECT_CAMERA (1<<1)
#define TNS_OBJECT_LIGHT  (1<<2)
#define TNS_OBJECT_MESH   (1<<3)
#define TNS_OBJECT_SHAPE  (1<<4)

#define TNS_OBJECT_FLAGS_SELECTED (1<<0)
#define TNS_OBJECT_FLAGS_PLAY_DUPLICATE (1<<1)

NEED_STRUCTURE(tnsBatch)

#define TNS_EVAL_LAYER_SOLID     (1<<0)
#define TNS_EVAL_LAYER_OVERLAY   (1<<1)
#define TNS_EVAL_LAYER_OUTLINE   (1<<2)
#define TNS_EVAL_LAYER_SELECTION (1<<3)
#define TNS_EVAL_LAYER_NO_2D     (1<<4)
#define TNS_EVAL_LAYER_BACKDROP  (1<<5)
#define TNS_EVAL_LAYER_SHOW_2D   (1<<6)

#define TNS_EVAL_MODE_INIT   (1<<0)
#define TNS_EVAL_MODE_ALWAYS (1<<1)
#define TNS_EvAL_MODE_DEINIT (1<<2)

NEED_STRUCTURE(tnsEvaluatedInstance);
typedef void (*tnsDrawEvaluatedInstanceF)(tnsEvaluatedInstance* ei, void* CustomData);

NEED_STRUCTURE(laActionRetarget);

STRUCTURE(tnsEvaluatedNode){
    laListItem Item;
    tnsMatrix44d Mat;
    tnsObject* Target;
    laActionRetarget* ActionRetarget;
    laListHandle Children;
    int LuaID, LuaLoaded;
};
STRUCTURE(tnsEvaluatedScene){
    int NextLuaID;
    laListHandle WastedEvaluateNodes;
    tnsEvaluatedNode* Root;
    tnsEvaluatedNode* CurrentParent;
    tnsEvaluatedNode* CurrentChild;
};
STRUCTURE(tnsEvaluatedInstance){
    tnsObject *Object;
    tnsMatrix44d Mat;
    int IsActive;
    int MeshSelectionType;
    int InstanceSelectionID;
    int DrawnIn2D;
    tnsDrawEvaluatedInstanceF Draw;
};
STRUCTURE(tnsEvaluateData){
    int Done;
    int SceneEvaluateMode;
    tnsObject *Active;
    int FillOutline;
    int FillSelectionID;
    int OverrideID;
    int State2D,Has2D; int W,H;
    laListHandle ParentStack;
    tnsEvaluatedInstance* Backdrop; int NextBackdrop, MaxBackdrop;
    tnsEvaluatedInstance* Commands; int NextCommand, MaxCommand;
    tnsEvaluatedInstance* Outlines; int NextOutline, MaxOutline;
    tnsEvaluatedInstance* Overlays; int NextOverlay, MaxOverlay;
    tnsEvaluatedInstance* Selections; int NextSelection, MaxSelection;
    tnsMatrix44d* MatArr; int NextMat, MaxMat;
    tnsEvaluatedScene* Scene;
#ifdef LA_WITH_LUAJIT
    lua_State* L;
#endif
};

NEED_STRUCTURE(laRackPageCollection);

NEED_STRUCTURE(tnsMaterial);
STRUCTURE(tnsObject){
    laListItem Item;

    int Type;
    int SelectID;
    int Flags;
    //u64bit ID;
    laSafeString *Name;
    int DrawMode;

    int Show;
    int ShowOnRender;
    int HideChildren;

    int RotationMode;
    int IsInstanced;

    real GLocation[3]; real GRotation[3]; real GScale[3];
    real Location[3];  real Rotation[4];  real Scale[3];

    // delta transforms for animation & drivers.
    real DLocation[3]; real DRotation[3]; real DScale[3];

    tnsMatrix44d GlobalTransform;
    tnsMatrix44d SelfTransform;
    tnsMatrix44d DeltaTransform;

    tnsObject *Active;  // may be used to store last select info in child objects.
    tnsObject *InRoot;
    tnsObject *ParentObject;   // reused as root active sun.
    laListHandle ChildObjects;

    laRackPageCollection* Drivers; // rack

    tnsEvaluateData Evaluated; // runtime
    tnsEvaluateData EvaluatedPlay; int LuaCacheID;
    tnsObject* PlayDuplicate;
    tnsObject* EditDuplicateTemp;
};

NEED_STRUCTURE(laNodeInSocket);
NEED_STRUCTURE(laNodeOutSocket);
NEED_STRUCTURE(laAction);

#define TNS_PAGE_TRIGGER_PROCESS 0
#define TNS_PAGE_TRIGGER_KEYBOARD 1
#define TNS_PAGE_TRIGGER_MOUSE 2
#define TNS_PAGE_TRIGGER_CONTROLLER 3

STRUCTURE(tnsTransformNode){
    laBaseNode Base;
    laNodeInSocket* Mat;
    tnsObject* Target;
};
STRUCTURE(tnsMakeTransformNode){
    laBaseNode Base;
    laNodeInSocket* Loc; laNodeInSocket* Rot; laNodeInSocket* Angle; laNodeInSocket* Sca;
    laNodeOutSocket* Out;
    tnsMatrix44d Mat; tnsVector3d UseLoc; tnsVector3d UseRot; real UseSca; real UseAngle;
};
STRUCTURE(tnsActionPlayerNode){
    laBaseNode Base;
    laNodeInSocket* Prev; laNodeOutSocket* Next;
    laNodeInSocket* Transport; int iTransport;
    laNodeInSocket* Time;
    laNodeInSocket* Detached; int iDetached;
    laAction* Action;
};

#define TNS_CAMERA_PERSP 0
#define TNS_CAMERA_ORTHO 1

#define TNS_ID_TO_COLOR(color, i) \
    {color[0]=(real)((i & 0x000000FF)>>0)/255.0; color[1]=(real)((i & 0x0000FF00)>>8)/255.0; color[2]=(real)((i & 0x00FF0000)>>16)/255.0;}

#define TNS_GRADIENT_MODE_LINEAR 1
#define TNS_GRADIENT_MODE_BOX    2
#define TNS_GRADIENT_MODE_RADIAL 3

STRUCTURE(tnsMaterial){
    laListItem Item;
    laSafeString *Name;
    real Color[4], Color2[4];
    int Colorful;
    int AsLibrary;
    laRackPage* Page;
    tnsShader* Shader;
    int GradientMode;
    tnsVector2d GradientCenter;
    tnsVector2d GradientSize;
    real GradientBoxR,GradientBoxF;
};

NEED_STRUCTURE(laActionRetarget);

STRUCTURE(tnsRootObject){
    tnsObject Base;

    laListHandle Actions;
    laListHandle ActionProps;
    laActionRetarget* ActionRetarget;

    tnsObject* ActiveCamera;
    int Is2D;
};

#define TNS_INSTANCER_HOOK_TL 1
#define TNS_INSTANCER_HOOK_TC 2
#define TNS_INSTANCER_HOOK_TR 3
#define TNS_INSTANCER_HOOK_CL 4
#define TNS_INSTANCER_HOOK_CC 5
#define TNS_INSTANCER_HOOK_CR 6
#define TNS_INSTANCER_HOOK_BL 7
#define TNS_INSTANCER_HOOK_BC 8
#define TNS_INSTANCER_HOOK_BR 9

STRUCTURE(tnsInstancer){
    tnsObject Base;
    
    tnsObject* Instance;

    int Hook; real HookOffset[2];
};

STRUCTURE(tnsCamera){
    tnsObject Base;

    int CameraType;
    real FOV;
    real ZMin, ZMax;
    real FocusDistance;

    real OrthScale;
    tnsVector3d RenderViewDir;
};

STRUCTURE(tnsLight){
    tnsObject Base;

    int LightType;
    int UniDirectional;
    real Strength;

    tnsMaterial *Material;
};

#define TNS_MESH_FLAG_SELECTED (1<<0)
#define TNS_MESH_FLAG_PICKED   (1<<1)
#define TNS_MESH_FLAG_LOOP_REVERSE (1<<2)

STRUCTURE(tnsMVert){
    laListItem Item;
    tnsVector3d p;
    tnsVector3d n;
    int flags;
    u32bit i;
    laListHandle elink;
    //laListHandle flink;
};
NEED_STRUCTURE(tnsMFace);
STRUCTURE(tnsMEdge){
    laListItem Item;
    tnsMVert *vl;
    tnsMVert *vr;
    tnsMFace *fl;
    tnsMFace *fr;
    u32bit i;
    int flags;
};
STRUCTURE(tnsMFace){
    laListItem Item;
    laListHandle l; //list item pointer of edges
    tnsVector3d n;
    tnsVector3d c;
    short looplen;
    short mat;
    u32bit i;
    int flags;
};

STRUCTURE(tnsVert){
    tnsVector3f p;
    tnsVector3f n;
    int flags;
};
STRUCTURE(tnsEdge){
    int l,r;
    int flags;
};
STRUCTURE(tnsFace){
    int* loop;
    int flags;
    tnsVector3f n;
    short looplen;
    short mat;
};

STRUCTURE(tnsBatchCommand){
    laListItem Item;
    const char* name;
    int DrawElements;
    GLuint EBO; //elem
    u32bit ElementCount;
    GLenum DrawAs;
    GLuint CBO; int ColorDimension; int OverrideColorArray;
    int Dimension;
    int HiddenByDefault;
    int UseUniformColor; real UniformColor[4];
    tnsMaterial* Material;
    real Width;
};
STRUCTURE(tnsBatch){
    laListItem Item;
    GLuint VBO; //vert
    GLuint NBO; int HasNormal; //normal
    GLuint CBO; int HasColor; //color
    u32bit NumVert;
    int Dimension;
    int NormalDimension;
    int ColorDimension;
    laListHandle Branches;
    GLuint BeginElementOffset;
};

#define TNS_MESH_OBJECT_MODE 0
#define TNS_MESH_EDIT_MODE   1

extern laPropContainer* TNS_PC_OBJECT_GENERIC;
extern laPropContainer* TNS_PC_OBJECT_INSTANCER;
extern laPropContainer* TNS_PC_OBJECT_CAMERA;
extern laPropContainer* TNS_PC_OBJECT_LIGHT;
extern laPropContainer* TNS_PC_OBJECT_MESH;
extern laPropContainer* TNS_PC_OBJECT_SHAPE;
extern laPropContainer* TNS_PC_OBJECT_ROOT;
extern laPropContainer* TNS_PC_MATERIAL;
extern laPropContainer* TNS_PC_MATERIAL_SLOT;

#define TNS_HINT_TRANSFORM (1<<1)
#define TNS_HINT_GEOMETRY  (1<<0)

STRUCTURE(tnsMeshFaceStorageSingleHead){
    int looplen, flags; //[1] [5] [4] [2] ... [-1 as end]
};
STRUCTURE(tnsMeshFaceStorage){
    int len; int arr;
};

STRUCTURE(tnsMaterialSlot){
    laListItem Item;
    tnsMaterial* Material;
    tnsObject* Parent;
    short Index;
};

#define TNS_SPOINT_ALIGNED (1<<3)
#define TNS_SPOINT_FREE    (1<<4)
#define TNS_SPOINT_SELECTED_L (1<<5)
#define TNS_SPOINT_SELECTED_R (1<<6)
#define TNS_SPOINT_BEZIER  (TNS_SPOINT_ALIGNED|TNS_SPOINT_FREE)
#define TNS_SPOINT_SELECTED (TNS_MESH_FLAG_SELECTED|TNS_SPOINT_SELECTED_R|TNS_SPOINT_SELECTED_L)
#define TNS_SHAPE_CLOSED   (1<<7)
#define TNS_SHAPE_HOLE     (1<<8)

STRUCTURE(tnsSPoint){
    laListItem Item;
    tnsVector2d p,dl,dr;
    int flags;
    u32bit i;
};
STRUCTURE(tnsShape){
    laListItem Item;
    laListHandle Points;
    int flags;
    short mat;
};

STRUCTURE(tnsShapeObject){
    tnsObject Base;

    laListHandle Shapes;
    
    tnsBatch *Batch;
    tnsBatch *ExtraBatch;

    tnsMaterialSlot* CurrentMaterial;
    laListHandle Materials;

    int Mode;
    int Backdrop;
};

STRUCTURE(tnsMeshObject){
    tnsObject Base;

    tnsVert* v; int totv, maxv;
    tnsEdge* e; int tote, maxe; // I intend this to only stores floating edges.
    tnsFace* f; int totf, maxf;

    laListHandle mv; int totmv;
    laListHandle me; int totme;
    laListHandle mf; int totmf;

    int TriangleCount;
    int TriangulatedEdgeCount;

    tnsMVert* LastSelectV, *FirstSelectV;
    tnsMEdge* LastSelectE, *FirstSelectE;
    
    tnsBatch *Batch;
    tnsBatch *ExtraBatch;

    tnsMaterialSlot* CurrentMaterial;
    laListHandle Materials;

    int Mode;
};

STRUCTURE(tnsEdgeHashEdge){
    int tv; tnsMEdge* me;
};
STRUCTURE(tnsEdgeHashVert){
    tnsEdgeHashEdge* e; int max, next; tnsMVert* mv;
};
STRUCTURE(tnsEdgeHash){
    tnsEdgeHashVert* vl; int max,next;
};

#define TNS_TILE(tile, r, c, CCount) \
    tile[r * CCount + c]

#define TNS_CLAMP(a, Min, Max) \
    a = a < Min ? Min : (a > Max ? Max : a)

#define TNS_MAX2(a, b) \
    (a > b ? a : b)

#define TNS_MIN2(a, b) \
    (a < b ? a : b)

#define TNS_MAX3(a, b, c) \
    (a > TNS_MAX2(b, c) ? a : TNS_MAX2(b, c))

#define TNS_MIN3(a, b, c) \
    (a < TNS_MIN2(b, c) ? a : TNS_MIN2(b, c))

#define TNS_MAX2_INDEX(a, b) \
    (a > b ? 0 : 1)

#define TNS_MIN2_INDEX(a, b) \
    (a < b ? 0 : 1)

#define TNS_MAX3_INDEX(a, b, c) \
    (a > b ? (a > c ? 0 : (b > c ? 1 : 2)) : (b > c ? 1 : 2))

#define TNS_MIN3_INDEX(a, b, c) \
    (a < b ? (a < c ? 0 : (b < c ? 1 : 2)) : (b < c ? 1 : 2))

#define TNS_MAX3_INDEX_ABC(x, y, z) \
    (x > y ? (x > z ? a : (y > z ? b : c)) : (y > z ? b : c))

#define TNS_MIN3_INDEX_ABC(x, y, z) \
    (x < y ? (x < z ? a : (y < z ? b : c)) : (y < z ? b : c))

#define TNS_ABC(index) \
    (index == 0 ? a : (index == 1 ? b : c))

#define TNS_DOUBLE_CLOSE_ENOUGH(a, b) \
    (((a) + DBL_EDGE_LIM) >= (b) && ((a)-DBL_EDGE_LIM) <= (b))

    //#define TNS_DOUBLE_CLOSE_ENOUGH(a,b) \
//(((a)+0.00000000001)>=(b) && ((a)-0.0000000001)<=(b))

#define TNS_FLOAT_CLOSE_ENOUGH_WIDER(a, b) \
    (((a) + 0.0000001) >= (b) && ((a)-0.0000001) <= (b))

#define TNS_FRAMEBUFFER_PIXEL(FrameBuffer, Row, Column) \
    &((FrameBuffer)->Pixels[Row * FrameBuffer->TileSizeW * FrameBuffer->W * FrameBuffer->SubPixelSample + Column * FrameBuffer->H * FrameBuffer->TileSizeH * FrameBuffer->SubPixelSample])

#define TNS_IN_TILE_X(RenderTile, Fx) \
    (RenderTile->FX <= Fx && RenderTile->FXLim >= Fx)

#define TNS_IN_TILE_Y(RenderTile, Fy) \
    (RenderTile->FY <= Fy && RenderTile->FYLim >= Fy)

#define TNS_IN_TILE(RenderTile, Fx, Fy) \
    (TNS_IN_TILE_X(RenderTile, Fx) && TNS_IN_TILE_Y(RenderTile, Fy))

void tnsSetuptnsFontManager();

char* tnsEnsureShaderCommoms(const char* Content, const char* Library, const char* Material);
tnsShader *tnsNewShaderProgram(int VertexShaderID, int FragmentShaderID, int GeometryShaderID);
int tnsNewGeometryShader(const char *Content);
int tnsNewFragmentShader(const char *Content);
int tnsNewFragmentShaderMaterial(const char *Content, const char* Library, const char* Material);
int tnsNewVertexShader(const char *Content);
void tnsDeleteShaderProgram(tnsShader* s);

int tnsNextPowOf2(int i);

int tnsEnableShader(int index);
int tnsEnableShaderv(tnsShader *shader);
void tnsUseShader(tnsShader *shader);
void tnsUseImmShader();
void tnsUseShadowShader();
void tnsUseSceneShader();
void tnsUseRayShader();
void tnsUseTransparentGridShader();

void tnsUseImagePeelShader();

void tnsInit();
void tnsInitRenderKernel(int matrixStackLevel);
void tnsInitBuiltinShaders();
void tnsInitWindowDefaultRenderConfig();
void tnsQuit();

void tnsRestoreFromNanoVG();

real *tnsGetModelMatrix();
real *tnsGetViewMatrix();
real *tnsGetProjectionMatrix();
void tnsGetMVMatrix(tnsMatrix44d r);
void tnsGetMVPMatrix(tnsMatrix44d r);
void tnsResetModelMatrix();
void tnsResetViewMatrix();
void tnsResetProjectionMatrix();

void tnsOrtho(real xMin, real xMax, real yMin, real yMax, real zMin, real zMax);
void tnsPerspective(real fFov_rad, real fAspect, real zMin, real zMax);

void tnsPopMatrix();
void tnsPushMatrix();

void tnsTranslate3d(real x, real y, real z);
void tnsPreTranslate3d(real x, real y, real z);
void tnsRotate4d(real degrees, real x, real y, real z);
void tnsPreRotate4d(real degrees, real x, real y, real z);
void tnsScale3d(real x, real y, real z);
void tnsPreScale3d(real x, real y, real z);

void tnsColor4d(real r, real g, real b, real a);
void tnsColor4dv(real *rgba);
void tnsLineWidth(real Width);
void tnsPointSize(real PointSize);

tnsBatch *tnsCreateBatch(u32bit NumVert, int Dimension, float *Data, int NormalDimension, float *Normal, int ColorDimension, float *Colors);
tnsBatch *tnsCreateBatchi(u32bit NumVert, int Dimension, int *Data);
void tnsCommandUseWidth(tnsBatchCommand*c, real width);
void tnsCommandUseUniformColor(tnsBatchCommand*c,real* color);
void tnsCommandUseMaterial(tnsBatchCommand*c, tnsMaterial* material);
void tnsCommandOverrideColorArray(tnsBatchCommand*c, int VertCount, int ColorDimension, float* colors);
tnsBatchCommand *tnsCreateCommand(tnsBatch *b, const char* name, u32bit ElementCount, int Dimension, GLenum DrawAs, u32bit *Elements, int HiddenByDefault);
void tnsDeleteBatch(tnsBatch *b);
int tnsDrawBatch(tnsBatch* batch, const char* OverrideCommand, real* OverrideUniformColor, int OverrideAsArray);

void tnsVertex3d(real x, real y, real z);
void tnsVertex2d(real x, real y);
void tnsVertexArray2d(real *verts, int amount);
void tnsVertexArray3d(real *verts, int amount);
void tnsColorArray4d(real *colors, int amount);
void tnsNormalArray3d(real *normals, int amount);
void tnsTexCoordArray2d(real *coords, int amount);
void tnsTexCoordArray3d(real *coords, int amount);
void tnsIndexArray(GLuint *index, short amount);
void tnsPackAs(GLenum Mode);
void tnsFlush();

#define tnsLinearItp(L, R, T) \
    ((L) * (1.0f - (T)) + (R) * (T))

double tnsGetRatiod(real L, real R, real FromL);

void tnsShaderApplyProjectionInverse(tnsShader *tns, tnsMatrix44d m);
void tnsShaderApplyProjection(tnsShader *tns, tnsMatrix44d m);
void tnsShaderApplyModel(tnsShader *tns, tnsMatrix44d m);
void tnsShaderApplyView(tnsShader *tns, tnsMatrix44d m);
void tnsShaderApplyNormalScaler(tnsShader *tns, tnsMatrix44d m);


int tnsLineIntersectTest2d(tnsVector2d a1, tnsVector2d a2, tnsVector2d b1, tnsVector2d b2, double *aRatio);
double tnsGetLineZ(tnsVector3d L, tnsVector3d R, real Ratio);
double tnsGetLineZPoint(tnsVector3d L, tnsVector3d R, tnsVector3d FromL);
double tnsGetRatio3d(tnsVector3d L, tnsVector3d R, tnsVector3d FromL);
double tnsGetRatiod(real L, real R, real FromL);
real tnsInterpolate(real L, real R, real T);
void tnsInterpolate2dv(real *L, real *R, real T, real *Result);
void tnsInterpolate3dv(real *L, real *R, real T, real *Result);
void tnsInterpolateTripple2d(tnsVector2d v1, tnsVector2d v2, tnsVector2d v3, real ratio, tnsVector2d result);
void tnsVectorMinus2d(tnsVector2d result, tnsVector2d l, tnsVector2d r);
void tnsVectorMinus3d(tnsVector3d result, tnsVector3d l, tnsVector3d r);
void tnsVectorSubtract3d(tnsVector3d l, tnsVector3d r);
void tnsVectorPlus3d(tnsVector3d result, tnsVector3d l, tnsVector3d r);
void tnsVectorAccum3d(tnsVector3d l, tnsVector3d r);
void tnsVectorAccum2d(tnsVector2d l, tnsVector2d r);
void tnsVectorNegate3d(tnsVector3d result, tnsVector3d l);
void tnsVectorNegateSelf3d(tnsVector3d l);
void tnsVectorCopy2d(tnsVector2d from, tnsVector2d to);
void tnsVectorCopy3d(tnsVector3d from, tnsVector3d to);
void tnsVectorCopy4d(tnsVector4d from, tnsVector4d to);
void tnsVectorMultiSelf4d(tnsVector4d from, real num);
void tnsVectorMultiSelf3d(tnsVector3d from, real num);
void tnsVectorMultiSelf2d(tnsVector2d from, real num);
void tnsVectorMulti4d(tnsVector4d to, tnsVector4d from, real num);
void tnsVectorMulti3d(tnsVector3d to, tnsVector3d from, real num);
void tnsVectorMulti2d(tnsVector2d to, tnsVector2d from, real num);
real tnsDirectionToRad(tnsVector2d Dir);
void tnsConvert44df(tnsMatrix44d from, tnsMatrix44f to);

#define tnsVectorSet2(to, x,y) \
    {(to)[0]=x;(to)[1]=y;}
#define tnsVectorSet3(to, x,y,z) \
    {(to)[0]=x;(to)[1]=y;(to)[2]=z;}
#define tnsVectorSet4(to, x,y,z,w) \
    {(to)[0]=x;(to)[1]=y;(to)[2]=z;(to)[3]=w;}
#define tnsVectorSet2v(to, from) \
    tnsVectorSet2(to,(from)[0],(from)[1])
#define tnsVectorSet3v(to, from) \
    tnsVectorSet3(to,(from)[0],(from)[1],(from)[2])
#define tnsVectorSet4v(to, from) \
    tnsVectorSet4(to,(from)[0],(from)[1],(from)[2],(from)[3])

real tnsDistIdv2(real x1, real y1, real x2, real y2);
real tnsDist3dv(tnsVector3d l, tnsVector3d r);
real tnsDist2dv(tnsVector2d l, tnsVector2d r);

real tnsLength3d(tnsVector3d l);
real tnsLength2d(tnsVector2d l);
void tnsNormalize3d(tnsVector3d result, tnsVector3d l);
void tnsNormalizeSelf2d(tnsVector3d result);
void tnsNormalizeSelf3d(tnsVector3d result);
real tnsDot3d(tnsVector3d l, tnsVector3d r, int normalize);
real tnsDot2d(tnsVector2d l, tnsVector2d r, int normalize);
real tnsVectorCross3d(tnsVector3d result, tnsVector3d l, tnsVector3d r);
real tnsAngleRad3d(tnsVector3d from, tnsVector3d to, tnsVector3d PositiveReference);
void tnsApplyRotation33d(tnsVector3d result, tnsMatrix44d mat, tnsVector3d v);
void tnsApplyRotation43d(tnsVector3d result, tnsMatrix44d mat, tnsVector3d v);
void tnsApplyTransform43d(tnsVector3d result, tnsMatrix44d mat, tnsVector3d v);
void tnsApplyNormalTransform43d(tnsVector3d result, tnsMatrix44d mat, tnsVector3d v);
void tnsApplyTransform44d(tnsVector4d result, tnsMatrix44d mat, tnsVector4d v);
void tnsApplyTransform44dTrue(tnsVector4d result, tnsMatrix44d mat, tnsVector4d v);

void tnsLoadIdentity44d(tnsMatrix44d m);
void tnsMakeOrthoMatrix44d(tnsMatrix44d mProjection, real xMin, real xMax, real yMin, real yMax, real zMin, real zMax);
void tnsMakePerspectiveMatrix44d(tnsMatrix44d mProjection, real fFov_rad, real fAspect, real zMin, real zMax);
void tnsMakeTranslationMatrix44d(tnsMatrix44d mTrans, real x, real y, real z);
void tnsMakeRotationMatrix44d(tnsMatrix44d m, real angle_rad, real x, real y, real z);
void tnsMakeScaleMatrix44d(tnsMatrix44d m, real x, real y, real z);
void tnsMakeViewportMatrix44d(tnsMatrix44d m, real w, real h, real Far, real Near);
void tnsInverse44d(tnsMatrix44d inverse, tnsMatrix44d mat);
void tnsMultiply44d(tnsMatrix44d result, tnsMatrix44d l, tnsMatrix44d r);
void tnsMakeRotationXMatrix44d(tnsMatrix44d m, real angle_rad);
void tnsMakeRotationYMatrix44d(tnsMatrix44d m, real angle_rad);
void tnsMakeRotationZMatrix44d(tnsMatrix44d m, real angle_rad);
void tnsRemoveTranslation44d(tnsMatrix44d result, tnsMatrix44d mat);
void tnsClearTranslation44d(tnsMatrix44d mat);

real tnsAngleRad3d(tnsVector3d from, tnsVector3d to, tnsVector3d PositiveReference);
real tnsLength3d(tnsVector3d l);
void tnsNormalize2d(tnsVector2d result, tnsVector2d l);
void tnsNormalize3d(tnsVector3d result, tnsVector3d l);
void tnsNormalizeSelf3d(tnsVector3d result);
real tnsDot3d(tnsVector3d l, tnsVector3d r, int normalize);
real tnsVectorCross3d(tnsVector3d result, tnsVector3d l, tnsVector3d r);
void tnsVectorCrossOnly3d(tnsVector3d result, tnsVector3d l, tnsVector3d r);

void tnsExtractXYZEuler44d(tnsMatrix44d mat, real *xyz_result);

void tnsPrintMatrix44d(tnsMatrix44d l);

int tnsPointInsideTriangle3d(tnsVector3d v, tnsVector3d v0, tnsVector3d v1, tnsVector3d v2);

int tnsIntersectPlaneRay(tnsVector3d n, tnsVector3d p0, tnsVector3d l0, tnsVector3d l, real* t);

void tnsExtractXYZEuler44d(tnsMatrix44d mat, real *xyz_result);
void tnsExtractLocation44d(tnsMatrix44d mat, real *xyz_result);
void tnsExtractScale44d(tnsMatrix44d mat, real *xyz_result);
void tnsSelfMatrixChanged(tnsObject* o, int ApplyToChild);
void tnsGlobalMatrixChanged(tnsObject* o, int ApplyToChild);
void tnsGlobalMatrixChangedForDelta(tnsObject* o, int ApplyToChild);
void tnsSelfTransformValueChanged(tnsObject* o);
void tnsDeltaTransformValueChanged(tnsObject* o);
void tnsGlobalTransformValueChanged(tnsObject* o);

void tnsClearPlayDuplicate(tnsObject* o);
void tnsFreeEvaluatedNode(tnsEvaluatedNode* n);
void tnsFreeEvaluatedScene(tnsEvaluateData* ed);
void tnsClearPlayState(tnsObject* o);
void tnsExtrtnsEnsurePlayDuplicate(tnsObject *o);
void tnsExtractSelfTransformValue(tnsObject *o);
void tnsExtractGlobalTransformValue(tnsObject *o);

void tnsSetCurrentRoot(tnsObject *o);
void tnsInitObjectBase(tnsObject *o, tnsObject *under, char *Name, int Type,
                        real AtX, real AtY, real AtZ,
                        real RotX, real RotY, real RotZ, real RotW, u8bit RotationMode,
                        real Scale);

tnsMeshObject *tnsLoadObjectFromFile(char *FileName);

int tnsLoadExchange(char *FileName);

tnsObject *tnsFindObject(char *Name, tnsObject *From);

void tns_RegisterNodes();

void tnsCopyObjectTransformationsLocal(tnsObject* to, tnsObject* from);
void tnsCopyObjectTransformationsGlobal(tnsObject* to, tnsObject* from);
void tnsRotateObjectGlobal(tnsObject *o, real x, real y, real z, real angle, real cx,real cy,real cz);
void tnsRotateObjectDelta(tnsObject *o, real x, real y, real z, real angle);
void tnsRotateObjectLocal(tnsObject *o, real x, real y, real z, real angle);
void tnsRotateObjectLocalValues(tnsObject *o, real x, real y, real z);
void tnsRotateObjectGlobalForDelta(tnsObject *o, real x, real y, real z, real angle, real cx,real cy,real cz);
void tnsTranslateObjectGlobal(tnsObject *o, real x, real y, real z);
void tnsTranslateObjectLocal(tnsObject *o, real x, real y, real z);
void tnsSetObjectPositionLocal(tnsObject*o, real x, real y, real z);
void tnsResetObjectTransformations(tnsObject* o, int reset_loc,int reset_rot,int reset_sca);
void tnsMoveObjectLocal(tnsObject *o, real x, real y, real z);
void tnsMoveObjectDelta(tnsObject *o, real x, real y, real z);
void tnsMoveObjectGlobal(tnsObject *o, real x, real y, real z);
void tnsMoveObjectGlobalForDelta(tnsObject *o, real x, real y, real z);
void tnsScaleObject(tnsObject *o, real x,real y,real z, real cx,real cy,real cz);
void tnsScaleObjectDelta(tnsObject *o, real x,real y,real z, real cx,real cy,real cz);
int tnsCheckParentable(tnsObject* child, tnsObject* parent);
void tnsParentObject(tnsObject *child, tnsObject *parent, int KeepTransform);
void tnsUnparentObject(tnsObject *o, int KeepTransform);
void tnsZoomViewingCamera(tnsCamera *c, real Ratio);
void tnsRotateViewingCamera(tnsCamera *c, real x, real z);
void tnsTranslateViewingCamera(tnsCamera *c, int ViewportW, int ViewportH, real x, real y);

void tnsGetCameraMovingDeltas(tnsCamera *c, int ViewportW, int ViewportH, real x, real y, tnsVector4d p);

laPropContainer* tnsget_ObjectType(tnsObject* o);

void *tnsget_detached_FirstRootObject(void *UNUSED1, void *UNUSED2);

void tnsDestroyRootObject(tnsObject *root);
void tnsDestroyObject(tnsObject *o);

tnsObject *tnsCreateRootObject(char *name, int Use2D);
tnsCamera *tnsCreateCamera(tnsObject *under, char *Name, real FOV,
                              real AtX, real AtY, real AtZ,
                              real RotX, real RotY, real RotZ,
                              real FocusDistance);
tnsObject *tnsCreateInstancer(tnsObject *under, char *Name, real AtX, real AtY, real AtZ);
tnsLight *tnsCreateLight(tnsObject *under, char *Name, real AtX, real AtY, real AtZ, real Strength, int UniDirectional);
tnsMeshObject *tnsCreateMeshEmpty(tnsObject *under, char *Name, real AtX, real AtY, real AtZ);
tnsMeshObject *tnsCreateMeshPlane(tnsObject *under, char *Name, real AtX, real AtY, real AtZ, real size);
tnsShapeObject *tnsCreateShapeEmpty(tnsObject *under, char *Name, real AtX, real AtY, real AtZ);
tnsShapeObject *tnsCreateShapeSquare(tnsObject *under, char *Name, real AtX, real AtY, real AtZ, real size);

int tnsMergeMeshObjects(tnsMeshObject* into, tnsMeshObject* mo);
tnsInstancer* tnsDuplicateInstancer(tnsInstancer* from);
tnsMeshObject* tnsDuplicateMeshObject(tnsMeshObject* from);
tnsShapeObject* tnsDuplicateShappeObject(tnsShapeObject* from);

void tnsInitMeshPlane(tnsMeshObject* mo, real size);
void tnsAddMMeshPlane(tnsMeshObject* mo, real size);
void tnsInitShapeSquare(tnsShapeObject* so, real size);

int tnsAnyObjectsSelected(tnsObject* parent);
void tnsDeselectAllObjects(tnsObject* parent);
void tnsSelectAllObjects(tnsObject* parent);
void tnsSelectObject(tnsObject* o, int Select, int Toggle);

#define TNS_MMESH_EDGE_BIT 0x800000
#define TNS_MMESH_FACE_BIT 0xC00000
#define TNS_MMESH_TYPE_BIT (TNS_MMESH_FACE_BIT|TNS_MMESH_EDGE_BIT)

tnsMFace* tnsMMeshNewFace(tnsMeshObject* mo);
tnsMEdge* tnsMMeshNewEdge(tnsMeshObject* mo);
tnsMVert* tnsMMeshNewVert(tnsMeshObject* mo);
void tnsMMeshCalculateNormalFrom(tnsMFace* mf);
int tnsMMeshCalculateNormal(tnsMeshObject* mo);
void tnsMMeshEdgeAssignVerts(tnsMEdge* me,tnsMVert* mv1,tnsMVert* mv2);
tnsMEdge* tnsMMeshVertsShareEdge(tnsMVert* mv0, tnsMVert* mv1);
int tnsMMeshEdgeHasVert(tnsMEdge* me, tnsMVert* MV);
tnsMFace* tnsMMeshEdgeShareFace(tnsMEdge* me0, tnsMEdge* me1);
tnsMVert* tnsMMeshEdgeShareVert(tnsMEdge* me0, tnsMEdge* me1);
tnsMVert* tnsMMeshEdgeAnotherVert(tnsMEdge* me, tnsMVert* v);
tnsMVert* tnsMMeshEdgeStartingVert(tnsMEdge* me0, tnsMEdge* me1);
int tnsMMeshSplitFace(tnsMeshObject* mo, tnsMFace* mf, tnsMEdge* me, tnsMFace** r_f1, tnsMFace** r_f2);
tnsMFace* tnsMMeshVertsShareFace(tnsMVert* v1, tnsMVert* v2);
tnsMEdge* tnsMMeshMakeEdge(tnsMeshObject* mo, tnsMVert* v1, tnsMVert* v2);
void tnsMMeshFaceAddEdge(tnsMFace* mf, tnsMEdge* me);
int tnsMMeshFaceMatches(tnsMFace* mf, int ecount, ...);
tnsMFace* tnsMMeshMakeFaceN(tnsMeshObject* mo, int count, laListHandle* vip, tnsMEdge** r_fallback_me);
tnsMFace* tnsMMeshMakeFace4v(tnsMeshObject* mo, tnsMVert* v1,tnsMVert* v2,tnsMVert* v3,tnsMVert* v4);
int tnsMMeshLoopIsInverted(laListItemPointer* l);
int tnsMMeshEdgeInsertVert(tnsMeshObject* mo, tnsMEdge* me, tnsMVert* mv, tnsMVert* ref_e1v_optional, tnsMEdge** r_e1, tnsMEdge** r_e2);
tnsMVert* tnsMMeshEdgeInsertVertAt(tnsMeshObject* mo, tnsMEdge* me, real at, tnsMVert* ref_e1v_optional, tnsMEdge** r_e1, tnsMEdge** r_e2);
void tnsMMeshRemoveFaceOnly(tnsMeshObject* mo, tnsMFace* mf);
void tnsMMeshRemoveEdgeFace(tnsMeshObject* mo, tnsMEdge* me);
void tnsMMeshRemoveVertEdgeFace(tnsMeshObject* mo, tnsMVert* mv);

void tnsMMeshRefreshIndex(tnsMeshObject* mo);

void tnsMeshEnterEditMode(tnsMeshObject* mo);
void tnsMeshLeaveEditMode(tnsMeshObject* mo);
int tnsMMeshAnySelected(tnsMeshObject* mo);
void tnsMMeshClearExtraFlags(tnsMeshObject* mo);
void tnsMMeshDeselectAll(tnsMeshObject* mo);
void tnsMMeshSelectAll(tnsMeshObject* mo);
void tnsMMeshSelectVert(tnsMeshObject* mo, tnsMVert* mv, int select, int toggle);
void tnsMMeshSelectEdge(tnsMeshObject* mo, tnsMEdge* me, int select, int toggle);
void tnsMMeshEnsureSelectionFromVerts(tnsMeshObject* mo);
void tnsMMeshEnsureSelectionFromEdges(tnsMeshObject* mo);
void tnsMMeshEnsureSelection(tnsMeshObject* mo, int SelectMode);
void tnsMMeshExpandBandList(tnsMeshObject* mo, tnsMEdge* me, laListHandle* lst);
int tnsMMeshSelectRingBandFrom(tnsMeshObject* mo, tnsMEdge* me, int ring_band, int select, int toggle);
void tnsMMeshSelectLinked(tnsMeshObject* mo);

int tnsMMeshVertsCanMerge(tnsMeshObject* mo, tnsMVert* into, tnsMVert* mv);
int tnsMMeshMergeVerts(tnsMeshObject* mo, tnsMVert* into, tnsMVert* mv);

void tnsInvalidateMeshBatch(tnsMeshObject* mo);
void tnsRegenerateMeshBatch(tnsMeshObject* mo);
void tnsEnsureMeshBatch(tnsMeshObject* mo);
void tnsEvaluateMeshObject(tnsMeshObject* mo, tnsEvaluateData* ed);
void tnsEvaluateShapeObject(tnsShapeObject* so, tnsEvaluateData* ed);

void tnsShapeSetHole(tnsShape* s, int closed, int toggle);
void tnsShapeSetClosed(tnsShape* s, int closed, int toggle);
int tnsShapePointAnySelected(tnsShape* s);
int tnsShapeAnySelected(tnsShapeObject* so);
void tnsShapeDeselectAll(tnsShapeObject* so);
void tnsShapeSelectAll(tnsShapeObject* so);
void tnsShapeSelectPoint(tnsShapeObject* so, tnsSPoint* sp, int select, int toggle, int lr);
void tnsShapeSelectRingFrom(tnsShapeObject* so, tnsSPoint* sp, int select, int toggle);
void tnsShapeSelectLinked(tnsShapeObject* so);

void tnsShapeSetEndPoint(tnsShape* s, tnsSPoint* p);
void tnsShapeRefreshIndex(tnsShapeObject* so);
tnsShape* tnsShapeRemovePoint(tnsShapeObject* so, tnsShape* s, tnsSPoint* sp, int SplitShape);
int tnsShapeConnect(tnsShapeObject* so, tnsShape* s1, tnsShape* s2, tnsSPoint* sp1, tnsSPoint* sp2);
int tnsShapeConnectSelected(tnsShapeObject* so);
int tnsShapeExtrudeSelected(tnsShapeObject* so, int DupliOnly);

void tnsShapeEnterEditMode(tnsShapeObject* mo);
void tnsShapeLeaveEditMode(tnsShapeObject* mo);

NEED_STRUCTURE(la3DObjectDrawExtra);

void tnsDrawShapePointsSelectionID(tnsEvaluatedInstance* ei, la3DObjectDrawExtra* de);
void tnsDrawShapeObject(tnsEvaluatedInstance* ei, la3DObjectDrawExtra* de);

tnsShapeObject* tnsDuplicateShapeObject(tnsShapeObject* from);

void la_RegisterModellingOperators();
void la_RegisterShapeOperators();

void tnsGetCameraProjection(tnsMatrix44d* mat, int w, int h, tnsCamera* Camera);
void tnsGetCameraViewProjection(tnsMatrix44d* mat, int w, int h, tnsCamera* Camera);
void tnsApplyCameraView(int W, int H, tnsCamera *Camera, tnsMatrix44d out_optional_view, tnsMatrix44d out_optional_projection);
void tnsApplyShadowCameraView(tnsLight *Light);
void tnsSetCameraViewDir(tnsVector3d dir);

void tnsApplyModelMatrix(tnsMatrix44d m);

void tnsLookAt(tnsObject *o, tnsVector3d Target, tnsVector3d Up);

tnsObject* tnsEnsurePlayDuplicate(tnsObject* o);

void tnsInvalidateEvaluation(tnsObject* o);
void tnsInvalidatePlayEvaluation(tnsObject* o);
void tnsFreeEvaluatedArray(tnsEvaluateData* ed);
void tnsEvaluateThisObject(tnsObject *o, tnsEvaluateData* ed);
void tnsSetObjectTreeEvaluationArgs(tnsObject* from, tnsObject* Active, int FillOutline, int FillSelectionID, int W, int H);
void tnsEvaluateObjectTree(tnsObject* from, tnsEvaluateData* UseED, int EvaluatePlay);
NEED_STRUCTURE(laEvent);
int tnsEvaluateObjectTreeEvent(tnsEvaluateData* ed_play, tnsEvaluatedNode* en, laEvent* e);
void tnsAddEvaluatedInstance(tnsEvaluateData* ed, tnsObject* ob, tnsDrawEvaluatedInstanceF Draw, int Layer, 
    int IsActive, int MeshSelectionType, int InstanceSelectionID);
void tnsDrawLayer(tnsEvaluateData* ed,int Layer,void* CustomData);

void tnsEvaluateNewNode(tnsEvaluateData* ed, tnsObject* ob);
int tnsEvaluateTryRelinkExistingNode(tnsEvaluateData* ed, tnsObject* ob);
void tnsEvaluateFreeNode(tnsEvaluatedNode* en);
void tnsEvaluateSyncNode(tnsEvaluateData* ed, tnsObject* ob);
void tnsEnsureEvaluatedScene(tnsEvaluateData* ed, tnsObject* root);
void tnsPrintEvaluatedNode(tnsEvaluatedNode* en,int level);

void tnsDrawObjectTree(tnsObject* from, int Layers,void* CustomData, int DrawRuntime);
void tnsDrawSingleObjectOrigin(tnsObject* o);
void tnsDrawObjectOrigins(tnsObject *from, tnsObject *active, int AllOrigins);
void tnsDrawCursor(tnsObject* root);
void tnsDrawScene(int W, int H, tnsObject *root);
void tnsDrawWorld(int W, int H);

tnsMaterial *tnsCreateMaterial(char *Name);
tnsMaterial *tnsFindMaterial(char *name);
tnsMaterialSlot* tnsNewMaterialSlot(tnsObject* o);
void tnsRemoveMaterial(tnsMaterial* mat);
void tnsRemoveMaterialSlot(tnsObject* o, tnsMaterialSlot* ms);
void tnsAssignMaterialSlot(tnsObject* o, tnsMaterialSlot* ms);

NVGpaint tns_GetMaterial2D(tnsMaterial* m);
void tnsRefreshMaterialLibraries();
void tnsEnsureMaterialShader(tnsMaterial* mat, int Refresh);

void tnsClearAll();
void tnsClearColorv(real *rgba);
void tnsClearColor(real r, real g, real b, real a);
#ifdef __linux__
void tnsContextMakeCurrent(SYSGLCONTEXT context, SYSWINDOW win,void* gles_surf);
#endif
#ifdef _WIN32
void tnsContextMakeCurrent(SYSGLCONTEXT context, SYSTEMDC hdc,void* unused);
#endif
NEED_STRUCTURE(laWindow)
void tnsContextMakeFBOCurrent(tnsOffscreen* off);
void tnsContextMakeWindowCurrent(laWindow* w);
void tnsSwitchToCurrentWindowContext(laWindow* w);
void tnsDeleteContext(SYSGLCONTEXT glc);
void tnsBindVertexArray(GLuint vao);

void tnsRecalculateVertNormal(tnsVert *v);
void tnsRecalculateFaceAverageNormal(tnsFace *f);

int tnsLoadVectorGraphPackage(const char *name, unsigned int size);
real tnsGetMonoFontAdvance();
void tnsInvalidateFontCache();
int tnsLoadSystemFontMono(char* from, char* mono);
int tnsLoadSystemFont(char* from, char* name);
int tnsGetTextureMemoryComponetCount(tnsTexture *t);

void tnsUseNormal(int Use);
void tnsUseHalftone(real Use);

void tnsUniformUseOffset(tnsShader* s, int use);

void tns_ImageToTexture(tnsImage* im);
tnsImage* tnsNewImage(void* MemPNG);
void tnsUseImage(tnsImage* im);
void tnsStopUsingImage(tnsImage* im);
void tnsRefreshImage(tnsImage* im,void* data);
void tnsRemoveImage(tnsImage* im);

void tnsSetRayShaderUniformTextures(tnsOffscreen* doff);
int tnsInit2DTexture(tnsTexture *t, GLint glInternalFormat, int w, int h, int Multisample);
tnsTexture *tnsCreate2DTexture(GLint glInternalFormat, int w, int h, int Multisample);
tnsTexture *tnsCreate3DTexture(GLint glInternalFormat, int w, int h, int slices);
void tnsConfigure2DTexture(tnsTexture *t);
void tnsConfigure3DTexture(tnsTexture *t);
void tnsReconfigureTextureParameters(int Multisample);
tnsOffscreen *tnsCreate2DOffscreen(int glInternalFormat, int w, int h, int Multisample, int WithDepth, int WithStencil);
tnsOffscreen *tnsCreateDeferredOffscreen(int w, int h, int Multisample);
void tnsCopyScreenTo2DTexture(tnsTexture *target, int x_lower_left, int y_lower_left, int w, int h);
void tnsActiveTexture(GLenum tex);
void tnsBindTexture(tnsTexture *t);
void tnsBindTexture2(tnsTexture *t);
void tnsUnbindTexture();
void tnsUnbindTexture2();
void tnsUniformUseNormal(tnsShader* s, int Use);
void tnsUniformUseHalftone(tnsShader* s, real Use);
void tnsUniformHalftoneSize(tnsShader* s, real Use);
void tnsUniformUseTexture(tnsShader* s, int mode, int sample, int TexIsUInt, int Tex2IsUInt);
void tnsUniformColorMode(tnsShader *s, int mode);
void tnsUniformHCYGamma(tnsShader* s, float Gamma);
void tnsUniformInputColorSpace(tnsShader* s, int ColorSpace);
void tnsUniformOutputColorSpace(tnsShader* s, int ColorSpace);
void tnsUniformShowColorOverflowStripes(tnsShader* s, int Show);
void tnsUniformColorComposing(tnsShader* s, int Composing, real gamma, real blackpoint, int UseLut);
void tnsUniformUseMultiplyColor(tnsShader* s, int enable);
void tnsUseMaskTexture(tnsTexture *t);
void tnsUseTexture(tnsTexture *t);
void tnsUseTexture2(tnsTexture* t, int mixmode);
void tnsUseNoTexture();
void tnsUseMultiplyColor(int enable);
void tnsDeleteTexture(tnsTexture *t);
void tnsDraw2DTextureDirectly(tnsTexture *t, real x, real y, real w, real h);
void tnsDraw2DTextureArg(tnsTexture *t,
                         real x_upper_right, real y_upper_right, int w, int h,
                         real *MultiplyColor,
                         real LPadding, real RPadding, real TPadding, real BPadding);

void tnsGet2DTextureSubImage(tnsTexture* t, int xoffset, int yoffset, uint32_t width, uint32_t height, int format, int type, size_t bufSize, void *pixels);
void tnsClearTextureImage(tnsTexture* t, int tex_format, int tex_bits_type);

int tnsTextureMemorySize(tnsTexture *t, int mem);

void tnsDelete2DOffscreen(tnsOffscreen *o);
tnsOffscreen *tnsCreateOffscreenHandle();
void tnsEnsureOffscreenStatus(tnsOffscreen *off, int w, int h);
void tnsAttach2DOffscreenBuffer(tnsOffscreen *target, GLuint attatchment, tnsTexture *use);
void tnsDetach2DOffscreenBuffer(tnsOffscreen *target, GLuint which_attach_point);

void tnsDrawToOffscreen(tnsOffscreen *toff, int HowMany, GLuint *AttachmentArray);
void tnsDrawToExtraColorAttachment(tnsOffscreen *toff);
void tnsDrawToExtraNormalAttachment(tnsOffscreen *toff);
void tnsDrawToAllExtraAttachments(tnsOffscreen *toff);
void tnsDrawToOffscreenOnlyBind(tnsOffscreen *toff);
void tnsReadFromOffscreen(tnsOffscreen *toff);

void tnsDrawToScreen();

//==============================================[STR]

int tnsUseFont(char *name);
int laStopFontService();

void tfntResizeFontTexture(tnsFont* f, int size);

void tnsPushStringClip(int L, int R, int U, int B);
void tnsPopStringClip();

int tnsStringGetDimension(char* content, uint32_t* contentU, int Count, int WLimit, int* Rows, int UseMono);
int tnsStringGetWidth(char *content, int Count, int UseMono);
int tnsStringGetWidthU(uint32_t *contentU, int Count, int UseMono);
void tnsDrawStringM(char *content, uint32_t* contentU, real Color[4], int L, int R, int T, uint64_t Flags);
int tnsDrawLCD7_ProgressSystem(real x, real y, real Percent);
void tnsDrawStringLCD(char *content, uint32_t* contentU, real Color[4], int L, int R, int T, uint64_t Flags, real Scale);
void tnsDrawStringAutoM(char *content, uint32_t* contentU, real Color[4], int L, int R, int T, uint64_t Flags);
void tnsDrawStringAuto(char *content, real Color[4], int L, int R, int T, uint64_t Flags);
void tnsDrawStringWithPriority(char *Label, char *MajorContent, real Color[4], int TextAlign, int L, int R, int T, uint64_t Flags);
void tnsDrawIcon(uint32_t ID, real Color[4], int L,int R, int T, uint64_t Flags);

void tnsUseFontCoord(real x, real y, real size);

///=================================

void tnsMakeTriangle(real *arr, real x1, real y1, real x2, real y2, real x3, real y3);
void tnsMakeQuad2d(real *arr, real x1, real y1, real x2, real y2, real x3, real y3, real x4, real y4);
void tnsMakeQuadT2d(real *arr, real x1, real y1, real x2, real y2, real x3, real y3, real x4, real y4);
void tnsMakeQuad3d(real *arr, real x1, real y1, real z1, real x2, real y2, real z2, real x3, real y3, real z3, real x4, real y4, real z4);
void tnsMakeQuad4d(real *arr, real x1, real y1, real z1, real w1, real x2, real y2, real z2, real w2, real x3, real y3, real z3, real w3, real x4, real y4, real z4, real w4);
void tnsMakeCircle2d(real *arr, int slices, real ctrX, real ctrY, real r, int jump);
void tnsMakeArc2d(real *arr, int slices, real ctrX, real ctrY, real r, real rad_begin, real rad_end);
void tnsMakeRing2d(real *arr, int *index, int slices, real ctrX, real ctrY, real r1, real r2);

void tnsMakeLinerGradient3d(real *arr, int num_points, real r0, real g0, real b0, real r1, real g1, real b1);
void tnsMakeLinerGradient4d(real *arr, int num_points, real r0, real g0, real b0, real a0, real r1, real g1, real b1, real a1);
void tnsMakeLinerGradient3dv(real *arr, int num_points, real *rgb0, real *rgb1);
void tnsMakeLinerGradient4dv(real *arr, int num_points, real *rgb0, real *rgb1);

void tnsMakeFoucsSquare(int L, int R, int U, int B, int W);
void tnsDrawFloor(real* CamPosition, real Far, int *ShowAxis);
void tnsDraw2DGrid10(real L, real R, real U, real B, real xmin, real xmax, real ymin, real ymax, real MostDenseW, real MostDenseH,
                     real* color4, real AlphaFactor, int ShowGrid, int TextAlign);

void tnsMakeIndexUInt(unsigned int *result, int num, ...);
void tnsMakeBridgedIndex(unsigned int *result, int num, int revert, int begin);

//================================

void DrawWireRect2dp(real x, real y, real x2, real y2);
void tnsViewportWithScissor(int x, int y, int w, int h);

void tnssRGB2XYZ(tnsVector3d rgb,tnsVector3d xyz);
void tnsClay2XYZ(tnsVector3d rgb,tnsVector3d xyz);
void tnsXYZ2sRGB(tnsVector3d xyz,tnsVector3d rgb);
void tnsXYZ2Clay(tnsVector3d xyz,tnsVector3d rgb);
void tnsRGB2Clay(tnsVector3d rgb, tnsVector3d clay);
void tnsClay2RGB(tnsVector3d clay, tnsVector3d rgb);

void tns2LogsRGBSingle(real* a);
void tns2LinearsRGBSingle(real* a);
void tns2LogsRGB(real* srgb);
void tns2LinearsRGB(real* srgb);
void tnsRGB2OKLAB(real* rgb, real* oklab);
void tnsOKLAB2RGB(real* oklab, real* rgb);

void tnsHCY2RGBLinear(real *hcy, real *rgb);
void tnsRGB2HCYLinear(real *rgb, real *hcy);
void tnsHCY2RGB(real *hcy, real *rgb);
void tnsRGB2HCY(real *rgb, real *hcy);
void tnsRGB2HSL(real *rgb, real *hsl);
void tnsHSL2RGB(real *hsl, real *rgb);
void tnsYUV2RGB(real* yuv,real* rgb);

#ifdef __cplusplus
extern "C"{
#endif

extern const char* LA_TNS_LIB_COMMON;

#ifdef __cplusplus
}
#endif
