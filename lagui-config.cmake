# LaGUI CMake config file.

get_filename_component(SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(${SELF_DIR}/lagui-targets.cmake)
get_filename_component(LAGUI_INCLUDE_DIRS "${SELF_DIR}/../../include/lagui" ABSOLUTE)

if (POLICY CMP0072)
  set(OpenGL_GL_PREFERENCE GLVND)
endif()

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_LIST_DIR})

SET(LAGUI_USE_GLES false CACHE BOOL "Whether to use GLES in LaGUI")
if(${LAGUI_USE_GLES})
    add_definitions(-DLA_USE_GLES)
    set(LAGUI_GL_LIB ${OPENGL_egl_LIBRARY} ${OPENGL_gles2_LIBRARY} ${OPENGL_opengl_LIBRARY})
else()
    set(LAGUI_GL_LIB ${OPENGL_glx_LIBRARY} ${OPENGL_opengl_LIBRARY})
endif()

find_package(OpenGL REQUIRED)
find_package(X11 REQUIRED)
find_package(Freetype REQUIRED)
find_package(GLEW REQUIRED)
find_package(PNG)
find_package(LuaJIT)

set(CMAKE_THREAD_PREFER_PTHREAD ON)
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

SET(LAGUI_USE_LUAJIT true CACHE BOOL "Whether to use LuaJIT in LaGUI")
if(${LuaJIT_FOUND} AND ${LAGUI_USE_LUAJIT})
    add_definitions(-DLA_WITH_LUAJIT)
endif()

SET(LAGUI_USE_PNG true CACHE BOOL "Whether to use LuaJIT in LaGUI")
if(${PNG_FOUND} AND ${LAGUI_USE_PNG})
    add_definitions(-DLA_WITH_PNG)
endif()

if (CMAKE_SYSTEM_NAME MATCHES "Linux")
    set(LAGUI_SHARED_LIBS
        ${OPENGL_LIBRARY}
        ${X11_LIBRARIES}
        ${X11_X11_LIB}
        ${GLEW_LIBRARIES}
        ${LAGUI_GL_LIB}
        ${FREETYPE_LIBRARIES}
        ${X11_Xfixes_LIB}
        ${X11_Xrandr_LIB}
        m X11 Xi Xcursor
        ${CMAKE_DL_LIBS}
        Threads::Threads
        ${LUA_LIBRARY}
        lagui
        CACHE INTERNAL "LaGUI shared libs"
    )
    set(LAGUI_INCLUDE_DIRS_ALL
        ${OPENGL_LIBRARY}
        ${CMAKE_SOURCE_DIR}
        ${X11_INCLUDE_DIR}
        ${GLEW_INCLUDE_PATH}
        ${FREETYPE_INCLUDE_DIRS}
        ${LAGUI_INCLUDE_DIRS}
        CACHE INTERNAL "Include dirs of LaGUI and dependencies"
    )
    if(${LuaJIT_FOUND})
        list(APPEND LAGUI_SHARED_LIBS ${LUA_LIBRARY})
        list(APPEND LAGUI_INCLUDE_DIRS_ALL ${LUA_INCLUDE_DIR})
    endif()
    if(${PNG_FOUND})
        list(APPEND LAGUI_SHARED_LIBS ${PNG_LIBRARY})
        list(APPEND LAGUI_INCLUDE_DIRS_ALL ${PNG_INCLUDE_DIR})
    endif()
elseif (CMAKE_SYSTEM_NAME MATCHES "Windows")
    set(LAGUI_SHARED_LIBS
        ${GLEW_LIBRARIES}
        ${OPENGL_LIBRARY}
        ${FREETYPE_LIBRARIES}
        ${CMAKE_DL_LIBS}
        Threads::Threads
        lagui shlwapi Shcore Imm32
        CACHE INTERNAL "LaGUI shared libs"
    )
    set(LAGUI_INCLUDE_DIRS_ALL
        ${CMAKE_SOURCE_DIR}
        ${GLEW_INCLUDE_PATH}
        ${FREETYPE_INCLUDE_DIRS}
        ${LAGUI_INCLUDE_DIRS}
        CACHE INTERNAL "Include dirs of LaGUI and dependencies"
    )
    if(${LuaJIT_FOUND})
        list(APPEND LAGUI_SHARED_LIBS ${LUA_LIBRARY})
        list(APPEND LAGUI_INCLUDE_DIRS_ALL ${LUA_INCLUDE_DIR})
    endif()
    if(${PNG_FOUND})
        list(APPEND LAGUI_SHARED_LIBS ${PNG_LIBRARY})
        list(APPEND LAGUI_INCLUDE_DIRS_ALL ${PNG_INCLUDE_DIR})
    endif()
endif()

# Build Types
set(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE}
    CACHE STRING "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel tnsan asan lsan msan ubsan"
    FORCE)

# ThreadSanitizer
set(CMAKE_C_FLAGS_TSAN
    "-fsanitize=thread -g -O1"
    CACHE STRING "Flags used by the C compiler during ThreadSanitizer builds."
    FORCE)
set(CMAKE_CXX_FLAGS_TSAN
    "-fsanitize=thread -g -O1"
    CACHE STRING "Flags used by the C++ compiler during ThreadSanitizer builds."
    FORCE)

# AddressSanitize
set(CMAKE_C_FLAGS_ASAN
    "-fsanitize=address -fno-optimize-sibling-calls -fsanitize-address-use-after-scope -fno-omit-frame-pointer -g"
    CACHE STRING "Flags used by the C compiler during AddressSanitizer builds."
    FORCE)
set(CMAKE_CXX_FLAGS_ASAN
    "-fsanitize=address -fno-optimize-sibling-calls -fsanitize-address-use-after-scope -fno-omit-frame-pointer -g"
    CACHE STRING "Flags used by the C++ compiler during AddressSanitizer builds."
    FORCE)

# LeakSanitizer
set(CMAKE_C_FLAGS_LSAN
    "-fsanitize=leak -fno-omit-frame-pointer -g"
    CACHE STRING "Flags used by the C compiler during LeakSanitizer builds."
    FORCE)
set(CMAKE_CXX_FLAGS_LSAN
    "-fsanitize=leak -fno-omit-frame-pointer -g"
    CACHE STRING "Flags used by the C++ compiler during LeakSanitizer builds."
    FORCE)

# MemorySanitizer
set(CMAKE_C_FLAGS_MSAN
    "-fsanitize=memory -fno-optimize-sibling-calls -fsanitize-memory-track-origins=2 -fno-omit-frame-pointer -g"
    CACHE STRING "Flags used by the C compiler during MemorySanitizer builds."
    FORCE)
set(CMAKE_CXX_FLAGS_MSAN
    "-fsanitize=memory -fno-optimize-sibling-calls -fsanitize-memory-track-origins=2 -fno-omit-frame-pointer -g"
    CACHE STRING "Flags used by the C++ compiler during MemorySanitizer builds."
    FORCE)

# UndefinedBehaviour
set(CMAKE_C_FLAGS_UBSAN
    "-fsanitize=undefined"
    CACHE STRING "Flags used by the C compiler during UndefinedBehaviourSanitizer builds."
    FORCE)
set(CMAKE_CXX_FLAGS_UBSAN
    "-fsanitize=undefined"
    CACHE STRING "Flags used by the C++ compiler during UndefinedBehaviourSanitizer builds."
	FORCE)

set(LAGUI_FONTS
    "NotoEmoji-Regular.ttf"
    "NotoSansCJK-Regular.ttc"
    "NotoSansMono-Regular.ttf"
    "NotoSansSymbols-Regular.ttf"
    "NotoSansSymbols2-Regular.ttf"
)
