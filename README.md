# LaGUI

LaGUI 是一个图形应用程序框架。

版权所有 (C) 2022-2025 吴奕茗

了解更多: https://ChengduLittleA.com/lagui

赞助：https://patreon.com/ChengduLittleA

LaGUI 采用 GNU GPL v3 许可证，Noto 字体采用 SIL Open Font 许可证
您将在源代码文件夹找到许可证的具体信息。

-----------------

# LaGUI

LaGUI: A graphical application framework.

Copyright (C) 2022-2025 Wu Yiming

Learn more about LaGUI: https://ChengduLittleA.com/lagui

Support the development: https://patreon.com/ChengduLittleA

LaGUI is licensed with GNU GPL v3, and Noto fonts are licensed with SIL Open Font license.
You should be able to find details about the license in the source code directory.
